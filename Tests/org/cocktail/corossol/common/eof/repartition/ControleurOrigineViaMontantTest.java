package org.cocktail.corossol.common.eof.repartition;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class ControleurOrigineViaMontantTest {

	private static final String STR_500 = "500";
	private static final String STR_300 = "300";

	private static final int INT_60 = 60;
	private static final int INT_100 = 100;
	private static final int INT_300 = 300;
	private static final int INT_500 = 500;

	private IControleurOrigine controleur;

	@Before
	public void setUp() throws Exception {
		controleur = new ControleurOrigineViaMontant();
	}

	@Test
	public void testUpdateInventaireComptableOrigine() {
		testViaMontant(INT_500, STR_500, null, bigD(INT_500), bigD(INT_100));
		testViaMontant(INT_500, STR_300, null, bigD(INT_300), bigD(INT_60));
		testViaMontant(INT_500, "166.65", null, bigD(166.65), bigD(33.33));
		testViaMontant(INT_500, "443.36", null, bigD(443.36), bigD(88.67));
		testViaMontant(INT_500, "443.38", null, bigD(443.38), bigD(88.68));
//		testViaMontant(INT_500, "443,38", null, bigD(443.38), bigD(88.68));

		// Avec des valeurs de pourcentage pour verifier que c'est bien écrasé.
		testViaMontant(INT_500, STR_500, "12", bigD(INT_500), bigD(INT_100));
		testViaMontant(INT_500, STR_500, "", bigD(INT_500), bigD(INT_100));
		testViaMontant(INT_500, STR_500, "100", bigD(INT_500), bigD(INT_100));
		testViaMontant(INT_500, STR_500, "500", bigD(INT_500), bigD(INT_100));
		// TODO avec des valeurs de pourcentage pour verifier que c'est bien écrasé.
	}

	private void testViaMontant(double montantAcquisition, String montant, String pourcentage, BigDecimal montantFinal, BigDecimal pourcentageFinal) {
		IInventaireComptable inventaireComptable = new MockInventaireComptable(bigD(montantAcquisition));
		IInventaireComptableOrig origine = new InventaireComptableOrigBean(inventaireComptable, montant, pourcentage);
		controleur.updateInventaireComptableOrigine(origine);

		assertEquals(montantFinal, origine.icorMontant());
		assertEquals(pourcentageFinal, origine.icorPourcentage());
	}

	/**
	 * @return
	 */
	private BigDecimal bigD(double val) {
		return BigDecimal.valueOf(val).setScale(2);
	}

}
