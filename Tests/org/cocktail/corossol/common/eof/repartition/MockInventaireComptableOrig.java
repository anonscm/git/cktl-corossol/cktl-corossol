package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import org.cocktail.corossol.common.eof.repartition.IInventaireComptable;
import org.cocktail.corossol.common.eof.repartition.IInventaireComptableOrig;
import org.cocktail.corossol.common.eof.repartition.ITitre;

class MockInventaireComptableOrig implements IInventaireComptableOrig {

	private IInventaireComptable inventaireComptable;
	private BigDecimal icorMontant;
	private BigDecimal icorPourcentage;

	public MockInventaireComptableOrig(String montantAcquisition, String montant, String pourcentage) {
		this.setIcorMontant(bigD(montant));
		this.setIcorPourcentage(bigD(pourcentage));
		this.inventaireComptable = new MockInventaireComptable(bigD(montantAcquisition));
	}

	private BigDecimal bigD(String val) {
		if (val == null) {
			return null;
		}
		return BigDecimal.valueOf(Double.valueOf(val)).setScale(2);
	}

	public void setIcorPourcentage(BigDecimal value) {
		icorPourcentage = value;
	}

	public void setIcorMontant(BigDecimal value) {
		icorMontant = value;
	}

	public BigDecimal icorPourcentage() {
		return icorPourcentage;
	}

	public BigDecimal icorMontant() {
		return icorMontant;
	}

	public IInventaireComptable inventaireComptable() {
		return inventaireComptable;
	}

	public void setInventaireComptable(IInventaireComptable inventaireComptable) {
		this.inventaireComptable = inventaireComptable;
	}

	public ITitre titre() {
		return null;
	}
}