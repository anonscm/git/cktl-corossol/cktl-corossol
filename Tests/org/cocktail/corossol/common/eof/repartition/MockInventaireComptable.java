package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class MockInventaireComptable implements IInventaireComptable {

	private BigDecimal montantAmortissable;
	private BigDecimal montantAmortissableAvecOrvs;

	public MockInventaireComptable(BigDecimal montantAcquisition) {
		this.montantAmortissable = montantAcquisition;
		this.montantAmortissableAvecOrvs = montantAcquisition;
	}

	public MockInventaireComptable(BigDecimal montantAcquisition, BigDecimal montantOrvs) {
		this.montantAmortissable = montantAcquisition;
		this.montantAmortissableAvecOrvs = montantAcquisition.add(montantOrvs);
	}

	public BigDecimal invcMontantAmortissable() {
		return montantAmortissable;
	}

	public BigDecimal sommePourcentageOriginesFinancement() {
		return null;
	}

	public BigDecimal sommeMontantOriginesFinancement() {
		return null;
	}

	public String methodeDeCalculPourLesRepartitions() {
		return null;
	}

	public NSArray inventaireComptableOrigs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		return null;
	}

	public BigDecimal invcMontantAmortissableAvecOrvs() {
		return montantAmortissableAvecOrvs;
	}
}
