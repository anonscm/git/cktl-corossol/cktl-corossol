/**
 * 
 */
package org.cocktail.corossol.common.eof.repartition;

import static org.junit.Assert.fail;

import org.junit.Test;

import com.webobjects.foundation.NSValidation;

/**
 * @author kahlua
 * 
 */
public class InventaireComptableOrigBeanTest {

	@Test
	public void testConstructeur() {
		checkConstructeur(null, null, true);
		checkConstructeur("", "", true);
		checkConstructeur("123", null, false);
		checkConstructeur(null, "123", false);
		checkConstructeur("123", "", false);
		checkConstructeur("", "123", false);
		checkConstructeur("123", "123", false);

		checkConstructeur("aze", null, true);
		checkConstructeur(null, "aze", true);
		checkConstructeur("aze", "aze", true);

	}

	private void checkConstructeur(String montant, String pourcentage, boolean mustFail) {
		try {
			new InventaireComptableOrigBean(null, montant, pourcentage);
			if (mustFail) {
				fail("Ce test devrait échouer pour " + montant + " / " + pourcentage);
			}
		} catch (NSValidation.ValidationException e) {
			if (!mustFail) {
				fail("Ce test ne devrait pas échouer pour " + montant + " / " + pourcentage);
			}
		} catch (NumberFormatException e) {
			if (!mustFail) {
				fail("Ce test ne devrait pas échouer pour " + montant + " / " + pourcentage);
			}
		} catch (Exception e) {
			fail("Type d'exception non prévu : " + e);
		}
	}

}
