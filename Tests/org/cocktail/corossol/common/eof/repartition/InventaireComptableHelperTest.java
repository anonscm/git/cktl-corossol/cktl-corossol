package org.cocktail.corossol.common.eof.repartition;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.serveur.eof.metier.EOInventaireComptableOrig;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class InventaireComptableHelperTest {
	private static final int INT_100 = 100;
	private static final int INT_1999700 = 1999700;
	private static final int INT_2000000 = 2000000;
	private static final int INT_6000000 = 6000000;

	private static final double DBL_33_33 = 33.33d;
	private static final double DBL_33_34 = 33.34d;

	@Rule
	public MockEditingContext edc = new MockEditingContext("corossol");

	@Test
	public void testCorrigerOrigines() {
		IInventaireComptableOrig orig1 = EOInventaireComptableOrig.creerInstance(edc);
		IInventaireComptableOrig orig2 = EOInventaireComptableOrig.creerInstance(edc);
		IInventaireComptableOrig orig3 = EOInventaireComptableOrig.creerInstance(edc);

		orig1.setIcorMontant(bigD(INT_2000000));
		orig2.setIcorMontant(bigD(INT_2000000));
		orig3.setIcorMontant(bigD(INT_2000000));

		IInventaireComptable inv = EOInventaireComptable.createEOInventaireComptable(edc);
		((EOInventaireComptable) inv).setInvcMontantAcquisition(bigD(INT_6000000));
		((EOInventaireComptable) inv).setInvcMontantAmortissable(bigD(INT_6000000));
		((EOInventaireComptable) inv).addToInventaireComptableOrigsRelationship((EOInventaireComptableOrig) orig1);
		((EOInventaireComptable) inv).addToInventaireComptableOrigsRelationship((EOInventaireComptableOrig) orig2);
		((EOInventaireComptable) inv).addToInventaireComptableOrigsRelationship((EOInventaireComptableOrig) orig3);

		InventaireComptableHelper.corrigerOrigines(inv);

		assertEquals(bigD(INT_2000000), orig1.icorMontant());
		assertEquals(bigD(DBL_33_33), orig1.icorPourcentage());

		assertEquals(bigD(INT_2000000), orig2.icorMontant());
		assertEquals(bigD(DBL_33_33), orig2.icorPourcentage());
		// Correction du pourcentage
		assertEquals(bigD(INT_2000000), orig3.icorMontant());
		assertEquals(bigD(DBL_33_34), orig3.icorPourcentage());

	}

	@Test
	public void testCorrigerOriginesPetitMontant() {

		IInventaireComptableOrig orig1 = EOInventaireComptableOrig.creerInstance(edc);
		IInventaireComptableOrig orig2 = EOInventaireComptableOrig.creerInstance(edc);
		IInventaireComptableOrig orig3 = EOInventaireComptableOrig.creerInstance(edc);
		IInventaireComptableOrig orig4 = EOInventaireComptableOrig.creerInstance(edc);
		IInventaireComptableOrig orig5 = EOInventaireComptableOrig.creerInstance(edc);
		IInventaireComptableOrig orig6 = EOInventaireComptableOrig.creerInstance(edc);

		orig1.setIcorMontant(bigD(INT_100));
		orig2.setIcorMontant(bigD(INT_100));
		orig3.setIcorMontant(bigD(INT_100));
		orig4.setIcorMontant(bigD(INT_2000000));
		orig5.setIcorMontant(bigD(INT_2000000));
		orig6.setIcorMontant(bigD(INT_1999700));

		IInventaireComptable inv = EOInventaireComptable.createEOInventaireComptable(edc);
		((EOInventaireComptable) inv).setInvcMontantAcquisition(bigD(INT_6000000));
		((EOInventaireComptable) inv).setInvcMontantAmortissable(bigD(INT_6000000));
		((EOInventaireComptable) inv).addToInventaireComptableOrigsRelationship((EOInventaireComptableOrig) orig2);
		((EOInventaireComptable) inv).addToInventaireComptableOrigsRelationship((EOInventaireComptableOrig) orig3);
		((EOInventaireComptable) inv).addToInventaireComptableOrigsRelationship((EOInventaireComptableOrig) orig4);
		((EOInventaireComptable) inv).addToInventaireComptableOrigsRelationship((EOInventaireComptableOrig) orig5);
		((EOInventaireComptable) inv).addToInventaireComptableOrigsRelationship((EOInventaireComptableOrig) orig6);
		((EOInventaireComptable) inv).addToInventaireComptableOrigsRelationship((EOInventaireComptableOrig) orig1);

		InventaireComptableHelper.corrigerOrigines(inv);

		// Tests
		
		assertEquals(bigD(INT_100), orig1.icorMontant());
		assertEquals(bigD(0.00d), orig1.icorPourcentage());

		assertEquals(bigD(INT_100), orig2.icorMontant());
		assertEquals(bigD(0.00d), orig2.icorPourcentage());

		assertEquals(bigD(INT_100), orig3.icorMontant());
		assertEquals(bigD(0.00d), orig3.icorPourcentage());

		// Correction du pourcentage
		assertEquals(bigD(INT_2000000), orig4.icorMontant());
		assertEquals(bigD(DBL_33_34), orig4.icorPourcentage());

		assertEquals(bigD(INT_2000000), orig5.icorMontant());
		assertEquals(bigD(DBL_33_33), orig5.icorPourcentage());

		assertEquals(bigD(INT_1999700), orig6.icorMontant());
		assertEquals(bigD(DBL_33_33), orig6.icorPourcentage());
	}

	private BigDecimal bigD(double d) {
		return BigDecimal.valueOf(d).setScale(2);
	}
}
