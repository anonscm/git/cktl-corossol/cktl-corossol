package org.cocktail.corossol.common.eof.repartition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.cocktail.corossol.common.zutil.CalculMontantUtil;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class RepartitionTest {
	private static final String STR_500 = "500";
	private static final String STR_100 = "100";

	private static final int INT_20 = 20;
	private static final int INT_40 = 40;
	private static final int INT_60 = 60;
	private static final int INT_100 = 100;
	private static final int INT_300 = 300;
	private static final int INT_500 = 500;
	private static final int INT_2000 = 2000;
	private static final int INT_4000 = 4000;
	private static final int INT_6000 = 6000;
	
	private static final int INT_2000000 = 2000000;
	private static final int INT_4000000 = 4000000;
	private static final int INT_6000000 = 6000000;
	
	

	private static final double DBL_33_33 = 33.33d;
	private static final double DBL_33_34 = 33.34d;
	private static final double DBL_66_67 = 66.67d;

	@Test
	public void testPourcentageDuMontant() {
		assertEquals(bigD(INT_100), CalculMontantUtil.pourcentageDuMontant(bigD(INT_500), bigD(INT_500)));
		assertEquals(bigD(INT_60), CalculMontantUtil.pourcentageDuMontant(bigD(INT_500), bigD(INT_300)));
		assertEquals(BigDecimal.ZERO, CalculMontantUtil.pourcentageDuMontant(bigD(INT_500), BigDecimal.ZERO));

		assertEquals(bigD(DBL_33_33), CalculMontantUtil.pourcentageDuMontant(bigD(INT_60), bigD(INT_20)));
		assertEquals(bigD(DBL_66_67), CalculMontantUtil.pourcentageDuMontant(bigD(INT_60), bigD(INT_40)));

		assertEquals(bigD(DBL_33_33), CalculMontantUtil.pourcentageDuMontant(bigD(INT_6000), bigD(INT_2000)));
		assertEquals(bigD(DBL_66_67), CalculMontantUtil.pourcentageDuMontant(bigD(INT_6000), bigD(INT_4000)));

		try {
			CalculMontantUtil.pourcentageDuMontant(BigDecimal.ZERO, bigD(INT_500));
			fail("Exception attendue");
		} catch (Exception e) {
		}

		try {
			CalculMontantUtil.pourcentageDuMontant(null, bigD(INT_500));
			fail("Exception attendue");
		} catch (Exception e) {
		}

		try {
			CalculMontantUtil.pourcentageDuMontant(bigD(INT_500), null);
			fail("Exception attendue");
		} catch (Exception e) {
		}
	}

	@Test
	public void testMontantDuPourcentage() {
		assertEquals(bigD(INT_500), CalculMontantUtil.montantDuPourcentage(bigD(INT_500), bigD(INT_100)));
		assertEquals(bigD(INT_300), CalculMontantUtil.montantDuPourcentage(bigD(INT_500), bigD(INT_60)));
		assertEquals(BigDecimal.ZERO, CalculMontantUtil.montantDuPourcentage(bigD(INT_500), BigDecimal.ZERO));

		assertEquals(bigD(INT_20), CalculMontantUtil.montantDuPourcentage(bigD(INT_60), bigD(DBL_33_33)));
		assertEquals(bigD(INT_40), CalculMontantUtil.montantDuPourcentage(bigD(INT_60), bigD(DBL_66_67)));

//		assertEquals(bigD(INT_4000), CalculMontantUtil.montantDuPourcentage(bigD(INT_6000), bigD(DBL_66_67)));
//		assertEquals(bigD(INT_2000), CalculMontantUtil.montantDuPourcentage(bigD(INT_6000), bigD(DBL_33_33)));

		try {
			CalculMontantUtil.montantDuPourcentage(BigDecimal.ZERO, bigD(INT_100));
			fail("Exception attendue");
		} catch (Exception e) {
		}

		try {
			CalculMontantUtil.montantDuPourcentage(null, bigD(INT_100));
			fail("Exception attendue");
		} catch (Exception e) {
		}

		try {
			CalculMontantUtil.montantDuPourcentage(bigD(INT_500), bigD(INT_500));
			fail("Exception attendue");
		} catch (Exception e) {
		}

		try {
			CalculMontantUtil.montantDuPourcentage(bigD(INT_500), null);
			fail("Exception attendue");
		} catch (Exception e) {
		}
	}

	@Test
	public void testCalculRepartitionUniqueCreation() {
		// Tester les autres cas simples : 1 origine, divers données en entrée (valides ou non)
		String chaineMontant = STR_500;
		String chainePourcentage = "";

		BigDecimal invcMontantAmortissable = bigD(INT_500);
		BigDecimal sommeMontantOriginesFinancement = BigDecimal.ZERO;
		BigDecimal sommePourcentageOriginesFinancement = BigDecimal.ZERO;

		IInventaireComptable inventaireComptable = initInvCompt(invcMontantAmortissable, sommeMontantOriginesFinancement, sommePourcentageOriginesFinancement);

		try {
			IInventaireComptableOrig currentInvComptableOrig = CalculMontantUtil.calculRepartition(chaineMontant, chainePourcentage, inventaireComptable);
			assertEquals(bigD(INT_500), currentInvComptableOrig.icorMontant());
			assertEquals(bigD(INT_100), currentInvComptableOrig.icorPourcentage());
		} catch (Exception e) {
			e.printStackTrace();
			fail("Ne devrait pas échouer");
		}
	}

	@Test
	public void testCalculRepartitionMultiOrigineCreation() {
		// Tester les autres cas simples : 1 origine, divers données en entrée (valides ou non)
		String chaineMontant = STR_500;
		String chainePourcentage = STR_100;

		BigDecimal invcMontantAmortissable = bigD(INT_500);
		BigDecimal sommeMontantOriginesFinancement = bigD(INT_300);
		BigDecimal sommePourcentageOriginesFinancement = bigD(INT_60);

		IInventaireComptable inventaireComptable = initInvCompt(invcMontantAmortissable, sommeMontantOriginesFinancement, sommePourcentageOriginesFinancement);

		try {
			IInventaireComptableOrig currentInvComptableOrig = CalculMontantUtil.calculRepartition(chaineMontant, chainePourcentage, inventaireComptable);
			assertEquals(bigD(INT_500), currentInvComptableOrig.icorMontant());
			assertEquals(bigD(INT_100), currentInvComptableOrig.icorPourcentage());
		} catch (Exception e) {
			fail("Ne devrait pas échouer");
		}
		// --		
		inventaireComptable = initInvCompt(bigD(INT_6000000), sommeMontantOriginesFinancement, sommePourcentageOriginesFinancement);
		try {
			IInventaireComptableOrig currentInvComptableOrig = CalculMontantUtil.calculRepartition("" + INT_2000000, null, inventaireComptable);
			assertEquals(bigD(INT_2000000), currentInvComptableOrig.icorMontant());
			assertEquals(bigD(DBL_33_33), currentInvComptableOrig.icorPourcentage());
		} catch (Exception e) {
			fail("Ne devrait pas échouer");
		}		
	}

	@Test
	public void testCheckTitreMontantDisponible() {
		// TODO
	}

	// -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

	/**
	 * @return
	 */
	private BigDecimal bigD(double d) {
		return BigDecimal.valueOf(d).setScale(2);
	}
	
	private IInventaireComptable initInvCompt(final BigDecimal invcMontantAmortissable, final BigDecimal sommeMontantOriginesFinancement, 
			final BigDecimal sommePourcentageOriginesFinancement) {
		return initInvCompt(invcMontantAmortissable, sommeMontantOriginesFinancement, sommePourcentageOriginesFinancement, null);
	}

	private IInventaireComptable initInvCompt(final BigDecimal invcMontantAmortissable, final BigDecimal sommeMontantOriginesFinancement, 
			final BigDecimal sommePourcentageOriginesFinancement, final NSArray origines) {
		return new IInventaireComptable() {
			public BigDecimal sommePourcentageOriginesFinancement() {
				return sommePourcentageOriginesFinancement;
			}

			public BigDecimal sommeMontantOriginesFinancement() {
				return sommeMontantOriginesFinancement;
			}

			public BigDecimal invcMontantAmortissable() {
				return invcMontantAmortissable;
			}

			public String methodeDeCalculPourLesRepartitions() {
				return MONTANT;
			}

			public NSArray inventaireComptableOrigs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
				return origines;
			}

			public BigDecimal invcMontantAmortissableAvecOrvs() {
				return invcMontantAmortissable;
			}
		};
	}
}
