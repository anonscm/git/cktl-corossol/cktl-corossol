package org.cocktail.corossol.common.eof.repartition;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class ControleurOrigineViaPourcentageTest {

	private static final String STR_100 = "100";

	private static final int INT_60 = 60;
	private static final int INT_100 = 100;
	private static final int INT_240 = 240;
	private static final int INT_300 = 300;
	private static final int INT_400 = 400;
	private static final int INT_500 = 500;

	private IControleurOrigine controleur;

	@Before
	public void setUp() throws Exception {
		controleur = new ControleurOrigineViaPourcentage();
	}

	@Test
	public void testCheckValidite() {
		// TODO
	}

	@Test
	public void testUpdateInventaireComptableOrigine() {
		testViaPourcentage(INT_500, null, STR_100, bigD(INT_500), bigD(INT_100));
		testViaPourcentage(INT_500, null, "60", bigD(INT_300), bigD(INT_60));
		testViaPourcentage(INT_500, null, "33.33", bigD(166.65), bigD(33.33));
		testViaPourcentage(500.33, null, "88.67", bigD(443.64), bigD(88.67));
		testViaPourcentage(500.66, null, "88.68", bigD(443.99), bigD(88.68));
		
		// Avec ORV
		testViaPourcentage(INT_500, -INT_100, null, STR_100, bigD(INT_400), bigD(INT_100));
		testViaPourcentage(INT_500, -INT_100, null, "60", bigD(INT_240), bigD(INT_60));
		testViaPourcentage(INT_500, -INT_100, null, "33.33", bigD(133.32), bigD(33.33));
		testViaPourcentage(500.33, -INT_100, null, "88.67", bigD(354.97), bigD(88.67));
		testViaPourcentage(500.66, -INT_100, null, "88.68", bigD(355.31), bigD(88.68));
	}

	private void testViaPourcentage(double montantAcquisition, String montant, String pourcentage, BigDecimal montantFinal, BigDecimal pourcentageFinal) {
		IInventaireComptable inventaireComptable = new MockInventaireComptable(bigD(montantAcquisition));
		IInventaireComptableOrig origine = new InventaireComptableOrigBean(inventaireComptable, montant, pourcentage);
		controleur.updateInventaireComptableOrigine(origine);

		assertEquals(montantFinal, origine.icorMontant());
		assertEquals(pourcentageFinal, origine.icorPourcentage());
	}
	
	private void testViaPourcentage(double montantAcquisition, double montantOrvs, String montant, String pourcentage, BigDecimal montantFinal, BigDecimal pourcentageFinal) {
		IInventaireComptable inventaireComptable = new MockInventaireComptable(bigD(montantAcquisition), bigD(montantOrvs));
		IInventaireComptableOrig origine = new InventaireComptableOrigBean(inventaireComptable, montant, pourcentage);
		controleur.updateInventaireComptableOrigine(origine);
		
		assertEquals(montantFinal, origine.icorMontant());
		assertEquals(pourcentageFinal, origine.icorPourcentage());
	}

	/**
	 * @return
	 */
	private BigDecimal bigD(double val) {
		return BigDecimal.valueOf(val).setScale(2);
	}

}
