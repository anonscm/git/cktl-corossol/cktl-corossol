package org.cocktail.corossol.client;


import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

public class UtilCtrlTest {
	
	@Test
	public void stringToBigDecimalTest(){
		assertEquals(UtilCtrl.stringToBigDecimal("10.9"), new BigDecimal ("10.9"));
		assertEquals(UtilCtrl.stringToBigDecimal("10,9"), new BigDecimal ("10.9"));
		assertEquals(UtilCtrl.stringToBigDecimal("1 000,9"), new BigDecimal ("1000.9"));
		assertEquals(UtilCtrl.stringToBigDecimal("10 000 000,9"), new BigDecimal ("10000000.9"));
	}

}
