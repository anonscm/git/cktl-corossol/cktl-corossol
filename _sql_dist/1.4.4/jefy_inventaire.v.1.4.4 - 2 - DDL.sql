
-- a executer a partir du user JEFY_INVENTAIRE

ALTER TABLE JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE MODIFY(CLIC_NUM_COMPLET VARCHAR2(200));

ALTER TABLE JEFY_INVENTAIRE.ORIGINE_FINANCEMENT ADD (EXE_ORDRE NUMBER default 2007);
ALTER TABLE JEFY_INVENTAIRE.ORIGINE_FINANCEMENT MODIFY(exe_ordre NOT NULL);
ALTER TABLE JEFY_INVENTAIRE.ORIGINE_FINANCEMENT ADD (PCO_NUM VARCHAR2(20));

ALTER TABLE JEFY_INVENTAIRE.INVENTAIRE_NON_BUDGETAIRE ADD (inb_date_acquisition  DATE);

drop sequence origine_financement_seq;
create SEQUENCE origine_financement_seq START WITH 100 increment by 1;

create table jefy_inventaire.inventaire_comptable_orig (
  icor_id          number not null,
  invc_id          number not null,
  orgf_id          number not null,
  icor_pourcentage number(12,2) not null,
  tit_id           number)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_inventaire.inventaire_comptable_orig is 'table de repartition des origines de financement d''un inventaire comptable';
COMMENT ON COLUMN jefy_inventaire.inventaire_comptable_orig.icor_id is 'Cle';
COMMENT ON COLUMN jefy_inventaire.inventaire_comptable_orig.invc_id is 'Cle etrangere table jefy_inventaire.inventaire_comptable';
COMMENT ON COLUMN jefy_inventaire.inventaire_comptable_orig.orgf_id is 'Cle etrangere table jefy_inventaire.origine_financement';
COMMENT ON COLUMN jefy_inventaire.inventaire_comptable_orig.icor_pourcentage is 'Pourcentage de la repartition';
COMMENT ON COLUMN jefy_inventaire.inventaire_comptable_orig.tit_id is 'Cle etrangere table maracuja.titre';

CREATE UNIQUE INDEX jefy_inventaire.PK_inv_compt_orig ON jefy_inventaire.inventaire_comptable_orig (icor_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_inventaire.inventaire_comptable_orig ADD (CONSTRAINT PK_inv_compt_orig PRIMARY KEY (icor_id));

ALTER TABLE jefy_inventaire.inventaire_comptable_orig ADD ( CONSTRAINT FK_inv_compt_o_tit_id FOREIGN KEY (tit_id) 
    REFERENCES maracuja.titre(tit_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_inventaire.inventaire_comptable_orig ADD ( CONSTRAINT FK_inv_compt_o_orgf_id FOREIGN KEY (orgf_id) 
    REFERENCES jefy_inventaire.origine_financement(orgf_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_inventaire.inventaire_comptable_orig ADD ( CONSTRAINT FK_inv_compt_o_invc_id FOREIGN KEY (invc_id) 
    REFERENCES jefy_inventaire.inventaire_comptable(invc_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_inventaire.inventaire_comptable_orig_seq start with 1 nocycle nocache;


CREATE OR REPLACE procedure JEFY_INVENTAIRE.prepare_exercice(nouvelExercice jefy_admin.exercice.exe_ordre%type)
is
  my_nb           integer;
  ancienExercice  jefy_admin.exercice.exe_ordre%type;
begin
     -- on reconduit les parametres.
     select max(exe_ordre) into ancienExercice from parametre;
     insert into parametre select nouvelExercice, par_key, par_value, par_description
          from jefy_inventaire.parametre where exe_ordre=ancienExercice;
     
     -- on reconduit les origines de financement.
     select max(exe_ordre) into ancienExercice from origine_financement;
     select count(*) into my_nb from origine_financement where exe_ordre=nouvelExercice;
     if my_nb=0 then
        insert into origine_financement select origine_financement_seq.nextval, orgf_libelle, nouvelExercice, pco_num
             from jefy_inventaire.origine_financement where exe_ordre=ancienExercice;
     end if;
end;
/


CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE.API_COROSSOL AS

PROCEDURE creer_lien_inv_dep (
   dpcoid  inventaire_comptable.dpco_id%type, 
   invcid  inventaire_comptable.invc_id%type,
   montant inventaire_comptable.invc_montant_acquisition%type
) IS 
  depidrev integer;
  orv number(12,2);
  pco      maracuja.plan_comptable.PCO_NUM%type;
  my_date  JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.invc_date_acquisition%type;
  clicid   JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.clic_id%type;
  exeordre JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.exe_ordre%type;
BEGIN
  orv := 0;
  my_date:=null;
  
  -- ORV
  if montant<0 then
    -- creation de l'enregistrment dans INVENTAIRE_COMPTABLE_ORV
    insert into INVENTAIRE_COMPTABLE_ORV values (invcid,dpcoid,INVENTAIRE_COMPTABLE_ORV_seq.nextval,montant);
  else

    if (dpcoid != -1000) then -- cas des integrations HB
       -- recup du compte d'imputation de la liquidation
       select pc.pco_num, dpp_date_service_fait into pco, my_date 
          from jefy_depense.depense_papier p, jefy_depense.depense_budget b, jefy_depense.depense_ctrl_planco pc
          where b.dep_id=pc.dep_id and b.dpp_id=p.dpp_id and pc.dpco_id=dpcoid;
       UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET invc_date_acquisition=my_date WHERE invc_id=invcid;
    end if;

    -- recup du numero comptable
    select clic_id,EXE_ORDRE into clicid,exeordre from JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE where invc_id = invcid;

    if (dpcoid != -1000) then
      -- Correction CH  maj du compte d'amortissement et de la dur?e 
      update JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE set pco_num = pco,
         PCOA_ID = substr(integration.get_pcoaidduree(exeordre, pco),1, instr(integration.get_pcoaidduree(exeordre, pco),'$')-1),
         CLIC_DUREE_AMORT = substr(integration.get_pcoaidduree(exeordre, pco),instr(integration.get_pcoaidduree(exeordre, pco),'$')+1)
        where clic_id = clicid;
    end if;

    -- maj inventaire_comptable : creation su liens
    UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id = dpcoid, invc_montant_acquisition=montant + nvl(orv,0),
      invc_montant_residuel = montant+ nvl(orv,0) WHERE invc_id = invcid;
  end if;

  -- CREATION DE L AMORTISSEMENT
  amortissement(invcid);
END;
 
PROCEDURE supprimer_lien_inv_dep (dpcoid inventaire_comptable.dpco_id%type) IS 
   cursor c1 is select invc_id from inventaire_comptable where dpco_id=dpcoid;
   invcid jefy_inventaire.inventaire_comptable.invc_id%type;
BEGIN
  -- on traite les ORVS 
  delete from jefy_inventaire.INVENTAIRE_COMPTABLE_ORV where dpco_id=dpcoid;
 
  open c1;
  loop
    fetch c1 into invcid;
    exit when c1%notfound;
    -- maj  inventaire_comptable : casse le lien
    UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id=NULL WHERE dpco_id=dpcoid and invc_id=invcid;
    -- SUPPRESSION DE L AMORTISSEMENT
    amort_vider(invcid);
  end loop;
  close c1;
END;

PROCEDURE AMORTISSEMENT_CLE (
   a01clicid cle_inventaire_comptable.clic_id%type,
   a02annule integer
) is
  cursor c1 is select invc_id from inventaire_comptable where clic_id=a01clicid;
  invcid jefy_inventaire.inventaire_comptable.invc_id%type;
begin

  if a02annule = 1 then
    open c1;
    loop
      fetch c1 into invcid ;
      exit when c1%notfound;
      API_COROSSOL.amort_vider(invcid);
    end loop;
    close c1;
  else
    open c1;
    loop
      fetch c1 into invcid ;
      exit when c1%notfound;
      amort_vider(invcid);
      amortissement(invcid);
    end loop;
    close c1;
  end if;
end;

PROCEDURE amortissement(
   invcid inventaire_comptable.invc_id%type
) IS
  cpt INTEGER;
  tmpinvComptable jefy_inventaire.INVENTAIRE_COMPTABLE%ROWTYPE;
  tmpCleComptable jefy_inventaire.CLE_INVENTAIRE_COMPTABLE%ROWTYPE;
  tmpplancoamort  maracuja.plan_comptable_amo%ROWTYPE;
  montant_orvs    jefy_inventaire.INVENTAIRE_COMPTABLE_ORV.INVO_MONTANT_ORV%type;
  my_dgco_coef    degressif_coef.dgco_coef%type;
  my_date_calcul  jefy_depense.depense_papier.dpp_date_service_fait%type;
  my_dgco_id      degressif_coef.dgco_id%type;
BEGIN
  -- recup des infos du numero comptable
  SELECT COUNT(*) INTO cpt FROM jefy_inventaire.INVENTAIRE_COMPTABLE WHERE invc_id = invcid ;
  IF cpt = 0 THEN
    RAISE_APPLICATION_ERROR (-20001,'inventaire comptable introuvable, invc_id='||invcid);
  END IF;
  
  SELECT * INTO tmpinvComptable FROM jefy_inventaire.INVENTAIRE_COMPTABLE WHERE invc_id = invcid ;
  SELECT nvl(sum(INVO_MONTANT_ORV),0)  INTO montant_orvs FROM jefy_inventaire.INVENTAIRE_COMPTABLE_ORV WHERE invc_id = invcid ;
  SELECT * INTO tmpCleComptable FROM jefy_inventaire.CLE_INVENTAIRE_COMPTABLE WHERE clic_id = tmpinvComptable.clic_id ;

  SELECT COUNT(*) INTO cpt FROM maracuja.plan_comptable_amo WHERE pcoa_id = tmpCleComptable.pcoa_id;
  IF cpt = 0 THEN
    raise_application_error (-20001,'compte d amortissement inexistant verifier votre parametrage dans maracuja '||tmpclecomptable.pcoa_id);
  end if;
  
  SELECT * INTO tmpplancoamort FROM maracuja.plan_comptable_amo WHERE pcoa_id = tmpCleComptable.pcoa_id;

  my_date_calcul:=tmpinvcomptable.invc_date_acquisition;

  if tmpinvComptable.dpco_id is not null then
    if my_date_calcul is null then 
       select count(*) into cpt from jefy_depense.depense_ctrl_planco where dpco_id=tmpinvcomptable.dpco_id;
       if cpt>0 then
          select dpp_date_service_fait into my_date_calcul 
             from jefy_depense.depense_papier p, jefy_depense.depense_budget b, jefy_depense.depense_ctrl_planco pc
             where b.dep_id=pc.dep_id and pc.dpco_id=tmpinvcomptable.dpco_id and b.dpp_id=p.dpp_id;
       end if;
    end if;
        
    my_dgco_coef:=null;
    my_dgco_id:=tmpinvComptable.dgco_id;
    if my_dgco_id is not null then
      select max(dgco_id) into my_dgco_id from degressif_coef where 
        dgrf_id in (select dgrf_id from degressif where dgrf_date_debut<=my_date_calcul and (dgrf_date_fin>=my_date_calcul or dgrf_date_fin is null))
        and dgco_duree_min<=tmpCleComptable.clic_duree_amort and dgco_duree_max>=tmpCleComptable.clic_duree_amort;
    end if;
    if my_dgco_id is not null then 
      select dgco_coef into my_dgco_coef from degressif_coef where dgco_id=my_dgco_id;
    end if;
  
    IF tmpCleComptable.clic_type_amort = 'Lineaire' THEN
      Api_Corossol.amort_lineaire(invcid,tmpinvComptable.invc_montant_acquisition+montant_orvs,tmpCleComptable.clic_duree_amort,
         tmpCleComptable.pco_num,tmpplancoamort.pcoa_num,tmpinvComptable.exe_ordre, tmpCleComptable.clic_pro_rata,my_date_calcul);
    ELSE
      Api_Corossol.amort_degressif(invcid,tmpinvComptable.invc_montant_acquisition+montant_orvs,tmpCleComptable.clic_duree_amort,
       tmpCleComptable.pco_num,tmpplancoamort.pcoa_num,tmpinvComptable.exe_ordre, tmpCleComptable.clic_pro_rata,my_dgco_coef, my_date_calcul);
    END IF;
  end if;
END;

PROCEDURE amort_lineaire(
   invcid   inventaire_comptable.invc_id%type,
   montant  inventaire_comptable.invc_montant_acquisition%type,
   duree    cle_inventaire_comptable.clic_duree_amort%type,
   pco      cle_inventaire_comptable.pco_num%type,
   pcoamort jefy_inventaire.amortissement.pco_num%type,
   exeordre inventaire_comptable.exe_ordre%type,
   prorata  cle_inventaire_comptable.clic_pro_rata%type,
   la_date  inventaire_comptable.invc_date_acquisition%type
) IS 
  cle         jefy_inventaire.amortissement.amo_id%type;
  annuite     jefy_inventaire.amortissement.amo_annuite%type;
  annuite_ref jefy_inventaire.amortissement.amo_annuite%type;
  cumul       jefy_inventaire.amortissement.amo_cumul%type;
  residuel    jefy_inventaire.amortissement.amo_residuel%type;
  iter        integer;
  my_nb       integer;
  my_nb_decimales  NUMBER;

  cursor c1 is select s.* from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid and s.invs_vnc>0;
  my_inventaire_sortie jefy_inventaire.inventaire_sortie%rowtype;
  exe_ordre_sortie     jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_amor       jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_debut      jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_fin        jefy_inventaire.amortissement.exe_ordre%type;
  my_amortissement     jefy_inventaire.amortissement.amo_annuite%type;
  my_residuel          jefy_inventaire.amortissement.amo_residuel%type;
  my_cumul             jefy_inventaire.amortissement.amo_cumul%type;
  my_acquisition       jefy_inventaire.amortissement.amo_acquisition%type;
BEGIN

  if prorata<>0 and la_date is null then
     raise_application_error (-20001,'Pour un amortissement prorata temporis il faut indiquer une date de debut d''amortissement.');  
  end if;

  amort_vider(invcid);

  my_nb_decimales:=get_nb_decimales(exeordre);

  iter :=0;
  cumul:=0;
  residuel:=montant;
  annuite_ref:=montant/duree;

  LOOP
    IF montant=0 or duree=0 or residuel=0 THEN EXIT; END IF;
    
    iter := iter +1;

    -- si on a laisse un amortissement c'est qu'on ne veut pas le recalculer pour cette annee la        
    select count(*) into my_nb from amortissement where invc_id=invcid and exe_ordre=exeordre+iter;
    if my_nb=0 then
       -- calcul des valeurs
       annuite:=annuite_ref;
       
       if prorata<>0 and iter=1 then
       dbms_output.put_line('taux_premiere_annee(exeordre, la_date):'||taux_premiere_annee(exeordre, la_date));
          annuite:=annuite*taux_premiere_annee(exeordre, la_date);
       end if;

       annuite:=round(annuite, my_nb_decimales);

       if annuite>residuel or (prorata=0 and iter=duree) OR (prorata<>0 and iter=duree+1) then annuite:=residuel; end if;
       if annuite<0 then annuite:=0; end if;

       cumul:=cumul+annuite;
       residuel:=montant-cumul;
    
       -- insertion
       select amortissement_seq.nextval into cle from dual;
       if prorata=0 then
       insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
          values (montant,annuite,cumul ,sysdate,cle,residuel,exeordre+iter,invcid,pcoamort );
       else
         insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
           values (montant,annuite,cumul ,sysdate,cle,residuel,exeordre+iter-1,invcid,pcoamort );
       end if;
    else
       select amo_annuite into annuite from amortissement where invc_id=invcid and exe_ordre=exeordre+iter; 
       cumul:=cumul+annuite;
       residuel:=montant-cumul;    
    end if;          

    -- test (attention : pour l'amortissement lineaire avec prorata temporis, on amortie sur duree+1 !!!!!!!
    
    IF ((prorata=0 or to_char(la_date,'dd/mm')='01/01') and iter=duree) OR (prorata<>0 and iter=duree+1) THEN
      UPDATE AMORTISSEMENT SET AMO_RESIDUEL=0, AMO_CUMUL=AMO_CUMUL+AMO_RESIDUEL, AMO_ANNUITE=AMO_ANNUITE+AMO_RESIDUEL WHERE AMO_ID=cle;
      EXIT;
    END IF;
  END LOOP;
  
  -- gerer sorties inventaire
  select count(*) into my_nb from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid;
  if my_nb>0 then
  
    exe_ordre_fin:=exeordre+duree;
    select min(exe_ordre) into exe_ordre_debut from amortissement where invc_id=invcid;

    open c1;
    loop
      fetch c1 into my_inventaire_sortie;
      exit when c1%notfound;
      
      exe_ordre_sortie:=to_number(to_char(my_inventaire_sortie.invs_date,'yyyy'));
      if exe_ordre_sortie<exe_ordre_debut then
         exe_ordre_sortie:=exe_ordre_debut;
      end if;
      exe_ordre_amor:=exe_ordre_sortie;
      
      LOOP
        IF exe_ordre_amor>exe_ordre_fin THEN EXIT; END IF;

        -- exercice de la sortie
        if exe_ordre_amor=exe_ordre_sortie then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           my_amortissement:=my_inventaire_sortie.invs_vnc;
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
           end if;
        end if;
      
        -- annees suivantes
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor<exe_ordre_fin then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
                        
           my_amortissement:=round(my_inventaire_sortie.invs_vnc/(exe_ordre_fin-exe_ordre_sortie), my_nb_decimales);        
                     
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
              select amo_annuite into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
              my_amortissement:=my_residuel-my_amortissement;
           end if;
           
           my_amortissement:=-my_amortissement;
        end if;

        -- derniere annee
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor=exe_ordre_fin then
           select nvl(sum(amo_annuite),0) into my_cumul from amortissement where exe_ordre<exe_ordre_fin and invc_id=invcid;
           select amo_acquisition, amo_annuite into my_acquisition, my_residuel from amortissement where exe_ordre=exe_ordre_fin and invc_id=invcid;
           my_amortissement:=my_acquisition-my_cumul-my_residuel;
        end if;

        if exe_ordre_amor>exe_ordre_sortie then
           select amo_cumul, amo_residuel into my_cumul, my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
           
           update amortissement set amo_cumul=my_cumul+amo_annuite, amo_residuel=my_residuel-amo_annuite 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;

        if my_amortissement<>0 then
          update amortissement
            set amo_annuite=amo_annuite+my_amortissement, amo_cumul=amo_cumul+my_amortissement, amo_residuel=amo_residuel-my_amortissement, amo_date=sysdate 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;
        
        exe_ordre_amor:=exe_ordre_amor+1;
      end loop;
    
    end loop;
    close c1;
  end if;
  
END;
 
PROCEDURE amort_degressif(
   invcid      inventaire_comptable.invc_id%type,
   montant     inventaire_comptable.invc_montant_acquisition%type,
   duree       cle_inventaire_comptable.clic_duree_amort%type,
   pco         cle_inventaire_comptable.pco_num%type,
   pcoamort    jefy_inventaire.amortissement.pco_num%type,
   exeordre    inventaire_comptable.exe_ordre%type,
   prorata     cle_inventaire_comptable.clic_pro_rata%type,
   coef_fiscal degressif_coef.dgco_coef%type,
   la_date     inventaire_comptable.invc_date_acquisition%type
) IS
  cle         jefy_inventaire.amortissement.amo_id%type;
  annuite     jefy_inventaire.amortissement.amo_annuite%type;
  cumul       jefy_inventaire.amortissement.amo_cumul%type;
  residuel    jefy_inventaire.amortissement.amo_residuel%type;
  iter        integer;
  my_nb       integer;
  my_nb_decimales  NUMBER;
  
  cursor c1 is select s.* from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid and s.invs_vnc>0;
  my_inventaire_sortie jefy_inventaire.inventaire_sortie%rowtype;
  exe_ordre_sortie     jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_amor       jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_debut      jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_fin        jefy_inventaire.amortissement.exe_ordre%type;
  my_amortissement     jefy_inventaire.amortissement.amo_annuite%type;
  my_residuel          jefy_inventaire.amortissement.amo_residuel%type;
  my_cumul             jefy_inventaire.amortissement.amo_cumul%type;
  my_acquisition       jefy_inventaire.amortissement.amo_acquisition%type;
BEGIN
  if prorata<>0 and la_date is null then
     raise_application_error (-20001,'Pour un amortissement prorata temporis il faut indiquer une date de debut d''amortissement.');  
  end if;

  amort_vider(invcid);
  
  my_nb_decimales:=get_nb_decimales(exeordre);
  
  iter :=0;
  cumul:=0;
  residuel:=montant;
  
  loop
    if montant=0 or duree=0 or residuel=0 then exit; end if;
    
    iter:=iter+1;
    
    -- calcul des valeurs
    dbms_output.put_line(duree||'+1-'||iter);
    if duree+1-iter=0 then
       annuite:=residuel;
    else
       if coef_fiscal*100.0/duree > 100/(duree+1-iter) then
          annuite:=residuel*(coef_fiscal/duree);
       else
          annuite:=residuel/(duree+1-iter);
       end if;
    end if;
 
    if prorata<>0 and iter=1 then
       annuite:=annuite*taux_premiere_annee(exeordre, la_date);
    end if;
    
    annuite:=round(annuite, my_nb_decimales);
    
    if annuite>residuel or duree=iter then annuite:=residuel; end if;
    if annuite<0 then annuite:=0; end if;
    
    select count(*) into my_nb from amortissement where invc_id=invcid and exe_ordre=exeordre+iter;
    if my_nb=0 then
       cumul:=cumul+annuite;
       residuel:=montant-cumul;
    
       -- insertion
       select amortissement_seq.nextval into cle from dual;
       if prorata=0 then
          insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
              values (montant, annuite, cumul, sysdate, cle, residuel, exeordre+iter, invcid, pcoamort);
       else
          insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
              values (montant, annuite, cumul, sysdate, cle, residuel, exeordre+iter-1, invcid, pcoamort);
       end if;
    else
       select amo_annuite into annuite from amortissement where invc_id=invcid and exe_ordre=exeordre+iter; 
       cumul:=cumul+annuite;
       residuel:=montant-cumul;    
    end if;
  end loop;
  
  -- gerer sorties inventaire
  select count(*) into my_nb from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid;
  if my_nb>0 then
  
    exe_ordre_fin:=exeordre+duree;
    select min(exe_ordre) into exe_ordre_debut from amortissement where invc_id=invcid;

    open c1;
    loop
      fetch c1 into my_inventaire_sortie;
      exit when c1%notfound;
      
      exe_ordre_sortie:=to_number(to_char(my_inventaire_sortie.invs_date,'yyyy'));
      if exe_ordre_sortie<exe_ordre_debut then
         exe_ordre_sortie:=exe_ordre_debut;
      end if;
      exe_ordre_amor:=exe_ordre_sortie;
      
      LOOP
        IF exe_ordre_amor>exe_ordre_fin THEN EXIT; END IF;

        -- exercice de la sortie
        if exe_ordre_amor=exe_ordre_sortie then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           my_amortissement:=my_inventaire_sortie.invs_vnc;
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
           end if;
        end if;
      
        -- annees suivantes
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor<exe_ordre_fin then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;

           iter:=exe_ordre_amor-exeordre;
              
           if coef_fiscal*100.0/duree > 100/(duree+1-iter) then
              my_amortissement:=residuel*(coef_fiscal/duree);
           else
              my_amortissement:=residuel/(duree+1-iter);
           end if;
                        
           my_amortissement:=round(my_amortissement, my_nb_decimales);        
                     
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
              select amo_annuite into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
              my_amortissement:=my_residuel-my_amortissement;
           end if;
           
           my_amortissement:=-my_amortissement;
        end if;

        -- derniere annee
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor=exe_ordre_fin then
           select nvl(sum(amo_annuite),0) into my_cumul from amortissement where exe_ordre<exe_ordre_fin and invc_id=invcid;
           select amo_acquisition, amo_annuite into my_acquisition, my_residuel from amortissement where exe_ordre=exe_ordre_fin and invc_id=invcid;
           my_amortissement:=my_acquisition-my_cumul-my_residuel;
        end if;

        if exe_ordre_amor>exe_ordre_sortie then
           select amo_cumul, amo_residuel into my_cumul, my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
           
           update amortissement set amo_cumul=my_cumul+amo_annuite, amo_residuel=my_residuel-amo_annuite 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;

        if my_amortissement<>0 then
          update amortissement
            set amo_annuite=amo_annuite+my_amortissement, amo_cumul=amo_cumul+my_amortissement, amo_residuel=amo_residuel-my_amortissement, amo_date=sysdate 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;
        
        exe_ordre_amor:=exe_ordre_amor+1;
      end loop;
    
    end loop;
    close c1;
  end if;
  
end;
 
PROCEDURE amort_vider(
   invcid inventaire_comptable.invc_id%type
) IS 
  my_nb                   integer;
  my_inventaire_comptable inventaire_comptable%rowtype;
BEGIN
  select * into my_inventaire_comptable from inventaire_comptable where invc_id=invcid;
  select count(*) into my_nb from parametre where par_key='RECALCUL_AMORTISSEMENTS_ANTERIEURS' and exe_ordre>my_inventaire_comptable.exe_ordre and par_value='NON';
  if my_nb>0 then
     DELETE FROM AMORTISSEMENT WHERE invc_id=invcid and exe_ordre>=grhum.en_nombre(to_char(sysdate,'yyyy'));
  else
     DELETE FROM AMORTISSEMENT WHERE invc_id=invcid;
  end if;

END;

function taux_premiere_annee(
   a_exe_ordre inventaire_comptable.exe_ordre%type,
   a_date      inventaire_comptable.invc_date_acquisition%type
) return number is
   annee    inventaire_comptable.exe_ordre%type;
   nb_jours integer;
begin
   -- Test date 
   if a_date is null then return 1.0; end if;
   
   annee:=to_number(to_char(a_date, 'yyyy'));
   if a_exe_ordre<>annee then return 1.0; end if;
   
   -- calcul
   nb_jours:=360 - (to_number(to_char(a_date,'mm'))-1)*30 - (to_number(to_char(a_date,'dd'))-1);
   if nb_jours>360 then nb_jours:=360.0; end if;
   if nb_jours<0 then nb_jours:=0.0; end if;

   return nb_jours/360.0;
end;

FUNCTION get_nb_decimales(a_exe_ordre inventaire_comptable.exe_ordre%type)
     return jefy_admin.devise.dev_nb_decimales%type
   is
     my_nb        integer;
     my_par_value jefy_admin.parametre.par_value%type;
     my_dev       jefy_admin.devise.dev_nb_decimales%type;
   begin
        SELECT COUNT(*) INTO my_nb FROM jefy_admin.PARAMETRE WHERE exe_ordre=a_exe_ordre AND par_key='DEVISE_DEV_CODE';
        IF my_nb=1 THEN
           SELECT par_value INTO my_par_value FROM jefy_admin.PARAMETRE
              WHERE exe_ordre=a_exe_ordre AND par_key='DEVISE_DEV_CODE';

           SELECT COUNT(*) INTO my_nb FROM jefy_admin.devise WHERE dev_code=my_par_value;
           IF my_nb=1 THEN
              SELECT dev_nb_decimales INTO my_dev FROM jefy_admin.devise WHERE dev_code=my_par_value;

              IF my_dev IS NOT NULL THEN
                 return my_dev;
              END IF;
           END IF;
        END IF;

        return 2;
   end;
END;
/


CREATE OR REPLACE PACKAGE JEFY_INVENTAIRE.API_regularisation AS

/*
 * Copyright Cocktail, 2001-2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 
procedure regrouper(
      a_clic_id_origine         inventaire_comptable.clic_id%type,
      a_clic_id_destination     inventaire_comptable.clic_id%type
);

procedure eclater(
      a_clic_id_origine         inventaire_comptable.clic_id%type,
      a_chaine_inventaires      varchar2,
      a_clic_id_destination out inventaire_comptable.clic_id%type
);

END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE.API_regularisation AS

procedure regrouper (
      a_clic_id_origine         inventaire_comptable.clic_id%type,
      a_clic_id_destination     inventaire_comptable.clic_id%type
) is
   my_nb                        integer;
   my_cle_inv_compt_origine     cle_inventaire_comptable%rowtype;
   my_cle_inv_compt_destination cle_inventaire_comptable%rowtype;
begin 
    select count(*) into my_nb from inventaire_comptable where clic_id=a_clic_id_origine;
    if my_nb=0 then
        raise_application_error(-20001,'L''inventaire comptable d''origine n''existe pas (clic_id='||a_clic_id_origine||')');
    end if;
    select count(*) into my_nb from inventaire_comptable where clic_id=a_clic_id_destination;
    if my_nb=0 then
        raise_application_error(-20001,'L''inventaire comptable de destination n''existe pas (clic_id='||a_clic_id_destination||')');
    end if;

    if a_clic_id_origine<>a_clic_id_destination then
       select * into my_cle_inv_compt_origine from cle_inventaire_comptable where clic_id=a_clic_id_origine;
       select * into my_cle_inv_compt_destination from cle_inventaire_comptable where clic_id=a_clic_id_destination;
    
       if my_cle_inv_compt_origine.exe_ordre<>my_cle_inv_compt_destination.exe_ordre then
           raise_application_error(-20001,'Les inventaires comptables doivent avoir le meme exercice');
       end if;
       if my_cle_inv_compt_origine.clic_comp<>my_cle_inv_compt_destination.clic_comp then
           raise_application_error(-20001,'Les inventaires comptables doivent avoir la meme UB');
       end if;
       if my_cle_inv_compt_origine.pco_num<>my_cle_inv_compt_destination.pco_num then
           raise_application_error(-20001,'Les inventaires comptables doivent avoir la meme imputation');
       end if;
    
       update inventaire_comptable set clic_id=a_clic_id_destination where clic_id=a_clic_id_origine;
       delete from cle_inventaire_comptable where clic_id=a_clic_id_origine;
    end if;
end;

procedure eclater (
      a_clic_id_origine         inventaire_comptable.clic_id%type,
      a_chaine_inventaires      varchar2,
      a_clic_id_destination out inventaire_comptable.clic_id%type
) is
   my_nb                        integer;
   my_inv_id                    inventaire.inv_id%type;
   my_clic_id                   cle_inventaire_comptable.clic_id%type;
   my_invc_id                   inventaire_comptable.clic_id%type;
   my_inventaire                inventaire%rowtype;
   my_inventaire_comptable      inventaire_comptable%rowtype;
   my_inv_montant_acquisition   inventaire.inv_montant_acquisition%type;
   my_cle_inventaire_comptable  cle_inventaire_comptable%rowtype;
   my_clic_numero               cle_inventaire_comptable.clic_numero%type;
   my_chaine				    VARCHAR2(30000);
begin
    select count(*) into my_nb from inventaire_comptable where clic_id=a_clic_id_origine;
    if my_nb=0 then
        raise_application_error(-20001,'L''inventaire comptable d''origine n''existe pas (clic_id='||a_clic_id_origine||')');
    end if;

    my_clic_id:=null;
    my_chaine:=a_chaine_inventaires;
	LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_inv_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            select * into my_inventaire from inventaire where inv_id=my_inv_id;
            select * into my_inventaire_comptable from inventaire_comptable where invc_id=my_inventaire.invc_id;
            select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=my_inventaire_comptable.clic_id;
            
          
            -- restriction qu'a certains cas
              -- pas de facture
              -- liquidation = montant acquisition
              -- ...
            --
            
            if my_clic_id is null then
               my_clic_numero:=inventaire_numero.get_numero_comptable(my_cle_inventaire_comptable.exe_ordre, my_cle_inventaire_comptable.clic_comp, 
                   my_cle_inventaire_comptable.clic_cr);
               select cle_inventaire_comptable_seq.nextval into my_clic_id from dual;
               insert into cle_inventaire_comptable values (my_cle_inventaire_comptable.clic_comp, my_cle_inventaire_comptable.clic_cr, 
                   my_cle_inventaire_comptable.clic_duree_amort, my_cle_inventaire_comptable.clic_etat, my_clic_id, 
                   my_cle_inventaire_comptable.clic_nb_etiquette, my_clic_numero, my_cle_inventaire_comptable.clic_pro_rata, 
                   my_cle_inventaire_comptable.clic_type_amort, my_cle_inventaire_comptable.exe_ordre, my_cle_inventaire_comptable.pco_num, 
                   my_cle_inventaire_comptable.pcoa_id, 
                   inventaire_numero.get_numero_complet(my_cle_inventaire_comptable.exe_ordre, my_cle_inventaire_comptable.clic_comp, 
                       my_cle_inventaire_comptable.clic_cr, my_clic_numero));
            end if;
            
            select count(*) into my_nb from inventaire where invc_id=my_inventaire.invc_id and inv_id<>my_inv_id;
            if my_nb=0 then
               update inventaire_comptable set clic_id=my_clic_id where invc_id=my_inventaire.invc_id;
            else
                -- si l'inventaire comptable correspond � plusieurs biens, et qu'il a un ORV ... erreur, 
                -- car on ne sait pas � quel bien il correspond 
                select count(*) into my_nb from inventaire_comptable_orv where invc_id=my_inventaire.invc_id;
                if my_nb>0 then
		           raise_application_error(-20001, 'On ne peut pas eclater un inventaire sur lequel il y a eu un reversement');
                end if;

                select nvl(sum(inv_montant_acquisition),0) into my_inv_montant_acquisition from inventaire where invc_id=my_inventaire_comptable.invc_id;
                if my_inv_montant_acquisition<>my_inventaire_comptable.invc_montant_acquisition then
		           raise_application_error(-20001, 'la somme des montants d''acquisition des inventaires physiques n''est pas egale a celui de l''inventaire comptable');
                end if;
                     
                my_invc_id:=null;
                api_comptable.enregistrer_comptable(my_clic_id, my_inventaire_comptable.dpco_id, my_inventaire_comptable.exe_ordre,
                   my_inventaire_comptable.invc_date_acquisition, my_inventaire_comptable.invc_etat, my_invc_id, my_inventaire.inv_montant_acquisition,
                   my_inventaire.inv_montant_acquisition, my_inventaire_comptable.orgf_id, my_inventaire_comptable.utl_ordre,
                   my_inventaire_comptable.invc_date_sortie, my_inventaire_comptable.dgco_id, my_inv_id||'$$', my_cle_inventaire_comptable.clic_comp,
                   my_cle_inventaire_comptable.clic_cr, my_cle_inventaire_comptable.clic_duree_amort, my_cle_inventaire_comptable.clic_etat,
                   my_cle_inventaire_comptable.clic_nb_etiquette, my_cle_inventaire_comptable.clic_pro_rata, my_cle_inventaire_comptable.clic_type_amort,
                   my_cle_inventaire_comptable.pco_num, my_cle_inventaire_comptable.pcoa_id);
                api_corossol.amortissement(my_invc_id);

                -- modification montants inventaire_comptable et amortissements associ�s ...
                update inventaire set invc_id=my_invc_id where inv_id=my_inv_id;
                update inventaire_comptable set invc_montant_acquisition=invc_montant_acquisition-my_inventaire.inv_montant_acquisition,
                  invc_montant_residuel=invc_montant_residuel-my_inventaire.inv_montant_acquisition where invc_id=my_inventaire_comptable.invc_id;
                api_corossol.amortissement(my_inventaire_comptable.invc_id);

            end if;
	END LOOP;
    
    a_clic_id_destination:=my_clic_id;
end;

END;
/


CREATE OR REPLACE PACKAGE JEFY_INVENTAIRE."INTEGRATION_HB" AS

procedure enregistrer_hb (
   a_INB_ID        in out NUMBER,
   a_UTL_ORDRE            NUMBER,
   a_EXE_ORDRE            NUMBER,
   a_ORG_ID               NUMBER,
   a_PCO_NUM              VARCHAR2,
   a_INB_DUREE            NUMBER,
   a_INB_TYPE_AMORT       VARCHAR2,
   a_INB_MONTANT          NUMBER,
   a_INB_MONTANT_RESIDUEL NUMBER,
   a_ORIG_ID              NUMBER,
   a_INB_NUMERO_SERIE     VARCHAR2,
   a_INB_INFORMATIONS     VARCHAR2,
   a_INB_FOURNISSEUR      VARCHAR2,
   a_INB_FACTURE          VARCHAR2,
   a_INVC_ID              NUMBER,
   a_INB_DATE_ACQUISITION date);

procedure integrer_hb (inbid integer) ;
procedure modifier_hb (inbid integer) ;
procedure supprimer_hb (inbid integer) ;

END INTEGRATION_HB;
/


CREATE OR REPLACE PACKAGE BODY "INTEGRATION_HB" AS

procedure enregistrer_hb (
   a_INB_ID        in out NUMBER,
   a_UTL_ORDRE            NUMBER,
   a_EXE_ORDRE            NUMBER,
   a_ORG_ID               NUMBER,
   a_PCO_NUM              VARCHAR2,
   a_INB_DUREE            NUMBER,
   a_INB_TYPE_AMORT       VARCHAR2,
   a_INB_MONTANT          NUMBER,
   a_INB_MONTANT_RESIDUEL NUMBER,
   a_ORIG_ID              NUMBER,
   a_INB_NUMERO_SERIE     VARCHAR2,
   a_INB_INFORMATIONS     VARCHAR2,
   a_INB_FOURNISSEUR      VARCHAR2,
   a_INB_FACTURE          VARCHAR2,
   a_INVC_ID              NUMBER,
   a_INB_DATE_ACQUISITION date
) is
   my_cpt integer;
begin
   if a_inb_id is null then
     select inventaire_non_budgetaire_seq.nextval into a_inb_id from dual;
   end if;

   select count(*) into my_cpt from inventaire_non_budgetaire where inb_id=a_inb_id;
   if my_cpt=0 then
      insert into inventaire_non_budgetaire values (a_inb_id, a_UTL_ORDRE, a_EXE_ORDRE, a_ORG_ID, a_PCO_NUM, a_INB_DUREE, a_INB_TYPE_AMORT,
          a_INB_MONTANT, a_INB_MONTANT_RESIDUEL, a_ORIG_ID, a_INB_NUMERO_SERIE, a_INB_INFORMATIONS, a_INB_FOURNISSEUR, a_INB_FACTURE,
          a_INVC_ID, a_INB_DATE_ACQUISITION);
      integrer_hb(a_inb_id);   
   else
      update inventaire_non_budgetaire set utl_ordre=a_utl_ordre, pco_num=a_pco_num, inb_duree=a_inb_duree, inb_type_amort=a_inb_type_amort,
          inb_montant=a_inb_montant, inb_montant_residuel=a_inb_montant_residuel, orig_id=a_orig_id, inb_numero_serie=a_inb_numero_serie, 
          inb_informations=a_inb_informations, inb_fournisseur=a_inb_fournisseur, inb_facture=a_inb_facture,
          inb_date_acquisition=a_inb_date_acquisition where inb_id=a_inb_id;
      modifier_hb(a_inb_id);   
   end if;
end;

procedure integrer_hb (inbid integer) is
   currentimp jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE%rowtype;
   CLICID  integer;
   INVCID  integer;
   numero  integer;
   comp    jefy_admin.organ.org_ub%type;
   cr      jefy_admin.organ.org_cr%type;
   exer    jefy_admin.exercice.exe_exercice%type;
   my_prorata_temporis integer;
BEGIN

   select * into currentimp from jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE where inb_id = inbid;

   -- creation de la cle d'inventaire  comptable
   -- recup de la cle primaire !
   select CLE_INVENTAIRE_COMPTABLE_SEQ.nextval into CLICID from dual;

   -- recup des infoamtions de la ligne budgetaire
   select org_ub,org_cr into comp,cr from jefy_admin.organ where org_id = currentimp.org_id;

   -- recup de lexercice
   select exe_exercice into exer from  jefy_admin.exercice where exe_ordre = currentimp.exe_ordre;

   -- recup prorata temporis ou non
   select decode(nvl(par_value,'NON'),'NON',0,1) into my_prorata_temporis from parametre 
      where par_key='PRORATA_TEMPORIS' and exe_ordre=currentimp.exe_ordre;

   -- insertion dans la table
   insert into  JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE 
      (CLIC_COMP,CLIC_CR,CLIC_DUREE_AMORT,CLIC_ETAT,CLIC_ID,CLIC_NB_ETIQUETTE,CLIC_NUMERO,CLIC_PRO_RATA,CLIC_TYPE_AMORT,
       EXE_ORDRE,PCO_NUM,PCOA_ID,CLIC_NUM_COMPLET)
     values (comp, cr, currentimp.inb_duree, 'etat', CLICID, 0,'automatique', my_prorata_temporis, currentimp.INB_TYPE_AMORT,
      currentimp.exe_ordre, currentimp.pco_num, jefy_inventaire.INTEGRATION.get_pcoaid(currentimp.exe_ordre,currentimp.pco_num), null);

   numero := inventaire_numero.get_numero_comptable (exer,comp,cr);

   -- maj de la cle d'inventaire  comptable
   update JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE set CLIC_NUMERO = nvl(numero,1), clic_num_complet = comp||'/'||cr||'/'||exer||'/'||nvl(numero,1)
      where CLIC_ID = CLICID;
 
   -- creation de l'inventaire comptable
   -- recup de la clef primaire
   select INVENTAIRE_COMPTABLE_SEQ.nextval into INVCID from dual;

   -- insertion dans la table 
   insert into  JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE 
      (CLIC_ID,DPCO_ID,EXE_ORDRE,INVC_DATE_ACQUISITION,INVC_ETAT,INVC_ID,INVC_MONTANT_ACQUISITION,INVC_MONTANT_RESIDUEL,ORGF_ID,UTL_ORDRE)
     values (CLICID,null, currentimp.exe_ordre, currentimp.inb_date_acquisition, null, INVCID, currentimp.INB_MONTANT, currentimp.INB_MONTANT_RESIDUEL,
       currentimp.ORiG_ID, currentimp.UTL_ORDRE);

   -- creation des amortissments
   if currentimp.inb_duree != 0 then
      api_corossol.creer_lien_inv_dep (-1000, invcid,currentimp.INB_MONTANT_RESIDUEL);
   end if;

   -- on marque l'enregistrement import? et on conserve le liens avec l'inventaire_comptable
   update INVENTAIRE_NON_BUDGETAIRE set invc_id = invcid where inb_id = inbid;
end;

procedure modifier_hb (inbid integer) is 
   my_inventaire jefy_inventaire.inventaire_non_budgetaire%rowtype;
begin
   select * into my_inventaire from jefy_inventaire.inventaire_non_budgetaire where inb_id=inbid;
 
   if my_inventaire.invc_id is null then
      integrer_hb(my_inventaire.inb_id);
   else
      -- maj  inventaire_comptable : casse le lien
      update jefy_inventaire.inventaire_comptable set invc_montant_acquisition=my_inventaire.inb_montant_residuel, 
         invc_montant_residuel=my_inventaire.inb_montant_residuel, invc_date_acquisition=my_inventaire.inb_date_acquisition
        where invc_id=my_inventaire.invc_id;

      Api_Corossol.amort_vider(my_inventaire.invc_id);
      Api_Corossol.amortissement(my_inventaire.invc_id);
   end if;
end;

procedure supprimer_hb (inbid integer) 
is 
   invcid jefy_inventaire.inventaire_non_budgetaire.invc_id%type;
   clicid jefy_inventaire.cle_inventaire_comptable.clic_id%type;
begin
   -- recup des infos 
   select invc_id into invcid from jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE where inb_id = inbid;

   if invcid is not null then
      UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id = NULL WHERE invc_id = invcid;
      Api_Corossol.amort_vider(invcid);
 
      -- suppression, recup du clicid
      select count(*) into clicid from jefy_inventaire.inventaire_comptable where invc_id = invcid;
      if (clicid != 0 ) then
         select clic_id into clicid from jefy_inventaire.inventaire_comptable where invc_id = invcid;
         delete from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE where  clic_id = clicid;
         delete from jefy_inventaire.INVENTAIRE_COMPTABLE where invc_id = invcid;
      end if;
   end if;
 
   delete from jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE where inb_id = inbid;
end;

END INTEGRATION_HB;
/



