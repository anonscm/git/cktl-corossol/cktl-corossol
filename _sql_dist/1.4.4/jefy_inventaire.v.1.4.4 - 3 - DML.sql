-- a executer sur le user JEFY_INVENTAIRE

declare
   cursor liste is select distinct exe_ordre, orgf_id from inventaire_comptable;
   my_exe_ordre    inventaire_comptable.exe_ordre%type;
   my_new_orgf_id  inventaire_comptable.orgf_id%type;
   my_old_orgf_id  inventaire_comptable.orgf_id%type;
   my_old_origine  origine_financement%rowtype;
   my_nb           integer;
begin
   select count(*) into my_nb from origine_financement where orgf_id=0;
   if my_nb=0 then
      insert into origine_financement values (0, 'A determiner', 2007, null);
   end if;
   
   update inventaire_comptable set orgf_id=0 where orgf_id is null;
  
   open liste();
   loop
      fetch liste into my_exe_ordre, my_old_orgf_id;
      exit when liste%notfound;
         
      select count(*) into my_nb from origine_financement where orgf_id=my_old_orgf_id and exe_ordre=my_exe_ordre;
      if my_nb=0 then
         select * into my_old_origine from origine_financement where orgf_id=my_old_orgf_id;
         
         select origine_financement_seq.nextval into my_new_orgf_id from dual;
         insert into origine_financement values (my_new_orgf_id, my_old_origine.orgf_libelle, my_exe_ordre, null);
         
         update inventaire_comptable set orgf_id=my_new_orgf_id where orgf_id=my_old_orgf_id and exe_ordre=my_exe_ordre;
      end if;      
   end loop;
   close liste;
   
   
   update inventaire_non_budgetaire 
   set inb_date_acquisition=(select invc_date_acquisition from inventaire_comptable where invc_id=inventaire_non_budgetaire.invc_id)
   where invc_id is not null;
   
   insert into db_version values (1440,'1.4.4.0',to_date('30/11/2010','dd/mm/yyyy'),sysdate,'');
   
   commit;
end;

ALTER TABLE JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE MODIFY(ORGF_ID NOT NULL);


