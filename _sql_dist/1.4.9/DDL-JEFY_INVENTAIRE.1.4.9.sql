-- Fichier :  no 1/2
-- Type : DDL
-- Schema modifie :  JEFY_INVENTAIRE
-- Schema d'execution du script : GRHUM
-- Numero de version : 1.4.9
-- Date de publication : 17/02/2012
-- Licence : CeCILL version 2

whenever sqlerror exit sql.sqlcode;


create or replace force view jefy_inventaire.v_actif_inventaire (exercice,ges_code,numero,exer,amo_annuite,amo_cumul,amo_residuel,amo_acquisition,pco_num)
as

   select exercice, ges_code, numero, exer, sum (amo_annuite), sum (amo_cumul), sum (amo_residuel), sum(amo_acquisition), pco_num from (
         select to_number (ee.exe_ordre) exercice, nvl(c.clic_comp,o.org_ub) ges_code, clic_num_complet numero, to_number(a.exe_ordre) exer,
                    amo_annuite, amo_cumul, amo_residuel, amo_acquisition, c.pco_num
               from jefy_inventaire.amortissement a, jefy_inventaire.inventaire_comptable i, jefy_inventaire.cle_inventaire_comptable c, 
               jefy_inventaire.inventaire_non_budgetaire inb, jefy_admin.organ o,
                    (select invc_id, min(exe_ordre) exe_ordre
                         from (select invc_id, to_number (exe_ordre) exe_ordre from jefy_inventaire.amortissement
                               union all
                               select i.invc_id, to_number(i.exe_ordre) from jefy_inventaire.inventaire_comptable i, 
                               jefy_inventaire.cle_inventaire_comptable c where c.clic_id=i.clic_id)
                     group by invc_id) ee
              where a.invc_id = i.invc_id and i.invc_id = inb.invc_id(+) and inb.org_id = o.org_id(+) and i.clic_id = c.clic_id and ee.invc_id = i.invc_id
         union all
         select to_number (i.exe_ordre) exercice, nvl (c.clic_comp, o.org_ub) ges_code, clic_num_complet numero, c.exe_ordre exer, 0 amo_annuite,
                    0 amo_cumul, decode (zz.nb, null, 1, 0) * amo_acquisition amo_residuel, decode (zz.nb, null, 1, 0) * amo_acquisition, c.pco_num
               from jefy_inventaire.amortissement a, jefy_inventaire.inventaire_comptable i, jefy_inventaire.cle_inventaire_comptable c,
                    jefy_inventaire.inventaire_non_budgetaire inb, jefy_admin.organ o,
                    (select   invc_id, exe_ordre, count (*) nb from jefy_inventaire.amortissement group by invc_id, exe_ordre) zz
              where a.invc_id = i.invc_id and i.invc_id = inb.invc_id(+) and inb.org_id = o.org_id(+) and i.clic_id = c.clic_id 
                    and amo_residuel = amo_acquisition - amo_annuite and zz.exe_ordre(+) = i.exe_ordre and zz.invc_id(+) = i.invc_id and i.exe_ordre <= c.exe_ordre
         union all
         select c.exe_ordre, c.clic_comp, c.clic_num_complet, c.exe_ordre,0,decode(inb_montant_residuel,0,inb_montant,0),
              decode(inb_montant_residuel,0,0,inb_montant),inb_montant, c.pco_num 
              from jefy_inventaire.inventaire_non_budgetaire inb, jefy_inventaire.inventaire_comptable ic, jefy_inventaire.cle_inventaire_comptable c
             where c.clic_id=ic.clic_id and ic.invc_id=inb.invc_id and inb.invc_id in (
                 select invc_id from jefy_inventaire.inventaire_non_budgetaire where invc_id is not null and (inb_duree=0 or inb_montant_residuel=0)
                 minus select invc_id from jefy_inventaire.amortissement) 
   ) 
   group by exercice, ges_code, numero, exer, pco_num
   having sum(amo_acquisition)<>0;

   
CREATE OR REPLACE PACKAGE JEFY_INVENTAIRE.API_regularisation AS

/*
 * Copyright Cocktail, 2001-2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 
procedure regrouper(
      a_clic_id_origine         inventaire_comptable.clic_id%type,
      a_clic_id_destination     inventaire_comptable.clic_id%type
);

procedure eclater(
      a_clic_id_origine         inventaire_comptable.clic_id%type,
      a_chaine_inventaires      varchar2,
      a_clic_id_destination out inventaire_comptable.clic_id%type
);

function chaine_origines (
      a_invc_id    inventaire_comptable_orig.invc_id%type
) return varchar2; 

END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE.API_regularisation AS

procedure regrouper (
      a_clic_id_origine         inventaire_comptable.clic_id%type,
      a_clic_id_destination     inventaire_comptable.clic_id%type
) is
   my_nb                        integer;
   my_cle_inv_compt_origine     cle_inventaire_comptable%rowtype;
   my_cle_inv_compt_destination cle_inventaire_comptable%rowtype;
begin 
    select count(*) into my_nb from inventaire_comptable where clic_id=a_clic_id_origine;
    if my_nb=0 then
        raise_application_error(-20001,'L''inventaire comptable d''origine n''existe pas (clic_id='||a_clic_id_origine||')');
    end if;
    select count(*) into my_nb from inventaire_comptable where clic_id=a_clic_id_destination;
    if my_nb=0 then
        raise_application_error(-20001,'L''inventaire comptable de destination n''existe pas (clic_id='||a_clic_id_destination||')');
    end if;

    if a_clic_id_origine<>a_clic_id_destination then
       select * into my_cle_inv_compt_origine from cle_inventaire_comptable where clic_id=a_clic_id_origine;
       select * into my_cle_inv_compt_destination from cle_inventaire_comptable where clic_id=a_clic_id_destination;
    
       if my_cle_inv_compt_origine.exe_ordre<>my_cle_inv_compt_destination.exe_ordre then
           raise_application_error(-20001,'Les inventaires comptables doivent avoir le meme exercice');
       end if;
       if my_cle_inv_compt_origine.clic_comp<>my_cle_inv_compt_destination.clic_comp then
           raise_application_error(-20001,'Les inventaires comptables doivent avoir la meme UB');
       end if;
       if my_cle_inv_compt_origine.pco_num<>my_cle_inv_compt_destination.pco_num then
           raise_application_error(-20001,'Les inventaires comptables doivent avoir la meme imputation');
       end if;
    
       update inventaire_comptable set clic_id=a_clic_id_destination where clic_id=a_clic_id_origine;
       delete from cle_inventaire_comptable where clic_id=a_clic_id_origine;
    end if;
end;

procedure eclater (
      a_clic_id_origine         inventaire_comptable.clic_id%type,
      a_chaine_inventaires      varchar2,
      a_clic_id_destination out inventaire_comptable.clic_id%type
) is
   my_nb                        integer;
   my_inv_id                    inventaire.inv_id%type;
   my_clic_id                   cle_inventaire_comptable.clic_id%type;
   my_invc_id                   inventaire_comptable.clic_id%type;
   my_inventaire                inventaire%rowtype;
   my_inventaire_comptable      inventaire_comptable%rowtype;
   my_inv_montant_acquisition   inventaire.inv_montant_acquisition%type;
   my_cle_inventaire_comptable  cle_inventaire_comptable%rowtype;
   my_clic_numero               cle_inventaire_comptable.clic_numero%type;
   my_chaine                    VARCHAR2(30000);
begin
    select count(*) into my_nb from inventaire_comptable where clic_id=a_clic_id_origine;
    if my_nb=0 then
        raise_application_error(-20001,'L''inventaire comptable d''origine n''existe pas (clic_id='||a_clic_id_origine||')');
    end if;

    my_clic_id:=null;
    my_chaine:=a_chaine_inventaires;
    LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_inv_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            select * into my_inventaire from inventaire where inv_id=my_inv_id;
            select * into my_inventaire_comptable from inventaire_comptable where invc_id=my_inventaire.invc_id;
            select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=my_inventaire_comptable.clic_id;
            
          
            -- restriction qu'a certains cas
              -- pas de facture
              -- liquidation = montant acquisition
              -- ...
            --
            
            if my_clic_id is null then
               my_clic_numero:=inventaire_numero.get_numero_comptable(my_cle_inventaire_comptable.exe_ordre, my_cle_inventaire_comptable.clic_comp, 
                   my_cle_inventaire_comptable.clic_cr);
               select cle_inventaire_comptable_seq.nextval into my_clic_id from dual;
               insert into cle_inventaire_comptable values (my_cle_inventaire_comptable.clic_comp, my_cle_inventaire_comptable.clic_cr, 
                   my_cle_inventaire_comptable.clic_duree_amort, my_cle_inventaire_comptable.clic_etat, my_clic_id, 
                   my_cle_inventaire_comptable.clic_nb_etiquette, my_clic_numero, my_cle_inventaire_comptable.clic_pro_rata, 
                   my_cle_inventaire_comptable.clic_type_amort, my_cle_inventaire_comptable.exe_ordre, my_cle_inventaire_comptable.pco_num, 
                   my_cle_inventaire_comptable.pcoa_id, 
                   inventaire_numero.get_numero_complet(my_cle_inventaire_comptable.exe_ordre, my_cle_inventaire_comptable.clic_comp, 
                       my_cle_inventaire_comptable.clic_cr, my_clic_numero));
            end if;
            
            select count(*) into my_nb from inventaire where invc_id=my_inventaire.invc_id and inv_id<>my_inv_id;
            if my_nb=0 then
               update inventaire_comptable set clic_id=my_clic_id where invc_id=my_inventaire.invc_id;
            else
                -- si l'inventaire comptable correspond � plusieurs biens, et qu'il a un ORV ... erreur, 
                -- car on ne sait pas � quel bien il correspond 
                select count(*) into my_nb from inventaire_comptable_orv where invc_id=my_inventaire.invc_id;
                if my_nb>0 then
                   raise_application_error(-20001, 'On ne peut pas eclater un inventaire sur lequel il y a eu un reversement');
                end if;

                select nvl(sum(inv_montant_acquisition),0) into my_inv_montant_acquisition from inventaire where invc_id=my_inventaire_comptable.invc_id;
                if my_inv_montant_acquisition<>my_inventaire_comptable.invc_montant_acquisition then
                   raise_application_error(-20001, 'la somme des montants d''acquisition des inventaires physiques n''est pas egale a celui de l''inventaire comptable');
                end if;
                     
                my_invc_id:=null;
                api_comptable.enregistrer_comptable(my_clic_id, my_inventaire_comptable.dpco_id, my_inventaire_comptable.exe_ordre,
                   my_inventaire_comptable.invc_date_acquisition, my_inventaire_comptable.invc_etat, my_invc_id, my_inventaire.inv_montant_acquisition,
                   my_inventaire.inv_montant_acquisition, chaine_origines(my_inventaire_comptable.invc_id), my_inventaire_comptable.utl_ordre,
                   my_inventaire_comptable.invc_date_sortie, my_inventaire_comptable.dgco_id, my_inv_id||'$$', my_cle_inventaire_comptable.clic_comp,
                   my_cle_inventaire_comptable.clic_cr, my_cle_inventaire_comptable.clic_duree_amort, my_cle_inventaire_comptable.clic_etat,
                   my_cle_inventaire_comptable.clic_nb_etiquette, my_cle_inventaire_comptable.clic_pro_rata, my_cle_inventaire_comptable.clic_type_amort,
                   my_cle_inventaire_comptable.pco_num, my_cle_inventaire_comptable.pcoa_id);
                api_corossol.amortissement(my_invc_id);

                -- modification montants inventaire_comptable et amortissements associ�s ...
                update inventaire set invc_id=my_invc_id where inv_id=my_inv_id;
                update inventaire_comptable set invc_montant_acquisition=invc_montant_acquisition-my_inventaire.inv_montant_acquisition,
                  invc_montant_residuel=invc_montant_residuel-my_inventaire.inv_montant_acquisition where invc_id=my_inventaire_comptable.invc_id;
                api_corossol.amortissement(my_inventaire_comptable.invc_id);

            end if;
    END LOOP;
    
    a_clic_id_destination:=my_clic_id;
end;

function chaine_origines (
      a_invc_id    inventaire_comptable_orig.invc_id%type
) return varchar2 
is
   my_chaine                    VARCHAR2(30000); 
   my_inventaire_comptable_orig jefy_inventaire.inventaire_comptable_orig%ROWTYPE;
   CURSOR my_cursor IS SELECT * FROM jefy_inventaire.inventaire_comptable_orig WHERE invc_id=a_invc_id;

begin
	   OPEN my_cursor;
	   LOOP
	   	   FETCH my_cursor INTO my_inventaire_comptable_orig;
	   	   EXIT WHEN my_cursor%NOTFOUND;

		   my_chaine:=my_chaine||my_inventaire_comptable_orig.orgf_id||'$'||my_inventaire_comptable_orig.icor_pourcentage||'$'||
               my_inventaire_comptable_orig.tit_id||'$';
	   END LOOP;
	   CLOSE my_cursor;

	   RETURN my_chaine||'$';
end;

END;
/

CREATE OR REPLACE PACKAGE JEFY_INVENTAIRE.API_REPORT AS

function chaine_financement (
      a_clic_id cle_inventaire_comptable.clic_id%type
) return varchar2;

end;
/

CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE.API_REPORT AS

function chaine_financement (
      a_clic_id cle_inventaire_comptable.clic_id%type
) return varchar2 is 
  my_chaine           varchar2(4000);
  my_orgf_libelle     origine_financement.orgf_libelle%type;
  my_icor_pourcentage inventaire_comptable_orig.icor_pourcentage%type;
  
  cursor c1 is
    select f.orgf_libelle, round(sum(o.icor_pourcentage*i.invc_montant_acquisition/total.montant),2)
    from jefy_inventaire.inventaire_comptable_orig o, jefy_inventaire.origine_financement f, 
       jefy_inventaire.inventaire_comptable i, 
       (select clic_id, sum(invc_montant_acquisition) montant from jefy_inventaire.inventaire_comptable where clic_id=a_clic_id group by clic_id) total
    where i.invc_id=o.invc_id and f.orgf_id=o.orgf_id and i.clic_id=a_clic_id and i.clic_id=total.clic_id
    group by f.orgf_libelle order by round(sum(o.icor_pourcentage*i.invc_montant_acquisition/total.montant),2) desc;
begin
  my_chaine:='';
  
  open c1;
  loop
    fetch c1 into my_orgf_libelle, my_icor_pourcentage;
    exit when c1%notfound;

    if length(my_chaine)>0 then my_chaine:=my_chaine||', '; end if;
    my_chaine:=my_chaine||my_orgf_libelle||' '||my_icor_pourcentage||'%';
  end loop;
  close c1;

  return my_chaine;
end;

end;
/



create or replace procedure grhum.inst_patch_jefy_inv_1490 is
 nb integer;
begin
	 insert into jefy_inventaire.db_version values (1490,'1.4.9.0',to_date('17/02/2012','dd/mm/yyyy'),sysdate,null);  
end;
/