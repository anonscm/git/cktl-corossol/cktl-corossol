-- Fichier :  no 2/2
-- Type : DML
-- Schema modifie :  JEFY_INVENTAIRE
-- Schema d'execution du script : GRHUM
-- Numero de version : 1.4.9
-- Date de publication : 17/02/2012
-- Licence : CeCILL version 2


whenever sqlerror exit sql.sqlcode ;

execute grhum.inst_patch_jefy_inv_1490;
commit;

drop procedure grhum.inst_patch_jefy_inv_1490;

