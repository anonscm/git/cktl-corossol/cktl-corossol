-- Vue Cocktail d'origine (version <= v1.5.4)
  CREATE OR REPLACE FORCE VIEW "JEFY_INVENTAIRE"."V_ACTIF_INVENTAIRE" ("EXERCICE", "GES_CODE", "NUMERO", "EXER", "AMO_ANNUITE", "AMO_CUMUL", "AMO_RESIDUEL", "AMO_ACQUISITION", "PCO_NUM") AS 
  select exercice, ges_code, numero, exer, sum (amo_annuite), sum (amo_cumul), sum (amo_residuel), sum(amo_acquisition), pco_num from (
         select to_number (ee.exe_ordre) exercice, nvl(c.clic_comp,o.org_ub) ges_code, clic_num_complet numero, to_number(a.exe_ordre) exer,
                    amo_annuite, amo_cumul, amo_residuel, amo_acquisition, c.pco_num
               from jefy_inventaire.amortissement a, jefy_inventaire.inventaire_comptable i, jefy_inventaire.cle_inventaire_comptable c, 
               jefy_inventaire.inventaire_non_budgetaire inb, jefy_admin.organ o,
                    (select invc_id, min(exe_ordre) exe_ordre
                         from (select invc_id, to_number (exe_ordre) exe_ordre from jefy_inventaire.amortissement
                               union all
                               select i.invc_id, to_number(i.exe_ordre) from jefy_inventaire.inventaire_comptable i, 
                               jefy_inventaire.cle_inventaire_comptable c where c.clic_id=i.clic_id)
                     group by invc_id) ee
              where a.invc_id = i.invc_id and i.invc_id = inb.invc_id(+) and inb.org_id = o.org_id(+) and i.clic_id = c.clic_id and ee.invc_id = i.invc_id
         union all
         select to_number (i.exe_ordre) exercice, nvl (c.clic_comp, o.org_ub) ges_code, clic_num_complet numero, c.exe_ordre exer, 0 amo_annuite,
                    0 amo_cumul, decode (zz.nb, null, 1, 0) * amo_acquisition amo_residuel, decode (zz.nb, null, 1, 0) * amo_acquisition, c.pco_num
               from jefy_inventaire.amortissement a, jefy_inventaire.inventaire_comptable i, jefy_inventaire.cle_inventaire_comptable c,
                    jefy_inventaire.inventaire_non_budgetaire inb, jefy_admin.organ o,
                    (select   invc_id, exe_ordre, count (*) nb from jefy_inventaire.amortissement group by invc_id, exe_ordre) zz
              where a.invc_id = i.invc_id and i.invc_id = inb.invc_id(+) and inb.org_id = o.org_id(+) and i.clic_id = c.clic_id 
                    and amo_residuel = amo_acquisition - amo_annuite and zz.exe_ordre(+) = i.exe_ordre and zz.invc_id(+) = i.invc_id and i.exe_ordre <= c.exe_ordre
         union all
         select c.exe_ordre, c.clic_comp, c.clic_num_complet, c.exe_ordre,0,decode(inb_montant_residuel,0,inb_montant,0),
              decode(inb_montant_residuel,0,0,inb_montant),inb_montant, c.pco_num 
              from jefy_inventaire.inventaire_non_budgetaire inb, jefy_inventaire.inventaire_comptable ic, jefy_inventaire.cle_inventaire_comptable c
             where c.clic_id=ic.clic_id and ic.invc_id=inb.invc_id and inb.invc_id in (
                 select invc_id from jefy_inventaire.inventaire_non_budgetaire where invc_id is not null and (inb_duree=0 or inb_montant_residuel=0)
                 minus select invc_id from jefy_inventaire.amortissement) 
   ) 
   group by exercice, ges_code, numero, exer, pco_num
   having sum(amo_acquisition)<>0
 
 ;
