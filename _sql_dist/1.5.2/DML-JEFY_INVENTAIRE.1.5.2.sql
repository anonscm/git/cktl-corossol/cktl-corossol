SET DEFINE OFF
--
--  
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié : JEFY_INVENTAIRE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.5.2
-- Date de publication : 02/04/2013
-- Licence : CeCILL version 2
--
-- Evolutions :
--   - Droit specifique pour les operations de regularisation
--   - Differenciation et mise en place des notions de sortie physique et de sortie comptable
--   - Mise en place de tables d'archive pour les reprises d'inventaire
--

whenever sqlerror exit sql.sqlcode ;

execute grhum.inst_patch_jefy_inv_1520;
commit;

drop procedure grhum.inst_patch_jefy_inv_1520;
