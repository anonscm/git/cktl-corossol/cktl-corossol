CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE.API_comptable AS

 procedure supprimer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type
) is
   my_nb             integer;
begin
     select count(*) into my_nb from inventaire_comptable where dgco_id in 
        (select dgco_id from degressif_coef where dgrf_id=a_dgrf_id);
     if my_nb>0 then
        raise_application_error(-20001,'Suppression impossible, des coefficients sont utilisés par des inventaires comptables');
     end if;

     delete from degressif_coef where dgrf_id=a_dgrf_id;
     delete from degressif where dgrf_id=a_dgrf_id;
end;

 procedure enregistrer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type,
      a_dgrf_date_debut          degressif.dgrf_date_debut%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type,
      a_chaine_degressif_coef    varchar2
) is
   my_nb             integer;
   my_chaine         varchar2(20000);
   my_dgco_duree_min degressif_coef.dgco_duree_min%type;
   my_dgco_duree_max degressif_coef.dgco_duree_max%type;
   my_dgco_coef      degressif_coef.dgco_coef%type;
   my_dgco_id        degressif_coef.dgco_id%type;
   my_dgrf_id        degressif.dgrf_id%type;
begin
  my_nb:=0;

  if a_dgrf_date_debut is null then
     raise_application_error(-20001,'La date de debut ne doit pas etre nulle !');
  end if;

  if a_dgrf_id is not null then
     -- verifie la non utilisation des coefficients
     select count(*) into my_nb from inventaire_comptable where dgco_id in 
        (select dgco_id from degressif_coef where dgrf_id=a_dgrf_id);
     if my_nb>0 then
        raise_application_error(-20001,'Des coefficients sont utilisés par des inventaires comptables, on ne peut donc rien modifier');
     end if;
     
     -- verifie les chevauchements de dates
     if a_dgrf_date_fin is null then
        select count(*) into my_nb from degressif where dgrf_id<>a_dgrf_id and (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     else
        select count(*) into my_nb from degressif where dgrf_id<>a_dgrf_id and (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null)
           and (dgrf_date_debut<a_dgrf_date_fin);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     end if;
     
     update degressif set dgrf_date_debut=a_dgrf_date_debut, dgrf_date_fin=a_dgrf_date_fin where dgrf_id=a_dgrf_id;
  else
     -- on cloture la periode précédente si elle existe
     select count(*) into my_nb from degressif where a_dgrf_date_debut>dgrf_date_debut and dgrf_date_fin is null;
     if my_nb>0 then
        select min(dgrf_id) into my_dgrf_id from degressif where a_dgrf_date_debut>dgrf_date_debut and dgrf_date_fin is null;
        set_degressif_date_fin(my_dgrf_id, to_date(to_char(a_dgrf_date_debut-1,'dd/mm/yyyy'),'dd/mm/yyyy'));
     end if; 
  
     -- verifie les chevauchements de dates
     if a_dgrf_date_fin is null then
        select count(*) into my_nb from degressif where (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     else
        select count(*) into my_nb from degressif where (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null)
           and (dgrf_date_debut<a_dgrf_date_fin);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     end if;

     select degressif_seq.nextval into a_dgrf_id from dual;
     insert into degressif values (a_dgrf_id, a_dgrf_date_debut, a_dgrf_date_fin);
  end if;

  delete from degressif_coef where dgrf_id=a_dgrf_id;
  my_chaine:=a_chaine_degressif_coef;
  loop
      if substr(my_chaine,1,1)='$' then exit; end if;

      -- on recupere la duree min
      select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_dgco_duree_min from dual;
      my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
      -- on recupere la diuree max
      select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_dgco_duree_max from dual;
      my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
      -- on recupere le coef
      select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_dgco_coef from dual;
      my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));

      if my_dgco_duree_min is null then 
         raise_application_error(-20001, 'La duree minimale ne doit pas etre nulle');
      end if;
      if my_dgco_duree_max is null then 
         raise_application_error(-20001, 'La duree maximale ne doit pas etre nulle');
      end if;
      if my_dgco_duree_min>my_dgco_duree_max then 
         raise_application_error(-20001, 'La duree maximale doit etre superieure a la duree minimale');
      end if;

      select count(*) into my_nb from degressif_coef
        where dgrf_id=a_dgrf_id and dgco_duree_min<=my_dgco_duree_max and dgco_duree_max>=my_dgco_duree_min;
      if my_nb>0 then
         raise_application_error(-20001, '2 coefficients ont des periodes qui se chevauchent');
      end if;

      -- insertion dans la base.
      select degressif_coef_seq.nextval into my_dgco_id from dual;
      insert into degressif_coef values (my_dgco_id, a_dgrf_id, my_dgco_duree_min, my_dgco_duree_max, my_dgco_coef);
  end loop;

end;

procedure set_degressif_date_fin (
      a_dgrf_id                  degressif.dgrf_id%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type
) is
  my_degressif     degressif%rowtype;
   my_nb           integer;
begin
  select * into my_degressif from degressif where dgrf_id=a_dgrf_id;
  if my_degressif.dgrf_date_debut>=a_dgrf_date_fin or a_dgrf_date_fin is null then
     raise_application_error(-20001, 'la date de fin doit etre superieure a la date de debut');
  end if;
     
  -- verifier qu'il n'y a pas deux periodes se chevauchant 
  select count(*) into my_nb from degressif where dgrf_id<>a_dgrf_id and (my_degressif.dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null)
     and (dgrf_date_debut<a_dgrf_date_fin);
  if my_nb>0 then
     raise_application_error(-20001,'Probleme de chevauchement de dates !');
  end if;
  
  update degressif set dgrf_date_fin=a_dgrf_date_fin where dgrf_id=a_dgrf_id;
end;


PROCEDURE enregistrer_comptable(   
      a_clic_id           in out inventaire_comptable.clic_id%type,
      a_dpco_id                  inventaire_comptable.dpco_id%type,
      a_exe_ordre                inventaire_comptable.exe_ordre%type,
      a_invc_date_acquisition    inventaire_comptable.invc_date_acquisition%type,
      a_invc_etat                inventaire_comptable.invc_etat%type,
      a_invc_id           in out inventaire_comptable.invc_id%type,
      a_invc_montant_acquisition inventaire_comptable.invc_montant_acquisition%type,
      a_invc_montant_residuel    inventaire_comptable.invc_montant_residuel%type,
      a_chaine_origine           varchar2,
      a_utl_ordre                inventaire_comptable.utl_ordre%type,
      a_invc_date_sortie         inventaire_comptable.invc_date_sortie%type,
      a_dgco_id                  inventaire_comptable.dgco_id%type,
      a_chaine_inventaire        varchar2,
      
      a_clic_comp                cle_inventaire_comptable.clic_comp%type,
      a_clic_cr                  cle_inventaire_comptable.clic_cr%type,
      a_clic_duree_amort         cle_inventaire_comptable.clic_duree_amort%type,
      a_clic_etat                cle_inventaire_comptable.clic_etat%type,
      a_clic_nb_etiquette        cle_inventaire_comptable.clic_nb_etiquette%type,
      a_clic_pro_rata            cle_inventaire_comptable.clic_pro_rata%type,
      a_clic_type_amort          cle_inventaire_comptable.clic_type_amort%type,
      a_pco_num                  cle_inventaire_comptable.pco_num%type,
      a_pcoa_id                  cle_inventaire_comptable.pcoa_id%type
) IS 
  my_chaine           varchar2(2000);
  my_nb               integer;
  my_inv_id           inventaire.inv_id%type;
  my_clic_numero      cle_inventaire_comptable.clic_numero%type;
  my_cle_inventaire_comptable cle_inventaire_comptable%rowtype;
  my_recalcul_autre   integer;
  my_invc_id          inventaire_comptable.invc_id%type;
  my_orgf_id          inventaire_comptable.orgf_id%type;
  cursor c1 is select invc_id from inventaire_comptable where clic_id=a_clic_id;
BEGIN
  my_nb:=0;
  my_recalcul_autre:=0;
  
  if a_invc_id is not null then
     select count(*) into my_nb from inventaire_comptable where invc_id=a_invc_id;
  end if;
  
  if a_invc_id is null or my_nb=0 then
     if a_clic_id is null then
       if a_clic_comp is null and a_clic_cr is null and a_exe_ordre is null then
          raise_application_error(-20001, 'Vous devez renseigner l''UB, le CR et l''exercice');
       end if;
       
       my_clic_numero:=inventaire_numero.get_numero_comptable(a_exe_ordre, a_clic_comp, a_clic_cr);
       select cle_inventaire_comptable_seq.nextval into a_clic_id from dual;
       
       insert into cle_inventaire_comptable values (a_clic_comp, a_clic_cr, a_clic_duree_amort, a_clic_etat, a_clic_id, a_clic_nb_etiquette,
          my_clic_numero, a_clic_pro_rata, a_clic_type_amort, a_exe_ordre, a_pco_num, a_pcoa_id, 
          inventaire_numero.get_numero_complet(a_exe_ordre, a_clic_comp, a_clic_cr, my_clic_numero));
     else
       select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=a_clic_id;
       if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort then
          insert into cle_inventaire_compt_modif values (cle_inventaire_compt_modif_seq.nextval, a_clic_id, a_utl_ordre, my_cle_inventaire_comptable.clic_duree_amort,
              a_clic_duree_amort, sysdate);
       end if;
       if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort or my_cle_inventaire_comptable.clic_pro_rata<>a_clic_pro_rata or 
          my_cle_inventaire_comptable.clic_type_amort<>a_clic_type_amort then
            my_recalcul_autre:=1;
       end if;
          
       update cle_inventaire_comptable set clic_duree_amort=a_clic_duree_amort, clic_nb_etiquette=a_clic_nb_etiquette,
          clic_type_amort=a_clic_type_amort, clic_pro_rata=a_clic_pro_rata, pco_num=a_pco_num, pcoa_id=a_pcoa_id where clic_id=a_clic_id;
     end if;
  
     select inventaire_comptable_seq.nextval into a_invc_id from dual;
     insert into inventaire_comptable values(a_clic_id, a_dpco_id, a_exe_ordre, a_invc_date_acquisition, a_invc_etat, a_invc_id,
        a_invc_montant_acquisition, a_invc_montant_residuel, 1, a_utl_ordre, a_invc_date_sortie, a_dgco_id);
     my_orgf_id:=inventaire_origines(a_invc_id, a_chaine_origine);
     update inventaire_comptable set orgf_id=my_orgf_id where invc_id=a_invc_id;
  else
     select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=a_clic_id;
     if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort then
        insert into cle_inventaire_compt_modif values (cle_inventaire_compt_modif_seq.nextval, a_clic_id, a_utl_ordre, my_cle_inventaire_comptable.clic_duree_amort,
            a_clic_duree_amort, sysdate);
     end if;
     if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort or my_cle_inventaire_comptable.clic_pro_rata<>a_clic_pro_rata or 
        my_cle_inventaire_comptable.clic_type_amort<>a_clic_type_amort then
          my_recalcul_autre:=1;
     end if;

     update cle_inventaire_comptable set clic_duree_amort=a_clic_duree_amort, clic_nb_etiquette=a_clic_nb_etiquette,
          clic_type_amort=a_clic_type_amort, clic_pro_rata=a_clic_pro_rata, pco_num=a_pco_num, pcoa_id=a_pcoa_id where clic_id=a_clic_id;
     my_orgf_id:=inventaire_origines(a_invc_id, a_chaine_origine);
     update inventaire_comptable set orgf_id=my_orgf_id, utl_ordre=a_utl_ordre, dpco_id=a_dpco_id, invc_montant_acquisition=a_invc_montant_acquisition,
        invc_montant_residuel=a_invc_montant_residuel, dgco_id=a_dgco_id where invc_id=a_invc_id;
  end if;
  
  my_chaine:=a_chaine_inventaire;
  loop
     if my_chaine is null or length(my_chaine)=0 or substr(my_chaine,1,1)='$' then exit; end if;

     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_inv_id from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));

     select count(*) into my_nb from inventaire where inv_id=my_inv_id and invc_id is null;
     if my_nb>0 then
        update inventaire set invc_id=a_invc_id where inv_id=my_inv_id and invc_id is null; 
     end if;
  end loop;

  if a_dpco_id is not null then
     api_corossol.AMORTISSEMENT(a_invc_id);
  end if;
  
  if my_recalcul_autre>0 then
    OPEN c1();
      LOOP
      FETCH  c1 INTO my_invc_id;
      EXIT WHEN c1%NOTFOUND;
        if a_invc_id<>my_invc_id then
           api_corossol.AMORTISSEMENT(my_invc_id);
        end if;
      END LOOP;
      CLOSE c1;
  end if;
  
END;
 
function inventaire_origines(a_invc_id inventaire_comptable.invc_id%type, a_chaine_origine varchar2)
return number
is
    my_chaine            varchar2(2000);
    my_inv_orgf_id       inventaire_comptable.orgf_id%type;

    my_icor_id           inventaire_comptable_orig.icor_id%type;
    my_orgf_id           inventaire_comptable_orig.orgf_id%type;
    my_icor_pourcentage  inventaire_comptable_orig.icor_pourcentage%type;
    my_somme_pourcentage inventaire_comptable_orig.icor_pourcentage%type;
    my_tit_id            inventaire_comptable_orig.tit_id%type;
begin
  my_chaine:=a_chaine_origine;
  my_inv_orgf_id:=null;
  my_somme_pourcentage:=0;
  
  delete from inventaire_comptable_orig where invc_id=a_invc_id;
  
  loop
     if my_chaine is null or length(my_chaine)=0 or substr(my_chaine,1,1)='$' then exit; end if;

     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_orgf_id from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_icor_pourcentage from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_tit_id from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));

     if my_inv_orgf_id is null then my_inv_orgf_id:=my_orgf_id; end if;
     my_somme_pourcentage:=my_somme_pourcentage+my_icor_pourcentage;
     
     select inventaire_comptable_orig_seq.nextval into my_icor_id from dual;
     insert into inventaire_comptable_orig values (my_icor_id, a_invc_id, my_orgf_id, my_icor_pourcentage, my_tit_id);
  end loop;
  
  if my_somme_pourcentage<>100.0 then 
     raise_application_error(-20001, 'La somme des pourcentages des origines de financement doit etre egale a 100%');
  end if;
  
  return my_inv_orgf_id;
end;

/*PROCEDURE supprimer_sortie (
      a_invs_id           inventaire_sortie.invs_id%type
) IS
  my_invc_id inventaire.invc_id%type;
BEGIN
  select i.invc_id into my_invc_id from inventaire i, inventaire_sortie s where s.inv_id=i.inv_id and s.invs_id=a_invs_id;
  delete from inventaire_sortie where invs_id=a_invs_id;

  if my_invc_id is not null then
     api_corossol.AMORTISSEMENT(my_invc_id);
  end if;
END;*/

END;
/
