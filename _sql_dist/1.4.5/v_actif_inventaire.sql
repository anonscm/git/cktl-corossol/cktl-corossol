DROP VIEW JEFY_INVENTAIRE.V_ACTIF_INVENTAIRE;

/* Formatted on 2010/12/16 15:40 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW jefy_inventaire.v_actif_inventaire (exercice,
                                                                 ges_code,
                                                                 numero,
                                                                 exer,
                                                                 amo_annuite,
                                                                 amo_cumul,
                                                                 amo_residuel,
                                                                 amo_acquisition,
                                                                 pco_num
                                                                )
AS
   (SELECT c.exe_ordre * 1 exercice, c.clic_comp ges_code,
           clic_num_complet numero, a.exe_ordre * 1 exer, amo_annuite,
           amo_cumul, amo_residuel, amo_acquisition, c.pco_num
      FROM amortissement a, inventaire_comptable i,
           cle_inventaire_comptable c
     WHERE a.invc_id = i.invc_id AND i.clic_id = c.clic_id
    UNION ALL
    SELECT c.exe_ordre * 1 exercice, c.clic_comp ges_code,
           clic_num_complet numero, c.exe_ordre exer, 0 amo_annuite,
           0 amo_cumul, decode(zz.nb,null,1,0)*amo_acquisition amo_residuel, decode(zz.nb,null,1,0)*amo_acquisition,
           c.pco_num
      FROM jefy_inventaire.amortissement a, jefy_inventaire.inventaire_comptable i, jefy_inventaire.cle_inventaire_comptable c,
      (select invc_id, exe_ordre, count(*) nb from jefy_inventaire.amortissement  group by invc_id, exe_ordre) zz
     WHERE a.invc_id = i.invc_id
       AND i.clic_id = c.clic_id
       AND amo_residuel = amo_acquisition - amo_annuite
       and zz.exe_ordre(+)=i.exe_ordre and zz.invc_id(+)=i.invc_id
/*union all
select RGP_EXER*1 exercice,RGP_COMP ges_code,RGP_NUMERO numero,AMO_EXER*1 exer,AMO_ANNUITE,AMO_CUMUL,AMO_RESIDUEL,AMO_ACQUISITION,PCO_NUM
from INVENTAIRE.ETAT_ACTIFS_BRUT*/
   );


