CREATE OR REPLACE PACKAGE JEFY_INVENTAIRE.API_COMPTABLE AS

/*
 * Copyright Cocktail, 2001-2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 
 procedure supprimer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type
);

procedure enregistrer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type,
      a_dgrf_date_debut          degressif.dgrf_date_debut%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type,
      a_chaine_degressif_coef    varchar2
);

procedure set_degressif_date_fin (
      a_dgrf_id                  degressif.dgrf_id%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type
);

 PROCEDURE enregistrer_comptable (
      a_clic_id           in out inventaire_comptable.clic_id%type,
      a_dpco_id                  inventaire_comptable.dpco_id%type,
      a_exe_ordre                inventaire_comptable.exe_ordre%type,
      a_invc_date_acquisition    inventaire_comptable.invc_date_acquisition%type,
      a_invc_etat                inventaire_comptable.invc_etat%type,
      a_invc_id           in out inventaire_comptable.invc_id%type,
      a_invc_montant_acquisition inventaire_comptable.invc_montant_acquisition%type,
      a_invc_montant_residuel    inventaire_comptable.invc_montant_residuel%type,
      a_chaine_origine           varchar2,
      a_utl_ordre                inventaire_comptable.utl_ordre%type,
      a_invc_date_sortie         inventaire_comptable.invc_date_sortie%type,
      a_dgco_id                  inventaire_comptable.dgco_id%type,
      a_chaine_inventaire        varchar2,
      
      a_clic_comp                cle_inventaire_comptable.clic_comp%type,
      a_clic_cr                  cle_inventaire_comptable.clic_cr%type,
      a_clic_duree_amort         cle_inventaire_comptable.clic_duree_amort%type,
      a_clic_etat                cle_inventaire_comptable.clic_etat%type,
      a_clic_nb_etiquette        cle_inventaire_comptable.clic_nb_etiquette%type,
      a_clic_pro_rata            cle_inventaire_comptable.clic_pro_rata%type,
      a_clic_type_amort          cle_inventaire_comptable.clic_type_amort%type,
      a_pco_num                  cle_inventaire_comptable.pco_num%type,
      a_pcoa_id                  cle_inventaire_comptable.pcoa_id%type
 );

function inventaire_origines(a_invc_id inventaire_comptable.invc_id%type, a_chaine_origine varchar2)
return number;

END;
/
