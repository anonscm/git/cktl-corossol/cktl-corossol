declare
begin
   jefy_admin.api_application.creerfonction(1708,'INVTITRE','Inventaire','Association Titre de recette/Inventaire comptable','Association Titre de recette/Inventaire comptable','N','N',17);

   insert into jefy_inventaire.inventaire_comptable_orig 
      select jefy_inventaire.inventaire_comptable_orig_seq.nextval, invc_id, orgf_id, 100, null from jefy_inventaire.inventaire_comptable 
       where invc_id in
         (select invc_id from jefy_inventaire.inventaire_comptable minus select invc_id from jefy_inventaire.inventaire_comptable_orig);

	 commit;
end;
/

