-- Fichier :  no 2/2
-- Type : DML
-- Schema modifie :  JEFY_INVENTAIRE
-- Schema d'execution du script : GRHUM
-- Numero de version : 1.5.0
-- Date de publication : 02/04/2012
-- Licence : CeCILL version 2


whenever sqlerror exit sql.sqlcode ;

execute grhum.inst_patch_jefy_inv_1500;
commit;

drop procedure grhum.inst_patch_jefy_inv_1500;

