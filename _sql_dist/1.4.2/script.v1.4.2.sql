create table jefy_inventaire.cle_inventaire_compt_modif (
  clicm_id         number not null,
  clic_id          number not null,
  utl_ordre        number not null,
  clicm_duree_old  number,
  clicm_duree_new  number,
  clicm_date       date not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_inventaire.cle_inventaire_compt_modif is 'table des modifications des durees d''amortissement de l''inventaire (utile pour recalcul)';
COMMENT ON COLUMN jefy_inventaire.cle_inventaire_compt_modif.clicm_id is 'Cle';
COMMENT ON COLUMN jefy_inventaire.cle_inventaire_compt_modif.clic_id is 'Cle etrangere table jefy_inventaire.cle_inventaire_comptable';
COMMENT ON COLUMN jefy_inventaire.cle_inventaire_compt_modif.utl_ordre is 'Cle etrangere table jefy_admin.utilisateur';
COMMENT ON COLUMN jefy_inventaire.cle_inventaire_compt_modif.clicm_duree_old is 'Ancienne duree';
COMMENT ON COLUMN jefy_inventaire.cle_inventaire_compt_modif.clicm_duree_new is 'Nouvelle duree';
COMMENT ON COLUMN jefy_inventaire.cle_inventaire_compt_modif.clicm_date is 'Date de la modification';

CREATE UNIQUE INDEX jefy_inventaire.PK_cle_inv_compt_modif ON jefy_inventaire.cle_inventaire_compt_modif (clicm_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_inventaire.cle_inventaire_compt_modif ADD (CONSTRAINT PK_cle_inv_compt_modif PRIMARY KEY (clicm_id));

ALTER TABLE jefy_inventaire.cle_inventaire_compt_modif ADD ( CONSTRAINT FK_cle_inv_compt_m_clic_id FOREIGN KEY (clic_id) 
    REFERENCES jefy_inventaire.cle_inventaire_comptable(clic_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_inventaire.cle_inventaire_compt_modif ADD ( CONSTRAINT FK_cle_inv_compt_m_utl_ordre FOREIGN KEY (utl_ordre) 
    REFERENCES jefy_admin.utilisateur(utl_ordre) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_inventaire.cle_inventaire_compt_modif_seq start with 1 nocycle nocache;



create table jefy_inventaire.inventaire_cb (
  incb_id          number not null,
  inv_id           number not null,
  incb_code        varchar2(50) not null, 
  incb_commentaire varchar2(200))
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_inventaire.inventaire_cb is 'table des code barre associes a l''inventaire physique';
COMMENT ON COLUMN jefy_inventaire.inventaire_cb.incb_id is 'Cle';
COMMENT ON COLUMN jefy_inventaire.inventaire_cb.inv_id  is 'Cle etrangere table jefy_inventaire.inventaire';
COMMENT ON COLUMN jefy_inventaire.inventaire_cb.incb_code is 'Code';
COMMENT ON COLUMN jefy_inventaire.inventaire_cb.incb_commentaire is 'Commentaire';

CREATE UNIQUE INDEX jefy_inventaire.PK_inventaire_cb ON jefy_inventaire.inventaire_cb (incb_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_inventaire.inventaire_cb ADD (CONSTRAINT PK_inventaire_cb PRIMARY KEY (incb_id));

ALTER TABLE jefy_inventaire.inventaire_cb ADD ( CONSTRAINT FK_inventaire_inv_id FOREIGN KEY (inv_id) 
    REFERENCES jefy_inventaire.inventaire(inv_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_inventaire.inventaire_cb_seq start with 1 nocycle nocache;




ALTER TABLE JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE MODIFY(CLIC_CR VARCHAR2(50));

create table jefy_inventaire.degressif (
  dgrf_id          number not null,
  dgrf_date_debut  date not null,
  dgrf_date_fin    date)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_inventaire.degressif is 'table des periodes des coefficients d''amortissement degressif';
COMMENT ON COLUMN jefy_inventaire.degressif.dgrf_id is 'Cle';
COMMENT ON COLUMN jefy_inventaire.degressif.dgrf_date_debut is 'Date de debut de la periode';
COMMENT ON COLUMN jefy_inventaire.degressif.dgrf_date_fin is 'Date de fin de la periode';

CREATE UNIQUE INDEX jefy_inventaire.PK_degressif ON jefy_inventaire.degressif (dgrf_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_inventaire.degressif ADD (CONSTRAINT PK_degressif PRIMARY KEY (dgrf_id));

create sequence jefy_inventaire.degressif_seq start with 1 nocycle nocache;


create table jefy_inventaire.degressif_coef (
  dgco_id          number not null,
  dgrf_id          number not null,
  dgco_duree_min   number not null, 
  dgco_duree_max   number not null, 
  dgco_coef        number(12,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_inventaire.degressif_coef is 'table des taux de coefficients d''amortissement degressif';
COMMENT ON COLUMN jefy_inventaire.degressif_coef.dgco_id is 'Cle';
COMMENT ON COLUMN jefy_inventaire.degressif_coef.dgrf_id  is 'Cle etrangere table jefy_inventaire.degressif';
COMMENT ON COLUMN jefy_inventaire.degressif_coef.dgco_duree_min is 'Duree minimum d''amortissement concernee';
COMMENT ON COLUMN jefy_inventaire.degressif_coef.dgco_duree_max is 'Duree maximum d''amortissement concernee';
COMMENT ON COLUMN jefy_inventaire.degressif_coef.dgco_coef is 'Coefficient';

CREATE UNIQUE INDEX jefy_inventaire.PK_degressif_coef ON jefy_inventaire.degressif_coef (dgco_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_inventaire.degressif_coef ADD (CONSTRAINT PK_degressif_coef PRIMARY KEY (dgco_id));

ALTER TABLE jefy_inventaire.degressif_coef ADD ( CONSTRAINT FK_degressif_dgrf_id FOREIGN KEY (dgrf_id) 
    REFERENCES jefy_inventaire.degressif(dgrf_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_inventaire.degressif_coef_seq start with 1 nocycle nocache;



ALTER TABLE JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE ADD (DGCO_ID  NUMBER);

ALTER TABLE jefy_inventaire.inventaire_comptable ADD (CONSTRAINT FK_invc_dgco_id FOREIGN KEY (dgco_id) 
    REFERENCES jefy_inventaire.degressif_coef(dgco_id) DEFERRABLE INITIALLY DEFERRED);

CREATE OR REPLACE PACKAGE "INVENTAIRE_NUMERO" AS

/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org

function get_numero_physique (clipid integer) return varchar;

function get_numero_comptable (a01exer varchar,a02comp varchar,a03cr varchar) return varchar;

function get_numero_complet (a01exer varchar,a02comp varchar,a03cr varchar, a04numero varchar) return varchar;

-- permet de corrige les imputations des numeros 
-- d'inventaire avec le compte de la liquidation : reimputation , modif a la liquidation (old)
procedure controle_pcoliq_pcoinv;
END;
/

CREATE OR REPLACE PACKAGE BODY "INVENTAIRE_NUMERO" AS

function get_numero_physique (clipid integer) return varchar is
  cpt integer;
  numero jefy_inventaire.cle_inventaire_physique%rowtype;
  clipnumero JEFY_INVENTAIRE.CLE_INVENTAIRE_physique.CLIp_NUMERO%type;
begin 

  select count(*) into cpt from JEFY_INVENTAIRE.CLE_INVENTAIRE_physique where clip_id=clipid;

  if cpt = 0 then
     return 'ko';
  else
     select * into numero from JEFY_INVENTAIRE.CLE_INVENTAIRE_physique where clip_id = clipid;

     -- recup du dernier numero pour cette composante , ce CR et cette exerice
     select max(clip_numero) into clipnumero from JEFY_INVENTAIRE.CLE_INVENTAIRE_physique
        where exe_ordre = numero.exe_ordre and clip_numero is null;
     -- incremente le numero d inventaire
     update JEFY_INVENTAIRE.CLE_INVENTAIRE_physique set clip_numero = clipnumero+1 where clip_id = clipid;

     return 'ok';
  end if;
end;


--function get_numero_comptable (clicid integer)
function get_numero_comptable (a01exer varchar,a02comp varchar,a03cr varchar) return varchar is
  cpt integer;
  numero jefy_inventaire.cle_inventaire_comptable%rowtype;
  clicnumero JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE.CLIC_NUMERO%type;
begin 
  -- corection auto si necessaire
  INVENTAIRE_NUMERO.controle_pcoliq_pcoinv;

  select count(*) into cpt from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE
     where clic_comp =a02comp and clic_cr = a03cr and exe_ordre =a01exer;

  if cpt = 0 then
     return '1';
  else
     update JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE set clic_numero = null where clic_comp =a02comp
        and clic_cr = a03cr and exe_ordre =a01exer and clic_numero = 'automatique';

     -- recup du dernier numero pour cette composante , ce CR et cette exerice
     select max(to_number(clic_numero))+1 into clicnumero from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE
        where clic_comp =a02comp and clic_cr = a03cr and exe_ordre =a01exer;

     return clicnumero||'';
  end if;
end;

function get_numero_complet (a01exer varchar,a02comp varchar,a03cr varchar, a04numero varchar) return varchar  is
begin
   return a02comp||'/'||a03cr||'/'||a01exer||'/'||a04numero; 
end;


procedure controle_pcoliq_pcoinv
is 
  leclicid integer;
  pcoinv varchar(20);
  pcodepense varchar(20);

  cursor c1 is select cic.clic_id,cic.PCO_NUM ,dc.PCO_NUM
    from inventaire_comptable ic , jefy_depense.DEPENSE_CTRL_PLANCO dc, cle_inventaire_comptable cic
    where ic.dpco_id = dc.dpco_id and cic.clic_id = ic.clic_id and cic.PCO_NUM != dc.PCO_NUM;
begin
  open c1;
  loop
     fetch c1 into leclicid,pcoinv,pcodepense;
     exit when c1%notfound;

     update cle_inventaire_comptable set PCO_NUM = pcodepense where clic_id = leclicid;
  end loop;
  close c1;
end;

END;
/

CREATE OR REPLACE PACKAGE jefy_inventaire.API_COROSSOL AS

/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 
 PROCEDURE creer_lien_inv_dep(
   dpcoid  inventaire_comptable.dpco_id%type,
   invcid  inventaire_comptable.invc_id%type,
   montant inventaire_comptable.invc_montant_acquisition%type);

 PROCEDURE supprimer_lien_inv_dep(
   dpcoid inventaire_comptable.dpco_id%type);

 PROCEDURE amortissement(
   invcid inventaire_comptable.invc_id%type);
 
 PROCEDURE AMORTISSEMENT_CLE(
   a01clicid cle_inventaire_comptable.clic_id%type,
   a02annule integer);
 
 -- private
 PROCEDURE amort_lineaire(
   invcid   inventaire_comptable.invc_id%type,
   montant  inventaire_comptable.invc_montant_acquisition%type,
   duree    cle_inventaire_comptable.clic_duree_amort%type,
   pco      cle_inventaire_comptable.pco_num%type,
   pcoamort jefy_inventaire.amortissement.pco_num%type,
   exeordre inventaire_comptable.exe_ordre%type,
   prorata  cle_inventaire_comptable.clic_pro_rata%type,
   la_date  inventaire_comptable.invc_date_acquisition%type);
 
 PROCEDURE amort_degressif(
   invcid   inventaire_comptable.invc_id%type,
   montant  inventaire_comptable.invc_montant_acquisition%type,
   duree    cle_inventaire_comptable.clic_duree_amort%type,
   pco      cle_inventaire_comptable.pco_num%type,
   pcoamort jefy_inventaire.amortissement.pco_num%type,
   exeordre inventaire_comptable.exe_ordre%type,
   prorata  cle_inventaire_comptable.clic_pro_rata%type,
   coef_fiscal degressif_coef.dgco_coef%type,
   la_date     inventaire_comptable.invc_date_acquisition%type);
 
 PROCEDURE amort_vider(
   invcid inventaire_comptable.invc_id%type);
 
 function taux_premiere_annee(
   a_exe_ordre inventaire_comptable.exe_ordre%type,
   a_date      inventaire_comptable.invc_date_acquisition%type) return number;
   
 function get_nb_decimales(a_exe_ordre inventaire_comptable.exe_ordre%type)
     return jefy_admin.devise.dev_nb_decimales%type;   
END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE.API_COROSSOL AS

PROCEDURE creer_lien_inv_dep (
   dpcoid  inventaire_comptable.dpco_id%type, 
   invcid  inventaire_comptable.invc_id%type,
   montant inventaire_comptable.invc_montant_acquisition%type
) IS 
  depidrev integer;
  orv number(12,2);
  pco      maracuja.plan_comptable.PCO_NUM%type;
  clicid   JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.clic_id%type;
  exeordre JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.exe_ordre%type;
BEGIN
  orv := 0;

  -- ORV
  if montant<0 then
    -- creation de l'enregistrment dans INVENTAIRE_COMPTABLE_ORV
    insert into INVENTAIRE_COMPTABLE_ORV values (invcid,dpcoid,INVENTAIRE_COMPTABLE_ORV_seq.nextval,montant);
  else

    if (dpcoid != -1000) then -- cas des integrations HB
       -- recup du compte d'imputation de la liquidation
       select pco_num into pco from jefy_depense.DEPENSE_CTRL_PLANCO where dpco_id = dpcoid;
    end if;

    -- recup du numero comptable
    select clic_id,EXE_ORDRE into clicid,exeordre from JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE where invc_id = invcid;

    if (dpcoid != -1000) then
      -- Correction CH  maj du compte d'amortissement et de la dur?e 
      update JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE set pco_num = pco,
         PCOA_ID = substr(integration.get_pcoaidduree(exeordre, pco),1, instr(integration.get_pcoaidduree(exeordre, pco),'$')-1),
         CLIC_DUREE_AMORT = substr(integration.get_pcoaidduree(exeordre, pco),instr(integration.get_pcoaidduree(exeordre, pco),'$')+1)
        where clic_id = clicid;
    end if;

    -- maj inventaire_comptable : creation su liens
    UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id = dpcoid, invc_montant_acquisition=montant + nvl(orv,0),
      invc_montant_residuel = montant+ nvl(orv,0) WHERE invc_id = invcid;
  end if;

  -- CREATION DE L AMORTISSEMENT
  amortissement(invcid);
END;
 
PROCEDURE supprimer_lien_inv_dep (dpcoid inventaire_comptable.dpco_id%type) IS 
   cursor c1 is select invc_id from inventaire_comptable where dpco_id=dpcoid;
   invcid jefy_inventaire.inventaire_comptable.invc_id%type;
BEGIN
  -- on traite les ORVS 
  delete from jefy_inventaire.INVENTAIRE_COMPTABLE_ORV where dpco_id=dpcoid;
 
  open c1;
  loop
    fetch c1 into invcid;
    exit when c1%notfound;
    -- maj  inventaire_comptable : casse le lien
    UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id=NULL WHERE dpco_id=dpcoid and invc_id=invcid;
    -- SUPPRESSION DE L AMORTISSEMENT
    amort_vider(invcid);
  end loop;
  close c1;
END;

PROCEDURE AMORTISSEMENT_CLE (
   a01clicid cle_inventaire_comptable.clic_id%type,
   a02annule integer
) is
  cursor c1 is select invc_id from inventaire_comptable where clic_id=a01clicid;
  invcid jefy_inventaire.inventaire_comptable.invc_id%type;
begin

  if a02annule = 1 then
    open c1;
    loop
      fetch c1 into invcid ;
      exit when c1%notfound;
      API_COROSSOL.amort_vider(invcid);
    end loop;
    close c1;
  else
    open c1;
    loop
      fetch c1 into invcid ;
      exit when c1%notfound;
      amort_vider(invcid);
      amortissement(invcid);
    end loop;
    close c1;
  end if;
end;

PROCEDURE amortissement(
   invcid inventaire_comptable.invc_id%type
) IS
  cpt INTEGER;
  tmpinvComptable jefy_inventaire.INVENTAIRE_COMPTABLE%ROWTYPE;
  tmpCleComptable jefy_inventaire.CLE_INVENTAIRE_COMPTABLE%ROWTYPE;
  tmpplancoamort  maracuja.plan_comptable_amo%ROWTYPE;
  montant_orvs    jefy_inventaire.INVENTAIRE_COMPTABLE_ORV.INVO_MONTANT_ORV%type;
  my_dgco_coef    degressif_coef.dgco_coef%type;
  my_date_calcul  jefy_depense.depense_papier.dpp_date_service_fait%type;
  my_dgco_id      degressif_coef.dgco_id%type;
BEGIN
  -- recup des infos du numero comptable
  SELECT COUNT(*) INTO cpt FROM jefy_inventaire.INVENTAIRE_COMPTABLE WHERE invc_id = invcid ;
  IF cpt = 0 THEN
    RAISE_APPLICATION_ERROR (-20001,'inventaire comptable introuvable, invc_id='||invcid);
  END IF;
  
  SELECT * INTO tmpinvComptable FROM jefy_inventaire.INVENTAIRE_COMPTABLE WHERE invc_id = invcid ;
  SELECT nvl(sum(INVO_MONTANT_ORV),0)  INTO montant_orvs FROM jefy_inventaire.INVENTAIRE_COMPTABLE_ORV WHERE invc_id = invcid ;
  SELECT * INTO tmpCleComptable FROM jefy_inventaire.CLE_INVENTAIRE_COMPTABLE WHERE clic_id = tmpinvComptable.clic_id ;

  SELECT COUNT(*) INTO cpt FROM maracuja.plan_comptable_amo WHERE pcoa_id = tmpCleComptable.pcoa_id;
  IF cpt = 0 THEN
    raise_application_error (-20001,'compte d amortissement inexistant verifier votre parametrage dans maracuja '||tmpclecomptable.pcoa_id);
  end if;
  
  SELECT * INTO tmpplancoamort FROM maracuja.plan_comptable_amo WHERE pcoa_id = tmpCleComptable.pcoa_id;

  my_dgco_coef:=null;
  my_dgco_id:=tmpinvComptable.dgco_id;
  if tmpinvComptable.dgco_id is null then
     select dpp_date_service_fait into my_date_calcul from jefy_depense.depense_papier p, jefy_depense.depense_budget b, jefy_depense.depense_ctrl_planco pc
       where b.dep_id=pc.dep_id and pc.dpco_id=tmpinvcomptable.dpco_id and b.dpp_id=p.dpp_id;
       
     select max(dgco_id) into my_dgco_id from degressif_coef where 
        dgrf_id in (select dgrf_id from degressif where dgrf_date_debut<=my_date_calcul and (dgrf_date_fin>=my_date_calcul or dgrf_date_fin is null))
        and dgco_duree_min<=tmpCleComptable.clic_duree_amort and dgco_duree_max>=tmpCleComptable.clic_duree_amort;
  end if;
  select dgco_coef into my_dgco_coef from degressif_coef where dgco_id=my_dgco_id;
  
  IF tmpCleComptable.clic_type_amort = 'Lineaire' THEN
    Api_Corossol.amort_lineaire(invcid,tmpinvComptable.invc_montant_acquisition+montant_orvs,tmpCleComptable.clic_duree_amort,
         tmpCleComptable.pco_num,tmpplancoamort.pcoa_num,tmpinvComptable.exe_ordre, tmpCleComptable.clic_pro_rata,my_date_calcul);
  ELSE
    Api_Corossol.amort_degressif(invcid,tmpinvComptable.invc_montant_acquisition+montant_orvs,tmpCleComptable.clic_duree_amort,
       tmpCleComptable.pco_num,tmpplancoamort.pcoa_num,tmpinvComptable.exe_ordre, tmpCleComptable.clic_pro_rata,my_dgco_coef, my_date_calcul);
  END IF;
END;

PROCEDURE amort_lineaire(
   invcid   inventaire_comptable.invc_id%type,
   montant  inventaire_comptable.invc_montant_acquisition%type,
   duree    cle_inventaire_comptable.clic_duree_amort%type,
   pco      cle_inventaire_comptable.pco_num%type,
   pcoamort jefy_inventaire.amortissement.pco_num%type,
   exeordre inventaire_comptable.exe_ordre%type,
   prorata  cle_inventaire_comptable.clic_pro_rata%type,
   la_date  inventaire_comptable.invc_date_acquisition%type
) IS 
  cle         jefy_inventaire.amortissement.amo_id%type;
  annuite     jefy_inventaire.amortissement.amo_annuite%type;
  annuite_ref jefy_inventaire.amortissement.amo_annuite%type;
  cumul       jefy_inventaire.amortissement.amo_cumul%type;
  residuel    jefy_inventaire.amortissement.amo_residuel%type;
  iter        integer;
  my_nb       integer;
  my_nb_decimales  NUMBER;

  cursor c1 is select s.* from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid and s.invs_vnc>0;
  my_inventaire_sortie jefy_inventaire.inventaire_sortie%rowtype;
  exe_ordre_sortie     jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_amor       jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_debut      jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_fin        jefy_inventaire.amortissement.exe_ordre%type;
  my_amortissement     jefy_inventaire.amortissement.amo_annuite%type;
  my_residuel          jefy_inventaire.amortissement.amo_residuel%type;
  my_cumul             jefy_inventaire.amortissement.amo_cumul%type;
  my_acquisition       jefy_inventaire.amortissement.amo_acquisition%type;
BEGIN

  if prorata<>0 and la_date is null then
     raise_application_error (-20001,'Pour un amortissement prorata temporis il faut indiquer une date de debut d''amortissement.');  
  end if;


  amort_vider(invcid);

  my_nb_decimales:=get_nb_decimales(exeordre);

  iter :=0;
  cumul:=0;
  residuel:=montant;
  annuite_ref:=montant/duree;

  LOOP
    IF montant=0 or duree=0 or residuel=0 THEN EXIT; END IF;
    
    iter := iter +1;

    -- si on a laisse un amortissement c'est qu'on ne veut pas le recalculer pour cette annee la        
    select count(*) into my_nb from amortissement where invc_id=invcid and exe_ordre=exeordre+iter;
    if my_nb=0 then
       -- calcul des valeurs
       annuite:=annuite_ref;
       if prorata<>0 and iter=1 then
          annuite:=annuite*taux_premiere_annee(exeordre, la_date);
       end if;

       annuite:=round(annuite, my_nb_decimales);

       if annuite>residuel or (prorata=0 and iter=duree) OR (prorata<>0 and iter=duree+1) then annuite:=residuel; end if;
       if annuite<0 then annuite:=0; end if;

       cumul:=cumul+annuite;
       residuel:=montant-cumul;
    
       -- insertion
       select amortissement_seq.nextval into cle from dual;
       if prorata=0 then
       insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
          values (montant,annuite,cumul ,sysdate,cle,residuel,exeordre+iter,invcid,pcoamort );
       else
         insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
           values (montant,annuite,cumul ,sysdate,cle,residuel,exeordre+iter-1,invcid,pcoamort );
       end if;
    else
       select amo_annuite into annuite from amortissement where invc_id=invcid and exe_ordre=exeordre+iter; 
       cumul:=cumul+annuite;
       residuel:=montant-cumul;    
    end if;          

    -- test (attention : pour l'amortissement lineaire avec prorata temporis, on amortie sur duree+1 !!!!!!!
    IF (prorata=0 and iter=duree) OR (prorata<>0 and iter=duree+1) THEN
      UPDATE AMORTISSEMENT SET AMO_RESIDUEL=0, AMO_CUMUL=AMO_CUMUL+AMO_RESIDUEL, AMO_ANNUITE=AMO_ANNUITE+AMO_RESIDUEL WHERE AMO_ID=cle;
      EXIT;
    END IF;
  END LOOP;
  
  -- gerer sorties inventaire
  select count(*) into my_nb from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid;
  if my_nb>0 then
  
    exe_ordre_fin:=exeordre+duree;
    select min(exe_ordre) into exe_ordre_debut from amortissement where invc_id=invcid;

    open c1;
    loop
      fetch c1 into my_inventaire_sortie;
      exit when c1%notfound;
      
      exe_ordre_sortie:=to_number(to_char(my_inventaire_sortie.invs_date,'yyyy'));
      if exe_ordre_sortie<exe_ordre_debut then
         exe_ordre_sortie:=exe_ordre_debut;
      end if;
      exe_ordre_amor:=exe_ordre_sortie;
      
      LOOP
        IF exe_ordre_amor>exe_ordre_fin THEN EXIT; END IF;

        -- exercice de la sortie
        if exe_ordre_amor=exe_ordre_sortie then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           my_amortissement:=my_inventaire_sortie.invs_vnc;
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
           end if;
        end if;
      
        -- annees suivantes
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor<exe_ordre_fin then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
                        
           my_amortissement:=round(my_inventaire_sortie.invs_vnc/(exe_ordre_fin-exe_ordre_sortie), my_nb_decimales);        
                     
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
              select amo_annuite into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
              my_amortissement:=my_residuel-my_amortissement;
           end if;
           
           my_amortissement:=-my_amortissement;
        end if;

        -- derniere annee
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor=exe_ordre_fin then
           select nvl(sum(amo_annuite),0) into my_cumul from amortissement where exe_ordre<exe_ordre_fin and invc_id=invcid;
           select amo_acquisition, amo_annuite into my_acquisition, my_residuel from amortissement where exe_ordre=exe_ordre_fin and invc_id=invcid;
           my_amortissement:=my_acquisition-my_cumul-my_residuel;
        end if;

        if exe_ordre_amor>exe_ordre_sortie then
           select amo_cumul, amo_residuel into my_cumul, my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
           
           update amortissement set amo_cumul=my_cumul+amo_annuite, amo_residuel=my_residuel-amo_annuite 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;

        if my_amortissement<>0 then
          update amortissement
            set amo_annuite=amo_annuite+my_amortissement, amo_cumul=amo_cumul+my_amortissement, amo_residuel=amo_residuel-my_amortissement, amo_date=sysdate 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;
        
        exe_ordre_amor:=exe_ordre_amor+1;
      end loop;
    
    end loop;
    close c1;
  end if;
  
END;
 
PROCEDURE amort_degressif(
   invcid      inventaire_comptable.invc_id%type,
   montant     inventaire_comptable.invc_montant_acquisition%type,
   duree       cle_inventaire_comptable.clic_duree_amort%type,
   pco         cle_inventaire_comptable.pco_num%type,
   pcoamort    jefy_inventaire.amortissement.pco_num%type,
   exeordre    inventaire_comptable.exe_ordre%type,
   prorata     cle_inventaire_comptable.clic_pro_rata%type,
   coef_fiscal degressif_coef.dgco_coef%type,
   la_date     inventaire_comptable.invc_date_acquisition%type
) IS
  cle         jefy_inventaire.amortissement.amo_id%type;
  annuite     jefy_inventaire.amortissement.amo_annuite%type;
  cumul       jefy_inventaire.amortissement.amo_cumul%type;
  residuel    jefy_inventaire.amortissement.amo_residuel%type;
  iter        integer;
  my_nb       integer;
  my_nb_decimales  NUMBER;
  
  cursor c1 is select s.* from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid and s.invs_vnc>0;
  my_inventaire_sortie jefy_inventaire.inventaire_sortie%rowtype;
  exe_ordre_sortie     jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_amor       jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_debut      jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_fin        jefy_inventaire.amortissement.exe_ordre%type;
  my_amortissement     jefy_inventaire.amortissement.amo_annuite%type;
  my_residuel          jefy_inventaire.amortissement.amo_residuel%type;
  my_cumul             jefy_inventaire.amortissement.amo_cumul%type;
  my_acquisition       jefy_inventaire.amortissement.amo_acquisition%type;
BEGIN
  if prorata<>0 and la_date is null then
     raise_application_error (-20001,'Pour un amortissement prorata temporis il faut indiquer une date de debut d''amortissement.');  
  end if;

  amort_vider(invcid);
  
  my_nb_decimales:=get_nb_decimales(exeordre);
  
  iter :=0;
  cumul:=0;
  residuel:=montant;
  
  loop
    if montant=0 or duree=0 or residuel=0 then exit; end if;
    
    iter:=iter+1;
    
    -- calcul des valeurs
    dbms_output.put_line(duree||'+1-'||iter);
    if duree+1-iter=0 then
       annuite:=residuel;
    else
       if coef_fiscal*100.0/duree > 100/(duree+1-iter) then
          annuite:=residuel*(coef_fiscal/duree);
       else
          annuite:=residuel/(duree+1-iter);
       end if;
    end if;
 
    if prorata<>0 and iter=1 then
       annuite:=annuite*taux_premiere_annee(exeordre, la_date);
    end if;
    
    annuite:=round(annuite, my_nb_decimales);
    
    if annuite>residuel or duree=iter then annuite:=residuel; end if;
    if annuite<0 then annuite:=0; end if;
    
    select count(*) into my_nb from amortissement where invc_id=invcid and exe_ordre=exeordre+iter;
    if my_nb=0 then
       cumul:=cumul+annuite;
       residuel:=montant-cumul;
    
       -- insertion
       select amortissement_seq.nextval into cle from dual;
       if prorata=0 then
          insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
              values (montant, annuite, cumul, sysdate, cle, residuel, exeordre+iter, invcid, pcoamort);
       else
          insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
              values (montant, annuite, cumul, sysdate, cle, residuel, exeordre+iter-1, invcid, pcoamort);
       end if;
    else
       select amo_annuite into annuite from amortissement where invc_id=invcid and exe_ordre=exeordre+iter; 
       cumul:=cumul+annuite;
       residuel:=montant-cumul;    
    end if;
  end loop;
  
  -- gerer sorties inventaire
  select count(*) into my_nb from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid;
  if my_nb>0 then
  
    exe_ordre_fin:=exeordre+duree;
    select min(exe_ordre) into exe_ordre_debut from amortissement where invc_id=invcid;

    open c1;
    loop
      fetch c1 into my_inventaire_sortie;
      exit when c1%notfound;
      
      exe_ordre_sortie:=to_number(to_char(my_inventaire_sortie.invs_date,'yyyy'));
      if exe_ordre_sortie<exe_ordre_debut then
         exe_ordre_sortie:=exe_ordre_debut;
      end if;
      exe_ordre_amor:=exe_ordre_sortie;
      
      LOOP
        IF exe_ordre_amor>exe_ordre_fin THEN EXIT; END IF;

        -- exercice de la sortie
        if exe_ordre_amor=exe_ordre_sortie then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           my_amortissement:=my_inventaire_sortie.invs_vnc;
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
           end if;
        end if;
      
        -- annees suivantes
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor<exe_ordre_fin then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;

           iter:=exe_ordre_amor-exeordre;
              
           if coef_fiscal*100.0/duree > 100/(duree+1-iter) then
              my_amortissement:=residuel*(coef_fiscal/duree);
           else
              my_amortissement:=residuel/(duree+1-iter);
           end if;
                        
           my_amortissement:=round(my_amortissement, my_nb_decimales);        
                     
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
              select amo_annuite into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
              my_amortissement:=my_residuel-my_amortissement;
           end if;
           
           my_amortissement:=-my_amortissement;
        end if;

        -- derniere annee
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor=exe_ordre_fin then
           select nvl(sum(amo_annuite),0) into my_cumul from amortissement where exe_ordre<exe_ordre_fin and invc_id=invcid;
           select amo_acquisition, amo_annuite into my_acquisition, my_residuel from amortissement where exe_ordre=exe_ordre_fin and invc_id=invcid;
           my_amortissement:=my_acquisition-my_cumul-my_residuel;
        end if;

        if exe_ordre_amor>exe_ordre_sortie then
           select amo_cumul, amo_residuel into my_cumul, my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
           
           update amortissement set amo_cumul=my_cumul+amo_annuite, amo_residuel=my_residuel-amo_annuite 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;

        if my_amortissement<>0 then
          update amortissement
            set amo_annuite=amo_annuite+my_amortissement, amo_cumul=amo_cumul+my_amortissement, amo_residuel=amo_residuel-my_amortissement, amo_date=sysdate 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;
        
        exe_ordre_amor:=exe_ordre_amor+1;
      end loop;
    
    end loop;
    close c1;
  end if;
  
end;
 
PROCEDURE amort_vider(
   invcid inventaire_comptable.invc_id%type
) IS 
  my_nb                   integer;
  my_inventaire_comptable inventaire_comptable%rowtype;
BEGIN
  select * into my_inventaire_comptable from inventaire_comptable where invc_id=invcid;
  select count(*) into my_nb from parametre where par_key='RECALCUL_AMORTISSEMENTS_ANTERIEURS' and exe_ordre>my_inventaire_comptable.exe_ordre and par_value='NON';
  if my_nb>0 then
     DELETE FROM AMORTISSEMENT WHERE invc_id=invcid and exe_ordre>=grhum.en_nombre(to_char(sysdate,'yyyy'));
  else
     DELETE FROM AMORTISSEMENT WHERE invc_id=invcid;
  end if;

END;

function taux_premiere_annee(
   a_exe_ordre inventaire_comptable.exe_ordre%type,
   a_date      inventaire_comptable.invc_date_acquisition%type
) return number is
   annee    inventaire_comptable.exe_ordre%type;
   nb_jours integer;
begin
   -- Test date 
   if a_date is null then return 1.0; end if;
   
   annee:=to_number(to_char(a_date, 'yyyy'));
   if a_exe_ordre<>annee then return 1.0; end if;
   
   -- calcul
   nb_jours:=360 - (to_number(to_char(a_date,'mm'))-1)*30 - (to_number(to_char(a_date,'dd'))-1);
   if nb_jours>360 then nb_jours:=360.0; end if;
   if nb_jours<0 then nb_jours:=0.0; end if;

   return nb_jours/360.0;
end;

FUNCTION get_nb_decimales(a_exe_ordre inventaire_comptable.exe_ordre%type)
     return jefy_admin.devise.dev_nb_decimales%type
   is
     my_nb        integer;
     my_par_value jefy_admin.parametre.par_value%type;
     my_dev       jefy_admin.devise.dev_nb_decimales%type;
   begin
        SELECT COUNT(*) INTO my_nb FROM jefy_admin.PARAMETRE WHERE exe_ordre=a_exe_ordre AND par_key='DEVISE_DEV_CODE';
        IF my_nb=1 THEN
           SELECT par_value INTO my_par_value FROM jefy_admin.PARAMETRE
              WHERE exe_ordre=a_exe_ordre AND par_key='DEVISE_DEV_CODE';

           SELECT COUNT(*) INTO my_nb FROM jefy_admin.devise WHERE dev_code=my_par_value;
           IF my_nb=1 THEN
              SELECT dev_nb_decimales INTO my_dev FROM jefy_admin.devise WHERE dev_code=my_par_value;

              IF my_dev IS NOT NULL THEN
                 return my_dev;
              END IF;
           END IF;
        END IF;

        return 2;
   end;
END;
/

CREATE OR REPLACE PACKAGE JEFY_INVENTAIRE.API_COMPTABLE AS

/*
 * Copyright Cocktail, 2001-2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 
 procedure supprimer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type
);

procedure enregistrer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type,
      a_dgrf_date_debut          degressif.dgrf_date_debut%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type,
      a_chaine_degressif_coef    varchar2
);

procedure set_degressif_date_fin (
      a_dgrf_id                  degressif.dgrf_id%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type
);

 PROCEDURE enregistrer_comptable (
      a_clic_id           in out inventaire_comptable.clic_id%type,
      a_dpco_id                  inventaire_comptable.dpco_id%type,
      a_exe_ordre                inventaire_comptable.exe_ordre%type,
      a_invc_date_acquisition    inventaire_comptable.invc_date_acquisition%type,
      a_invc_etat                inventaire_comptable.invc_etat%type,
      a_invc_id           in out inventaire_comptable.invc_id%type,
      a_invc_montant_acquisition inventaire_comptable.invc_montant_acquisition%type,
      a_invc_montant_residuel    inventaire_comptable.invc_montant_residuel%type,
      a_orgf_id                  inventaire_comptable.orgf_id%type,
      a_utl_ordre                inventaire_comptable.utl_ordre%type,
      a_invc_date_sortie         inventaire_comptable.invc_date_sortie%type,
      a_dgco_id                  inventaire_comptable.dgco_id%type,
      a_chaine_inventaire        varchar2,
      
      a_clic_comp                cle_inventaire_comptable.clic_comp%type,
      a_clic_cr                  cle_inventaire_comptable.clic_cr%type,
      a_clic_duree_amort         cle_inventaire_comptable.clic_duree_amort%type,
      a_clic_etat                cle_inventaire_comptable.clic_etat%type,
      a_clic_nb_etiquette        cle_inventaire_comptable.clic_nb_etiquette%type,
      a_clic_pro_rata            cle_inventaire_comptable.clic_pro_rata%type,
      a_clic_type_amort          cle_inventaire_comptable.clic_type_amort%type,
      a_pco_num                  cle_inventaire_comptable.pco_num%type,
      a_pcoa_id                  cle_inventaire_comptable.pcoa_id%type
 );

END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE.API_comptable AS

 procedure supprimer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type
) is
   my_nb             integer;
begin
     select count(*) into my_nb from inventaire_comptable where dgco_id in 
        (select dgco_id from degressif_coef where dgrf_id=a_dgrf_id);
     if my_nb>0 then
        raise_application_error(-20001,'Suppression impossible, des coefficients sont utilis�s par des inventaires comptables');
     end if;

     delete from degressif_coef where dgrf_id=a_dgrf_id;
     delete from degressif where dgrf_id=a_dgrf_id;
end;

 procedure enregistrer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type,
      a_dgrf_date_debut          degressif.dgrf_date_debut%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type,
      a_chaine_degressif_coef    varchar2
) is
   my_nb             integer;
   my_chaine         varchar2(20000);
   my_dgco_duree_min degressif_coef.dgco_duree_min%type;
   my_dgco_duree_max degressif_coef.dgco_duree_max%type;
   my_dgco_coef      degressif_coef.dgco_coef%type;
   my_dgco_id        degressif_coef.dgco_id%type;
   my_dgrf_id        degressif.dgrf_id%type;
begin
  my_nb:=0;

  if a_dgrf_date_debut is null then
     raise_application_error(-20001,'La date de debut ne doit pas etre nulle !');
  end if;

  if a_dgrf_id is not null then
     -- verifie la non utilisation des coefficients
     select count(*) into my_nb from inventaire_comptable where dgco_id in 
        (select dgco_id from degressif_coef where dgrf_id=a_dgrf_id);
     if my_nb>0 then
        raise_application_error(-20001,'Des coefficients sont utilis�s par des inventaires comptables, on ne peut donc rien modifier');
     end if;
     
     -- verifie les chevauchements de dates
     if a_dgrf_date_fin is null then
        select count(*) into my_nb from degressif where dgrf_id<>a_dgrf_id and (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     else
        select count(*) into my_nb from degressif where dgrf_id<>a_dgrf_id and (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null)
           and (dgrf_date_debut<a_dgrf_date_fin);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     end if;
     
     update degressif set dgrf_date_debut=a_dgrf_date_debut, dgrf_date_fin=a_dgrf_date_fin where dgrf_id=a_dgrf_id;
  else
     -- on cloture la periode pr�c�dente si elle existe
     select count(*) into my_nb from degressif where a_dgrf_date_debut>dgrf_date_debut and dgrf_date_fin is null;
     if my_nb>0 then
        select min(dgrf_id) into my_dgrf_id from degressif where a_dgrf_date_debut>dgrf_date_debut and dgrf_date_fin is null;
        set_degressif_date_fin(my_dgrf_id, to_date(to_char(a_dgrf_date_debut-1,'dd/mm/yyyy'),'dd/mm/yyyy'));
     end if; 
  
     -- verifie les chevauchements de dates
     if a_dgrf_date_fin is null then
        select count(*) into my_nb from degressif where (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     else
        select count(*) into my_nb from degressif where (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null)
           and (dgrf_date_debut<a_dgrf_date_fin);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     end if;

     select degressif_seq.nextval into a_dgrf_id from dual;
     insert into degressif values (a_dgrf_id, a_dgrf_date_debut, a_dgrf_date_fin);
  end if;

  delete from degressif_coef where dgrf_id=a_dgrf_id;
  my_chaine:=a_chaine_degressif_coef;
  loop
      if substr(my_chaine,1,1)='$' then exit; end if;

      -- on recupere la duree min
      select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_dgco_duree_min from dual;
      my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
      -- on recupere la diuree max
      select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_dgco_duree_max from dual;
      my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
      -- on recupere le coef
      select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_dgco_coef from dual;
      my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));

      if my_dgco_duree_min is null then 
         raise_application_error(-20001, 'La duree minimale ne doit pas etre nulle');
      end if;
      if my_dgco_duree_max is null then 
         raise_application_error(-20001, 'La duree maximale ne doit pas etre nulle');
      end if;
      if my_dgco_duree_min>my_dgco_duree_max then 
         raise_application_error(-20001, 'La duree maximale doit etre superieure a la duree minimale');
      end if;

      select count(*) into my_nb from degressif_coef
        where dgrf_id=a_dgrf_id and dgco_duree_min<=my_dgco_duree_max and dgco_duree_max>=my_dgco_duree_min;
      if my_nb>0 then
         raise_application_error(-20001, '2 coefficients ont des periodes qui se chevauchent');
      end if;

      -- insertion dans la base.
      select degressif_coef_seq.nextval into my_dgco_id from dual;
      insert into degressif_coef values (my_dgco_id, a_dgrf_id, my_dgco_duree_min, my_dgco_duree_max, my_dgco_coef);
  end loop;

end;

procedure set_degressif_date_fin (
      a_dgrf_id                  degressif.dgrf_id%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type
) is
  my_degressif     degressif%rowtype;
   my_nb           integer;
begin
  select * into my_degressif from degressif where dgrf_id=a_dgrf_id;
  if my_degressif.dgrf_date_debut>=a_dgrf_date_fin or a_dgrf_date_fin is null then
     raise_application_error(-20001, 'la date de fin doit etre superieure a la date de debut');
  end if;
     
  -- verifier qu'il n'y a pas deux periodes se chevauchant 
  select count(*) into my_nb from degressif where dgrf_id<>a_dgrf_id and (my_degressif.dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null)
     and (dgrf_date_debut<a_dgrf_date_fin);
  if my_nb>0 then
     raise_application_error(-20001,'Probleme de chevauchement de dates !');
  end if;
  
  update degressif set dgrf_date_fin=a_dgrf_date_fin where dgrf_id=a_dgrf_id;
end;


PROCEDURE enregistrer_comptable(   
      a_clic_id           in out inventaire_comptable.clic_id%type,
      a_dpco_id                  inventaire_comptable.dpco_id%type,
      a_exe_ordre                inventaire_comptable.exe_ordre%type,
      a_invc_date_acquisition    inventaire_comptable.invc_date_acquisition%type,
      a_invc_etat                inventaire_comptable.invc_etat%type,
      a_invc_id           in out inventaire_comptable.invc_id%type,
      a_invc_montant_acquisition inventaire_comptable.invc_montant_acquisition%type,
      a_invc_montant_residuel    inventaire_comptable.invc_montant_residuel%type,
      a_orgf_id                  inventaire_comptable.orgf_id%type,
      a_utl_ordre                inventaire_comptable.utl_ordre%type,
      a_invc_date_sortie         inventaire_comptable.invc_date_sortie%type,
      a_dgco_id                  inventaire_comptable.dgco_id%type,
      a_chaine_inventaire        varchar2,
      
      a_clic_comp                cle_inventaire_comptable.clic_comp%type,
      a_clic_cr                  cle_inventaire_comptable.clic_cr%type,
      a_clic_duree_amort         cle_inventaire_comptable.clic_duree_amort%type,
      a_clic_etat                cle_inventaire_comptable.clic_etat%type,
      a_clic_nb_etiquette        cle_inventaire_comptable.clic_nb_etiquette%type,
      a_clic_pro_rata            cle_inventaire_comptable.clic_pro_rata%type,
      a_clic_type_amort          cle_inventaire_comptable.clic_type_amort%type,
      a_pco_num                  cle_inventaire_comptable.pco_num%type,
      a_pcoa_id                  cle_inventaire_comptable.pcoa_id%type
) IS 
  my_nb               integer;
  my_chaine	          varchar2(2000);
  my_inv_id           inventaire.inv_id%type;
  my_clic_numero      cle_inventaire_comptable.clic_numero%type;
  my_cle_inventaire_comptable cle_inventaire_comptable%rowtype;
  my_recalcul_autre   integer;
  my_invc_id          inventaire_comptable.invc_id%type;
  cursor c1 is select invc_id from inventaire_comptable where clic_id=a_clic_id;
BEGIN
  my_nb:=0;
  my_recalcul_autre:=0;
  
  if a_invc_id is not null then
     select count(*) into my_nb from inventaire_comptable where invc_id=a_invc_id;
  end if;
  
  if a_invc_id is null or my_nb=0 then
     if a_clic_id is null then
       if a_clic_comp is null and a_clic_cr is null and a_exe_ordre is null then
          raise_application_error(-20001, 'Vous devez renseigner l''UB, le CR et l''exercice');
       end if;
       
       my_clic_numero:=inventaire_numero.get_numero_comptable(a_exe_ordre, a_clic_comp, a_clic_cr);
       select cle_inventaire_comptable_seq.nextval into a_clic_id from dual;
       
       insert into cle_inventaire_comptable values (a_clic_comp, a_clic_cr, a_clic_duree_amort, a_clic_etat, a_clic_id, a_clic_nb_etiquette,
          my_clic_numero, a_clic_pro_rata, a_clic_type_amort, a_exe_ordre, a_pco_num, a_pcoa_id, 
          inventaire_numero.get_numero_complet(a_exe_ordre, a_clic_comp, a_clic_cr, my_clic_numero));
     else
       select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=a_clic_id;
       if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort then
          insert into cle_inventaire_compt_modif values (cle_inventaire_compt_modif_seq.nextval, a_clic_id, a_utl_ordre, my_cle_inventaire_comptable.clic_duree_amort,
              a_clic_duree_amort, sysdate);
       end if;
       if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort or my_cle_inventaire_comptable.clic_pro_rata<>a_clic_pro_rata or 
          my_cle_inventaire_comptable.clic_type_amort<>a_clic_type_amort then
            my_recalcul_autre:=1;
       end if;
          
       update cle_inventaire_comptable set clic_duree_amort=a_clic_duree_amort, clic_nb_etiquette=a_clic_nb_etiquette,
          clic_type_amort=a_clic_type_amort, clic_pro_rata=a_clic_pro_rata, pco_num=a_pco_num, pcoa_id=a_pcoa_id where clic_id=a_clic_id;
     end if;
  
     select inventaire_comptable_seq.nextval into a_invc_id from dual;
     insert into inventaire_comptable values(a_clic_id, a_dpco_id, a_exe_ordre, a_invc_date_acquisition, a_invc_etat, a_invc_id,
        a_invc_montant_acquisition, a_invc_montant_residuel, a_orgf_id, a_utl_ordre, a_invc_date_sortie, a_dgco_id);
  else
     select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=a_clic_id;
     if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort then
        insert into cle_inventaire_compt_modif values (cle_inventaire_compt_modif_seq.nextval, a_clic_id, a_utl_ordre, my_cle_inventaire_comptable.clic_duree_amort,
            a_clic_duree_amort, sysdate);
     end if;
     if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort or my_cle_inventaire_comptable.clic_pro_rata<>a_clic_pro_rata or 
        my_cle_inventaire_comptable.clic_type_amort<>a_clic_type_amort then
          my_recalcul_autre:=1;
     end if;

     update cle_inventaire_comptable set clic_duree_amort=a_clic_duree_amort, clic_nb_etiquette=a_clic_nb_etiquette,
          clic_type_amort=a_clic_type_amort, clic_pro_rata=a_clic_pro_rata, pco_num=a_pco_num, pcoa_id=a_pcoa_id where clic_id=a_clic_id;
     update inventaire_comptable set orgf_id=a_orgf_id, utl_ordre=a_utl_ordre, dpco_id=a_dpco_id, invc_montant_acquisition=a_invc_montant_acquisition,
        invc_montant_residuel=a_invc_montant_residuel, dgco_id=a_dgco_id where invc_id=a_invc_id;
  end if;
  
  my_chaine:=a_chaine_inventaire;
  loop
     if my_chaine is null or length(my_chaine)=0 or substr(my_chaine,1,1)='$' then exit; end if;

     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_inv_id from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));

     select count(*) into my_nb from inventaire where inv_id=my_inv_id and invc_id is null;
     if my_nb>0 then
        update inventaire set invc_id=a_invc_id where inv_id=my_inv_id and invc_id is null; 
     end if;
  end loop;

  if a_dpco_id is not null then
     api_corossol.AMORTISSEMENT(a_invc_id);
  end if;
  
  if my_recalcul_autre>0 then
    OPEN c1();
      LOOP
      FETCH  c1 INTO my_invc_id;
      EXIT WHEN c1%NOTFOUND;
        if a_invc_id<>my_invc_id then
           api_corossol.AMORTISSEMENT(my_invc_id);
        end if;
      END LOOP;
      CLOSE c1;
  end if;
  
END;
 
/*PROCEDURE supprimer_sortie (
      a_invs_id           inventaire_sortie.invs_id%type
) IS
  my_invc_id inventaire.invc_id%type;
BEGIN
  select i.invc_id into my_invc_id from inventaire i, inventaire_sortie s where s.inv_id=i.inv_id and s.invs_id=a_invs_id;
  delete from inventaire_sortie where invs_id=a_invs_id;

  if my_invc_id is not null then
     api_corossol.AMORTISSEMENT(my_invc_id);
  end if;
END;*/

END;
/

CREATE TABLE jefy_inventaire.DB_VERSION (
  DB_VERSION_ID       NUMBER               NOT NULL,
  DB_VERSION_LIBELLE  VARCHAR2(15)         NOT NULL,
  DB_VERSION_DATE     DATE                 NOT NULL,
  DB_INSTALL_DATE     DATE                 NOT NULL,
  DB_COMMENT          VARCHAR2(100)
)
TABLESPACE GFC NOCACHE NOPARALLEL;

COMMENT ON TABLE DB_VERSION IS 'Table contenant les versions des scripts pass�s';
COMMENT ON COLUMN DB_VERSION.DB_VERSION_ID IS 'Version';
COMMENT ON COLUMN DB_VERSION.DB_VERSION_LIBELLE IS 'Version';
COMMENT ON COLUMN DB_VERSION.DB_VERSION_DATE IS 'Date de la version';
COMMENT ON COLUMN DB_VERSION.DB_INSTALL_DATE IS 'Date de passage du script';
COMMENT ON COLUMN DB_VERSION.DB_COMMENT IS 'Commentaires eventuels';

CREATE UNIQUE INDEX PK_DB_VERSION ON DB_VERSION (DB_VERSION_ID) TABLESPACE GFC_INDX NOPARALLEL;

ALTER TABLE DB_VERSION ADD (CONSTRAINT PK_DB_VERSION PRIMARY KEY (DB_VERSION_ID) USING INDEX TABLESPACE GFC_INDX);

insert into jefy_inventaire.db_version select 1420, '1.4.2.0', to_date('22/04/2010','dd/mm/yyyy'), sysdate, null from dual;

insert into jefy_inventaire.parametre select distinct exe_ordre, 'TYPE_AMORTISSEMENT', 'LINEAIRE', 
  'Type d''amortissement (LINEAIRE/DEGRESSIF), si le champ est vide alors les 2 possibles' from jefy_inventaire.parametre;

insert into jefy_inventaire.parametre select distinct exe_ordre, 'RECALCUL_AMORTISSEMENTS_ANTERIEURS', 'OUI', 
  'Recalcul amortissements anterieurs lors de la modification de la duree' from jefy_inventaire.parametre;

Insert into jefy_inventaire.PARAMETRE select distinct exe_ordre, 'LIBELLE_ETIQUETTE','UNIVERSITE',
  'Libelle affich� en haut de l''etiquette � code barre' from jefy_inventaire.parametre;

update jefy_inventaire.cle_inventaire_comptable set clic_type_amort='Lineaire' where clic_type_amort='Lineraire';

commit;