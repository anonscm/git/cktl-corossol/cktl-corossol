SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°2/3
-- Type : DDL
-- Schéma modifié : JEFY_INVENTAIRE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.5.4
-- Release
-- Date de publication : 20/09/2013
-- Licence : CeCILL version 2
--
-- Notes
--  
--  
--  

--whenever sqlerror exit sql.sqlcode ;

-- On peut rejouer comme on veut le script en décommentant les deux lignes suivantes !
-- delete from JEFY_INVENTAIRE.db_version where db_version_id='1540';

CREATE OR REPLACE VIEW JEFY_INVENTAIRE.V_TITRE_RECETTE AS 
SELECT -- Affiche la liste des titres, avec leurs réductions éventuelles, les montants utilisés et disponibles
  t.tit_id,
  t.pco_num,
  t.exe_ordre,
  t.ges_code,
  t.tit_numero,
  t.bor_id,
  t.tit_libelle,
  t.fou_ordre,
  t.tit_ht,
  t.tit_ttc,
  t.org_ordre,
  jrRec.montant_reduction                                              AS montant_reduction,
  NVL(SUM(ico.icor_montant), 0)                                        AS montant_utilise,
  (t.tit_ht - NVL(SUM(ico.icor_montant), 0) + jrRec.montant_reduction) AS montant_disponible
FROM
  MARACUJA.titre                                                    t
  LEFT JOIN jefy_inventaire.inventaire_comptable_orig               ico
    ON t.tit_id = ico.tit_id,
  ( -- On recherche les recettes (r) et leurs réductions associées (rt) en passant par recette_ctrl_planco
    SELECT
      rcp.tit_id, 
      NVL(SUM(rt.rec_montant_budgetaire), 0) AS montant_reduction
    FROM
      JEFY_RECETTE.recette_ctrl_planco  rcp,
      JEFY_RECETTE.recette              r
      -- Une recette peut avoir des réductions
      LEFT JOIN JEFY_RECETTE.recette    rt
        ON rt.rec_id_reduction = r.rec_id
    WHERE r.rec_id_reduction IS NULL
    AND   r.rec_id           = rcp.rec_id
    GROUP BY rcp.tit_id
  )                                                                 jrRec
WHERE
    t.tit_id   = jrRec.tit_id
AND t.tit_etat = 'VISE'
GROUP BY
    t.tit_id, t.pco_num, t.exe_ordre, t.ges_code, t.tit_numero, t.bor_id, 
    t.tit_libelle, t.fou_ordre, t.tit_ht, t.tit_ttc, t.org_ordre, jrRec.montant_reduction ;


create or replace procedure grhum.inst_patch_jefy_inv_1540 is
begin
  -- Mise à jour de la base de donnée
  insert into jefy_inventaire.db_version values (1540,'1.5.4.0',to_date('20/09/2013','dd/mm/yyyy'),sysdate,null);
end;
/
