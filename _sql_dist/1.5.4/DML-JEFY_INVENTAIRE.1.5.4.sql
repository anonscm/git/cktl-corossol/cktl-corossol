SET DEFINE OFF
--
--  
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°3/3
-- Type : DML
-- Schéma modifié : JEFY_INVENTAIRE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.5.4
-- Date de publication : 20/09/2013
-- Licence : CeCILL version 2
--
-- Evolutions :
--   
--   
--

whenever sqlerror exit sql.sqlcode ;

execute grhum.inst_patch_jefy_inv_1540;
commit;

drop procedure grhum.inst_patch_jefy_inv_1540;
