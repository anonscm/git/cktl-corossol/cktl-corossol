SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/3
-- Type : DBA
-- Schéma modifié : JEFY_INVENTAIRE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.5.4
-- Release
-- Date de publication : 20/09/2013
-- Licence : CeCILL version 2
--
-- Notes
--  
--  
--  

--whenever sqlerror exit sql.sqlcode ;

grant select on jefy_recette.recette to jefy_inventaire;
grant select on jefy_recette.recette_ctrl_planco to jefy_inventaire;
grant select on maracuja.recette to jefy_inventaire;