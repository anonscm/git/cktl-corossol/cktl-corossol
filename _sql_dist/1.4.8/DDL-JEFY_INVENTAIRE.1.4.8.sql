-- Fichier :  no 1/2
-- Type : DDL
-- Schema modifie :  JEFY_INVENTAIRE
-- Schema d'execution du script : GRHUM
-- Numero de version : 1.4.8
-- Date de publication : 27/01/2012
-- Licence : CeCILL version 2

whenever sqlerror exit sql.sqlcode;

CREATE OR REPLACE FORCE VIEW jefy_inventaire.v_actif_inventaire (exercice, ges_code, numero, exer,
    amo_annuite, amo_cumul, amo_residuel, amo_acquisition, pco_num)
AS
   SELECT   /*MIN (*/exercice/*)*/, ges_code, numero, exer, SUM (amo_annuite),
            SUM (amo_cumul), SUM (amo_residuel), SUM (amo_acquisition), pco_num
       FROM (SELECT TO_NUMBER (ee.exe_ordre) exercice, NVL (c.clic_comp,o.org_ub) ges_code,
                    clic_num_complet numero, TO_NUMBER (a.exe_ordre) exer,
                    amo_annuite, amo_cumul, amo_residuel, amo_acquisition, c.pco_num
               FROM amortissement a,
                    inventaire_comptable i,
                    cle_inventaire_comptable c,
                    inventaire_non_budgetaire inb,
                    jefy_admin.organ o,
                    (SELECT   invc_id, MIN (exe_ordre) exe_ordre
                         FROM (SELECT invc_id, TO_NUMBER (exe_ordre) exe_ordre FROM amortissement
                               UNION ALL
                               SELECT i.invc_id, TO_NUMBER (i.exe_ordre)
                                 FROM inventaire_comptable i, cle_inventaire_comptable c
                                WHERE c.clic_id = i.clic_id)
                     GROUP BY invc_id) ee
              WHERE a.invc_id = i.invc_id
                AND i.invc_id = inb.invc_id(+)
                AND inb.org_id = o.org_id(+)
                AND i.clic_id = c.clic_id
                AND ee.invc_id = i.invc_id
             UNION ALL
             SELECT TO_NUMBER (i.exe_ordre) exercice,
                    NVL (c.clic_comp, o.org_ub) ges_code,
                    clic_num_complet numero, c.exe_ordre exer, 0 amo_annuite,
                    0 amo_cumul,
                    DECODE (zz.nb, NULL, 1, 0) * amo_acquisition amo_residuel,
                    DECODE (zz.nb, NULL, 1, 0) * amo_acquisition, c.pco_num
               FROM jefy_inventaire.amortissement a,
                    jefy_inventaire.inventaire_comptable i,
                    jefy_inventaire.cle_inventaire_comptable c,
                    inventaire_non_budgetaire inb,
                    jefy_admin.organ o,
                    (SELECT   invc_id, exe_ordre, COUNT (*) nb FROM jefy_inventaire.amortissement
                     GROUP BY invc_id, exe_ordre) zz
              WHERE a.invc_id = i.invc_id
                AND i.invc_id = inb.invc_id(+)
                AND inb.org_id = o.org_id(+)
                AND i.clic_id = c.clic_id
                AND amo_residuel = amo_acquisition - amo_annuite
                AND zz.exe_ordre(+) = i.exe_ordre
                AND zz.invc_id(+) = i.invc_id
                and i.exe_ordre<=c.exe_ordre
/*union all
select RGP_EXER*1 exercice,RGP_COMP ges_code,RGP_NUMERO numero,AMO_EXER*1 exer,AMO_ANNUITE,AMO_CUMUL,AMO_RESIDUEL,AMO_ACQUISITION,PCO_NUM
from INVENTAIRE.ETAT_ACTIFS_BRUT*/
            )
   GROUP BY exercice,  ges_code, numero, exer, /*amo_acquisition, */ pco_num
   having SUM (amo_acquisition)<>0;


create or replace procedure grhum.inst_patch_jefy_inv_1480 is
 nb integer;
begin
	 insert into jefy_inventaire.db_version values (1480,'1.4.8.0',to_date('27/01/2012','dd/mm/yyyy'),sysdate,null);  
end;
/