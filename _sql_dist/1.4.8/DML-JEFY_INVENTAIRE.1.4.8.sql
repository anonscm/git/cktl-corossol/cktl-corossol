-- Fichier :  no 2/2
-- Type : DML
-- Schema modifie :  JEFY_INVENTAIRE
-- Schema d'execution du script : GRHUM
-- Numero de version : 1.4.8
-- Date de publication : 27/01/2012
-- Licence : CeCILL version 2


whenever sqlerror exit sql.sqlcode ;

execute grhum.inst_patch_jefy_inv_1480;
commit;

drop procedure grhum.inst_patch_jefy_inv_1480;

