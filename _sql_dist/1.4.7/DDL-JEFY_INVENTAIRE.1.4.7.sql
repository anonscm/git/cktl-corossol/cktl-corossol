

CREATE SEQUENCE JEFY_INVENTAIRE.INVENTAIRE_IMP_SEQ  START WITH 1 NOCYCLE NOCACHE NOORDER;

CREATE TABLE JEFY_INVENTAIRE.INVENTAIRE_NON_BUDGETAIRE_COMP
(
  INB_ID                NUMBER,
  INB_NUMERO_INVENTAIRE VARCHAR2(200)
)
TABLESPACE GFC NOCACHE NOPARALLEL MONITORING;

COMMENT ON COLUMN JEFY_INVENTAIRE.INVENTAIRE_NON_BUDGETAIRE_COMP.INB_ID IS 'id interne';
COMMENT ON COLUMN JEFY_INVENTAIRE.INVENTAIRE_NON_BUDGETAIRE_COMP.INB_NUMERO_INVENTAIRE IS 'numero d''inventaire d''origine (import)';

ALTER TABLE JEFY_INVENTAIRE.INVENTAIRE_NON_BUDGETAIRE_COMP ADD (PRIMARY KEY (INB_ID) USING INDEX TABLESPACE GFC);


ALTER TABLE JEFY_INVENTAIRE.INVENTAIRE_IMP ADD (DATE_ACQUISITION  DATE);
Alter TRIGGER JEFY_INVENTAIRE.CONTROL_IMPORTATION disable; 



CREATE OR REPLACE PACKAGE JEFY_INVENTAIRE."INTEGRATION_HB" AS

procedure enregistrer_hb (
   a_INB_ID        in out NUMBER,
   a_UTL_ORDRE            NUMBER,
   a_EXE_ORDRE            NUMBER,
   a_ORG_ID               NUMBER,
   a_PCO_NUM              VARCHAR2,
   a_INB_DUREE            NUMBER,
   a_INB_TYPE_AMORT       VARCHAR2,
   a_INB_MONTANT          NUMBER,
   a_INB_MONTANT_RESIDUEL NUMBER,
   a_ORIG_ID              NUMBER,
   a_INB_NUMERO_SERIE     VARCHAR2,
   a_INB_INFORMATIONS     VARCHAR2,
   a_INB_FOURNISSEUR      VARCHAR2,
   a_INB_FACTURE          VARCHAR2,
   a_INVC_ID              NUMBER,
   a_INB_DATE_ACQUISITION date);

procedure enregistrer_hb_comp (
   a_INB_ID        in out NUMBER,
   a_UTL_ORDRE            NUMBER,
   a_EXE_ORDRE            NUMBER,
   a_ORG_ID               NUMBER,
   a_PCO_NUM              VARCHAR2,
   a_INB_DUREE            NUMBER,
   a_INB_TYPE_AMORT       VARCHAR2,
   a_INB_MONTANT          NUMBER,
   a_INB_MONTANT_RESIDUEL NUMBER,
   a_ORIG_ID              NUMBER,
   a_INB_NUMERO_SERIE     VARCHAR2,
   a_INB_INFORMATIONS     VARCHAR2,
   a_INB_FOURNISSEUR      VARCHAR2,
   a_INB_FACTURE          VARCHAR2,
   a_INVC_ID              NUMBER,
   a_INB_DATE_ACQUISITION date,
   a_inb_numero_inventaire inventaire_non_budgetaire_comp.inb_numero_inventaire%type);

procedure integrer_hb (inbid integer) ;
procedure modifier_hb (inbid integer) ;
procedure supprimer_hb (inbid integer) ;

END INTEGRATION_HB;
/

CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE."INTEGRATION_HB" AS

procedure enregistrer_hb (
   a_INB_ID        in out NUMBER,
   a_UTL_ORDRE            NUMBER,
   a_EXE_ORDRE            NUMBER,
   a_ORG_ID               NUMBER,
   a_PCO_NUM              VARCHAR2,
   a_INB_DUREE            NUMBER,
   a_INB_TYPE_AMORT       VARCHAR2,
   a_INB_MONTANT          NUMBER,
   a_INB_MONTANT_RESIDUEL NUMBER,
   a_ORIG_ID              NUMBER,
   a_INB_NUMERO_SERIE     VARCHAR2,
   a_INB_INFORMATIONS     VARCHAR2,
   a_INB_FOURNISSEUR      VARCHAR2,
   a_INB_FACTURE          VARCHAR2,
   a_INVC_ID              NUMBER,
   a_INB_DATE_ACQUISITION date
) is
begin
   enregistrer_hb_comp(a_INB_ID, a_UTL_ORDRE, a_EXE_ORDRE, a_ORG_ID, a_PCO_NUM, a_INB_DUREE, a_INB_TYPE_AMORT,
   a_INB_MONTANT, a_INB_MONTANT_RESIDUEL, a_ORIG_ID, a_INB_NUMERO_SERIE, a_INB_INFORMATIONS,
   a_INB_FOURNISSEUR, a_INB_FACTURE, a_INVC_ID, a_INB_DATE_ACQUISITION, null);
end;

procedure enregistrer_hb_comp (
   a_INB_ID        in out NUMBER,
   a_UTL_ORDRE            NUMBER,
   a_EXE_ORDRE            NUMBER,
   a_ORG_ID               NUMBER,
   a_PCO_NUM              VARCHAR2,
   a_INB_DUREE            NUMBER,
   a_INB_TYPE_AMORT       VARCHAR2,
   a_INB_MONTANT          NUMBER,
   a_INB_MONTANT_RESIDUEL NUMBER,
   a_ORIG_ID              NUMBER,
   a_INB_NUMERO_SERIE     VARCHAR2,
   a_INB_INFORMATIONS     VARCHAR2,
   a_INB_FOURNISSEUR      VARCHAR2,
   a_INB_FACTURE          VARCHAR2,
   a_INVC_ID              NUMBER,
   a_INB_DATE_ACQUISITION date,
   a_inb_numero_inventaire inventaire_non_budgetaire_comp.inb_numero_inventaire%type
) is
   my_cpt integer;
begin
   if a_inb_id is null then
     select inventaire_non_budgetaire_seq.nextval into a_inb_id from dual;
   end if;

   select count(*) into my_cpt from inventaire_non_budgetaire where inb_id=a_inb_id;
   if my_cpt=0 then
      insert into inventaire_non_budgetaire values (a_inb_id, a_UTL_ORDRE, a_EXE_ORDRE, a_ORG_ID, a_PCO_NUM, a_INB_DUREE, a_INB_TYPE_AMORT,
          a_INB_MONTANT, a_INB_MONTANT_RESIDUEL, a_ORIG_ID, a_INB_NUMERO_SERIE, a_INB_INFORMATIONS, a_INB_FOURNISSEUR, a_INB_FACTURE,
          a_INVC_ID, a_INB_DATE_ACQUISITION);
      
      if a_inb_numero_inventaire is not null then
         insert into inventaire_non_budgetaire_comp values (a_inb_id, a_inb_numero_inventaire);
      end if;
      
      integrer_hb(a_inb_id);   
   else
      update inventaire_non_budgetaire set utl_ordre=a_utl_ordre, pco_num=a_pco_num, inb_duree=a_inb_duree, inb_type_amort=a_inb_type_amort,
          inb_montant=a_inb_montant, inb_montant_residuel=a_inb_montant_residuel, orig_id=a_orig_id, inb_numero_serie=a_inb_numero_serie, 
          inb_informations=a_inb_informations, inb_fournisseur=a_inb_fournisseur, inb_facture=a_inb_facture,
          inb_date_acquisition=a_inb_date_acquisition where inb_id=a_inb_id;

      select count(*) into my_cpt from inventaire_non_budgetaire_comp where inb_id=a_inb_id;
      if my_cpt=0 then
         if a_inb_numero_inventaire is not null then
            insert into inventaire_non_budgetaire_comp values (a_inb_id, a_inb_numero_inventaire);
         end if;
      else
         if a_inb_numero_inventaire is not null then
            update inventaire_non_budgetaire_comp set inb_numero_inventaire=a_inb_numero_inventaire where inb_id=a_inb_id;
         else 
            delete from inventaire_non_budgetaire_comp where inb_id=a_inb_id;
         end if;
      
      end if;

      modifier_hb(a_inb_id);   
   end if;
end;

procedure integrer_hb (inbid integer) is
   currentimp jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE%rowtype;
   CLICID  integer;
   INVCID  integer;
   my_icor_id integer;
   my_nb   integer;
   numero  integer;
   comp    jefy_admin.organ.org_ub%type;
   cr      jefy_admin.organ.org_cr%type;
   exer    jefy_admin.exercice.exe_exercice%type;
   my_clic_num_complet cle_inventaire_comptable.clic_num_complet%type;
   my_prorata_temporis integer;
   my_pcoa_id cle_inventaire_comptable.pcoa_id%type;
BEGIN

   select * into currentimp from jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE where inb_id = inbid;

   -- recup des infoamtions de la ligne budgetaire
   select org_ub,org_cr into comp,cr from jefy_admin.organ where org_id = currentimp.org_id;

   -- recup de lexercice
   select exe_exercice into exer from  jefy_admin.exercice where exe_ordre = currentimp.exe_ordre;

   -- recup prorata temporis ou non
   select decode(nvl(par_value,'NON'),'NON',0,1) into my_prorata_temporis from parametre 
      where par_key='PRORATA_TEMPORIS' and exe_ordre=currentimp.exe_ordre;

   -- insertion dans la table
   my_pcoa_id:=null;
   if currentimp.inb_duree>0 then 
      my_pcoa_id:=jefy_inventaire.INTEGRATION.get_pcoaid(currentimp.exe_ordre,currentimp.pco_num);
   end if;

   clicid:=null;
   
   select count(*) into my_nb from inventaire_non_budgetaire_comp where inb_id=inbid;
   if my_nb=1 then
      select inb_numero_inventaire into my_clic_num_complet from inventaire_non_budgetaire_comp where inb_id=inbid;
      select count(*) into my_nb from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE  where clic_num_complet=my_clic_num_complet;
      if my_nb=1 then
         select clic_id into clicid from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE  where clic_num_complet=my_clic_num_complet;
      end if;
   end if;
     
   if clicid is null then 
      select CLE_INVENTAIRE_COMPTABLE_SEQ.nextval into CLICID from dual;  
      insert into  JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE 
        (CLIC_COMP,CLIC_CR,CLIC_DUREE_AMORT,CLIC_ETAT,CLIC_ID,CLIC_NB_ETIQUETTE,CLIC_NUMERO,CLIC_PRO_RATA,CLIC_TYPE_AMORT,
          EXE_ORDRE,PCO_NUM,PCOA_ID,CLIC_NUM_COMPLET)
        values (comp, cr, currentimp.inb_duree, 'etat', CLICID, 0,'automatique', my_prorata_temporis, currentimp.INB_TYPE_AMORT,
          currentimp.exe_ordre, currentimp.pco_num, my_pcoa_id, null);
   end if;
   
   select count(*) into my_nb from inventaire_non_budgetaire_comp where inb_id=inbid;
   if my_nb=0 then
      numero := inventaire_numero.get_numero_comptable (exer,comp,cr);
      my_clic_num_complet:=inventaire_numero.get_numero_complet(exer,comp,cr,numero);
   else
       select inb_numero_inventaire into my_clic_num_complet from inventaire_non_budgetaire_comp where inb_id=inbid;
       if my_clic_num_complet is null then
          numero := inventaire_numero.get_numero_comptable (exer,comp,cr);
          my_clic_num_complet:=inventaire_numero.get_numero_complet(exer,comp,cr,numero);
       end if;
   end if; 


   -- maj de la cle d'inventaire  comptable
   update JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE set CLIC_NUMERO = nvl(numero,1), clic_num_complet=my_clic_num_complet 
      where CLIC_ID = CLICID;
 
   -- creation de l'inventaire comptable
   -- recup de la clef primaire
   select INVENTAIRE_COMPTABLE_SEQ.nextval into INVCID from dual;

   -- insertion dans la table 
   insert into  JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE 
      (CLIC_ID,DPCO_ID,EXE_ORDRE,INVC_DATE_ACQUISITION,INVC_ETAT,INVC_ID,INVC_MONTANT_ACQUISITION,INVC_MONTANT_RESIDUEL,ORGF_ID,UTL_ORDRE)
     values (CLICID,null, currentimp.exe_ordre, currentimp.inb_date_acquisition, null, INVCID, currentimp.INB_MONTANT, currentimp.INB_MONTANT_RESIDUEL,
       currentimp.ORiG_ID, currentimp.UTL_ORDRE);

   select inventaire_comptable_orig_seq.nextval into my_icor_id from dual;
   insert into jefy_inventaire.inventaire_comptable_orig values (my_icor_id, invcid, currentimp.ORiG_ID, 100, null);
   
   -- creation des amortissments
   if currentimp.inb_duree <> 0 then
      api_corossol.creer_lien_inv_dep (-1000, invcid,currentimp.INB_MONTANT_RESIDUEL);
   end if;

   -- on marque l'enregistrement import? et on conserve le liens avec l'inventaire_comptable
   update INVENTAIRE_NON_BUDGETAIRE set invc_id = invcid where inb_id = inbid;
end;

procedure modifier_hb (inbid integer) is 
   my_inventaire                jefy_inventaire.inventaire_non_budgetaire%rowtype;
   my_inventaire_comptable      jefy_inventaire.inventaire_comptable%rowtype;
   my_cle_inventaire_comptable  jefy_inventaire.cle_inventaire_comptable%rowtype;
   my_pcoa_id                   cle_inventaire_comptable.pcoa_id%type;
begin
   select * into my_inventaire from jefy_inventaire.inventaire_non_budgetaire where inb_id=inbid;
 
   if my_inventaire.invc_id is null then
      integrer_hb(my_inventaire.inb_id);
   else
       select * into my_inventaire_comptable from inventaire_comptable where invc_id=my_inventaire.invc_id;
       select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=my_inventaire_comptable.clic_id;
       
       my_pcoa_id:=null;
       if my_inventaire.inb_duree>0 then 
         my_pcoa_id:=jefy_inventaire.INTEGRATION.get_pcoaid(my_inventaire.exe_ordre, my_inventaire.pco_num);
       end if;

       api_comptable.enregistrer_comptable(my_inventaire_comptable.clic_id, my_inventaire_comptable.dpco_id, my_inventaire.exe_ordre, my_inventaire.inb_date_acquisition,
         null, my_inventaire.invc_id, my_inventaire.inb_montant_residuel, my_inventaire.inb_montant_residuel, my_inventaire.orig_id||'$100$$$', my_inventaire.utl_ordre,
         my_inventaire_comptable.invc_date_sortie, null,null, my_cle_inventaire_comptable.clic_comp, my_cle_inventaire_comptable.clic_cr,
         my_inventaire.inb_duree, my_cle_inventaire_comptable.clic_etat, my_cle_inventaire_comptable.clic_nb_etiquette, my_cle_inventaire_comptable.clic_pro_rata,
         my_inventaire.inb_type_amort, my_inventaire.pco_num, my_pcoa_id);     
/*
      -- maj  inventaire_comptable : casse le lien
      update jefy_inventaire.inventaire_comptable set invc_montant_acquisition=my_inventaire.inb_montant_residuel, 
         invc_montant_residuel=my_inventaire.inb_montant_residuel, invc_date_acquisition=my_inventaire.inb_date_acquisition
        where invc_id=my_inventaire.invc_id;

      Api_Corossol.amort_vider(my_inventaire.invc_id);
      Api_Corossol.amortissement(my_inventaire.invc_id);
*/
   end if;
end;

procedure supprimer_hb (inbid integer) 
is 
   invcid jefy_inventaire.inventaire_non_budgetaire.invc_id%type;
   my_nb integer;
   clicid jefy_inventaire.cle_inventaire_comptable.clic_id%type;
begin
   -- recup des infos 
   select invc_id into invcid from jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE where inb_id = inbid;

   if invcid is not null then
      UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id = NULL WHERE invc_id = invcid;
      Api_Corossol.amort_vider(invcid);
 
      -- suppression, recup du clicid
      select count(*) into clicid from jefy_inventaire.inventaire_comptable where invc_id = invcid;
      if (clicid != 0 ) then
         select clic_id into clicid from jefy_inventaire.inventaire_comptable where invc_id = invcid;
         select count(*) into my_nb from inventaire_comptable where invc_id<>invcid and clic_id=clic_id;
         if my_nb=0 then
            delete from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE where  clic_id = clicid;
         end if;
         delete from jefy_inventaire.amortissement where invc_id = invcid;
         delete from jefy_inventaire.INVENTAIRE_COMPTABLE_orig where invc_id = invcid;
         delete from jefy_inventaire.INVENTAIRE_imp where invc_id = invcid;
         delete from jefy_inventaire.INVENTAIRE_COMPTABLE where invc_id = invcid;
      end if;
   end if;
 
   delete from jefy_inventaire.inventaire_non_budgetaire_comp where inb_id=inbid;
   delete from jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE where inb_id = inbid;
end;

END INTEGRATION_HB;
/

CREATE OR REPLACE PACKAGE JEFY_INVENTAIRE.INTEGRATION AS
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
/*

grant select on maracuja.PLANCO_AMORTISSEMENT to jefy_inventaire
*/


PROCEDURE integration_import_all;
PROCEDURE integration_import(idexterne integer);
procedure verification_import;

-- private
function get_exeordre (exer varchar) return integer;
function get_pcoaid (exeordre integer,imput varchar) return integer;
function get_ORGFID (origine varchar, exeordre integer) return integer;
function get_ORGID (ub varchar, cr varchar) return integer;
function get_utlordre return integer;

-- Ajout CH 
function get_pcoaidduree (exeordre integer,imput varchar) return varchar;

END ;
/


CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE.INTEGRATION as


PROCEDURE integration_import_all   as
   currentimp   jefy_inventaire.INVENTAIRE_IMP%rowtype;
   cursor c1 is select * from jefy_inventaire.inventaire_imp where invc_id is null;
BEGIN
   open c1;
   loop
      fetch c1 into currentimp;
      exit when c1%notfound;
 
      integration_import(currentimp.id_externe);
   end loop;
   close c1;
end;


PROCEDURE integration_import (idexterne integer)  as
   currentimp jefy_inventaire.INVENTAIRE_IMP%rowtype;
   my_inb_id integer;
   
   INVCID integer;
BEGIN
   select * into currentimp from jefy_inventaire.inventaire_imp where id_externe = idexterne;
   
   my_inb_id:=null;
   integration_hb.enregistrer_hb_comp(my_inb_id, get_utlordre, get_exeordre(currentimp.EXERCICE),
       get_ORGID(currentimp.ub, currentimp.cr), currentimp.imputation,
       currentimp.DUREE, currentimp.TYPE_AMORT, currentimp.MONTANT, currentimp.MONTANT_RESIDUEL,
       get_ORGFID(currentimp.type_financement, get_exeordre(currentimp.EXERCICE)), currentimp.NUMERO_SERIE,
       currentimp.INFORMATIONS, currentimp.FOURNISSEUR, currentimp.FACTURE,
       null, currentimp.DATE_ACQUISITION, currentimp.numero);

   -- on marque l'enregistrement import? et on conserve le liens avec l'inventaire_comptable
   select invc_id into invcid from INVENTAIRE_NON_BUDGETAIRE where inb_id=my_inb_id;
   update INVENTAIRE_IMP set invc_id = invcid where id_externe = idexterne;
end;



procedure verification_import
is
   exercice_tmp  varchar(2000);
   imput_tmp     varchar(2000);
   exeordre_tmp  integer;
   origine_tmp   varchar(2000);
   ub_tmp        jefy_admin.organ.org_ub%type;
   cr_tmp        jefy_admin.organ.org_cr%type;
   resultat      integer;

   cursor c_exercice is select distinct exercice from jefy_inventaire.inventaire_imp where invc_id is null;
   cursor c_imput is select distinct exercice,imputation from jefy_inventaire.inventaire_imp where invc_id is null;
   cursor c_financement is select distinct type_financement from jefy_inventaire.inventaire_imp where invc_id is null;
   cursor c_organ is select distinct ub,cr from jefy_inventaire.inventaire_imp where invc_id is null;
BEGIN
   resultat:=get_utlordre;
   
   open c_exercice;
   loop
      fetch c_exercice into exercice_tmp;
      exit when c_exercice%notfound;
        
      resultat:=INTEGRATION.get_exeordre (exercice_tmp);
   end loop;
   close c_exercice;

   open c_imput;
   loop
      fetch c_imput into exercice_tmp,imput_tmp;
      exit when c_imput%notfound;
  
      resultat:=INTEGRATION.get_pcoaid (get_exeordre(exercice_tmp),imput_tmp);
   end loop;
   close c_imput;

   open c_financement;
   loop
      fetch c_financement into origine_tmp;
      exit when c_financement%notfound;
  
      resultat:=INTEGRATION.get_ORGFID (origine_tmp, get_exeordre(exercice_tmp)) ;
   end loop;
   close c_financement;

   open c_organ;
   loop
      fetch c_organ into ub_tmp, cr_tmp;
      exit when c_organ%notfound;
  
      resultat:=INTEGRATION.get_orgid (ub_tmp, cr_tmp);
   end loop;
   close c_organ;
end;


-- private
-- private
-- private
function get_exeordre (exer varchar) return integer is
   exeordre integer;
   cpt integer;
begin
   select count(*) into cpt from jefy_admin.EXERCICE where exe_exercice = exer;

   if cpt = 1 then 
      select exe_ordre into exeordre from jefy_admin.EXERCICE where exe_exercice = exer;
   else
      raise_application_error (-20001,'Impossible de retrouver l''exe_ordre de l''exercice '||exer);
   end if;

   return exeordre;
end;

function get_pcoaid (exeordre integer,imput varchar) return integer is
   pcoaid integer;
   cpt integer;
begin
   /*select count(*) into cpt from maracuja.PLANCO_AMORTISSEMENT where  pco_num = imput and exe_ordre = exeordre;

   if cpt = 1 then 
      select pcoa_id into pcoaid from maracuja.PLANCO_AMORTISSEMENT where  pco_num = imput and exe_ordre = exeordre;
   else
      select count(*) into cpt from maracuja.PLANCO_AMORTISSEMENT where  pco_num = imput;

      if cpt = 0 then
         raise_application_error (-20001,'Impossible de retrouver le compte d''amortissement pour l''exeordre '||exeordre||' et l''imputation '||imput);
      else
         select max(exe_ordre),pcoa_id into cpt,pcoaid from maracuja.PLANCO_AMORTISSEMENT where  pco_num = imput;
      end if;
   end if;*/

   select pcoa_id into pcoaid from maracuja.PLANCO_AMORTISSEMENT where pco_num=imput
   and exe_ordre=(select max(exe_ordre) from maracuja.PLANCO_AMORTISSEMENT where pco_num=imput);

   return pcoaid;
end;

function get_orgfid (origine varchar, exeordre integer) return integer is
   orgfid integer;
   cpt integer;
begin
   select count(*) into cpt from jefy_inventaire.origine_financement where orgf_libelle = origine and exe_ordre=exeordre;

   if cpt = 1 then
      select orgf_id into orgfid from jefy_inventaire.origine_financement where orgf_libelle = origine and exe_ordre=exeordre;
   else
      if cpt=0 and exeordre<2007 then
         select origine_financement_seq.nextval into orgfid from dual;
         insert into origine_financement values (orgfid, origine, exeordre, null); 
      else
         raise_application_error (-20001,'Impossible de retrouver l''orgf_id  pour le libelle '||origine);
      end if;
   end if;

   return orgfid;
end;

function get_orgid (ub varchar, cr varchar) return integer is
   orgid integer;
   cpt integer;
begin
   select count(*) into cpt from jefy_admin.organ where org_ub=ub and org_cr=cr;

   if cpt>=1 then
      select max(org_id) into orgid from jefy_admin.organ where org_ub=ub and org_cr=cr;
   else
      raise_application_error (-20001,'Impossible de retrouver l''org_id  pour l''UB '||ub||' et le CR '||cr);
   end if;

   return orgid;
end;

-- Ajout CH 
function get_pcoaidduree (exeordre integer,imput varchar) return varchar is
   pcoaid varchar2(15);
   cpt integer;
   my_exe_ordre integer;
begin
   select count(*) into cpt from maracuja.PLANCO_AMORTISSEMENT where  pco_num = imput and exe_ordre = exeordre;

   if cpt = 1 then
      select pcoa_id||'$'||pca_duree into pcoaid from maracuja.PLANCO_AMORTISSEMENT where  pco_num = imput and exe_ordre = exeordre;
   else
      select count(*) into cpt from maracuja.PLANCO_AMORTISSEMENT where  pco_num = imput;

      if cpt = 0 then
         raise_application_error (-20001,'Impossible de retrouver le compte d''amortissement pour l''exeordre '||exeordre||' et l''imputation '||imput);
      else
         select min (exe_ordre) into my_exe_ordre from maracuja.PLANCO_AMORTISSEMENT where  pco_num = imput;
         select pcoa_id into pcoaid from maracuja.PLANCO_AMORTISSEMENT where pco_num=imput and exe_ordre=my_exe_ordre;
      end if;
   end if;

   return pcoaid;
end;

function get_utlordre return integer is
   utlordre integer;
   cpt integer;
begin
---------------------------------------
   -- Choix pour l'utilisateur utilis� pour l'import
   --   SOIT COMMENTER :
   
   --        utlordre :=   XXXXX   ;

   --   SOIT COMMENTER :

           select count(*) into cpt from INVENTAIRE_NON_BUDGETAIRE;
           if cpt>0 then 
              select min(utl_ordre) into utlordre from INVENTAIRE_NON_BUDGETAIRE;
           else
              raise_application_error (-20001,'D�finissez un utl_ordre pour l''importation dans la fonction get_utlordre du package integration');
           end if;
---------------------------------------

   return utlordre;
end;


END ;
/



CREATE OR REPLACE FORCE VIEW jefy_inventaire.v_actif_inventaire (exercice,
                                                                 ges_code,
                                                                 numero,
                                                                 exer,
                                                                 amo_annuite,
                                                                 amo_cumul,
                                                                 amo_residuel,
                                                                 amo_acquisition,
                                                                 pco_num
                                                                )
AS
   SELECT   MIN (exercice), ges_code, numero, exer, SUM (amo_annuite),
            SUM (amo_cumul), SUM (amo_residuel), SUM (amo_acquisition),
            pco_num
       FROM (SELECT TO_NUMBER (ee.exe_ordre) exercice, c.clic_comp ges_code,
                    clic_num_complet numero, TO_NUMBER (a.exe_ordre) exer,
                    amo_annuite, amo_cumul, amo_residuel, amo_acquisition,
                    c.pco_num
               FROM amortissement a,
                    inventaire_comptable i,
                    cle_inventaire_comptable c,
                    (SELECT   invc_id, MIN (exe_ordre) exe_ordre
                         FROM (SELECT invc_id, TO_NUMBER (exe_ordre)
                                                                    exe_ordre
                                 FROM amortissement
                               UNION ALL
                               SELECT i.invc_id, TO_NUMBER (c.exe_ordre)
                                 FROM inventaire_comptable i,
                                      cle_inventaire_comptable c
                                WHERE c.clic_id = i.clic_id)
                     GROUP BY invc_id) ee
              WHERE a.invc_id = i.invc_id
                AND i.clic_id = c.clic_id
                AND ee.invc_id = i.invc_id
             UNION ALL
             SELECT TO_NUMBER (c.exe_ordre) exercice, c.clic_comp ges_code,
                    clic_num_complet numero, c.exe_ordre exer, 0 amo_annuite,
                    0 amo_cumul,
                    DECODE (zz.nb, NULL, 1, 0) * amo_acquisition amo_residuel,
                    DECODE (zz.nb, NULL, 1, 0) * amo_acquisition, c.pco_num
               FROM jefy_inventaire.amortissement a,
                    jefy_inventaire.inventaire_comptable i,
                    jefy_inventaire.cle_inventaire_comptable c,
                    (SELECT   invc_id, exe_ordre, COUNT (*) nb
                         FROM jefy_inventaire.amortissement
                     GROUP BY invc_id, exe_ordre) zz
              WHERE a.invc_id = i.invc_id
                AND i.clic_id = c.clic_id
                AND amo_residuel = amo_acquisition - amo_annuite
                AND zz.exe_ordre(+) = i.exe_ordre
                AND zz.invc_id(+) = i.invc_id
/*union all
select RGP_EXER*1 exercice,RGP_COMP ges_code,RGP_NUMERO numero,AMO_EXER*1 exer,AMO_ANNUITE,AMO_CUMUL,AMO_RESIDUEL,AMO_ACQUISITION,PCO_NUM
from INVENTAIRE.ETAT_ACTIFS_BRUT*/
            )
   GROUP BY /*exercice, */ ges_code, numero, exer, /*amo_acquisition, */ pco_num;


CREATE OR REPLACE FORCE VIEW jefy_inventaire.v_actif_inventaire_cc (exercice, ges_code, numero, exer, amo_annuite, amo_cumul, amo_residuel, amo_acquisition, pco_num)
AS
   SELECT   MIN (exercice), ges_code, numero, exer, SUM (amo_annuite), SUM (amo_cumul), SUM (amo_residuel), SUM (amo_acquisition), pco_num
       FROM (SELECT TO_NUMBER (ee.exe_ordre) exercice, c.clic_comp ges_code,
                    clic_num_complet numero, TO_NUMBER (a.exe_ordre) exer,
                    amo_annuite, amo_cumul, amo_residuel, amo_acquisition,
                    c.pco_num
               FROM jefy_inventaire.amortissement a,
                    jefy_inventaire.inventaire_comptable i,
                    jefy_inventaire.cle_inventaire_comptable c,
                    (SELECT   invc_id, MIN (exe_ordre) exe_ordre
                         FROM (SELECT i.invc_id,
                                      TO_NUMBER (c.exe_ordre) exe_ordre
                                 FROM jefy_inventaire.inventaire_comptable i,
                                      jefy_inventaire.cle_inventaire_comptable c
                                WHERE c.clic_id = i.clic_id)
                     GROUP BY invc_id) ee
              WHERE a.invc_id = i.invc_id
                AND i.clic_id = c.clic_id
                AND ee.invc_id = i.invc_id
             UNION ALL
             SELECT TO_NUMBER (c.exe_ordre) exercice, c.clic_comp ges_code,
                    clic_num_complet numero, c.exe_ordre exer, 0 amo_annuite,
                    0 amo_cumul,
                    DECODE (zz.nb, NULL, 1, 0) * amo_acquisition amo_residuel,
                    DECODE (zz.nb, NULL, 1, 0) * amo_acquisition, c.pco_num
               FROM jefy_inventaire.amortissement a,
                    jefy_inventaire.inventaire_comptable i,
                    jefy_inventaire.cle_inventaire_comptable c,
                    (SELECT   invc_id, exe_ordre, COUNT (*) nb
                         FROM jefy_inventaire.amortissement
                     GROUP BY invc_id, exe_ordre) zz
              WHERE a.invc_id = i.invc_id
                AND i.clic_id = c.clic_id
                AND amo_residuel = amo_acquisition - amo_annuite
                AND zz.exe_ordre(+) = i.exe_ordre
                AND zz.invc_id(+) = i.invc_id
/*union all
select RGP_EXER*1 exercice,RGP_COMP ges_code,RGP_NUMERO numero,AMO_EXER*1 exer,AMO_ANNUITE,AMO_CUMUL,AMO_RESIDUEL,AMO_ACQUISITION,PCO_NUM
from INVENTAIRE.ETAT_ACTIFS_BRUT*/
            )
   GROUP BY /*exercice,*/ ges_code, numero, exer, /*amo_acquisition, */ pco_num;


CREATE OR REPLACE PACKAGE BODY JEFY_INVENTAIRE.API_COROSSOL AS

PROCEDURE creer_lien_inv_dep (
   dpcoid  inventaire_comptable.dpco_id%type, 
   invcid  inventaire_comptable.invc_id%type,
   montant inventaire_comptable.invc_montant_acquisition%type
) IS 
  depidrev integer;
  orv number(12,2);
  pco      maracuja.plan_comptable.PCO_NUM%type;
  my_date  JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.invc_date_acquisition%type;
  clicid   JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.clic_id%type;
  exeordre JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.exe_ordre%type;
BEGIN
  orv := 0;
  my_date:=null;
  
  -- ORV
  if montant<0 then
    -- creation de l'enregistrment dans INVENTAIRE_COMPTABLE_ORV
    insert into INVENTAIRE_COMPTABLE_ORV values (invcid,dpcoid,INVENTAIRE_COMPTABLE_ORV_seq.nextval,montant);
  else

    if (dpcoid != -1000) then -- cas des integrations HB
       -- recup du compte d'imputation de la liquidation
       select pc.pco_num, dpp_date_service_fait into pco, my_date 
          from jefy_depense.depense_papier p, jefy_depense.depense_budget b, jefy_depense.depense_ctrl_planco pc
          where b.dep_id=pc.dep_id and b.dpp_id=p.dpp_id and pc.dpco_id=dpcoid;
       UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET invc_date_acquisition=my_date WHERE invc_id=invcid;
    end if;

    -- recup du numero comptable
    select clic_id,EXE_ORDRE into clicid,exeordre from JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE where invc_id = invcid;

    if (dpcoid != -1000) then
      -- Correction CH  maj du compte d'amortissement et de la dur?e 
      update JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE set pco_num = pco,
         PCOA_ID = substr(integration.get_pcoaidduree(exeordre, pco),1, instr(integration.get_pcoaidduree(exeordre, pco),'$')-1),
         CLIC_DUREE_AMORT = substr(integration.get_pcoaidduree(exeordre, pco),instr(integration.get_pcoaidduree(exeordre, pco),'$')+1)
        where clic_id = clicid;
    end if;

    -- maj inventaire_comptable : creation su liens
    UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id = dpcoid, invc_montant_acquisition=montant + nvl(orv,0),
      invc_montant_residuel = montant+ nvl(orv,0) WHERE invc_id = invcid;
  end if;

  -- CREATION DE L AMORTISSEMENT
  amortissement(invcid);
END;
 
PROCEDURE supprimer_lien_inv_dep (dpcoid inventaire_comptable.dpco_id%type) IS 
   cursor c1 is select invc_id from inventaire_comptable where dpco_id=dpcoid;
   invcid jefy_inventaire.inventaire_comptable.invc_id%type;
BEGIN
  -- on traite les ORVS 
  delete from jefy_inventaire.INVENTAIRE_COMPTABLE_ORV where dpco_id=dpcoid;
 
  open c1;
  loop
    fetch c1 into invcid;
    exit when c1%notfound;
    -- maj  inventaire_comptable : casse le lien
    UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id=NULL WHERE dpco_id=dpcoid and invc_id=invcid;
    -- SUPPRESSION DE L AMORTISSEMENT
    amort_vider(invcid);
  end loop;
  close c1;
END;

PROCEDURE AMORTISSEMENT_CLE (
   a01clicid cle_inventaire_comptable.clic_id%type,
   a02annule integer
) is
  cursor c1 is select invc_id from inventaire_comptable where clic_id=a01clicid;
  invcid jefy_inventaire.inventaire_comptable.invc_id%type;
begin

  if a02annule = 1 then
    open c1;
    loop
      fetch c1 into invcid ;
      exit when c1%notfound;
      API_COROSSOL.amort_vider(invcid);
    end loop;
    close c1;
  else
    open c1;
    loop
      fetch c1 into invcid ;
      exit when c1%notfound;
      amort_vider(invcid);
      amortissement(invcid);
    end loop;
    close c1;
  end if;
end;

PROCEDURE amortissement(
   invcid inventaire_comptable.invc_id%type
) IS
  cpt INTEGER;
  tmpinvComptable jefy_inventaire.INVENTAIRE_COMPTABLE%ROWTYPE;
  tmpCleComptable jefy_inventaire.CLE_INVENTAIRE_COMPTABLE%ROWTYPE;
  tmpplancoamort  maracuja.plan_comptable_amo%ROWTYPE;
  montant_orvs    jefy_inventaire.INVENTAIRE_COMPTABLE_ORV.INVO_MONTANT_ORV%type;
  my_dgco_coef    degressif_coef.dgco_coef%type;
  my_date_calcul  jefy_depense.depense_papier.dpp_date_service_fait%type;
  my_dgco_id      degressif_coef.dgco_id%type;
BEGIN
  -- recup des infos du numero comptable
  SELECT COUNT(*) INTO cpt FROM jefy_inventaire.INVENTAIRE_COMPTABLE WHERE invc_id = invcid ;
  IF cpt = 0 THEN
    RAISE_APPLICATION_ERROR (-20001,'inventaire comptable introuvable, invc_id='||invcid);
  END IF;
  
  SELECT * INTO tmpinvComptable FROM jefy_inventaire.INVENTAIRE_COMPTABLE WHERE invc_id = invcid ;
  SELECT nvl(sum(INVO_MONTANT_ORV),0)  INTO montant_orvs FROM jefy_inventaire.INVENTAIRE_COMPTABLE_ORV WHERE invc_id = invcid ;
  SELECT * INTO tmpCleComptable FROM jefy_inventaire.CLE_INVENTAIRE_COMPTABLE WHERE clic_id = tmpinvComptable.clic_id ;

  SELECT COUNT(*) INTO cpt FROM maracuja.plan_comptable_amo WHERE pcoa_id = tmpCleComptable.pcoa_id;
  IF cpt = 0 THEN
    raise_application_error (-20001,'compte d amortissement inexistant verifier votre parametrage dans maracuja '||tmpclecomptable.pcoa_id);
  end if;
  
  SELECT * INTO tmpplancoamort FROM maracuja.plan_comptable_amo WHERE pcoa_id = tmpCleComptable.pcoa_id;

  my_date_calcul:=tmpinvcomptable.invc_date_acquisition;

  if tmpinvComptable.dpco_id is not null then
    if my_date_calcul is null then 
       select count(*) into cpt from jefy_depense.depense_ctrl_planco where dpco_id=tmpinvcomptable.dpco_id;
       if cpt>0 then
          select dpp_date_service_fait into my_date_calcul 
             from jefy_depense.depense_papier p, jefy_depense.depense_budget b, jefy_depense.depense_ctrl_planco pc
             where b.dep_id=pc.dep_id and pc.dpco_id=tmpinvcomptable.dpco_id and b.dpp_id=p.dpp_id;
       end if;
    end if;
        
    my_dgco_coef:=null;
    my_dgco_id:=tmpinvComptable.dgco_id;
    if my_dgco_id is not null then
      select max(dgco_id) into my_dgco_id from degressif_coef where 
        dgrf_id in (select dgrf_id from degressif where dgrf_date_debut<=my_date_calcul and (dgrf_date_fin>=my_date_calcul or dgrf_date_fin is null))
        and dgco_duree_min<=tmpCleComptable.clic_duree_amort and dgco_duree_max>=tmpCleComptable.clic_duree_amort;
    end if;
    if my_dgco_id is not null then 
      select dgco_coef into my_dgco_coef from degressif_coef where dgco_id=my_dgco_id;
    end if;
  
    IF tmpCleComptable.clic_type_amort = 'Lineaire' THEN
      Api_Corossol.amort_lineaire(invcid,tmpinvComptable.invc_montant_acquisition+montant_orvs,tmpCleComptable.clic_duree_amort,
         tmpCleComptable.pco_num,tmpplancoamort.pcoa_num,tmpinvComptable.exe_ordre, tmpCleComptable.clic_pro_rata,my_date_calcul);
    ELSE
      Api_Corossol.amort_degressif(invcid,tmpinvComptable.invc_montant_acquisition+montant_orvs,tmpCleComptable.clic_duree_amort,
       tmpCleComptable.pco_num,tmpplancoamort.pcoa_num,tmpinvComptable.exe_ordre, tmpCleComptable.clic_pro_rata,my_dgco_coef, my_date_calcul);
    END IF;
  end if;
END;

PROCEDURE amort_lineaire(
   invcid   inventaire_comptable.invc_id%type,
   montant  inventaire_comptable.invc_montant_acquisition%type,
   duree    cle_inventaire_comptable.clic_duree_amort%type,
   pco      cle_inventaire_comptable.pco_num%type,
   pcoamort jefy_inventaire.amortissement.pco_num%type,
   exeordre inventaire_comptable.exe_ordre%type,
   prorata  cle_inventaire_comptable.clic_pro_rata%type,
   la_date  inventaire_comptable.invc_date_acquisition%type
) IS 
  cle         jefy_inventaire.amortissement.amo_id%type;
  annuite     jefy_inventaire.amortissement.amo_annuite%type;
  annuite_ref jefy_inventaire.amortissement.amo_annuite%type;
  cumul       jefy_inventaire.amortissement.amo_cumul%type;
  residuel    jefy_inventaire.amortissement.amo_residuel%type;
  iter        integer;
  thefirst    integer;
  my_nb       integer;
  my_nb_decimales  NUMBER;

  myexeordre inventaire_comptable.exe_ordre%type;

  cursor c1 is select s.* from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid and s.invs_vnc>0;
  my_inventaire_sortie jefy_inventaire.inventaire_sortie%rowtype;
  exe_ordre_sortie     jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_amor       jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_debut      jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_fin        jefy_inventaire.amortissement.exe_ordre%type;
  my_amortissement     jefy_inventaire.amortissement.amo_annuite%type;
  my_residuel          jefy_inventaire.amortissement.amo_residuel%type;
  my_cumul             jefy_inventaire.amortissement.amo_cumul%type;
  my_acquisition       jefy_inventaire.amortissement.amo_acquisition%type;
  my_annees_passees    integer;
BEGIN

  if prorata<>0 and la_date is null then
     raise_application_error (-20001,'Pour un amortissement prorata temporis il faut indiquer une date de debut d''amortissement.');  
  end if;

  amort_vider(invcid);

  my_annees_passees:=0;
  if la_date is not null then
    my_annees_passees:=exeordre-to_number(to_char(la_date,'yyyy'));
    if my_annees_passees<0 then my_annees_passees:=0; end if;
  end if;
 
  my_nb_decimales:=get_nb_decimales(exeordre);

  iter :=0;
  cumul:=0;
  thefirst:=0;
  residuel:=montant;
  
  dbms_output.put_line('cumul : '||cumul);
  dbms_output.put_line('residuel : '||residuel);

  dbms_output.put_line('duree : '||duree);
  dbms_output.put_line('my_annees_passees : '||my_annees_passees);

  if duree-my_annees_passees=0 then
     annuite_ref:=montant;
  else
     if prorata=0 and my_annees_passees>0 /*and duree-my_annees_passees-1<>0*/ then
     annuite_ref:=montant/(duree-my_annees_passees+1);
     /*else
     annuite_ref:=montant/(duree-my_annees_passees);*/
     end if;
  end if;

  if annuite_ref is null then 
     annuite_ref:=montant/(duree-my_annees_passees);
  end if;
  
  myexeordre:=exeordre-my_annees_passees;
  
  dbms_output.put_line('annuite ref : '||annuite_ref);
  LOOP
    IF montant=0 or duree=0 or residuel=0 THEN EXIT; END IF;

     if prorata=0 and my_annees_passees>0 and thefirst=0 then
        iter:=0;
     else
        iter:=iter+1;
     end if;

    thefirst:=thefirst+1;
    
    -- si on a laisse un amortissement c'est qu'on ne veut pas le recalculer pour cette annee la        
    select count(*) into my_nb from amortissement where invc_id=invcid and exe_ordre=myexeordre+iter;
    if my_nb=0 then
       -- calcul des valeurs
       annuite:=annuite_ref;
       
       if prorata<>0 and iter=my_annees_passees+1 then
       dbms_output.put_line('taux_premiere_annee(exeordre, la_date):'||exeordre||','||iter||','||taux_premiere_annee(exeordre, la_date));
          annuite:=annuite*taux_premiere_annee(exeordre, la_date);
       end if;

       if iter<=my_annees_passees and ((my_annees_passees=0 and iter=0) or (my_annees_passees>0 and prorata>0) 
            or (my_annees_passees>0 and prorata=0 and iter<my_annees_passees)) then
          annuite:=0;
       end if;

  --dbms_output.put_line('iter : '||iter||', annees passees : '||my_annees_passees||', annuite : '||annuite);

       annuite:=round(annuite, my_nb_decimales);

       if annuite>residuel or (iter>=my_annees_passees and (prorata=0 and iter>=duree) OR (prorata<>0 and iter>=duree+1)) then
        annuite:=residuel; 
       end if;
       if annuite<0 then annuite:=0; end if;

  dbms_output.put_line('annuite : '||annuite);

       cumul:=cumul+annuite;
       residuel:=montant-cumul;
    
       -- insertion
       select amortissement_seq.nextval into cle from dual;
       if prorata=0 then
       insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
          values (montant,annuite,cumul ,sysdate,cle,residuel,myexeordre+iter,invcid,pcoamort );
       else
         insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
           values (montant,annuite,cumul ,sysdate,cle,residuel,myexeordre+iter-1,invcid,pcoamort );
       end if;
    else
       select amo_annuite into annuite from amortissement where invc_id=invcid and exe_ordre=myexeordre+iter; 
       cumul:=cumul+annuite;
       residuel:=montant-cumul;    
    end if;          

    -- test (attention : pour l'amortissement lineaire avec prorata temporis, on amortie sur duree+1 !!!!!!!
    
    --dbms_output.put_line('ann amor :'||to_char(la_date,'yyyy')||'+'||iter||'>=exe:'||exeordre);
    --dbms_output.put_line('prorata :'||prorata||', iter:'||iter||', duree:'||duree);
    --dbms_output.put_line('      annuite : '||annuite);
    
    IF ((to_number(to_char(la_date,'yyyy'))+iter>=exeordre) and
       ((prorata=0 or to_char(la_date,'dd/mm')='01/01') and iter=duree) OR (prorata<>0 and iter=duree+1) 
        or (prorata<>0 and iter=duree and residuel<1)) THEN
      UPDATE AMORTISSEMENT SET AMO_RESIDUEL=0, AMO_CUMUL=AMO_CUMUL+AMO_RESIDUEL, AMO_ANNUITE=AMO_ANNUITE+AMO_RESIDUEL WHERE AMO_ID=cle;
      EXIT;
    END IF;
    
  END LOOP;
  
  -- gerer sorties inventaire
  select count(*) into my_nb from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid;
  if my_nb>0 then
  
    --exe_ordre_fin:=exeordre+duree;
    select min(exe_ordre) into exe_ordre_debut from amortissement where invc_id=invcid;
    select max(exe_ordre) into exe_ordre_fin   from amortissement where invc_id=invcid;

    open c1;
    loop
      fetch c1 into my_inventaire_sortie;
      exit when c1%notfound;
      
      exe_ordre_sortie:=to_number(to_char(my_inventaire_sortie.invs_date,'yyyy'));
      if exe_ordre_sortie<exe_ordre_debut then
         exe_ordre_sortie:=exe_ordre_debut;
      end if;
      exe_ordre_amor:=exe_ordre_sortie;
      
      LOOP
        IF exe_ordre_amor>exe_ordre_fin THEN EXIT; END IF;

        -- exercice de la sortie
        if exe_ordre_amor=exe_ordre_sortie then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           my_amortissement:=my_inventaire_sortie.invs_vnc;
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
           end if;
        end if;
      
        -- annees suivantes
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor<exe_ordre_fin then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
                        
           my_amortissement:=round(my_inventaire_sortie.invs_vnc/(exe_ordre_fin-exe_ordre_sortie), my_nb_decimales);        
                     
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
              select amo_annuite into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
              my_amortissement:=my_residuel-my_amortissement;
           end if;
           
           my_amortissement:=-my_amortissement;
        end if;

        -- derniere annee
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor=exe_ordre_fin then
           select nvl(sum(amo_annuite),0) into my_cumul from amortissement where exe_ordre<exe_ordre_fin and invc_id=invcid;
           select amo_acquisition, amo_annuite into my_acquisition, my_residuel from amortissement where exe_ordre=exe_ordre_fin and invc_id=invcid;
           my_amortissement:=my_acquisition-my_cumul-my_residuel;
        end if;

        if exe_ordre_amor>exe_ordre_sortie then
           select amo_cumul, amo_residuel into my_cumul, my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
           
           update amortissement set amo_cumul=my_cumul+amo_annuite, amo_residuel=my_residuel-amo_annuite 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;

        if my_amortissement<>0 then
          update amortissement
            set amo_annuite=amo_annuite+my_amortissement, amo_cumul=amo_cumul+my_amortissement, amo_residuel=amo_residuel-my_amortissement, amo_date=sysdate 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;
        
        exe_ordre_amor:=exe_ordre_amor+1;
      end loop;
    
    end loop;
    close c1;
  end if;
  
  
  delete from amortissement where amo_residuel=0 and amo_annuite=0 and invc_id=invcid;
END;
 
PROCEDURE amort_degressif(
   invcid      inventaire_comptable.invc_id%type,
   montant     inventaire_comptable.invc_montant_acquisition%type,
   duree       cle_inventaire_comptable.clic_duree_amort%type,
   pco         cle_inventaire_comptable.pco_num%type,
   pcoamort    jefy_inventaire.amortissement.pco_num%type,
   exeordre    inventaire_comptable.exe_ordre%type,
   prorata     cle_inventaire_comptable.clic_pro_rata%type,
   coef_fiscal degressif_coef.dgco_coef%type,
   la_date     inventaire_comptable.invc_date_acquisition%type
) IS
  cle         jefy_inventaire.amortissement.amo_id%type;
  annuite     jefy_inventaire.amortissement.amo_annuite%type;
  cumul       jefy_inventaire.amortissement.amo_cumul%type;
  residuel    jefy_inventaire.amortissement.amo_residuel%type;
  iter        integer;
  my_nb       integer;
  my_nb_decimales  NUMBER;
  
  cursor c1 is select s.* from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid and s.invs_vnc>0;
  my_inventaire_sortie jefy_inventaire.inventaire_sortie%rowtype;
  exe_ordre_sortie     jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_amor       jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_debut      jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_fin        jefy_inventaire.amortissement.exe_ordre%type;
  my_amortissement     jefy_inventaire.amortissement.amo_annuite%type;
  my_residuel          jefy_inventaire.amortissement.amo_residuel%type;
  my_cumul             jefy_inventaire.amortissement.amo_cumul%type;
  my_acquisition       jefy_inventaire.amortissement.amo_acquisition%type;
BEGIN
  if prorata<>0 and la_date is null then
     raise_application_error (-20001,'Pour un amortissement prorata temporis il faut indiquer une date de debut d''amortissement.');  
  end if;

  amort_vider(invcid);
  
  my_nb_decimales:=get_nb_decimales(exeordre);
  
  iter :=0;
  cumul:=0;
  residuel:=montant;
  
  loop
    if montant=0 or duree=0 or residuel=0 then exit; end if;
    
    iter:=iter+1;
    
    -- calcul des valeurs
    dbms_output.put_line(duree||'+1-'||iter);
    if duree+1-iter=0 then
       annuite:=residuel;
    else
       if coef_fiscal*100.0/duree > 100/(duree+1-iter) then
          annuite:=residuel*(coef_fiscal/duree);
       else
          annuite:=residuel/(duree+1-iter);
       end if;
    end if;
 
    if prorata<>0 and iter=1 then
       annuite:=annuite*taux_premiere_annee(exeordre, la_date);
    end if;
    
    annuite:=round(annuite, my_nb_decimales);
    
    if annuite>residuel or duree=iter then annuite:=residuel; end if;
    if annuite<0 then annuite:=0; end if;
    
    select count(*) into my_nb from amortissement where invc_id=invcid and exe_ordre=exeordre+iter;
    if my_nb=0 then
       cumul:=cumul+annuite;
       residuel:=montant-cumul;
    
       -- insertion
       select amortissement_seq.nextval into cle from dual;
       if prorata=0 then
          insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
              values (montant, annuite, cumul, sysdate, cle, residuel, exeordre+iter, invcid, pcoamort);
       else
          insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
              values (montant, annuite, cumul, sysdate, cle, residuel, exeordre+iter-1, invcid, pcoamort);
       end if;
    else
       select amo_annuite into annuite from amortissement where invc_id=invcid and exe_ordre=exeordre+iter; 
       cumul:=cumul+annuite;
       residuel:=montant-cumul;    
    end if;
  end loop;
  
  -- gerer sorties inventaire
  select count(*) into my_nb from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid;
  if my_nb>0 then
  
    exe_ordre_fin:=exeordre+duree;
    select min(exe_ordre) into exe_ordre_debut from amortissement where invc_id=invcid;

    open c1;
    loop
      fetch c1 into my_inventaire_sortie;
      exit when c1%notfound;
      
      exe_ordre_sortie:=to_number(to_char(my_inventaire_sortie.invs_date,'yyyy'));
      if exe_ordre_sortie<exe_ordre_debut then
         exe_ordre_sortie:=exe_ordre_debut;
      end if;
      exe_ordre_amor:=exe_ordre_sortie;
      
      LOOP
        IF exe_ordre_amor>exe_ordre_fin THEN EXIT; END IF;

        -- exercice de la sortie
        if exe_ordre_amor=exe_ordre_sortie then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           my_amortissement:=my_inventaire_sortie.invs_vnc;
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
           end if;
        end if;
      
        -- annees suivantes
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor<exe_ordre_fin then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;

           iter:=exe_ordre_amor-exeordre;
              
           if coef_fiscal*100.0/duree > 100/(duree+1-iter) then
              my_amortissement:=residuel*(coef_fiscal/duree);
           else
              my_amortissement:=residuel/(duree+1-iter);
           end if;
                        
           my_amortissement:=round(my_amortissement, my_nb_decimales);        
                     
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
              select amo_annuite into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
              my_amortissement:=my_residuel-my_amortissement;
           end if;
           
           my_amortissement:=-my_amortissement;
        end if;

        -- derniere annee
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor=exe_ordre_fin then
           select nvl(sum(amo_annuite),0) into my_cumul from amortissement where exe_ordre<exe_ordre_fin and invc_id=invcid;
           select amo_acquisition, amo_annuite into my_acquisition, my_residuel from amortissement where exe_ordre=exe_ordre_fin and invc_id=invcid;
           my_amortissement:=my_acquisition-my_cumul-my_residuel;
        end if;

        if exe_ordre_amor>exe_ordre_sortie then
           select amo_cumul, amo_residuel into my_cumul, my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
           
           update amortissement set amo_cumul=my_cumul+amo_annuite, amo_residuel=my_residuel-amo_annuite 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;

        if my_amortissement<>0 then
          update amortissement
            set amo_annuite=amo_annuite+my_amortissement, amo_cumul=amo_cumul+my_amortissement, amo_residuel=amo_residuel-my_amortissement, amo_date=sysdate 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;
        
        exe_ordre_amor:=exe_ordre_amor+1;
      end loop;
    
    end loop;
    close c1;
  end if;
  
end;
 
PROCEDURE amort_vider(
   invcid inventaire_comptable.invc_id%type
) IS 
  my_nb                   integer;
  my_inventaire_comptable inventaire_comptable%rowtype;
BEGIN
  select * into my_inventaire_comptable from inventaire_comptable where invc_id=invcid;
  select count(*) into my_nb from parametre where par_key='RECALCUL_AMORTISSEMENTS_ANTERIEURS' and exe_ordre>my_inventaire_comptable.exe_ordre and par_value='NON';
  if my_nb>0 then
     DELETE FROM AMORTISSEMENT WHERE invc_id=invcid and exe_ordre>=grhum.en_nombre(to_char(sysdate,'yyyy'));
  else
     DELETE FROM AMORTISSEMENT WHERE invc_id=invcid;
  end if;

END;

function taux_premiere_annee(
   a_exe_ordre inventaire_comptable.exe_ordre%type,
   a_date      inventaire_comptable.invc_date_acquisition%type
) return number is
   annee    inventaire_comptable.exe_ordre%type;
   nb_jours integer;
begin
   -- Test date 
   if a_date is null then return 1.0; end if;
   
   annee:=to_number(to_char(a_date, 'yyyy'));
   if a_exe_ordre>annee then return 1.0; end if;
   
   -- calcul
   nb_jours:=360 - (to_number(to_char(a_date,'mm'))-1)*30 - (to_number(to_char(a_date,'dd'))-1);
   if nb_jours>360 then nb_jours:=360.0; end if;
   if nb_jours<0 then nb_jours:=0.0; end if;

   return nb_jours/360.0;
end;

FUNCTION get_nb_decimales(a_exe_ordre inventaire_comptable.exe_ordre%type)
     return jefy_admin.devise.dev_nb_decimales%type
   is
     my_nb        integer;
     my_par_value jefy_admin.parametre.par_value%type;
     my_dev       jefy_admin.devise.dev_nb_decimales%type;
   begin
        SELECT COUNT(*) INTO my_nb FROM jefy_admin.PARAMETRE WHERE exe_ordre=a_exe_ordre AND par_key='DEVISE_DEV_CODE';
        IF my_nb=1 THEN
           SELECT par_value INTO my_par_value FROM jefy_admin.PARAMETRE
              WHERE exe_ordre=a_exe_ordre AND par_key='DEVISE_DEV_CODE';

           SELECT COUNT(*) INTO my_nb FROM jefy_admin.devise WHERE dev_code=my_par_value;
           IF my_nb=1 THEN
              SELECT dev_nb_decimales INTO my_dev FROM jefy_admin.devise WHERE dev_code=my_par_value;

              IF my_dev IS NOT NULL THEN
                 return my_dev;
              END IF;
           END IF;
        END IF;

        return 2;
   end;
END;
/


CREATE OR REPLACE FORCE VIEW jefy_inventaire.v_commande_engage_ctrl_planco (exe_ordre,comm_numero,comm_id,comm_libelle,epco_id,pco_num,org_id,tcd_ordre,fou_ordre,dev_id,utl_ordre,comm_date,tap_taux)
AS
   SELECT DISTINCT c.exe_ordre, c.comm_numero, c.comm_id, c.comm_libelle,epco_id epco_id, epco.pco_num pco_num, eb.org_id,
                   eb.tcd_ordre, eb.fou_ordre, c.dev_id, c.utl_ordre,c.comm_date,tp.tap_taux
              FROM jefy_depense.commande c,jefy_depense.commande_engagement ce,jefy_depense.engage_budget eb,jefy_admin.taux_prorata tp,jefy_depense.engage_ctrl_planco epco
             WHERE c.comm_id = ce.comm_id
               AND ce.eng_id = eb.eng_id and eb.tap_id=tp.tap_id
               AND epco.eng_id = eb.eng_id
               AND (epco.pco_num LIKE '2%' OR epco.pco_num LIKE '6%')
               AND epco.pco_num NOT LIKE '23%';



create or replace procedure grhum.inst_patch_jefy_inv_1470 is
 nb integer;
begin
     UPDATE JEFY_INVENTAIRE.INVENTAIRE_IMP set date_acquisition=to_date('01/01/'||exercice,'dd/mm/yyyy') where date_acquisition is null;

     insert into jefy_inventaire.parametre values (2002,'PRORATA_TEMPORIS','OUI', 'utilise-t-on le prorata temporis pour cet exercice (OUI/NON)');
     insert into jefy_inventaire.parametre values (2003,'PRORATA_TEMPORIS','OUI', 'utilise-t-on le prorata temporis pour cet exercice (OUI/NON)');
     insert into jefy_inventaire.parametre values (2004,'PRORATA_TEMPORIS','OUI', 'utilise-t-on le prorata temporis pour cet exercice (OUI/NON)');
     insert into jefy_inventaire.parametre values (2005,'PRORATA_TEMPORIS','OUI', 'utilise-t-on le prorata temporis pour cet exercice (OUI/NON)');
     insert into jefy_inventaire.parametre values (2006,'PRORATA_TEMPORIS','OUI', 'utilise-t-on le prorata temporis pour cet exercice (OUI/NON)');

	 insert into jefy_inventaire.db_version values (1470,'1.4.7.0',to_date('12/12/2011','dd/mm/yyyy'),sysdate,null);  
end;
/