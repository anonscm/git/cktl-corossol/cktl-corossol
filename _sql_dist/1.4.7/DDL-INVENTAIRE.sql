SET DEFINE OFF;

-- Fichier :  n�1/1
-- Type : DDL
-- Sch�ma modifi� :  INVENTAIRE
-- Sch�ma d'execution du script : GRHUM
-- Num�ro de version :  
-- Date de publication :  12/12/2011
-- Licence : CeCILL version 2

whenever sqlerror exit sql.sqlcode ;

CREATE OR REPLACE FORCE VIEW inventaire.les_factures_complet(
   exer, dep_ordre, cde_ordre, man_ordre, man_num, pco_num, bor_num, fou_ordre, cde_num, dep_fact, dep_date, dep_mont,DEP_TTC, bor_date, tcd_code, cde_prorata,
   organ)
AS
   SELECT '1997', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_ordre, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy97.facture f, jefy97.mandat m, jefy97.bordero b, jefy97.commande c, jefy97.engage e, jefy97.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre
   UNION ALL
   SELECT '1998', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_ordre, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy98.facture f, jefy98.mandat m, jefy98.bordero b, jefy98.commande c, jefy98.engage e, jefy98.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre
   UNION ALL
   SELECT '1999', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_ordre, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy99.facture f, jefy99.mandat m, jefy99.bordero b, jefy99.commande c, jefy99.engage e, jefy99.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre
   UNION ALL
   SELECT '2000', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_ordre, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy00.facture f, jefy00.mandat m, jefy00.bordero b, jefy00.commande c, jefy00.engage e, jefy00.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre
   UNION ALL
   SELECT '2001', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_ordre, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy01.facture f, jefy01.mandat m, jefy01.bordero b, jefy01.commande c, jefy01.engage e, jefy01.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre
   UNION ALL
   SELECT '2002', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_ordre, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy02.facture f, jefy02.mandat m, jefy02.bordero b, jefy02.commande c, jefy02.engage e, jefy02.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre
   UNION ALL
   SELECT '2003', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_regroupement, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy03.facture f, jefy03.mandat m, jefy03.bordero b, jefy03.commande c, jefy03.engage e, jefy03.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre
   UNION ALL
   SELECT '2004', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_regroupement, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy04.facture f, jefy04.mandat m, jefy04.bordero b, jefy04.commande c, jefy04.engage e, jefy04.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre
   UNION ALL
   SELECT '2005', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_regroupement, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy05.facture f, jefy05.mandat m, jefy05.bordero b, jefy05.commande c, jefy05.engage e, jefy05.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre
   UNION ALL
   SELECT '2006', f.dep_ordre, f.cde_ordre, f.man_ordre, m.man_num, m.pco_num, b.bor_num, m.fou_ordre, c.cde_regroupement, f.dep_fact, f.dep_date, f.dep_mont, f.DEP_TTC, b.bor_date, c.tcd_code, c.cde_prorata
       ,o.org_comp||'/'||o.org_lbud||'/'||o.org_uc
     FROM jefy.facture f, jefy.mandat m, jefy.bordero b, jefy.commande c, jefy.engage e, jefy.organ o
    WHERE f.man_ordre = m.man_ordre AND m.bor_ordre = b.bor_ordre AND f.cde_ordre = c.cde_ordre AND c.tcd_code = 20
      and c.cde_ordre=e.cde_ordre and e.eng_stat<>'A' and e.org_ordre=o.org_ordre;
