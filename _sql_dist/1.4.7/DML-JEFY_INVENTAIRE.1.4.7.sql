
--
-- 

-- Fichier :  n�2/2
-- Type : DML
-- Sch�ma modifi� :  JEFY_INVENTAIRE
-- Sch�ma d'execution du script : GRHUM
-- Num�ro de version : 1.4.7
-- Date de publication : 12/12/2011 
-- Licence : CeCILL version 2


whenever sqlerror exit sql.sqlcode ;

    execute grhum.inst_patch_jefy_inv_1470;
commit;

drop procedure grhum.inst_patch_jefy_inv_1470;

ALTER TABLE JEFY_INVENTAIRE.INVENTAIRE_IMP MODIFY(DATE_ACQUISITION NOT NULL);
