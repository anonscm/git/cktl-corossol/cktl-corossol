--requete d'insertion
select 
r.rgp_numero numero, r.rgp_exer exercice, r.rgp_comp ub, r.rgp_cr cr, f.pco_num imputation, pa.pco_duree duree, 'Lineaire' type_amort, 
    sum(i.inv_ht) montant, sum(i.inv_ht) montant_residuel, nvl(r.rgp_origine,'Autofinancement') origine,
       /*inv_serie*/ null serie, 'Cde : '||f.cde_num||chr(13)||chr(10)||'  Ligne budgetaire : '||f.organ||' TC : '||f.tcd_code||
       ' Prorata : '||f.cde_prorata||chr(13)||chr(10)--||
       /*inv_modele*/ informations, f.man_num mandat, f.bor_num bordereau,  r.rgp_comp gestion, fo.fou_code||' '||fo.fou_nom fournisseur, 
       'N� Facture : '||f.dep_fact||' du '||f.dep_date||', montant : '||f.dep_mont||chr(13)||chr(10)||
         '  Mandat : '||f.MAN_NUM||' du '||f.bor_date||', Bordereau : '||f.bor_num||chr(13)||chr(10)||
         '  Date de controle : '||chr(13)||chr(10)||'  Responsable du controle : '||chr(13)||chr(10)||
         '  Proprietaire : '||chr(13)||chr(10)||'  Localisation : '||chr(13)||chr(10) facture, null invc_id, f.bor_date date_acquisition
from inventaire.regroupement r, inventaire.inventaires i, inventaire.les_factures_complet f, 
     inventaire.planco_amort pa, jefy_depense.v_fournisseur fo
where r.rgp_ordre=i.rgp_ordre and rgp_exer=f.exer(+) and r.dep_ordre=f.dep_ordre(+) and f.pco_num=pa.pco_num(+) and
     f.fou_ordre=fo.fou_ordre(+) 
 /*and rgp_numero in ('962/CRI/06/8','962/CRI/06/9',
'962/CRI/06/10','962/CRI/06/11',
      '962/CRI/06/12','962/CRI/06/13')*/
 --and f.dep_fact in ('F06-001')
group by r.rgp_numero, r.rgp_exer, r.rgp_comp, r.rgp_cr, f.pco_num, pa.pco_duree, nvl(r.rgp_origine,'Autofinancement'),f.cde_num,f.organ,f.tcd_code,
       f.cde_prorata, f.man_num, f.bor_num,  r.rgp_comp, fo.fou_code,fo.fou_nom,f.dep_fact,f.dep_date,f.dep_mont,f.bor_date
order by sum(inv_ht)


select sum(montant) from jefy_inventaire.inventaire_imp where numero='980/RECH/03/11'

select sum(montant) from jefy_inventaire.inventaire_imp where numero='980/RECH/03/11' and invc_id is null