--requete d'insertion
select distinct
r.rgp_numero numero, r.rgp_exer exercice, r.rgp_comp ub, r.rgp_cr cr, f.pco_num imputation, pa.pco_duree duree, 'Lineaire' type_amort, 
    sum(i.inv_ht) montant, sum(i.inv_ht) montant_residuel, nvl(r.rgp_origine,'Autofinancement') origine,
       /*inv_serie*/ null serie, 'Cde : '||f.cde_num||chr(13)||chr(10)||'  Ligne budgetaire : '||f.organ||' TC : '||f.tcd_code||
       ' Prorata : '||f.cde_prorata||chr(13)||chr(10)--||
       /*inv_modele*/ informations, f.man_num mandat, f.bor_num bordereau,  r.rgp_comp gestion, fo.fou_code||' '||fo.fou_nom fournisseur, 
       'N� Facture : '||f.dep_fact||' du '||f.dep_date||', montant : '||f.dep_mont||chr(13)||chr(10)||
         '  Mandat : '||f.MAN_NUM||' du '||f.bor_date||', Bordereau : '||f.bor_num||chr(13)||chr(10)||
         '  Date de controle : '||chr(13)||chr(10)||'  Responsable du controle : '||chr(13)||chr(10)||
         '  Proprietaire : '||chr(13)||chr(10)||'  Localisation : '||chr(13)||chr(10) facture, null invc_id, f.bor_date date_acquisition
from inventaire.regroupement r, inventaire.inventaires i, inventaire.les_factures_complet f, 
     inventaire.planco_amort pa, jefy_depense.v_fournisseur fo
where r.rgp_ordre=i.rgp_ordre and rgp_exer=f.exer(+) and r.dep_ordre=f.dep_ordre(+) and f.pco_num=pa.pco_num(+) and
     f.fou_ordre=fo.fou_ordre(+) 
 and rgp_numero like '941/%'
group by r.rgp_numero, r.rgp_exer, r.rgp_comp, r.rgp_cr, f.pco_num, pa.pco_duree, nvl(r.rgp_origine,'Autofinancement'),f.cde_num,f.organ,f.tcd_code,
       f.cde_prorata, f.man_num, f.bor_num,  r.rgp_comp, fo.fou_code,fo.fou_nom,f.dep_fact,f.dep_date,f.dep_mont,f.bor_date
order by r.rgp_numero
       
-- v�rifie si un rgp_numero est plusieurs pr�sents dans la table et si au moins un d'eux est associ� � plusieurs biens
--   si �� arrive v�rifier la multiplicit� des donn�es ramen�es par la requ�te d'import
select rgp_numero, count(rgp_ordre) from inventaire.regroupement
where rgp_ordre in (select rgp_ordre from inventaire.inventaires group by rgp_ordre having count(inv_ordre)>1)
 and rgp_numero in ('980/SRV/05/17','980/SRV/06/17')
group by rgp_numero
having count(rgp_ordre)>1;


-- Verification que le montant budgetaire de la facture correspond a la somme des montants de l'inventaire 
select rgp_exer, dep_ordre, sum(montant_inventaire) montant_inventaire, sum(montant_facture) montant_facture
from (
select rgp_exer, dep_ordre, rgp_montant montant_inventaire, 0 montant_facture from inventaire.regroupement where dep_ordre is not null
and rgp_numero in ('980/SRV/05/17','980/SRV/06/17')
union all
select  rgp_exer, r.dep_ordre, 0, min(f.dep_mont) from inventaire.regroupement r, inventaire.les_factures_complet f
where r.rgp_exer=f.exer and r.dep_ordre=f.dep_ordre and r.dep_ordre is not null
and rgp_numero in ('980/SRV/05/17','980/SRV/06/17')
group by rgp_exer, r.dep_ordre
)
group by rgp_exer, dep_ordre
having sum(montant_inventaire)<>sum(montant_facture)
order by rgp_exer, dep_ordre

