
-- NUMERO, EXERCICE, UB, CR, IMPUTATION, DUREE, TYPE_AMORT, MONTANT, MONTANT_RESIDUEL, TYPE_FINANCEMENT,
-- NUMERO_SERIE, INFORMATIONS, MANDAT, BORDEREAU, GESTION, FOURNISSEUR, FACTURE, INVC_ID
--  champs en plus : DATE_ACQ, DATE_MANDAT, TYPE_CREDIT, PRORATA


-- GESTION = UB d'origine ? (ou 900 comme pour l'immobilier)

-- Informations Reprise :
--      Avance 30% plateforme interaction audio/vid�o
--      March� : 2010.000003.00.17.00.00
--      N�inventaire : 920/CPER/2011/14
--      Mdt 2588 Bde 493 imput 238 15510,99 HT
--      CDE : 7761
-- Factures :
--      N� Facture : 95748
--      Ligne budg�taire : 920/CPER/B11/EDUCATION

insert into jefy_inventaire.inventaire_imp
select jefy_inventaire.inventaire_imp_seq.nextval, liste.*
from (select distinct
r.rgp_numero numero, r.rgp_exer exercice, r.rgp_comp ub, r.rgp_cr cr, f.pco_num imputation, pa.pco_duree duree, 'Lineaire' type_amort, 
    sum(i.inv_ht) montant, sum(i.inv_ht) montant_residuel, nvl(r.rgp_origine,'Autofinancement') origine,
       /*inv_serie*/ null serie, 'Cde : '||f.cde_num||chr(13)||chr(10)||'  Ligne budgetaire : '||f.organ||' TC : '||f.tcd_code||
       ' Prorata : '||f.cde_prorata||chr(13)||chr(10)--||
       /*inv_modele*/ informations, f.man_num mandat, f.bor_num bordereau,  r.rgp_comp gestion, fo.fou_code||' '||fo.fou_nom fournisseur, 
       'N� Facture : '||f.dep_fact||' du '||f.dep_date||', montant : '||f.dep_mont||chr(13)||chr(10)||
         '  Mandat : '||f.MAN_NUM||' du '||f.bor_date||', Bordereau : '||f.bor_num||chr(13)||chr(10)||
         '  Date de controle : '||chr(13)||chr(10)||'  Responsable du controle : '||chr(13)||chr(10)||
         '  Proprietaire : '||chr(13)||chr(10)||'  Localisation : '||chr(13)||chr(10) facture, null invc_id, f.bor_date date_acquisition
from inventaire.regroupement r, inventaire.inventaires i, inventaire.les_factures_complet f, 
     inventaire.planco_amort pa, jefy_depense.v_fournisseur fo
where r.rgp_ordre=i.rgp_ordre and rgp_exer=f.exer(+) and r.dep_ordre=f.dep_ordre(+) and f.pco_num=pa.pco_num(+) and
     f.fou_ordre=fo.fou_ordre(+) and f.pco_num is not null
 and (rgp_numero in ('903/DITR/04/3','903/DITR/04/2'
          ) 
    /*or (rgp_numero in ('980/SRV/05/17') and f.man_num=644) or (rgp_numero in ('980/RECH/03/35') and f.man_num=308)*/ )
group by r.rgp_numero, r.rgp_exer, r.rgp_comp, r.rgp_cr, f.pco_num, pa.pco_duree, nvl(r.rgp_origine,'Autofinancement'),f.cde_num,f.organ,f.tcd_code,
       f.cde_prorata, f.man_num, f.bor_num,  r.rgp_comp, fo.fou_code,fo.fou_nom,f.dep_fact,f.dep_date,f.dep_mont,f.bor_date
order by r.rgp_exer, r.rgp_numero
) liste 
 
 
BEGIN 
  JEFY_INVENTAIRE.INTEGRATION.INTEGRATION_IMPORT_ALL;
  COMMIT; 
END; 