SET DEFINE OFF
--
--  
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié : JEFY_INVENTAIRE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.5.3
-- Date de publication : xx/xx/2013
-- Licence : CeCILL version 2
--
-- Evolutions :
--   - Correction bug effacement inventaire repris
--   - Enregistrement du montant d'origine de financement lors de l'enregistrement d'un inventaire comptable
--

whenever sqlerror exit sql.sqlcode ;

execute grhum.inst_patch_jefy_inv_1530;
commit;

drop procedure grhum.inst_patch_jefy_inv_1530;
