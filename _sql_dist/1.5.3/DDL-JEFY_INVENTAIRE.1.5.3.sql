SET DEFINE OFF
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié : JEFY_INVENTAIRE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.5.3
-- Realease
-- Date de publication : 15/07/2013
-- Licence : CeCILL version 2
--
-- Notes
-- API COMPTABLE
-- API INTEGRATION HB
-- API COROSSOL

--whenever sqlerror exit sql.sqlcode ;

-- On peut rejouer comme on veut le script en décommentant les deux lignes suivantes !
-- alter table JEFY_INVENTAIRE.inventaire_comptable_orig drop column icor_montant; 
-- delete from JEFY_INVENTAIRE.db_version where db_version_id='1530';




alter table jefy_inventaire.inventaire_comptable_orig add ( icor_montant number (12,2));
comment on column jefy_inventaire.inventaire_comptable_orig.icor_montant is 'Montant de la répartition du financement';

-- création  de vue pour les reports
-------------------------------------------------------
--  DDL for View V_CLE_INVENTAIRE_MONTANT
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "JEFY_INVENTAIRE"."V_CLE_INVENTAIRE_MONTANT" ("CLIC_ID", "MONTANT") AS 
  select clic_id, sum(invc_montant_acquisition) montant 
from jefy_inventaire.inventaire_comptable
group by clic_id;

--------------------------------------------------------
--  DDL for View V_ORIGF_MONTANT
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "JEFY_INVENTAIRE"."V_ORIGF_MONTANT" ("CLIC_ID", "ORGF_LIBELLE", "TIT_ID", "MONTANT") AS 
  select i.clic_id,  f.orgf_libelle orgf_libelle, o.tit_id tit_id, decode(total.montant, 0, null,sum(o.icor_montant)) montant
    from jefy_inventaire.inventaire_comptable_orig o, jefy_inventaire.origine_financement f,
       jefy_inventaire.inventaire_comptable i, jefy_inventaire.v_cle_inventaire_montant total
    where i.invc_id=o.invc_id and f.orgf_id=o.orgf_id  and i.clic_id=total.clic_id
    group by i.clic_id, f.orgf_libelle, o.tit_id, total.montant;

--------------------------------------------------------
--  DDL for View V_TITRE_BORDEREAU
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "JEFY_INVENTAIRE"."V_TITRE_BORDEREAU" ("TIT_ID", "EXE_ORDRE", "TIT_NUMERO", "BOR_NUM", "GES_CODE", "ORG_ORDRE", "BOR_ID") AS 
  select t.tit_id, t.exe_ordre, t.tit_numero, b.bor_num, NVL2 (o.org_id,o.org_ub||'/'||o.org_cr||NVL2(o.org_souscr,'/'||o.org_souscr,'') ,b.ges_code) ges_code, t.org_ordre, t.bor_id
from maracuja.bordereau b, maracuja.titre t, jefy_admin.organ o 
where b.bor_id=t.bor_id
and o.org_id(+)=t.org_ordre;

-- Vue modifié car il loupait des inventaires avec des livraisons differentes
--------------------------------------------------------
--  DDL for View V_INVENTAIRE_EXPORT
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "JEFY_INVENTAIRE"."V_INVENTAIRE_EXPORT" ("ORGF_ID", "INVC_MONTANT_ACQUISITION", "INVC_MONTANT_RESIDUEL", "LIV_ORDRE", "LID_ORDRE", "INV_ID", "INVC_ID", "CLIC_ID", "COMM_ID", "ORG_ID", "TCD_ORDRE", "PCO_NUM", "LID_MONTANT", "CLIC_NUM_COMPLET", "DPCO_ID") AS 
  SELECT orgf_id, invc_montant_acquisition, invc_montant_residuel,
          bien.liv_ordre, bien.lid_ordre, bien.inv_id, bien.invc_id,
          cic.clic_id, bien.comm_id, bien.org_id, bien.tcd_ordre,
          cic.pco_num, bien.lid_montant, cic.clic_num_complet, ic.dpco_id
     FROM (SELECT min(l.liv_ordre) liv_ordre, min(ld.lid_ordre) lid_ordre, min(i.inv_id) inv_id, i.invc_id invc_id, min(l.comm_id) comm_id,
                  min(ld.org_id) org_id, min(ld.tcd_ordre) tcd_ordre, min(ld.pco_num) pco_num, min(ld.lid_montant) lid_montant
             FROM jefy_inventaire.livraison l,
                  jefy_inventaire.livraison_detail ld,
                  jefy_inventaire.inventaire i,
                  jefy_depense.commande comm
            WHERE ld.lid_ordre = i.lid_ordre
              AND l.liv_ordre = ld.liv_ordre
              AND l.comm_id=comm.comm_id 
              AND comm.tyet_id<>207
            GROUP BY invc_id ) bien,
          jefy_inventaire.inventaire_comptable ic,
          jefy_inventaire.cle_inventaire_comptable cic,
          jefy_depense.commande cde
    WHERE bien.invc_id = ic.invc_id AND ic.clic_id = cic.clic_id and bien.comm_id=cde.comm_id and cde.tyet_id<>207
   UNION ALL
   SELECT orgf_id, invc_montant_acquisition, invc_montant_residuel, NULL,
          NULL, NULL, ic.invc_id, cic.clic_id, NULL, NULL, NULL, cic.pco_num,
          ic.invc_montant_acquisition, cic.clic_num_complet, ic.dpco_id
     FROM jefy_inventaire.inventaire i,
          jefy_inventaire.inventaire_comptable ic,
          jefy_inventaire.cle_inventaire_comptable cic
    WHERE ic.invc_id = i.invc_id(+) AND ic.clic_id = cic.clic_id
          AND i.invc_id IS NULL;

--------------------------------------------------------
--  DDL for Package Body API_COMPTABLE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_INVENTAIRE"."API_COMPTABLE" AS

 procedure supprimer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type
) is
   my_nb             integer;
begin
     select count(*) into my_nb from inventaire_comptable where dgco_id in
        (select dgco_id from degressif_coef where dgrf_id=a_dgrf_id);
     if my_nb>0 then
        raise_application_error(-20001,'Suppression impossible, des coefficients sont utilisés par des inventaires comptables');
     end if;

     delete from degressif_coef where dgrf_id=a_dgrf_id;
     delete from degressif where dgrf_id=a_dgrf_id;
end;

 procedure enregistrer_degressif (
      a_dgrf_id           in out degressif.dgrf_id%type,
      a_dgrf_date_debut          degressif.dgrf_date_debut%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type,
      a_chaine_degressif_coef    varchar2
) is
   my_nb             integer;
   my_chaine         varchar2(20000);
   my_dgco_duree_min degressif_coef.dgco_duree_min%type;
   my_dgco_duree_max degressif_coef.dgco_duree_max%type;
   my_dgco_coef      degressif_coef.dgco_coef%type;
   my_dgco_id        degressif_coef.dgco_id%type;
   my_dgrf_id        degressif.dgrf_id%type;
begin
  my_nb:=0;

  if a_dgrf_date_debut is null then
     raise_application_error(-20001,'La date de debut ne doit pas etre nulle !');
  end if;

  if a_dgrf_id is not null then
     -- verifie la non utilisation des coefficients
     select count(*) into my_nb from inventaire_comptable where dgco_id in
        (select dgco_id from degressif_coef where dgrf_id=a_dgrf_id);
     if my_nb>0 then
        raise_application_error(-20001,'Des coefficients sont utilisés par des inventaires comptables, on ne peut donc rien modifier');
     end if;

     -- verifie les chevauchements de dates
     if a_dgrf_date_fin is null then
        select count(*) into my_nb from degressif where dgrf_id<>a_dgrf_id and (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     else
        select count(*) into my_nb from degressif where dgrf_id<>a_dgrf_id and (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null)
           and (dgrf_date_debut<a_dgrf_date_fin);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     end if;

     update degressif set dgrf_date_debut=a_dgrf_date_debut, dgrf_date_fin=a_dgrf_date_fin where dgrf_id=a_dgrf_id;
  else
     -- on cloture la periode précédente si elle existe
     select count(*) into my_nb from degressif where a_dgrf_date_debut>dgrf_date_debut and dgrf_date_fin is null;
     if my_nb>0 then
        select min(dgrf_id) into my_dgrf_id from degressif where a_dgrf_date_debut>dgrf_date_debut and dgrf_date_fin is null;
        set_degressif_date_fin(my_dgrf_id, to_date(to_char(a_dgrf_date_debut-1,'dd/mm/yyyy'),'dd/mm/yyyy'));
     end if;

     -- verifie les chevauchements de dates
     if a_dgrf_date_fin is null then
        select count(*) into my_nb from degressif where (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     else
        select count(*) into my_nb from degressif where (a_dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null)
           and (dgrf_date_debut<a_dgrf_date_fin);
        if my_nb>0 then
           raise_application_error(-20001,'Probleme de chevauchement de dates !');
        end if;
     end if;

     select degressif_seq.nextval into a_dgrf_id from dual;
     insert into degressif values (a_dgrf_id, a_dgrf_date_debut, a_dgrf_date_fin);
  end if;

  delete from degressif_coef where dgrf_id=a_dgrf_id;
  my_chaine:=a_chaine_degressif_coef;
  loop
      if substr(my_chaine,1,1)='$' then exit; end if;

      -- on recupere la duree min
      select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_dgco_duree_min from dual;
      my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
      -- on recupere la diuree max
      select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_dgco_duree_max from dual;
      my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
      -- on recupere le coef
      select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_dgco_coef from dual;
      my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));

      if my_dgco_duree_min is null then
         raise_application_error(-20001, 'La duree minimale ne doit pas etre nulle');
      end if;
      if my_dgco_duree_max is null then
         raise_application_error(-20001, 'La duree maximale ne doit pas etre nulle');
      end if;
      if my_dgco_duree_min>my_dgco_duree_max then
         raise_application_error(-20001, 'La duree maximale doit etre superieure a la duree minimale');
      end if;

      select count(*) into my_nb from degressif_coef
        where dgrf_id=a_dgrf_id and dgco_duree_min<=my_dgco_duree_max and dgco_duree_max>=my_dgco_duree_min;
      if my_nb>0 then
         raise_application_error(-20001, '2 coefficients ont des periodes qui se chevauchent');
      end if;

      -- insertion dans la base.
      select degressif_coef_seq.nextval into my_dgco_id from dual;
      insert into degressif_coef values (my_dgco_id, a_dgrf_id, my_dgco_duree_min, my_dgco_duree_max, my_dgco_coef);
  end loop;

end;

procedure set_degressif_date_fin (
      a_dgrf_id                  degressif.dgrf_id%type,
      a_dgrf_date_fin            degressif.dgrf_date_fin%type
) is
  my_degressif     degressif%rowtype;
   my_nb           integer;
begin
  select * into my_degressif from degressif where dgrf_id=a_dgrf_id;
  if my_degressif.dgrf_date_debut>=a_dgrf_date_fin or a_dgrf_date_fin is null then
     raise_application_error(-20001, 'la date de fin doit etre superieure a la date de debut');
  end if;

  -- verifier qu'il n'y a pas deux periodes se chevauchant
  select count(*) into my_nb from degressif where dgrf_id<>a_dgrf_id and (my_degressif.dgrf_date_debut<dgrf_date_fin or dgrf_date_fin is null)
     and (dgrf_date_debut<a_dgrf_date_fin);
  if my_nb>0 then
     raise_application_error(-20001,'Probleme de chevauchement de dates !');
  end if;

  update degressif set dgrf_date_fin=a_dgrf_date_fin where dgrf_id=a_dgrf_id;
end;


PROCEDURE enregistrer_comptable(
      a_clic_id           in out inventaire_comptable.clic_id%type,
      a_dpco_id                  inventaire_comptable.dpco_id%type,
      a_exe_ordre                inventaire_comptable.exe_ordre%type,
      a_invc_date_acquisition    inventaire_comptable.invc_date_acquisition%type,
      a_invc_etat                inventaire_comptable.invc_etat%type,
      a_invc_id           in out inventaire_comptable.invc_id%type,
      a_invc_montant_acquisition inventaire_comptable.invc_montant_acquisition%type,
      a_invc_montant_residuel    inventaire_comptable.invc_montant_residuel%type,
      a_chaine_origine           varchar2,
      a_utl_ordre                inventaire_comptable.utl_ordre%type,
      a_invc_date_sortie         inventaire_comptable.invc_date_sortie%type,
      a_dgco_id                  inventaire_comptable.dgco_id%type,
      a_chaine_inventaire        varchar2,

      a_clic_comp                cle_inventaire_comptable.clic_comp%type,
      a_clic_cr                  cle_inventaire_comptable.clic_cr%type,
      a_clic_duree_amort         cle_inventaire_comptable.clic_duree_amort%type,
      a_clic_etat                cle_inventaire_comptable.clic_etat%type,
      a_clic_nb_etiquette        cle_inventaire_comptable.clic_nb_etiquette%type,
      a_clic_pro_rata            cle_inventaire_comptable.clic_pro_rata%type,
      a_clic_type_amort          cle_inventaire_comptable.clic_type_amort%type,
      a_pco_num                  cle_inventaire_comptable.pco_num%type,
      a_pcoa_id                  cle_inventaire_comptable.pcoa_id%type
) IS
  my_chaine           varchar2(2000);
  my_nb               integer;
  my_inv_id           inventaire.inv_id%type;
  my_clic_numero      cle_inventaire_comptable.clic_numero%type;
  my_cle_inventaire_comptable cle_inventaire_comptable%rowtype;
  my_recalcul_autre   integer;
  my_invc_id          inventaire_comptable.invc_id%type;
  my_orgf_id          inventaire_comptable.orgf_id%type;
  my_invc_date_acquisition inventaire_comptable.invc_date_acquisition%type; 
  cursor c1 is select invc_id from inventaire_comptable where clic_id=a_clic_id;
BEGIN
  my_nb:=0;
  my_recalcul_autre:=0;

  if a_invc_id is not null then
     select count(*) into my_nb from inventaire_comptable where invc_id=a_invc_id;
  end if;

  if a_invc_id is null or my_nb=0 then
     if a_clic_id is null then
       if a_clic_comp is null and a_clic_cr is null and a_exe_ordre is null then
          raise_application_error(-20001, 'Vous devez renseigner l''UB, le CR et l''exercice');
       end if;

       my_clic_numero:=inventaire_numero.get_numero_comptable(a_exe_ordre, a_clic_comp, a_clic_cr);
       select cle_inventaire_comptable_seq.nextval into a_clic_id from dual;

       insert into cle_inventaire_comptable values (a_clic_comp, a_clic_cr, a_clic_duree_amort, a_clic_etat, a_clic_id, a_clic_nb_etiquette,
          my_clic_numero, a_clic_pro_rata, a_clic_type_amort, a_exe_ordre, a_pco_num, a_pcoa_id,
          inventaire_numero.get_numero_complet(a_exe_ordre, a_clic_comp, a_clic_cr, my_clic_numero));
     else
       select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=a_clic_id;
       if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort then
          insert into cle_inventaire_compt_modif values (cle_inventaire_compt_modif_seq.nextval, a_clic_id, a_utl_ordre, my_cle_inventaire_comptable.clic_duree_amort,
              a_clic_duree_amort, sysdate);
       end if;
       if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort or my_cle_inventaire_comptable.clic_pro_rata<>a_clic_pro_rata or
          my_cle_inventaire_comptable.clic_type_amort<>a_clic_type_amort then
            my_recalcul_autre:=1;
       end if;

       update cle_inventaire_comptable set clic_duree_amort=a_clic_duree_amort, clic_nb_etiquette=a_clic_nb_etiquette,
          clic_type_amort=a_clic_type_amort, clic_pro_rata=a_clic_pro_rata, pco_num=a_pco_num, pcoa_id=a_pcoa_id where clic_id=a_clic_id;
     end if;

     select inventaire_comptable_seq.nextval into a_invc_id from dual;
     insert into inventaire_comptable values(a_clic_id, a_dpco_id, a_exe_ordre, a_invc_date_acquisition, a_invc_etat, a_invc_id,
        a_invc_montant_acquisition, a_invc_montant_residuel, 1, a_utl_ordre, a_invc_date_sortie, a_dgco_id);
     my_orgf_id:=inventaire_origines(a_invc_id, a_chaine_origine);
     update inventaire_comptable set orgf_id=my_orgf_id where invc_id=a_invc_id;
  else
     select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=a_clic_id;
     if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort then
        insert into cle_inventaire_compt_modif values (cle_inventaire_compt_modif_seq.nextval, a_clic_id, a_utl_ordre, my_cle_inventaire_comptable.clic_duree_amort,
            a_clic_duree_amort, sysdate);
     end if;
     if my_cle_inventaire_comptable.clic_duree_amort<>a_clic_duree_amort or my_cle_inventaire_comptable.clic_pro_rata<>a_clic_pro_rata or
        my_cle_inventaire_comptable.clic_type_amort<>a_clic_type_amort  then
          my_recalcul_autre:=1;
     end if; 
     update cle_inventaire_comptable set clic_duree_amort=a_clic_duree_amort, clic_nb_etiquette=a_clic_nb_etiquette,
          clic_type_amort=a_clic_type_amort, clic_pro_rata=a_clic_pro_rata, pco_num=a_pco_num, pcoa_id=a_pcoa_id where clic_id=a_clic_id;
     my_orgf_id:=inventaire_origines(a_invc_id, a_chaine_origine);
     -- recherche si la date a changé pour recalcul
     select invc_date_acquisition into my_invc_date_acquisition from inventaire_comptable where invc_id=a_invc_id;
     if trunc(my_invc_date_acquisition) <> trunc(a_invc_date_acquisition) then
       my_recalcul_autre:=1;
     end if;
     update inventaire_comptable set orgf_id=my_orgf_id, utl_ordre=a_utl_ordre, dpco_id=a_dpco_id, invc_montant_acquisition=a_invc_montant_acquisition,
        invc_montant_residuel=a_invc_montant_residuel, dgco_id=a_dgco_id, invc_date_acquisition=a_invc_date_acquisition where invc_id=a_invc_id;
  end if;

  my_chaine:=a_chaine_inventaire;
  loop
     if my_chaine is null or length(my_chaine)=0 or substr(my_chaine,1,1)='$' then exit; end if;

     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_inv_id from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));

     select count(*) into my_nb from inventaire where inv_id=my_inv_id and invc_id is null;
     if my_nb>0 then
        update inventaire set invc_id=a_invc_id where inv_id=my_inv_id and invc_id is null;
     end if;
  end loop;

  if a_dpco_id is not null then
     api_corossol.AMORTISSEMENT(a_invc_id);
  end if;

  if my_recalcul_autre>0 then
    OPEN c1();
      LOOP
      FETCH  c1 INTO my_invc_id;
      EXIT WHEN c1%NOTFOUND;
        if a_invc_id<>my_invc_id then
           api_corossol.AMORTISSEMENT(my_invc_id);
        end if;
      END LOOP;
      CLOSE c1;
  end if;

END;

function inventaire_origines(a_invc_id inventaire_comptable.invc_id%type, a_chaine_origine varchar2)
return number
is
    my_chaine            varchar2(2000);
    my_inv_orgf_id       inventaire_comptable.orgf_id%type;

    my_icor_id           inventaire_comptable_orig.icor_id%type;
    my_orgf_id           inventaire_comptable_orig.orgf_id%type;
    my_icor_pourcentage  inventaire_comptable_orig.icor_pourcentage%type;
    my_icor_montant  inventaire_comptable_orig.icor_montant%type;
    my_somme_pourcentage inventaire_comptable_orig.icor_pourcentage%type;
    my_tit_id            inventaire_comptable_orig.tit_id%type;
begin
  my_chaine:=a_chaine_origine;
  my_inv_orgf_id:=null;
  my_somme_pourcentage:=0;

  delete from inventaire_comptable_orig where invc_id=a_invc_id;

  loop
     if my_chaine is null or length(my_chaine)=0 or substr(my_chaine,1,1)='$' then exit; end if;

     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_orgf_id from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_icor_pourcentage from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_tit_id from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
     -- chaine montant
     select grhum.en_nombre(substr(my_chaine,1,instr(my_chaine,'$')-1)) into my_icor_montant from dual;
     my_chaine:=substr(my_chaine,instr(my_chaine,'$')+1,length(my_chaine));
    
     if my_inv_orgf_id is null then my_inv_orgf_id:=my_orgf_id; end if;
     my_somme_pourcentage:=my_somme_pourcentage+my_icor_pourcentage;

     select inventaire_comptable_orig_seq.nextval into my_icor_id from dual;
     insert into inventaire_comptable_orig (icor_id, invc_id, orgf_id, icor_pourcentage, tit_id, icor_montant) 
                         values (my_icor_id, a_invc_id, my_orgf_id, my_icor_pourcentage, my_tit_id, my_icor_montant);
  end loop;

  if my_somme_pourcentage<>100.0 then
     raise_application_error(-20001, 'La somme des pourcentages des origines de financement doit etre egale a 100%');
  end if;

  return my_inv_orgf_id;
end;

/*PROCEDURE supprimer_sortie (
      a_invs_id           inventaire_sortie.invs_id%type
) IS
  my_invc_id inventaire.invc_id%type;
BEGIN
  select i.invc_id into my_invc_id from inventaire i, inventaire_sortie s where s.inv_id=i.inv_id and s.invs_id=a_invs_id;
  delete from inventaire_sortie where invs_id=a_invs_id;

  if my_invc_id is not null then
     api_corossol.AMORTISSEMENT(my_invc_id);
  end if;
END;*/

END;
/

--------------------------------------------------------
--  DDL for Package Body INTEGRATION_HB
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_INVENTAIRE"."INTEGRATION_HB" AS

procedure enregistrer_hb (
   a_INB_ID        in out NUMBER,
   a_UTL_ORDRE            NUMBER,
   a_EXE_ORDRE            NUMBER,
   a_ORG_ID               NUMBER,
   a_PCO_NUM              VARCHAR2,
   a_INB_DUREE            NUMBER,
   a_INB_TYPE_AMORT       VARCHAR2,
   a_INB_MONTANT          NUMBER,
   a_INB_MONTANT_RESIDUEL NUMBER,
   a_ORIG_ID              NUMBER,
   a_INB_NUMERO_SERIE     VARCHAR2,
   a_INB_INFORMATIONS     VARCHAR2,
   a_INB_FOURNISSEUR      VARCHAR2,
   a_INB_FACTURE          VARCHAR2,
   a_INVC_ID              NUMBER,
   a_INB_DATE_ACQUISITION date
) is
begin
   enregistrer_hb_comp(a_INB_ID, a_UTL_ORDRE, a_EXE_ORDRE, a_ORG_ID, a_PCO_NUM, a_INB_DUREE, a_INB_TYPE_AMORT,
   a_INB_MONTANT, a_INB_MONTANT_RESIDUEL, a_ORIG_ID, a_INB_NUMERO_SERIE, a_INB_INFORMATIONS,
   a_INB_FOURNISSEUR, a_INB_FACTURE, a_INVC_ID, a_INB_DATE_ACQUISITION, null);
end;

procedure enregistrer_hb_comp (
   a_INB_ID        in out NUMBER,
   a_UTL_ORDRE            NUMBER,
   a_EXE_ORDRE            NUMBER,
   a_ORG_ID               NUMBER,
   a_PCO_NUM              VARCHAR2,
   a_INB_DUREE            NUMBER,
   a_INB_TYPE_AMORT       VARCHAR2,
   a_INB_MONTANT          NUMBER,
   a_INB_MONTANT_RESIDUEL NUMBER,
   a_ORIG_ID              NUMBER,
   a_INB_NUMERO_SERIE     VARCHAR2,
   a_INB_INFORMATIONS     VARCHAR2,
   a_INB_FOURNISSEUR      VARCHAR2,
   a_INB_FACTURE          VARCHAR2,
   a_INVC_ID              NUMBER,
   a_INB_DATE_ACQUISITION date,
   a_inb_numero_inventaire inventaire_non_budgetaire_comp.inb_numero_inventaire%type
) is
   my_cpt integer;
begin
   if a_inb_id is null then
     select inventaire_non_budgetaire_seq.nextval into a_inb_id from dual;
   end if;

   select count(*) into my_cpt from inventaire_non_budgetaire where inb_id=a_inb_id;
   if my_cpt=0 then
      insert into inventaire_non_budgetaire values (a_inb_id, a_UTL_ORDRE, a_EXE_ORDRE, a_ORG_ID, a_PCO_NUM, a_INB_DUREE, a_INB_TYPE_AMORT,
          a_INB_MONTANT, a_INB_MONTANT_RESIDUEL, a_ORIG_ID, a_INB_NUMERO_SERIE, a_INB_INFORMATIONS, a_INB_FOURNISSEUR, a_INB_FACTURE,
          a_INVC_ID, a_INB_DATE_ACQUISITION);
      
      if a_inb_numero_inventaire is not null then
         insert into inventaire_non_budgetaire_comp values (a_inb_id, a_inb_numero_inventaire);
      end if;
      
      integrer_hb(a_inb_id);   
   else
      update inventaire_non_budgetaire set utl_ordre=a_utl_ordre, pco_num=a_pco_num, inb_duree=a_inb_duree, inb_type_amort=a_inb_type_amort,
          inb_montant=a_inb_montant, inb_montant_residuel=a_inb_montant_residuel, orig_id=a_orig_id, inb_numero_serie=a_inb_numero_serie, 
          inb_informations=a_inb_informations, inb_fournisseur=a_inb_fournisseur, inb_facture=a_inb_facture,
          inb_date_acquisition=a_inb_date_acquisition where inb_id=a_inb_id;

      select count(*) into my_cpt from inventaire_non_budgetaire_comp where inb_id=a_inb_id;
      if my_cpt=0 then
         if a_inb_numero_inventaire is not null then
            insert into inventaire_non_budgetaire_comp values (a_inb_id, a_inb_numero_inventaire);
         end if;
      else
         if a_inb_numero_inventaire is not null then
            update inventaire_non_budgetaire_comp set inb_numero_inventaire=a_inb_numero_inventaire where inb_id=a_inb_id;
         else 
            delete from inventaire_non_budgetaire_comp where inb_id=a_inb_id;
         end if;
      
      end if;

      modifier_hb(a_inb_id);   
   end if;
end;

procedure integrer_hb (inbid integer) is
   currentimp jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE%rowtype;
   CLICID  integer;
   INVCID  integer;
   my_icor_id integer;
   my_nb   integer;
   numero  integer;
   comp    jefy_admin.organ.org_ub%type;
   cr      jefy_admin.organ.org_cr%type;
   exer    jefy_admin.exercice.exe_exercice%type;
   my_clic_num_complet cle_inventaire_comptable.clic_num_complet%type;
   my_prorata_temporis integer;
   my_pcoa_id cle_inventaire_comptable.pcoa_id%type;
BEGIN

   select * into currentimp from jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE where inb_id = inbid;

   -- recup des infoamtions de la ligne budgetaire
   select org_ub,org_cr into comp,cr from jefy_admin.organ where org_id = currentimp.org_id;

   -- recup de lexercice
   select exe_exercice into exer from  jefy_admin.exercice where exe_ordre = currentimp.exe_ordre;

   -- recup prorata temporis ou non
   select decode(nvl(par_value,'NON'),'NON',0,1) into my_prorata_temporis from parametre 
      where par_key='PRORATA_TEMPORIS' and exe_ordre=currentimp.exe_ordre;

   -- insertion dans la table
   my_pcoa_id:=null;
   if currentimp.inb_duree>0 then 
      my_pcoa_id:=jefy_inventaire.INTEGRATION.get_pcoaid(currentimp.exe_ordre,currentimp.pco_num);
   end if;

   clicid:=null;
   
   select count(*) into my_nb from inventaire_non_budgetaire_comp where inb_id=inbid;
   if my_nb=1 then
      select inb_numero_inventaire into my_clic_num_complet from inventaire_non_budgetaire_comp where inb_id=inbid;
      select count(*) into my_nb from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE  where clic_num_complet=my_clic_num_complet;
      if my_nb=1 then
         select clic_id into clicid from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE  where clic_num_complet=my_clic_num_complet;
      end if;
   end if;
     
   if clicid is null then 
      select CLE_INVENTAIRE_COMPTABLE_SEQ.nextval into CLICID from dual;  
      insert into  JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE 
        (CLIC_COMP,CLIC_CR,CLIC_DUREE_AMORT,CLIC_ETAT,CLIC_ID,CLIC_NB_ETIQUETTE,CLIC_NUMERO,CLIC_PRO_RATA,CLIC_TYPE_AMORT,
          EXE_ORDRE,PCO_NUM,PCOA_ID,CLIC_NUM_COMPLET)
        values (comp, cr, currentimp.inb_duree, 'etat', CLICID, 0,'automatique', my_prorata_temporis, currentimp.INB_TYPE_AMORT,
          currentimp.exe_ordre, currentimp.pco_num, my_pcoa_id, null);
   end if;
   
   select count(*) into my_nb from inventaire_non_budgetaire_comp where inb_id=inbid;
   if my_nb=0 then
      numero := inventaire_numero.get_numero_comptable (exer,comp,cr);
      my_clic_num_complet:=inventaire_numero.get_numero_complet(exer,comp,cr,numero);
   else
       select inb_numero_inventaire into my_clic_num_complet from inventaire_non_budgetaire_comp where inb_id=inbid;
       if my_clic_num_complet is null then
          numero := inventaire_numero.get_numero_comptable (exer,comp,cr);
          my_clic_num_complet:=inventaire_numero.get_numero_complet(exer,comp,cr,numero);
       end if;
   end if; 


   -- maj de la cle d'inventaire  comptable
   update JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE set CLIC_NUMERO = nvl(numero,1), clic_num_complet=my_clic_num_complet 
      where CLIC_ID = CLICID;
 
   -- creation de l'inventaire comptable
   -- recup de la clef primaire
   select INVENTAIRE_COMPTABLE_SEQ.nextval into INVCID from dual;

   -- insertion dans la table 
   insert into  JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE 
      (CLIC_ID,DPCO_ID,EXE_ORDRE,INVC_DATE_ACQUISITION,INVC_ETAT,INVC_ID,INVC_MONTANT_ACQUISITION,INVC_MONTANT_RESIDUEL,ORGF_ID,UTL_ORDRE)
     values (CLICID,null, currentimp.exe_ordre, currentimp.inb_date_acquisition, null, INVCID, currentimp.INB_MONTANT, currentimp.INB_MONTANT_RESIDUEL,
       currentimp.ORiG_ID, currentimp.UTL_ORDRE);

   select inventaire_comptable_orig_seq.nextval into my_icor_id from dual;
   insert into jefy_inventaire.inventaire_comptable_orig (icor_id, invc_id, orgf_id, icor_pourcentage, tit_id, icor_montant) 
   values (my_icor_id, invcid, currentimp.ORiG_ID, 100, null,null);
   
   -- creation des amortissments
   if currentimp.inb_duree <> 0 then
      api_corossol.creer_lien_inv_dep (-1000, invcid,currentimp.INB_MONTANT_RESIDUEL);
   end if;

   -- on marque l'enregistrement import? et on conserve le liens avec l'inventaire_comptable
   update INVENTAIRE_NON_BUDGETAIRE set invc_id = invcid where inb_id = inbid;
end;

procedure modifier_hb (inbid integer) is 
   my_inventaire                jefy_inventaire.inventaire_non_budgetaire%rowtype;
   my_inventaire_comptable      jefy_inventaire.inventaire_comptable%rowtype;
   my_cle_inventaire_comptable  jefy_inventaire.cle_inventaire_comptable%rowtype;
   my_pcoa_id                   cle_inventaire_comptable.pcoa_id%type;
begin
   select * into my_inventaire from jefy_inventaire.inventaire_non_budgetaire where inb_id=inbid;
 
   log_hb(inbid,null);

   if my_inventaire.invc_id is null then
      integrer_hb(my_inventaire.inb_id);
   else
       select * into my_inventaire_comptable from inventaire_comptable where invc_id=my_inventaire.invc_id;
       select * into my_cle_inventaire_comptable from cle_inventaire_comptable where clic_id=my_inventaire_comptable.clic_id;
       
       my_pcoa_id:=null;
       if my_inventaire.inb_duree>0 then 
         my_pcoa_id:=jefy_inventaire.INTEGRATION.get_pcoaid(my_inventaire.exe_ordre, my_inventaire.pco_num);
       end if;

       api_comptable.enregistrer_comptable(my_inventaire_comptable.clic_id, my_inventaire_comptable.dpco_id, my_inventaire.exe_ordre, my_inventaire.inb_date_acquisition,
         null, my_inventaire.invc_id, my_inventaire.inb_montant_residuel, my_inventaire.inb_montant_residuel, my_inventaire.orig_id||'$100$$'||my_inventaire.inb_montant_residuel||'$', my_inventaire.utl_ordre,
         my_inventaire_comptable.invc_date_sortie, null,null, my_cle_inventaire_comptable.clic_comp, my_cle_inventaire_comptable.clic_cr,
         my_inventaire.inb_duree, my_cle_inventaire_comptable.clic_etat, my_cle_inventaire_comptable.clic_nb_etiquette, my_cle_inventaire_comptable.clic_pro_rata,
         my_inventaire.inb_type_amort, my_inventaire.pco_num, my_pcoa_id);     
   end if;
end;

procedure supprimer_hb (inbid integer) 
is 
   invcid jefy_inventaire.inventaire_non_budgetaire.invc_id%type;
   my_nb integer;
   clicid jefy_inventaire.cle_inventaire_comptable.clic_id%type;
begin
   -- recup des infos 
   select invc_id into invcid from jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE where inb_id = inbid;

   if invcid is not null then
      select count(*) into my_nb from inventaire_compt_sortie where invc_id=invcid;
      if my_nb>0 then
         raise_application_error(-20001, 'On en peut pas supprimer une reprise d''inventaire sur laquelle il y a eu des sorties comptables');
      end if;
   
      UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id = NULL WHERE invc_id = invcid;
      Api_Corossol.amort_vider(invcid);
 
      -- suppression, recup du clicid
      select count(*) into clicid from jefy_inventaire.inventaire_comptable where invc_id = invcid;
      if (clicid != 0 ) then
         select clic_id into clicid from jefy_inventaire.inventaire_comptable where invc_id = invcid;
         select count(*) into my_nb from inventaire_comptable where invc_id<>invcid and clic_id=clicid;
         if my_nb=0 then
		    delete from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPT_MODIF where  clic_id = clicid;
            delete from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE where  clic_id = clicid;
         end if;
         delete from jefy_inventaire.amortissement where invc_id = invcid;
         delete from jefy_inventaire.INVENTAIRE_COMPTABLE_orig where invc_id = invcid;
         delete from jefy_inventaire.INVENTAIRE_imp where invc_id = invcid;
         delete from jefy_inventaire.INVENTAIRE_COMPTABLE where invc_id = invcid;
      end if;
   end if;
 
   log_hb(inbid,null);

   delete from jefy_inventaire.inventaire_non_budgetaire_comp where inb_id=inbid;
   delete from jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE where inb_id = inbid;
end;

procedure log_hb(a_inb_id integer, a_utl_ordre integer)
is
   my_zinb_id   z_inventaire_non_budget.zinb_id%type;
begin
   select z_inventaire_non_budget_seq.nextval into my_zinb_id from dual;

   insert into z_inventaire_non_budget select my_zinb_id, sysdate, a_utl_ordre, e.*
      from inventaire_non_budgetaire e where inb_id=a_inb_id;

   insert into z_inventaire_non_budget_c select my_zinb_id, e.*
      from inventaire_non_budgetaire_comp e where inb_id=a_inb_id;
end;

END INTEGRATION_HB;
/

--------------------------------------------------------
--  DDL for Package Body API_COROSSOL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_INVENTAIRE"."API_COROSSOL" AS

PROCEDURE creer_lien_inv_dep (
   dpcoid  inventaire_comptable.dpco_id%type, 
   invcid  inventaire_comptable.invc_id%type,
   montant inventaire_comptable.invc_montant_acquisition%type
) IS 
  depidrev integer;
  orv number(12,2);
  pco      maracuja.plan_comptable.PCO_NUM%type;
  my_date  JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.invc_date_acquisition%type;
  clicid   JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.clic_id%type;
  exeordre JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE.exe_ordre%type;
  old_pcoa_id JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE.pcoa_id%type;
  old_clic_duree_amort JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE.clic_duree_amort%type;
  old_pco_num JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE.pco_num%type;
  new_pcoa_id JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE.pcoa_id%type;
  new_clic_duree_amort JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE.clic_duree_amort%type;
BEGIN
  orv := 0;
  my_date:=null;
  
  -- ORV
  if montant<0 then
    -- creation de l'enregistrment dans INVENTAIRE_COMPTABLE_ORV
    insert into INVENTAIRE_COMPTABLE_ORV (invc_id,dpco_id,invo_id,invo_montant_orv) 
                 values (invcid,dpcoid,INVENTAIRE_COMPTABLE_ORV_seq.nextval,montant);
  else

    if (dpcoid != -1000) then -- cas des integrations HB
       -- recup du compte d'imputation de la liquidation
       select pc.pco_num, dpp_date_service_fait into pco, my_date 
          from jefy_depense.depense_papier p, jefy_depense.depense_budget b, jefy_depense.depense_ctrl_planco pc
          where b.dep_id=pc.dep_id and b.dpp_id=p.dpp_id and pc.dpco_id=dpcoid;
       UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET invc_date_acquisition=my_date WHERE invc_id=invcid;
    end if;

    -- recup du numero comptable
    select clic_id,EXE_ORDRE into clicid,exeordre from JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE where invc_id = invcid;

    if (dpcoid != -1000) then
     -- Récupération des anciens paramètres
     select pco_num, pcoa_id, clic_duree_amort into old_pco_num, old_pcoa_id, old_clic_duree_amort 
     from JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE 
     where clic_id = clicid;
     
     -- Récupération des nouveaux paramètres
     new_pcoa_id := substr(integration.get_pcoaidduree(exeordre, pco),1, instr(integration.get_pcoaidduree(exeordre, pco),'$')-1);
     new_clic_duree_amort := substr(integration.get_pcoaidduree(exeordre, pco),instr(integration.get_pcoaidduree(exeordre, pco),'$')+1);
    
     -- Test pour mettre à jour la durée 
     -- Limitation si la durée du même PCO_NUM/PCOAID a changé on ne voit que du feu
     if (old_pco_num<>pco) or (old_pcoa_id<>new_pcoa_id) or (old_clic_duree_amort is null) then   
        -- Correction CH  maj du compte d'amortissement et de la duree 
        update JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE set pco_num = pco,
             PCOA_ID =  new_pcoa_id,
             CLIC_DUREE_AMORT = new_clic_duree_amort
        where clic_id = clicid;
     end if;
    end if;

    -- maj inventaire_comptable : creation su liens
    UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id = dpcoid, invc_montant_acquisition=montant + nvl(orv,0),
      invc_montant_residuel = montant + nvl(orv,0) WHERE invc_id = invcid;
  end if;

  -- CREATION DE L AMORTISSEMENT
  amortissement(invcid);
END;
 
PROCEDURE supprimer_lien_inv_dep (dpcoid inventaire_comptable.dpco_id%type) IS 
   cursor c1 is select invc_id from inventaire_comptable where dpco_id=dpcoid;
   invcid jefy_inventaire.inventaire_comptable.invc_id%type;
BEGIN
  -- on traite les ORVS 
  delete from jefy_inventaire.INVENTAIRE_COMPTABLE_ORV where dpco_id=dpcoid;
 
  open c1;
  loop
    fetch c1 into invcid;
    exit when c1%notfound;
    -- maj  inventaire_comptable : casse le lien
    UPDATE jefy_inventaire.INVENTAIRE_COMPTABLE SET dpco_id=NULL WHERE dpco_id=dpcoid and invc_id=invcid;
    -- SUPPRESSION DE L AMORTISSEMENT
    amort_vider(invcid);
  end loop;
  close c1;
END;

PROCEDURE AMORTISSEMENT_CLE (
   a01clicid cle_inventaire_comptable.clic_id%type,
   a02annule integer
) is
  cursor c1 is select invc_id from inventaire_comptable where clic_id=a01clicid;
  invcid jefy_inventaire.inventaire_comptable.invc_id%type;
begin

  if a02annule = 1 then
    open c1;
    loop
      fetch c1 into invcid ;
      exit when c1%notfound;
      API_COROSSOL.amort_vider(invcid);
    end loop;
    close c1;
  else
    open c1;
    loop
      fetch c1 into invcid ;
      exit when c1%notfound;
      amort_vider(invcid);
      amortissement(invcid);
    end loop;
    close c1;
  end if;
end;

PROCEDURE amortissement(
   invcid inventaire_comptable.invc_id%type
) IS
  cpt INTEGER;
  tmpinvComptable jefy_inventaire.INVENTAIRE_COMPTABLE%ROWTYPE;
  tmpCleComptable jefy_inventaire.CLE_INVENTAIRE_COMPTABLE%ROWTYPE;
  tmpplancoamort  maracuja.plan_comptable_amo%ROWTYPE;
  montant_orvs    jefy_inventaire.INVENTAIRE_COMPTABLE_ORV.INVO_MONTANT_ORV%type;
  my_dgco_coef    degressif_coef.dgco_coef%type;
  my_date_calcul  jefy_depense.depense_papier.dpp_date_service_fait%type;
  my_dgco_id      degressif_coef.dgco_id%type;
  my_exe_ordre inventaire_comptable.exe_ordre%type;
BEGIN
  -- recup des infos du numero comptable
  SELECT COUNT(*) INTO cpt FROM jefy_inventaire.INVENTAIRE_COMPTABLE WHERE invc_id = invcid ;
  IF cpt = 0 THEN
    RAISE_APPLICATION_ERROR (-20001,'inventaire comptable introuvable, invc_id='||invcid);
  END IF;

  SELECT * INTO tmpinvComptable FROM jefy_inventaire.INVENTAIRE_COMPTABLE WHERE invc_id = invcid ;
  SELECT nvl(sum(INVO_MONTANT_ORV),0)  INTO montant_orvs FROM jefy_inventaire.INVENTAIRE_COMPTABLE_ORV WHERE invc_id = invcid ;
  SELECT * INTO tmpCleComptable FROM jefy_inventaire.CLE_INVENTAIRE_COMPTABLE WHERE clic_id = tmpinvComptable.clic_id ;

  SELECT COUNT(*) INTO cpt FROM maracuja.plan_comptable_amo WHERE pcoa_id = tmpCleComptable.pcoa_id;
  IF cpt = 0 THEN
    raise_application_error (-20001,'compte d amortissement inexistant verifier votre parametrage dans maracuja '||tmpclecomptable.pcoa_id);
  end if;

  SELECT * INTO tmpplancoamort FROM maracuja.plan_comptable_amo WHERE pcoa_id = tmpCleComptable.pcoa_id;

  my_date_calcul:=tmpinvcomptable.invc_date_acquisition;

  if tmpinvComptable.dpco_id is not null then
    if my_date_calcul is null then
       select count(*) into cpt from jefy_depense.depense_ctrl_planco where dpco_id=tmpinvcomptable.dpco_id;
       if cpt>0 then
          select dpp_date_service_fait into my_date_calcul
             from jefy_depense.depense_papier p, jefy_depense.depense_budget b, jefy_depense.depense_ctrl_planco pc
             where b.dep_id=pc.dep_id and pc.dpco_id=tmpinvcomptable.dpco_id and b.dpp_id=p.dpp_id;
       end if;
    end if;

    my_dgco_coef:=null;
    my_dgco_id:=tmpinvComptable.dgco_id;
    if my_dgco_id is not null then
      select max(dgco_id) into my_dgco_id from degressif_coef where
        dgrf_id in (select dgrf_id from degressif where dgrf_date_debut<=my_date_calcul and (dgrf_date_fin>=my_date_calcul or dgrf_date_fin is null))
        and dgco_duree_min<=tmpCleComptable.clic_duree_amort and dgco_duree_max>=tmpCleComptable.clic_duree_amort;
    end if;
    if my_dgco_id is not null then
      select dgco_coef into my_dgco_coef from degressif_coef where dgco_id=my_dgco_id;
    end if;
    
  -- On met l'année d'exercice au bon niveau
    my_exe_ordre:=to_number(to_char(my_date_calcul,'yyyy'));
    if my_exe_ordre<tmpinvComptable.exe_ordre then
        my_exe_ordre:=tmpinvComptable.exe_ordre;
    end if;
    
    IF tmpCleComptable.clic_type_amort = 'Lineaire' THEN
      Api_Corossol.amort_lineaire(invcid,tmpinvComptable.invc_montant_acquisition+montant_orvs,tmpCleComptable.clic_duree_amort,
         tmpCleComptable.pco_num,tmpplancoamort.pcoa_num,my_exe_ordre, tmpCleComptable.clic_pro_rata,my_date_calcul);
    ELSE
      Api_Corossol.amort_degressif(invcid,tmpinvComptable.invc_montant_acquisition+montant_orvs,tmpCleComptable.clic_duree_amort,
       tmpCleComptable.pco_num,tmpplancoamort.pcoa_num,my_exe_ordre, tmpCleComptable.clic_pro_rata,my_dgco_coef, my_date_calcul);
    END IF;
  end if;
END;

PROCEDURE amort_lineaire(
   invcid   inventaire_comptable.invc_id%type,
   montant  inventaire_comptable.invc_montant_acquisition%type,
   duree    cle_inventaire_comptable.clic_duree_amort%type,
   pco      cle_inventaire_comptable.pco_num%type,
   pcoamort jefy_inventaire.amortissement.pco_num%type,
   exeordre inventaire_comptable.exe_ordre%type,
   prorata  cle_inventaire_comptable.clic_pro_rata%type,
   la_date  inventaire_comptable.invc_date_acquisition%type
) IS 
  cle         jefy_inventaire.amortissement.amo_id%type;
  annuite     jefy_inventaire.amortissement.amo_annuite%type;
  annuite_ref jefy_inventaire.amortissement.amo_annuite%type;
  cumul       jefy_inventaire.amortissement.amo_cumul%type;
  residuel    jefy_inventaire.amortissement.amo_residuel%type;
  iter        integer;
  thefirst    integer;
  my_nb       integer;
  my_nb_decimales  NUMBER;

  my_nb_amort_anterieur integer;
  my_cumul_anterieur    jefy_inventaire.amortissement.amo_cumul%type;

  myexeordre inventaire_comptable.exe_ordre%type;

  cursor c1 is select * from inventaire_compt_sortie where invc_id=invcid and invcs_vnc>0;
  my_inventaire_compt_sortie jefy_inventaire.inventaire_compt_sortie%rowtype;
  exe_ordre_sortie     jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_amor       jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_debut      jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_fin        jefy_inventaire.amortissement.exe_ordre%type;
  my_amortissement     jefy_inventaire.amortissement.amo_annuite%type;
  my_residuel          jefy_inventaire.amortissement.amo_residuel%type;
  my_cumul             jefy_inventaire.amortissement.amo_cumul%type;
  my_acquisition       jefy_inventaire.amortissement.amo_acquisition%type;
  my_annees_passees    integer;
BEGIN

  if prorata<>0 and la_date is null then
     raise_application_error (-20001,'Pour un amortissement prorata temporis il faut indiquer une date de debut d''amortissement.');  
  end if;

  amort_vider(invcid);

  my_annees_passees:=0;
  if la_date is not null then
    my_annees_passees:=exeordre-to_number(to_char(la_date,'yyyy'));
    if my_annees_passees<0 then my_annees_passees:=0; end if;
  end if;
 
  my_nb_decimales:=get_nb_decimales(exeordre);

  iter :=0;
  cumul:=0;
  thefirst:=0;
  residuel:=montant;
  
  select count(*) into my_nb_amort_anterieur from amortissement where invc_id=invcid;

  if my_nb_amort_anterieur>0 then
    select sum(amo_annuite) into my_cumul_anterieur from amortissement where invc_id=invcid;
    annuite_ref:=(montant-my_cumul_anterieur)/(duree-my_nb_amort_anterieur-my_annees_passees);
  else
    if duree-my_annees_passees=0 then
       annuite_ref:=montant;
    else
       if prorata=0 and my_annees_passees>0 /*and duree-my_annees_passees-1<>0*/ then
       annuite_ref:=montant/(duree-my_annees_passees+1);
       /*else
       annuite_ref:=montant/(duree-my_annees_passees);*/
       end if;
    end if;

    if annuite_ref is null then 
       annuite_ref:=montant/(duree-my_annees_passees);
    end if;
  end if;
  
  myexeordre:=exeordre-my_annees_passees;

  LOOP
    IF montant=0 or duree=0 or residuel=0 THEN EXIT; END IF;

     if prorata=0 and my_annees_passees>0 and thefirst=0 then
        iter:=0;
     else
        iter:=iter+1;
     end if;

    thefirst:=thefirst+1;
    
    -- si on a laisse un amortissement c'est qu'on ne veut pas le recalculer pour cette annee la        
    select count(*) into my_nb from amortissement where invc_id=invcid and exe_ordre=myexeordre+iter;
    if my_nb=0 then
       -- calcul des valeurs
       annuite:=annuite_ref;
       
       if prorata<>0 and iter=my_annees_passees+1 then
          annuite:=annuite*taux_premiere_annee(exeordre, la_date);
       end if;

       if iter<=my_annees_passees and ((my_annees_passees=0 and iter=0) or (my_annees_passees>0 and prorata>0) 
            or (my_annees_passees>0 and prorata=0 and iter<my_annees_passees)) then
          annuite:=0;
       end if;

       annuite:=round(annuite, my_nb_decimales);

       if annuite>residuel or (iter>=my_annees_passees and (prorata=0 and iter>=duree) OR (prorata<>0 and iter>=duree+1)) then
        annuite:=residuel; 
       end if;
       if annuite<0 then annuite:=0; end if;

       cumul:=cumul+annuite;
       residuel:=montant-cumul;
    
       -- insertion
       select amortissement_seq.nextval into cle from dual;
       if prorata=0 then
       insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
          values (montant,annuite,cumul ,sysdate,cle,residuel,myexeordre+iter,invcid,pcoamort );
       else
         insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
           values (montant,annuite,cumul ,sysdate,cle,residuel,myexeordre+iter-1,invcid,pcoamort );
       end if;
    else
       select amo_annuite into annuite from amortissement where invc_id=invcid and exe_ordre=myexeordre+iter; 
       cumul:=cumul+annuite;
       residuel:=montant-cumul;    
    end if;          

    -- pour l'amortissement lineaire avec prorata temporis, on amortie sur duree+1   
    IF ((to_number(to_char(la_date,'yyyy'))+iter>=exeordre) and
       ((prorata=0 or to_char(la_date,'dd/mm')='01/01') and iter=duree) OR (prorata<>0 and iter=duree+1) 
        or (prorata<>0 and iter=duree and residuel<1)) THEN
      UPDATE AMORTISSEMENT SET AMO_RESIDUEL=0, AMO_CUMUL=AMO_CUMUL+AMO_RESIDUEL, AMO_ANNUITE=AMO_ANNUITE+AMO_RESIDUEL WHERE AMO_ID=cle;
      EXIT;
    END IF;
    
  END LOOP;
  
  -- gerer sorties inventaire
  select count(*) into my_nb from inventaire_compt_sortie where invc_id=invcid;
  if my_nb>0 then
  
    --exe_ordre_fin:=exeordre+duree;
    select min(exe_ordre) into exe_ordre_debut from amortissement where invc_id=invcid;
    select max(exe_ordre) into exe_ordre_fin   from amortissement where invc_id=invcid;

    open c1;
    loop
      fetch c1 into my_inventaire_compt_sortie;
      exit when c1%notfound;
      
      exe_ordre_sortie:=to_number(to_char(my_inventaire_compt_sortie.invcs_date,'yyyy'));
      if exe_ordre_sortie<exe_ordre_debut then
         exe_ordre_sortie:=exe_ordre_debut;
      end if;
      exe_ordre_amor:=exe_ordre_sortie;
      
      LOOP
        IF exe_ordre_amor>exe_ordre_fin THEN EXIT; END IF;

      dbms_output.put_line('exe amort '||exe_ordre_amor);

        -- exercice de la sortie
        if exe_ordre_amor=exe_ordre_sortie then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           if prorata=1 and exe_ordre_sortie-exe_ordre_debut-1>=0 then
              

             my_amortissement:=my_inventaire_compt_sortie.invcs_montant_acq_sortie-
               round(my_inventaire_compt_sortie.invcs_montant_acq_sortie/duree, my_nb_decimales)*
                 (exe_ordre_sortie-exe_ordre_debut+taux_premiere_annee(exeordre, la_date));
           
             dbms_output.put_line('   '||my_inventaire_compt_sortie.invcs_montant_acq_sortie||'- '||
                    round(my_inventaire_compt_sortie.invcs_montant_acq_sortie/duree, my_nb_decimales)||'*'||
                    (exe_ordre_sortie-exe_ordre_debut+taux_premiere_annee(exeordre, la_date)));
                    
             if (my_amortissement<0) then my_amortissement:=0; end if;
           
             if (my_amortissement>my_inventaire_compt_sortie.invcs_montant_acq_sortie) then 
                 my_amortissement:=my_inventaire_compt_sortie.invcs_montant_acq_sortie; 
             end if;
             


             /*      dbms_output.put_line('   '||my_inventaire_compt_sortie.invcs_vnc||'- '||
                    round(my_inventaire_compt_sortie.invcs_montant_acq_sortie/duree, my_nb_decimales));

             my_amortissement:=my_inventaire_compt_sortie.invcs_vnc-
               round(my_inventaire_compt_sortie.invcs_montant_acq_sortie/duree, my_nb_decimales);*/
           else
              my_amortissement:=my_inventaire_compt_sortie.invcs_vnc;
           end if;
           
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
           end if;

      dbms_output.put_line('   amort sortie'||my_amortissement);

        end if;
      
        -- annees suivantes
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor<exe_ordre_fin then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
                        
           --my_amortissement:=round(my_inventaire_compt_sortie.invcs_vnc/(exe_ordre_fin-exe_ordre_sortie), my_nb_decimales);        
           my_amortissement:=round(my_inventaire_compt_sortie.invcs_montant_acq_sortie/duree, my_nb_decimales);        
                     
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
              select amo_annuite into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
              my_amortissement:=my_residuel-my_amortissement;
           end if;
           
           my_amortissement:=-my_amortissement;

           dbms_output.put_line('   amort ... '||my_amortissement);
           
        end if;

        -- derniere annee
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor=exe_ordre_fin then
           select nvl(sum(amo_annuite),0) into my_cumul from amortissement where exe_ordre<exe_ordre_fin and invc_id=invcid;
           select nvl(amo_acquisition,0) into my_acquisition from amortissement where exe_ordre=exe_ordre_debut and invc_id=invcid;
           select amo_annuite into my_residuel from amortissement where exe_ordre=exe_ordre_fin and invc_id=invcid;
           my_amortissement:=my_acquisition-my_cumul-my_residuel;

           dbms_output.put_line('      '||my_acquisition||'-'||my_cumul||'-'||my_residuel);
           
           dbms_output.put_line('   amort fin '||my_amortissement);

        end if;

        if exe_ordre_amor>exe_ordre_sortie then
           select amo_residuel into my_residuel
              from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
           select amo_cumul, amo_acquisition into my_cumul, my_acquisition from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           update amortissement set amo_acquisition=my_acquisition-my_inventaire_compt_sortie.invcs_montant_acq_sortie,
              amo_residuel=my_residuel-amo_annuite where invc_id=invcid and exe_ordre=exe_ordre_amor;
           update amortissement set amo_cumul=amo_acquisition-amo_residuel where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;

        if my_amortissement<>0 then
          update amortissement
            set amo_annuite=amo_annuite+my_amortissement, amo_cumul=amo_cumul+my_amortissement, amo_residuel=amo_residuel-my_amortissement, amo_date=sysdate 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;
        
        exe_ordre_amor:=exe_ordre_amor+1;
      end loop;
    
    end loop;
    close c1;
  end if;
  
  delete from amortissement where amo_residuel=0 and amo_annuite=0 and invc_id=invcid;
END;
 
PROCEDURE amort_degressif(
   invcid      inventaire_comptable.invc_id%type,
   montant     inventaire_comptable.invc_montant_acquisition%type,
   duree       cle_inventaire_comptable.clic_duree_amort%type,
   pco         cle_inventaire_comptable.pco_num%type,
   pcoamort    jefy_inventaire.amortissement.pco_num%type,
   exeordre    inventaire_comptable.exe_ordre%type,
   prorata     cle_inventaire_comptable.clic_pro_rata%type,
   coef_fiscal degressif_coef.dgco_coef%type,
   la_date     inventaire_comptable.invc_date_acquisition%type
) IS
  cle         jefy_inventaire.amortissement.amo_id%type;
  annuite     jefy_inventaire.amortissement.amo_annuite%type;
  cumul       jefy_inventaire.amortissement.amo_cumul%type;
  residuel    jefy_inventaire.amortissement.amo_residuel%type;
  iter        integer;
  my_nb       integer;
  my_nb_decimales  NUMBER;
  
  cursor c1 is select * from inventaire_compt_sortie where invc_id=invcid and invcs_vnc>0;
  my_inventaire_compt_sortie jefy_inventaire.inventaire_compt_sortie%rowtype;
  exe_ordre_sortie     jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_amor       jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_debut      jefy_inventaire.amortissement.exe_ordre%type;
  exe_ordre_fin        jefy_inventaire.amortissement.exe_ordre%type;
  my_amortissement     jefy_inventaire.amortissement.amo_annuite%type;
  my_residuel          jefy_inventaire.amortissement.amo_residuel%type;
  my_cumul             jefy_inventaire.amortissement.amo_cumul%type;
  my_acquisition       jefy_inventaire.amortissement.amo_acquisition%type;
BEGIN
  if prorata<>0 and la_date is null then
     raise_application_error (-20001,'Pour un amortissement prorata temporis il faut indiquer une date de debut d''amortissement.');  
  end if;

  amort_vider(invcid);
  
  my_nb_decimales:=get_nb_decimales(exeordre);
  
  iter :=0;
  cumul:=0;
  residuel:=montant;
       
  loop
    if montant=0 or duree=0 or residuel=0 then exit; end if;
    
    iter:=iter+1;
    
    -- calcul des valeurs
    if duree+1-iter=0 then
       annuite:=residuel;
    else
       if coef_fiscal*100.0/duree > 100/(duree+1-iter) then
          annuite:=residuel*(coef_fiscal/duree);
       else
          annuite:=residuel/(duree+1-iter);
       end if;
    end if;
 
    if prorata<>0 and iter=1 then
       annuite:=annuite*taux_premiere_annee(exeordre, la_date);
    end if;
    
    annuite:=round(annuite, my_nb_decimales);
    
    if annuite>residuel or duree=iter then annuite:=residuel; end if;
    if annuite<0 then annuite:=0; end if;
    
    select count(*) into my_nb from amortissement where invc_id=invcid and exe_ordre=exeordre+iter;
    if my_nb=0 then
       cumul:=cumul+annuite;
       residuel:=montant-cumul;
    
       -- insertion
       select amortissement_seq.nextval into cle from dual;
       if prorata=0 then
          insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
              values (montant, annuite, cumul, sysdate, cle, residuel, exeordre+iter, invcid, pcoamort);
       else
          insert into amortissement (amo_acquisition, amo_annuite, amo_cumul, amo_date, amo_id, amo_residuel, exe_ordre, invc_id, pco_num)
              values (montant, annuite, cumul, sysdate, cle, residuel, exeordre+iter-1, invcid, pcoamort);
       end if;
    else
       select amo_annuite into annuite from amortissement where invc_id=invcid and exe_ordre=exeordre+iter; 
       cumul:=cumul+annuite;
       residuel:=montant-cumul;    
    end if;
  end loop;
  
  -- gerer sorties inventaire
  select count(*) into my_nb from inventaire_sortie s, inventaire i where i.inv_id=s.inv_id and i.invc_id=invcid;
  if my_nb>0 then
  
    exe_ordre_fin:=exeordre+duree;
    select min(exe_ordre) into exe_ordre_debut from amortissement where invc_id=invcid;

    open c1;
    loop
      fetch c1 into my_inventaire_compt_sortie;
      exit when c1%notfound;
      
      exe_ordre_sortie:=to_number(to_char(my_inventaire_compt_sortie.invcs_date,'yyyy'));
      if exe_ordre_sortie<exe_ordre_debut then
         exe_ordre_sortie:=exe_ordre_debut;
      end if;
      exe_ordre_amor:=exe_ordre_sortie;
      
      LOOP
        IF exe_ordre_amor>exe_ordre_fin THEN EXIT; END IF;

        -- exercice de la sortie
        if exe_ordre_amor=exe_ordre_sortie then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           my_amortissement:=my_inventaire_compt_sortie.invcs_vnc;
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
           end if;
        end if;
      
        -- annees suivantes
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor<exe_ordre_fin then
           select amo_residuel into my_residuel from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;

           iter:=exe_ordre_amor-exeordre;
              
           if coef_fiscal*100.0/duree > 100/(duree+1-iter) then
              my_amortissement:=residuel*(coef_fiscal/duree);
           else
              my_amortissement:=residuel/(duree+1-iter);
           end if;
                        
           my_amortissement:=round(my_amortissement, my_nb_decimales);        
                     
           if my_amortissement>my_residuel then
              my_amortissement:=my_residuel;
              select amo_annuite into my_residuel from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
              my_amortissement:=my_residuel-my_amortissement;
           end if;
           
           my_amortissement:=-my_amortissement;
        end if;

        -- derniere annee
        if exe_ordre_amor>exe_ordre_sortie and exe_ordre_amor=exe_ordre_fin then
           select nvl(sum(amo_annuite),0) into my_cumul from amortissement where exe_ordre<exe_ordre_fin and invc_id=invcid;
           select amo_acquisition, amo_annuite into my_acquisition, my_residuel from amortissement where exe_ordre=exe_ordre_fin and invc_id=invcid;
           my_amortissement:=my_acquisition-my_cumul-my_residuel;
        end if;

        if exe_ordre_amor>exe_ordre_sortie then
           select amo_residuel into my_residuel
              from amortissement where exe_ordre=exe_ordre_amor-1 and invc_id=invcid;
           select amo_cumul, amo_acquisition into my_cumul, my_acquisition from amortissement where exe_ordre=exe_ordre_amor and invc_id=invcid;
           
           update amortissement set amo_acquisition=my_acquisition-my_inventaire_compt_sortie.invcs_montant_acq_sortie,
              amo_residuel=my_residuel-amo_annuite where invc_id=invcid and exe_ordre=exe_ordre_amor;
           update amortissement set amo_cumul=amo_acquisition-amo_residuel where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;

        if my_amortissement<>0 then
          update amortissement
            set amo_annuite=amo_annuite+my_amortissement, amo_cumul=amo_cumul+my_amortissement, amo_residuel=amo_residuel-my_amortissement, amo_date=sysdate 
            where invc_id=invcid and exe_ordre=exe_ordre_amor;
        end if;
        
        exe_ordre_amor:=exe_ordre_amor+1;
      end loop;
    
    end loop;
    close c1;
  end if;
  
end;
 
PROCEDURE amort_vider(
   invcid inventaire_comptable.invc_id%type
) IS 
  my_nb                   integer;
  my_inventaire_comptable inventaire_comptable%rowtype;
BEGIN
  select * into my_inventaire_comptable from inventaire_comptable where invc_id=invcid;
  select count(*) into my_nb from parametre where par_key='RECALCUL_AMORTISSEMENTS_ANTERIEURS' and exe_ordre>my_inventaire_comptable.exe_ordre and par_value='NON';
  if my_nb>0 then
     DELETE FROM AMORTISSEMENT WHERE invc_id=invcid and exe_ordre>=grhum.en_nombre(to_char(sysdate,'yyyy'));
  else
     DELETE FROM AMORTISSEMENT WHERE invc_id=invcid;
  end if;

END;

function taux_premiere_annee(
   a_exe_ordre inventaire_comptable.exe_ordre%type,
   a_date      inventaire_comptable.invc_date_acquisition%type
) return number is
   annee    inventaire_comptable.exe_ordre%type;
   nb_jours integer;
begin
   -- Test date 
   if a_date is null then return 1.0; end if;
   
   annee:=to_number(to_char(a_date, 'yyyy'));
   if a_exe_ordre>annee then return 1.0; end if;
   
   -- calcul
   nb_jours:=360 - (to_number(to_char(a_date,'mm'))-1)*30 - (to_number(to_char(a_date,'dd'))-1);
   if nb_jours>360 then nb_jours:=360.0; end if;
   if nb_jours<0 then nb_jours:=0.0; end if;

   return nb_jours/360.0;
end;

FUNCTION get_nb_decimales(a_exe_ordre inventaire_comptable.exe_ordre%type)
     return jefy_admin.devise.dev_nb_decimales%type
   is
     my_nb        integer;
     my_par_value jefy_admin.parametre.par_value%type;
     my_dev       jefy_admin.devise.dev_nb_decimales%type;
   begin
        SELECT COUNT(*) INTO my_nb FROM jefy_admin.PARAMETRE WHERE exe_ordre=a_exe_ordre AND par_key='DEVISE_DEV_CODE';
        IF my_nb=1 THEN
           SELECT par_value INTO my_par_value FROM jefy_admin.PARAMETRE
              WHERE exe_ordre=a_exe_ordre AND par_key='DEVISE_DEV_CODE';

           SELECT COUNT(*) INTO my_nb FROM jefy_admin.devise WHERE dev_code=my_par_value;
           IF my_nb=1 THEN
              SELECT dev_nb_decimales INTO my_dev FROM jefy_admin.devise WHERE dev_code=my_par_value;

              IF my_dev IS NOT NULL THEN
                 return my_dev;
              END IF;
           END IF;
        END IF;

        return 2;
   end;
END;
/

create or replace procedure grhum.inst_patch_jefy_inv_1530 is

cursor c_inv is
select invc.invc_id, invc.invc_montant_acquisition, orig.icor_id, orig.icor_pourcentage
from jefy_inventaire.inventaire_comptable invc, jefy_inventaire.inventaire_comptable_orig orig
where invc.invc_id=orig.invc_id;

cursor c_cent is
select invc.invc_id, icor_cor.icor_id, ico.icor_montant,(mbienc.montant_calcule - invc.invc_montant_acquisition) diff
from jefy_inventaire.inventaire_comptable invc, jefy_inventaire.inventaire_comptable_orig ico,
-- On cherche la somme des montants
(select  tico.invc_id, sum (tico.icor_montant) montant_calcule 
from jefy_inventaire.inventaire_comptable_orig tico
group by tico.invc_id) mbienc,
-- On cherche le plus récent qui ne donne pas un chiffre rond
(select  tico2.invc_id, max (tico2.icor_id) icor_id
from jefy_inventaire.inventaire_comptable_orig tico2, jefy_inventaire.inventaire_comptable invc2
where invc2.invc_id=tico2.invc_id
and ((tico2.icor_pourcentage/100)*invc2.invc_montant_acquisition) <> round ((tico2.icor_pourcentage/100)*invc2.invc_montant_acquisition,2)
group by tico2.invc_id) icor_cor
where invc.invc_id=mbienc.invc_id
and invc.invc_id=icor_cor.invc_id
and invc.invc_id=ico.invc_id
and ico.icor_id = icor_cor.icor_id
and mbienc.montant_calcule <> invc.invc_montant_acquisition;

w_mnt number;

begin
  -- execution de la mise des montants au pourcentages
  for rec in c_inv loop
    w_mnt := round ((rec.icor_pourcentage/100)*rec.invc_montant_acquisition,2);
    update jefy_inventaire.inventaire_comptable_orig set icor_montant=w_mnt where icor_id=rec.icor_id; 
  end loop;
  
  commit;
  
  -- Correction des centimes
  for rec in c_cent loop
    update jefy_inventaire.inventaire_comptable_orig set icor_montant=rec.icor_montant-rec.diff where icor_id=rec.icor_id; 
  end loop;
  
  -- Mise à jour de la base de donnée
  insert into jefy_inventaire.db_version values (1530,'1.5.3.0',to_date('15/07/2013','dd/mm/yyyy'),sysdate,null);  
end;
/
