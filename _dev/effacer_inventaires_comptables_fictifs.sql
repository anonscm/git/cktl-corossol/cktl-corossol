-- Script qui efface les inventaires comptables fantômes de corossol.
-- Les inventaires comptables ne sont attachés à aucun inventaire physique, hors budgétaire
declare 
cursor c_invcf is 
 select ic.exe_ordre exe_ordre, cic.clic_id clic_id, ic.invc_id invc_id,
 (select count(*) from jefy_inventaire.inventaire_comptable iccount where iccount.clic_id = cic.clic_id ) nbocc,
 cic.clic_num_complet clic_num_complet
from jefy_inventaire.inventaire_comptable ic, jefy_inventaire.cle_inventaire_comptable cic, jefy_inventaire.inventaire i
where cic.clic_id = ic.clic_id
      and i.invc_id(+)=ic.invc_id
      and i.invc_id is null
      and ic.dpco_id is null
  --    and ic.invc_id=6361
  --    and ic.exe_ordre = 2010
order by exe_ordre, clic_id;

begin
 for rec in c_invcf loop
   -- effacement invc_orig, invc
       delete from jefy_inventaire.inventaire_comptable_orig where invc_id=rec.invc_id;
       delete from jefy_inventaire.inventaire_comptable where invc_id=rec.invc_id;
   -- Si tout seul virer cic et cicm 
   if rec.nbocc=1 then
       delete from jefy_inventaire.cle_inventaire_compt_modif where clic_id=rec.clic_id;
       delete from jefy_inventaire.cle_inventaire_comptable where clic_id=rec.clic_id; 
   else
   -- sinon recalculer amortissement
       for invc_rec in (select invc_id from jefy_inventaire.inventaire_comptable where clic_id=rec.clic_id) loop
        jefy_inventaire.api_corossol.amortissement(invc_rec.invc_id);
       end loop;
   end if;
 end loop;
end;