--Solution d'effacement d'un grand nombre anormal d'inventaire
--Efface tous les inventaires d'un détail de livraison non lié à un inventaire comptable.
--La quantité du detail de livraison est ajusté en conséquence
--Attention aucun client corrosol ne doit fonctionner lors de l'effacement !
declare 
-- Entrer la clé du detail de Livraison (lid_ordre)
-- Entrer la quantite (lid_quantite)
-- Voir table JEFY_INVENTAIRE.LIVRAISON_DETAIL et
-- Reperer l'enregistrement qui a la plus grande quantité LID_QUANTITE avec les instructions
-- select lid.lid_ordre as idLivraisonDetail, lid.lid_quantite as qte from JEFY_INVENTAIRE.livraison_detail lid 
-- where lid.lid_quantite =( select max (lid1.lid_quantite) from JEFY_INVENTAIRE.livraison_detail lid1);
--
-- Possibilté d'automatisation voir ci dessus, decommenter les lignes
--
-- Modification
idLivraisonDetail NUMBER := 0;
qte NUMBER := 0;
-- Fin modification

cursor cInventaire is 
 select inv_id from JEFY_INVENTAIRE.inventaire 
 where lid_ordre =  idLivraisonDetail
 and invc_id is null;
 counter NUMBER := 0;
begin

--  Decommenter le sql pour automatiser
--  La derniere ligne peut permettre une selection
--  select lid.lid_ordre, lid.lid_quantite into idLivraisonDetail, qte from JEFY_INVENTAIRE.livraison_detail lid 
--  where lid.lid_quantite = (select max (lid1.lid_quantite) from JEFY_INVENTAIRE.livraison_detail lid1)
--  and lid.lid_quantite > 1000;

  for rec in cInventaire loop
      delete from JEFY_INVENTAIRE.inventaire_cb where inv_id = rec.inv_id;
      delete from JEFY_INVENTAIRE.inventaire    where inv_id = rec.inv_id;
      counter := counter + 1;
  end loop;
  
  if counter > 0 then
      if  qte = counter then
          qte := 1; counter := 1; 
      else
          qte := qte - counter; counter := 0; 
      end if; 
      update JEFY_INVENTAIRE.livraison_detail set lid_quantite = qte , lid_reste = counter where lid_ordre = idLivraisonDetail;
  end if;
end;
