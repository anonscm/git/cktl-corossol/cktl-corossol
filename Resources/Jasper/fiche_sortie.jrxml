<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="fiche_sortie" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="2.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="293"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="CLIC_ID" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[new Integer(147)]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT DISTINCT clic.CLIC_NUM_COMPLET,
  clic.EXE_ORDRE,
  jefy_admin.API_JASPER_PARAM.get_param_admin('UNIV', (SELECT exe_ordre FROM jefy_admin.exercice WHERE exe_stat='O')) AS lib_univ,
  clic.CLIC_COMP,
  clic.CLIC_CR,
  ic.DPCO_ID,
  dp.dpp_numero_facture,
  dp.dpp_date_facture,
  man.man_numero,
  bor.bor_num,
  clic.PCO_NUM,
  pca.PCOA_NUM,
  clic.CLIC_DUREE_AMORT,
  clic.CLIC_TYPE_AMORT,
  ld.LID_LIBELLE,
  DECODE(CLIC_PRO_RATA,1,'Prorata temporis',' ') CLIC_PRO_RATA,
  DECODE (ivs.invs_id,NULL,' ','Sortie : '
  ||tvs.tyso_libelle
  ||' le '
  ||TO_CHAR(ivs.invs_date,'dd/mm/yyyy')
  ||', '
  ||ivs.invs_motif
  ||', VNC:'
  ||ivs.invs_vnc
  ||', cession:'
  ||ivs.invs_valeur_cession) sortie,
  titu.nom_usuel
  || ' '
  || titu.PRENOM AS titu_nom,
  NVL(ivs.invs_valeur_cession,0) invs_valeur_cession,
  resp.NOM_USUEL
  || ' '
  || resp.PRENOM AS resp_nom,
  DECODE(inv.sal_numero,NULL,' ',lo.APPELLATION
  || ' / '
  || s.SAL_ETAGE
  || ' / '
  || s.SAL_PORTE) AS lib_lieu,
  l.COMM_ID,
  cmd.comm_numero,
  pfou.pers_libelle fou_nom, -- c.COMM_LIBELLE,
  NVL(inv.INV_serie,' ') INV_serie,
  to_char(nvl(ic.INVC_DATE_ACQUISITION, dp.dpp_date_service_fait), 'dd/mm/yyyy') invc_date_acquisition,
  inv.INV_MONTANT_ACQUISITION,
  icor.INVC_ACQUISITION_CORRIGE INVC_MONTANT_ACQUISITION,
  icor.invc_montant_amortissable invc_montant_amortissable,
  JEFY_INVENTAIRE.API_REPORT.chaine_financement(clic.clic_id) orgf_libelle,
  fou.fou_code,
  pfou.pers_libelle fournis,
  org.org_UB
  ||'/'
  ||org.org_cr
  ||'/'
  ||org.org_souscr lb,
  typc.tcd_code,
  ic.INVC_ID,
  inv.INV_ID,
  tp.tap_taux,
  inb.inb_fournisseur,
  inb.inb_facture,
  inb.inb_informations,
  total.somme_acq,
  total.somme_amort
FROM jefy_inventaire.livraison l,
  jefy_inventaire.cle_inventaire_comptable clic,
  jefy_inventaire.origine_financement orgf,
  jefy_inventaire.livraison_detail ld,
  jefy_inventaire.inventaire_comptable ic,
  jefy_inventaire.v_inventaire_cptable_corrige icor,
  jefy_inventaire.inventaire_non_budgetaire inb,
  jefy_inventaire.inventaire inv,
  maracuja.plan_comptable_amo pca,
  maracuja.mandat man,
  maracuja.bordereau bor, -- jefy_depense.commande c,
  grhum.salles s,
  grhum.local lo,
  grhum.individu_ulr titu,
  grhum.individu_ulr resp,
  jefy_depense.commande cmd,
  jefy_depense.depense_ctrl_planco dcp,
  jefy_depense.depense_budget db,
  jefy_admin.taux_prorata tp,
  jefy_depense.depense_papier dp,
  jefy_depense.engage_budget eng,
  jefy_admin.organ org,
  jefy_admin.type_credit typc,
  jefy_inventaire.inventaire_sortie ivs,
  jefy_inventaire.type_sortie tvs,
  grhum.fournis_ulr fou,
  grhum.personne pfou, -- grhum.personne presp
  (select clic_id, sum(invc_acquisition_corrige) somme_acq, sum(invc_montant_amortissable) somme_amort
     from jefy_inventaire.v_inventaire_cptable_corrige subIcor
    group by clic_id) total
WHERE clic.CLIC_ID           =$P{CLIC_ID}
AND total.clic_id            =clic.clic_id
AND eng.tcd_ordre            =typc.tcd_ordre(+)
AND db.tap_id                =tp.tap_id(+)
AND inv.inv_id               =ivs.inv_id(+)
AND ivs.tyso_id              =tvs.tyso_id(+)
AND db.eng_id                =eng.eng_id(+)
AND eng.org_id               =org.org_id(+)
AND ic.CLIC_ID               =clic.CLIC_ID
AND ic.INVC_ID               =inv.INVC_ID(+)
AND ic.INVC_ID               =icor.INVC_ID
AND inv.LID_ORDRE            =ld.LID_ORDRE(+)
AND ld.LIV_ORDRE             =l.LIV_ORDRE(+)
AND inv.SAL_NUMERO           =s.SAL_NUMERO(+)
AND s.C_LOCAL                =lo.C_LOCAL(+)
AND pca.PCOA_ID              =clic.PCOA_ID(+)
AND inv.NO_INDIVIDU_RESP     =resp.NO_INDIVIDU(+)
AND inv.NO_INDIVIDU_TITULAIRE=titu.NO_INDIVIDU(+)
AND l.comm_id                =cmd.comm_id(+)
AND cmd.fou_ordre            =fou.fou_ordre(+)
AND fou.pers_id              =pfou.pers_id(+)
AND ic.dpco_id               =dcp.dpco_id(+)
AND dcp.DEP_ID               =db.DEP_ID(+)
AND db.DPP_ID                =dp.dpp_id(+)
AND dcp.MAN_ID               =man.man_id(+)
AND man.bor_id               =bor.bor_id(+)
AND ic.orgf_id               =orgf.orgf_id(+)
AND ic.invc_id               =inb.invc_id(+)
ORDER BY clic.CLIC_NUM_COMPLET,
  l.COMM_ID,
  DPCO_ID]]>
	</queryString>
	<field name="CLIC_NUM_COMPLET" class="java.lang.String"/>
	<field name="EXE_ORDRE" class="java.math.BigDecimal"/>
	<field name="LIB_UNIV" class="java.lang.String"/>
	<field name="CLIC_COMP" class="java.lang.String"/>
	<field name="CLIC_CR" class="java.lang.String"/>
	<field name="DPCO_ID" class="java.math.BigDecimal"/>
	<field name="DPP_NUMERO_FACTURE" class="java.lang.String"/>
	<field name="DPP_DATE_FACTURE" class="java.sql.Timestamp"/>
	<field name="MAN_NUMERO" class="java.math.BigDecimal"/>
	<field name="BOR_NUM" class="java.math.BigDecimal"/>
	<field name="PCO_NUM" class="java.lang.String"/>
	<field name="PCOA_NUM" class="java.lang.String"/>
	<field name="CLIC_DUREE_AMORT" class="java.math.BigDecimal"/>
	<field name="CLIC_TYPE_AMORT" class="java.lang.String"/>
	<field name="LID_LIBELLE" class="java.lang.String"/>
	<field name="CLIC_PRO_RATA" class="java.lang.String"/>
	<field name="SORTIE" class="java.lang.String"/>
	<field name="TITU_NOM" class="java.lang.String"/>
	<field name="INVS_VALEUR_CESSION" class="java.math.BigDecimal"/>
	<field name="RESP_NOM" class="java.lang.String"/>
	<field name="LIB_LIEU" class="java.lang.String"/>
	<field name="COMM_ID" class="java.math.BigDecimal"/>
	<field name="COMM_NUMERO" class="java.math.BigDecimal"/>
	<field name="FOU_NOM" class="java.lang.String"/>
	<field name="INV_SERIE" class="java.lang.String"/>
	<field name="INVC_DATE_ACQUISITION" class="java.lang.String"/>
	<field name="INV_MONTANT_ACQUISITION" class="java.math.BigDecimal"/>
	<field name="INVC_MONTANT_ACQUISITION" class="java.math.BigDecimal"/>
	<field name="ORGF_LIBELLE" class="java.lang.String"/>
	<field name="FOU_CODE" class="java.lang.String"/>
	<field name="FOURNIS" class="java.lang.String"/>
	<field name="LB" class="java.lang.String"/>
	<field name="TCD_CODE" class="java.lang.String"/>
	<field name="INVC_ID" class="java.math.BigDecimal"/>
	<field name="INV_ID" class="java.math.BigDecimal"/>
	<field name="TAP_TAUX" class="java.math.BigDecimal"/>
	<field name="INB_FOURNISSEUR" class="java.lang.String"/>
	<field name="INB_FACTURE" class="java.lang.String"/>
	<field name="INB_INFORMATIONS" class="java.lang.String"/>
	<field name="SOMME_ACQ" class="java.math.BigDecimal"/>
	<field name="SOMME_AMORT" class="java.math.BigDecimal"/>
	<variable name="sum_mont_acq" class="java.math.BigDecimal" incrementType="Group" incrementGroup="Inventaire_comptable" calculation="Sum">
		<variableExpression><![CDATA[$F{INVC_MONTANT_ACQUISITION}]]></variableExpression>
	</variable>
	<variable name="sum_montant_cession" class="java.math.BigDecimal" incrementType="Group" incrementGroup="amortissement" calculation="Sum">
		<variableExpression><![CDATA[$F{INVS_VALEUR_CESSION}]]></variableExpression>
	</variable>
	<group name="amortissement">
		<groupExpression><![CDATA[null]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="146" splitType="Stretch">
				<textField isBlankWhenNull="false">
					<reportElement key="textField-18" x="0" y="5" width="534" height="14"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Center">
						<font isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA["Tableau des Dotations Amortissements/Dépréciations"]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-19" x="6" y="22" width="527" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Compte amortissement/dépréciation : "+$F{PCOA_NUM}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-20" x="6" y="35" width="527" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Durée amortissement/dépréciation : "+$F{CLIC_DUREE_AMORT}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-22" x="6" y="75" width="124" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Montant d'acquisition : "]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-24" x="6" y="61" width="527" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Amortissement : "+$F{CLIC_TYPE_AMORT}+" "+$F{CLIC_PRO_RATA}]]></textFieldExpression>
				</textField>
				<textField pattern="##0.00" isBlankWhenNull="false">
					<reportElement key="textField" x="132" y="75" width="124" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA[$F{SOMME_ACQ}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="false">
					<reportElement key="textField-37" x="6" y="102" width="527" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Financement : "+$F{ORGF_LIBELLE}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="false">
					<reportElement key="textField-43" x="6" y="48" width="527" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Date de début d'amortissement : "+($F{INVC_DATE_ACQUISITION}!=null ? $F{INVC_DATE_ACQUISITION} : " ")]]></textFieldExpression>
				</textField>
				<subreport isUsingCache="true">
					<reportElement key="subreport-1" x="0" y="132" width="535" height="14"/>
					<subreportParameter name="CLIC_ID">
						<subreportParameterExpression><![CDATA[$P{CLIC_ID}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "tab_amortissement.jasper"]]></subreportExpression>
				</subreport>
				<textField pattern="##0.00" isBlankWhenNull="false">
					<reportElement mode="Transparent" x="132" y="89" width="124" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="SansSerif" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{SOMME_AMORT} == null ? $F{SOMME_ACQ} : $F{SOMME_AMORT}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="false">
					<reportElement mode="Transparent" x="6" y="89" width="120" height="13" forecolor="#000000" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" markup="none">
						<font fontName="SansSerif" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA["Montant amortissable : "]]></textFieldExpression>
				</textField>
				<subreport isUsingCache="true">
					<reportElement key="subreport-2" x="0" y="116" width="535" height="14"/>
					<subreportParameter name="CLIC_ID">
						<subreportParameterExpression><![CDATA[$P{CLIC_ID}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "tab_financement.jasper"]]></subreportExpression>
				</subreport>
			</band>
		</groupFooter>
	</group>
	<group name="Inventaire_comptable">
		<groupExpression><![CDATA[$F{INVC_ID}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<group name="Inventaire">
		<groupExpression><![CDATA[$F{INVC_ID}]]></groupExpression>
		<groupHeader>
			<band height="29" splitType="Stretch">
				<printWhenExpression><![CDATA[new Boolean($F{COMM_ID}!=null)]]></printWhenExpression>
				<textField isBlankWhenNull="false">
					<reportElement key="textField" x="0" y="2" width="427" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Cde : "+$F{COMM_NUMERO}+"   Facture : "+$F{DPP_NUMERO_FACTURE}+"   Mandat : "+$F{MAN_NUMERO}+" Bord.:"+$F{BOR_NUM}]]></textFieldExpression>
				</textField>
				<textField pattern="dd/MM/yyyy" isBlankWhenNull="false">
					<reportElement key="textField" x="464" y="1" width="70" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA[$F{DPP_DATE_FACTURE}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-3" x="434" y="1" width="31" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<text><![CDATA[Date :]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="false">
					<reportElement key="textField-38" x="25" y="15" width="508" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Fournisseur :"+$F{FOU_CODE}+" "+$F{FOURNIS}+" Ligne Budg.: "+$F{LB}+", TC : "+$F{TCD_CODE}+", prorata : "+$F{TAP_TAUX}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<group name="InventaireReprise">
		<groupExpression><![CDATA[$F{INVC_ID}]]></groupExpression>
		<groupHeader>
			<band height="47" splitType="Stretch">
				<printWhenExpression><![CDATA[new Boolean($F{COMM_ID}==null)]]></printWhenExpression>
				<textField isStretchWithOverflow="true" isBlankWhenNull="false">
					<reportElement key="textField-42" x="0" y="1" width="533" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Fournisseur : "+$F{INB_FOURNISSEUR}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="false">
					<reportElement key="textField-43" x="0" y="13" width="534" height="13"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Facture : "+$F{INB_FACTURE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="false">
					<reportElement key="textField-44" positionType="Float" x="0" y="27" width="533" height="20"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement/>
					<textFieldExpression><![CDATA["Informations : "+$F{INB_INFORMATIONS}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="99" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="textField-1" x="2" y="16" width="533" height="18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{CLIC_NUM_COMPLET}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-1" x="1" y="34" width="534" height="21"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[FICHE DE SORTIE D'INVENTAIRE]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-2" x="1" y="54" width="534" height="20"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA["Exercice budgetaire d'entrée "+$F{EXE_ORDRE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-3" x="1" y="82" width="287" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA["UB : "+$F{CLIC_COMP}+" CR : "+$F{CLIC_CR}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-1" x="0" y="97" width="535" height="1"/>
			</line>
			<textField isBlankWhenNull="false">
				<reportElement key="textField" x="1" y="2" width="534" height="14"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[$F{LIB_UNIV}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-35" x="288" y="82" width="245" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["Compte d'immobilisation : "+$F{PCO_NUM}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="42" splitType="Stretch">
			<printWhenExpression><![CDATA[new Boolean(!$F{INV_ID}.equals( null ))]]></printWhenExpression>
			<textField isBlankWhenNull="false">
				<reportElement key="textField" x="11" y="2" width="437" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="10" isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{LID_LIBELLE}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="false">
				<reportElement key="textField-5" x="26" y="14" width="507" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["Prop : "+$F{TITU_NOM}+" Resp : "+$F{RESP_NOM}+" Salle : "+$F{LIB_LIEU}+" Num série : "+$F{INV_SERIE}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" isBlankWhenNull="false">
				<reportElement key="textField-15" x="448" y="2" width="85" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{INV_MONTANT_ACQUISITION}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="false">
				<reportElement key="textField-39" positionType="Float" x="25" y="27" width="509" height="15"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SORTIE}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="58" splitType="Stretch">
			<textField isBlankWhenNull="false">
				<reportElement key="textField-30" x="1" y="2" width="532" height="25"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="14" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA["Montant de l'acquisition ou valeur"]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-31" x="85" y="29" width="62" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA["Montant : "]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-33" x="317" y="29" width="165" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA["Visa du responsable"]]></textFieldExpression>
			</textField>
			<textField pattern="##0.00" isBlankWhenNull="false">
				<reportElement key="textField-34" x="148" y="29" width="163" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[$F{SOMME_ACQ}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="false">
				<reportElement key="textField-40" x="49" y="42" width="98" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA["Montant cession : "]]></textFieldExpression>
			</textField>
			<textField pattern="##0.00" isBlankWhenNull="false">
				<reportElement key="textField-41" x="148" y="42" width="163" height="13"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement/>
				<textFieldExpression><![CDATA[$V{sum_montant_cession}]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="2" splitType="Stretch"/>
	</lastPageFooter>
	<summary>
		<band splitType="Prevent"/>
	</summary>
</jasperReport>
