SELECT nvl(amo.exe_ordre, clic.exe_ordre) exe_ordre,
  i.invc_id,
  clic.clic_num_complet numero_inventaire,
  comm.comm_numero numero_commande,
  i.exe_ordre exercice_debut,
  orgf.orgf_libelle origine_financement,
  NVL(inb.inb_fournisseur,fou.fou_code ||' ' ||fou.fou_nom) fournisseur,
  clic.pco_num imputation_depense,
  pcoe.pco_libelle imputation_dep_libelle,
  NVL(clic.clic_comp, NVL(org.org_ub,orgnb.org_ub)) ub,
  NVL(clic_cr, NVL(org.org_cr,orgnb.org_cr)) cr,
  NVL(amo.pco_num,' ') compte_amortissement,
  jefy_inventaire.formater_nombre_xls(DECODE(amo.exe_ordre,$P{EXERCICE},DECODE(icor.invc_acquisition_corrige, NULL, inb.inb_montant, icor.invc_acquisition_corrige), nvl(sortie.montant, icor.INVC_ACQUISITION))) acquisition,
  jefy_inventaire.formater_nombre_xls(DECODE(amo.exe_ordre,$P{EXERCICE},NVL(amo.amo_acquisition, DECODE(icor.invc_acquisition_corrige, NULL, inb.inb_montant, icor.invc_acquisition_corrige)), nvl(sortie.montant, amo.AMO_ACQUISITION))) amortissable ,
  jefy_inventaire.formater_nombre_xls(DECODE(amo.amo_residuel,NULL,0,0,DECODE(amo.exe_ordre,$P{EXERCICE},amo.amo_annuite,0),amo.amo_annuite)) annuite,
  jefy_inventaire.formater_nombre_xls(NVL(amo.amo_cumul,DECODE(inb_duree,0,0,inb_montant_residuel))) cumul,
  jefy_inventaire.formater_nombre_xls(NVL(amo.amo_residuel,DECODE(inb_duree,0,inb.inb_montant,inb_montant_residuel))) residuel,
  amomax.exe_ordre exercice_fin
FROM jefy_inventaire.amortissement amo,
     jefy_inventaire.amortissement amomax,
     jefy_inventaire.inventaire_comptable i,
     jefy_inventaire.v_inventaire_cptable_corrige icor,
     jefy_inventaire.origine_financement orgf,
     jefy_inventaire.cle_inventaire_comptable clic,
     jefy_depense.v_fournisseur fou,
     jefy_depense.depense_ctrl_planco dpco,
     jefy_depense.depense_budget dep,
     jefy_depense.engage_budget eng,
     jefy_admin.organ org,
     jefy_inventaire.inventaire_non_budgetaire inb,
     jefy_depense.commande_engagement come,
     jefy_depense.commande comm,
     maracuja.plan_comptable_exer pcoe,
     jefy_admin.organ orgnb,
     (SELECT sma.invc_id, sum(sma.montant) montant
        FROM jefy_inventaire.v_sortie_montant_annuel sma
        WHERE (sma.annee_sortie < $P{EXERCICE} or sma.annee_sortie is null)        
       GROUP BY sma.invc_id
     ) sortie
WHERE (amo.exe_ordre=$P{EXERCICE}
        OR (amo.amo_residuel=0 AND amo.exe_ordre  <=$P{EXERCICE})
        OR (exists (select 1 from jefy_inventaire.v_inventaire_repris_non_amorti irmo where icor.invc_id = irmo.invc_id)
        	AND not exists (
        		select invc_id from jefy_inventaire.inventaire_compt_sortie s where icor.invc_id = s.invc_id and grhum.en_nombre(to_char(s.invcs_date,'yyyy'))<=$P{EXERCICE}
        	)
        )
      )
AND i.invc_id           = icor.invc_id
AND icor.invc_id        =sortie.invc_id(+)
AND DECODE(amo.exe_ordre,$P{EXERCICE},NVL(amo.amo_acquisition, DECODE(icor.invc_acquisition_corrige, NULL, inb.inb_montant, icor.invc_acquisition_corrige)), nvl(sortie.montant, amo.AMO_ACQUISITION)) > 0
AND amo.invc_id(+)      =i.invc_id
AND orgf.orgf_id        =i.orgf_id
AND i.clic_id           =clic.clic_id
AND i.invc_id           =inb.invc_id(+)
AND inb.org_id          =orgnb.org_id(+)
AND i.dpco_id           =dpco.dpco_id(+)
AND dpco.dep_id         =dep.dep_id(+)
AND dep.eng_id          =eng.eng_id(+)
AND eng.org_id          =org.org_id(+)
AND eng.fou_ordre       =fou.fou_ordre(+)
AND eng.eng_id          =come.eng_id(+)
AND come.comm_id        =comm.comm_id(+)
AND amo.invc_id         =amomax.invc_id(+)
AND (amomax.amo_residuel=0 OR amomax.amo_residuel IS NULL)
AND pcoe.exe_ordre(+)   =$P{EXERCICE}
AND pcoe.pco_num(+)     =clic.pco_num
ORDER BY NVL(clic.clic_comp, NVL(org.org_ub,orgnb.org_ub)), clic.pco_num, clic.clic_num_complet