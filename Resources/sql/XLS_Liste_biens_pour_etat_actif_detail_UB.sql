SELECT nvl(amo.exe_ordre, clic.exe_ordre) exe_ordre,
       invc.invc_id invc_id, 
  clic.clic_num_complet numero_inventaire,
       nvl(to_char(comm.comm_numero), ' ') numero_commande, 
       invc.exe_ordre exercice_debut, 
       min(orgf.orgf_libelle) origine_financement,
       nvl(inb.inb_fournisseur, fou.fou_code || ' ' || fou.fou_nom) fournisseur, 
  clic.pco_num imputation_depense,
       nvl (pcoe.pco_libelle, ' ') imputation_dep_libelle, 
       nvl(clic.clic_comp, org.org_ub) ub, 
       nvl(clic.clic_cr, org.org_cr) cr,
       nvl(amo.pco_num, ' ') compte_amortissement, 
       jefy_inventaire.formater_nombre_xls(DECODE(amo.exe_ordre,$P{EXERCICE},DECODE(icor.invc_acquisition_corrige, NULL, inb.inb_montant, icor.invc_acquisition_corrige), nvl(sortie.montant, icor.INVC_ACQUISITION))) acquisition,
       jefy_inventaire.formater_nombre_xls(DECODE(amo.exe_ordre,$P{EXERCICE},NVL(amo.amo_acquisition, DECODE(icor.invc_acquisition_corrige, NULL, inb.inb_montant, icor.invc_acquisition_corrige)), nvl(sortie.montant, amo.AMO_ACQUISITION))) amortissable ,
       jefy_inventaire.formater_nombre_xls(
          decode(amo.amo_residuel, null, 0, 0, decode(amo.exe_ordre, $P{EXERCICE}, amo.amo_annuite, 0), amo.amo_annuite)
       ) annuite,
       jefy_inventaire.formater_nombre_xls(
          nvl(amo.amo_cumul, decode(inb_duree, 0, 0, inb_montant_residuel))
       ) cumul,
       jefy_inventaire.formater_nombre_xls(
          nvl(amo.amo_residuel, decode(inb_duree, 0, inb.inb_montant, inb_montant_residuel))
       ) residuel,
       nvl(amomax.exe_ordre,' ') exercice_fin, 
       REPLACE(REPLACE(nvl(min(ld.lid_libelle),' '), CHR(13),' '), CHR(10),' ') bien, 
       nvl(min(s.c_local), ' ') SALLE_BATIMENT, 
       nvl(min(s.sal_etage),' ') SALLE_ETAGE,
       nvl(min(s.sal_porte),' ') SALLE_PORTE,
       nvl(min(i.inv_serie), ' ') NUMERO_SERIE,
       jefy_inventaire.api_report.chaine_nom_prenom(min(i.no_individu_titulaire)) UTILISATEUR_DU_BIEN
FROM jefy_inventaire.amortissement amo,
     jefy_inventaire.amortissement amomax,
      jefy_inventaire.inventaire_comptable invc,
     jefy_inventaire.v_inventaire_cptable_corrige icor,
     jefy_inventaire.origine_financement orgf,
     jefy_inventaire.cle_inventaire_comptable clic,
     jefy_depense.v_fournisseur fou,
     jefy_depense.depense_ctrl_planco dpco,
     jefy_depense.depense_budget dep,
     jefy_depense.engage_budget eng,
     jefy_admin.organ org,
     jefy_inventaire.inventaire_non_budgetaire inb,
     jefy_depense.commande_engagement come,
     jefy_depense.commande comm,
     maracuja.plan_comptable_exer pcoe,
      jefy_inventaire.livraison_detail ld, 
      jefy_inventaire.inventaire i,
      grhum.salles s,
     (SELECT sma.invc_id, sum(sma.montant) montant
        FROM jefy_inventaire.v_sortie_montant_annuel sma
        WHERE (sma.annee_sortie < $P{EXERCICE} or sma.annee_sortie is null)        
       GROUP BY sma.invc_id
     ) sortie
WHERE (amo.exe_ordre=$P{EXERCICE}
        OR (amo.amo_residuel=0 AND amo.exe_ordre  <=$P{EXERCICE})
        OR (exists (select 1 from jefy_inventaire.v_inventaire_repris_non_amorti irmo where icor.invc_id = irmo.invc_id)
        	AND not exists (
        		select invc_id from jefy_inventaire.inventaire_compt_sortie s where icor.invc_id = s.invc_id and grhum.en_nombre(to_char(s.invcs_date,'yyyy'))<=$P{EXERCICE}
        	)
        )
      )
      AND amo.invc_id(+) = invc.invc_id
      AND invc.invc_id = icor.invc_id
      AND orgf.orgf_id = invc.orgf_id
      AND invc.invc_id = sortie.invc_id(+)
      AND DECODE(amo.exe_ordre,$P{EXERCICE},NVL(amo.amo_acquisition, DECODE(icor.invc_acquisition_corrige, NULL, inb.inb_montant, icor.invc_acquisition_corrige)), nvl(sortie.montant, amo.AMO_ACQUISITION)) > 0
      AND invc.clic_id = clic.clic_id 
      AND invc.invc_id = inb.invc_id(+) 
      AND invc.dpco_id = dpco.dpco_id(+) 
AND dpco.dep_id         =dep.dep_id(+)
AND dep.eng_id          =eng.eng_id(+)
AND eng.org_id          =org.org_id(+)
AND eng.fou_ordre       =fou.fou_ordre(+)
AND eng.eng_id          =come.eng_id(+)
AND come.comm_id        =comm.comm_id(+)
AND amo.invc_id         =amomax.invc_id(+)
      AND (amomax.amo_residuel = 0 OR amomax.amo_residuel is null)
AND pcoe.exe_ordre(+)   =$P{EXERCICE}
AND pcoe.pco_num(+)     =clic.pco_num
      AND invc.INVC_ID = i.invc_id(+) 
      AND i.lid_ordre = ld.lid_ordre(+) 
      AND i.sal_numero = s.sal_numero(+)
GROUP BY nvl(amo.exe_ordre, clic.exe_ordre), invc.invc_id, clic.clic_num_complet, nvl(to_char(comm.comm_numero),' '),
   invc.exe_ordre, nvl(inb.inb_fournisseur,fou.fou_code||' '||fou.fou_nom), clic.pco_num,
   pcoe.pco_libelle, nvl(clic.clic_comp,org.org_ub), nvl(clic.clic_cr,org.org_cr), nvl(amo.pco_num,' '),
   jefy_inventaire.formater_nombre_xls(DECODE(amo.exe_ordre,$P{EXERCICE},DECODE(icor.invc_acquisition_corrige, NULL, inb.inb_montant, icor.invc_acquisition_corrige), nvl(sortie.montant, icor.INVC_ACQUISITION))),
   jefy_inventaire.formater_nombre_xls(DECODE(amo.exe_ordre,$P{EXERCICE},NVL(amo.amo_acquisition, DECODE(icor.invc_acquisition_corrige, NULL, inb.inb_montant, icor.invc_acquisition_corrige)), nvl(sortie.montant, amo.AMO_ACQUISITION))), 
   jefy_inventaire.formater_nombre_xls(
          decode(amo.amo_residuel, null, 0, 0, decode(amo.exe_ordre, $P{EXERCICE}, amo.amo_annuite, 0), amo.amo_annuite)
       ),
       jefy_inventaire.formater_nombre_xls(nvl(amo.amo_cumul, decode(inb_duree, 0, 0, inb_montant_residuel))),
       jefy_inventaire.formater_nombre_xls(nvl(amo.amo_residuel, decode(inb_duree, 0, inb.inb_montant, inb_montant_residuel))),
   nvl(amomax.exe_ordre,' ')
 order by nvl(clic.clic_comp,org.org_ub), clic.pco_num, clic.clic_num_complet