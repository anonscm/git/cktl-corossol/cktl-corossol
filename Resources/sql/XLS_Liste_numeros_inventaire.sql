SELECT DISTINCT 
       jefy_inventaire.formater_nombre_xls(sum(v.INVC_MONTANT_ACQUISITION)),
       jefy_inventaire.formater_nombre_xls(sum(v.INVC_MONTANT_AMORTISSABLE)),
       jefy_inventaire.formater_nombre_xls(sum(v.INVC_MONTANT_RESIDUEL)),
       o.org_ub, 
       o.org_cr, 
       nvl(o.org_souscr,' '),
       o.org_lib, 
       c.exe_ordre, 
       c.comm_numero, 
       replace(c.comm_reference,'\"',''),
       replace(c.comm_libelle,'\"',''), 
       p.pco_num, 
       p.pco_libelle, 
       tc.tcd_code, 
       tc.tcd_libelle, 
       nvl(m.man_numero,'0'),
       nvl(b.bor_num,'0'), 
       nvl(b.ges_code,'0'), 
       four.fou_code, 
       four.adr_civilite, 
       four.adr_nom || four.adr_prenom,
       v.clic_num_complet, 
       replace(nvl(fin.orgf_libelle,'a renseigner'),'\"','') orgf_libelle
 FROM jefy_inventaire.v_inventaire_export v, 
      jefy_depense.commande c, 
      jefy_depense.depense_ctrl_planco dpco,
      jefy_admin.organ o, 
      maracuja.PLAN_COMPTABLE p, 
      jefy_admin.TYPE_CREDIT tc, 
      maracuja.mandat m ,
      maracuja.bordereau b, 
      grhum.V_FOURNIS_GRHUM four, 
      jefy_inventaire.ORIGINE_FINANCEMENT fin
 WHERE v.org_id = o.org_id 
   and v.tcd_ordre = tc.tcd_ordre 
   and v.pco_num = p.pco_num 
   and v.dpco_id = dpco.dpco_id (+)
   and dpco.man_id = m.man_id(+) 
   and m.bor_id = b.bor_id(+) 
   and v.comm_id = c.comm_id(+) 
   and c.fou_ordre = four.fou_ordre
   and v.orgf_id = fin.orgf_id (+)
   and c.exe_ordre = $P{EXERCICE}
GROUP BY o.org_ub, o.org_cr, o.org_souscr, o.org_lib, c.exe_ordre, c.comm_numero, replace(c.comm_reference,'\"',''),
   replace(c.comm_libelle,'\"',''), p.pco_num, p.pco_libelle, tc.tcd_code, tc.tcd_libelle, nvl(m.man_numero,'0'),
   nvl(b.bor_num,'0'), nvl(b.ges_code,'0'), four.fou_code, four.adr_civilite, four.adr_nom||four.adr_prenom,
   v.clic_num_complet, replace(nvl(fin.orgf_libelle,'a renseigner'),'\"','')
 UNION
SELECT DISTINCT 
       jefy_inventaire.formater_nombre_xls(inb.INB_MONTANT),
       jefy_inventaire.formater_nombre_xls(inb.INB_MONTANT_RESIDUEL),
       jefy_inventaire.formater_nombre_xls(inb.INB_MONTANT_RESIDUEL),
       o.org_ub, 
       o.org_cr, 
       nvl (o.org_souscr,' '), 
       o.org_lib, 
       inb.EXE_ORDRE exercice, 
       0 comm_numero,
       ' ' com_reference, 
       ' ' comm_libelle,
       pco.pco_num, 
       pco.pco_libelle,
       ' ' tcd_code, 
       ' ' tcd_libelle,
       0 man_numero, 
       0 bor_num, 
       '0' ges_code, 
       ' ' fou_code, 
       ' ' adr_civilite,
       inb.INB_FOURNISSEUR FOUR,
       cic.clic_num_complet, 
       replace(nvl(fin.orgf_libelle,'a renseigner'),'\"','') orgf_libelle
 FROM jefy_inventaire.INVENTAIRE_NON_BUDGETAIRE inb, 
      jefy_admin.organ o, maracuja.plan_comptable pco,
      jefy_inventaire.INVENTAIRE_COMPTABLE ic , 
      jefy_inventaire.CLE_INVENTAIRE_COMPTABLE cic, 
      jefy_inventaire.ORIGINE_FINANCEMENT fin
WHERE inb.pco_num = pco.pco_num
 and o.org_id = inb.org_id
 and inb.invc_id =  ic.invc_id
 and ic.dpco_id is not null
 AND ic.clic_id = cic.clic_id
 and inb.orig_id = fin.orgf_id (+)
 and inb.exe_ordre = $P{EXERCICE}
 order by org_ub,pco_num,org_cr,clic_num_complet
