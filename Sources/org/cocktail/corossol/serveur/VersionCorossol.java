package org.cocktail.corossol.serveur;

import org.cocktail.application.serveur.CocktailCollecteVersion;

public class VersionCorossol extends CocktailCollecteVersion {


	public String appliDate() {
		return VersionMe.VERSIONDATE;
	}

	public String appliId() {
		return VersionMe.APPLICATIONSTRID;
	}

	public int appliVersionMajeure() {
		return VersionMe.VERSIONNUMMAJ;
	}

	public int appliVersionMineure() {
		return VersionMe.VERSIONNUMMIN;
	}

	public int appliVersionPatch() {
		return VersionMe.VERSIONNUMPATCH;
	}

	public int appliVersionBuild() {
		return VersionMe.VERSIONNUMBUILD;
	}

	public void checkDependencies() throws Exception {
	}

	public String commentaire() {
		return null;
	}

	public String minAppliBdVersion() {
		return VersionMe.MINDBVERSION;
	}
	

}
