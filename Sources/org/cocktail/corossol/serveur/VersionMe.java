
package org.cocktail.corossol.serveur;

import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;


// Versions du serveur, mini du framework, mini de la base
// nom de l'application

public class VersionMe {
	
	// Nom de l'appli
	
	public static final String APPLICATIONFINALNAME = "Corossol";
	public static final String APPLICATIONINTERNALNAME = "Corossol";
	public static final String APPLICATIONSTRID = "COROSSOL";

	public static final String MINDBVERSION = "1.7.2";

	public static final int VERSIONNUMMAJ;
	public static final int VERSIONNUMMIN;
	public static final int VERSIONNUMPATCH;
	public static final int VERSIONNUMBUILD;

	public static final String VERSIONDATE;
	public static final String COMMENT = null;

	static {
		VersionApp versionApp = Application.application().injector().getInstance(VersionApp.class);
		VERSIONNUMMAJ = versionApp.majVersion();
		VERSIONNUMMIN = versionApp.minVersion();
		VERSIONNUMPATCH = versionApp.patchVersion();
		VERSIONNUMBUILD = versionApp.buildVersion();
		VERSIONDATE = versionApp.dateVersion();
	}

	public static final String VERSIONNUM = VERSIONNUMMAJ+"."+VERSIONNUMMIN+"."+VERSIONNUMPATCH+"."+VERSIONNUMBUILD;



	public static String appliVersion() {
		String appliVersion = VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD;
		return appliVersion;
	}

	public static String htmlAppliVersion() {
		String htmlAppliVersion = "<b>Version " + appliVersion();
		htmlAppliVersion += " du " + VERSIONDATE + "</b>";
		return htmlAppliVersion;
	}

	public static String txtAppliVersion() {
		return "Version " + appliVersion() + " du " + VERSIONDATE;
	}

	public static String rawVersion() {
		return appliVersion();
	}

	
}
