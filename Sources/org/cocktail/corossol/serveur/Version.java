package org.cocktail.corossol.serveur;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava;

public final class Version extends A_CktlVersion {

	// Version appli

	public static final int VERSIONNUMMAJ = VersionMe.VERSIONNUMMAJ;
	public static final int VERSIONNUMMIN = VersionMe.VERSIONNUMMIN;
	public static final int VERSIONNUMPATCH = VersionMe.VERSIONNUMPATCH;
	public static final int VERSIONNUMBUILD = VersionMe.VERSIONNUMBUILD;

	private static final String VERSIONDATE = VersionMe.VERSIONDATE;
	private static final String COMMENT = null;
   
    /** Version du JRE */
    private static final String JRE_VERSION_MIN = "1.5";
    private static final String JRE_VERSION_MAX = null;
    

	public String name() {
		return VersionMe.APPLICATIONFINALNAME;
	}

	public String internalName() {
		return VersionMe.APPLICATIONINTERNALNAME;
	}

	public int versionNumBuild() {
		return VERSIONNUMBUILD;
	}

	public int versionNumMaj() {
		return VERSIONNUMMAJ;
	}

	public int versionNumMin() {
		return VERSIONNUMMIN;
	}

	public int versionNumPatch() {
		return VERSIONNUMPATCH;
	}

	public String date() {
		return VERSIONDATE;
	}

	public String comment() {
		return COMMENT;
	}
	
	
	/**
	 * Liste des d̩ependances 
	 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#dependencies()
	 */
	public CktlVersionRequirements[] dependencies() {
		    return new CktlVersionRequirements[]{
				new CktlVersionRequirements(new CktlVersionJava(), JRE_VERSION_MIN, JRE_VERSION_MAX, true)
		      };
	}	    
}
