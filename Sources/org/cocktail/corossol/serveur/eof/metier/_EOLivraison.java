/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOLivraison.java instead.
package org.cocktail.corossol.serveur.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOLivraison extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Livraison";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.LIVRAISON";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "livOrdre";

	public static final String LIV_DATE_KEY = "livDate";
	public static final String LIV_DIVERS_KEY = "livDivers";
	public static final String LIV_NUMERO_KEY = "livNumero";
	public static final String LIV_RECEPTION_KEY = "livReception";

// Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String DEV_ID_KEY = "devId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String LIV_ORDRE_KEY = "livOrdre";
	public static final String MAG_ID_KEY = "magId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String LIV_DATE_COLKEY = "LIV_DATE";
	public static final String LIV_DIVERS_COLKEY = "LIV_DIVERS";
	public static final String LIV_NUMERO_COLKEY = "LIV_NUMERO";
	public static final String LIV_RECEPTION_COLKEY = "LIV_RECEPTION";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String DEV_ID_COLKEY = "DEV_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String LIV_ORDRE_COLKEY = "LIV_ORDRE";
	public static final String MAG_ID_COLKEY = "mag_id";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String COMMANDE_KEY = "commande";
	public static final String DEVISE_KEY = "devise";
	public static final String EXERCICE_KEY = "exercice";
	public static final String LIVRAISON_DETAILS_KEY = "livraisonDetails";
	public static final String MAGASIN_KEY = "magasin";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp livDate() {
    return (NSTimestamp) storedValueForKey(LIV_DATE_KEY);
  }

  public void setLivDate(NSTimestamp value) {
    takeStoredValueForKey(value, LIV_DATE_KEY);
  }

  public String livDivers() {
    return (String) storedValueForKey(LIV_DIVERS_KEY);
  }

  public void setLivDivers(String value) {
    takeStoredValueForKey(value, LIV_DIVERS_KEY);
  }

  public String livNumero() {
    return (String) storedValueForKey(LIV_NUMERO_KEY);
  }

  public void setLivNumero(String value) {
    takeStoredValueForKey(value, LIV_NUMERO_KEY);
  }

  public String livReception() {
    return (String) storedValueForKey(LIV_RECEPTION_KEY);
  }

  public void setLivReception(String value) {
    takeStoredValueForKey(value, LIV_RECEPTION_KEY);
  }

  public org.cocktail.corossol.serveur.eof.metier.EOVCommande commande() {
    return (org.cocktail.corossol.serveur.eof.metier.EOVCommande)storedValueForKey(COMMANDE_KEY);
  }

  public void setCommandeRelationship(org.cocktail.corossol.serveur.eof.metier.EOVCommande value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOVCommande oldValue = commande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMANDE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EODevise devise() {
    return (org.cocktail.application.serveur.eof.EODevise)storedValueForKey(DEVISE_KEY);
  }

  public void setDeviseRelationship(org.cocktail.application.serveur.eof.EODevise value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EODevise oldValue = devise();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEVISE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEVISE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.corossol.serveur.eof.metier.EOMagasin magasin() {
    return (org.cocktail.corossol.serveur.eof.metier.EOMagasin)storedValueForKey(MAGASIN_KEY);
  }

  public void setMagasinRelationship(org.cocktail.corossol.serveur.eof.metier.EOMagasin value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOMagasin oldValue = magasin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MAGASIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MAGASIN_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray livraisonDetails() {
    return (NSArray)storedValueForKey(LIVRAISON_DETAILS_KEY);
  }

  public NSArray livraisonDetails(EOQualifier qualifier) {
    return livraisonDetails(qualifier, null, false);
  }

  public NSArray livraisonDetails(EOQualifier qualifier, boolean fetch) {
    return livraisonDetails(qualifier, null, fetch);
  }

  public NSArray livraisonDetails(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.serveur.eof.metier.EOLivraisonDetail.LIVRAISON_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.serveur.eof.metier.EOLivraisonDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = livraisonDetails();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToLivraisonDetailsRelationship(org.cocktail.corossol.serveur.eof.metier.EOLivraisonDetail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, LIVRAISON_DETAILS_KEY);
  }

  public void removeFromLivraisonDetailsRelationship(org.cocktail.corossol.serveur.eof.metier.EOLivraisonDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LIVRAISON_DETAILS_KEY);
  }

  public org.cocktail.corossol.serveur.eof.metier.EOLivraisonDetail createLivraisonDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("LivraisonDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, LIVRAISON_DETAILS_KEY);
    return (org.cocktail.corossol.serveur.eof.metier.EOLivraisonDetail) eo;
  }

  public void deleteLivraisonDetailsRelationship(org.cocktail.corossol.serveur.eof.metier.EOLivraisonDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, LIVRAISON_DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllLivraisonDetailsRelationships() {
    Enumeration objects = livraisonDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteLivraisonDetailsRelationship((org.cocktail.corossol.serveur.eof.metier.EOLivraisonDetail)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOLivraison avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOLivraison createEOLivraison(EOEditingContext editingContext, NSTimestamp livDate
			) {
    EOLivraison eo = (EOLivraison) createAndInsertInstance(editingContext, _EOLivraison.ENTITY_NAME);    
		eo.setLivDate(livDate);
    return eo;
  }

  
	  public EOLivraison localInstanceIn(EOEditingContext editingContext) {
	  		return (EOLivraison)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLivraison creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLivraison creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOLivraison object = (EOLivraison)createAndInsertInstance(editingContext, _EOLivraison.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOLivraison localInstanceIn(EOEditingContext editingContext, EOLivraison eo) {
    EOLivraison localInstance = (eo == null) ? null : (EOLivraison)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOLivraison#localInstanceIn a la place.
   */
	public static EOLivraison localInstanceOf(EOEditingContext editingContext, EOLivraison eo) {
		return EOLivraison.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOLivraison fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOLivraison fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLivraison eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLivraison)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLivraison fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLivraison fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLivraison eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLivraison)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOLivraison fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLivraison eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLivraison ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLivraison fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
