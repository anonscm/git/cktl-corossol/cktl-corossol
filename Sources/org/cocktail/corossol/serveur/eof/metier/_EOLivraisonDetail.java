/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOLivraisonDetail.java instead.
package org.cocktail.corossol.serveur.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOLivraisonDetail extends  EOGenericRecord {
	public static final String ENTITY_NAME = "LivraisonDetail";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.LIVRAISON_DETAIL";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "lidOrdre";

	public static final String LID_LIBELLE_KEY = "lidLibelle";
	public static final String LID_MONTANT_KEY = "lidMontant";
	public static final String LID_QUANTITE_KEY = "lidQuantite";
	public static final String LID_RESTE_KEY = "lidReste";

// Attributs non visibles
	public static final String LID_ORDRE_KEY = "lidOrdre";
	public static final String LIV_ORDRE_KEY = "livOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String LID_LIBELLE_COLKEY = "LID_LIBELLE";
	public static final String LID_MONTANT_COLKEY = "LID_MONTANT";
	public static final String LID_QUANTITE_COLKEY = "LID_QUANTITE";
	public static final String LID_RESTE_COLKEY = "LID_RESTE";

	public static final String LID_ORDRE_COLKEY = "LID_ORDRE";
	public static final String LIV_ORDRE_COLKEY = "LIV_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String INVENTAIRES_KEY = "inventaires";
	public static final String LIVRAISON_KEY = "livraison";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String lidLibelle() {
    return (String) storedValueForKey(LID_LIBELLE_KEY);
  }

  public void setLidLibelle(String value) {
    takeStoredValueForKey(value, LID_LIBELLE_KEY);
  }

  public java.math.BigDecimal lidMontant() {
    return (java.math.BigDecimal) storedValueForKey(LID_MONTANT_KEY);
  }

  public void setLidMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, LID_MONTANT_KEY);
  }

  public Integer lidQuantite() {
    return (Integer) storedValueForKey(LID_QUANTITE_KEY);
  }

  public void setLidQuantite(Integer value) {
    takeStoredValueForKey(value, LID_QUANTITE_KEY);
  }

  public Integer lidReste() {
    return (Integer) storedValueForKey(LID_RESTE_KEY);
  }

  public void setLidReste(Integer value) {
    takeStoredValueForKey(value, LID_RESTE_KEY);
  }

  public org.cocktail.corossol.serveur.eof.metier.EOLivraison livraison() {
    return (org.cocktail.corossol.serveur.eof.metier.EOLivraison)storedValueForKey(LIVRAISON_KEY);
  }

  public void setLivraisonRelationship(org.cocktail.corossol.serveur.eof.metier.EOLivraison value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOLivraison oldValue = livraison();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LIVRAISON_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LIVRAISON_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOPlanComptable planComptable() {
    return (org.cocktail.application.serveur.eof.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.application.serveur.eof.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeCredit typeCredit() {
    return (org.cocktail.application.serveur.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.application.serveur.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public NSArray inventaires() {
    return (NSArray)storedValueForKey(INVENTAIRES_KEY);
  }

  public NSArray inventaires(EOQualifier qualifier) {
    return inventaires(qualifier, null, false);
  }

  public NSArray inventaires(EOQualifier qualifier, boolean fetch) {
    return inventaires(qualifier, null, fetch);
  }

  public NSArray inventaires(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.serveur.eof.metier.EOInventaire.LIVRAISON_DETAIL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.serveur.eof.metier.EOInventaire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inventaires();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInventairesRelationship(org.cocktail.corossol.serveur.eof.metier.EOInventaire object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRES_KEY);
  }

  public void removeFromInventairesRelationship(org.cocktail.corossol.serveur.eof.metier.EOInventaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRES_KEY);
  }

  public org.cocktail.corossol.serveur.eof.metier.EOInventaire createInventairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Inventaire");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRES_KEY);
    return (org.cocktail.corossol.serveur.eof.metier.EOInventaire) eo;
  }

  public void deleteInventairesRelationship(org.cocktail.corossol.serveur.eof.metier.EOInventaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventairesRelationships() {
    Enumeration objects = inventaires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventairesRelationship((org.cocktail.corossol.serveur.eof.metier.EOInventaire)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOLivraisonDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOLivraisonDetail createEOLivraisonDetail(EOEditingContext editingContext, Integer lidQuantite
, Integer lidReste
			) {
    EOLivraisonDetail eo = (EOLivraisonDetail) createAndInsertInstance(editingContext, _EOLivraisonDetail.ENTITY_NAME);    
		eo.setLidQuantite(lidQuantite);
		eo.setLidReste(lidReste);
    return eo;
  }

  
	  public EOLivraisonDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOLivraisonDetail)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLivraisonDetail creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLivraisonDetail creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOLivraisonDetail object = (EOLivraisonDetail)createAndInsertInstance(editingContext, _EOLivraisonDetail.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOLivraisonDetail localInstanceIn(EOEditingContext editingContext, EOLivraisonDetail eo) {
    EOLivraisonDetail localInstance = (eo == null) ? null : (EOLivraisonDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOLivraisonDetail#localInstanceIn a la place.
   */
	public static EOLivraisonDetail localInstanceOf(EOEditingContext editingContext, EOLivraisonDetail eo) {
		return EOLivraisonDetail.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOLivraisonDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOLivraisonDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLivraisonDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLivraisonDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLivraisonDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLivraisonDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLivraisonDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLivraisonDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOLivraisonDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLivraisonDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLivraisonDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLivraisonDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
