/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODegressif.java instead.
package org.cocktail.corossol.serveur.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODegressif extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Degressif";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.DEGRESSIF";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dgrfId";

	public static final String DGRF_DATE_DEBUT_KEY = "dgrfDateDebut";
	public static final String DGRF_DATE_FIN_KEY = "dgrfDateFin";

// Attributs non visibles
	public static final String DGRF_ID_KEY = "dgrfId";

//Colonnes dans la base de donnees
	public static final String DGRF_DATE_DEBUT_COLKEY = "DGRF_DATE_DEBUT";
	public static final String DGRF_DATE_FIN_COLKEY = "DGRF_DATE_FIN";

	public static final String DGRF_ID_COLKEY = "DGRF_ID";


	// Relationships
	public static final String DEGRESSIF_COEFS_KEY = "degressifCoefs";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dgrfDateDebut() {
    return (NSTimestamp) storedValueForKey(DGRF_DATE_DEBUT_KEY);
  }

  public void setDgrfDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, DGRF_DATE_DEBUT_KEY);
  }

  public NSTimestamp dgrfDateFin() {
    return (NSTimestamp) storedValueForKey(DGRF_DATE_FIN_KEY);
  }

  public void setDgrfDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DGRF_DATE_FIN_KEY);
  }

  public NSArray degressifCoefs() {
    return (NSArray)storedValueForKey(DEGRESSIF_COEFS_KEY);
  }

  public NSArray degressifCoefs(EOQualifier qualifier) {
    return degressifCoefs(qualifier, null, false);
  }

  public NSArray degressifCoefs(EOQualifier qualifier, boolean fetch) {
    return degressifCoefs(qualifier, null, fetch);
  }

  public NSArray degressifCoefs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.serveur.eof.metier.EODegressifCoef.DEGRESSIF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.serveur.eof.metier.EODegressifCoef.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = degressifCoefs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDegressifCoefsRelationship(org.cocktail.corossol.serveur.eof.metier.EODegressifCoef object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEGRESSIF_COEFS_KEY);
  }

  public void removeFromDegressifCoefsRelationship(org.cocktail.corossol.serveur.eof.metier.EODegressifCoef object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEGRESSIF_COEFS_KEY);
  }

  public org.cocktail.corossol.serveur.eof.metier.EODegressifCoef createDegressifCoefsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("DegressifCoef");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEGRESSIF_COEFS_KEY);
    return (org.cocktail.corossol.serveur.eof.metier.EODegressifCoef) eo;
  }

  public void deleteDegressifCoefsRelationship(org.cocktail.corossol.serveur.eof.metier.EODegressifCoef object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEGRESSIF_COEFS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDegressifCoefsRelationships() {
    Enumeration objects = degressifCoefs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDegressifCoefsRelationship((org.cocktail.corossol.serveur.eof.metier.EODegressifCoef)objects.nextElement());
    }
  }


/**
 * Créer une instance de EODegressif avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODegressif createEODegressif(EOEditingContext editingContext, NSTimestamp dgrfDateDebut
			) {
    EODegressif eo = (EODegressif) createAndInsertInstance(editingContext, _EODegressif.ENTITY_NAME);    
		eo.setDgrfDateDebut(dgrfDateDebut);
    return eo;
  }

  
	  public EODegressif localInstanceIn(EOEditingContext editingContext) {
	  		return (EODegressif)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODegressif creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODegressif creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODegressif object = (EODegressif)createAndInsertInstance(editingContext, _EODegressif.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODegressif localInstanceIn(EOEditingContext editingContext, EODegressif eo) {
    EODegressif localInstance = (eo == null) ? null : (EODegressif)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODegressif#localInstanceIn a la place.
   */
	public static EODegressif localInstanceOf(EOEditingContext editingContext, EODegressif eo) {
		return EODegressif.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODegressif fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODegressif fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODegressif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODegressif)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODegressif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODegressif fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODegressif eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODegressif)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODegressif fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODegressif eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODegressif ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODegressif fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
