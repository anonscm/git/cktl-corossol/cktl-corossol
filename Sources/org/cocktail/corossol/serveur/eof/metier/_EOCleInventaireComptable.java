/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCleInventaireComptable.java instead.
package org.cocktail.corossol.serveur.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCleInventaireComptable extends  EOGenericRecord {
	public static final String ENTITY_NAME = "CleInventaireComptable";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.CLE_INVENTAIRE_COMPTABLE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "clicId";

	public static final String CLIC_COMP_KEY = "clicComp";
	public static final String CLIC_CR_KEY = "clicCr";
	public static final String CLIC_DUREE_AMORT_KEY = "clicDureeAmort";
	public static final String CLIC_ETAT_KEY = "clicEtat";
	public static final String CLIC_ID_BIS_KEY = "clicIdBis";
	public static final String CLIC_NB_ETIQUETTE_KEY = "clicNbEtiquette";
	public static final String CLIC_NUM_COMPLET_KEY = "clicNumComplet";
	public static final String CLIC_NUMERO_KEY = "clicNumero";
	public static final String CLIC_PRO_RATA_KEY = "clicProRata";
	public static final String CLIC_TYPE_AMORT_KEY = "clicTypeAmort";

// Attributs non visibles
	public static final String CLIC_ID_KEY = "clicId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCOAID_KEY = "pcoaid";

//Colonnes dans la base de donnees
	public static final String CLIC_COMP_COLKEY = "CLIC_COMP";
	public static final String CLIC_CR_COLKEY = "CLIC_CR";
	public static final String CLIC_DUREE_AMORT_COLKEY = "CLIC_DUREE_AMORT";
	public static final String CLIC_ETAT_COLKEY = "CLIC_ETAT";
	public static final String CLIC_ID_BIS_COLKEY = "CLIC_ID";
	public static final String CLIC_NB_ETIQUETTE_COLKEY = "CLIC_NB_ETIQUETTE";
	public static final String CLIC_NUM_COMPLET_COLKEY = "CLIC_NUM_COMPLET";
	public static final String CLIC_NUMERO_COLKEY = "CLIC_NUMERO";
	public static final String CLIC_PRO_RATA_COLKEY = "CLIC_PRO_RATA";
	public static final String CLIC_TYPE_AMORT_COLKEY = "CLIC_TYPE_AMORT";

	public static final String CLIC_ID_COLKEY = "CLIC_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PCOAID_COLKEY = "PCOA_ID";


	// Relationships
	public static final String CLE_INVENTAIRE_COMPT_MODIFS_KEY = "cleInventaireComptModifs";
	public static final String EXERCICE_KEY = "exercice";
	public static final String INVENTAIRE_COMPTABLES_KEY = "inventaireComptables";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String PLAN_COMPTABLE_AMORT_KEY = "planComptableAmort";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String clicComp() {
    return (String) storedValueForKey(CLIC_COMP_KEY);
  }

  public void setClicComp(String value) {
    takeStoredValueForKey(value, CLIC_COMP_KEY);
  }

  public String clicCr() {
    return (String) storedValueForKey(CLIC_CR_KEY);
  }

  public void setClicCr(String value) {
    takeStoredValueForKey(value, CLIC_CR_KEY);
  }

  public Integer clicDureeAmort() {
    return (Integer) storedValueForKey(CLIC_DUREE_AMORT_KEY);
  }

  public void setClicDureeAmort(Integer value) {
    takeStoredValueForKey(value, CLIC_DUREE_AMORT_KEY);
  }

  public String clicEtat() {
    return (String) storedValueForKey(CLIC_ETAT_KEY);
  }

  public void setClicEtat(String value) {
    takeStoredValueForKey(value, CLIC_ETAT_KEY);
  }

  public Integer clicIdBis() {
    return (Integer) storedValueForKey(CLIC_ID_BIS_KEY);
  }

  public void setClicIdBis(Integer value) {
    takeStoredValueForKey(value, CLIC_ID_BIS_KEY);
  }

  public Integer clicNbEtiquette() {
    return (Integer) storedValueForKey(CLIC_NB_ETIQUETTE_KEY);
  }

  public void setClicNbEtiquette(Integer value) {
    takeStoredValueForKey(value, CLIC_NB_ETIQUETTE_KEY);
  }

  public String clicNumComplet() {
    return (String) storedValueForKey(CLIC_NUM_COMPLET_KEY);
  }

  public void setClicNumComplet(String value) {
    takeStoredValueForKey(value, CLIC_NUM_COMPLET_KEY);
  }

  public String clicNumero() {
    return (String) storedValueForKey(CLIC_NUMERO_KEY);
  }

  public void setClicNumero(String value) {
    takeStoredValueForKey(value, CLIC_NUMERO_KEY);
  }

  public Integer clicProRata() {
    return (Integer) storedValueForKey(CLIC_PRO_RATA_KEY);
  }

  public void setClicProRata(Integer value) {
    takeStoredValueForKey(value, CLIC_PRO_RATA_KEY);
  }

  public String clicTypeAmort() {
    return (String) storedValueForKey(CLIC_TYPE_AMORT_KEY);
  }

  public void setClicTypeAmort(String value) {
    takeStoredValueForKey(value, CLIC_TYPE_AMORT_KEY);
  }

  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOPlanComptable planComptable() {
    return (org.cocktail.application.serveur.eof.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.application.serveur.eof.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOPlanComptableAmo planComptableAmort() {
    return (org.cocktail.application.serveur.eof.EOPlanComptableAmo)storedValueForKey(PLAN_COMPTABLE_AMORT_KEY);
  }

  public void setPlanComptableAmortRelationship(org.cocktail.application.serveur.eof.EOPlanComptableAmo value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOPlanComptableAmo oldValue = planComptableAmort();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_AMORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_AMORT_KEY);
    }
  }
  
  public NSArray cleInventaireComptModifs() {
    return (NSArray)storedValueForKey(CLE_INVENTAIRE_COMPT_MODIFS_KEY);
  }

  public NSArray cleInventaireComptModifs(EOQualifier qualifier) {
    return cleInventaireComptModifs(qualifier, null);
  }

  public NSArray cleInventaireComptModifs(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = cleInventaireComptModifs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToCleInventaireComptModifsRelationship(org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptModif object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CLE_INVENTAIRE_COMPT_MODIFS_KEY);
  }

  public void removeFromCleInventaireComptModifsRelationship(org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptModif object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CLE_INVENTAIRE_COMPT_MODIFS_KEY);
  }

  public org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptModif createCleInventaireComptModifsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CleInventaireComptModif");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CLE_INVENTAIRE_COMPT_MODIFS_KEY);
    return (org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptModif) eo;
  }

  public void deleteCleInventaireComptModifsRelationship(org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptModif object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CLE_INVENTAIRE_COMPT_MODIFS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCleInventaireComptModifsRelationships() {
    Enumeration objects = cleInventaireComptModifs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCleInventaireComptModifsRelationship((org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptModif)objects.nextElement());
    }
  }

  public NSArray inventaireComptables() {
    return (NSArray)storedValueForKey(INVENTAIRE_COMPTABLES_KEY);
  }

  public NSArray inventaireComptables(EOQualifier qualifier) {
    return inventaireComptables(qualifier, null, false);
  }

  public NSArray inventaireComptables(EOQualifier qualifier, boolean fetch) {
    return inventaireComptables(qualifier, null, fetch);
  }

  public NSArray inventaireComptables(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inventaireComptables();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInventaireComptablesRelationship(org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLES_KEY);
  }

  public void removeFromInventaireComptablesRelationship(org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLES_KEY);
  }

  public org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable createInventaireComptablesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("InventaireComptable");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRE_COMPTABLES_KEY);
    return (org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable) eo;
  }

  public void deleteInventaireComptablesRelationship(org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventaireComptablesRelationships() {
    Enumeration objects = inventaireComptables().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventaireComptablesRelationship((org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCleInventaireComptable avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCleInventaireComptable createEOCleInventaireComptable(EOEditingContext editingContext			) {
    EOCleInventaireComptable eo = (EOCleInventaireComptable) createAndInsertInstance(editingContext, _EOCleInventaireComptable.ENTITY_NAME);    
    return eo;
  }

  
	  public EOCleInventaireComptable localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCleInventaireComptable)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCleInventaireComptable creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCleInventaireComptable creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCleInventaireComptable object = (EOCleInventaireComptable)createAndInsertInstance(editingContext, _EOCleInventaireComptable.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCleInventaireComptable localInstanceIn(EOEditingContext editingContext, EOCleInventaireComptable eo) {
    EOCleInventaireComptable localInstance = (eo == null) ? null : (EOCleInventaireComptable)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCleInventaireComptable#localInstanceIn a la place.
   */
	public static EOCleInventaireComptable localInstanceOf(EOEditingContext editingContext, EOCleInventaireComptable eo) {
		return EOCleInventaireComptable.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCleInventaireComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCleInventaireComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCleInventaireComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCleInventaireComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCleInventaireComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCleInventaireComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCleInventaireComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCleInventaireComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCleInventaireComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCleInventaireComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCleInventaireComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCleInventaireComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
