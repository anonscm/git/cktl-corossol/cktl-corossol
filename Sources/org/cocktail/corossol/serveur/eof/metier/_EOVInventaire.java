/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVInventaire.java instead.
package org.cocktail.corossol.serveur.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVInventaire extends  EOGenericRecord {
	public static final String ENTITY_NAME = "VInventaire";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.V_INVENTAIRE";



	// Attributes


	public static final String CLIC_ID_KEY = "clicId";
	public static final String CLIC_NUM_COMPLET_KEY = "clicNumComplet";
	public static final String COMM_ID_KEY = "commId";
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String INVC_ID_KEY = "invcId";
	public static final String INV_ID_KEY = "invId";
	public static final String LID_MONTANT_KEY = "lidMontant";
	public static final String LID_ORDRE_KEY = "lidOrdre";
	public static final String LIV_ORDRE_KEY = "livOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String CLIC_ID_COLKEY = "CLIC_ID";
	public static final String CLIC_NUM_COMPLET_COLKEY = "CLIC_NUM_COMPLET";
	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String INVC_ID_COLKEY = "INVC_ID";
	public static final String INV_ID_COLKEY = "INV_ID";
	public static final String LID_MONTANT_COLKEY = "LID_MONTANT";
	public static final String LID_ORDRE_COLKEY = "LID_ORDRE";
	public static final String LIV_ORDRE_COLKEY = "LIV_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";



	// Relationships
	public static final String CLE_INVENTAIRE_COMPTABLE_KEY = "cleInventaireComptable";
	public static final String INVENTAIRE_COMPTABLE_KEY = "inventaireComptable";
	public static final String LIVRAISON_KEY = "livraison";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer clicId() {
    return (Integer) storedValueForKey(CLIC_ID_KEY);
  }

  public void setClicId(Integer value) {
    takeStoredValueForKey(value, CLIC_ID_KEY);
  }

  public String clicNumComplet() {
    return (String) storedValueForKey(CLIC_NUM_COMPLET_KEY);
  }

  public void setClicNumComplet(String value) {
    takeStoredValueForKey(value, CLIC_NUM_COMPLET_KEY);
  }

  public Integer commId() {
    return (Integer) storedValueForKey(COMM_ID_KEY);
  }

  public void setCommId(Integer value) {
    takeStoredValueForKey(value, COMM_ID_KEY);
  }

  public Integer dpcoId() {
    return (Integer) storedValueForKey(DPCO_ID_KEY);
  }

  public void setDpcoId(Integer value) {
    takeStoredValueForKey(value, DPCO_ID_KEY);
  }

  public Integer invcId() {
    return (Integer) storedValueForKey(INVC_ID_KEY);
  }

  public void setInvcId(Integer value) {
    takeStoredValueForKey(value, INVC_ID_KEY);
  }

  public Integer invId() {
    return (Integer) storedValueForKey(INV_ID_KEY);
  }

  public void setInvId(Integer value) {
    takeStoredValueForKey(value, INV_ID_KEY);
  }

  public java.math.BigDecimal lidMontant() {
    return (java.math.BigDecimal) storedValueForKey(LID_MONTANT_KEY);
  }

  public void setLidMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, LID_MONTANT_KEY);
  }

  public Integer lidOrdre() {
    return (Integer) storedValueForKey(LID_ORDRE_KEY);
  }

  public void setLidOrdre(Integer value) {
    takeStoredValueForKey(value, LID_ORDRE_KEY);
  }

  public Integer livOrdre() {
    return (Integer) storedValueForKey(LIV_ORDRE_KEY);
  }

  public void setLivOrdre(Integer value) {
    takeStoredValueForKey(value, LIV_ORDRE_KEY);
  }

  public Integer orgId() {
    return (Integer) storedValueForKey(ORG_ID_KEY);
  }

  public void setOrgId(Integer value) {
    takeStoredValueForKey(value, ORG_ID_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public Integer tcdOrdre() {
    return (Integer) storedValueForKey(TCD_ORDRE_KEY);
  }

  public void setTcdOrdre(Integer value) {
    takeStoredValueForKey(value, TCD_ORDRE_KEY);
  }

  public org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptable cleInventaireComptable() {
    return (org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptable)storedValueForKey(CLE_INVENTAIRE_COMPTABLE_KEY);
  }

  public void setCleInventaireComptableRelationship(org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptable value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOCleInventaireComptable oldValue = cleInventaireComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CLE_INVENTAIRE_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CLE_INVENTAIRE_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable inventaireComptable() {
    return (org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable)storedValueForKey(INVENTAIRE_COMPTABLE_KEY);
  }

  public void setInventaireComptableRelationship(org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable oldValue = inventaireComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INVENTAIRE_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INVENTAIRE_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.corossol.serveur.eof.metier.EOLivraison livraison() {
    return (org.cocktail.corossol.serveur.eof.metier.EOLivraison)storedValueForKey(LIVRAISON_KEY);
  }

  public void setLivraisonRelationship(org.cocktail.corossol.serveur.eof.metier.EOLivraison value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOLivraison oldValue = livraison();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LIVRAISON_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LIVRAISON_KEY);
    }
  }
  

/**
 * Créer une instance de EOVInventaire avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVInventaire createEOVInventaire(EOEditingContext editingContext, Integer clicId
, Integer commId
, Integer invcId
, Integer invId
, Integer lidOrdre
, Integer livOrdre
, Integer orgId
, String pcoNum
, Integer tcdOrdre
			) {
    EOVInventaire eo = (EOVInventaire) createAndInsertInstance(editingContext, _EOVInventaire.ENTITY_NAME);    
		eo.setClicId(clicId);
		eo.setCommId(commId);
		eo.setInvcId(invcId);
		eo.setInvId(invId);
		eo.setLidOrdre(lidOrdre);
		eo.setLivOrdre(livOrdre);
		eo.setOrgId(orgId);
		eo.setPcoNum(pcoNum);
		eo.setTcdOrdre(tcdOrdre);
    return eo;
  }

  
	  public EOVInventaire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVInventaire)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVInventaire creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVInventaire creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVInventaire object = (EOVInventaire)createAndInsertInstance(editingContext, _EOVInventaire.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVInventaire localInstanceIn(EOEditingContext editingContext, EOVInventaire eo) {
    EOVInventaire localInstance = (eo == null) ? null : (EOVInventaire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVInventaire#localInstanceIn a la place.
   */
	public static EOVInventaire localInstanceOf(EOEditingContext editingContext, EOVInventaire eo) {
		return EOVInventaire.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVInventaire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVInventaire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVInventaire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVInventaire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVInventaire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVInventaire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVInventaire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVInventaire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
