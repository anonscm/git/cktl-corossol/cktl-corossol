/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.corossol.serveur.eof.metier;

import java.math.BigDecimal;

import org.cocktail.corossol.serveur.eof.metier.EOInventaireComptableOrv;
import org.cocktail.corossol.common.eof.repartition.IInventaireComptable;

import com.webobjects.foundation.NSValidation;

public class EOInventaireComptable extends _EOInventaireComptable implements IInventaireComptable {

    public EOInventaireComptable() {
        super();
    }

    
    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	// super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
        //super.validateBeforeTransactionSave();
    }

    
    public BigDecimal sommePourcentageOriginesFinancement() {
        BigDecimal somme = BigDecimal.ZERO;

        if (inventaireComptableOrigs() == null || inventaireComptableOrigs().count() == 0) {
            return somme;
        }

        for (int i = 0; i < inventaireComptableOrigs().count(); i++) {
            EOInventaireComptableOrig orig = (EOInventaireComptableOrig) inventaireComptableOrigs().objectAtIndex(i);
            if (orig != null && orig.icorPourcentage() != null) {
                somme = somme.add(orig.icorPourcentage());
            }
        }
        return somme;
    }

    /**
     * Retourne la somme des montants des origines de financements
     * 
     * @return
     */
    public BigDecimal sommeMontantOriginesFinancement() {
        BigDecimal somme = BigDecimal.ZERO;
        if (inventaireComptableOrigs() == null || inventaireComptableOrigs().count() == 0) {
            return somme;
        }

        for (int i = 0; i < inventaireComptableOrigs().count(); i++) {
            EOInventaireComptableOrig orig = (EOInventaireComptableOrig) inventaireComptableOrigs().objectAtIndex(i);
            if (orig != null && orig.icorMontant() != null) {
                somme = somme.add(orig.icorMontant());
            }
        }
        return somme;
    }

    public String methodeDeCalculPourLesRepartitions() {
        // TODO a finir
        return MONTANT;
    }
    
	public BigDecimal invcMontantAmortissableAvecOrvs() {
		return invcMontantAmortissable().add(
				(BigDecimal) inventaireComptableOrvs().valueForKey(
						"@sum." + EOInventaireComptableOrv.INVO_MONTANT_ORV_KEY));
	}
}
