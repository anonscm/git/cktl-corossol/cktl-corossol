/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVCommande.java instead.
package org.cocktail.corossol.serveur.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVCommande extends  EOGenericRecord {
	public static final String ENTITY_NAME = "VCommande";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.V_COMMANDE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "commId";

	public static final String COMM_DATE_KEY = "commDate";
	public static final String COMM_DATE_CREATION_KEY = "commDateCreation";
	public static final String COMM_LIBELLE_KEY = "commLibelle";
	public static final String COMM_NUMERO_KEY = "commNumero";
	public static final String COMM_REFERENCE_KEY = "commReference";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String TYET_ID_IMPRIMABLE_KEY = "tyetIdImprimable";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

// Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String DEV_ID_KEY = "devId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";

//Colonnes dans la base de donnees
	public static final String COMM_DATE_COLKEY = "COMM_DATE";
	public static final String COMM_DATE_CREATION_COLKEY = "COMM_DATE_CREATION";
	public static final String COMM_LIBELLE_COLKEY = "COMM_LIBELLE";
	public static final String COMM_NUMERO_COLKEY = "COMM_NUMERO";
	public static final String COMM_REFERENCE_COLKEY = "COMM_REFERENCE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String TYET_ID_IMPRIMABLE_COLKEY = "TYET_ID_IMPRIMABLE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String DEV_ID_COLKEY = "DEV_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";


	// Relationships
	public static final String DEVISE_KEY = "devise";
	public static final String EXERCICE_KEY = "exercice";
	public static final String V_FOURNISSEUR_KEY = "vFournisseur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp commDate() {
    return (NSTimestamp) storedValueForKey(COMM_DATE_KEY);
  }

  public void setCommDate(NSTimestamp value) {
    takeStoredValueForKey(value, COMM_DATE_KEY);
  }

  public NSTimestamp commDateCreation() {
    return (NSTimestamp) storedValueForKey(COMM_DATE_CREATION_KEY);
  }

  public void setCommDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, COMM_DATE_CREATION_KEY);
  }

  public String commLibelle() {
    return (String) storedValueForKey(COMM_LIBELLE_KEY);
  }

  public void setCommLibelle(String value) {
    takeStoredValueForKey(value, COMM_LIBELLE_KEY);
  }

  public Integer commNumero() {
    return (Integer) storedValueForKey(COMM_NUMERO_KEY);
  }

  public void setCommNumero(Integer value) {
    takeStoredValueForKey(value, COMM_NUMERO_KEY);
  }

  public String commReference() {
    return (String) storedValueForKey(COMM_REFERENCE_KEY);
  }

  public void setCommReference(String value) {
    takeStoredValueForKey(value, COMM_REFERENCE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public Integer tyetId() {
    return (Integer) storedValueForKey(TYET_ID_KEY);
  }

  public void setTyetId(Integer value) {
    takeStoredValueForKey(value, TYET_ID_KEY);
  }

  public Integer tyetIdImprimable() {
    return (Integer) storedValueForKey(TYET_ID_IMPRIMABLE_KEY);
  }

  public void setTyetIdImprimable(Integer value) {
    takeStoredValueForKey(value, TYET_ID_IMPRIMABLE_KEY);
  }

  public Integer utlOrdre() {
    return (Integer) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Integer value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public org.cocktail.application.serveur.eof.EODevise devise() {
    return (org.cocktail.application.serveur.eof.EODevise)storedValueForKey(DEVISE_KEY);
  }

  public void setDeviseRelationship(org.cocktail.application.serveur.eof.EODevise value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EODevise oldValue = devise();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEVISE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEVISE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.VFournisseur vFournisseur() {
    return (org.cocktail.application.serveur.eof.VFournisseur)storedValueForKey(V_FOURNISSEUR_KEY);
  }

  public void setVFournisseurRelationship(org.cocktail.application.serveur.eof.VFournisseur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.VFournisseur oldValue = vFournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_FOURNISSEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOVCommande avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVCommande createEOVCommande(EOEditingContext editingContext, NSTimestamp commDate
, NSTimestamp commDateCreation
, String commLibelle
, Integer commNumero
, String commReference
, String pcoNum
, Integer tyetId
, Integer tyetIdImprimable
, Integer utlOrdre
			) {
    EOVCommande eo = (EOVCommande) createAndInsertInstance(editingContext, _EOVCommande.ENTITY_NAME);    
		eo.setCommDate(commDate);
		eo.setCommDateCreation(commDateCreation);
		eo.setCommLibelle(commLibelle);
		eo.setCommNumero(commNumero);
		eo.setCommReference(commReference);
		eo.setPcoNum(pcoNum);
		eo.setTyetId(tyetId);
		eo.setTyetIdImprimable(tyetIdImprimable);
		eo.setUtlOrdre(utlOrdre);
    return eo;
  }

  
	  public EOVCommande localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVCommande)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVCommande creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVCommande creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVCommande object = (EOVCommande)createAndInsertInstance(editingContext, _EOVCommande.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVCommande localInstanceIn(EOEditingContext editingContext, EOVCommande eo) {
    EOVCommande localInstance = (eo == null) ? null : (EOVCommande)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVCommande#localInstanceIn a la place.
   */
	public static EOVCommande localInstanceOf(EOEditingContext editingContext, EOVCommande eo) {
		return EOVCommande.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVCommande fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVCommande fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVCommande)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVCommande fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVCommande eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVCommande)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVCommande fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVCommande eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVCommande ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVCommande fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
