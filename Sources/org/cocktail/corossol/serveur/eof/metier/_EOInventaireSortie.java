/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOInventaireSortie.java instead.
package org.cocktail.corossol.serveur.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOInventaireSortie extends  EOGenericRecord {
	public static final String ENTITY_NAME = "InventaireSortie";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.INVENTAIRE_SORTIE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "invsId";

	public static final String INVS_DATE_KEY = "invsDate";
	public static final String INVS_MOTIF_KEY = "invsMotif";
	public static final String INVS_VALEUR_CESSION_KEY = "invsValeurCession";
	public static final String INVS_VNC_KEY = "invsVnc";

// Attributs non visibles
	public static final String INV_ID_KEY = "invId";
	public static final String INVS_ID_KEY = "invsId";
	public static final String TYSO_ID_KEY = "tysoId";

//Colonnes dans la base de donnees
	public static final String INVS_DATE_COLKEY = "INVS_DATE";
	public static final String INVS_MOTIF_COLKEY = "INVS_MOTIF";
	public static final String INVS_VALEUR_CESSION_COLKEY = "INVS_VALEUR_CESSION";
	public static final String INVS_VNC_COLKEY = "INVS_VNC";

	public static final String INV_ID_COLKEY = "INV_ID";
	public static final String INVS_ID_COLKEY = "INVS_ID";
	public static final String TYSO_ID_COLKEY = "TYSO_ID";


	// Relationships
	public static final String INVENTAIRE_KEY = "inventaire";
	public static final String TYPE_SORTIE_KEY = "typeSortie";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp invsDate() {
    return (NSTimestamp) storedValueForKey(INVS_DATE_KEY);
  }

  public void setInvsDate(NSTimestamp value) {
    takeStoredValueForKey(value, INVS_DATE_KEY);
  }

  public String invsMotif() {
    return (String) storedValueForKey(INVS_MOTIF_KEY);
  }

  public void setInvsMotif(String value) {
    takeStoredValueForKey(value, INVS_MOTIF_KEY);
  }

  public java.math.BigDecimal invsValeurCession() {
    return (java.math.BigDecimal) storedValueForKey(INVS_VALEUR_CESSION_KEY);
  }

  public void setInvsValeurCession(java.math.BigDecimal value) {
    takeStoredValueForKey(value, INVS_VALEUR_CESSION_KEY);
  }

  public java.math.BigDecimal invsVnc() {
    return (java.math.BigDecimal) storedValueForKey(INVS_VNC_KEY);
  }

  public void setInvsVnc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, INVS_VNC_KEY);
  }

  public org.cocktail.corossol.serveur.eof.metier.EOInventaire inventaire() {
    return (org.cocktail.corossol.serveur.eof.metier.EOInventaire)storedValueForKey(INVENTAIRE_KEY);
  }

  public void setInventaireRelationship(org.cocktail.corossol.serveur.eof.metier.EOInventaire value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOInventaire oldValue = inventaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INVENTAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INVENTAIRE_KEY);
    }
  }
  
  public org.cocktail.corossol.serveur.eof.metier.EOTypeSortie typeSortie() {
    return (org.cocktail.corossol.serveur.eof.metier.EOTypeSortie)storedValueForKey(TYPE_SORTIE_KEY);
  }

  public void setTypeSortieRelationship(org.cocktail.corossol.serveur.eof.metier.EOTypeSortie value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOTypeSortie oldValue = typeSortie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_SORTIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_SORTIE_KEY);
    }
  }
  

/**
 * Créer une instance de EOInventaireSortie avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOInventaireSortie createEOInventaireSortie(EOEditingContext editingContext, NSTimestamp invsDate
, String invsMotif
, java.math.BigDecimal invsValeurCession
, java.math.BigDecimal invsVnc
			) {
    EOInventaireSortie eo = (EOInventaireSortie) createAndInsertInstance(editingContext, _EOInventaireSortie.ENTITY_NAME);    
		eo.setInvsDate(invsDate);
		eo.setInvsMotif(invsMotif);
		eo.setInvsValeurCession(invsValeurCession);
		eo.setInvsVnc(invsVnc);
    return eo;
  }

  
	  public EOInventaireSortie localInstanceIn(EOEditingContext editingContext) {
	  		return (EOInventaireSortie)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOInventaireSortie creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOInventaireSortie creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOInventaireSortie object = (EOInventaireSortie)createAndInsertInstance(editingContext, _EOInventaireSortie.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOInventaireSortie localInstanceIn(EOEditingContext editingContext, EOInventaireSortie eo) {
    EOInventaireSortie localInstance = (eo == null) ? null : (EOInventaireSortie)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOInventaireSortie#localInstanceIn a la place.
   */
	public static EOInventaireSortie localInstanceOf(EOEditingContext editingContext, EOInventaireSortie eo) {
		return EOInventaireSortie.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOInventaireSortie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOInventaireSortie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOInventaireSortie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOInventaireSortie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOInventaireSortie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOInventaireSortie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOInventaireSortie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOInventaireSortie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOInventaireSortie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOInventaireSortie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOInventaireSortie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOInventaireSortie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
