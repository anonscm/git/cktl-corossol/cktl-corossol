/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOInventaireNonBudgetaire.java instead.
package org.cocktail.corossol.serveur.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOInventaireNonBudgetaire extends  EOGenericRecord {
	public static final String ENTITY_NAME = "InventaireNonBudgetaire";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.INVENTAIRE_NON_BUDGETAIRE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "inbId";

	public static final String INB_DATE_ACQUISITION_KEY = "inbDateAcquisition";
	public static final String INB_DUREE_KEY = "inbDuree";
	public static final String INB_FACTURE_KEY = "inbFacture";
	public static final String INB_FOURNISSEUR_KEY = "inbFournisseur";
	public static final String INB_ID_BIS_KEY = "inbIdBis";
	public static final String INB_INFORMATIONS_KEY = "inbInformations";
	public static final String INB_MONTANT_KEY = "inbMontant";
	public static final String INB_MONTANT_RESIDUEL_KEY = "inbMontantResiduel";
	public static final String INB_NUMERO_SERIE_KEY = "inbNumeroSerie";
	public static final String INB_TYPE_AMORT_KEY = "inbTypeAmort";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String INB_ID_KEY = "inbId";
	public static final String INVC_ID_KEY = "invcId";
	public static final String ORG_ID_KEY = "orgId";
	public static final String ORGF_ID_KEY = "orgfId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String INB_DATE_ACQUISITION_COLKEY = "INB_DATE_ACQUISITION";
	public static final String INB_DUREE_COLKEY = "INB_DUREE";
	public static final String INB_FACTURE_COLKEY = "INB_FACTURE";
	public static final String INB_FOURNISSEUR_COLKEY = "INB_FOURNISSEUR";
	public static final String INB_ID_BIS_COLKEY = "INB_ID";
	public static final String INB_INFORMATIONS_COLKEY = "INB_INFORMATIONS";
	public static final String INB_MONTANT_COLKEY = "INB_MONTANT";
	public static final String INB_MONTANT_RESIDUEL_COLKEY = "INB_MONTANT_RESIDUEL";
	public static final String INB_NUMERO_SERIE_COLKEY = "INB_NUMERO_SERIE";
	public static final String INB_TYPE_AMORT_COLKEY = "INB_TYPE_AMORT";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String INB_ID_COLKEY = "INB_ID";
	public static final String INVC_ID_COLKEY = "INVC_ID";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String ORGF_ID_COLKEY = "ORIG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String INVENTAIRE_COMPTABLE_KEY = "inventaireComptable";
	public static final String ORGAN_KEY = "organ";
	public static final String ORIGINE_FINANCEMENT_KEY = "origineFinancement";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp inbDateAcquisition() {
    return (NSTimestamp) storedValueForKey(INB_DATE_ACQUISITION_KEY);
  }

  public void setInbDateAcquisition(NSTimestamp value) {
    takeStoredValueForKey(value, INB_DATE_ACQUISITION_KEY);
  }

  public Integer inbDuree() {
    return (Integer) storedValueForKey(INB_DUREE_KEY);
  }

  public void setInbDuree(Integer value) {
    takeStoredValueForKey(value, INB_DUREE_KEY);
  }

  public String inbFacture() {
    return (String) storedValueForKey(INB_FACTURE_KEY);
  }

  public void setInbFacture(String value) {
    takeStoredValueForKey(value, INB_FACTURE_KEY);
  }

  public String inbFournisseur() {
    return (String) storedValueForKey(INB_FOURNISSEUR_KEY);
  }

  public void setInbFournisseur(String value) {
    takeStoredValueForKey(value, INB_FOURNISSEUR_KEY);
  }

  public Integer inbIdBis() {
    return (Integer) storedValueForKey(INB_ID_BIS_KEY);
  }

  public void setInbIdBis(Integer value) {
    takeStoredValueForKey(value, INB_ID_BIS_KEY);
  }

  public String inbInformations() {
    return (String) storedValueForKey(INB_INFORMATIONS_KEY);
  }

  public void setInbInformations(String value) {
    takeStoredValueForKey(value, INB_INFORMATIONS_KEY);
  }

  public java.math.BigDecimal inbMontant() {
    return (java.math.BigDecimal) storedValueForKey(INB_MONTANT_KEY);
  }

  public void setInbMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, INB_MONTANT_KEY);
  }

  public java.math.BigDecimal inbMontantResiduel() {
    return (java.math.BigDecimal) storedValueForKey(INB_MONTANT_RESIDUEL_KEY);
  }

  public void setInbMontantResiduel(java.math.BigDecimal value) {
    takeStoredValueForKey(value, INB_MONTANT_RESIDUEL_KEY);
  }

  public String inbNumeroSerie() {
    return (String) storedValueForKey(INB_NUMERO_SERIE_KEY);
  }

  public void setInbNumeroSerie(String value) {
    takeStoredValueForKey(value, INB_NUMERO_SERIE_KEY);
  }

  public String inbTypeAmort() {
    return (String) storedValueForKey(INB_TYPE_AMORT_KEY);
  }

  public void setInbTypeAmort(String value) {
    takeStoredValueForKey(value, INB_TYPE_AMORT_KEY);
  }

  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable inventaireComptable() {
    return (org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable)storedValueForKey(INVENTAIRE_COMPTABLE_KEY);
  }

  public void setInventaireComptableRelationship(org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOInventaireComptable oldValue = inventaireComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INVENTAIRE_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INVENTAIRE_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.corossol.serveur.eof.metier.EOOrigineFinancement origineFinancement() {
    return (org.cocktail.corossol.serveur.eof.metier.EOOrigineFinancement)storedValueForKey(ORIGINE_FINANCEMENT_KEY);
  }

  public void setOrigineFinancementRelationship(org.cocktail.corossol.serveur.eof.metier.EOOrigineFinancement value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOOrigineFinancement oldValue = origineFinancement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORIGINE_FINANCEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORIGINE_FINANCEMENT_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOPlanComptable planComptable() {
    return (org.cocktail.application.serveur.eof.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.application.serveur.eof.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOInventaireNonBudgetaire avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOInventaireNonBudgetaire createEOInventaireNonBudgetaire(EOEditingContext editingContext, Integer inbDuree
, java.math.BigDecimal inbMontant
, java.math.BigDecimal inbMontantResiduel
, String inbTypeAmort
, org.cocktail.application.serveur.eof.EOExercice exercice, org.cocktail.application.serveur.eof.EOOrgan organ, org.cocktail.corossol.serveur.eof.metier.EOOrigineFinancement origineFinancement, org.cocktail.application.serveur.eof.EOPlanComptable planComptable, org.cocktail.application.serveur.eof.EOUtilisateur utilisateur			) {
    EOInventaireNonBudgetaire eo = (EOInventaireNonBudgetaire) createAndInsertInstance(editingContext, _EOInventaireNonBudgetaire.ENTITY_NAME);    
		eo.setInbDuree(inbDuree);
		eo.setInbMontant(inbMontant);
		eo.setInbMontantResiduel(inbMontantResiduel);
		eo.setInbTypeAmort(inbTypeAmort);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setOrigineFinancementRelationship(origineFinancement);
    eo.setPlanComptableRelationship(planComptable);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOInventaireNonBudgetaire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOInventaireNonBudgetaire)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOInventaireNonBudgetaire creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOInventaireNonBudgetaire creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOInventaireNonBudgetaire object = (EOInventaireNonBudgetaire)createAndInsertInstance(editingContext, _EOInventaireNonBudgetaire.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOInventaireNonBudgetaire localInstanceIn(EOEditingContext editingContext, EOInventaireNonBudgetaire eo) {
    EOInventaireNonBudgetaire localInstance = (eo == null) ? null : (EOInventaireNonBudgetaire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOInventaireNonBudgetaire#localInstanceIn a la place.
   */
	public static EOInventaireNonBudgetaire localInstanceOf(EOEditingContext editingContext, EOInventaireNonBudgetaire eo) {
		return EOInventaireNonBudgetaire.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOInventaireNonBudgetaire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOInventaireNonBudgetaire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOInventaireNonBudgetaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOInventaireNonBudgetaire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOInventaireNonBudgetaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOInventaireNonBudgetaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOInventaireNonBudgetaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOInventaireNonBudgetaire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOInventaireNonBudgetaire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOInventaireNonBudgetaire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOInventaireNonBudgetaire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOInventaireNonBudgetaire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
