/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVCommandeEngageCtrlPlanco.java instead.
package org.cocktail.corossol.serveur.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVCommandeEngageCtrlPlanco extends  EOGenericRecord {
	public static final String ENTITY_NAME = "VCommandeEngageCtrlPlanco";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.V_COMMANDE_ENGAGE_CTRL_PLANCO";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "epcoId";

	public static final String COMM_DATE_KEY = "commDate";
	public static final String COMM_ID_KEY = "commId";
	public static final String COMM_LIBELLE_KEY = "commLibelle";
	public static final String COMM_NUMERO_KEY = "commNumero";
	public static final String TAP_TAUX_KEY = "tapTaux";

// Attributs non visibles
	public static final String DEV_ID_KEY = "devId";
	public static final String EPCO_ID_KEY = "epcoId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String COMM_DATE_COLKEY = "COMM_DATE";
	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String COMM_LIBELLE_COLKEY = "COMM_LIBELLE";
	public static final String COMM_NUMERO_COLKEY = "COMM_NUMERO";
	public static final String TAP_TAUX_COLKEY = "TAP_TAUX";

	public static final String DEV_ID_COLKEY = "DEV_ID";
	public static final String EPCO_ID_COLKEY = "EPCO_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String DEVISE_KEY = "devise";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final String UTILISATEUR_KEY = "utilisateur";
	public static final String V_COMMANDE_KEY = "vCommande";
	public static final String V_FOURNISSEUR_KEY = "vFournisseur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp commDate() {
    return (NSTimestamp) storedValueForKey(COMM_DATE_KEY);
  }

  public void setCommDate(NSTimestamp value) {
    takeStoredValueForKey(value, COMM_DATE_KEY);
  }

  public Integer commId() {
    return (Integer) storedValueForKey(COMM_ID_KEY);
  }

  public void setCommId(Integer value) {
    takeStoredValueForKey(value, COMM_ID_KEY);
  }

  public String commLibelle() {
    return (String) storedValueForKey(COMM_LIBELLE_KEY);
  }

  public void setCommLibelle(String value) {
    takeStoredValueForKey(value, COMM_LIBELLE_KEY);
  }

  public Integer commNumero() {
    return (Integer) storedValueForKey(COMM_NUMERO_KEY);
  }

  public void setCommNumero(Integer value) {
    takeStoredValueForKey(value, COMM_NUMERO_KEY);
  }

  public java.math.BigDecimal tapTaux() {
    return (java.math.BigDecimal) storedValueForKey(TAP_TAUX_KEY);
  }

  public void setTapTaux(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TAP_TAUX_KEY);
  }

  public org.cocktail.application.serveur.eof.EODevise devise() {
    return (org.cocktail.application.serveur.eof.EODevise)storedValueForKey(DEVISE_KEY);
  }

  public void setDeviseRelationship(org.cocktail.application.serveur.eof.EODevise value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EODevise oldValue = devise();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEVISE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEVISE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOOrgan organ() {
    return (org.cocktail.application.serveur.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.serveur.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOPlanComptable planComptable() {
    return (org.cocktail.application.serveur.eof.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.application.serveur.eof.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeCredit typeCredit() {
    return (org.cocktail.application.serveur.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.application.serveur.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public org.cocktail.corossol.serveur.eof.metier.EOVCommande vCommande() {
    return (org.cocktail.corossol.serveur.eof.metier.EOVCommande)storedValueForKey(V_COMMANDE_KEY);
  }

  public void setVCommandeRelationship(org.cocktail.corossol.serveur.eof.metier.EOVCommande value) {
    if (value == null) {
    	org.cocktail.corossol.serveur.eof.metier.EOVCommande oldValue = vCommande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_COMMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_COMMANDE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.VFournisseur vFournisseur() {
    return (org.cocktail.application.serveur.eof.VFournisseur)storedValueForKey(V_FOURNISSEUR_KEY);
  }

  public void setVFournisseurRelationship(org.cocktail.application.serveur.eof.VFournisseur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.VFournisseur oldValue = vFournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_FOURNISSEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOVCommandeEngageCtrlPlanco avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVCommandeEngageCtrlPlanco createEOVCommandeEngageCtrlPlanco(EOEditingContext editingContext, NSTimestamp commDate
, Integer commId
, String commLibelle
, Integer commNumero
, java.math.BigDecimal tapTaux
			) {
    EOVCommandeEngageCtrlPlanco eo = (EOVCommandeEngageCtrlPlanco) createAndInsertInstance(editingContext, _EOVCommandeEngageCtrlPlanco.ENTITY_NAME);    
		eo.setCommDate(commDate);
		eo.setCommId(commId);
		eo.setCommLibelle(commLibelle);
		eo.setCommNumero(commNumero);
		eo.setTapTaux(tapTaux);
    return eo;
  }

  
	  public EOVCommandeEngageCtrlPlanco localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVCommandeEngageCtrlPlanco)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVCommandeEngageCtrlPlanco creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVCommandeEngageCtrlPlanco creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVCommandeEngageCtrlPlanco object = (EOVCommandeEngageCtrlPlanco)createAndInsertInstance(editingContext, _EOVCommandeEngageCtrlPlanco.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVCommandeEngageCtrlPlanco localInstanceIn(EOEditingContext editingContext, EOVCommandeEngageCtrlPlanco eo) {
    EOVCommandeEngageCtrlPlanco localInstance = (eo == null) ? null : (EOVCommandeEngageCtrlPlanco)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVCommandeEngageCtrlPlanco#localInstanceIn a la place.
   */
	public static EOVCommandeEngageCtrlPlanco localInstanceOf(EOEditingContext editingContext, EOVCommandeEngageCtrlPlanco eo) {
		return EOVCommandeEngageCtrlPlanco.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVCommandeEngageCtrlPlanco fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVCommandeEngageCtrlPlanco fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVCommandeEngageCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVCommandeEngageCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVCommandeEngageCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVCommandeEngageCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVCommandeEngageCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVCommandeEngageCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVCommandeEngageCtrlPlanco fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVCommandeEngageCtrlPlanco eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVCommandeEngageCtrlPlanco ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVCommandeEngageCtrlPlanco fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
