package org.cocktail.corossol.serveur;
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

import java.util.Properties;

import org.cocktail.application.serveur.CocktailApplication;
import org.cocktail.corossol.serveur.components.Main;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation._NSUtilities;
import com.woinject.WOInject;


public class Application extends CocktailApplication {
	
	private VersionDatabase dataBaseVersion;
	private A_CktlVersion applicationVersion;

	public static void main(String argv[]) {
		NSLegacyBundleMonkeyPatch.apply();
		WOInject.init("org.cocktail.corossol.serveur.Application", argv);
	}

	public Application() {
		
		try {
			CktlLog.rawLog("Controle de la version de la base de données\n");
			appCktlVersionDb().checkDependencies();
			CktlLog.rawLog("\n");
		} catch (Exception e) {
			e.printStackTrace();
			CktlLog.rawLog("La version de base de données n'est pas celle requise\n");
			System.exit(-1);
		}
		
		Properties prop = System.getProperties();
		addLogMessage("INFORMATION", "WOOutputPath = "+ (prop.getProperty("WOOutputPath")==null? "":prop.getProperty("WOOutputPath")));
		addLogMessage("INFORMATION", "WOPort = " + (prop.getProperty("WOPort")==null ? "":prop.getProperty("WOPort")));
		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));
		
		fixClassLoading();
		
		 EOEditingContext.setDefaultFetchTimestampLag(2);
	}
	
	private void fixClassLoading() {
		_NSUtilities.setClassForName(Main.class, Main.class.getSimpleName());
	}
	
	public A_CktlVersion appCktlVersion() {
		if (applicationVersion==null) applicationVersion = new Version();
		return applicationVersion;
	}
	
	public A_CktlVersion appCktlVersionDb() {
		if (dataBaseVersion==null) dataBaseVersion = new VersionDatabase();
		return dataBaseVersion;
	}

	public String version() {
		return versionApplication().txtAppliVersion();
	}

	public String getMailHost() {
		return getMandatoryParameterFoKeyString("HOST_MAIL");
	}

	public String configFileName() {
		return "Corossol.config";
	}

	public String getVersionApp() {
		return version();
	}

	public String configTableName() {
		return "FwkCktlWebApp_GrhumParametres";
	}

	public String mainModelName() {
		return "corossol";
	}
}
