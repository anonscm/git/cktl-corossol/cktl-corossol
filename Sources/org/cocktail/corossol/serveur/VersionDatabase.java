package org.cocktail.corossol.serveur;

import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleUser;

/**
*
* Permet de controler les versions de votre user base de donnees.
* A adapter a votre configuration.
* @see org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleUser
*/

public class VersionDatabase extends CktlVersionOracleUser {
	private static String NAME = "BD USER JEFY_INVENTAIRE";
	private static String DB_USER_TABLE_NAME = "JEFY_INVENTAIRE.DB_VERSION";
	private static String DB_VERSION_DATE_COLUMN_NAME = "DB_VERSION_DATE";
	private static String DB_VERSION_ID_COLUMN_NAME = "DB_VERSION_LIBELLE";
	
	public String dbUserTableName() {
		return DB_USER_TABLE_NAME;
	}

	public String dbVersionDateColumnName() {
		return DB_VERSION_DATE_COLUMN_NAME;
	}

	public String dbVersionIdColumnName() {
		return DB_VERSION_ID_COLUMN_NAME;
	}

	public String name() {
		return NAME;
	}

	public String sqlRestriction() {
		return "";
	}
	
	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[]{
				new CktlVersionRequirements(this, VersionMe.MINDBVERSION, null, true)
		};
	}
}
