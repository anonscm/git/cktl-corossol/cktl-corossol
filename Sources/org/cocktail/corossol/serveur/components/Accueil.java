/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.corossol.serveur.components;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.cocktail.corossol.serveur.Application;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eodistribution.WOJavaClientComponent;
import com.webobjects.eodistribution._EOWebStartAction;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

public class Accueil extends WOComponent {
    public Accueil(WOContext context) {
        super(context);
    }
    public String javaClientLink() {
        return WOJavaClientComponent.webStartActionURL(context(), "JavaClient");
    } 
    
    public String version () {
      return ((Application) application()).version();
    }
    
    public String nomApp () {
        return ((Application) application()).applicationName();
    }
    
    public WOActionResults genererJNLP() {
    	
    	WOResponse jnlpFile = new WOResponse();
    	
    	String Serveur = context().request()._serverName();
    	if (Serveur==null) Serveur = "ServeurPortIntrouvable";
    	String Port = context().request()._serverPort();
    	if (Port==null) Port = ""; else Port = ":"+Port;
    	String adresseServeur = Serveur+Port;
    	String urlServeurCGI = context().completeURLWithRequestHandlerKey(null, null, null, false, 0);
    	if (urlServeurCGI==null) urlServeurCGI = "URLServeurCGIIntrouvable";
        
    	String fileName =  nomApp() + ".jnlp";      
    	String filePathName = getAppResourcesDir()+fileName;
    	
        String data = chargerFichier(filePathName).replace("$P{SERVEURPORT}", adresseServeur)
        		                                  .replace("$P{URLSERVEURCGI}", urlServeurCGI);
        
        // Pour IE
        jnlpFile.disableClientCaching();
        jnlpFile.removeHeadersForKey("Cache-Control");
        jnlpFile.removeHeadersForKey("pragma");
         
        jnlpFile.setHeader( "attachement; filename=\"" + fileName + "\"", "Content-Disposition"); 
        jnlpFile.setHeader(Integer.toString(data.length()), "content-length");
    	jnlpFile.appendContentData(new NSData(data));
        jnlpFile.setHeader("x-eojavaclient-message", "Content-type");
    	
    	return jnlpFile; 
    }
    
    /**
	 * Retourne le chemin absolu du repertoire contenant les ressources de
	 * l'appli.
	 */
	static public String getAppResourcesDir() {
		return  WOApplication.application().path() + File.separator + 
				"Contents" + File.separator + 
		        "Resources" + File.separator;
	}
	/**
	 * Charge un fichier texte
	 * @param strFile
	 * @return
	 */
	String chargerFichier(String filePathName)
    {
		try {
		    BufferedReader br = new BufferedReader(new FileReader(filePathName));
		    StringBuffer str = new StringBuffer();
		    String line = br.readLine();
		    while (line != null) {
		        str.append(line);
		        str.append("\n");
		        line = br.readLine();
		    }
		    return str.toString();
		}
		catch (Exception e) { 
    	          e.printStackTrace(); 
                  return "ERROR loading file "+filePathName; 
        }
    }

    
}