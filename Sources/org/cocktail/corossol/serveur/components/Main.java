package org.cocktail.corossol.serveur.components;
//CONTRAT DE LICENCE DE LOGICIEL LIBRE CeCILL
//                ===========================================
//                	
//                	
//                	Avertissement
//                	-------------
//                	
//                	Ce contrat est une licence de logiciel libre issue d'une concertation  entre
//                	ses auteurs afin que le respect  de  deux  grands  principes  preside  à  sa
//                	r�daction :
//                		- d'une part, sa conformité au droit fran�ais, tant au regard du droit de
//                		la responsabilité civile que du droit de la  propri�té  intellectuelle
//                		et de la protection qu'il offre aux auteurs et titulaires  des  droits
//                		patrimoniaux sur un logiciel.
//                		- d'autre part, le respect des principes de diffusion des logiciels
//                		libres : acc�s au code  source, droits �tendus conf�r�s aux
//                		utilisateurs.
//                		
//                		Les auteurs de la cette licence CeCILL (Ce : CEA, C : CNRS, I : INRIA, LL :
//                			Logiciel Libre) sont :
//                				
//                				Commissariat à l'Energie Atomique - CEA, etablissement public  de  caract�re
//                				scientifique technique et industriel, dont le si�ge est situé 31-33  rue  de
//                				la F�d�ration, 75752 PARIS cedex 15.
//                				
//                				Centre National de la Recherche Scientifique - CNRS, etablissement public  �
//                				caract�re scientifique et technologique, dont  le  si�ge  est  situé  3  rue
//                				Michel-Ange 75794 Paris cedex 16.
//                				
//                				Institut National de Recherche en Informatique  et  en  Automatique - INRIA,
//                				etablissement public à caract�re  scientifique  et  technologique,  dont  le
//                				si�ge est situé Domaine de Voluceau, Rocquencourt, BP 105, 78153 Le  Chesnay
//                				cedex.
//                				
//                				
//                				PREAMBULE
//                				---------
//                				
//                				Ce contrat est  une  licence  de  logiciel  libre  dont  l'objectif  est  de
//                				conf�rer aux utilisateurs la liberté de modification  et  de  redistribution
//                				du logiciel r�gi par cette licence dans le cadre d'un  modèle  de  diffusion
//                				é open source à fond�e sur le droit fran�ais.
//                				
//                				L'exercice de ces libertes est assorti de certains devoirs à la  charge  des
//                				utilisateurs afin de  preserver  ce  statut  au  cours  des  redistributions
//                				ult�rieures.
//                				
//                				L'accessibilité au code source et les droits de copie,  de  modification  et
//                				de redistribution qui en decoulent ont pour  contrepartie  de  n'offrir  aux
//                				utilisateurs qu'une garantie limit�e et de ne faire peser  sur  l'auteur  du
//                				logiciel, le titulaire des droits patrimoniaux et les conc�dants  successifs
//                				qu'une responsabilité restreinte.
//                				
//                				A cet �gard  l'attention  de  l'utilisateur  est  attir�e  sur  les  risques
//                				associ�s  au  chargement,  à  l'utilisation,  à  la  modification  et/ou  au
//                				d�veloppement et à la  reproduction  du  logiciel  par  l'utilisateur  etant
//                				donné sa sp�cificité de logiciel  libre,  qui  peut  le  rendre  complexe  �
//                				manipuler et qui le r�serve donc à des d�veloppeurs  et  des  professionnels
//                				avertis  possedant  des  connaissances   informatiques   approfondies.   Les
//                				utilisateurs sont donc invites à charger et tester l'ad�quation du  Logiciel
//                				é leurs besoins dans des conditions  permettant  d'assurer  la  securité  de
//                				leurs syst�mes et ou de leurs données et, plus  g�n�ralement,  à  l'utiliser
//                				et l'exploiter dans les m�me conditions de securit�. Ce  contrat  peut  être
//                				reproduit et diffusé librement, sous r�serve  de  le  conserver  en  l'etat,
//                				sans ajout ni suppression de clauses.
//                				
//                				Ce contrat est susceptible de s'appliquer à tout logiciel dont le  titulaire
//                				des droits patrimoniaux decide de soumettre l'exploitation aux  dispositions
//                				qu'il  contient.
//
//								...

//		Texte complet de la licence a l'url suivante :
//		http://www.cecill.info/licences/Licence_CeCILL_V1-fr.html

//		Developpement dans le cadre des activites du Consortium Cocktail : http://www.cocktail.org
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)


//
//  Main.java
//  Abricot
//
//  Created by Frederic Rivalland on Mon Oct 23 2006.
//  Copyright (c) 2001 __MyCompanyName__. All rights reserved.
//

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eodistribution.WOJavaClientComponent;

public class Main extends WOComponent {

    public Main(WOContext context) {
        super(context);
    }

    public String javaClientLink() {
        return WOJavaClientComponent.webStartActionURL(context(), "JavaClient");
    }    
}
