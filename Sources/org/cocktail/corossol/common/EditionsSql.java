package org.cocktail.corossol.common;

public enum EditionsSql {

	XLS_ETAT_ACTIF_DETAIL_UB("XLS_Etat_actif_detail_UB.sql"),
	XLS_LISTE_BIENS_ETAT_ACTIF_DETAIL_UB("XLS_Liste_biens_pour_etat_actif_detail_UB.sql"),
	XLS_LISTE_NUMEROS_INVENTAIRE("XLS_Liste_numeros_inventaire.sql");

	private String filename;
	private EditionsSql(String filename) {
		this.filename = filename;
	}
	
	public String filename() {
		return filename;
	}
	
}
