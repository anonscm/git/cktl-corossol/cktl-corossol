package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import org.cocktail.corossol.common.zutil.CalculMontantUtil;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class CorrecteurMontantsOriginesSurPourcentageMax extends ACorrecteurOrigines {

	public void corrigerOrigines(IInventaireComptable inventaireComptable) {
		recalculValeurs(inventaireComptable);
		correctionMontant(inventaireComptable);
	}

	private void correctionMontant(IInventaireComptable inventaireComptable) {
		IInventaireComptableOrig lastOrigine = recupereOrigineACorriger(inventaireComptable);
		BigDecimal sommePourcentage = inventaireComptable.sommePourcentageOriginesFinancement();
		if (sommePourcentage.compareTo(CalculMontantUtil.CENT) == 0) {
			BigDecimal sommeMontant = inventaireComptable.sommeMontantOriginesFinancement();
			BigDecimal diff = inventaireComptable.invcMontantAmortissableAvecOrvs().subtract(sommeMontant);
			lastOrigine.setIcorMontant(lastOrigine.icorMontant().add(diff));
		}
	}

	@Override
	protected NSArray triPourInventaireComptableOrigines() {
		return new NSArray(EOSortOrdering.sortOrderingWithKey(IInventaireComptableOrig.I_ICOR_POURCENTAGE_KEY, EOSortOrdering.CompareAscending));
	}
	
	@Override
	protected IControleurOrigine getControleurOrigine() {
		return new ControleurOrigineViaPourcentage();
	}
}
