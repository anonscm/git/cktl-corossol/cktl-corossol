package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public interface IInventaireComptable {

	String POURCENTAGE = "P";
	String MONTANT = "M";

	BigDecimal invcMontantAmortissable();

	BigDecimal invcMontantAmortissableAvecOrvs();

	BigDecimal sommePourcentageOriginesFinancement();

	BigDecimal sommeMontantOriginesFinancement();

	String methodeDeCalculPourLesRepartitions();

	NSArray inventaireComptableOrigs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch);
}
