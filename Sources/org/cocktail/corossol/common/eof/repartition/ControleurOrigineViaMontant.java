package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import org.cocktail.corossol.common.zutil.CalculMontantUtil;

import com.webobjects.foundation.NSValidation;

public class ControleurOrigineViaMontant extends AControleurOrigine {

	public void checkValidite(IInventaireComptableOrig origine) {
		if (origine == null || origine.inventaireComptable() == null) {
			return;
		}

		if (origine.icorMontant() == null) {
			throw new NSValidation.ValidationException("Vous devez saisir un montant !");
		}

		if (!comprisDansIntervalleInclus(origine.icorMontant(), BigDecimal.ZERO, origine.inventaireComptable().invcMontantAmortissableAvecOrvs())) {
			throw new NSValidation.ValidationException("Le montant doit être compris entre 0 et " + origine.inventaireComptable().invcMontantAmortissableAvecOrvs());
		}
	}

	public void updateInventaireComptableOrigine(IInventaireComptableOrig origine) {
		origine.setIcorPourcentage(CalculMontantUtil.pourcentageDuMontant(origine.inventaireComptable().invcMontantAmortissableAvecOrvs(), origine.icorMontant()));
	}

}
