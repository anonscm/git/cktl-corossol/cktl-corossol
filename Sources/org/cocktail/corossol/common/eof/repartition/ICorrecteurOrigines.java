package org.cocktail.corossol.common.eof.repartition;

public interface ICorrecteurOrigines {

	/**
	 * Corrige les montant ou les pourcentages suivant la méthode de calcul<br>
	 * <ul>
	 * <li>Si on travaille sur les montants, alors on corrige les pourcentages</li>
	 * <li>Inversement si on travaille sur les pourcentages</li>
	 * </ul>
	 * 
	 * @param inventaireComptable
	 *            L'inventaire comptable.
	 */
	void corrigerOrigines(IInventaireComptable inventaireComptable);

}
