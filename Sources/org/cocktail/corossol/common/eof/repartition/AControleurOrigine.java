package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

public abstract class AControleurOrigine implements IControleurOrigine {

	protected boolean comprisDansIntervalleInclus(BigDecimal valeur, BigDecimal borneInf, BigDecimal borneSup) {
		return valeur.compareTo(borneInf) >= 0 && valeur.compareTo(borneSup) <= 0;
	}
}
