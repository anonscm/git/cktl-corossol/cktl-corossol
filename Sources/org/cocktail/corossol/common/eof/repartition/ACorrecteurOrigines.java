package org.cocktail.corossol.common.eof.repartition;

import com.webobjects.foundation.NSArray;

public abstract class ACorrecteurOrigines implements ICorrecteurOrigines {

	protected abstract NSArray triPourInventaireComptableOrigines();

	protected abstract IControleurOrigine getControleurOrigine();

	protected IInventaireComptableOrig recupereOrigineACorriger(IInventaireComptable inventaireComptable) {
		NSArray invComptOrigs = inventaireComptable.inventaireComptableOrigs(null, triPourInventaireComptableOrigines(), false);
		return (IInventaireComptableOrig) invComptOrigs.lastObject();
	}

	/**
	 * Recalcule les montants de chaque origine, à partir des pourcentages
	 * 
	 * @param inventaireComptable
	 * @return La dernière origine
	 */
	protected void recalculValeurs(IInventaireComptable inventaireComptable) {
		NSArray invComptOrigs = inventaireComptable.inventaireComptableOrigs(null, null, false);
		for (Object o : invComptOrigs) {
			IInventaireComptableOrig origine = (IInventaireComptableOrig) o;
			getControleurOrigine().updateInventaireComptableOrigine(origine);
		}
	}
}
