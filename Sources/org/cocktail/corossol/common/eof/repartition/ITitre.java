/**
 * 
 */
package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

/**
 * @author kahlua
 * 
 */
public interface ITitre {
	BigDecimal montantDisponible();
}
