package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import org.cocktail.corossol.common.zutil.CalculMontantUtil;

import com.webobjects.foundation.NSValidation;

public class ControleurOrigineViaPourcentage extends AControleurOrigine {

	public void checkValidite(IInventaireComptableOrig origine) {
		if (origine == null || origine.inventaireComptable() == null) {
			return;
		}

		if (origine.icorPourcentage() == null) {
			throw new NSValidation.ValidationException("Vous devez saisir un pourcentage !");
		}

		if (!comprisDansIntervalleInclus(origine.icorPourcentage(), BigDecimal.ZERO, CalculMontantUtil.CENT)) {
			throw new NSValidation.ValidationException("Le montant doit être compris entre 0 et 100");
		}
	}

	public void updateInventaireComptableOrigine(IInventaireComptableOrig origine) {
		origine.setIcorMontant(CalculMontantUtil.montantDuPourcentage(origine.inventaireComptable().invcMontantAmortissableAvecOrvs(), origine.icorPourcentage()));
	}

}
