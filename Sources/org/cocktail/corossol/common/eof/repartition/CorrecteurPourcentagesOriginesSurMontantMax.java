package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import org.cocktail.corossol.common.zutil.CalculMontantUtil;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class CorrecteurPourcentagesOriginesSurMontantMax extends ACorrecteurOrigines {

	public void corrigerOrigines(IInventaireComptable inventaireComptable) {
		recalculValeurs(inventaireComptable);
		correctionPourcentage(inventaireComptable);
	}

	private void correctionPourcentage(IInventaireComptable inventaireComptable) {
		IInventaireComptableOrig lastOrigine = recupereOrigineACorriger(inventaireComptable);
		BigDecimal sommeMontant = inventaireComptable.sommeMontantOriginesFinancement();
		if (sommeMontant.compareTo(inventaireComptable.invcMontantAmortissableAvecOrvs()) == 0) {
			BigDecimal sommePourcentage = inventaireComptable.sommePourcentageOriginesFinancement();
			BigDecimal diff = CalculMontantUtil.CENT.subtract(sommePourcentage);
			lastOrigine.setIcorPourcentage(lastOrigine.icorPourcentage().add(diff));
		}
	}

	@Override
	protected NSArray triPourInventaireComptableOrigines() {
		return new NSArray(EOSortOrdering.sortOrderingWithKey(IInventaireComptableOrig.I_ICOR_MONTANT_KEY, EOSortOrdering.CompareAscending));
	}
	
	/**
	 * @return
	 */
	@Override
	protected IControleurOrigine getControleurOrigine() {
		return new ControleurOrigineViaMontant();
	}
}
