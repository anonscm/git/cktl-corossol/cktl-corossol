/**
 * 
 */
package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import com.webobjects.foundation.NSValidation;

/**
 *
 *
 */
public class InventaireComptableOrigHelper {

	public static void updateInventaireComptableOrig(IInventaireComptableOrig origine) {
		IControleurOrigine controleurOrigine = createControleurOrigine(origine);
		controleurOrigine.checkValidite(origine);
		controleurOrigine.updateInventaireComptableOrigine(origine);
	}

	protected static IControleurOrigine createControleurOrigine(IInventaireComptableOrig origine) {
		if (origine.inventaireComptable() == null || origine.inventaireComptable().methodeDeCalculPourLesRepartitions() == null) {
			throw new NSValidation.ValidationException("Impossible de déterminer la méthode de calcul pour les répartitions !");
		}

		IControleurOrigine controleurOrigine = null;
		if (IInventaireComptable.MONTANT.equals(origine.inventaireComptable().methodeDeCalculPourLesRepartitions())) {
			controleurOrigine = new ControleurOrigineViaMontant();
		} else if (IInventaireComptable.MONTANT.equals(origine.inventaireComptable().methodeDeCalculPourLesRepartitions())) {
			controleurOrigine = new ControleurOrigineViaPourcentage();
		}

		return controleurOrigine;
	}

	/**
	 * @param currentInvComptableOrig
	 * @param titreMontantDispo
	 * @throws Exception
	 */
	public static void checkTitreMontantDisponible(IInventaireComptableOrig ancienneOrigine, IInventaireComptableOrig origine, ITitre titre) throws Exception {
		BigDecimal titreMontantDispo = null;
		if (titre != null) {
			// Montant du titre
			titreMontantDispo = titre.montantDisponible();
			if (ancienneOrigine != null && ancienneOrigine.icorMontant() != null && ancienneOrigine.titre() != null && ancienneOrigine.titre().equals(titre)) {
				// On ajoute le montant de origine pour avoir le vrai montant dispo (modification du
				// currentInventaireComptableOrig)
				titreMontantDispo = titreMontantDispo.add(ancienneOrigine.icorMontant());
			}
		}

		// Contrôle du montant par rapport au montant du titre
		if (origine.icorMontant() != null && titreMontantDispo != null && titreMontantDispo.compareTo(origine.icorMontant()) < 0) {
			throw new Exception("Le montant disponible du titre (" + titreMontantDispo + ") est inférieur au montant financé  (" + origine.icorMontant()
					+ ") !");
		}
	}
}
