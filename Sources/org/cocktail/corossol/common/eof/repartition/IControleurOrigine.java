package org.cocktail.corossol.common.eof.repartition;

public interface IControleurOrigine {

	void checkValidite(IInventaireComptableOrig origine);

	void updateInventaireComptableOrigine(IInventaireComptableOrig origine);

}
