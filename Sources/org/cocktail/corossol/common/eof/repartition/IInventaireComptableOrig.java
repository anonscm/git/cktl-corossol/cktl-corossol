package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public interface IInventaireComptableOrig {

	public static String I_ICOR_MONTANT_KEY = "icorMontant";
	public static final String I_ICOR_POURCENTAGE_KEY = "icorPourcentage";

	BigDecimal icorMontant();

	void setIcorMontant(BigDecimal value);

	BigDecimal icorPourcentage();

	void setIcorPourcentage(BigDecimal value);

	IInventaireComptable inventaireComptable();

	ITitre titre();
}
