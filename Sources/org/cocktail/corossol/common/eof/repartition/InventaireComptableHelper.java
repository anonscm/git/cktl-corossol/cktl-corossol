package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import org.cocktail.corossol.common.zutil.CalculMontantUtil;

import com.webobjects.foundation.NSValidation;

public class InventaireComptableHelper {

	public static void corrigerOrigines(IInventaireComptable inventaireComptable) {
		ICorrecteurOrigines correcteurOrigines = createCorrigerOrigines(inventaireComptable);
		correcteurOrigines.corrigerOrigines(inventaireComptable);
	}

	protected static ICorrecteurOrigines createCorrigerOrigines(IInventaireComptable inventaireComptable) {
		if (inventaireComptable == null || inventaireComptable.methodeDeCalculPourLesRepartitions() == null) {
			throw new NSValidation.ValidationException("Impossible de déterminer la méthode de calcul pour les financements !");
		}

		ICorrecteurOrigines correcteurOrigines = null;
		if (isCalculViaMontants(inventaireComptable)) {
			correcteurOrigines = new CorrecteurPourcentagesOriginesSurMontantMax();
		} else if (isCalculViaPourcentages(inventaireComptable)) {
			correcteurOrigines = new CorrecteurMontantsOriginesSurPourcentageMax();
		}

		return correcteurOrigines;
	}

	public static void checkSomme(IInventaireComptable inventaireComptable) {
		if (isCalculViaMontants(inventaireComptable)) {
			checkSommeMontants(inventaireComptable);
		} else if (isCalculViaPourcentages(inventaireComptable)) {
			checkSommePourcentages(inventaireComptable);
		}
	}

	protected static void checkSommeMontants(IInventaireComptable inventaireComptable) {
		BigDecimal sommeMontants = inventaireComptable.sommeMontantOriginesFinancement();
		if (sommeMontants.compareTo(inventaireComptable.invcMontantAmortissableAvecOrvs()) != 0) {
			throw new NSValidation.ValidationException("Le total des financements doit être égal au montant amortissable (" + sommeMontants + " ≠ "
					+ inventaireComptable.invcMontantAmortissableAvecOrvs() + ") !");
		}
	}

	/**
	 * @param inventaireComptable
	 */
	protected static void checkSommePourcentages(IInventaireComptable inventaireComptable) {
		BigDecimal sommePourcentages = inventaireComptable.sommePourcentageOriginesFinancement();
		if (sommePourcentages.compareTo(CalculMontantUtil.CENT) != 0) {
			throw new NSValidation.ValidationException("Le total des financements doit être égal à 100%  (" + sommePourcentages + "%) !");
		}
	}

	/**
	 * @param inventaireComptable
	 * @return
	 */
	private static boolean isCalculViaPourcentages(IInventaireComptable inventaireComptable) {
		return IInventaireComptable.POURCENTAGE.equals(inventaireComptable.methodeDeCalculPourLesRepartitions());
	}

	/**
	 * @param inventaireComptable
	 * @return
	 */
	private static boolean isCalculViaMontants(IInventaireComptable inventaireComptable) {
		return IInventaireComptable.MONTANT.equals(inventaireComptable.methodeDeCalculPourLesRepartitions());
	}

}
