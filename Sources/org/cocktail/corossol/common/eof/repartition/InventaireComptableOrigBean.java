/**
 * 
 */
package org.cocktail.corossol.common.eof.repartition;

import java.math.BigDecimal;

import com.webobjects.foundation.NSValidation;

/**
 * @author kahlua
 * 
 */
public class InventaireComptableOrigBean implements IInventaireComptableOrig {

	private IInventaireComptable inventaireComptable;
	private BigDecimal icorMontant;
	private BigDecimal icorPourcentage;
	private ITitre titre;

	public InventaireComptableOrigBean(IInventaireComptable inventaireComptable, String montant, String pourcentage) {
		this.icorMontant = bigD(montant);
		this.icorPourcentage = bigD(pourcentage);
		this.inventaireComptable = inventaireComptable;

		if (this.icorMontant == null && this.icorPourcentage == null) {
			throw new NSValidation.ValidationException("Vous devez saisir un montant ou un pourcentage !");
		}
	}

	private BigDecimal bigD(String val) {
		if (val == null || val.length() == 0) {
			return null;
		}
		try {
			return BigDecimal.valueOf(Double.valueOf(val)).setScale(2, BigDecimal.ROUND_HALF_UP);
		} catch (NumberFormatException nfe) {
			throw new NumberFormatException("Impossible de convertir la chaîne \"" + val + "\" en nombre.");
		}
	}

	public void setIcorPourcentage(BigDecimal value) {
		icorPourcentage = value;
	}

	public void setIcorMontant(BigDecimal value) {
		icorMontant = value;
	}

	public BigDecimal icorPourcentage() {
		return icorPourcentage;
	}

	public BigDecimal icorMontant() {
		return icorMontant;
	}

	public IInventaireComptable inventaireComptable() {
		return inventaireComptable;
	}

	public ITitre titre() {
		return titre;
	}

	public void setTitre(ITitre titre) {
		this.titre = titre;
	}
}
