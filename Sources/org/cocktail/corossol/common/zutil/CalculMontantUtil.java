package org.cocktail.corossol.common.zutil;

import java.math.BigDecimal;

import org.cocktail.corossol.common.eof.repartition.IInventaireComptable;
import org.cocktail.corossol.common.eof.repartition.IInventaireComptableOrig;
import org.cocktail.corossol.common.eof.repartition.InventaireComptableOrigBean;
import org.cocktail.corossol.common.eof.repartition.InventaireComptableOrigHelper;

import com.webobjects.foundation.NSValidation;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class CalculMontantUtil {
	public static final BigDecimal CENT = BigDecimal.valueOf(100d);

	private static final int ROUND = BigDecimal.ROUND_HALF_UP;
	private static final int SCALE = 2;

	/**
	 * @param montantTotal
	 * @param montant
	 * @return Le pourcentage du montant, arrondi à 2 chiffres après la virgule
	 */
	public static BigDecimal pourcentageDuMontant(BigDecimal montantTotal, BigDecimal montant) {
		if (montant == null) {
			throw new NSValidation.ValidationException("Le montant saisi est vide.");
		}
		if (montantTotal == null || montantTotal.signum() <= 0) {
			throw new NSValidation.ValidationException("Le montant total est vide ou égal à 0.");
		}

		if (montant.signum() <= 0) {
			return BigDecimal.ZERO;
		} else {
			return montant.multiply(CENT).divide(montantTotal, SCALE, ROUND);
		}
	}

	/**
	 * Retourne le montant associé au pourcentage du bien
	 * 
	 * @return
	 */
	public static BigDecimal montantDuPourcentage(BigDecimal montantTotal, BigDecimal pourcentage) {
		if (pourcentage == null) {
			throw new NSValidation.ValidationException("Le pourcentage saisi est vide.");
		}
		if (pourcentage.compareTo(CENT) > 0) {
			throw new NSValidation.ValidationException("Le pourcentage saisi (" + pourcentage + ") est supérieur à 100.");
		}

		if (montantTotal == null || montantTotal.signum() <= 0) {
			throw new NSValidation.ValidationException("Le montant total est vide ou égal à 0.");
		}

		if (pourcentage.signum() <= 0) {
			return BigDecimal.ZERO;
		} else {
			return montantTotal.multiply(pourcentage.divide(CENT)).setScale(SCALE, ROUND);
		}
	}

	/**
	 * @param chaineMontant
	 * @param chainePourcentage
	 * @param currentInvComptableOrig
	 * @param inventaireComptable
	 * @param titreMontantDispo
	 * @throws Exception
	 */
	public static IInventaireComptableOrig calculRepartition(String chaineMontant, String chainePourcentage, IInventaireComptable inventaireComptable)
			throws Exception {
		IInventaireComptableOrig origine = new InventaireComptableOrigBean(inventaireComptable, chaineMontant, chainePourcentage);
		InventaireComptableOrigHelper.updateInventaireComptableOrig(origine);
		return origine;
	}

}
