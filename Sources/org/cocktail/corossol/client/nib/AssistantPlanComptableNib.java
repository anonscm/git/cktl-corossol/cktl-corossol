package org.cocktail.corossol.client.nib;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JLabelCocktail;
import org.cocktail.application.palette.JTextFieldCocktail;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class AssistantPlanComptableNib extends org.cocktail.application.palette.JPanelCocktail {
	private JPanel jPanelResultat;
	private JButtonCocktail jButtonCocktailFiltrer;
	private JLabelCocktail jLabelCocktail1;
	private JTextFieldCocktail jTextFieldCocktailCompte;
	private JButtonCocktail jButtonCocktailSelectionner;
	private JButtonCocktail jButtonCocktailAnnuler;

	/**
	* Auto-generated main method to display this 
	* JPanel inside a new JFrame.
	*/
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new AssistantPlanComptableNib());
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	public AssistantPlanComptableNib() {
		super();
		initGUI();
	}
	
	private void initGUI() {
		try {
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {0.0, 0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.0};
			thisLayout.rowHeights = new int[] {21, 27, 7, 7, 7, 7, 7, 7, 72, 7, 7};
			thisLayout.columnWeights = new double[] {0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.1, 0.0};
			thisLayout.columnWidths = new int[] {7, 7, 7, 7, 7, 7, 7, 7, 85, 7, 7, 7};
			this.setLayout(thisLayout);
			setPreferredSize(new Dimension(400, 380));
			{
				jPanelResultat = new JPanel();
				BoxLayout jPanelResultatLayout = new BoxLayout(
					jPanelResultat,
					javax.swing.BoxLayout.X_AXIS);
				jPanelResultat.setLayout(jPanelResultatLayout);
				this.add(getJPanelResultat(), new GridBagConstraints(1, 2, 10, 7, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));
			}
			{
				jButtonCocktailAnnuler = new JButtonCocktail();
				this.add(getJButtonCocktailAnnuler(), new GridBagConstraints(7, 9, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailAnnuler.setText("Annuler");
			}
			{
				jButtonCocktailSelectionner = new JButtonCocktail();
				this.add(getJButtonCocktailSelectionner(), new GridBagConstraints(9, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailSelectionner.setText("Selectionner");
			}
			{
				jTextFieldCocktailCompte = new JTextFieldCocktail();
				this.add(getJTextFieldCocktailCompte(), new GridBagConstraints(5, 1, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
			}
			{
				jLabelCocktail1 = new JLabelCocktail();
				this.add(jLabelCocktail1, new GridBagConstraints(1, 1, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jLabelCocktail1.setText("Compte ?");
			}
			{
				jButtonCocktailFiltrer = new JButtonCocktail();
				this.add(getJButtonCocktailFiltrer(), new GridBagConstraints(9, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailFiltrer.setText("Filtrer");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JPanel getJPanelResultat() {
		return jPanelResultat;
	}
	
	public JButtonCocktail getJButtonCocktailAnnuler() {
		return jButtonCocktailAnnuler;
	}
	
	public JButtonCocktail getJButtonCocktailSelectionner() {
		return jButtonCocktailSelectionner;
	}
	
	public JTextFieldCocktail getJTextFieldCocktailCompte() {
		return jTextFieldCocktailCompte;
	}
	
	public JButtonCocktail getJButtonCocktailFiltrer() {
		return jButtonCocktailFiltrer;
	}

}
