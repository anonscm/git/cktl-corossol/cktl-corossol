package org.cocktail.corossol.client.nib;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie;

import com.webobjects.foundation.NSArray;


public class SortieComptableNib extends JFrame {

	private JButton btEnregistrer, btCalculer;

	private JTextPane textInventaire;
	private JTextField tfDateSortie, tfMotifSortie, tfValeurNetteComptable, tfValeurCession, tfMontantAcquisitionSortie;

	private JComboBox popupTypeSortie;

	public SortieComptableNib() {
		super("Sortie d'inventaire");
		setSize(600, 600);
		centerWindow();
		initGui();
	}

	private void initGui() {
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(panelSaisieSortie());
	}

	public JComboBox popupTypeSortie() {
		return popupTypeSortie;
	}

	public void remplirPopupTypeSortie(NSArray array) {
		popupTypeSortie.removeAllItems();
		if (array==null)
			return;
		for (int i=0; i<array.count(); i++)
			popupTypeSortie.addItem(array.objectAtIndex(i));
	}

	public void afficherInventaire(EOInventaireComptable inventaire, EOInventaireComptableSortie inventaireSortie) {
		textInventaire.setText("");	
		tfDateSortie.setText("");
		tfMotifSortie.setText("");
		tfValeurNetteComptable.setText("");
		tfValeurCession.setText("");
		tfMontantAcquisitionSortie.setText("");

		if (inventaire==null)
			return;

		textInventaire.setText(inventaire.resume());

		if (inventaireSortie!=null) {
			if (inventaireSortie.invcsDate()!=null)
				tfDateSortie.setText(DateCtrl.dateToString(inventaireSortie.invcsDate(), "%d/%m/%Y"));
			if (inventaireSortie.invcsMotif()!=null)
				tfMotifSortie.setText(inventaireSortie.invcsMotif());
			if (inventaireSortie.invcsVnc()!=null)
				tfValeurNetteComptable.setText(inventaireSortie.invcsVnc().toString());
			if (inventaireSortie.invcsValeurCession()!=null)
				tfValeurCession.setText(inventaireSortie.invcsValeurCession().toString());
			if (inventaireSortie.invcsMontantAcquisitionSortie()!=null)
				tfMontantAcquisitionSortie.setText(inventaireSortie.invcsMontantAcquisitionSortie().toString());
		}
	}

	public JButton btEnregistrer() {
		return btEnregistrer;
	}
	public JButton btCalculer() {
		return btCalculer;
	}
	public JTextField tfDateSortie() {
		return tfDateSortie;
	}
	public JTextField tfMotifSortie() {
		return tfMotifSortie;
	}
	public JTextField tfValeurNetteComptable() {
		return tfValeurNetteComptable;
	}
	public JTextField tfValeurCession() {
		return tfValeurCession;
	}
	public JTextField tfMontantAcquisitionSortie() {
		return tfMontantAcquisitionSortie;
	}
	
	private void centerWindow() {
		int screenWidth = (int)getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int)getGraphicsConfiguration().getBounds().getHeight();
		setLocation((screenWidth/2)-((int)getSize().getWidth()/2), ((screenHeight/2)-((int)getSize().getHeight()/2)));
	}

	private JPanel panelSaisieSortie() {
		JPanel panel=new JPanel(new BorderLayout());

		panel.add(sousPanelSaisieSortieSaisie(), BorderLayout.CENTER);
		panel.add(sousPanelSaisieSortieBoutons(), BorderLayout.SOUTH);

		return panel;
	}

	private JPanel sousPanelSaisieSortieSaisie() {
		JPanel panel=new JPanel(new BorderLayout());

		textInventaire=new JTextPane();
		textInventaire.setPreferredSize(new Dimension(200,100));
		textInventaire.setEditable(false);
		textInventaire.setBorder(BorderFactory.createLineBorder(Color.black));

		JScrollPane scrollpane=new JScrollPane(textInventaire);
		scrollpane.setWheelScrollingEnabled(true);

		panel.add(scrollpane, BorderLayout.NORTH);

		JPanel panelSaisie=new JPanel(new GridLayout(0,1));
		panelSaisie.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

		//MISE EN PLACE PANEL
		JLabel label=new JLabel("Montant acq sortie :");
		label.setPreferredSize(new Dimension(80,20));
		tfMontantAcquisitionSortie=new JTextField();

		JPanel panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfMontantAcquisitionSortie, BorderLayout.CENTER);

		panelSaisie.add(panelElement);

		label=new JLabel("Date sortie :");
		label.setPreferredSize(new Dimension(80,20));
		tfDateSortie=new JTextField();

		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfDateSortie, BorderLayout.CENTER);

		panelSaisie.add(panelElement);

		popupTypeSortie = new JComboBox();
		popupTypeSortie.setFocusable(false);

		panelSaisie.add(popupTypeSortie);

		label=new JLabel("Motif :");
		label.setPreferredSize(new Dimension(80,20));
		tfMotifSortie=new JTextField();

		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfMotifSortie, BorderLayout.CENTER);

		panelSaisie.add(panelElement);

		label=new JLabel("V.N.C :");
		label.setPreferredSize(new Dimension(80,20));
		tfValeurNetteComptable=new JTextField();
		btCalculer=new JButton("Calculer");
		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfValeurNetteComptable, BorderLayout.CENTER);
		panelElement.add(btCalculer, BorderLayout.EAST);

		panelSaisie.add(panelElement);

		label=new JLabel("Valeur cession :");
		label.setPreferredSize(new Dimension(80,20));
		tfValeurCession=new JTextField();

		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfValeurCession, BorderLayout.CENTER);

		panelSaisie.add(panelElement);

		JPanel panelChamps=new JPanel(new BorderLayout());
		panelChamps.add(panelSaisie, BorderLayout.NORTH);
		panelChamps.add(new JPanel(), BorderLayout.CENTER);

		panel.add(panelChamps, BorderLayout.CENTER);

		return panel;
	}

	private JPanel sousPanelSaisieSortieBoutons() {
		JPanel panel=new JPanel(new BorderLayout());

		JPanel panelBoutons=new JPanel(new GridLayout(1,0));

		btEnregistrer=new JButton("Enregistrer");
		panelBoutons.add(btEnregistrer);

		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(panelBoutons, BorderLayout.EAST);

		return panel;
	}
}