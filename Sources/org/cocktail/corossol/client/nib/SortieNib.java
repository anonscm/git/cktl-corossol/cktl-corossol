package org.cocktail.corossol.client.nib;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireSortie;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOVCommande;
import org.cocktail.corossol.client.zutil.wo.table.TableSorter;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTable;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class SortieNib extends JFrame {

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private final Color tableBackgroundColor=new Color(230, 230, 230);
	private final Color tableSelectionBackgroundColor=new Color(127,127,127);

	private CardLayout cardLayout;
	private JPanel panelCard;
	private String panelConsultation="panelConsultation", panelSaisie="panelSaisie";

	private JButton btRechercher, btPrecedent, btSuivant, btEnregistrer, btSupprimer, btImprimer, btCalculer;
	private JTextField tfRechercheCommande, tfRechercheNumeroInventaire;
	private JCheckBox cbRechercheSortie;

	private JTextPane textInventaire;
	private JTextField tfDateSortie, tfMotifSortie, tfValeurNetteComptable, tfValeurCession;
	
	private JComboBox popupTypeSortie;

	public SortieNib() {
		super("Sortie d'inventaire");
		setSize(600, 600);
		centerWindow();
		initGui();
	}

	private void initGui() {
		cardLayout=new CardLayout();
		getContentPane().setLayout(new BorderLayout());

		panelCard=new JPanel(cardLayout);
		panelCard.add(panelRecherche(), panelConsultation);
		panelCard.add(panelSaisieSortie(), panelSaisie);
		getContentPane().add(panelCard);
	}

	public JComboBox popupTypeSortie() {
		return popupTypeSortie;
	}

	public void remplirPopupTypeSortie(NSArray array) {
		popupTypeSortie.removeAllItems();
		if (array==null)
			return;
		for (int i=0; i<array.count(); i++)
			popupTypeSortie.addItem(array.objectAtIndex(i));
	}

	public EOInventaire inventaireSelectionne() {
		try {
			return (EOInventaire)myEOTable.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}

	public void setInventaire(NSArray data) {
		eod.setObjectArray(data);
		eod.updateDisplayedObjects();
		refreshInventaire();
	}

	public void afficherInventaire(EOInventaire inventaire, EOInventaireSortie inventaireSortie) {
		textInventaire.setText("");	
		tfDateSortie.setText("");
		tfMotifSortie.setText("");
		tfValeurNetteComptable.setText("");
		tfValeurCession.setText("");
		
		if (inventaire==null)
			return;
		
		textInventaire.setText(inventaire.resume());
		
		if (inventaireSortie!=null) {
			if (inventaireSortie.invsDate()!=null)
				tfDateSortie.setText(DateCtrl.dateToString(inventaireSortie.invsDate(), "%d/%m/%Y"));
			if (inventaireSortie.invsMotif()!=null)
				tfMotifSortie.setText(inventaireSortie.invsMotif());
			if (inventaireSortie.invsVnc()!=null)
				tfValeurNetteComptable.setText(inventaireSortie.invsVnc().toString());
			if (inventaireSortie.invsValeurCession()!=null)
				tfValeurCession.setText(inventaireSortie.invsValeurCession().toString());
		}
	}
	
	public void refreshInventaire() {
		myEOTable.updateData();		
	}

	public JButton btSuivant() {
		return btSuivant;
	}
	public JButton btPrecedent() {
		return btPrecedent;
	}
	public JButton btRechercher() {
		return btRechercher;
	}
	public JButton btSupprimer() {
		return btSupprimer;
	}
	public JButton btImprimer() {
		return btImprimer;
	}
	public JButton btEnregistrer() {
		return btEnregistrer;
	}
	public JButton btCalculer() {
		return btCalculer;
	}
	public JTextField tfDateSortie() {
		return tfDateSortie;
	}
	public JTextField tfMotifSortie() {
		return tfMotifSortie;
	}
	public JTextField tfValeurNetteComptable() {
		return tfValeurNetteComptable;
	}
	public JTextField tfValeurCession() {
		return tfValeurCession;
	}
	public JTextField tfRechercheCommande() {
		return tfRechercheCommande;
	}
	public JTextField tfRechercheNumeroInventaire() {
		return tfRechercheNumeroInventaire;
	}
	
	public JCheckBox cbRechercheSortie() {
		return cbRechercheSortie;
	}
	public ZEOTable table() {
		return myEOTable;
	}
	
	private void centerWindow() {
		int screenWidth = (int)getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int)getGraphicsConfiguration().getBounds().getHeight();
		setLocation((screenWidth/2)-((int)getSize().getWidth()/2), ((screenHeight/2)-((int)getSize().getHeight()/2)));
	}

	private JPanel panelRecherche() {
		JPanel panel=new JPanel(new BorderLayout());

		panel.add(sousPanelRechercheCriteres(), BorderLayout.NORTH);
		panel.add(sousPanelRechercheResultats(), BorderLayout.CENTER);
		panel.add(sousPanelRechercheBoutons(), BorderLayout.SOUTH);

		return panel;
	}

	private JPanel sousPanelRechercheCriteres() {
		JPanel  panelCritereCommande=new JPanel(new BorderLayout()), panelCritereNumeroInventaire=new JPanel(new BorderLayout());

		panelCritereCommande.add(new JLabel("Commande :"),BorderLayout.WEST);
		tfRechercheCommande=new JTextField();
		tfRechercheCommande.setPreferredSize(new Dimension(50,10));
		panelCritereCommande.add(tfRechercheCommande,BorderLayout.CENTER);
		panelCritereNumeroInventaire.add(new JLabel("Numero inventaire :"),BorderLayout.WEST);
		tfRechercheNumeroInventaire=new JTextField();
		panelCritereNumeroInventaire.add(tfRechercheNumeroInventaire,BorderLayout.CENTER);
		
		cbRechercheSortie=new JCheckBox("Sorties");
		
		JPanel panel=new JPanel(new BorderLayout());
		btRechercher=new JButton("Rechercher");
		panel.add(panelCritereCommande, BorderLayout.WEST);
		panel.add(panelCritereNumeroInventaire, BorderLayout.CENTER);
		
		
		JPanel panelplus=new JPanel(new BorderLayout());
		panelplus.add(cbRechercheSortie, BorderLayout.WEST);
		panelplus.add(btRechercher, BorderLayout.CENTER);
		
		panel.add(panelplus, BorderLayout.EAST);
		
		tfRechercheCommande.addKeyListener( new KeyListener(){
			public void keyPressed(KeyEvent e) {if (e.getKeyCode()== KeyEvent.VK_ENTER)	btRechercher.doClick();}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){} });
		tfRechercheNumeroInventaire.addKeyListener( new KeyListener(){
			public void keyPressed(KeyEvent e) {if (e.getKeyCode()== KeyEvent.VK_ENTER)	btRechercher.doClick();}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){} });

		return panel;
	}

	private JPanel sousPanelRechercheResultats() {
		Vector myCols = new Vector();
		eod=new EODisplayGroup();

		ZEOTableModelColumn colNumeroInventaire = new ZEOTableModelColumn(eod, EOInventaire.INVENTAIRE_COMPTABLE_KEY+"."+
				EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY, "Num Inventaire", 100);
		colNumeroInventaire.setAlignment(SwingConstants.LEFT);
		myCols.add(colNumeroInventaire);

		ZEOTableModelColumn colCommande= new ZEOTableModelColumn(eod, EOInventaire.LIVRAISON_DETAIL_KEY+"."+EOLivraisonDetail.LIVRAISON_KEY+"."+
				EOLivraison.COMMANDE_KEY+"."+EOVCommande.COMM_NUMERO_KEY, "Commande", 10);
		colCommande.setAlignment(SwingConstants.LEFT);
		myCols.add(colCommande);

		ZEOTableModelColumn colLibelle= new ZEOTableModelColumn(eod, EOInventaire.LIVRAISON_DETAIL_KEY+"."+EOLivraisonDetail.LID_LIBELLE_KEY, "Libelle", 150);
		colLibelle.setAlignment(SwingConstants.LEFT);
		myCols.add(colLibelle);

		ZEOTableModelColumn colNumeroSerie= new ZEOTableModelColumn(eod, EOInventaire.INV_SERIE_KEY, "Num Serie", 100);
		colNumeroSerie.setAlignment(SwingConstants.LEFT);
		myCols.add(colNumeroSerie);
		
		ZEOTableModelColumn colMontant= new ZEOTableModelColumn(eod, EOInventaire.INV_MONTANT_ACQUISITION_KEY, "Montant Acq.", 50);
		colMontant.setAlignment(SwingConstants.RIGHT);
		myCols.add(colMontant);

		ZEOTableModelColumn colSortie= new ZEOTableModelColumn(eod, EOInventaire.STRING_IS_SORTIE, "Sortie", 10);
		colSortie.setAlignment(SwingConstants.CENTER);
		myCols.add(colSortie);
	
		
		TableSorter myTableSorter=new TableSorter(new ZEOTableModel(eod, myCols));
		myEOTable=new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(tableBackgroundColor);
		myEOTable.setSelectionBackground(tableSelectionBackgroundColor);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JPanel panel=new JPanel(new BorderLayout());
		panel.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
		return panel;
	}

	private JPanel sousPanelRechercheBoutons() {
		JPanel panel=new JPanel(new BorderLayout());

		btSuivant=new JButton("Suivant");

		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(btSuivant, BorderLayout.EAST);

		return panel;
	}

	private JPanel panelSaisieSortie() {
		JPanel panel=new JPanel(new BorderLayout());

		panel.add(sousPanelSaisieSortieSaisie(), BorderLayout.CENTER);
		panel.add(sousPanelSaisieSortieBoutons(), BorderLayout.SOUTH);

		return panel;
	}

	private JPanel sousPanelSaisieSortieSaisie() {
		JPanel panel=new JPanel(new BorderLayout());
		
		textInventaire=new JTextPane();
		textInventaire.setPreferredSize(new Dimension(200,100));
		textInventaire.setEditable(false);
		textInventaire.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JScrollPane scrollpane=new JScrollPane(textInventaire);
        scrollpane.setWheelScrollingEnabled(true);
        
		panel.add(scrollpane, BorderLayout.NORTH);
		
		JPanel panelSaisie=new JPanel(new GridLayout(0,1));
		panelSaisie.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

		//MISE EN PLACE PANEL
		JLabel label=new JLabel("Date sortie :");
		label.setPreferredSize(new Dimension(80,20));
		tfDateSortie=new JTextField();

		JPanel panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfDateSortie, BorderLayout.CENTER);

		panelSaisie.add(panelElement);

		popupTypeSortie = new JComboBox();
		popupTypeSortie.setFocusable(false);

		panelSaisie.add(popupTypeSortie);

		label=new JLabel("Motif :");
		label.setPreferredSize(new Dimension(80,20));
		tfMotifSortie=new JTextField();

		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfMotifSortie, BorderLayout.CENTER);

		panelSaisie.add(panelElement);

		label=new JLabel("V.N.C :");
		label.setPreferredSize(new Dimension(80,20));
		tfValeurNetteComptable=new JTextField();
		btCalculer=new JButton("Calculer");
		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfValeurNetteComptable, BorderLayout.CENTER);
		panelElement.add(btCalculer, BorderLayout.EAST);

		panelSaisie.add(panelElement);

		label=new JLabel("Valeur cession :");
		label.setPreferredSize(new Dimension(80,20));
		tfValeurCession=new JTextField();

		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfValeurCession, BorderLayout.CENTER);

		panelSaisie.add(panelElement);

		JPanel panelChamps=new JPanel(new BorderLayout());
		panelChamps.add(panelSaisie, BorderLayout.NORTH);
		panelChamps.add(new JPanel(), BorderLayout.CENTER);
		
		panel.add(panelChamps, BorderLayout.CENTER);

		return panel;
	}

	private JPanel sousPanelSaisieSortieBoutons() {
		JPanel panel=new JPanel(new BorderLayout());

		btPrecedent=new JButton("Précédent");
		
		JPanel panelBoutons=new JPanel(new GridLayout(1,0));

		btEnregistrer=new JButton("Enregistrer");
		btSupprimer=new JButton("Supprimer");
		btImprimer=new JButton("Imprimer");
		panelBoutons.add(btSupprimer);
		panelBoutons.add(btEnregistrer);
		panelBoutons.add(btImprimer());
		
		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(btPrecedent, BorderLayout.WEST);
		panel.add(panelBoutons, BorderLayout.EAST);

		return panel;
	}

	public void panneauSuivant() { cardLayout.show(panelCard, panelSaisie); }
	public void panneauPrecedent() { cardLayout.show(panelCard, panelConsultation); }

}