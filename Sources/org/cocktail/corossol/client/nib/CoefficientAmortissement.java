package org.cocktail.corossol.client.nib;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.DateFormat;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.corossol.client.eof.metier.EODegressif;
import org.cocktail.corossol.client.eof.metier.EODegressifCoef;
import org.cocktail.corossol.client.zutil.wo.table.TableSorter;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTable;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class CoefficientAmortissement extends JFrame {

	private EODisplayGroup eodDegressif;
	private ZEOTable myEOTableDegressif;

	private EODisplayGroup eodDegressifCoef;
	private ZEOTable myEOTableDegressifCoef;
//	private ZEOTableModel myTableModelDegressifCoef;
//	private TableSorter myTableSorterDegressifCoef;

	private EODisplayGroup eodModifDegressifCoef;
	private ZEOTable myEOTableModifDegressifCoef;
//	private ZEOTableModel myTableModifModelDegressifCoef;
//	private TableSorter myTableSorterModifDegressifCoef;

	private final Color tableBackgroundColor=new Color(230, 230, 230);
	private final Color tableSelectionBackgroundColor=new Color(127,127,127);

	private CardLayout cardLayout;
	private JPanel panelCard;
	private String panelConsultation="panelConsultation", panelSaisie="panelSaisie";

	private JButton btAnnuler, btModifier, btNouveau, btEnregistrer, btSupprimer;
	private JButtonCocktail btAjouterCoef, btModifierCoef, btSupprimerCoef, btAnnulerCoef, btValiderCoef;
	private JTextField tfDegressifDateDebut, tfDegressifDateFin;
	private JTextField tfCoefDureeMin, tfCoefDureeMax, tfCoefficient;
	private JTextPane textInventaire;

	private CardLayout cardLayoutCoef;
	private JPanel panelCardCoef;
	private String panelConsultationCoef="panelConsultationCoef", panelSaisieCoef="panelSaisieCoef";

	public CoefficientAmortissement() {
		super("Coefficient d'amortissement degressif");
		setSize(600, 600);
		centerWindow();
		initGui();
	}

	private void initGui() {
		cardLayout=new CardLayout();
		getContentPane().setLayout(new BorderLayout());

		panelCard=new JPanel(cardLayout);
		panelCard.add(panelConsultation(), panelConsultation);
		panelCard.add(panelSaisie(), panelSaisie);
		getContentPane().add(panelCard);
	}

	public JTextField getTfCoefDureeMin() {
		return tfCoefDureeMin;
	}
	public JTextField getTfCoefDureeMax() {
		return tfCoefDureeMax;
	}
	public JTextField getTfCoefficient() {
		return tfCoefficient;
	}
	public JTextField getTfDegressifDateDebut() {
		return tfDegressifDateDebut;
	}
	public JTextField getTfDegressifDateFin() {
		return tfDegressifDateFin;
	}
	
	public void afficherDegressif(EODegressif degressif) {
		tfDegressifDateDebut.setText("");
		tfDegressifDateFin.setText("");
		setModifDegressifCoef(new NSArray());
		
		if (degressif==null)
			return;

		if (degressif.dgrfDateDebut()!=null)
			tfDegressifDateDebut.setText(DateCtrl.dateToString(degressif.dgrfDateDebut(), "%d/%m/%Y"));
		if (degressif.dgrfDateFin()!=null)
			tfDegressifDateFin.setText(DateCtrl.dateToString(degressif.dgrfDateFin(), "%d/%m/%Y"));
		setModifDegressifCoef(degressif.degressifCoefs());
	}

	public void afficherDegressifCoef(EODegressifCoef degressifCoef) {
		tfCoefDureeMin.setText("");
		tfCoefDureeMax.setText("");
		tfCoefficient.setText("");
		
		if (degressifCoef==null)
			return;

		if (degressifCoef.dgcoDureeMin()!=null)
			tfCoefDureeMin.setText(""+degressifCoef.dgcoDureeMin());
		if (degressifCoef.dgcoDureeMax()!=null)
			tfCoefDureeMax.setText(""+degressifCoef.dgcoDureeMax());
		if (degressifCoef.dgcoCoef()!=null)
			tfCoefficient.setText(""+degressifCoef.dgcoCoef());
	}

	public void refreshDegressif() {
		myEOTableDegressif.updateData();		
	}

	public void refreshDegressifCoef() {
		myEOTableDegressifCoef.updateData();		
	}
	public void refreshModifDegressifCoef() {
		myEOTableModifDegressifCoef.updateData();		
	}

	public void setDegressif(NSArray data) {
		eodDegressif.setObjectArray(data);
		eodDegressif.updateDisplayedObjects();
		refreshDegressif();
	}

	public EODegressif degressifSelectionne() {
		try {
			return (EODegressif)myEOTableDegressif.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}

	public void setDegressifCoef(NSArray data) {
		eodDegressifCoef.setObjectArray(data);
		eodDegressifCoef.updateDisplayedObjects();
		refreshDegressifCoef();
	}
	public void setModifDegressifCoef(NSArray data) {
		eodModifDegressifCoef.setObjectArray(data);
		eodModifDegressifCoef.updateDisplayedObjects();
		refreshModifDegressifCoef();
	}

	public EODegressifCoef degressifCoefSelectionne() {
		try {
			return (EODegressifCoef)myEOTableDegressifCoef.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}

	public EODegressifCoef modifDegressifCoefSelectionne() {
		try {
			return (EODegressifCoef)myEOTableModifDegressifCoef.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}

	public JButton btNouveau() {
		if (btNouveau==null)
			btNouveau=new JButton("Nouveau");
		return btNouveau;
	}
	public JButton btModifier() {
		if (btModifier==null)
			btModifier=new JButton("Modifier");
		return btModifier;
	}
	public JButton btAnnuler() {
		if (btAnnuler==null)
			btAnnuler=new JButton("Annuler");
		return btAnnuler;
	}
	public JButton btSupprimer() {
		if (btSupprimer==null)
			btSupprimer=new JButton("Supprimer");
		return btSupprimer;
	}
	public JButton btEnregistrer() {
		if (btEnregistrer==null)
			btEnregistrer=new JButton("Enregistrer");
		return btEnregistrer;
	}
	public JButton btAjouterCoef() {
		if (btAjouterCoef==null) {
			btAjouterCoef=new JButtonCocktail();
			btAjouterCoef.setIcone(IconeCocktail.AJOUTER);
		}
		return btAjouterCoef;
	}
	public JButton btModifierCoef() {
		if (btModifierCoef==null) {
			btModifierCoef=new JButtonCocktail();
			btModifierCoef.setIcone(IconeCocktail.REFRESH);
		}
		return btModifierCoef;
	}
	public JButton btSupprimerCoef() {
		if (btSupprimerCoef==null) {
			btSupprimerCoef=new JButtonCocktail();
			btSupprimerCoef.setIcone(IconeCocktail.SUPPRIMER);
		}
		return btSupprimerCoef;
	}
	public JButton btAnnulerCoef() {
		if (btAnnulerCoef==null) {
			btAnnulerCoef=new JButtonCocktail();
			btAnnulerCoef.setIcone(IconeCocktail.REFUSER);
		}
		return btAnnulerCoef;
	}
	public JButton btValiderCoef() {
		if (btValiderCoef==null) {
			btValiderCoef=new JButtonCocktail();
			btValiderCoef.setPreferredSize(new Dimension(20,20));
			btValiderCoef.setIcone(IconeCocktail.VALIDER);
		}
		return btValiderCoef;
	}
	
	public ZEOTable tableDegressif() {
		if (myEOTableDegressif==null) {
			Vector myCols = new Vector();
			eodDegressif=new EODisplayGroup();

			ZEOTableModelColumn colDegressifDateDebut= new ZEOTableModelColumn(eodDegressif, EODegressif.DGRF_DATE_DEBUT_KEY, "Debut", 100);
			colDegressifDateDebut.setAlignment(SwingConstants.CENTER);
			colDegressifDateDebut.setFormatDisplay(DateFormat.getDateInstance());
			myCols.add(colDegressifDateDebut);

			ZEOTableModelColumn colDegressifDateFin= new ZEOTableModelColumn(eodDegressif, EODegressif.DGRF_DATE_FIN_KEY, "Fin", 100);
			colDegressifDateFin.setAlignment(SwingConstants.CENTER);
			colDegressifDateFin.setFormatDisplay(DateFormat.getDateInstance());
			myCols.add(colDegressifDateFin);

			TableSorter myTableSorterDegressif=new TableSorter(new ZEOTableModel(eodDegressif, myCols));
			myEOTableDegressif=new ZEOTable(myTableSorterDegressif);
			myTableSorterDegressif.setTableHeader(myEOTableDegressif.getTableHeader());		

			myEOTableDegressif.setBackground(tableBackgroundColor);
			myEOTableDegressif.setSelectionBackground(tableSelectionBackgroundColor);
			myEOTableDegressif.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}

		return myEOTableDegressif;
	}
	public ZEOTable tableDegressifCoef() {
		if (myEOTableDegressifCoef==null) {
			Vector myCols = new Vector();
			eodDegressifCoef=new EODisplayGroup();

			ZEOTableModelColumn colDegressifCoefDureeMin= new ZEOTableModelColumn(eodDegressifCoef, EODegressifCoef.DGCO_DUREE_MIN_KEY, "Duree Min", 100);
			colDegressifCoefDureeMin.setAlignment(SwingConstants.CENTER);
			myCols.add(colDegressifCoefDureeMin);

			ZEOTableModelColumn colDegressifCoefDureeMax= new ZEOTableModelColumn(eodDegressifCoef, EODegressifCoef.DGCO_DUREE_MAX_KEY, "Duree Max", 100);
			colDegressifCoefDureeMax.setAlignment(SwingConstants.CENTER);
			myCols.add(colDegressifCoefDureeMax);

			ZEOTableModelColumn colDegressifCoefCoefficient= new ZEOTableModelColumn(eodDegressifCoef, EODegressifCoef.DGCO_COEF_KEY, "Coef.", 100);
			colDegressifCoefCoefficient.setAlignment(SwingConstants.CENTER);
			myCols.add(colDegressifCoefCoefficient);

			TableSorter myTableSorterDegressifCoef=new TableSorter(new ZEOTableModel(eodDegressifCoef, myCols));
			myEOTableDegressifCoef=new ZEOTable(myTableSorterDegressifCoef);
			myTableSorterDegressifCoef.setTableHeader(myEOTableDegressifCoef.getTableHeader());		

			myEOTableDegressifCoef.setBackground(tableBackgroundColor);
			myEOTableDegressifCoef.setSelectionBackground(tableSelectionBackgroundColor);
			myEOTableDegressifCoef.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return myEOTableDegressifCoef;
	}

	public ZEOTable tableModifDegressifCoef() {
		if (myEOTableModifDegressifCoef==null) {
			Vector myCols = new Vector();
			eodModifDegressifCoef=new EODisplayGroup();

			ZEOTableModelColumn colDegressifCoefDureeMin= new ZEOTableModelColumn(eodModifDegressifCoef, EODegressifCoef.DGCO_DUREE_MIN_KEY, "Duree Min", 100);
			colDegressifCoefDureeMin.setAlignment(SwingConstants.CENTER);
			myCols.add(colDegressifCoefDureeMin);

			ZEOTableModelColumn colDegressifCoefDureeMax= new ZEOTableModelColumn(eodModifDegressifCoef, EODegressifCoef.DGCO_DUREE_MAX_KEY, "Duree Max", 100);
			colDegressifCoefDureeMax.setAlignment(SwingConstants.CENTER);
			myCols.add(colDegressifCoefDureeMax);

			ZEOTableModelColumn colDegressifCoefCoefficient= new ZEOTableModelColumn(eodModifDegressifCoef, EODegressifCoef.DGCO_COEF_KEY, "Coef.", 100);
			colDegressifCoefCoefficient.setAlignment(SwingConstants.CENTER);
			myCols.add(colDegressifCoefCoefficient);

			TableSorter myTableSorterDegressifCoef=new TableSorter(new ZEOTableModel(eodModifDegressifCoef, myCols));
			myEOTableModifDegressifCoef=new ZEOTable(myTableSorterDegressifCoef);
			myTableSorterDegressifCoef.setTableHeader(myEOTableModifDegressifCoef.getTableHeader());		

			myEOTableModifDegressifCoef.setBackground(tableBackgroundColor);
			myEOTableModifDegressifCoef.setSelectionBackground(tableSelectionBackgroundColor);
			myEOTableModifDegressifCoef.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		}
		return myEOTableModifDegressifCoef;
	}

	private void centerWindow() {
		int screenWidth = (int)getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int)getGraphicsConfiguration().getBounds().getHeight();
		setLocation((screenWidth/2)-((int)getSize().getWidth()/2), ((screenHeight/2)-((int)getSize().getHeight()/2)));
	}

	private JPanel panelConsultation() {
		JPanel panel=new JPanel(new BorderLayout());

		panel.add(sousPanelConsultationDonnees(), BorderLayout.CENTER);
		panel.add(sousPanelConsultationBoutons(), BorderLayout.SOUTH);

		return panel;
	}

	private JPanel sousPanelConsultationDonnees() {
		JPanel panel=new JPanel(new BorderLayout());
		JScrollPane scrollPaneDegressif=new JScrollPane(tableDegressif());
		scrollPaneDegressif.setPreferredSize(new Dimension(300,100));
		JSplitPane splitPane=new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollPaneDegressif, new JScrollPane(tableDegressifCoef()));

		panel.add(splitPane, BorderLayout.CENTER);
		return panel;
	}

	private JPanel sousPanelConsultationBoutons() {
		JPanel panel=new JPanel(new BorderLayout());

		JPanel panelBoutons=new JPanel(new GridLayout(1,0));
		panelBoutons.add(btNouveau());
		panelBoutons.add(btModifier());

		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(panelBoutons, BorderLayout.EAST);

		return panel;
	}

	private JPanel panelSaisie() {
		JPanel panel=new JPanel(new BorderLayout());

		panel.add(sousPanelSaisieSaisie(), BorderLayout.CENTER);
		panel.add(sousPanelSaisieBoutons(), BorderLayout.SOUTH);

		return panel;
	}

	private JPanel sousPanelSaisieSaisie() {
		JPanel panel=new JPanel(new BorderLayout());

		JPanel panelSaisie=new JPanel(new GridLayout(0,1));
		panelSaisie.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

		//MISE EN PLACE PANEL
		JLabel label=new JLabel("Date debut :");
		label.setPreferredSize(new Dimension(80,20));
		tfDegressifDateDebut=new JTextField();

		JPanel panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfDegressifDateDebut, BorderLayout.CENTER);
		panelSaisie.add(panelElement);

		//MISE EN PLACE PANEL
		label=new JLabel("Date fin :");
		label.setPreferredSize(new Dimension(80,20));
		tfDegressifDateFin=new JTextField();

		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfDegressifDateFin, BorderLayout.CENTER);
		panelSaisie.add(panelElement);
		
		
		cardLayoutCoef=new CardLayout();
		panelCardCoef=new JPanel(cardLayoutCoef);
		panelCardCoef.add(panelConsultationCoef(), panelConsultationCoef);
		panelCardCoef.add(panelSaisieCoef(), panelSaisieCoef);

		JPanel panelChamps=new JPanel(new BorderLayout());
		panelChamps.add(panelSaisie, BorderLayout.NORTH);
		panelChamps.add(panelCardCoef, BorderLayout.CENTER);
		panel.add(panelChamps, BorderLayout.CENTER);

		return panel;
	}
	
	private JPanel panelConsultationCoef() {
		JPanel panel=new JPanel(new BorderLayout());
		
		panel.add(sousPanelConsultationCoefBoutons(), BorderLayout.NORTH);
		panel.add(new JScrollPane(tableModifDegressifCoef()), BorderLayout.CENTER);
		
		return  panel;
	}
	private JPanel panelSaisieCoef() {
		JPanel panel=new JPanel(new BorderLayout());
		
		JPanel panelSaisie=new JPanel(new GridLayout(0,1));
		panelSaisie.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

		//MISE EN PLACE PANEL
		JLabel label=new JLabel("Duree min. :");
		label.setPreferredSize(new Dimension(80,20));
		tfCoefDureeMin=new JTextField();

		JPanel panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfCoefDureeMin, BorderLayout.CENTER);
		panelSaisie.add(panelElement);

		//MISE EN PLACE PANEL
		label=new JLabel("Duree max. :");
		label.setPreferredSize(new Dimension(80,20));
		tfCoefDureeMax=new JTextField();

		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfCoefDureeMax, BorderLayout.CENTER);
		panelSaisie.add(panelElement);

		//MISE EN PLACE PANEL
		label=new JLabel("Coefficient :");
		label.setPreferredSize(new Dimension(80,20));
		tfCoefficient=new JTextField();

		panelElement=new JPanel(new BorderLayout());
		panelElement.add(label,BorderLayout.WEST);
		panelElement.add(tfCoefficient, BorderLayout.CENTER);
		panelSaisie.add(panelElement);

		//MISE EN PLACE PANEL	
		panel.add(panelSaisie, BorderLayout.NORTH);
		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(sousPanelSaisieCoefBoutons(), BorderLayout.SOUTH);
		
		return  panel;
	}

	private JPanel sousPanelSaisieCoefBoutons() {
		JPanel panel=new JPanel(new BorderLayout());

		JPanel panelBoutons=new JPanel(new GridLayout(1,0));

		panelBoutons.add(btAnnulerCoef());
		panelBoutons.add(btValiderCoef());

		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(panelBoutons, BorderLayout.EAST);

		return panel;
	}

	private JPanel sousPanelConsultationCoefBoutons() {
		JPanel panel=new JPanel(new BorderLayout());

		JPanel panelBoutons=new JPanel(new GridLayout(1,0));

		panelBoutons.add(btAjouterCoef());
		panelBoutons.add(btModifierCoef());
		panelBoutons.add(btSupprimerCoef());

		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(panelBoutons, BorderLayout.EAST);

		return panel;
	}

	private JPanel sousPanelSaisieBoutons() {
		JPanel panel=new JPanel(new BorderLayout());

		JPanel panelBoutons=new JPanel(new GridLayout(1,0));

		panelBoutons.add(btSupprimer());
		panelBoutons.add(btEnregistrer());

		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(btAnnuler(), BorderLayout.WEST);
		panel.add(panelBoutons, BorderLayout.EAST);

		return panel;
	}

	public void panneauSuivant() { cardLayout.show(panelCard, panelSaisie); panneauConsultationCoef(); }
	public void panneauPrecedent() { cardLayout.show(panelCard, panelConsultation); }

	public void panneauSaisieCoef() { 
		cardLayoutCoef.show(panelCardCoef, panelSaisieCoef);
		btAnnuler().setEnabled(false);
		btEnregistrer().setEnabled(false);
		btSupprimer().setEnabled(false);
		
	}
	public void panneauConsultationCoef() { 
		cardLayoutCoef.show(panelCardCoef, panelConsultationCoef); 
		btAnnuler().setEnabled(true);
		btEnregistrer().setEnabled(true);
		btSupprimer().setEnabled(true);
	}
}