/*
 * ImpressionsNib2.java
 *
 * Created on 29 novembre 2007, 17:27
 */

package org.cocktail.corossol.client.nib;

import java.awt.BorderLayout;
import java.awt.Color;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOVCommande;
import org.cocktail.corossol.client.eof.metier.EOVDepense;
import org.cocktail.corossol.client.zutil.wo.table.TableSorter;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTable;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class ImpressionsNib extends org.cocktail.application.palette.JPanelCocktail  {

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private final Color tableBackgroundColor=new Color(230, 230, 230);
	private final Color tableSelectionBackgroundColor=new Color(127,127,127);

    /** Creates new form ImpressionsNib2 */
    public ImpressionsNib() {
        initComponents();
    }

	public void initTableView() {
		Vector myCols = new Vector();
		eod=new EODisplayGroup();

		ZEOTableModelColumn colNumero = new ZEOTableModelColumn(eod, EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY, "Numero", 130);
		colNumero.setAlignment(SwingConstants.LEFT);
		myCols.add(colNumero);

		ZEOTableModelColumn colCommande= new ZEOTableModelColumn(eod, EOInventaireComptable.COMMANDE_KEY+"."+EOVCommande.COMM_NUMERO_KEY, "Cde", 40);
		colCommande.setAlignment(SwingConstants.CENTER);
		myCols.add(colCommande);

		ZEOTableModelColumn colAmortissement= new ZEOTableModelColumn(eod, EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+EOCleInventaireComptable.CLIC_TYPE_AMORT_KEY, "Amort.", 40);
		colAmortissement.setAlignment(SwingConstants.CENTER);
		myCols.add(colAmortissement);

		ZEOTableModelColumn colMontant= new ZEOTableModelColumn(eod, EOInventaireComptable.INVC_MONTANT_RESIDUEL_KEY, "Montant", 60);
		colMontant.setAlignment(SwingConstants.RIGHT);
		colMontant.setColumnClass(BigDecimal.class);
		myCols.add(colMontant);
		
		ZEOTableModelColumn colPlanco= new ZEOTableModelColumn(eod, EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+EOCleInventaireComptable.PLAN_COMPTABLE_KEY+"."+
				EOPlanComptable.PCO_NUM_KEY, "Imput.", 40);
		colPlanco.setAlignment(SwingConstants.CENTER);
		myCols.add(colPlanco);
		
		ZEOTableModelColumn colFacture= new ZEOTableModelColumn(eod, EOInventaireComptable.V_DEPENSE_KEY+"."+EOVDepense.DPP_NUMERO_FACTURE_KEY, "Facture", 70);
		colFacture.setAlignment(SwingConstants.LEFT);
		myCols.add(colFacture);
		
		ZEOTableModelColumn colFactureMontantBud= new ZEOTableModelColumn(eod, EOInventaireComptable.V_DEPENSE_KEY+"."+EOVDepense.DPCO_MONTANT_BUDGETAIRE_KEY, "Fact. Mont bud.", 70);
		colFactureMontantBud.setAlignment(SwingConstants.RIGHT);
		colFactureMontantBud.setColumnClass(BigDecimal.class);
		myCols.add(colFactureMontantBud);
		
		ZEOTableModelColumn colFactureMontantTtc= new ZEOTableModelColumn(eod, EOInventaireComptable.V_DEPENSE_KEY+"."+EOVDepense.DPP_TTC_INITIAL_KEY, "Fact. Mont TTC", 70);
		colFactureMontantTtc.setAlignment(SwingConstants.RIGHT);
		colFactureMontantTtc.setColumnClass(BigDecimal.class);
		myCols.add(colFactureMontantTtc);
		
		ZEOTableModelColumn colMandat= new ZEOTableModelColumn(eod, EOInventaireComptable.V_DEPENSE_KEY+"."+EOVDepense.MAN_NUMERO_KEY, "Mandat", 30);
		colMandat.setAlignment(SwingConstants.CENTER);
		colMandat.setColumnClass(Integer.class);
		myCols.add(colMandat);
		
		ZEOTableModelColumn colBordereau= new ZEOTableModelColumn(eod, EOInventaireComptable.V_DEPENSE_KEY+"."+EOVDepense.BOR_NUM_KEY, "Bord.", 30);
		colBordereau.setAlignment(SwingConstants.CENTER);
		myCols.add(colBordereau);
		
		TableSorter myTableSorter=new TableSorter(new ZEOTableModel(eod, myCols));
		myEOTable=new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(tableBackgroundColor);
		myEOTable.setSelectionBackground(tableSelectionBackgroundColor);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		getListeTBV().setLayout(new BorderLayout());
		getListeTBV().add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	public ZEOTable getMaTable() {
		return myEOTable;
	}
	public NSArray inventaires() {
		try {
			return eod.allObjects();
		} catch (Exception e) {
			return new NSArray();
		}
	}
	public EOInventaireComptable inventaireComptableSelectionne() {
		try {
			return (EOInventaireComptable)myEOTable.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}

	public NSArray inventairesComptableSelectionnes() {
		try {
			return myEOTable.getSelectedObjects();
		} catch (Exception e) {
			return new NSArray();
		}
	}

	public void setInventaires(NSArray data) {
		if (eod==null)
			return;
		eod.setObjectArray(data);
		eod.updateDisplayedObjects();
		refresh();
	}

	public void refresh() {
		if (myEOTable==null)
			return;
		myEOTable.updateData();		
	}

    public javax.swing.JButton getJButtonCocktailDepense() {
        return jButtonCocktailDepense;
    }

    public javax.swing.JButton getJButtonCocktailFermer() {
        return jButtonCocktailFermer;
    }

    public javax.swing.JButton getJButtonCocktailImprimer() {
        return jButtonCocktailImprimer;
    }

    public javax.swing.JButton getJButtonCocktailModifier() {
        return jButtonCocktailModifier;
    }

    public javax.swing.JButton getJButtonCocktailRecherche() {
        return jButtonCocktailRecherche;
    }

    public javax.swing.JComboBox getJComboBoxCocktailExercice() {
        return jComboBoxCocktailExercice;
    }



    public javax.swing.JTextField getJTextFieldCocktailCr() {
        return jTextFieldCocktailCommande;
    }

    public javax.swing.JTextField getJTextFieldCocktailLabelInfo() {
        return jTextFieldCocktailLabelInfo;
    }

    public javax.swing.JTextField getJTextFieldCocktailUb() {
        return jTextFieldCocktailInventaire;
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jButtonCocktailModifier = new javax.swing.JButton();
        jButtonCocktailDepense = new javax.swing.JButton();
        jButtonCocktailImprimer = new javax.swing.JButton();
        jButtonCocktailEtiquette = new javax.swing.JButton();
        jCheckBoxA4 = new javax.swing.JCheckBox();
        jButtonCocktailCbPhysique = new javax.swing.JButton();
        jTextFieldCocktailInventaire = new javax.swing.JTextField();
        jLabelCocktail2 = new javax.swing.JLabel();
        jTextFieldCocktailCommande = new javax.swing.JTextField();
        jLabelCocktail1 = new javax.swing.JLabel();
        jComboBoxCocktailExercice = new javax.swing.JComboBox();
        jButtonCocktailRecherche = new javax.swing.JButton();
        jTextFieldCocktailImputation = new javax.swing.JTextField();
        jLabelCocktail3 = new javax.swing.JLabel();
        jTextFieldCocktailLabelInfo = new javax.swing.JTextField();
        jButtonCocktailFermer = new javax.swing.JButton();
        listeTBV = new javax.swing.JPanel();

        jButtonCocktailModifier.setText("Modifier les informations");

        jButtonCocktailDepense.setText("<html>G&eacute;rer le lien avec les d&eacute;penses");

        jButtonCocktailImprimer.setText("Fiche Inv.");

        jButtonCocktailEtiquette.setText("Etiquettes");

        jCheckBoxA4.setSelected(true);
        jCheckBoxA4.setText("A4");
        jCheckBoxA4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxA4ActionPerformed(evt);
            }
        });

        jButtonCocktailCbPhysique.setLabel("Code Barre Inv. Phys.");

        jLabelCocktail2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelCocktail2.setText("Cde :");

        jLabelCocktail1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelCocktail1.setText("Inventaire :");

        jComboBoxCocktailExercice.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jButtonCocktailRecherche.setText("Rechercher");

        jLabelCocktail3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelCocktail3.setText("Imput. :");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jButtonCocktailModifier, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 173, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailDepense))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jComboBoxCocktailExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 66, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabelCocktail1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jTextFieldCocktailInventaire, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 185, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jLabelCocktail2)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jButtonCocktailImprimer, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jTextFieldCocktailCommande, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE))
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(16, 16, 16)
                        .add(jButtonCocktailCbPhysique)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailEtiquette, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 107, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabelCocktail3)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jTextFieldCocktailImputation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 84, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailRecherche)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jCheckBoxA4)))
                .add(40, 40, 40))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jButtonCocktailEtiquette)
                        .add(jButtonCocktailCbPhysique))
                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jButtonCocktailModifier)
                        .add(jButtonCocktailDepense, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jButtonCocktailImprimer)))
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(11, 11, 11)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jComboBoxCocktailExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabelCocktail1)
                            .add(jTextFieldCocktailInventaire, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabelCocktail2)
                            .add(jTextFieldCocktailCommande, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabelCocktail3)
                            .add(jTextFieldCocktailImputation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jButtonCocktailRecherche)))
                    .add(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jCheckBoxA4)))
                .addContainerGap())
        );

        jTextFieldCocktailLabelInfo.setBackground(new java.awt.Color(255, 255, 0));
        jTextFieldCocktailLabelInfo.setEditable(false);
        jTextFieldCocktailLabelInfo.setText("IMPRESSIONS / MODIFICATIONS / RECHERCHES");

        jButtonCocktailFermer.setText("Fermer");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, listeTBV, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 759, Short.MAX_VALUE)
                    .add(jPanel2, 0, 759, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(jTextFieldCocktailLabelInfo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 615, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jButtonCocktailFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 121, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonCocktailFermer)
                    .add(jTextFieldCocktailLabelInfo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(listeTBV, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxA4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxA4ActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jCheckBoxA4ActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButtonCocktailCbPhysique;
    public javax.swing.JButton jButtonCocktailDepense;
    public javax.swing.JButton jButtonCocktailEtiquette;
    public javax.swing.JButton jButtonCocktailFermer;
    public javax.swing.JButton jButtonCocktailImprimer;
    public javax.swing.JButton jButtonCocktailModifier;
    public javax.swing.JButton jButtonCocktailRecherche;
    public javax.swing.JCheckBox jCheckBoxA4;
    public javax.swing.JComboBox jComboBoxCocktailExercice;
    public javax.swing.JLabel jLabelCocktail1;
    public javax.swing.JLabel jLabelCocktail2;
    public javax.swing.JLabel jLabelCocktail3;
    public javax.swing.JPanel jPanel2;
    public javax.swing.JTextField jTextFieldCocktailCommande;
    public javax.swing.JTextField jTextFieldCocktailImputation;
    public javax.swing.JTextField jTextFieldCocktailInventaire;
    public javax.swing.JTextField jTextFieldCocktailLabelInfo;
    public javax.swing.JPanel listeTBV;
    // End of variables declaration//GEN-END:variables
	public javax.swing.JButton getJButtonCocktailEtiquette() {
		return jButtonCocktailEtiquette;
	}

    public javax.swing.JButton getJButtonCocktailCbPhysique() {
		return jButtonCocktailCbPhysique;
	}

	public void setJButtonCocktailCbPhysique(
			javax.swing.JButton jButtonCocktailCbPhysique) {
		this.jButtonCocktailCbPhysique = jButtonCocktailCbPhysique;
	}

	public javax.swing.JCheckBox getJCheckBoxA4() {
		return jCheckBoxA4;
	}

	public javax.swing.JPanel getListeTBV() {
		return listeTBV;
	}

	public void setListeTBV(
			javax.swing.JPanel listeTBV) {
		this.listeTBV = listeTBV;
	}
    
}
