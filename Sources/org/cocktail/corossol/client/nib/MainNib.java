/*
 * MainNibnb.java
 *
 * Created on 30 octobre 2008, 09:31
 */

package org.cocktail.corossol.client.nib;

import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JPanelCocktail;

public class MainNib extends JPanelCocktail {
    
    public org.cocktail.application.palette.JTextFieldCocktail getJLabelCocktailCde() {
		return jLabelCocktailCde;
	}

	public void setJLabelCocktailCde(
			org.cocktail.application.palette.JTextFieldCocktail labelCocktailCde) {
		jLabelCocktailCde = labelCocktailCde;
	}

	public javax.swing.JLabel getJLabelInfo() {
		return jLabelInfo;
	}

	public void setJLabelInfo(javax.swing.JLabel labelInfo) {
		jLabelInfo = labelInfo;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailActif() {
		return jButtonCocktailActif;
	}

	public void setJButtonCocktailActif(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailActif) {
		jButtonCocktailActif = buttonCocktailActif;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailCommande() {
		return jButtonCocktailCommande;
	}

	public void setJButtonCocktailCommande(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailCommande) {
		jButtonCocktailCommande = buttonCocktailCommande;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailExercice() {
		return jButtonCocktailExercice;
	}

	public void setJButtonCocktailExercice(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailExercice) {
		jButtonCocktailExercice = buttonCocktailExercice;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailImpressions() {
		return jButtonCocktailImpressions;
	}

	public void setJButtonCocktailImpressions(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailImpressions) {
		jButtonCocktailImpressions = buttonCocktailImpressions;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailInvComptable() {
		return jButtonCocktailInvComptable;
	}

	public void setJButtonCocktailInvComptable(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailInvComptable) {
		jButtonCocktailInvComptable = buttonCocktailInvComptable;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailInvPhysique() {
		return jButtonCocktailInvPhysique;
	}

	public void setJButtonCocktailInvPhysique(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailInvPhysique) {
		jButtonCocktailInvPhysique = buttonCocktailInvPhysique;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailLivraison() {
		return jButtonCocktailLivraison;
	}

	public void setJButtonCocktailLivraison(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailLivraison) {
		jButtonCocktailLivraison = buttonCocktailLivraison;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailQuitter() {
		return jButtonCocktailQuitter;
	}

	public void setJButtonCocktailQuitter(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailQuitter) {
		jButtonCocktailQuitter = buttonCocktailQuitter;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailReprise() {
		return jButtonCocktailReprise;
	}

	public void setJButtonCocktailReprise(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailReprise) {
		jButtonCocktailReprise = buttonCocktailReprise;
	}

	public org.cocktail.application.palette.JButtonCocktail getJButtonCocktailSortie() {
		return jButtonCocktailSortie;
	}

	public void setJButtonCocktailSortie(
			org.cocktail.application.palette.JButtonCocktail buttonCocktailSortie) {
		jButtonCocktailSortie = buttonCocktailSortie;
	}

	public org.cocktail.application.palette.JButtonCocktail getjButtonCocktailSortie1() {
		return jButtonCocktailSortie1;
	}

	public void setjButtonCocktailSortie1(
			org.cocktail.application.palette.JButtonCocktail jButtonCocktailSortie1) {
		this.jButtonCocktailSortie1 = jButtonCocktailSortie1;
	}

       public JButtonCocktail getjButtonCocktailEditionsFinancements() {
          return jButtonCocktailEditionsFinancements;
       }

        public void setjButtonCocktailEditionsFinancements(JButtonCocktail jButtonCocktailEditionsFinancements) {
          this.jButtonCocktailEditionsFinancements = jButtonCocktailEditionsFinancements;
       }
        

	public org.cocktail.application.palette.JLabelCocktail getJLabelExercice() {
		return jLabelExercice;
	}

	public void setJLabelExercice(
			org.cocktail.application.palette.JLabelCocktail labelExercice) {
		jLabelExercice = labelExercice;
	}

	public javax.swing.JLabel getJLabelIcone() {
		return jLabelIcone;
	}

	public void setJLabelIcone(javax.swing.JLabel labelIcone) {
		jLabelIcone = labelIcone;
	}

	public org.cocktail.application.palette.JLabelCocktail getJLabelIconeBd() {
		return jLabelIconeBd;
	}

	public void setJLabelIconeBd(
			org.cocktail.application.palette.JLabelCocktail labelIconeBd) {
		jLabelIconeBd = labelIconeBd;
	}

	public javax.swing.JTextPane getJTextPaneBdJvm() {
		return jTextPaneBdJvm;
	}

	public void setJTextPaneBdJvm(javax.swing.JTextPane textPaneBdJvm) {
		jTextPaneBdJvm = textPaneBdJvm;
	}

	/** Creates new form MainNibnb */
    public MainNib() {
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelIcone = new javax.swing.JLabel();
        jLabelInfo = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jButtonCocktailLivraison = new org.cocktail.application.palette.JButtonCocktail();
        jTextFieldCocktail1 = new org.cocktail.application.palette.JTextFieldCocktail();
        jPanel2 = new javax.swing.JPanel();
        jButtonCocktailInvPhysique = new org.cocktail.application.palette.JButtonCocktail();
        jTextFieldCocktail2 = new org.cocktail.application.palette.JTextFieldCocktail();
        jPanel5 = new javax.swing.JPanel();
        jButtonCocktailInvComptable = new org.cocktail.application.palette.JButtonCocktail();
        jTextFieldCocktail5 = new org.cocktail.application.palette.JTextFieldCocktail();
        jPanel6 = new javax.swing.JPanel();
        jButtonCocktailReprise = new org.cocktail.application.palette.JButtonCocktail();
        jTextFieldCocktail6 = new org.cocktail.application.palette.JTextFieldCocktail();
        jPanel7 = new javax.swing.JPanel();
        jButtonCocktailImpressions = new org.cocktail.application.palette.JButtonCocktail();
        jTextFieldCocktail7 = new org.cocktail.application.palette.JTextFieldCocktail();
        jTextFieldCocktail9 = new org.cocktail.application.palette.JTextFieldCocktail();
        jButtonCocktailEditionsFinancements = new org.cocktail.application.palette.JButtonCocktail();
        jPanel8 = new javax.swing.JPanel();
        jTextFieldCocktail8 = new org.cocktail.application.palette.JTextFieldCocktail();
        jButtonCocktailActif = new org.cocktail.application.palette.JButtonCocktail();
        jPanel10 = new javax.swing.JPanel();
        jButtonCocktailQuitter = new org.cocktail.application.palette.JButtonCocktail();
        jPanel11 = new javax.swing.JPanel();
        jButtonCocktailSortie = new org.cocktail.application.palette.JButtonCocktail();
        jTextFieldCocktail10 = new org.cocktail.application.palette.JTextFieldCocktail();
        jButtonCocktailSortie1 = new org.cocktail.application.palette.JButtonCocktail();
        jTextFieldCocktail11 = new org.cocktail.application.palette.JTextFieldCocktail();
        jButtonCocktailExercice = new org.cocktail.application.palette.JButtonCocktail();
        jLabelCocktail1 = new org.cocktail.application.palette.JLabelCocktail();
        jButtonCocktailCommande = new org.cocktail.application.palette.JButtonCocktail();
        jLabelCocktailCde = new org.cocktail.application.palette.JTextFieldCocktail();
        jLabelIconeBd = new org.cocktail.application.palette.JLabelCocktail();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPaneBdJvm = new javax.swing.JTextPane();
        jLabelExercice = new org.cocktail.application.palette.JLabelCocktail();

        setMinimumSize(new java.awt.Dimension(0, 0));

        jLabelIcone.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabelInfo.setText("Version");

        jButtonCocktailLivraison.setText("Gestion des Livraisons");
        jButtonCocktailLivraison.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jTextFieldCocktail1.setEditable(false);
        jTextFieldCocktail1.setBackground(new java.awt.Color(128, 255, 255));
        jTextFieldCocktail1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextFieldCocktail1.setText("    ");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jTextFieldCocktail1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonCocktailLivraison, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(jTextFieldCocktail1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(jButtonCocktailLivraison, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jButtonCocktailInvPhysique.setText("Inventaire Physique");
        jButtonCocktailInvPhysique.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButtonCocktailInvPhysique.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCocktailInvPhysiqueActionPerformed(evt);
            }
        });

        jTextFieldCocktail2.setEditable(false);
        jTextFieldCocktail2.setBackground(new java.awt.Color(255, 128, 128));
        jTextFieldCocktail2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextFieldCocktail2.setText("    ");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jTextFieldCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonCocktailInvPhysique, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(jTextFieldCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(jButtonCocktailInvPhysique, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jButtonCocktailInvComptable.setText("Inventaire Comptable");
        jButtonCocktailInvComptable.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jTextFieldCocktail5.setEditable(false);
        jTextFieldCocktail5.setBackground(new java.awt.Color(128, 255, 0));
        jTextFieldCocktail5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextFieldCocktail5.setText("    ");

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jTextFieldCocktail5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonCocktailInvComptable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(jTextFieldCocktail5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(jButtonCocktailInvComptable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jButtonCocktailReprise.setText("Reprise d'inventaire");
        jButtonCocktailReprise.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jTextFieldCocktail6.setEditable(false);
        jTextFieldCocktail6.setBackground(new java.awt.Color(255, 128, 0));
        jTextFieldCocktail6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextFieldCocktail6.setText("    ");

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(jTextFieldCocktail6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonCocktailReprise, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(jTextFieldCocktail6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(jButtonCocktailReprise, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jButtonCocktailImpressions.setText("Impressions");
        jButtonCocktailImpressions.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jTextFieldCocktail7.setEditable(false);
        jTextFieldCocktail7.setBackground(new java.awt.Color(255, 255, 0));
        jTextFieldCocktail7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextFieldCocktail7.setText("    ");

        jTextFieldCocktail9.setEditable(false);
        jTextFieldCocktail9.setBackground(new java.awt.Color(255, 0, 128));
        jTextFieldCocktail9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextFieldCocktail9.setText("    ");

        jButtonCocktailEditionsFinancements.setText("Editions financements");
        jButtonCocktailEditionsFinancements.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButtonCocktailEditionsFinancements.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCocktailEditionsFinancementsActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel7Layout = new org.jdesktop.layout.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel7Layout.createSequentialGroup()
                        .add(jTextFieldCocktail7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailImpressions, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE))
                    .add(jPanel7Layout.createSequentialGroup()
                        .add(jTextFieldCocktail9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailEditionsFinancements, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextFieldCocktail7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailImpressions, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonCocktailEditionsFinancements, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldCocktail9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        jTextFieldCocktail8.setEditable(false);
        jTextFieldCocktail8.setBackground(new java.awt.Color(192, 192, 192));
        jTextFieldCocktail8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextFieldCocktail8.setText("    ");

        jButtonCocktailActif.setText("Suivi de l'actif");
        jButtonCocktailActif.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        org.jdesktop.layout.GroupLayout jPanel8Layout = new org.jdesktop.layout.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .add(jTextFieldCocktail8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonCocktailActif, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel8Layout.createSequentialGroup()
                .add(0, 0, Short.MAX_VALUE)
                .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextFieldCocktail8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailActif, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        jButtonCocktailQuitter.setText("Quitter");
        jButtonCocktailQuitter.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButtonCocktailQuitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCocktailQuitterActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel10Layout = new org.jdesktop.layout.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jButtonCocktailQuitter, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel10Layout.createSequentialGroup()
                .add(jButtonCocktailQuitter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButtonCocktailSortie.setText("Sortie d'inventaire ");
        jButtonCocktailSortie.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jTextFieldCocktail10.setEditable(false);
        jTextFieldCocktail10.setBackground(new java.awt.Color(128, 128, 255));
        jTextFieldCocktail10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextFieldCocktail10.setText("    ");

        jButtonCocktailSortie1.setText("Opérations de régularisation");
        jButtonCocktailSortie1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jTextFieldCocktail11.setEditable(false);
        jTextFieldCocktail11.setBackground(new java.awt.Color(204, 0, 204));
        jTextFieldCocktail11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextFieldCocktail11.setText("    ");

        org.jdesktop.layout.GroupLayout jPanel11Layout = new org.jdesktop.layout.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jTextFieldCocktail10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldCocktail11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jButtonCocktailSortie1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                    .add(jButtonCocktailSortie, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonCocktailSortie1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldCocktail11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel11Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextFieldCocktail10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailSortie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
        );

        jButtonCocktailExercice.setText("Exercice :");
        jButtonCocktailExercice.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabelCocktail1.setText("Commande :   ");

        jButtonCocktailCommande.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCocktailCommandeActionPerformed(evt);
            }
        });

        jLabelCocktailCde.setEditable(false);
        jLabelCocktailCde.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabelCocktailCde.setForeground(new java.awt.Color(255, 0, 0));
        jLabelCocktailCde.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jLabelCocktailCde.setText("Choisir une commande");
        jLabelCocktailCde.setAutoscrolls(false);
        jLabelCocktailCde.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabelCocktailCde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jLabelCocktailCdeActionPerformed(evt);
            }
        });

        jTextPaneBdJvm.setEditable(false);
        jTextPaneBdJvm.setBackground(new java.awt.Color(128, 128, 128));
        jTextPaneBdJvm.setBorder(null);
        jTextPaneBdJvm.setAutoscrolls(false);
        jTextPaneBdJvm.setEnabled(false);
        jTextPaneBdJvm.setFocusCycleRoot(false);
        jTextPaneBdJvm.setFocusable(false);
        jTextPaneBdJvm.setOpaque(false);
        jTextPaneBdJvm.setRequestFocusEnabled(false);
        jTextPaneBdJvm.setVerifyInputWhenFocusTarget(false);
        jScrollPane1.setViewportView(jTextPaneBdJvm);

        jLabelExercice.setForeground(new java.awt.Color(255, 0, 0));
        jLabelExercice.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelExercice.setText("XXXX");
        jLabelExercice.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jLabelCocktailCde, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel11, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel10, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(jLabelExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 116, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(layout.createSequentialGroup()
                                        .add(jLabelCocktail1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(111, 111, 111)
                                        .add(jButtonCocktailCommande, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .add(9, 9, 9))))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jLabelInfo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jLabelIconeBd, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 35, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jScrollPane1))
                    .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .add(20, 20, 20)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButtonCocktailExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 139, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabelIcone, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(jLabelInfo)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabelIcone, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 106, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonCocktailExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonCocktailCommande, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelCocktail1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jLabelCocktailCde, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanel11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanel7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanel10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 38, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelIconeBd, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCocktailQuitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCocktailQuitterActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jButtonCocktailQuitterActionPerformed

    private void jButtonCocktailInvPhysiqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCocktailInvPhysiqueActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jButtonCocktailInvPhysiqueActionPerformed

    private void jButtonCocktailCommandeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCocktailCommandeActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jButtonCocktailCommandeActionPerformed

    private void jLabelCocktailCdeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jLabelCocktailCdeActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jLabelCocktailCdeActionPerformed

    private void jButtonCocktailEditionsFinancementsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCocktailEditionsFinancementsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonCocktailEditionsFinancementsActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailActif;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailCommande;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailEditionsFinancements;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailExercice;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailImpressions;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailInvComptable;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailInvPhysique;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailLivraison;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailQuitter;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailReprise;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailSortie;
    public org.cocktail.application.palette.JButtonCocktail jButtonCocktailSortie1;
    public org.cocktail.application.palette.JLabelCocktail jLabelCocktail1;
    public org.cocktail.application.palette.JTextFieldCocktail jLabelCocktailCde;
    public org.cocktail.application.palette.JLabelCocktail jLabelExercice;
    public javax.swing.JLabel jLabelIcone;
    public org.cocktail.application.palette.JLabelCocktail jLabelIconeBd;
    public javax.swing.JLabel jLabelInfo;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JPanel jPanel10;
    public javax.swing.JPanel jPanel11;
    public javax.swing.JPanel jPanel2;
    public javax.swing.JPanel jPanel5;
    public javax.swing.JPanel jPanel6;
    public javax.swing.JPanel jPanel7;
    public javax.swing.JPanel jPanel8;
    public javax.swing.JScrollPane jScrollPane1;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktail1;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktail10;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktail11;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktail2;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktail5;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktail6;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktail7;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktail8;
    public org.cocktail.application.palette.JTextFieldCocktail jTextFieldCocktail9;
    public javax.swing.JTextPane jTextPaneBdJvm;
    // End of variables declaration//GEN-END:variables
    
}
