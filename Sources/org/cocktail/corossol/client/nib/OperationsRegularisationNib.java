package org.cocktail.corossol.client.nib;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.zutil.wo.table.TableSorter;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTable;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class OperationsRegularisationNib extends JFrame {

	private final Color tableBackgroundColor=new Color(230, 230, 230);
	private final Color tableSelectionBackgroundColor=new Color(127,127,127);

	private CardLayout cardLayout;
	private JPanel panelCard;
	private String panelConsultation="panelConsultation", panelRegroupement="panelRegroupement", panelEclatement="panelEclatement";

	private JButton btRechercher, btRegroupement, btEclatement;
	private JTextField tfRechercheCommande, tfRechercheNumeroInventaire;
	private EODisplayGroup eod;
	private ZEOTable myEOTable;

	private JButton btPrecedentRegroupement, btEnregistrerRegroupement;
	private JTextField tfInventaireRegroupement;
	private EODisplayGroup eodRegroupement;
	private ZEOTable myEOTableRegroupement;

	private JButton btPrecedentEclatement, btEnregistrerEclatement;
	private EODisplayGroup eodEclatement;
	private ZEOTable myEOTableEclatement;

	public OperationsRegularisationNib() {
		super("Operations de regularisation");
		setSize(600, 600);
		centerWindow();
		initGui();
	}

	private void initGui() {
		cardLayout=new CardLayout();
		getContentPane().setLayout(new BorderLayout());

		panelCard=new JPanel(cardLayout);
		panelCard.add(panelRecherche(), panelConsultation);
		panelCard.add(panelRegroupement(), panelRegroupement);
		panelCard.add(panelEclatement(), panelEclatement);
		getContentPane().add(panelCard);
	}

	public EOCleInventaireComptable inventaireSelectionne() {
		try {
			return (EOCleInventaireComptable)myEOTable.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}

	public void setInventaire(NSArray data) {
		eod.setObjectArray(data);
		eod.updateDisplayedObjects();
		refreshInventaire();
	}

	public void afficherInventaire(EOCleInventaireComptable inventaire) {/*
		textInventaire.setText("");	
		tfDateSortie.setText("");
		tfMotifSortie.setText("");
		tfValeurNetteComptable.setText("");
		tfValeurCession.setText("");

		if (inventaire==null)
			return;

		textInventaire.setText(inventaire.resume());

		if (inventaireSortie!=null) {
			if (inventaireSortie.invsDate()!=null)
				tfDateSortie.setText(DateCtrl.dateToString(inventaireSortie.invsDate(), "%d/%m/%Y"));
			if (inventaireSortie.invsMotif()!=null)
				tfMotifSortie.setText(inventaireSortie.invsMotif());
			if (inventaireSortie.invsVnc()!=null)
				tfValeurNetteComptable.setText(inventaireSortie.invsVnc().toString());
			if (inventaireSortie.invsValeurCession()!=null)
				tfValeurCession.setText(inventaireSortie.invsValeurCession().toString());
		}*/
	}

	public void refreshInventaire() {
		myEOTable.updateData();		
	}

	public JButton btRegroupement() {
		return btRegroupement;
	}
	public JButton btEclatement() {
		return btEclatement;
	}
	public JButton btPrecedentRegroupement() {
		return btPrecedentRegroupement;
	}
	public JButton btPrecedentEclatement() {
		return btPrecedentEclatement;
	}
	public JButton btRechercher() {
		return btRechercher;
	}
	public JButton btEnregistrerRegroupement() {
		return btEnregistrerRegroupement;
	}
	public JButton btEnregistrerEclatement() {
		return btEnregistrerEclatement;
	}
	public JTextField tfRechercheCommande() {
		return tfRechercheCommande;
	}
	public JTextField tfRechercheNumeroInventaire() {
		return tfRechercheNumeroInventaire;
	}
	public ZEOTable table() {
		return myEOTable;
	}

	private void centerWindow() {
		int screenWidth = (int)getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int)getGraphicsConfiguration().getBounds().getHeight();
		setLocation((screenWidth/2)-((int)getSize().getWidth()/2), ((screenHeight/2)-((int)getSize().getHeight()/2)));
	}

	private JPanel panelRecherche() {
		JPanel panel=new JPanel(new BorderLayout());

		panel.add(sousPanelRechercheCriteres(), BorderLayout.NORTH);
		panel.add(sousPanelRechercheResultats(), BorderLayout.CENTER);
		panel.add(sousPanelRechercheBoutons(), BorderLayout.SOUTH);

		return panel;
	}

	private JPanel sousPanelRechercheCriteres() {
		JPanel  panelCritereCommande=new JPanel(new BorderLayout()), panelCritereNumeroInventaire=new JPanel(new BorderLayout());

		panelCritereCommande.add(new JLabel("Commande :"),BorderLayout.WEST);
		tfRechercheCommande=new JTextField();
		tfRechercheCommande.setPreferredSize(new Dimension(50,10));
		panelCritereCommande.add(tfRechercheCommande,BorderLayout.CENTER);
		panelCritereNumeroInventaire.add(new JLabel("Numero inventaire :"),BorderLayout.WEST);
		tfRechercheNumeroInventaire=new JTextField();
		panelCritereNumeroInventaire.add(tfRechercheNumeroInventaire,BorderLayout.CENTER);

		JPanel panel=new JPanel(new BorderLayout());
		btRechercher=new JButton("Rechercher");
		panel.add(panelCritereCommande, BorderLayout.WEST);
		panel.add(panelCritereNumeroInventaire, BorderLayout.CENTER);
		panel.add(btRechercher, BorderLayout.EAST);

		tfRechercheCommande.addKeyListener( new KeyListener(){
			public void keyPressed(KeyEvent e) {if (e.getKeyCode()== KeyEvent.VK_ENTER)	btRechercher.doClick();}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){} });
		tfRechercheNumeroInventaire.addKeyListener( new KeyListener(){
			public void keyPressed(KeyEvent e) {if (e.getKeyCode()== KeyEvent.VK_ENTER)	btRechercher.doClick();}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){} });

		return panel;
	}

	private JPanel sousPanelRechercheResultats() {
		Vector myCols = new Vector();
		eod=new EODisplayGroup();

		ZEOTableModelColumn colNumeroInventaire = new ZEOTableModelColumn(eod,
				EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY, "Num Inventaire", 100);
		colNumeroInventaire.setAlignment(SwingConstants.LEFT);
		myCols.add(colNumeroInventaire);

		TableSorter myTableSorter=new TableSorter(new ZEOTableModel(eod, myCols));
		myEOTable=new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(tableBackgroundColor);
		myEOTable.setSelectionBackground(tableSelectionBackgroundColor);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JPanel panel=new JPanel(new BorderLayout());
		panel.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
		return panel;
	}

	private JPanel sousPanelRechercheBoutons() {
		JPanel panelBoutons=new JPanel(new GridLayout(1,0));
		btRegroupement=new JButton("Regroupement");
		btEclatement=new JButton("Eclatement");
		panelBoutons.add(btRegroupement);
		panelBoutons.add(btEclatement);

		JPanel panel=new JPanel(new BorderLayout());
		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(panelBoutons, BorderLayout.EAST);

		return panel;
	}

	private JPanel panelRegroupement() {
		JPanel panel=new JPanel(new BorderLayout());

		panel.add(sousPanelRegroupementSaisie(), BorderLayout.CENTER);
		panel.add(sousPanelRegroupementBoutons(), BorderLayout.SOUTH);

		return panel;
	}

	private JPanel sousPanelRegroupementSaisie() {
		Vector myCols = new Vector();
		eodRegroupement=new EODisplayGroup();

		ZEOTableModelColumn colNumeroInventaire = new ZEOTableModelColumn(eodRegroupement, 
				EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY, "Num Inventaire", 100);
		colNumeroInventaire.setAlignment(SwingConstants.LEFT);
		myCols.add(colNumeroInventaire);

		TableSorter myTableSorter=new TableSorter(new ZEOTableModel(eodRegroupement, myCols));
		myEOTableRegroupement=new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTableRegroupement.getTableHeader());		

		myEOTableRegroupement.setBackground(tableBackgroundColor);
		myEOTableRegroupement.setSelectionBackground(tableSelectionBackgroundColor);
		myEOTableRegroupement.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		tfInventaireRegroupement=new JTextField();
		JPanel panel=new JPanel(new BorderLayout());
		panel.add(tfInventaireRegroupement, BorderLayout.NORTH);
		panel.add(new JScrollPane(myEOTableRegroupement), BorderLayout.CENTER);
		return panel;
	}

	public EOCleInventaireComptable inventaireRegroupementSelectionne() {
		try {
			return (EOCleInventaireComptable)myEOTableRegroupement.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}

	public void setInventaireRegroupement(NSArray data) {
		eodRegroupement.setObjectArray(data);
		eodRegroupement.updateDisplayedObjects();
		refreshInventaireRegroupement();
	}

	public void refreshInventaireRegroupement() {
		myEOTableRegroupement.updateData();		
	}

	private JPanel sousPanelRegroupementBoutons() {
		JPanel panel=new JPanel(new BorderLayout());

		btPrecedentRegroupement=new JButton("Précédent");

		JPanel panelBoutons=new JPanel(new GridLayout(1,0));

		btEnregistrerRegroupement=new JButton("Enregistrer");
		panelBoutons.add(btEnregistrerRegroupement);

		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(btPrecedentRegroupement, BorderLayout.WEST);
		panel.add(panelBoutons, BorderLayout.EAST);

		return panel;
	}

	private JPanel panelEclatement() {
		JPanel panel=new JPanel(new BorderLayout());

		panel.add(sousPanelEclatementSaisie(), BorderLayout.CENTER);
		panel.add(sousPanelEclatementBoutons(), BorderLayout.SOUTH);

		return panel;
	}

	private JPanel sousPanelEclatementSaisie() {
		Vector myCols = new Vector();
		eodEclatement=new EODisplayGroup();

		ZEOTableModelColumn colLibelle = new ZEOTableModelColumn(eodEclatement, EOInventaire.LIVRAISON_DETAIL_KEY+"."+EOLivraisonDetail.LID_LIBELLE_KEY, "Libelle", 300);
		colLibelle.setAlignment(SwingConstants.LEFT);
		myCols.add(colLibelle);

		ZEOTableModelColumn colModele = new ZEOTableModelColumn(eodEclatement, EOInventaire.INV_MODELE_KEY, "Modele", 70);
		colModele.setAlignment(SwingConstants.LEFT);
		myCols.add(colModele);

		ZEOTableModelColumn colSerie = new ZEOTableModelColumn(eodEclatement, EOInventaire.INV_SERIE_KEY, "Num serie", 70);
		colSerie.setAlignment(SwingConstants.LEFT);
		myCols.add(colSerie);

		ZEOTableModelColumn colMontant = new ZEOTableModelColumn(eodEclatement, EOInventaire.INV_MONTANT_ACQUISITION_KEY, "Montant acq.", 70);
		colMontant.setAlignment(SwingConstants.RIGHT);
		myCols.add(colMontant);

		TableSorter myTableSorter=new TableSorter(new ZEOTableModel(eodEclatement, myCols));
		myEOTableEclatement=new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTableEclatement.getTableHeader());		

		myEOTableEclatement.setBackground(tableBackgroundColor);
		myEOTableEclatement.setSelectionBackground(tableSelectionBackgroundColor);
		myEOTableEclatement.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		//tfInventaireRegroupement=new JTextField();
		JPanel panel=new JPanel(new BorderLayout());
		//panel.add(tfInventaireRegroupement, BorderLayout.NORTH);
		panel.add(new JScrollPane(myEOTableEclatement), BorderLayout.CENTER);
		return panel;
	}

	public NSArray inventairesEclatementSelectionnes() {
		try {
			return myEOTableEclatement.getSelectedObjects();
		} catch (Exception e) {
			System.out.println(e);
			return new NSArray();
		}
	}

	public void setInventaireEclatement(NSArray data) {
		eodEclatement.setObjectArray(data);
		eodEclatement.updateDisplayedObjects();
		refreshInventaireEclatement();
	}

	public void refreshInventaireEclatement() {
		myEOTableEclatement.updateData();		
	}

	private JPanel sousPanelEclatementBoutons() {
		JPanel panel=new JPanel(new BorderLayout());

		btPrecedentEclatement=new JButton("Précédent");

		JPanel panelBoutons=new JPanel(new GridLayout(1,0));

		btEnregistrerEclatement=new JButton("Enregistrer");
		panelBoutons.add(btEnregistrerEclatement);

		panel.add(new JPanel(), BorderLayout.CENTER);
		panel.add(btPrecedentEclatement, BorderLayout.WEST);
		panel.add(panelBoutons, BorderLayout.EAST);

		return panel;
	}

	public void panneauRegroupement() { cardLayout.show(panelCard, panelRegroupement); }
	public void panneauEclatement() { cardLayout.show(panelCard, panelEclatement); }
	public void panneauPrecedent() { cardLayout.show(panelCard, panelConsultation); }

}