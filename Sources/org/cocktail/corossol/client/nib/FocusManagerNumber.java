/* FocusManagerNumber.java created by tsaivre on Wed 08-Oct-2003 */
package org.cocktail.corossol.client.nib;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.KeyEvent;

import javax.swing.DefaultFocusManager;
import javax.swing.JTextField;

import com.webobjects.foundation.NSArray;

public class FocusManagerNumber extends DefaultFocusManager {
    FocusManagerNumber() {
        super();
    }

    public void processKeyEvent(Component focusedComponent, KeyEvent anEvent) {
        // Test supprime, car le formattage ne marchait plus
        if (anEvent.getID()!=KeyEvent.KEY_TYPED) {
            super.processKeyEvent(focusedComponent, anEvent);
            return;
        }
        
        // Test sur les touches 'systemes' autorises
        if (anEvent.getKeyCode()==KeyEvent.VK_DELETE)
            return;
        if (anEvent.getKeyCode()==KeyEvent.VK_LEFT)
            return;
        if (anEvent.getKeyCode()==KeyEvent.VK_RIGHT)
            return;
        if (anEvent.getKeyCode()==KeyEvent.VK_BACK_SPACE)
            return;
        if (anEvent.getKeyChar()=='\b')
            return;
        if ((anEvent.getKeyChar()=='\n') || (anEvent.getKeyChar()=='\t')) {
            // le super.processKeyEvent(), permet aux enchainements entre textfield de fonctionner
            if (anEvent.getKeyChar()=='\t')
                super.processKeyEvent(focusedComponent, anEvent);

            if (anEvent.getKeyChar()=='\n')
                this.focusNextComponent(focusedComponent);

            return;
        }
        
        ///////////////////////////////////////////////////////////////////////////////////
        if (focusedComponent.getClass().getName().equals("arretes.client.JTextFieldInteger")) {
            if (!this.isInteger(anEvent.getKeyChar())) {
                anEvent.consume();
                return;
            }

            // Est-ce que la touche est valide selon sa position et les parametres du textfield
            if (!this.testValidite(focusedComponent, anEvent, false)) {
                anEvent.consume();
                return;
            }
            
            // On va remplacer la selection
            ((JTextField)focusedComponent).cut();
        }

        ///////////////////////////////////////////////////////////////////////////////////
        if (focusedComponent.getClass().getName().equals("arretes.client.JTextFieldNumber")) {
            if (anEvent.getKeyChar()==',')
                anEvent.setKeyChar('.');
            
            if (!this.isNumber(anEvent.getKeyChar())) {
                anEvent.consume();
                return;
            }

            // Est-ce que la touche est valide selon sa position et les parametres du textfield
            if (!this.testValidite(focusedComponent, anEvent, true)) {
                anEvent.consume();
                return;
            }

            // On va remplacer la selection
            ((JTextField)focusedComponent).cut();
        }

        ///////////////////////////////////////////////////////////////////////////////////
        if (focusedComponent.getClass().getName().equals("arretes.client.JTextFieldDate")) {
            if (!this.isDate(anEvent.getKeyChar())) {
                anEvent.consume();
                return;
            }

            // Est-ce que la touche est valide selon sa position et les parametres du textfield
            if (!this.testValiditeDate(focusedComponent, anEvent)) {
                anEvent.consume();
                return;
            }

            // On va remplacer la selection
            ((JTextField)focusedComponent).cut();
        }
    }

    public boolean testValidite(Component focusedComponent, KeyEvent anEvent, boolean isfloat) {
        if (anEvent.getKeyChar()=='-') {
            // Le moins ne peut etre accepte, car le textfield n'accepte pas les nombres negatifs
            if ((focusedComponent.getClass().getName().equals("arretes.client.JTextFieldInteger")) && (!((JTextFieldInteger)focusedComponent).negative()))
                return false;
            if ((focusedComponent.getClass().getName().equals("arretes.client.JTextFieldNumber")) && (!((JTextFieldNumber)focusedComponent).negative()))
                return false;

            // On interdit le deuxieme '-'
            if (StringCtrl.componentsSeparatedByString(((JTextField)focusedComponent).getText(), "-").count()>=2)
                return false;

            // Le moins ne peut etre qu'en premiere position
            if ( ((((JTextField)focusedComponent).getSelectedText()==null) && (((JTextField)focusedComponent).getCaret().getMark()>0)) ||
                 ((((JTextField)focusedComponent).getSelectedText()!=null) && (((JTextField)focusedComponent).getSelectionStart()>0)) )
                return false;

            return true;
        }

        // Dans le cas d'un JTextFieldNumber
        if (isfloat) {
            // Si il y a deja un point...
            NSArray	larray=StringCtrl.componentsSeparatedByString(((JTextField)focusedComponent).getText(), ".");
            if (larray.count()>=2) {
                // Si le point est dans la selection qui va etre remplacee alors aucun probleme sinon ...
                if ((((JTextField)focusedComponent).getSelectedText()==null) || (StringCtrl.componentsSeparatedByString(((JTextField)focusedComponent).getSelectedText(),".").count()==1)) {
                    // ...on interdit le deuxieme
                    if (anEvent.getKeyChar()=='.')
                        return false;

                    // ...si le curseur est apres le point, on limite a deux chiffres apres le point (en testant le cas de la selection)
                    if (((String)larray.objectAtIndex(1)).length()==((JTextFieldNumber)focusedComponent).nombresApresLaVirgule()) {
                        if ((((JTextField)focusedComponent).getCaret().getMark() > ((String)larray.objectAtIndex(0)).length()) && (((JTextField)focusedComponent).getSelectedText()==null))
                            return false;
                    }
                }
            }
        }

        // Dans le cas d'un JTextFieldInteger
        if (!isfloat) {
            // nombreChiffres <= -1   ->   pas de limitation !!
            if (((JTextFieldInteger)focusedComponent).nombreChiffres()>0) {
                if ((((JTextField)focusedComponent).getText().length()>=((JTextFieldInteger)focusedComponent).nombreChiffres()) && (((JTextField)focusedComponent).getSelectedText()==null))
                    return false;
            }
        }
        
        // Si il y a un moins on ne peut pas taper d'autres touches en premiere position
        if ((StringCtrl.componentsSeparatedByString(((JTextField)focusedComponent).getText(), "-").count()>=2) &&
            ((((JTextField)focusedComponent).getSelectedText()==null) && (((JTextField)focusedComponent).getCaret().getMark()==0)) )
            return false;

        return true;
    }

    public boolean testValiditeDate(Component focusedComponent, KeyEvent anEvent) {
        if (anEvent.getKeyChar()=='/') {
            // On interdit le troisieme '/'
            if (StringCtrl.componentsSeparatedByString(((JTextField)focusedComponent).getText(), "/").count()>=3)
                return false;

            // Le moins ne peut etre qu'en troisieme et sixieme position
            //   si pas de selection
            if (((JTextField)focusedComponent).getSelectedText()==null) {
                if ((((JTextField)focusedComponent).getCaret().getMark()!=2) && (((JTextField)focusedComponent).getCaret().getMark()!=5))
                    return false;
            }
            return true;
        }

        // Dans le cas des chiffres
        if (((JTextField)focusedComponent).getSelectedText()==null) {
            if ((((JTextField)focusedComponent).getText().length()>=10) || (((JTextField)focusedComponent).getCaret().getMark()==2) || (((JTextField)focusedComponent).getCaret().getMark()==5))
                return false;
        }

        return true;
    }

    // Methodes definies pas le DefaultFocusManager
    public void focusNextComponent(Component aComponent)     { super.focusNextComponent(aComponent);     }
    public void focusPreviousComponent(Component aComponent) { super.focusPreviousComponent(aComponent); }

    public Component getFirstComponent(Container aContainer) { return super.getFirstComponent(aContainer); }
    public Component getLastComponent(Container aContainer)  { return super.getLastComponent(aContainer);  }

    public Component getComponentBefore(Container aContainer, Component aComponent) { return super.getComponentBefore(aContainer, aComponent); }
    public Component getComponentAfter(Container aContainer, Component aComponent)  { return super.getComponentAfter(aContainer, aComponent);  }

    public boolean compareTabOrder(Component a, Component b) { return super.compareTabOrder(a, b); }

        
    // Methodes definissant les caracteres autorises
    public boolean isNumber(char code) {
        if ((this.isInteger(code)) || (code=='.'))
            return true;
        return false;
    }

    public boolean isDate(char code) {
        if ((this.isInteger(code)) || (code=='/'))
            return true;
        return false;
    }

    public boolean isInteger(char code) {
        if ((code=='0') || (code=='1') || (code=='2') || (code=='3') || (code=='4') || (code=='5') || (code=='6') || (code=='7') || (code=='8') || (code=='9') || (code=='-'))
            return true;
        return false;
    }
}
