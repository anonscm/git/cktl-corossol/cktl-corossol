/*
 * NewJPanel.java
 *
 * Created on 21 octobre 2007, 14:16
 */

package org.cocktail.corossol.client.nib;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;
import org.cocktail.corossol.client.zutil.wo.table.TableSorter;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTable;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class IntegrationNib extends JPanelCocktail {
    
    /** Creates new form NewJPanel */
    public IntegrationNib() {
        initComponents();
    }
    
	private EODisplayGroup eodReprises, eodSorties;
	private ZEOTable myEOTableReprises, myEOTableSorties;
	private final Color tableBackgroundColor=new Color(230, 230, 230);
	private final Color tableSelectionBackgroundColor=new Color(127,127,127);

	public void initTableViewReprises() {
		Vector myCols = new Vector();
		eodReprises=new EODisplayGroup();

		ZEOTableModelColumn colExercice = new ZEOTableModelColumn(eodReprises, EOInventaireNonBudgetaire.EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY, "Exercice", 30);
		colExercice.setAlignment(SwingConstants.LEFT);
		myCols.add(colExercice);

		ZEOTableModelColumn colNumInventaireComplet= new ZEOTableModelColumn(eodReprises, EOInventaireNonBudgetaire.INVENTAIRE_COMPTABLE_KEY+"."+
				EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY, "Num. Inventaire", 100);
		colNumInventaireComplet.setAlignment(SwingConstants.LEFT);
		myCols.add(colNumInventaireComplet);

		ZEOTableModelColumn colUb= new ZEOTableModelColumn(eodReprises, EOInventaireNonBudgetaire.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY, "Ub", 30);
		colUb.setAlignment(SwingConstants.LEFT);
		myCols.add(colUb);

		ZEOTableModelColumn colCr= new ZEOTableModelColumn(eodReprises, EOInventaireNonBudgetaire.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY, "Cr", 50);
		colCr.setAlignment(SwingConstants.LEFT);
		myCols.add(colCr);

		ZEOTableModelColumn colImputation= new ZEOTableModelColumn(eodReprises, EOInventaireNonBudgetaire.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY, "Imput.", 50);
		colImputation.setAlignment(SwingConstants.LEFT);
		myCols.add(colImputation);
		
		ZEOTableModelColumn colMontant= new ZEOTableModelColumn(eodReprises, EOInventaireNonBudgetaire.INB_MONTANT_KEY, "Montant", 80);
		colMontant.setAlignment(SwingConstants.RIGHT);
		myCols.add(colMontant);

		ZEOTableModelColumn colDuree= new ZEOTableModelColumn(eodReprises, EOInventaireNonBudgetaire.INB_DUREE_KEY, "Duree", 30);
		colDuree.setAlignment(SwingConstants.CENTER);
		myCols.add(colDuree);
	
		ZEOTableModelColumn colFournisseur= new ZEOTableModelColumn(eodReprises, EOInventaireNonBudgetaire.INB_FOURNISSEUR_KEY, "Fournis.", 150);
		colFournisseur.setAlignment(SwingConstants.LEFT);
		myCols.add(colFournisseur);
		
		TableSorter myTableSorter=new TableSorter(new ZEOTableModel(eodReprises, myCols));
		myEOTableReprises=new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTableReprises.getTableHeader());		

		myEOTableReprises.setBackground(tableBackgroundColor);
		myEOTableReprises.setSelectionBackground(tableSelectionBackgroundColor);
		myEOTableReprises.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		getInventairesTBVpanel().setLayout(new BorderLayout());
		getInventairesTBVpanel().add(new JScrollPane(myEOTableReprises), BorderLayout.CENTER);
	}

	public void initTableViewSorties() {
		Vector myCols = new Vector();
		eodSorties=new EODisplayGroup();

		ZEOTableModelColumn colDate = new ZEOTableModelColumn(eodSorties, EOInventaireComptableSortie.INVCS_DATE_KEY, "Date", 50);
		colDate.setAlignment(SwingConstants.LEFT);
		colDate.setColumnClass(NSTimestamp.class);
		colDate.setFormatDisplay(new NSTimestampFormatter("%d/%m/%Y"));
		myCols.add(colDate);

		ZEOTableModelColumn colAcquisition= new ZEOTableModelColumn(eodSorties, EOInventaireComptableSortie.INVCS_MONTANT_ACQUISITION_SORTIE_KEY, "Acq.", 50);
		colAcquisition.setAlignment(SwingConstants.LEFT);
		myCols.add(colAcquisition);

		ZEOTableModelColumn colVnc= new ZEOTableModelColumn(eodSorties, EOInventaireComptableSortie.INVCS_VNC_KEY, "VNC", 50);
		colVnc.setAlignment(SwingConstants.LEFT);
		myCols.add(colVnc);

		ZEOTableModelColumn colMotif= new ZEOTableModelColumn(eodSorties, EOInventaireComptableSortie.INVCS_MOTIF_KEY, "Motif", 50);
		colMotif.setAlignment(SwingConstants.LEFT);
		myCols.add(colMotif);

		TableSorter myTableSorter=new TableSorter(new ZEOTableModel(eodSorties, myCols));
		myEOTableSorties=new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTableSorties.getTableHeader());		

		myEOTableSorties.setBackground(tableBackgroundColor);
		myEOTableSorties.setSelectionBackground(tableSelectionBackgroundColor);
		myEOTableSorties.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		getSortiesTBVpanel().setLayout(new BorderLayout());
		getSortiesTBVpanel().add(new JScrollPane(myEOTableSorties), BorderLayout.CENTER);
	}

	public ZEOTable getMaTableReprises() {
		return myEOTableReprises;
	}

	public ZEOTable getMaTableSorties() {
		return myEOTableSorties;
	}
	public EOInventaireNonBudgetaire inventaireSelectionne() {
		try {
			return (EOInventaireNonBudgetaire)myEOTableReprises.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}
	public EOInventaireComptableSortie inventaireSortieSelectionne() {
		try {
			return (EOInventaireComptableSortie)myEOTableSorties.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}

	public void setInventaire(NSArray data) {
		if (eodReprises==null)
			return;
		eodReprises.setObjectArray(data);
		eodReprises.updateDisplayedObjects();
		refreshInventaire();
	}

	public void setSorties(NSArray data) {
		if (eodSorties==null)
			return;
		eodSorties.setObjectArray(data);
		eodSorties.updateDisplayedObjects();
		refreshSorties();
	}

	public void setQualifier(EOQualifier qualifier) {
		if (eodReprises==null)
			return;
		eodReprises.setQualifier(qualifier);
		eodReprises.updateDisplayedObjects();
		refreshInventaire();
	}

	public void refreshInventaire() {
		if (myEOTableReprises==null)
			return;
		myEOTableReprises.updateData();		
	}
	public void refreshSorties() {
		if (myEOTableSorties==null)
			return;
		myEOTableSorties.updateData();		
	}

    public JPanel getInventairesTBVpanel() {
		return inventairesTBVpanel;
	}

    public JPanel getSortiesTBVpanel() {
		return sortiesTBVpanel;
	}

	public void setInventairesTBVpanel(JPanel inventairesTBV) {
		this.inventairesTBVpanel = inventairesTBV;
	}

    public javax.swing.JButton getBtAjouterSortie() {
		return btAjouterSortie;
	}

    public javax.swing.JButton getBtSupprimerSortie() {
		return btSupprimerSortie;	}

	/** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextFieldCocktailLabelInfo = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabelCocktail1 = new javax.swing.JLabel();
        jLabelCocktail3 = new javax.swing.JLabel();
        jLabelCocktail5 = new javax.swing.JLabel();
        jLabelCocktail6 = new javax.swing.JLabel();
        jLabelCocktail7 = new javax.swing.JLabel();
        jLabelCocktail8 = new javax.swing.JLabel();
        jLabelCocktail9 = new javax.swing.JLabel();
        jTextFieldExercice = new javax.swing.JTextField();
        jTextFieldBudgetaire = new javax.swing.JTextField();
        jTextFieldImputation = new javax.swing.JTextField();
        jButtonCocktailExercice = new javax.swing.JButton();
        jButtonCocktailOrgan = new javax.swing.JButton();
        jButtonCocktailImputation = new javax.swing.JButton();
        jComboBoxCocktailFinancement = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldCocktailAmort = new javax.swing.JTextField();
        jTextFieldResiduel = new javax.swing.JFormattedTextField();
        jTextFieldMontant = new javax.swing.JFormattedTextField();
        jComboBoxCocktailAmort = new javax.swing.JComboBox();
        jTextFieldNumero = new javax.swing.JTextField();
        jLabelCocktail2 = new javax.swing.JLabel();
        jTextFieldDuree = new javax.swing.JFormattedTextField();
        jLabelCocktail10 = new javax.swing.JLabel();
        jTextFieldDateAcquisition = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaFactures = new javax.swing.JTextArea();
        jLabelCocktail13 = new javax.swing.JLabel();
        jTextFieldFournisseur = new javax.swing.JTextField();
        jLabelCocktail12 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaInformations = new javax.swing.JTextArea();
        jLabelCocktail11 = new javax.swing.JLabel();
        jButtonCocktailImprimer = new javax.swing.JButton();
        jButtonCocktailFermer = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jButtonCocktailSupprimer = new javax.swing.JButton();
        jLabelCocktail4 = new javax.swing.JLabel();
        jButtonCocktailValider = new javax.swing.JButton();
        jButtonCocktailAjouter = new javax.swing.JButton();
        jButtonCocktailModifier = new javax.swing.JButton();
        jTextFieldRecherche = new javax.swing.JFormattedTextField();
        jButtonRechercher = new javax.swing.JButton();
        jButtonReinitRecherche = new javax.swing.JButton();
        inventairesTBVpanel = new javax.swing.JPanel();
        sortiesTBVpanel = new javax.swing.JPanel();
        btAjouterSortie = new javax.swing.JButton();
        btSupprimerSortie = new javax.swing.JButton();

        jTextFieldCocktailLabelInfo.setBackground(new java.awt.Color(255, 153, 51));
        jTextFieldCocktailLabelInfo.setEditable(false);
        jTextFieldCocktailLabelInfo.setText("INTEGRATION D UN NOUVEAU NUMERO D INVENTAIRE");
        jTextFieldCocktailLabelInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCocktailLabelInfoActionPerformed(evt);
            }
        });

        jLabelCocktail1.setText("Exercice :");

        jLabelCocktail3.setText("<html>Ligne budg&eacute;taire :");

        jLabelCocktail5.setText("Imputation :");

        jLabelCocktail6.setText("<html>Dur&eacute;e :");

        jLabelCocktail7.setText("Type Financement :");

        jLabelCocktail8.setText("Montant :");

        jLabelCocktail9.setText("Montant Residuel :");

        jTextFieldExercice.setEditable(false);
        jTextFieldExercice.setEnabled(false);

        jTextFieldBudgetaire.setEditable(false);
        jTextFieldBudgetaire.setEnabled(false);

        jTextFieldImputation.setEditable(false);
        jTextFieldImputation.setEnabled(false);

        jComboBoxCocktailFinancement.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel1.setText("Imputation Amort.");

        jTextFieldCocktailAmort.setEditable(false);
        jTextFieldCocktailAmort.setEnabled(false);

        jTextFieldResiduel.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jTextFieldMontant.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jComboBoxCocktailAmort.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jTextFieldNumero.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextFieldNumero.setEnabled(false);
        jTextFieldNumero.setFocusable(false);

        jLabelCocktail2.setText("<html>Num&eacute;ro :");

        jLabelCocktail10.setText("Date acquisition :");

        jTextFieldDateAcquisition.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabelCocktail7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .add(jLabelCocktail8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .add(jLabelCocktail9, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .add(jLabelCocktail6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jTextFieldResiduel)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jTextFieldMontant)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, jComboBoxCocktailFinancement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 259, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jPanel1Layout.createSequentialGroup()
                                    .add(jTextFieldDuree, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 51, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(jComboBoxCocktailAmort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 156, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap())
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .add(jLabelCocktail3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .add(jLabelCocktail5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabelCocktail1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .add(jLabelCocktail10, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jTextFieldCocktailAmort)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jTextFieldImputation)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jTextFieldBudgetaire, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jTextFieldDateAcquisition)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jTextFieldExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 75, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(jButtonCocktailExercice, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jButtonCocktailOrgan, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jButtonCocktailImputation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)))
                            .add(jTextFieldNumero, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 217, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(11, 11, 11))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldNumero, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail1)
                    .add(jTextFieldExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(8, 8, 8)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldBudgetaire, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailOrgan, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail5)
                    .add(jTextFieldImputation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailImputation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(jTextFieldCocktailAmort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail10)
                    .add(jTextFieldDateAcquisition, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(14, 14, 14)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabelCocktail6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jTextFieldDuree, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jComboBoxCocktailAmort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .add(jComboBoxCocktailFinancement, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail8)
                    .add(jTextFieldMontant, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail9)
                    .add(jTextFieldResiduel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2.setAlignmentX(0.0F);
        jPanel2.setAlignmentY(0.0F);

        jTextAreaFactures.setColumns(20);
        jTextAreaFactures.setRows(5);
        jScrollPane2.setViewportView(jTextAreaFactures);

        jLabelCocktail13.setText("Factures :");

        jLabelCocktail12.setText("Fournisseur :");

        jTextAreaInformations.setColumns(20);
        jTextAreaInformations.setRows(5);
        jScrollPane1.setViewportView(jTextAreaInformations);

        jLabelCocktail11.setText("Informations :");

        jButtonCocktailImprimer.setText("Imprimer Fiche");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabelCocktail11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 89, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 108, Short.MAX_VALUE)
                        .add(jButtonCocktailImprimer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 138, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jLabelCocktail12)
                    .add(jTextFieldFournisseur, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                    .add(jLabelCocktail13)
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail11)
                    .add(jButtonCocktailImprimer))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 84, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabelCocktail12)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jTextFieldFournisseur, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabelCocktail13)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButtonCocktailFermer.setText("Fermer");
        jButtonCocktailFermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCocktailFermerActionPerformed(evt);
            }
        });

        jButtonCocktailSupprimer.setText("Supprimer");

        jLabelCocktail4.setText("Recherche :");

        jButtonCocktailValider.setText("Valider");
        jButtonCocktailValider.setPreferredSize(new java.awt.Dimension(60, 23));

        jButtonCocktailAjouter.setText("Ajouter");
        jButtonCocktailAjouter.setPreferredSize(new java.awt.Dimension(60, 23));

        jButtonCocktailModifier.setText("Modifier");

        jTextFieldRecherche.setHorizontalAlignment(javax.swing.JTextField.LEFT);

        jButtonRechercher.setText("Chercher");

        jButtonReinitRecherche.setText("Tous");

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel3Layout.createSequentialGroup()
                        .add(jButtonCocktailValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 89, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailModifier)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailSupprimer, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE))
                    .add(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jLabelCocktail4)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jTextFieldRecherche, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 171, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonRechercher)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonReinitRecherche, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonCocktailValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailAjouter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailModifier)
                    .add(jButtonCocktailSupprimer))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 31, Short.MAX_VALUE)
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 26, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldRecherche, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonRechercher)
                    .add(jButtonReinitRecherche)))
        );

        btAjouterSortie.setText("+");

        btSupprimerSortie.setText("-");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jTextFieldCocktailLabelInfo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
                                    .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(18, 18, 18)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(layout.createSequentialGroup()
                                        .add(sortiesTBVpanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 257, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(18, 18, 18)
                                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                            .add(btAjouterSortie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .add(btSupprimerSortie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(15, 15, 15))
                                    .add(jButtonCocktailFermer, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                            .add(layout.createSequentialGroup()
                                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .add(inventairesTBVpanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextFieldCocktailLabelInfo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailFermer))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(btAjouterSortie)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btSupprimerSortie))
                    .add(sortiesTBVpanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                    .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(inventairesTBVpanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldCocktailLabelInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCocktailLabelInfoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCocktailLabelInfoActionPerformed

    private void jButtonCocktailFermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCocktailFermerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonCocktailFermerActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAjouterSortie;
	private javax.swing.JButton btSupprimerSortie;
    private javax.swing.JPanel inventairesTBVpanel;
    public javax.swing.JButton jButtonCocktailAjouter;
    public javax.swing.JButton jButtonCocktailExercice;
    public javax.swing.JButton jButtonCocktailFermer;
    public javax.swing.JButton jButtonCocktailImprimer;
    public javax.swing.JButton jButtonCocktailImputation;
    public javax.swing.JButton jButtonCocktailModifier;
    public javax.swing.JButton jButtonCocktailOrgan;
    public javax.swing.JButton jButtonCocktailSupprimer;
    public javax.swing.JButton jButtonCocktailValider;
    public javax.swing.JButton jButtonRechercher;
    public javax.swing.JButton jButtonReinitRecherche;
    public javax.swing.JComboBox jComboBoxCocktailAmort;
    public javax.swing.JComboBox jComboBoxCocktailFinancement;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelCocktail1;
    private javax.swing.JLabel jLabelCocktail10;
    private javax.swing.JLabel jLabelCocktail11;
    private javax.swing.JLabel jLabelCocktail12;
    private javax.swing.JLabel jLabelCocktail13;
    private javax.swing.JLabel jLabelCocktail2;
    private javax.swing.JLabel jLabelCocktail3;
    private javax.swing.JLabel jLabelCocktail4;
    private javax.swing.JLabel jLabelCocktail5;
    private javax.swing.JLabel jLabelCocktail6;
    private javax.swing.JLabel jLabelCocktail7;
    private javax.swing.JLabel jLabelCocktail8;
    private javax.swing.JLabel jLabelCocktail9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JTextArea jTextAreaFactures;
    public javax.swing.JTextArea jTextAreaInformations;
    public javax.swing.JTextField jTextFieldBudgetaire;
    public javax.swing.JTextField jTextFieldCocktailAmort;
    public javax.swing.JTextField jTextFieldCocktailLabelInfo;
    public javax.swing.JFormattedTextField jTextFieldDateAcquisition;
    public javax.swing.JFormattedTextField jTextFieldDuree;
    public javax.swing.JTextField jTextFieldExercice;
    public javax.swing.JTextField jTextFieldFournisseur;
    public javax.swing.JTextField jTextFieldImputation;
    public javax.swing.JFormattedTextField jTextFieldMontant;
    public javax.swing.JTextField jTextFieldNumero;
    public javax.swing.JFormattedTextField jTextFieldRecherche;
    public javax.swing.JFormattedTextField jTextFieldResiduel;
    private javax.swing.JPanel sortiesTBVpanel;
    // End of variables declaration//GEN-END:variables
    
}
