/*
 * CommandeNibnb.java
 *
 * Created on 30 octobre 2008, 10:40
 */

package org.cocktail.corossol.client.nib;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.application.client.eof.VFournisseur;
import org.cocktail.corossol.client.eof.metier.EOVCommande;
import org.cocktail.corossol.client.zutil.wo.table.TableSorter;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTable;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class CommandeNib extends JPanelCocktail {
    
	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private final Color tableBackgroundColor=new Color(230, 230, 230);
	private final Color tableSelectionBackgroundColor=new Color(127,127,127);
	
	public void initTableView() {
		Vector myCols = new Vector();
		eod=new EODisplayGroup();

		ZEOTableModelColumn colNumero = new ZEOTableModelColumn(eod, EOVCommande.EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY, "Exercice", 50);
		colNumero.setAlignment(SwingConstants.CENTER);
		myCols.add(colNumero);

		ZEOTableModelColumn colCommande= new ZEOTableModelColumn(eod, EOVCommande.COMM_NUMERO_KEY, "Commande", 50);
		colCommande.setAlignment(SwingConstants.CENTER);
		myCols.add(colCommande);

		ZEOTableModelColumn colReference= new ZEOTableModelColumn(eod, EOVCommande.COMM_REFERENCE_KEY, "Reference", 100);
		colReference.setAlignment(SwingConstants.LEFT);
		myCols.add(colReference);

		ZEOTableModelColumn colDate= new ZEOTableModelColumn(eod, EOVCommande.COMM_DATE_KEY, "Du", 80);
		colDate.setAlignment(SwingConstants.CENTER);
		colDate.setColumnClass(NSTimestamp.class);
		colDate.setFormatDisplay(new NSTimestampFormatter("%d/%m/%Y"));
		myCols.add(colDate);
		
		ZEOTableModelColumn colFournisseur= new ZEOTableModelColumn(eod, EOVCommande.V_FOURNISSEUR_KEY+"."+VFournisseur.ADR_NOM_KEY, "Four.", 100);
		colFournisseur.setAlignment(SwingConstants.LEFT);
		myCols.add(colFournisseur);
		
		ZEOTableModelColumn colLibelle= new ZEOTableModelColumn(eod, EOVCommande.COMM_LIBELLE_KEY, "Libelle", 100);
		colLibelle.setAlignment(SwingConstants.LEFT);
		myCols.add(colLibelle);
		
		TableSorter myTableSorter=new TableSorter(new ZEOTableModel(eod, myCols));
		myEOTable=new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());		

		myEOTable.setBackground(tableBackgroundColor);
		myEOTable.setSelectionBackground(tableSelectionBackgroundColor);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		getJPanelListeDesCommandes().setLayout(new BorderLayout());
		getJPanelListeDesCommandes().add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	public ZEOTable getMaTable() {
		return myEOTable;
	}
	public NSArray commandes() {
		try {
			return eod.allObjects();
		} catch (Exception e) {
			return new NSArray();
		}
	}
	public EOVCommande commandeSelectionnee() {
		try {
			return (EOVCommande)myEOTable.getSelectedObject();
		} catch (Exception e) {
			return null;
		}
	}

	public void setCommandes(NSArray data) {
		if (eod==null)
			return;
		eod.setObjectArray(data);
		eod.updateDisplayedObjects();
		refresh();
	}

	public void refresh() {
		if (myEOTable==null)
			return;
		myEOTable.updateData();		
	}

    public JButton getJButtonCocktailAnnuler() {
		return jButtonCocktailAnnuler;
	}

	public void setJButtonCocktailAnnuler(JButton buttonCocktailAnnuler) {
		jButtonCocktailAnnuler = buttonCocktailAnnuler;
	}

	public JButton getJButtonCocktailRecherche() {
		return jButtonCocktailRecherche;
	}

	public void setJButtonCocktailRecherche(JButton buttonCocktailRecherche) {
		jButtonCocktailRecherche = buttonCocktailRecherche;
	}

	public JButton getJButtonCocktailSelectionner() {
		return jButtonCocktailSelectionner;
	}

	public void setJButtonCocktailSelectionner(JButton buttonCocktailSelectionner) {
		jButtonCocktailSelectionner = buttonCocktailSelectionner;
	}

	public javax.swing.JCheckBox getJCheckBoxClasse() {
		return jCheckBoxClasse;
	}

	public void setJCheckBoxClasse(javax.swing.JCheckBox checkBoxClasse) {
		jCheckBoxClasse = checkBoxClasse;
	}

	public JFormattedTextField getJFormattedTextFieldBorneInf() {
		return jFormattedTextFieldBorneInf;
	}

	public void setJFormattedTextFieldBorneInf(
			JFormattedTextField formattedTextFieldBorneInf) {
		jFormattedTextFieldBorneInf = formattedTextFieldBorneInf;
	}

	public JFormattedTextField getJFormattedTextFieldBorneSup() {
		return jFormattedTextFieldBorneSup;
	}

	public void setJFormattedTextFieldBorneSup(JFormattedTextField formattedTextFieldBorneSup) {
		jFormattedTextFieldBorneSup = formattedTextFieldBorneSup;
	}

	public JPanel getJPanelListeDesCommandes() {
		return jPanelListeDesCommandes;
	}

	public void setJPanelListeDesCommandes(JPanel panelListeDesCommandes) {
		jPanelListeDesCommandes = panelListeDesCommandes;
	}

	/** Creates new form CommandeNibnb */
    public CommandeNib() {
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabelCocktail3 = new javax.swing.JLabel();
        jButtonCocktailRecherche = new javax.swing.JButton();
        jFormattedTextFieldBorneInf = new javax.swing.JFormattedTextField();
        jFormattedTextFieldBorneSup = new javax.swing.JFormattedTextField();
        jCheckBoxClasse = new javax.swing.JCheckBox();
        jLabelCocktail2 = new javax.swing.JLabel();
        jButtonCocktailAnnuler = new javax.swing.JButton();
        jLabelCocktail1 = new javax.swing.JLabel();
        jButtonCocktailSelectionner = new javax.swing.JButton();
        jPanelListeDesCommandes = new javax.swing.JPanel();

        jLabelCocktail3.setText("et");

        jButtonCocktailRecherche.setText("Rechercher");

        jFormattedTextFieldBorneInf.setText("0");
        jFormattedTextFieldBorneInf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTextFieldBorneInfActionPerformed(evt);
            }
        });

        jFormattedTextFieldBorneSup.setText("0");

        jCheckBoxClasse.setText("Uniquement classe 2");

        jLabelCocktail2.setText("Numero de commande entre : ");

        jButtonCocktailAnnuler.setText("Annuler");

        jLabelCocktail1.setText("Choisir votre commande : ");

        jButtonCocktailSelectionner.setText("ok");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanelListeDesCommandes, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 553, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2Layout.createSequentialGroup()
                        .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 205, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 27, Short.MAX_VALUE)
                        .add(jFormattedTextFieldBorneInf, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 88, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabelCocktail3)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jFormattedTextFieldBorneSup, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 98, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailRecherche, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 111, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2Layout.createSequentialGroup()
                        .add(jLabelCocktail1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jCheckBoxClasse))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jButtonCocktailAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 101, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(59, 59, 59)
                        .add(jButtonCocktailSelectionner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 106, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail1)
                    .add(jCheckBoxClasse))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jFormattedTextFieldBorneInf, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelCocktail3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jFormattedTextFieldBorneSup, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonCocktailRecherche)
                    .add(jLabelCocktail2))
                .add(7, 7, 7)
                .add(jPanelListeDesCommandes, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonCocktailAnnuler)
                    .add(jButtonCocktailSelectionner))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jFormattedTextFieldBorneInfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedTextFieldBorneInfActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jFormattedTextFieldBorneInfActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButtonCocktailAnnuler;
    public javax.swing.JButton jButtonCocktailRecherche;
    public javax.swing.JButton jButtonCocktailSelectionner;
    public javax.swing.JCheckBox jCheckBoxClasse;
    public javax.swing.JFormattedTextField jFormattedTextFieldBorneInf;
    public javax.swing.JFormattedTextField jFormattedTextFieldBorneSup;
    public javax.swing.JLabel jLabelCocktail1;
    public javax.swing.JLabel jLabelCocktail2;
    public javax.swing.JLabel jLabelCocktail3;
    public javax.swing.JPanel jPanel2;
    public javax.swing.JPanel jPanelListeDesCommandes;
    // End of variables declaration//GEN-END:variables
    
}
