package org.cocktail.corossol.client.nib;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.cocktail.application.palette.JButtonCocktail;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class CalendarNib extends org.cocktail.application.palette.JPanelCocktail {
	private JPanel jPanelCal;
	private JButtonCocktail jButtonCocktailFermer;
	private JButtonCocktail jButtonCocktailAnnuler;
	private JButtonCocktail jButtonCocktailOk;

	/**
	* Auto-generated main method to display this 
	* JPanel inside a new JFrame.
	*/
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new CalendarNib());
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	public CalendarNib() {
		super();
		initGUI();
	}
	
	private void initGUI() {
		try {
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.rowWeights = new double[] {0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0};
			thisLayout.rowHeights = new int[] {7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7};
			thisLayout.columnWeights = new double[] {0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0};
			thisLayout.columnWidths = new int[] {7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7};
			this.setLayout(thisLayout);
			setPreferredSize(new Dimension(300, 200));
			{
				jPanelCal = new JPanel();
				BoxLayout jPanelCalLayout = new BoxLayout(
					jPanelCal,
					javax.swing.BoxLayout.X_AXIS);
				jPanelCal.setLayout(jPanelCalLayout);
				this.add(jPanelCal, new GridBagConstraints(2, 1, 8, 9, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
			}
			{
				jButtonCocktailOk = new JButtonCocktail();
				this.add(getJButtonCocktailOk(), new GridBagConstraints(9, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailOk.setText("Valider");
			}
			{
				jButtonCocktailAnnuler = new JButtonCocktail();
				this.add(getJButtonCocktailAnnuler(), new GridBagConstraints(6, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailAnnuler.setText("Annuler");
			}
			{
				jButtonCocktailFermer = new JButtonCocktail();
				this.add(getJButtonCocktailFermer(), new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
				jButtonCocktailFermer.setText("Fermer");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JPanel getJPanelCal() {
		return jPanelCal;
	}
	
	public JButtonCocktail getJButtonCocktailOk() {
		return jButtonCocktailOk;
	}
	
	public JButtonCocktail getJButtonCocktailAnnuler() {
		return jButtonCocktailAnnuler;
	}
	
	public JButtonCocktail getJButtonCocktailFermer() {
		return jButtonCocktailFermer;
	}

}
