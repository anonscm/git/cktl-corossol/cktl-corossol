/* JTextFieldDate.java created by tsaivre on Wed 16-Jun-2004 */
package org.cocktail.corossol.client.nib;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;

import javax.swing.JTextField;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class JTextFieldDate extends com.webobjects.eointerface.swing.EOTextField
{
    JTextFieldDate() {
        super();

        this.addFocusListener(new FocusListenerJTextFieldDate());
        this.addActionListener(new ActionListenerJTextFieldDate());
    }

    public void setText(String text) {
        BigDecimal	nombre;
        try {
            NSTimestampFormatter formatter= new NSTimestampFormatter("%d/%m/%Y");
            NSTimestamp date = (NSTimestamp)formatter.parseObject(text);
        }
        catch(Exception e) {
            super.setText(this.mettreEnForme(""));
            return;
        }

        super.setText(this.mettreEnForme(text));
    }

    public String mettreEnForme(String text) {
        if ((text==null) || (text.equals("")))
            return "";
        return DateCtrl.dateCompletion(text);
    }

    public class FocusListenerJTextFieldDate implements FocusListener {
        public void focusGained(FocusEvent e) {
            ((JTextField)e.getSource()).selectAll();
        }
        public void focusLost(FocusEvent e) {
            if (e.isTemporary())
                return;
            ((JTextFieldDate)e.getSource()).setText(mettreEnForme(((JTextField)e.getSource()).getText()));
        }
    }

    public class ActionListenerJTextFieldDate implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ((JTextFieldDate)e.getSource()).setText(mettreEnForme(((JTextField)e.getSource()).getText()));
        }
    }
}
