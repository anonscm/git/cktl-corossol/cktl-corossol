/* JTextFieldNumber.java created by tsaivre on Wed 08-Oct-2003 */
package org.cocktail.corossol.client.nib;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;

import javax.swing.JTextField;

import com.webobjects.foundation.NSArray;

public class JTextFieldNumber extends com.webobjects.eointerface.swing.EOTextField
{
    protected int	nombresApresLaVirgule;
    protected boolean	negative;

    public JTextFieldNumber() {
        super();
        this.initObject(2, false);
    }

    public JTextFieldNumber(int nb) {
        super();
        this.initObject(nb, false);
    }

    public BigDecimal getValue() {
        try {
            return new BigDecimal(getText());
        }
        catch(Exception e) {
            return new BigDecimal("0.00");
        }
    }

    public void setValue(BigDecimal value) {
    	setText(mettreEnForme(value.toString()));
    }

    public JTextFieldNumber(boolean neg) {
        super();
        this.initObject(2, neg);
    }

    public JTextFieldNumber(int nb, boolean neg) {
        super();
        this.initObject(nb, neg);
    }

    protected void initObject(int nb, boolean neg) {
        if (nb<0)
            nb=2;

        nombresApresLaVirgule=nb;
        negative=neg;

//        this.addFocusListener(new FocusListenerJTextFieldNumber());
        this.addFocusListener(new FocusListenerJTextFieldNumber());
        this.addActionListener(new ActionListenerJTextFieldNumber());

        this.setText("0");
    }

    public void setText(String text) {
        BigDecimal	nombre;

        try {
            nombre=new BigDecimal(text);
            nombre=this.arrondir(nombre);
        }
        catch(Exception e) {
            super.setText(this.mettreEnForme("0"));
            return;
        }

        super.setText(this.mettreEnForme(nombre.toString()));
    }

    protected BigDecimal arrondir(BigDecimal nombre) {
        try {
            return nombre.setScale(nombresApresLaVirgule, BigDecimal.ROUND_HALF_UP);
        }
        catch(Exception e) {
            return new BigDecimal("0.00");
        }
    }

    public boolean negative() {
        return negative;
    }

    public int nombresApresLaVirgule() {
        return nombresApresLaVirgule;
    }

    private String mettreEnForme(String text) {
        NSArray	larray=StringCtrl.componentsSeparatedByString(text, "-");

        if ((!negative) && (larray.count()>1))
            text=(String)larray.objectAtIndex(1);

        text=StringCtrl.replace(text, ",", ".");
        larray=StringCtrl.componentsSeparatedByString(text, ".");

        if (text.equals(""))
            text="0";

        if (nombresApresLaVirgule==0)
            return ((String)larray.objectAtIndex(0));

        if (larray.count()==1)
            return this.completer(text+".","0",nombresApresLaVirgule);

        if (((String)larray.objectAtIndex(0)).length()==0)
            text="0".concat(text);

        if (((String)larray.objectAtIndex(0)).equals("-"))
            text="-0".concat((String)larray.objectAtIndex(1));

        return this.completer(text,"0",nombresApresLaVirgule-((String)larray.objectAtIndex(1)).length());
    }

    protected String completer(String chaine, String complement, int nb) {
        if (nb<0)
            return chaine;

        for (int i=0; i<nb; i++)
            chaine=chaine.concat(complement);
        return chaine;
    }

    private class FocusListenerJTextFieldNumber implements FocusListener {
        public void focusGained(FocusEvent e) {
            ((JTextField)e.getSource()).selectAll();
        }
        public void focusLost(FocusEvent e) {
            if (e.isTemporary())
                return;
            ((JTextFieldNumber)e.getSource()).setText(mettreEnForme(((JTextField)e.getSource()).getText()));
        }
    }

    private class ActionListenerJTextFieldNumber implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ((JTextFieldNumber)e.getSource()).setText(mettreEnForme(((JTextField)e.getSource()).getText()));
        }
    }
}
