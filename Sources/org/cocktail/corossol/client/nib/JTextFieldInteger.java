/* JTextFieldInteger.java created by tsaivre on Wed 08-Oct-2003 */
package org.cocktail.corossol.client.nib;

import java.math.BigInteger;

public class JTextFieldInteger extends com.webobjects.eointerface.swing.EOTextField {
    protected int	nombreChiffres;
    protected boolean	negative;

    JTextFieldInteger() {
        super();
        this.initObject(-1, false);
    }

    JTextFieldInteger(int nb) {
        super();
        this.initObject(nb, false);
    }

    JTextFieldInteger(boolean neg) {
        super();
        this.initObject(-1, neg);
    }

    JTextFieldInteger(int nb, boolean neg) {
        super();
        this.initObject(nb, neg);
    }

    protected void initObject(int nb, boolean neg) {
        // nb=-1 = pas de limitation !!
        if (nb<=0)
            nb=-1;

        nombreChiffres=nb;
        negative=neg;
    }

    public boolean negative() {
        return negative;
    }

    public int nombreChiffres() {
        return nombreChiffres;
    }

    public void setText(String text) {
        BigInteger nombre;

        try {
            nombre=new BigInteger(text);
        }
        catch(Exception e) {
            super.setText("");
            return;
        }

        super.setText(text);
    }
}
