/*
 * TitreNib.java
 *
 */

package org.cocktail.corossol.client.nib;

import java.awt.BorderLayout;
import java.awt.Color;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.corossol.client.eof.metier.EOBordereau;
import org.cocktail.corossol.client.eof.metier.EOTitre;
import org.cocktail.corossol.client.eof.metier.EOVTitreRecette;
import org.cocktail.corossol.client.zutil.wo.table.TableSorter;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTable;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModel;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTableModelColumn;
import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public class TitreNib extends JPanelCocktail {

	private EODisplayGroup eod;
	private ZEOTable myEOTable;
	private final Color tableBackgroundColor=new Color(230, 230, 230);
	private final Color tableSelectionBackgroundColor=new Color(127,127,127);

    /** Creates new form TitreNib */
    public TitreNib() {
        initComponents();
        initTableView();
    }
    
	public void initTableView() {
		Vector myCols = new Vector();
		eod = new EODisplayGroup();
		
		ZEOTableModelColumn colExer = new ZEOTableModelColumn(eod, EOVTitreRecette.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, "Exercice", 15);
		colExer.setAlignment(SwingConstants.CENTER);
		myCols.add(colExer);

		ZEOTableModelColumn colUB = new ZEOTableModelColumn(eod, EOVTitreRecette.GES_CODE_KEY, "UB", 25);
		colUB.setAlignment(SwingConstants.CENTER);
		myCols.add(colUB);

		ZEOTableModelColumn colNumero = new ZEOTableModelColumn(eod, EOVTitreRecette.TIT_NUMERO_KEY, "Numéro", 25);
		colNumero.setAlignment(SwingConstants.CENTER);
		colNumero.setFormatDisplay(CocktailConstantes.FORMAT_NUMBER);
		colNumero.setColumnClass(Integer.class);
		myCols.add(colNumero);

		ZEOTableModelColumn colBordereau = new ZEOTableModelColumn(eod, EOVTitreRecette.BORDEREAU_KEY + "." + EOBordereau.BOR_NUM_KEY, "Bordereau", 25);
		colBordereau.setAlignment(SwingConstants.CENTER);
		colBordereau.setColumnClass(Integer.class);
		myCols.add(colBordereau);

		ZEOTableModelColumn colLibelle = new ZEOTableModelColumn(eod, EOVTitreRecette.TIT_LIBELLE_KEY, "Libelle", 120);
		colLibelle.setAlignment(SwingConstants.LEFT);
		myCols.add(colLibelle);

		ZEOTableModelColumn colHT = new ZEOTableModelColumn(eod, EOVTitreRecette.TIT_HT_KEY, "HT", 50);
		colHT.setAlignment(SwingConstants.RIGHT);
		colHT.setFormatDisplay(CocktailConstantes.FORMAT_DECIMAL);
		colHT.setColumnClass(BigDecimal.class);
		myCols.add(colHT);

		ZEOTableModelColumn colTTC = new ZEOTableModelColumn(eod, EOVTitreRecette.TIT_TTC_KEY, "TTC", 50);
		colTTC.setAlignment(SwingConstants.RIGHT);
		colTTC.setFormatDisplay(CocktailConstantes.FORMAT_DECIMAL);
		colTTC.setColumnClass(BigDecimal.class);
		myCols.add(colTTC);
		
		// Reduction
		ZEOTableModelColumn colMntReduction = new ZEOTableModelColumn(eod, EOVTitreRecette.MONTANT_REDUCTION_KEY, "Réductions", 50);
		colMntReduction.setAlignment(SwingConstants.RIGHT);
		colMntReduction.setFormatDisplay(CocktailConstantes.FORMAT_DECIMAL);
		colMntReduction.setColumnClass(BigDecimal.class);
		myCols.add(colMntReduction);

		ZEOTableModelColumn colMntDispo = new ZEOTableModelColumn(eod, EOVTitreRecette.MONTANT_DISPONIBLE_KEY, "Disponible", 50);
		colMntDispo.setAlignment(SwingConstants.RIGHT);
		colMntDispo.setFormatDisplay(CocktailConstantes.FORMAT_DECIMAL);
		colMntDispo.setColumnClass(BigDecimal.class);
		myCols.add(colMntDispo);

		TableSorter myTableSorter = new TableSorter(new ZEOTableModel(eod, myCols));
		myEOTable = new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

		myEOTable.setBackground(tableBackgroundColor);
		myEOTable.setSelectionBackground(tableSelectionBackgroundColor);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		getJPanelTbvTitres().setLayout(new BorderLayout());
		getJPanelTbvTitres().add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	public ZEOTable getMaTable() {
		return myEOTable;
	}

	public EOTitre titreSelectionne() {
		EOVTitreRecette selected = (EOVTitreRecette) myEOTable.getSelectedObject();
		if (selected != null) {
			return selected.titre();
		} else {
			return null;
		}
	}

	public void setVTitresRecette(NSArray data) {
		if (eod == null) {
			return;
		}
		eod.setObjectArray(data);
		eod.updateDisplayedObjects();
		refresh();
	}

	public void refresh() {
		if (myEOTable == null) {
			return;
		}
		myEOTable.updateData();
	}

    public JPanel getJPanelTbvTitres() {
		return jPanelTbvTitres;
	}

	public void setJPanelTbvTitres(JPanel tbv) {
		this.jPanelTbvTitres = tbv;
	}

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelCocktail1 = new javax.swing.JLabel();
        jLabelCocktail8 = new javax.swing.JLabel();
        jButtonCocktailAnnuler = new javax.swing.JButton();
        jButtonCocktailValider = new javax.swing.JButton();
        jTextFieldExercice = new javax.swing.JTextField();
        jTextFieldTitLibelle = new javax.swing.JTextField();
        jButtonRechercher = new javax.swing.JButton();
        jTextFieldUb = new javax.swing.JTextField();
        jLabelCocktail9 = new javax.swing.JLabel();
        jTextFieldTitNumero = new javax.swing.JTextField();
        jLabelCocktail10 = new javax.swing.JLabel();
        jTextFieldBorNumero = new javax.swing.JTextField();
        jLabelCocktail11 = new javax.swing.JLabel();
        jTextFieldFournisseur = new javax.swing.JTextField();
        jLabelCocktail2 = new javax.swing.JLabel();
        jLabelCocktail12 = new javax.swing.JLabel();
        jTextFieldHT = new javax.swing.JTextField();
        jLabelCocktail13 = new javax.swing.JLabel();
        jTextFieldTTC = new javax.swing.JTextField();
        jPanelTbvTitres = new javax.swing.JPanel();

//        setPreferredSize(new java.awt.Dimension(900, 500));
//        setSize(new java.awt.Dimension(900, 500));

        jLabelCocktail1.setText("Libelle :");

        jLabelCocktail8.setText("Exercice :");

        jButtonCocktailAnnuler.setText("Annuler");
        jButtonCocktailAnnuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCocktailAnnulerActionPerformed(evt);
            }
        });

        jButtonCocktailValider.setText("Valider");

        jTextFieldExercice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldExerciceActionPerformed(evt);
            }
        });

        jButtonRechercher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRechercherActionPerformed(evt);
            }
        });

        jLabelCocktail9.setText("UB :");

        jLabelCocktail10.setText("Numéro :");

        jLabelCocktail11.setText("Numéro bordereau :");

        jLabelCocktail2.setText("Fournisseur :");

        jLabelCocktail12.setText("HT :");

        jTextFieldHT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldHTActionPerformed(evt);
            }
        });

        jLabelCocktail13.setText("TTC :");

        org.jdesktop.layout.GroupLayout jPanelTbvTitresLayout = new org.jdesktop.layout.GroupLayout(jPanelTbvTitres);
        jPanelTbvTitres.setLayout(jPanelTbvTitresLayout);
        jPanelTbvTitresLayout.setHorizontalGroup(
            jPanelTbvTitresLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 0, Short.MAX_VALUE)
        );
        jPanelTbvTitresLayout.setVerticalGroup(
            jPanelTbvTitresLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 311, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(jButtonCocktailAnnuler, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 134, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButtonCocktailValider, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 136, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanelTbvTitres, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(layout.createSequentialGroup()
                                .add(jLabelCocktail8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 82, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jTextFieldExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 59, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(18, 18, 18)
                                .add(jLabelCocktail9)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jTextFieldUb, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 59, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(18, 18, 18)
                                .add(jLabelCocktail10)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jTextFieldTitNumero, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 59, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(18, 18, 18)
                                .add(jLabelCocktail11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 125, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jTextFieldBorNumero, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 59, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createSequentialGroup()
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                    .add(jLabelCocktail1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jLabelCocktail2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(layout.createSequentialGroup()
                                        .add(jLabelCocktail12)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jTextFieldHT, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 102, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(18, 18, 18)
                                        .add(jLabelCocktail13)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jTextFieldTTC, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 106, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(jButtonRechercher, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 29, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(jTextFieldTitLibelle)
                                    .add(jTextFieldFournisseur))))
                        .add(0, 285, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail8)
                    .add(jTextFieldExercice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelCocktail9)
                    .add(jTextFieldUb, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelCocktail10)
                    .add(jTextFieldTitNumero, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelCocktail11)
                    .add(jTextFieldBorNumero, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail1)
                    .add(jTextFieldTitLibelle, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelCocktail2)
                    .add(jTextFieldFournisseur, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabelCocktail12)
                        .add(jTextFieldHT)
                        .add(jLabelCocktail13)
                        .add(jTextFieldTTC))
                    .add(jButtonRechercher, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelTbvTitres, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonCocktailAnnuler)
                    .add(jButtonCocktailValider))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCocktailAnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCocktailAnnulerActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jButtonCocktailAnnulerActionPerformed

    private void jButtonRechercherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRechercherActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_jButtonRechercherActionPerformed

    private void jTextFieldExerciceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldExerciceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldExerciceActionPerformed

    private void jTextFieldHTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldHTActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldHTActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButtonCocktailAnnuler;
    public javax.swing.JButton jButtonCocktailValider;
    public javax.swing.JButton jButtonRechercher;
    public javax.swing.JLabel jLabelCocktail1;
    public javax.swing.JLabel jLabelCocktail10;
    public javax.swing.JLabel jLabelCocktail11;
    public javax.swing.JLabel jLabelCocktail12;
    public javax.swing.JLabel jLabelCocktail13;
    public javax.swing.JLabel jLabelCocktail2;
    public javax.swing.JLabel jLabelCocktail8;
    public javax.swing.JLabel jLabelCocktail9;
    public javax.swing.JPanel jPanelTbvTitres;
    public javax.swing.JTextField jTextFieldBorNumero;
    public javax.swing.JTextField jTextFieldExercice;
    public javax.swing.JTextField jTextFieldFournisseur;
    public javax.swing.JTextField jTextFieldHT;
    public javax.swing.JTextField jTextFieldTTC;
    public javax.swing.JTextField jTextFieldTitLibelle;
    public javax.swing.JTextField jTextFieldTitNumero;
    public javax.swing.JTextField jTextFieldUb;
    // End of variables declaration//GEN-END:variables
    
    public javax.swing.JTextField getjTextFieldExercice() {
		return jTextFieldExercice;
	}

	public void setjTextFieldExercice(javax.swing.JTextField jTextFieldExercice) {
		this.jTextFieldExercice = jTextFieldExercice;
	}

    public javax.swing.JTextField getjTextFieldFournisseur() {
		return jTextFieldFournisseur;
	}

	public void setjTextFieldFournisseur(
			javax.swing.JTextField jTextFieldFournisseur) {
		this.jTextFieldFournisseur = jTextFieldFournisseur;
	}

    public javax.swing.JTextField getjTextFieldHT() {
		return jTextFieldHT;
	}

	public void setjTextFieldHT(javax.swing.JTextField jTextFieldHT) {
		this.jTextFieldHT = jTextFieldHT;
	}

    public javax.swing.JTextField getjTextFieldTTC() {
		return jTextFieldTTC;
	}

	public void setjTextFieldTTC(javax.swing.JTextField jTextFieldTTC) {
		this.jTextFieldTTC = jTextFieldTTC;
	}

    public javax.swing.JTextField getjTextFieldTitLibelle() {
		return jTextFieldTitLibelle;
	}

	public void setjTextFieldTitLibelle(javax.swing.JTextField jTextFieldTitLibelle) {
		this.jTextFieldTitLibelle = jTextFieldTitLibelle;
	}

    public javax.swing.JTextField getjTextFieldTitNumero() {
		return jTextFieldTitNumero;
	}

	public void setjTextFieldTitNumero(javax.swing.JTextField jTextFieldTitNumero) {
		this.jTextFieldTitNumero = jTextFieldTitNumero;
	}

    public javax.swing.JTextField getjTextFieldUb() {
		return jTextFieldUb;
	}

	public void setjTextFieldUb(javax.swing.JTextField jTextFieldUb) {
		this.jTextFieldUb = jTextFieldUb;
	}

	public javax.swing.JTextField getjTextFieldBorNumero() {
		return jTextFieldBorNumero;
	}

	public void setjTextFieldBorNumero(javax.swing.JTextField jTextFieldBorNumero) {
		this.jTextFieldBorNumero = jTextFieldBorNumero;
	}

	public javax.swing.JButton getJButtonCocktailAnnuler() {
		return jButtonCocktailAnnuler;
	}

	public void setJButtonCocktailAnnuler(javax.swing.JButton buttonCocktailAnnuler) {
		jButtonCocktailAnnuler = buttonCocktailAnnuler;
	}

	public javax.swing.JButton getJButtonCocktailRechercher() {
		return jButtonRechercher;
	}

	public void setJButtonCocktailRechercher(javax.swing.JButton buttonCocktailRechercher) {
		jButtonRechercher = buttonCocktailRechercher;
	}

	public javax.swing.JButton getJButtonCocktailValider() {
		return jButtonCocktailValider;
	}

	public void setJButtonCocktailValider(javax.swing.JButton buttonCocktailValider) {
		jButtonCocktailValider = buttonCocktailValider;
	}

}
