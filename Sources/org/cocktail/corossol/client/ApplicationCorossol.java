/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)



package org.cocktail.corossol.client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Calendar;
import java.util.TimeZone;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.InterfaceApplicationCocktail;
import org.cocktail.application.client.MainNibControlerInterfaceController;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.eof.EOUtilisateurFonction;
import org.cocktail.application.client.finder.FinderGrhum;
import org.cocktail.application.client.tools.ToolsCocktailObjectMessage;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.corossol.client.eof.metier.EOVCommande;
import org.cocktail.corossol.client.finder.FinderEngagementCtrlPlanco;
import org.cocktail.corossol.client.nib.MainNib;
import org.cocktail.corossol.client.nibctrl.CoefficientAmortissementCtrl;
import org.cocktail.corossol.client.nibctrl.MainNibCtrl;

import com.webobjects.eoapplication.EOAction;
import com.webobjects.eoapplication.EOFrameController;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimeZone;


public class ApplicationCorossol extends ApplicationCocktail implements InterfaceApplicationCocktail{

	private static final String COROSOL_GESTION_INVENTAIRE = "Corossol - Gestion de l'inventaire ...";
	private MainNib 		monMainNib=null;
	private MainNibCtrl		monMainNibCtrl=null;
	private CoefficientAmortissementCtrl monCoefficientAmortissementCtrl=null;

	private NSMutableArray mesPrivilegesDepense =null;


	public ToolsCocktailObjectMessage observeurCommandeSelection=null;

	EOVCommande currentCommande = null;
	NSMutableArray currentEgagements = null;

	public ApplicationCorossol() {
		super();
		setWithLogs(false);
		setNAME_APP("COROSSOL");
		setTYAPSTRID("COROSSOL");
//		creation d un observeur pour le changement de commande		
		setObserveurCommandeSelection (new ToolsCocktailObjectMessage(this));
		ApplicationCorossol.fixWoBug_responseToMessage(new String[]{ "Inventaire","InventaireComptable","CleInventaireComptable"});
	}

	protected NSArray defaultActions() {
		NSMutableArray array=new NSMutableArray(super.defaultActions());
		array.addObject(EOAction.actionForObject("afficherGestionCoeffecientsAmortissementDegressif", "Outils/Gestion coefs amort. degressif", null, null, null, null,
				EOAction.HelpCategoryPriority, EOAction.InfoActionPriority,this));
		return array;
	}
	
	public void afficherGestionCoeffecientsAmortissementDegressif() {   
		if (monCoefficientAmortissementCtrl==null)
			monCoefficientAmortissementCtrl=new CoefficientAmortissementCtrl(this);
		monCoefficientAmortissementCtrl.afficherFenetre();
	}
     
	/**
	  * 
	  * @see org.cocktail.application.client.ApplicationCocktail#accesOk()
	  * Bug parce qu'il force le TIME_ZONE du client à "WEST"
	  */
	@Override
	public void accesOk() {
		    super.accesOk();
			TimeZone defaultTZ = Calendar.getInstance().getTimeZone();
			TimeZone.setDefault(defaultTZ);
			NSTimeZone.setDefault(defaultTZ);
	}
	
	public void creationDesPrivileges() {
		super.creationDesPrivileges();
		setMesPrivilegesDepense(new NSMutableArray());

		for (int i = 0; i < getMesUtilisateurFonction().count(); i++) {
			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INVTITRE)) {
				addPrivileges(new Privileges(Privileges.INVTITRE));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}

			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INV1)) {
				addPrivileges(new Privileges(Privileges.INV1));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}

			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INV2)) {
				addPrivileges(new Privileges(Privileges.INV2));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}

			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INV3)) {
				addPrivileges(new Privileges(Privileges.INV3));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}

			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INV4)) {
				addPrivileges(new Privileges(Privileges.INV4));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}

			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INV5)) {
				addPrivileges(new Privileges(Privileges.INV5));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}

			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INV6)) {
				addPrivileges(new Privileges(Privileges.INV6));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}
			
			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INV7)) {
				addPrivileges(new Privileges(Privileges.INV7));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}
			
			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INVREGUL)) {
				addPrivileges(new Privileges(Privileges.INVREGUL));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}
			
			if (((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne().equals(Privileges.INVAMORT)) {
				addPrivileges(new Privileges(Privileges.INVAMORT));
				getMesPrivilegesDepense().addObject(new Privileges(((EOUtilisateurFonction)getMesUtilisateurFonction().objectAtIndex(i)).fonction().fonIdInterne()));
			}
			
		}
	}
	
	public void initFindersAccesEtDroits() {
		super.initFindersAccesEtDroits();
		setMesUtilisateurFonction(FinderGrhum.findUtilisateurFonctions(this, getCurrentUtilisateur(), getCurrentTypeApplication()));
		setMesTypeApplicationFonctions(FinderGrhum.findTypeApplicationfonctions(this,getCurrentTypeApplication()));
	}
	
	public void finishInitialization() {
		try {
			super.finishInitialization();

			try {
				compareJarVersionsClientAndServer();
			} catch (Exception e) {
				e.printStackTrace();
				this.fenetreDeDialogueInformation(e.getMessage());
				quit();
			}

		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	// Initialisation du panel principal de l'application
	public void initMonApplication() {
		super.initMonApplication();
		try {
			if (getMonMainNibCtrl() == null) {
				setMonMainNibCtrl(new MainNibCtrl(this, 100, 100, 300, 550));
				setMonMainNib(new MainNib());
				getMonMainNibCtrl().creationFenetre(getMonMainNib(), COROSOL_GESTION_INVENTAIRE);
				getMonMainNibCtrl().setInfos(this.getVersionApplication());

				// getMonMainNibCtrl().afficherFenetre();
				getMonMainNibCtrl().getFrameMain().pack();
				setExercice(getCurrentExercice());
				setMainComponentCocktail(COROSOL_GESTION_INVENTAIRE, getMonMainNibCtrl().currentNib, 100, 100, getMonMainNibCtrl().getFrameMain().getWidth(),
						getMonMainNibCtrl().getFrameMain().getHeight());
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setMainComponentCocktail(String frameName, JPanelCocktail nib, int x, int y, int w, int h) {
		MainNibControlerInterfaceController main = new MainNibControlerInterfaceController(getAppEditingContext());
		EOFrameController.runControllerInNewFrame(main, frameName);
		main.view.setLayout(new BorderLayout());
		main.view.add(nib);
		main.component().getTopLevelAncestor().setBounds(x, y, w + 40, h + 60);
		addLesPanelsModal(main);
	}

	public static final void fixWoBug_responseToMessage(final String[] list) {
		for (int i = 0; i < list.length; i++) {
			EOClassDescription.classDescriptionForEntityName(list[i]);
			System.out.println(list[i] +" OK");
		}
	} 

	public void setExercice(EOExercice exer){
		setCurrentExercice(exer);
		getMonMainNib().getJLabelExercice().setText(exer.exeExercice().toString());
	}

	public EOUtilisateur getUtilisateur(){
		return getCurrentUtilisateur();
	}

	void setMonMainNib(MainNib monMainNib) {
		this.monMainNib = monMainNib;
	}

	MainNib getMonMainNib() {
		return monMainNib;
	}

	void setMonMainNibCtrl(MainNibCtrl monMainNibCtrl) {
		this.monMainNibCtrl = monMainNibCtrl;
	}

	public MainNibCtrl getMonMainNibCtrl() {
		return monMainNibCtrl;
	}

	private void setMesPrivilegesDepense(NSMutableArray mesPrivilegesDepense) {
		this.mesPrivilegesDepense = mesPrivilegesDepense;
	}

	public NSMutableArray getMesPrivilegesDepense() {
		return mesPrivilegesDepense;
	}

	// MODIF POUR  GERER DES PROBLEMES VIA DES EXCEPTIONS
	public void executeStoredProcedure(String proc, NSDictionary dico) throws Exception{
		System.out.println(proc+" : "+dico);
		MyToolsCocktailEOF tool=new MyToolsCocktailEOF(this);
		NSDictionary dicoRetour=tool.executeStoredProcedure(proc, dico);
		if (!((Boolean)dicoRetour.objectForKey("ok")).booleanValue())
			throw new Exception("PROBLEME LORS DE L'EXECUTION DE LA PROCEDURE : "+proc+"\n"+dicoRetour.objectForKey("message"));
	}

	public NSDictionary returnValuesForLastStoredProcedureInvocation(){
		return getToolsCocktailEOF().returnValuesForLastStoredProcedureInvocation();
	}

	public ToolsCocktailObjectMessage getObserveurCommandeSelection() {
		return observeurCommandeSelection; 
	}

	public void setObserveurCommandeSelection(ToolsCocktailObjectMessage observeurCommandeSelection) {
		this.observeurCommandeSelection = observeurCommandeSelection;
	}

	public EOVCommande getCurrentCommande() {
		return currentCommande;
	}

	public void setCurrentCommande(EOVCommande commande) {	
		currentCommande=commande;
	  if (commande!=null) {	
		 getMonMainNib().getJLabelCocktailCde().setText(commande.commandeLibelle());
		// fetcher les engagements planco ctrl
		 currentEgagements = FinderEngagementCtrlPlanco.findLesEngagementsCtrlPlanco(this, commande);
	  } else {
		  currentEgagements=null;
	  }
		getMonMainNibCtrl().changementCommande();
	}
}
