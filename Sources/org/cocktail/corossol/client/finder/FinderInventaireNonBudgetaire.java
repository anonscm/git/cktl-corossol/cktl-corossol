package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderInventaireNonBudgetaire extends Finder {

	public FinderInventaireNonBudgetaire(boolean arg0) {
		super(arg0);
		
	}

	static public NSMutableArrayDisplayGroup findLesInventaireNonBudgetaire(ApplicationCocktail app, EOExercice exercice) {
		NSMutableArray leSort = new NSMutableArray();

		// le sort 
		leSort.addObject(EOSortOrdering.sortOrderingWithKey(EOInventaireNonBudgetaire.EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY, 
				EOSortOrdering.CompareCaseInsensitiveAscending));
		leSort.addObject(EOSortOrdering.sortOrderingWithKey(EOInventaireNonBudgetaire.INVENTAIRE_COMPTABLE_KEY+"."+
				EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY, 
				EOSortOrdering.CompareCaseInsensitiveAscending));

		//leSort.addObject(EOSortOrdering.sortOrderingWithKey(InventaireNonBudgetaire.INB_DATE_ACQUISITION_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));

		NSArray resultat=EOSortOrdering.sortedArrayUsingKeyOrderArray(findLocal(app, null, 
				EOQualifier.qualifierWithQualifierFormat(EOInventaireNonBudgetaire.INVENTAIRE_COMPTABLE_KEY+"!=nil", null)), leSort);
		
		return new NSMutableArrayDisplayGroup(resultat); 
	}
	
    static public NSMutableArrayDisplayGroup findLocal(ApplicationCocktail app, NSArray leSort, EOQualifier aQualifier) {
        //try {
        	EOFetchSpecification myFetch;
            myFetch = new EOFetchSpecification(EOInventaireNonBudgetaire.ENTITY_NAME, aQualifier, leSort);
            myFetch.setRefreshesRefetchedObjects(true);
            myFetch.setUsesDistinct(true);
            
            NSArray keyPaths=new NSArray(new Object[]{EOInventaireNonBudgetaire.INVENTAIRE_COMPTABLE_KEY, 
            		EOInventaireNonBudgetaire.INVENTAIRE_COMPTABLE_KEY+"."+EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY,
            		EOInventaireNonBudgetaire.ORGAN_KEY, EOInventaireNonBudgetaire.PLAN_COMPTABLE_KEY});
            myFetch.setPrefetchingRelationshipKeyPaths(keyPaths);
            
            return new NSMutableArrayDisplayGroup(new NSArray(app.getAppEditingContext().objectsWithFetchSpecification(myFetch)));
        /*} catch (Throwable e) {
            e.printStackTrace();
            return null;
        }*/

    }

}
