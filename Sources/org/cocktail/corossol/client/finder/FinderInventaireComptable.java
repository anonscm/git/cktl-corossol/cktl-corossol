package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderInventaireComptable extends Finder {

	public FinderInventaireComptable(boolean withLogs) {
		super(withLogs);
		
	}

	public static NSMutableArrayDisplayGroup findLesClesInventaireComptables(ApplicationCocktail app, EOCleInventaireComptable invccle) {
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOCleInventaireComptable.CLIC_COMP_KEY+"=%@ AND "+EOCleInventaireComptable.EXERCICE_KEY+
				"=%@ AND "+EOCleInventaireComptable.PLAN_COMPTABLE_KEY+"=%@",
				new NSArray(new Object[]{ invccle.clicComp(),invccle.exercice(),invccle.planComptable()}));

		return Finder.find(app, EOCleInventaireComptable.ENTITY_NAME, leSort, aQualifier);
	}

	public static NSMutableArrayDisplayGroup findAllInventaireComptables(ApplicationCocktail app) {
		NSArray leSort = null;
		
		return Finder.find(app, EOCleInventaireComptable.ENTITY_NAME, leSort, null);
	}
}
