package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EOAmortissement;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;

public class FinderAmortissement extends Finder {

	public FinderAmortissement(boolean withLogs) {
		super(withLogs);
	}

	public static NSMutableArrayDisplayGroup findLesAmortissements(ApplicationCocktail app, EOInventaireComptable invc) {
		EOQualifier qual = new EOKeyValueQualifier(EOAmortissement.INVENTAIRE_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, invc);
		return Finder.find(app, EOAmortissement.ENTITY_NAME, null, qual);
	}

}
