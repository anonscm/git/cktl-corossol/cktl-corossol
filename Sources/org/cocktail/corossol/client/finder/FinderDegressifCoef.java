package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EODegressif;
import org.cocktail.corossol.client.eof.metier.EODegressifCoef;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FinderDegressifCoef extends Finder {

	public FinderDegressifCoef(boolean withLogs) {
		super(withLogs);
	}

	static public EODegressifCoef findLeCoefficient(ApplicationCocktail app, Number duree, NSTimestamp date) {

		NSMutableArrayDisplayGroup array=FinderDegressif.find(app, date);
		if (array==null || array.count()!=1)
			return null;
		EODegressif degressif=(EODegressif)array.objectAtIndex(0);
		
		// le sort 
		NSArray leSort=null;
		
		//le qualifier
		EOQualifier aQualifier=EOQualifier.qualifierWithQualifierFormat(EODegressifCoef.DGCO_DUREE_MIN_KEY+"<=%@ and "+EODegressifCoef.DGCO_DUREE_MAX_KEY+">=%@ and "+
				EODegressifCoef.DEGRESSIF_KEY+"=%@", new NSArray(new Object[]{duree, duree, degressif}));
		NSArray result=Finder.find(app, EODegressifCoef.ENTITY_NAME, leSort, aQualifier);
		
		if (result==null ||result.count()!=1)
			return null;
		return (EODegressifCoef)result.objectAtIndex(0);
	}
}
