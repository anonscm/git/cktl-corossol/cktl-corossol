package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EOVCommande;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderCommande extends Finder {

	public FinderCommande(boolean arg0) {
		super(arg0);
		
	}

	static public NSMutableArrayDisplayGroup findLesCommandes(ApplicationCocktail app,Number inf,Number sup,boolean classe2uniquement) {
		NSMutableArray arrayQualifier = new NSMutableArray();
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOVCommande.EXERCICE_KEY+"=%@",new NSArray(app.getCurrentExercice())));
		
		if (inf!=null && sup!=null && inf.intValue()>sup.intValue())
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOVCommande.COMM_NUMERO_KEY+"=%@",new NSArray(inf)));
		else
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOVCommande.COMM_NUMERO_KEY+">=%@ AND "+EOVCommande.COMM_NUMERO_KEY+"<=%@",
					new NSArray(new Object[]{inf,sup})));
		if (classe2uniquement)
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOVCommande.PCO_NUM_KEY+" caseinsensitivelike %@",new NSArray("2*")));
		
		return Finder.find(app, EOVCommande.ENTITY_NAME, sort(), new EOAndQualifier(arrayQualifier));
	}

	private static NSArray sort() {
		NSMutableArray array=new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOVCommande.COMM_NUMERO_KEY, EOSortOrdering.CompareDescending));
		return array;
	}
}
