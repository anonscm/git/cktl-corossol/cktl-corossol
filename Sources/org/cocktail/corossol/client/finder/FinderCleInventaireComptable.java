package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireSortie;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOVCommande;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderCleInventaireComptable extends Finder {

	public FinderCleInventaireComptable(boolean withLogs) {
		super(withLogs);
		
	}

	static public NSMutableArrayDisplayGroup findLesInventaireComptables(ApplicationCocktail app, String critereNumeroInventaire) {
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY+" caseInsensitiveLike %@",
				new NSArray(new Object[]{ "*"+critereNumeroInventaire+"*"}));
		return Finder.find(app, EOCleInventaireComptable.ENTITY_NAME, leSort, aQualifier);
	}

	static public NSMutableArrayDisplayGroup findLesInventaireComptables(ApplicationCocktail app, String critereNumeroInventaire, 
			String critereNumeroCommande) {
		NSArray leSort = null;
		NSMutableArray andQualifier=new NSMutableArray();

		// le sort 

		//le qualifier
		if (critereNumeroInventaire!=null && !critereNumeroInventaire.equals(""))
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY+" caseInsensitiveLike %@",
					new NSArray(new Object[]{ "*"+critereNumeroInventaire+"*"})));

		if (critereNumeroCommande!=null && !critereNumeroCommande.equals(""))
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOCleInventaireComptable.INVENTAIRE_COMPTABLES_KEY+"."+
					EOInventaireComptable.INVENTAIRES_KEY+"."+EOInventaire.LIVRAISON_DETAIL_KEY+"."+EOLivraisonDetail.LIVRAISON_KEY+"."+
					EOLivraison.COMMANDE_KEY+"."+EOVCommande.COMM_NUMERO_KEY+"=%@", new NSArray(new Integer(critereNumeroCommande))));
		
		return Finder.find(app, EOCleInventaireComptable.ENTITY_NAME, leSort, new EOAndQualifier(andQualifier));
	}

	static public EOCleInventaireComptable findInventaireComptable(ApplicationCocktail app, Integer clicId) {
		//le qualifier
		EOQualifier aQualifier = EOQualifier.qualifierWithQualifierFormat(EOCleInventaireComptable.CLIC_ID_KEY+"=%@", new NSArray(clicId));
		try {
			return (EOCleInventaireComptable)Finder.find(app, EOCleInventaireComptable.ENTITY_NAME, null, aQualifier).objectAtIndex(0);
		} catch (Exception e) {
			return null;
		}
	}

	static public NSMutableArrayDisplayGroup findAllInventaireComptables(ApplicationCocktail app) {
		NSArray leSort = null;
		
		// le sort 
		return Finder.find(app, EOCleInventaireComptable.ENTITY_NAME, leSort, null);
	}
}
