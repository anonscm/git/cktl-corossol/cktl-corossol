package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOPlanComptableAmo;
import org.cocktail.application.client.eof.EOPlancoAmortissement;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOOrigineFinancement;
import org.cocktail.corossol.client.eof.metier.EOVCommande;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderComptable extends Finder {

	public FinderComptable(boolean arg0) {
		super(arg0);
		
	}

	static public NSMutableArrayDisplayGroup findLesBiensAttente(ApplicationCocktail app, EOVCommande commande) {
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(
				EOInventaire.LIVRAISON_DETAIL_KEY+"."+EOLivraisonDetail.LIVRAISON_KEY+"."+EOLivraison.COMMANDE_KEY+"=%@ and "+
				EOInventaire.INVENTAIRE_COMPTABLE_KEY+"."+EOInventaireComptable.INVC_MONTANT_ACQUISITION_KEY+"=null", new NSArray(commande));

		return Finder.find(app, EOInventaire.ENTITY_NAME, leSort, aQualifier);
	}

	static public NSMutableArrayDisplayGroup findLesInventairesComptableDuneCleInventaire(ApplicationCocktail app,EOCleInventaireComptable cle) {
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"=%@", new NSArray(cle));

		return Finder.find(app, EOInventaireComptable.ENTITY_NAME, leSort, aQualifier);
	}

	static public NSMutableArrayDisplayGroup findLesInventairesComptable(ApplicationCocktail app) {
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOInventaireComptable.EXERCICE_KEY+"=%@", new NSArray(app.getCurrentExercice()));

		return Finder.find(app, EOInventaireComptable.ENTITY_NAME, leSort, aQualifier);
	}

	static public NSMutableArrayDisplayGroup findLesInventairesComptableExerUbCr(ApplicationCocktail app, EOExercice exer, String ub, String cr ) {
		// le sort 
		NSArray leSort = null;

		//le qualifier
		NSMutableArray arrayQualifier=new NSMutableArray();
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireComptable.EXERCICE_KEY+"=%@", new NSArray(exer)));
		if (ub!=null && !ub.equals(""))
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireComptable.INVENTAIRES_KEY+"."+
					EOInventaire.LIVRAISON_DETAIL_KEY+"."+EOLivraisonDetail.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY+" caseinsensitivelike %@", new NSArray("*"+ub+"*")));
		if (cr!=null && !cr.equals(""))
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireComptable.INVENTAIRES_KEY+"."+
					EOInventaire.LIVRAISON_DETAIL_KEY+"."+EOLivraisonDetail.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY+" caseinsensitivelike %@", new NSArray("*"+cr+"*")));

		// 	System.out.println("actionImprimer aQualifier"+aQualifier.toString());
		return Finder.find(app, EOInventaireComptable.ENTITY_NAME, leSort, new EOAndQualifier(arrayQualifier));
	}

	static public NSMutableArrayDisplayGroup findLesInventairesComptable(ApplicationCocktail app, EOExercice exer, String inventaire, String commande, String imputation) {
		// le sort 
		NSArray leSort=null;

		//le qualifier
		NSMutableArray arrayQualifier=new NSMutableArray();

		if (inventaire!=null && !inventaire.equals(""))
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+
					EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY+" caseInsensitiveLike %@", new NSArray("*"+inventaire+"*")));
		if (commande!=null && !commande.equals(""))
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireComptable.INVENTAIRES_KEY+"."+
					EOInventaire.LIVRAISON_DETAIL_KEY+"."+EOLivraisonDetail.LIVRAISON_KEY+"."+EOLivraison.COMMANDE_KEY+"."+EOVCommande.COMM_NUMERO_KEY+"=%@", new NSArray(new Integer(commande))));
		if (imputation!=null && !imputation.equals(""))
			arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+
					EOCleInventaireComptable.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY+" caseInsensitiveLike %@", new NSArray("*"+imputation+"*")));

		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireComptable.EXERCICE_KEY+"=%@", new NSArray(exer)));

		return findLocal(app, leSort, new EOAndQualifier(arrayQualifier)); 
		//return Finder.find(app, InventaireComptable.ENTITY_NAME, leSort, new EOAndQualifier(arrayQualifier));
	}

	static public NSMutableArrayDisplayGroup findLesBiensInventorieDeLaCommande(ApplicationCocktail app, EOVCommande commande) {
		NSArray leSort = null;
		EOQualifier aQualifier = null;
		// le sort

		// le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(
				EOInventaireComptable.INVENTAIRES_KEY + "." + EOInventaire.LIVRAISON_DETAIL_KEY + "."
						+ EOLivraisonDetail.LIVRAISON_KEY + "." + EOLivraison.COMMANDE_KEY + "=%@ and "
						+ EOInventaireComptable.INVC_MONTANT_ACQUISITION_KEY + "!=null", new NSArray(commande));

		NSArray<EOInventaireComptable> resultat = new NSArray(Finder.find(app, EOInventaireComptable.ENTITY_NAME, leSort, aQualifier));
		NSMutableArrayDisplayGroup leResultat = new NSMutableArrayDisplayGroup();
		for (EOInventaireComptable invc : resultat) {
			if (invc != null && !leResultat.contains(invc)) {
				leResultat.addObject(invc);
			}
		}
		return leResultat;
	}

	static public NSMutableArrayDisplayGroup findLesBiensInventories(ApplicationCocktail app,EOInventaireComptable inventaireComptable) {
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOInventaire.INVENTAIRE_COMPTABLE_KEY+"=%@", new NSArray(inventaireComptable));

		return Finder.find(app, EOInventaire.ENTITY_NAME, leSort, aQualifier);
	}

	static public Number findLeCompteAmortissementDuree(ApplicationCocktail app, EOPlanComptable monEOPlanComptable) {
		String entityName ="ca_PlancoAmortissement";
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat("planComptable=%@ AND exercice=%@",
				new NSArray(new Object[]{ monEOPlanComptable,app.getCurrentExercice()}));

		NSMutableArrayDisplayGroup tmp = new  NSMutableArrayDisplayGroup( Finder.find(app, entityName, leSort, aQualifier));
		return ((EOPlancoAmortissement)tmp.objectAtIndex(0)).pcaDuree();
	}

	static public EOPlanComptableAmo findLeCompteAmortissement(ApplicationCocktail app, EOPlanComptable monEOPlanComptable) {
		String entityName ="ca_PlancoAmortissement";
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat("planComptable = %@ AND exercice = %@",
				new NSArray(new Object[]{ monEOPlanComptable,app.getCurrentExercice()}));

		NSMutableArrayDisplayGroup tmp = new  NSMutableArrayDisplayGroup( Finder.find(app, entityName, leSort, aQualifier));

		if (tmp.count() == 0)
			return null;

		return ((EOPlancoAmortissement)tmp.objectAtIndex(0)).planComptableAmo();
	}

	static public NSMutableArrayDisplayGroup findLesFinancements(ApplicationCocktail app, EOExercice exercice) {
		NSArray leSort = new NSArray(EOSortOrdering.sortOrderingWithKey(EOOrigineFinancement.ORGF_LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		EOQualifier aQualifier = EOQualifier.qualifierWithQualifierFormat(EOOrigineFinancement.EXERCICE_KEY+"=%@", new NSArray(exercice));
		return Finder.find(app, EOOrigineFinancement.ENTITY_NAME, leSort, aQualifier);
	}
	
    static public NSMutableArrayDisplayGroup findLocal(ApplicationCocktail app, NSArray leSort, EOQualifier aQualifier) {
        //try {
        	EOFetchSpecification myFetch;
            myFetch = new EOFetchSpecification(EOInventaireComptable.ENTITY_NAME, aQualifier, leSort);
            myFetch.setRefreshesRefetchedObjects(true);
            myFetch.setUsesDistinct(true);
            
            NSArray keyPaths=new NSArray(new Object[]{EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY, 
            		EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+EOCleInventaireComptable.PLAN_COMPTABLE_KEY});
            myFetch.setPrefetchingRelationshipKeyPaths(keyPaths);

            return new NSMutableArrayDisplayGroup(new NSArray(app.getAppEditingContext().objectsWithFetchSpecification(myFetch)));
        /*} catch (Throwable e) {
            e.printStackTrace();
            return null;
        }*/

    }
}
