package org.cocktail.corossol.client.finder;

import java.math.BigDecimal;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOUtilisateurOrgan;
import org.cocktail.application.client.eof.VFournisseur;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.corossol.client.eof.metier.EOBordereau;
import org.cocktail.corossol.client.eof.metier.EOTitre;
import org.cocktail.corossol.client.eof.metier.EOVTitreRecette;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/**
 *
 */
public class FinderTitre extends Finder {

	/**
	 * @param withLogs
	 *            Logs activés ?
	 */
	public FinderTitre(boolean withLogs) {
		super(withLogs);
	}

	/**
	 * @param app
	 *            l'{@link ApplicationCocktail}
	 * @return Tous les {@link EOTitre}
	 */
	public static NSArray findAll(ApplicationCocktail app) {
		return Finder.find(app, EOTitre.ENTITY_NAME, null, null);
	}

	/**
	 * @param app
	 *            l'{@link ApplicationCocktail}
	 * @param bindings
	 *            Un dictionnaire permettant d'initialiser le qualifier pour la recherche
	 * @return Le résultat de la recherche sur les {@link EOTitre}
	 */
	public static NSArray find(ApplicationCocktail app, NSDictionary bindings) {
		EOQualifier qualifier = null;
		NSMutableArray arrayQualifier = new NSMutableArray();

		if (bindings != null) {
			qualPcoNum(bindings, arrayQualifier);
			qualExeOrdre(bindings, arrayQualifier);
			qualGesCode(bindings, arrayQualifier);
			qualTitNumero(bindings, arrayQualifier);
			qualBorNum(bindings, arrayQualifier);
			qualTitLibelle(bindings, arrayQualifier);
			qualFournisseur(bindings, arrayQualifier);
			qualTitHt(bindings, arrayQualifier);
			qualTitTtc(bindings, arrayQualifier);
			qualUtlOrdre(bindings, arrayQualifier);
		}
		if (arrayQualifier.count() > 0) {
			qualifier = new EOAndQualifier(arrayQualifier);
		}

		System.out.println(qualifier);
		return Finder.find(app, EOVTitreRecette.ENTITY_NAME, null, qualifier);
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualPcoNum(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String pcoNum = (String) bindings.objectForKey(EOVTitreRecette.PCO_NUM_KEY);
		if (pcoNum != null) {
			arrayQualifier.addObject(new EOKeyValueQualifier(EOVTitreRecette.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, pcoNum + "*"));
		}
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualExeOrdre(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String exeOrdre = (String) bindings.objectForKey(EOVTitreRecette.EXE_ORDRE_KEY);
		if (exeOrdre != null) {
			arrayQualifier.addObject(new EOKeyValueQualifier(EOVTitreRecette.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual,
					new Integer(exeOrdre)));
		}
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualGesCode(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String gesCode = (String) bindings.objectForKey(EOVTitreRecette.GES_CODE_KEY);
		if (gesCode != null) {
			arrayQualifier.addObject(new EOKeyValueQualifier(EOVTitreRecette.GES_CODE_KEY, EOQualifier.QualifierOperatorEqual, gesCode));
		}
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualTitNumero(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String titNumero = (String) bindings.objectForKey(EOVTitreRecette.TIT_NUMERO_KEY);
		if (titNumero != null) {
			arrayQualifier.addObject(new EOKeyValueQualifier(EOVTitreRecette.TIT_NUMERO_KEY, EOQualifier.QualifierOperatorEqual, new Integer(titNumero)));
		}
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualBorNum(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String borNum = (String) bindings.objectForKey(EOBordereau.BOR_NUM_KEY);
		if (borNum != null) {
			arrayQualifier.addObject(new EOKeyValueQualifier(EOVTitreRecette.BORDEREAU_KEY + "." + EOBordereau.BOR_NUM_KEY, EOQualifier.QualifierOperatorEqual,
					new Integer(borNum)));
		}
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualTitLibelle(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String titLibelle = (String) bindings.objectForKey(EOVTitreRecette.TIT_LIBELLE_KEY);
		if (titLibelle != null) {
			arrayQualifier
					.addObject(new EOKeyValueQualifier(EOVTitreRecette.TIT_LIBELLE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + titLibelle + "*"));
		}
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualFournisseur(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String fournisseur = (String) bindings.objectForKey("fournisseur");
		if (fournisseur != null) {
			arrayQualifier.addObject(new EOKeyValueQualifier(EOVTitreRecette.V_FOURNISSEUR_KEY + "." + VFournisseur.ADR_NOM_KEY,
					EOQualifier.QualifierOperatorCaseInsensitiveLike, "*" + fournisseur + "*"));
		}
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualTitHt(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String titHt = (String) bindings.objectForKey(EOVTitreRecette.TIT_HT_KEY);
		if (titHt != null) {
			arrayQualifier.addObject(new EOKeyValueQualifier(EOVTitreRecette.TIT_HT_KEY, EOQualifier.QualifierOperatorEqual, new BigDecimal(titHt)));
		}
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualTitTtc(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String titTtc = (String) bindings.objectForKey(EOVTitreRecette.TIT_TTC_KEY);
		if (titTtc != null) {
			arrayQualifier.addObject(new EOKeyValueQualifier(EOVTitreRecette.TIT_TTC_KEY, EOQualifier.QualifierOperatorEqual, new BigDecimal(titTtc)));
		}
	}

	/**
	 * @param bindings
	 * @param arrayQualifier
	 */
	private static void qualUtlOrdre(NSDictionary bindings, NSMutableArray arrayQualifier) {
		String utlOrdre = (String) bindings.objectForKey(EOUtilisateurOrgan.UTL_ORDRE_KEY);
		if (utlOrdre != null) {
			arrayQualifier.addObject(new EOKeyValueQualifier(EOVTitreRecette.ORGAN_KEY + "." + EOOrgan.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateurOrgan.UTL_ORDRE_KEY,
					EOQualifier.QualifierOperatorEqual, new Integer(utlOrdre)));
		}
	}
}
