package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOVArticles;
import org.cocktail.corossol.client.eof.metier.EOVCommandeEngageCtrlPlanco;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderLivraisons extends Finder {

	public FinderLivraisons(boolean arg0) {
		super(arg0);
	}

	static public NSMutableArrayDisplayGroup findLesLivraisons(ApplicationCocktail app) {
		// le sort 
		NSArray leSort = null;
		//le qualifier
		EOQualifier aQualifier = EOQualifier.qualifierWithQualifierFormat(EOLivraison.COMMANDE_KEY+"=%@ AND "+EOLivraison.EXERCICE_KEY+"=%@",
				new NSArray(new Object[]{((ApplicationCorossol)app).getCurrentCommande(),app.getCurrentExercice()}));

		return Finder.find(app, EOLivraison.ENTITY_NAME, leSort, aQualifier);
	}
	
	static public NSMutableArrayDisplayGroup findToutesLesLivraisons(ApplicationCocktail app) {
		// le sort 
		NSArray leSort = null;
		//le qualifier
		EOQualifier aQualifier = EOQualifier.qualifierWithQualifierFormat(EOLivraison.EXERCICE_KEY+"=%@", new NSArray(new Object[]{app.getCurrentExercice()}));

		return Finder.find(app, EOLivraison.ENTITY_NAME, leSort, aQualifier);
	}

	public static NSArray findLesLivraisonDetails(ApplicationCocktail app,EOLivraison laLivraison) {
		// le sort 
		NSArray leSort = null;
		//le qualifier
		EOQualifier aQualifier = EOQualifier.qualifierWithQualifierFormat(EOLivraisonDetail.LIVRAISON_KEY+"=%@", new NSArray(new Object[]{laLivraison}));

		return Finder.find(app, EOLivraisonDetail.ENTITY_NAME, leSort, aQualifier);
	}
	
	public static NSArray findLesEngagements(ApplicationCocktail app) {
		// le sort 
		NSArray leSort = null;
		//le qualifier
		EOQualifier aQualifier = EOQualifier.qualifierWithQualifierFormat(EOVCommandeEngageCtrlPlanco.V_COMMANDE_KEY+"=%@",
				new NSArray(((ApplicationCorossol)app).getCurrentCommande()));

		return Finder.find(app, EOVCommandeEngageCtrlPlanco.ENTITY_NAME, leSort, aQualifier);
	}
	
	public static NSArray findLesArticles(ApplicationCocktail app) {
		// le sort 
		NSArray leSort = null;
		//le qualifier
		EOQualifier aQualifier = EOQualifier.qualifierWithQualifierFormat(EOVArticles.V_COMMANDE_KEY+"=%@", 
				new NSArray(((ApplicationCorossol)app).getCurrentCommande()));

		return Finder.find(app, EOVArticles.ENTITY_NAME, leSort, aQualifier);
	}
		
	public static NSArray findLesInventaires(ApplicationCocktail app,EOLivraisonDetail livraisonDetail) {
		// le sort 
		NSArray leSort = null;
		//le qualifier
		EOQualifier aQualifier = EOQualifier.qualifierWithQualifierFormat(EOInventaire.LIVRAISON_DETAIL_KEY+"=%@", new NSArray(livraisonDetail));

		return Finder.find(app, EOInventaire.ENTITY_NAME, leSort, aQualifier);
	}
}
