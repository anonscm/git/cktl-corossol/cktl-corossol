package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EOParametre;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderParametre extends Finder {

	public FinderParametre(boolean withLogs) {
		super(withLogs);
		
	}

	public static EOParametre findParametre(ApplicationCocktail app, String parametre, EOExercice exercice) {
		NSMutableArray arrayQualifier = new NSMutableArray();
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOParametre.PAR_KEY_KEY + "=%@", new NSArray(parametre)));
		arrayQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOParametre.EXERCICE_KEY + "=%@", new NSArray(exercice)));

		NSMutableArrayDisplayGroup array = Finder.find(app, EOParametre.ENTITY_NAME, null, new EOAndQualifier(arrayQualifier));
		if (array.count() > 0) {
			return (EOParametre) array.objectAtIndex(0);
		}
		return null;
	}

	public static boolean isParametreProrataTemporis(ApplicationCocktail app, EOExercice exercice) {
		EOParametre parametre=findParametre(app, EOParametre.PRORATA_TEMPORIS, exercice);
		if (parametre!=null && parametre.parValue()!=null && parametre.parValue().equals(EOParametre.OUI))
			return true;
		return false;
	}

	public static boolean isParametreRecalculAmortissementsAnterieurs(ApplicationCocktail app, EOExercice exercice) {
		EOParametre parametre=findParametre(app, EOParametre.RECALCUL_AMORTISSEMENTS_ANTERIEURS, exercice);
		if (parametre!=null && parametre.parValue()!=null && parametre.parValue().equals(EOParametre.OUI))
			return true;
		return false;
	}

	public static String parametreTypeAmortissement(ApplicationCocktail app, EOExercice exercice) {
		EOParametre parametre = findParametre(app, EOParametre.TYPE_AMORTISSEMENT, exercice);
		if (parametre == null) {
		    return null;
		}
		// can be null.
		return parametre.parValue();
	}
}
