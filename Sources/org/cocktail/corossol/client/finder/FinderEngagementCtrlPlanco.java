package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EOVCommande;
import org.cocktail.corossol.client.eof.metier.EOVCommandeEngageCtrlPlanco;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderEngagementCtrlPlanco extends Finder {

		public FinderEngagementCtrlPlanco(boolean arg0) {
		super(arg0);
		
	}
	
	static public NSMutableArrayDisplayGroup findLesEngagementsCtrlPlanco(ApplicationCocktail app, EOVCommande commande) {
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOVCommandeEngageCtrlPlanco.V_COMMANDE_KEY+"=%@" , new NSArray(commande));
		
		return Finder.find(app, EOVCommandeEngageCtrlPlanco.ENTITY_NAME, leSort, aQualifier);
	}
}
