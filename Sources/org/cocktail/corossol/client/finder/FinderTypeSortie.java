package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.corossol.client.eof.metier.EOTypeSortie;

import com.webobjects.foundation.NSArray;

public class FinderTypeSortie extends Finder {

	public FinderTypeSortie(boolean withLogs) {
		super(withLogs);
		
	}

	static public NSArray findAll(ApplicationCocktail app) {
		return Finder.find(app, EOTypeSortie.ENTITY_NAME, null, null);
	}
}
