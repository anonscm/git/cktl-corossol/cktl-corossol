package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EODegressif;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FinderDegressif extends Finder {

	public FinderDegressif(boolean withLogs) {
		super(withLogs);
	}

	static public NSMutableArrayDisplayGroup find(ApplicationCocktail app, NSTimestamp date) {
		// le sort 
		NSArray leSort = null;

		//le qualifier
		EOQualifier aQualifier=EOQualifier.qualifierWithQualifierFormat(EODegressif.DGRF_DATE_DEBUT_KEY+"<=%@ and ("+EODegressif.DGRF_DATE_FIN_KEY+">=%@ or "+
				EODegressif.DGRF_DATE_FIN_KEY+"=nil)", new NSArray(new Object[]{date, date}));

		return Finder.find(app, EODegressif.ENTITY_NAME, leSort, aQualifier);
	}

	static public NSMutableArrayDisplayGroup findAll(ApplicationCocktail app) {
		return Finder.find(app, EODegressif.ENTITY_NAME, sort(), null);
	}

	private static NSArray sort() {
		NSMutableArray array=new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EODegressif.DGRF_DATE_DEBUT_KEY, EOSortOrdering.CompareDescending));
		return array;
	}
}
