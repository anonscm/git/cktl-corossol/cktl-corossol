package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.eof.metier.EOVCommande;
import org.cocktail.corossol.client.eof.metier.EOVDepense;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderDepenses extends Finder {

	public FinderDepenses(boolean withLogs) {
		super(withLogs);
		
	}

	static public NSMutableArrayDisplayGroup findLesDepensesDeLaCommande(ApplicationCocktail app, EOVCommande commande) {
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat(EOVDepense.V_COMMANDE_KEY+"=%@", new NSArray(commande));

		return Finder.find(app, EOVDepense.ENTITY_NAME, leSort, aQualifier);
	}
}
