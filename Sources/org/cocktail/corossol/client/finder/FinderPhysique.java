package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOLivraison;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderPhysique extends Finder {

	public FinderPhysique(boolean arg0) {
		super(arg0);
		
	}
	
	static public NSMutableArrayDisplayGroup findLesLivraisons(ApplicationCocktail app) {
		String entityName ="Livraison";
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat("commande = %@ AND exercice = %@",
				new NSArray(new Object[]{((ApplicationCorossol)app).getCurrentCommande(),app.getCurrentExercice()}));

		return Finder.find(app, entityName, leSort, aQualifier);
	}
	
	public static NSArray findLesInventaires(ApplicationCocktail app,EOLivraison laLivraison) {
		String entityName ="Inventaire";
		NSArray leSort = null;
		EOQualifier aQualifier = null;

		// le sort 

		//le qualifier
		aQualifier = EOQualifier.qualifierWithQualifierFormat("livraisonDetail.livraison=%@", new NSArray(new Object[]{laLivraison}));

		return Finder.find(app, entityName, leSort, aQualifier);
	}
}