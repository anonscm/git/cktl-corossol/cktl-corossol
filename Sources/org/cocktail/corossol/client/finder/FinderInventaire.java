package org.cocktail.corossol.client.finder;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.finder.Finder;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireSortie;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOVCommande;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderInventaire extends Finder {

	public FinderInventaire(boolean withLogs) {
		super(withLogs);
		
	}

	static public NSArray findLesInventaires(ApplicationCocktail app, String numeroCommande, String numeroInventaire, boolean isSortie) {
		NSArray leSort = null;
		NSMutableArray andQualifier=new NSMutableArray();

		// le sort 
//		leSort=new NSArray(EOSortOrdering.sortOrderingWithKey(Inventaire.INVENTAIRE_COMPTABLE_KEY+"."+
//				InventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+CleInventaireComptable.CLIC_NUM_COMPLET_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		
		//le qualifier
		if (numeroInventaire!=null && numeroInventaire.length()>0)
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaire.INVENTAIRE_COMPTABLE_KEY+"."+
					EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY+" caseInsensitiveLike %@", 
					new NSArray("*"+numeroInventaire+"*")));
		if (numeroCommande!=null && numeroCommande.length()>0)
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaire.LIVRAISON_DETAIL_KEY+"."+EOLivraisonDetail.LIVRAISON_KEY+"."+
					EOLivraison.COMMANDE_KEY+"."+EOVCommande.COMM_NUMERO_KEY+"=%@", new NSArray(new Integer(numeroCommande))));

		if (isSortie)
			andQualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaire.INVENTAIRE_SORTIES_KEY+"."+EOInventaireSortie.TYPE_SORTIE_KEY+"!=nil",null));
System.out.println(new EOAndQualifier(andQualifier));

		return Finder.find(app, EOInventaire.ENTITY_NAME, leSort, new EOAndQualifier(andQualifier));
	}
}
