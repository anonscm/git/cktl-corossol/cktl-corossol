package org.cocktail.corossol.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;

import org.cocktail.corossol.client.nib.StringCtrl;

import com.webobjects.foundation.NSData;

public class UtilCtrl {

	private static String chaineMontant(String chaine) {
		return chaine.replace(" ", "").replace(",", ".");
	}
	
	/**
	 * 
	 * @param chaine
	 * @return
	 */
	public static BigDecimal stringToBigDecimal (String chaine){
		chaine=chaineMontant(chaine);
		BigDecimal montant = new BigDecimal(chaine);
		return montant;
	}	
	/**
	 *
	 * @return
	 */
    public static String lastModif (){
    	java.util.Calendar currentTime = java.util.Calendar.getInstance();
		return "-"+ currentTime.get(java.util.Calendar.DAY_OF_MONTH)
			  +"."+ (currentTime.get(java.util.Calendar.MONTH)+1) 
			  +"."+ currentTime.get(java.util.Calendar.YEAR)
			  +"-"+ currentTime.get(java.util.Calendar.HOUR_OF_DAY)
			  +"h"+ currentTime.get(java.util.Calendar.MINUTE)
			  +"m"+ currentTime.get(java.util.Calendar.SECOND);
    }
    /**
     * 
     * @param data
     * @return
     */
    public static String nsdataToCsvString (NSData data){
		String tmp = new String(data.bytes());
		tmp = "\""+tmp;
		tmp = StringCtrl.replace(tmp, "\"","");
		tmp = StringCtrl.replace(tmp, "##","\"\n\"");	
		tmp	= StringCtrl.replace(tmp, "||","\";\"");
		return tmp;
	}
    
    /**
     * Retourne un charset par défaut suivant l'OS
     * @return
     */
    private static String getOperatingSystemTextEncoding() {
        String OS = System.getProperties().getProperty("os.name");
        if (OS.startsWith("Win")) return "Cp1252";
        if (OS.startsWith("Mac")) return "MacRoman";
        return System.getProperties().getProperty("file.encoding");
    }
    
    /**
     * Essaie d'écrire le fichier suivant le système local d'encodage sinon écrit le fichier tel quel
     * @param f
     * @param data
     */
    public static void writeTextFileSystemEncoding (File f, String data) throws Exception {
    	try {
    		//FIXME L'encodage n'a pas l'air de fonctionner en déploiement.
    		FileOutputStream fos = new FileOutputStream(f);
    		Writer out = new OutputStreamWriter(fos, getOperatingSystemTextEncoding());
			out.write(data);
			out.close();
			fos.close();
			return ;
		} catch (Exception e) {
		    System.err.println("L'écriture de fichier texte avec conversion a échoué");
		    System.err.println("Ecriture du fichier sans conversion");
		}
    	/* Si conversion est impossible */
    	FileOutputStream fileOutputStream;
		fileOutputStream = new java.io.FileOutputStream(f);
		fileOutputStream.write(data.getBytes());
		fileOutputStream.close();
    }
}
