package org.cocktail.corossol.client.zutil;

import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.text.NumberFormatter;

/**
 * Formatteur personalisé pour la saisie de montants
 */
@SuppressWarnings("serial")
public class EditDecimalFormatter extends NumberFormatter {

	// Constructors
	/**
	 * constructeur par defaut
	 */
	public EditDecimalFormatter() {

	}

	/**
	 * @param format : format attendu
	 */
	public EditDecimalFormatter(NumberFormat format) {
		super(format);
	}

	// Public Methods
	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.text.InternationalFormatter#valueToString(java.lang.Object)
	 */
	@Override
	public String valueToString(Object value) throws ParseException {
		Number number = (Number) value;
		return super.valueToString(number);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.text.InternationalFormatter#stringToValue(java.lang.String)
	 */
	@Override
	public Object stringToValue(String text) throws ParseException {

		text = text.replaceAll("\\.", ",");
		text = text.replaceAll(" ", "");
		text = text.trim();

		Number number = (Number) super.stringToValue(text);
		return number;
	}
}