/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.corossol.client.zutil.wo.table;

import java.util.Map;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * Descriptif d'une colonne dont les valeurs à afficher sont r�cup�r�es depuis un HashMap, dont les cl�s sont les EOEnterpriseObjects contenues dans le displaygroup.
 * Cette classe est utile pour afficher/modifier des valeurs en lien avec les lignes du displaygroup.
 * Pour faire une colonne qui contient des cases à cocher pour permetre à l'utilisateur de selectionner des lignes, créer une instance de cette en passant un HashMap
 * contenant en clé les objets du displayGroup et en valeur Boolean.TRUE ou Boolean.FALSE.
 * @author Rodolphe Prin
 */
public class ZEOTableModelColumnDico extends ZEOTableModelColumn {
	private final Map dico;

	/**
	 * @param attName
	 * @param vTitle
	 * @param vpreferredWidth
	 * @param checkedDico Dictionnaire contenant en clé un enregistrement du displaygroup et en valeur un Boolean. La synchronisation de ce dico doit etre effectuee manuellement.
	 */
	public ZEOTableModelColumnDico(EODisplayGroup vDg, String vTitle, int vpreferredWidth, Map aDico) {
		super(vDg,vTitle, vTitle, vpreferredWidth);
		dico = aDico;
	}


	public Object getValueAtRow(final int row) {
		return dico.get(getMyDg().displayedObjects().objectAtIndex(row));
	}

	public void setValueAtRow(final Object value, final int row) {
		if (getMyModifier()!=null) {
			getMyModifier().setValueAtRow(value, row);
		}
		dico.put(getMyDg().displayedObjects().objectAtIndex(row), value);
	}

	/**
	 * Change la valeurs de toutes les lignes pour cette colonne.
	 * Cette méthode ne fait pas appel à fireTableDataChanged() du model, à vous de l'implémenter...
	 * @param newVal
	 */
	public void setValueForAllRows(final Object newVal) {
		final NSArray res = getMyDg().displayedObjects();
		for (int i = 0; i < res.count(); i++) {
			setValueAtRow(newVal,i);
//			dico.put(res.objectAtIndex(i),newVal);
		}
	}









}