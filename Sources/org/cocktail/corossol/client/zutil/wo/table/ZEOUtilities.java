/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.corossol.client.zutil.wo.table;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;


/**
 *
 *
 * @author Rodolphe Prin
 */
public abstract class ZEOUtilities {
    public static final String OPERATEUR_EGAL="=";
    public static final String OPERATEUR_CASEINSENSITIVELIKE=" caseInsensitiveLike ";

	/**
	 * Calcule la somme de toutes les valeurs contenues dans un champ des EOEnterpriseObjects contenues dans le tableau.
	 * @param array Tableau d'EOEnterpriseObjects
	 * @param keyName Nom de l'attribut qui contient les valeurs à sommer (le champ doit être un BigDecimal)
	 * @return
	 */
	public static final BigDecimal calcSommeOfBigDecimals(final NSArray array, final String keyName) {
		return calcSommeOfBigDecimals(array.vector(), keyName);
	}

	public static final BigDecimal calcSommeOfBigDecimals(final Vector array, final String keyName) {
		BigDecimal res = new BigDecimal(0.0).setScale(2);
		Iterator iter = array.iterator();
		while (iter.hasNext()) {
			EOEnterpriseObject element = (EOEnterpriseObject) iter.next();
			res = res.add((BigDecimal)element.valueForKey(keyName));
		}
		return res;
	}

	public static final BigDecimal calcSommeOfBigDecimals(final ArrayList array, final String keyName) {
		BigDecimal res = new BigDecimal(0.0).setScale(2);
		Iterator iter = array.iterator();
		while (iter.hasNext()) {
			EOEnterpriseObject element = (EOEnterpriseObject) iter.next();
			res = res.add((BigDecimal)element.valueForKey(keyName));
		}
		return res;
	}



	/**
	 * Renvoie un NSArray d'EOEnterpriseObjects filtr�s.
	 *
	 * @param array
	 * @param key
	 * @param value
	 * @return
	 */
	public static final NSArray getFilteredArrayByKeyValue(final NSArray array, final String key, final Object value) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(key+"=%@", new NSArray(value));
		NSArray res = EOQualifier.filteredArrayWithQualifier(array, qual);
		return res;
	}


	public static final NSArray getFilteredArrayByKeyValue(final NSArray array, final String key, final Object value, final String comparator) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(key+ " "+comparator+" %@", new NSArray(value));
		NSArray res = EOQualifier.filteredArrayWithQualifier(array, qual);
		return res;
	}

	/**
	 * @param childEc
	 * @return Une chaine representant une arbrescence d'editingContext.
	 */
	public static String arborescenceOfEc(EOEditingContext childEc) {
	    EOEditingContext parentEc=null;
	    if (childEc.parentObjectStore() instanceof EOEditingContext) {
	        parentEc = (EOEditingContext)childEc.parentObjectStore();
	    }

		//Si on est arrivé en haut de l'arborescence, ca veut dire que l'objet n'a pas été trouv�, on renvoie nul.
		if (parentEc==null) {
			return "O";
		}
        return  arborescenceOfEc(parentEc) + "-->" + childEc.toString() ;
	}


    /**
     * @param obj L'EOEnterpriseObject à convertir.
     * @return Un hashMap construit à partir des propri�tes et des relations d'un EOEnterpriseObject
     */
    public static HashMap convertEOEnterpriseObjectToHashMap(EOEnterpriseObject obj) {
        HashMap res = new HashMap();
        NSArray lesattr = obj.attributeKeys();
		for (int i=0; i <lesattr.count();i++) {
			String keyname = (String) lesattr.objectAtIndex(i);
			res.put(keyname, obj.valueForKey(keyname));
		}

//		R�cup�rer les relations
		NSArray lesrel = obj.toOneRelationshipKeys();
		for (int i=0; i <lesrel.count();i++) {
			String keyname = (String) lesrel.objectAtIndex(i);
			res.put(keyname, obj.valueForKey(keyname));
		}
		return res;
    }


	public static final ArrayList convertNSArrayOfNSDictionaryToArrayListOfMap(final NSArray array) {
	    if (array==null) {
	        return null;
	    }
	    ArrayList list = new ArrayList(array.count());
	    for (int i = 0; i < array.count(); i++) {
            NSDictionary element = (NSDictionary) array.objectAtIndex(i);
            list.add(element.hashtable());
        }
//	    Enumeration enum = array.objectEnumerator();
//	    while (enum.hasMoreElements()) {
//            NSDictionary element = (NSDictionary) enum.nextElement();
//            list.add(element.hashtable());
//        }
	    return list;
	}



	/**
	 * Méthode qui <b>tente</b> de contourner le bug EOF qui se produit lors
	 * d'un saveChanges avec l'erreur "reentered responseToMessage()".<br>
	 * <b>Il faut appeler cette méthode avant de créer un descendant d'EOCustomObject, donc bien avant le saveChanges()</b><br>
	 *
	 * Le principe est d'appeler la méthode EOClassDescription.classDescriptionForEntityName("A") pour chaque relation de l'objet qu'on va créer.
	 * Il faut appeler cette méthode avant de créer un objet, au lancement de l'application par exemple, sur toutes les entites du modèle.
	 * Par exemple dans le cas d'un objet Facture qui a des objets Ligne, appeler EOClassDescription.classDescriptionForEntityName("Facture")
	 * avant de créer un objet Ligne.
	 * R�p�ter l'op�ration pour toutes les relations de l'objet.
	 *
	 * @param list Liste de String identifiant une entité du modèle.
	 * @see http://www.omnigroup.com/mailman/archive/webobjects-dev/2002-May/023698.html
	 */
	public static final void fixWoBug_responseToMessage(final String[] list) {
	    for (int i = 0; i < list.length; i++) {
	        EOClassDescription.classDescriptionForEntityName(list[i]);
//	        System.out.println(list[i] +" OK");
        }
	}


    /**
     * Effectue une intersection entre plusieurs NSArray.
     * @param lesTableaux
     * @return
     */
    public static final NSArray intersectionOfNSArray(ArrayList lesTableaux) {
        int nbTabs = lesTableaux.size();
        if (nbTabs==0) {
            return new NSArray();
        }
        final NSArray tab1 = (NSArray) lesTableaux.get(0);
        if (nbTabs==1) {
            return tab1;
        }

        final NSMutableArray res = new NSMutableArray();
        for (int i = 0; i < tab1.count(); i++) {
//            for (int i = 1; i < tab1.count(); i++) {
            boolean cond=true;
            for (Iterator iter = lesTableaux.iterator(); iter.hasNext() && cond;) {
                cond = cond && ((NSArray)iter.next()).containsObject(tab1.objectAtIndex(i));
            }
            if (cond) {
                res.addObject(tab1.objectAtIndex(i));
            }
        }
        return res.immutableClone();
    }

    
    /**
     * Renvoi le complement de ensembleReference dans ensemble. 
     * (cad Tous les elements de ensemble qui ne font pas partie de 
     * l'intersection entre ensembleReference et ensemble)
     * @param ensembleReference
     * @param ensembleTeste
     * @return
     */
    public static final NSArray complementOfNSArray(final NSArray ensembleReference, final NSArray ensemble) {
        final ArrayList tabs = new ArrayList(2);
        tabs.add(ensembleReference);
        tabs.add(ensemble);
        final NSArray intersect = intersectionOfNSArray(tabs);
        final NSMutableArray res = new NSMutableArray();
        final Enumeration iterator = ensemble.objectEnumerator();
        while (iterator.hasMoreElements()) {
            final Object element = (Object) iterator.nextElement();
            if (intersect.indexOfObject(element)==NSArray.NotFound) {
                res.addObject(element);
            }
        }
        return res;
    }
    
    
    /**
     * Renvoi un tableau contenant les objets de ensemble1 qui ne sont pas dans ensemble2 
     * et les objets de enemble2 qui ne sont pas dans ensemble1. (l'union du complemente de 1 dans 2 
     * et du complement de 1 dans 2).  
     * @param ensemble1
     * @param ensemble2
     * @return
     */
    public static final NSArray extOfNSArrays(final NSArray ensemble1, final NSArray ensemble2, boolean distinct) {
        final NSArray fermeture1 = complementOfNSArray(ensemble1, ensemble2);
        final NSArray fermeture2 = complementOfNSArray(ensemble2, ensemble1);
        final NSArray res = unionOfNSArrays(new NSArray[]{fermeture1, fermeture2});
        if (distinct) {
            return getDistinctsOfNSArray(res);
        }
        return res;
    }
    
    
    /**
     * Renvoie l'union de tous les tableaux (sans effectuer de distinct)
     * @param lesTableaux
     * @return
     */
    public static final NSArray unionOfNSArrays(final ArrayList lesTableaux) {
        int nbTabs = lesTableaux.size();
        if (nbTabs==0) {
            return new NSArray();
        }
        final NSArray tab1 = (NSArray) lesTableaux.get(0);
        if (nbTabs==1) {
            return tab1;
        }

        final NSMutableArray res = new NSMutableArray();
            for (Iterator iter = lesTableaux.iterator(); iter.hasNext();) {
                res.addObjectsFromArray(((NSArray)iter.next()));
            }
        return res.immutableClone();        
    }
    
    public static final NSArray unionOfNSArrays(NSArray[] lesTableaux) {
        int nbTabs = lesTableaux.length;
        if (nbTabs==0) {
            return new NSArray();
        }
        final NSArray tab1 = (NSArray) lesTableaux[0];
        if (nbTabs==1) {
            return tab1;
        }
        
        final NSMutableArray res = new NSMutableArray();
        for (int i = 0; i < lesTableaux.length; i++) {
            res.addObjectsFromArray(lesTableaux[i]);
        }
        return res.immutableClone();        
    }
    


    /**
     * Supprime les doublons dans un NSMutableArray, les objets conserv�s sont ceux avec le plus petit index.
     * @param array
     */
    public static final void removeDuplicatesInNSArray(final NSMutableArray array) {
        int i=array.count()-1;        
        while (i>=0) {
            final Object obj = array.objectAtIndex(i);
            int found = array.indexOfObject(obj); 
            if (found != NSArray.NotFound && found != i) {
                array.removeObjectAtIndex(i);
            }
            i--;
        }
    }
    
    
    /**
     * Renvoi un nouveau NSArray contenant les elements de array sans les doublons.
     * @param array
     * @return
     */
    public static final NSArray getDistinctsOfNSArray(final NSArray array) {
        final NSMutableArray res = new NSMutableArray();
        for (int i = 0; i < array.count(); i++) {
            final Object element = (Object) array.objectAtIndex(i);
            if (res.indexOfObject(element)==NSArray.NotFound) {
                res.addObject(element);
            }
        }
        return res.immutableClone();
    }
        
    
    
    /**
     * Construit et renvoi un EOQualifier de type fourchette du type min&lt;= x &lt;=max
     * @param attributeName nom de l'attribut dans l'entit�
     * @param keyNameMin Nom de la clé dans la map qui est susceptible de contenir la valeur basse
     * @param keyNameMax Nom de la clé dans la map qui est susceptible de contenir la valeur haute
     * @param values	Map contenant les valeurs exemples
     * @return
     */
    public final static EOQualifier buildFourchetteFilter(final String attributeName,  final String keyNameMin, final String keyNameMax, final Map values ) {
        final NSMutableArray quals = new NSMutableArray();
        EOQualifier res = new EOAndQualifier(new NSArray());
        Object valMin = values.get(keyNameMin);
        Object valMax = values.get(keyNameMax);
        if (valMin!=null) {
            if (valMin instanceof Number) {
                valMin = new BigDecimal(((Number)valMin).doubleValue() ).setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            quals.addObject(EOQualifier.qualifierWithQualifierFormat(attributeName + ">=%@", new NSArray( valMin )));
        }
        if (valMax!=null) {
            if (valMax instanceof Number) {
                valMax = new BigDecimal(((Number)valMax).doubleValue() ).setScale(2, BigDecimal.ROUND_HALF_UP);
            }
            quals.addObject(EOQualifier.qualifierWithQualifierFormat(attributeName+"<=%@", new NSArray( valMax )));
        }
        if (quals.count()>0) {
            res = new EOAndQualifier(quals);
        }
        return res;
    }


    public final static EOQualifier buildSimpleFilter(final String attributeName,  final String keyName, final Map values, final String operator ) {
        EOQualifier res = new EOAndQualifier(new NSArray());
        if (values.get(keyName)!=null) {
            res = EOQualifier.qualifierWithQualifierFormat(attributeName + operator +"%@", new NSArray( values.get(keyName) ));
        }
        return res;
    }

    public final static EOQualifier buildSimpleFilter(final String attributeName,  final String keyName, final Map values, final String operator, final String prefix, final String suffix ) {
        EOQualifier res = new EOAndQualifier(new NSArray());
        if (values.get(keyName)!=null) {
            res = EOQualifier.qualifierWithQualifierFormat(attributeName + operator +"%@", new NSArray(prefix+ values.get(keyName)+suffix ));
        }
        return res;
    }


    /**
     * Renvoie une liste de valeurs separee par des virgules.<br> Les valeurs sont recup�r�es à partir des objets EOKeyValueCoding contenus dans le NSArray passes en parametre, sur le champ sp�cifié par keyname. 
     * @param eos
     * @param keyName
     * @return
     */
    public final static String getCommaSeparatedListOfValues(final NSArray eos, final String keyName) {
        return getSeparatedListOfValues(eos, keyName, ",");
        
    }
    
    
    public final static String getSeparatedListOfValues(final NSArray eos, final String keyName, final String separator) {
        if (eos==null || eos.count()==0) {
            return null;
        }
        final LinkedList list = new LinkedList();
//System.out.println("cle=/"+keyName+"/");
        for (int i = 0; i < eos.count(); i++) {
            final NSKeyValueCoding element = (NSKeyValueCoding) eos.objectAtIndex(i);
//System.out.println(element);
            list.add(element.valueForKey(keyName));
        }
        return getSeparatedListOfValues(list, separator);
        
    }

    
    /**
     * une liste de valeurs separee par des virgules.<br> 
     * @param list
     * @return
     */
    public final static String getCommaSeparatedListOfValues(final Collection list) {
        return getSeparatedListOfValues(list, ",");
    }
    
    /**
     * une liste de valeurs separee par un separateur.<br> 
     * @param list
     * @return
     */    
    public final static String getSeparatedListOfValues(final Collection list, final String separator) {
        String res = "";
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            final Object element = iterator.next();
            if (element != null) {
                if (res.length()>0) {
                    res += separator;
                }
                res += element.toString();
            }
        }
        return res;
    }


    /**
     * Renvoie une liste de valeurs obtenues à partir d'un tableau d'EOs. 
     * @param eos
     * @param keyName 
     * @param separator
     * @param distinct
     * @return
     */
    public final static Collection getCollectionOfValuesFromEos (final NSArray eos, final String keyName, final boolean distinct) {
        final LinkedList list = new LinkedList();
        if (eos==null || eos.count()==0) {
            return list;
        }
        
        for (int i = 0; i < eos.count(); i++) {
            final NSKeyValueCoding element = (NSKeyValueCoding) eos.objectAtIndex(i);
            if (distinct) {
                if (list.indexOf(element.valueForKey(keyName)) == -1) {
                    list.add(element.valueForKey(keyName));
                }
            }
            else {
                list.add(element.valueForKey(keyName));
            }
        }
        return list;
        
    }    
    
    
    /**
     * 
     * @param ec
     * @param objects
     * @return Un NSArray contenant les globalIDs des EOEnterpriseObject contenus dans objects. 
     */
    public static final NSArray globalIDsForObjects(final EOEditingContext ec, final NSArray objects) {
        final NSMutableArray lesGlobalIds = new NSMutableArray();
        for (int i = 0; i < objects.count(); i++) {
            lesGlobalIds.addObject(ec.globalIDForObject( ((EOEnterpriseObject)objects.objectAtIndex(i))) )    ;
        }
        return lesGlobalIds;
    }
    
 
    
    
    
    
    
}
