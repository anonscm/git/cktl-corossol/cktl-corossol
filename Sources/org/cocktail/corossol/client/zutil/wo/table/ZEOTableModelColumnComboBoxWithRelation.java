/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of Karukera.
 *
 * Karukera is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Karukera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.corossol.client.zutil.wo.table;

import java.text.Format;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.eointerface.EODisplayGroup;


/**
 * Modele de colonne de table pour afficher/ modifier directement la relation d'un EO.
 * A utiliser avec les relations 1/1.
 *
 * @author Rodolphe Prin
 */
public class ZEOTableModelColumnComboBoxWithRelation extends ZEOTableModelColumn {
	private EODisplayGroup _objectsToList;
	private String _attributeForList;
	private final String _relationName;
	private String _nullLabel;
	private Format _formatPattern;
	private ZEOComboBoxModel _comboboxModel;
	private final JComboBox	_myComboBox;




	/**
	 * crée un modele de colonne basé sur une relation d'enterpriseobject, avec gestion d'une combobox.
	 *
	 * @param dg Displaygroup contenant les enterpriseObjects des rows
	 * @param attName Nom de l'attribut associé à la colonne
	 * @param vTitle Titre de la colonne
	 * @param objectsToList DisplayGroup contenant les objets qui seront listes dans la combobox
	 * @param attributeForList Attribut utilispour r�cup�rer les valeurs à afficher dans la combobox
	 * @param relationName Nom de la relation entre l'eoenterpriseObject affiché dans la table et l'eoenterpriseObjet affiché dans la combobox
	 * @param nullLabel Valeur à associer à l'objet null à afficher dans la combobox. Si null, l'option nulle ne sera pas proposee dans la combobox.
	 * @param formatPattern Format à appliquer pour afficher la valeur dans la combobox
	 * @param vPreferredWidth Taille par défaut de la colonne
	 * @throws DefaultClientException
	 */
	public ZEOTableModelColumnComboBoxWithRelation(
		final EODisplayGroup dg,
		final String attName,
		final String vTitle, final EODisplayGroup objectsToList, final String attributeForList, final String relationName, final String nullLabel, final Format formatPattern, int vPreferredWidth) throws Exception {
			super(dg, attName, vTitle, vPreferredWidth);
		_relationName =relationName;

		_myComboBox = new JComboBox();
		if (objectsToList!=null) {
			setTableCellEditor(new DefaultCellEditor(_myComboBox));
			updateComboWithData(objectsToList,attributeForList,nullLabel,formatPattern);
		}
	}

	public final void updateComboWithData( final EODisplayGroup objectsToList, final String attributeForList,final String nullLabel, final Format formatPattern) throws Exception {
		try {
			_objectsToList = objectsToList;
			_attributeForList = attributeForList;
			_nullLabel = nullLabel;
			_formatPattern = formatPattern;
			_comboboxModel = new ZEOComboBoxModel(_objectsToList.displayedObjects(),_attributeForList,_nullLabel,_formatPattern);
			_myComboBox.setModel(_comboboxModel);

		} catch (Exception e) {
			throw e;
		}
	}



	/* (non-Javadoc)
	 * @see fr.univlr.karukera.client.zutil.wo.table.ZEOTableModelColumn#getValueAtRow(int)
	 */
	public Object getValueAtRow(final int row) {
		return ((EOEnterpriseObject)(getMyDg().displayedObjects().objectAtIndex(row))).valueForKeyPath( getAttributeName()  ) ;
	}




	/**
	 * Si un modifier est afefcté à la colonne, la méthode setValueAtRow du modifier est appel�e,
	 * avec l'EOEnterprisObject comme parametre.
	 * @see org.cocktail.corossol.client.zutil.wo.table.ZEOTableModelColumn#setValueAtRow(java.lang.Object, int)
	 */
	public void setValueAtRow(final Object value, final int row) {
	    if (getMyModifier()!=null) {
	        getMyModifier().setValueAtRow(_comboboxModel.getSelectedEObject(), row);
	    }
		else {

		    final EOEnterpriseObject obj = ((EOEnterpriseObject)getMyDg().displayedObjects().objectAtIndex(row));
			//Nettoyer la relation si deja presente
			if ((obj.valueForKey(_relationName) != null) && (obj.valueForKey(_relationName) != EOKeyValueCoding.NullValue)) {
				obj.removeObjectFromBothSidesOfRelationshipWithKey((EOEnterpriseObject)obj.valueForKey(_relationName), _relationName);
			}
			if (value!=null) {
				obj.addObjectToBothSidesOfRelationshipWithKey((EOEnterpriseObject)_comboboxModel.getSelectedEObject(), _relationName);
			}


		}
	}



}
