/* ZJRadioButton.java created by rprin on Tue 23-Sep-2003 */
package org.cocktail.corossol.client.zutil.wo.table;

import java.awt.Dimension;

import javax.swing.JRadioButton;

import com.webobjects.eocontrol.EOEnterpriseObject;

/**
* Classe representant un bouton radio auquel on associe un objet EOEnterpriseObject.
 * @see ZEOMatrix
 */
public class ZJRadioButton extends JRadioButton {
    /** Objet m�tier associ�*/
    private EOEnterpriseObject eo;

    public ZJRadioButton(EOEnterpriseObject peo, String libelle,boolean selected, Dimension dimension) {
        super(libelle, selected);
        this.setName(libelle);
        this.setToolTipText(libelle);
        this.setSize(dimension);
        eo=peo;
    }

    public EOEnterpriseObject eo() {
        return eo;
    }

}