/*
 * Copyright (C) 2004 Université de La Rochelle
 *
 * This file is part of PrestationJC.
 *
 * PrestationJC is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * PrestationJC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.corossol.client.zutil.wo.table;

import java.text.Format;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;


/**
 * Classe pour créer un modéle pour les listes déroulantes.<br>
 * Ce modéle permet d'affecter un tableau d'objets respectant l'interface NSKeyValueCoding (par exmple un NSArray d'EOEnterpriseObjects ou d'EOGenericRecord),
 * en spécifiant la clé é utiliser pour afficher les valeurs dans la liste déroulante.<br>
 * <br>
 * Les
 * Exemple:<br>
 * <code>
 * ...<br>
 * 		NSArray res = fetchDataCatalogues(currentFournis); //res contient un tableau d'EOGenericRecord<br>
 *		ZEOComboBoxModel myZEOComboBoxModel = new ZEOComboBoxModel(res, "catLibelle");  //le combobox affichera le contenu de l'attribut catLibelle<br>
 *		ldCatalogues.setModel(myZEOComboBoxModel); //ldCatalogue est un JComboBox<br>
 *		if ((res!=null) && (res.count()>0)) {<br>
 *			myZEOComboBoxModel.setSelectedEObject(res.objectAtIndex(0));<br>
 *		}<br>
 *		<br>
 *...<br>
 * </code>
 *
 * @author rprin
 */
public class ZEOComboBoxModel extends DefaultComboBoxModel {
//	private Hashtable myHashtable;
//	private Object[] myObjects;
	private ArrayList myObjects;
	private final String myDisplayKey;
	private final String nullValue;
	private final Format formatDisplay;

	/**
	 * Contructeur du modéle.
	 *
	 * @param lesObjets Tableau contenant des objets implémentant l'interface NSKeyValueCoding.
	 * @param displayKey Nom de la propriété qui doit permettre d'afficher le libellé de chaque élément de la liste déroulante (en NSKeyValueCoding). Sera affichée avec valueForKey().
	 * Si null, la valeur affichée sera obtenue via un toString().
	 * @param labelForNullValue si non null, une valeur sera affichée en début de modele et getSelectedEObject renverra null si selectionné.
	 * @param formatPattern
	 */
	public ZEOComboBoxModel(NSArray lesObjets, String displayKey, String labelForNullValue, Format format)  {
		super();
		myDisplayKey = displayKey;
		nullValue = labelForNullValue;
		formatDisplay = format;
		updateListWithData(lesObjets);
	}


	/**
	 * Renvoie l'objet correspondant é la sélection.
	 */
	public NSKeyValueCoding getSelectedEObject() {
		if (super.getSelectedItem()==null) {
			return null;
		}
        final int i = super.getIndexOf( super.getSelectedItem() );
        if (i==-1) {
            return null;
        }
        return (NSKeyValueCoding) myObjects.get(i);
	}



	/**
	 *
	 * Met à jour les donnees.
	 *
	 * @param lesObjets Tableau contenant des objets implémentant l'interface NSKeyValueCoding.
	 * @param displayKey Nom de la propriété qui doit permettre d'afficher le libella de chaque element de la liste déroulante (en NSKeyValueCoding). Sera affichee avec valueForKey().
	 * Si null, la valeur affichée sera obtenue via un toString().
	 * @param labelForNullValue si non null, une valeur sera affichee en debut de modele et getSelectedEObject renverra null si selectionne.
	 */

	public final void updateListWithData(final NSArray lesObjets)  {
//	    System.out.println("");
//	    System.out.println("-------------------");
//	    System.out.println("Mise a jour de la liste deroulante");
//	    System.out.println("NBObjets a mettre dans la liste : " + lesObjets.count());
//	    System.out.println("Objets a mettre dans la liste : " + lesObjets);
		//nettoyer
	    this.removeAllElements();

		if (lesObjets!=null) {
			
		
		myObjects =  new ArrayList();

		//Gerer l'affichage ou non d'un element null
		if (nullValue!=null) {
			myObjects.add(null);
			this.addElement(nullValue);
		}

		//Gerer l'affichage des infos
		for (int i = 0; i < lesObjets.objects().length; i++) {
			myObjects.add(lesObjets.objects()[i]);
		}

//		System.out.println("myObjects : " + myObjects);


//		Object tmp;
		for (int i = 0; i < lesObjets.count(); i++) {
			final Object tmp = lesObjets.objectAtIndex(i);
//			if ( !(tmp instanceof NSKeyValueCoding) ) {
//				throw new ZUtilException ("L'objet ne gere pas l'interface NSKeyValueCoding.");
//			}
			if (myDisplayKey==null) {
				this.addElement(((NSKeyValueCoding)tmp).toString());
			}
			else {
				final Object obj = ((NSKeyValueCoding)tmp).valueForKey(myDisplayKey);
				if (formatDisplay==null) {
				    this.addElement( obj.toString() );
				}
				else {
				    this.addElement( formatDisplay.format(obj));
				}

			}
		}
        }

//		System.out.println("nb obj dans la liste"+ this.getSize() );
//		System.out.println("-------------------");
	}


	/**
	 * Definit quel est l'element qui doit etre selectionne dans la liste deroulante.
	 * @param selectedObject
	 */
	public void setSelectedEObject(final NSKeyValueCoding selectedObject) {
		if (selectedObject==null) {
			if (nullValue==null) {
			    super.setSelectedItem(null);
			}
			else {
			    super.setSelectedItem(nullValue);
			}
		}
		else {
			if (formatDisplay==null) {
			    super.setSelectedItem(selectedObject.valueForKey(myDisplayKey));
			}
			else {
			    super.setSelectedItem( formatDisplay.format(selectedObject.valueForKey(myDisplayKey)));
			}

		}
	}


    public final ArrayList getMyObjects() {
        return myObjects;
    }

    
    
    






}
