package org.cocktail.corossol.client.zutil;

import java.text.DecimalFormat;

import javax.swing.JFormattedTextField.AbstractFormatterFactory;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

/**
 * Fournit des factory de formatage pour des types de champs.
 */
public class FormattedFieldProvider {

	protected FormattedFieldProvider() {
	}

	/**
	 * @return factory de formatage pour les montants
	 */
	public static AbstractFormatterFactory getMontantFormatterFactory() {
		return new DefaultFormatterFactory(new NumberFormatter(new DecimalFormat("#,###.###")), // defaut
		        new NumberFormatter(new DecimalFormat("#,###.##")), // display
		        new EditDecimalFormatter(new DecimalFormat("###.##"))); // edit
	}

	public static AbstractFormatterFactory getMontantAvecDecimalesFormatterFactory() {
		return new DefaultFormatterFactory(new NumberFormatter(new DecimalFormat("#,##0.00")), // defaut
				new NumberFormatter(new DecimalFormat("#,##0.00")), // display
				new EditDecimalFormatter(new DecimalFormat("##0.00"))); // edit
	}

}
