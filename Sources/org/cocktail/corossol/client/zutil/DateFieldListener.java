//
//  DateFieldListener.java
//
//  Created by jfguilla on Thu Oct 02 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//
package org.cocktail.corossol.client.zutil;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

import org.cocktail.corossol.client.nib.DateCtrl;

/**
 * Listener pour les JTextComponent (ex : JTextField) dans lesquels une date
 * (ddd,00) doit etre saisie. Controle et reformatte la date saisie. Quitte le
 * champ si rien n'a ete saisi. Reste sur le champ avec le texte selectionne si
 * une date erronnee a ete saisie.<br>
 * Ajouter le listener par l'appel de methode :<br>
 * <br>
 * <code>
 JTextField monTextField;<br>
<br>
 ...<br>
<br>
monTextField.addFocusListener(new DateFieldListener());<br>
...<br>
</code> <br>
 * <br>
 * C'est tout !
 */
public class DateFieldListener implements FocusListener {
	public void focusGained(final FocusEvent e) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// On sélectionne la valeur
				((JTextComponent) e.getComponent()).selectAll();
			}
		});
	}

	/**
	 * Methode appelee lorsque le focus quitte le champ. Elle est implementee
	 * pour realiser le controle de la saisie.
	 */
	public void focusLost(FocusEvent e) {
		dateStringControl((JTextComponent) e.getComponent());
	}

	private void dateStringControl(final JTextComponent textField) {
		String dateEntree, dateResult;
		// Si l'utilisateur n'a rien saisi, on ne l'agasse pas...
		if (textField.getText().equals(""))
			return;
		// Controle de la date saisie
		dateEntree = textField.getText();
		dateResult = DateCtrl.dateCompletion(dateEntree);
		if (dateResult == null || dateResult.equals("")) {
			// On reprend le focus si la date est erronée...
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					textField.requestFocus();
					textField.selectAll();
				}
			});
		} else {
			textField.setText(dateResult);
		}
	}

}
