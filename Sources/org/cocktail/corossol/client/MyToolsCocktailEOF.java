/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.corossol.client;


import org.cocktail.application.client.ApplicationCocktail;

import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;

public class MyToolsCocktailEOF {

	private ApplicationCocktail app =null;


	public NSDictionary executeStoredProcedure(String proc, NSDictionary dico) {
		NSDictionary codeRetour = (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session", "clientSideRequestMyExecuteStoredProcedure", new Class[]{String.class, NSDictionary.class}, new Object[]{proc, dico}, false);
		return codeRetour;
	}

	public NSDictionary returnValuesForLastStoredProcedureInvocation() {
		NSDictionary returnDico;
		returnDico = (NSDictionary)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestReturnValuesForLastStoredProcedureInvocation", null, null, false);
		return returnDico;
	}

	// Debut de transaction
	public boolean beginTransaction() {
		Boolean codeRetour;
		codeRetour = (Boolean)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestBeginTransaction", null, null, false);
		return codeRetour.booleanValue();
	}

	// Validation de transaction
	public boolean commitTransaction() {
		Boolean codeRetour;
		codeRetour = (Boolean)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestCommitTransaction", null, null, false);
		return codeRetour.booleanValue();
	}

	// Annulation de transaction
	public boolean rollbackTransaction() {
		Boolean codeRetour;
		codeRetour = (Boolean)((EODistributedObjectStore)getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(getApp().getAppEditingContext(), "session.remoteCallDelagate", "clientSideRequestRollbackTransaction", null, null, false);
		return codeRetour.booleanValue();
	}

	public MyToolsCocktailEOF(ApplicationCocktail app) {
		super();
		this.setApp(app);
	}

	void setApp(ApplicationCocktail app) {
		this.app = app;
	}

	ApplicationCocktail getApp() {
		return app;
	}
	
	public String loadSql(String name) {
		String sql = (String) ((EODistributedObjectStore) getApp().getAppEditingContext().parentObjectStore()).invokeRemoteMethodWithKeyPath(
				getApp().getAppEditingContext(), 
				"session", 
				"clientSideRequestEditionSqlQuery", 
				new Class[] {String.class}, 
				new Object[] {name}, 
				false);
		return sql;
	}
}
