package org.cocktail.corossol.client.nibctrl;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.finder.FinderInventaireComptable;
import org.cocktail.corossol.client.nib.ComptableInventaireRechercherNib;

import com.webobjects.foundation.NSMutableArray;

public class ComptableInventaireRechercherNibCtrl extends NibCtrl {

	// methodes
	private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_RECHERCHER = "actionRechercher";

	// les tables view
	private JTableViewCocktail inventairesTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup inventairesDG = new NSMutableArrayDisplayGroup();

	// le nib
	private ComptableInventaireRechercherNib monComptableInventaireRechercherNib = null;

	private ComptableInventaireInspecteurNibCtrl parentControleur=null;
	
	
	public ComptableInventaireRechercherNibCtrl(ApplicationCocktail ctrl, int alocation_x,
			int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(false);
	}
	
	public void creationFenetre(ComptableInventaireRechercherNib leComptableInventaireRechercherNib, String title,ComptableInventaireInspecteurNibCtrl parentControleur) {
		super.creationFenetre(leComptableInventaireRechercherNib, title);
		setMonComptableInventaireRechercherNib(leComptableInventaireRechercherNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
		this.parentControleur =  parentControleur;
	}
	
	public void afficherFenetre() {
		super.afficherFenetre();
		setComptableInventaireRechercherDG((EOCleInventaireComptable)parentControleur.getCurrentInventaireComptable().cleInventaireComptable());
	}

	private void bindingAndCustomization() {

		try {
			// creation de la table view des types de bordereaux
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Ub", 50,JLabel.CENTER, "clicComp", "String"));
			lesColumns.addObject(new ColumnData("Exercice", 50,JLabel.CENTER, "exercice.exeExercice", "String"));
			lesColumns.addObject(new ColumnData("Numero", 200,JLabel.CENTER, "clicNumComplet", "String"));

			setComptableInventaireRechercherDG(null);
			setInventairesTBV(new JTableViewCocktail(lesColumns, getInventairesDG(),
					new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonComptableInventaireRechercherNib().getInventairesTBV().initTableViewCocktail(getInventairesTBV());

			// multi ou mono selection dans les tbv
			getInventairesTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			// le cablage des  objets
			getMonComptableInventaireRechercherNib().getJButtonAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonComptableInventaireRechercherNib().getJButtonCocktailSelectionner().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
			
			// les images des objets
			getMonComptableInventaireRechercherNib().getJButtonCocktailSelectionner().setIcone(IconeCocktail.VALIDER);//"valider16");
			getMonComptableInventaireRechercherNib().getJButtonAnnuler().setIcone(IconeCocktail.FERMER);//"close_view");
			
			// obseration des changements de selection de lexercice :
			((ApplicationCorossol) app).getObserveurExerciceSelection()
					.addObserverForMessage(this, "changementExercice");

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
					e.getMessage(), true);
		}

		getInventairesTBV().refresh();

	}

	private void setComptableInventaireRechercherDG(EOCleInventaireComptable invccle) {
		getInventairesDG().removeAllObjects();
		if (getInventairesTBV() != null)
			getInventairesTBV().refresh();	

		try {
			if (invccle != null)
				getInventairesDG().addObjectsFromArray(FinderInventaireComptable.findLesClesInventaireComptables(app, invccle));

			if (getInventairesTBV() != null)
				getInventairesTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME setComptableInventaireRechercherDG ! \n"+e.toString());
		}

		
	}
	

	// actions
	public void actionAnnuler() {
		parentControleur.recupererCleInventaireComptable(null);
		masquerFenetre();
	}
	
	public void actionSelectionner() {
		parentControleur.recupererCleInventaireComptable((EOCleInventaireComptable)getInventairesTBV().getSelectedObjects().objectAtIndex(0));
		masquerFenetre();
	}
	
	
	

	public void changementExercice() {
		//		System.out.println("MODIFIER LES QUALIFIERS VIDER LES SELECTION ETC.......");

	}

	private NSMutableArrayDisplayGroup getInventairesDG() {
		return inventairesDG;
	}

	private void setInventairesDG(NSMutableArrayDisplayGroup inventairesDG) {
		this.inventairesDG = inventairesDG;
	}

	private JTableViewCocktail getInventairesTBV() {
		return inventairesTBV;
	}

	private void setInventairesTBV(JTableViewCocktail inventairesTBV) {
		this.inventairesTBV = inventairesTBV;
	}

	private ComptableInventaireRechercherNib getMonComptableInventaireRechercherNib() {
		return monComptableInventaireRechercherNib;
	}

	private void setMonComptableInventaireRechercherNib(
			ComptableInventaireRechercherNib monComptableInventaireRechercherNib) {
		this.monComptableInventaireRechercherNib = monComptableInventaireRechercherNib;
	}
}
