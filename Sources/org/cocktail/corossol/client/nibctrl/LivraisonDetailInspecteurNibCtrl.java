package org.cocktail.corossol.client.nibctrl;
import java.awt.Dimension;
import java.math.BigDecimal;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.UtilCtrl;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOVArticles;
import org.cocktail.corossol.client.eof.metier.EOVCommandeEngageCtrlPlanco;
import org.cocktail.corossol.client.eof.process.ProcessLivraisonDetail;
import org.cocktail.corossol.client.finder.FinderLivraisons;
import org.cocktail.corossol.client.nib.LivraisonDetailInspecteurNib;

import com.webobjects.foundation.NSMutableArray;

public class LivraisonDetailInspecteurNibCtrl extends NibCtrl {

	// methodes
	private static final String METHODE_ACTION_VALIDER = "actionValider";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_COPIECOLLE = "actionCopieColle";
	private static final String CREATION = "CREATION";
	private static final String MODIFICATION = "MODIFICATION";
	private static final String SUPPRESSION = "SUPPRESSION";

	private static final String METHODE_CHANGEMENT_SELECTION_ENGAGEMENT= "changementSelectionEngagement";

	private String currentAction = null;
	private static final boolean AVEC_LOGS = true;

	// les tables view
	private JTableViewCocktail mesEngagementsTBV = null;
	private JTableViewCocktail mesArticlesTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup mesEngagementsDG = new NSMutableArrayDisplayGroup();
	private NSMutableArrayDisplayGroup mesArticlesDG = new NSMutableArrayDisplayGroup();

	// le nib
	private LivraisonDetailInspecteurNib monLivraisonDetailInspecteurNib = null;

	// LivraisonNibCtrl parentCtrl
	private LivraisonNibCtrl parentCtrl = null;

	private ProcessLivraisonDetail monProcessLivraisonDetail =null;
	private EOLivraison currentLivraisonDetail = null;


	public LivraisonDetailInspecteurNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(AVEC_LOGS);
		setMonProcessLivraisonDetail(new ProcessLivraisonDetail(AVEC_LOGS));
	}

	public void creationFenetre(LivraisonDetailInspecteurNib leLivraisonDetailInspecteurNib, String title) {
		super.creationFenetre(leLivraisonDetailInspecteurNib, title);
		setMonLivraisonDetailInspecteurNib(leLivraisonDetailInspecteurNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void afficherFenetreCreationDetail() {
		if (getParentCtrl() == null){
			fenetreDeDialogueInformation("PROBLEME ! \n PAS DE CONTROLEUR PARENTS");
			return;
		}

		if (getParentCtrl().getMesLivraisonsTBV().getSelectedObjects().count() == 0){
			fenetreDeDialogueInformation("Il faut choisir une livraison !");
			return;
		}

		super.afficherFenetre();

		viderAffichageArticles();
		viderAffichageEngagements();

		setEngagementsDG();
		setArticlesDG();
		setCurrentLivraisonDetail(null);

		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonLivraisonDetailInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();

		if (getCurrentLivraisonDetail() != null)
			afficherCurrentLivraisonDetail();
		setCurrentAction(CREATION);
	}

	public void afficherFenetreModificationDetail() {
		if (getParentCtrl() == null){
			fenetreDeDialogueInformation("PROBLEME ! \n PAS DE CONTROLEUR PARENTS");
			return;
		}

		if (getParentCtrl().getMesDetailsLivraisonTBV().getSelectedObjects().count() == 0){
			fenetreDeDialogueInformation("Il faut choisir un article!");
			return;
		}

		super.afficherFenetre();
		getMesEngagementsDG().removeAllObjects();
		getMesArticlesDG().removeAllObjects();

		setCurrentLivraisonDetail((EOLivraison)getParentCtrl().getMesLivraisonsTBV().getSelectedObjects().objectAtIndex(0));
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonLivraisonDetailInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();

		if (getCurrentLivraisonDetail() != null)
			afficherCurrentLivraisonDetail();
		setCurrentAction(MODIFICATION);
	}

	public void afficherFenetreSuppressionDetail() {
		if (getParentCtrl() == null){
			fenetreDeDialogueInformation("PROBLEME ! \n PAS DE CONTROLEUR PARENTS");
			return;
		}

		if (getParentCtrl().getMesDetailsLivraisonTBV().getSelectedObjects().count() == 0){
			fenetreDeDialogueInformation("Il faut choisir un article!");
			return;
		}

		super.afficherFenetre();
		getMesEngagementsDG().removeAllObjects();
		getMesArticlesDG().removeAllObjects();

		setCurrentLivraisonDetail((EOLivraison)getParentCtrl().getMesLivraisonsTBV().getSelectedObjects().objectAtIndex(0));
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonLivraisonDetailInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();

		if (getCurrentLivraisonDetail() != null)
			afficherCurrentLivraisonDetail();
		setCurrentAction(SUPPRESSION);
	}

	private void bindingAndCustomization() {
		try {
			getMonLivraisonDetailInspecteurNib().getJFormattedTextFieldQte().setText("");
			getMonLivraisonDetailInspecteurNib().getJTextFieldMontant().setText("");
			getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle().setText("");
			
			// creation de la table view des engagements
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Imputation", 80,JLabel.CENTER, "planComptable.pcoNum", "String"));
			lesColumns.addObject(new ColumnData("Credits", 50,JLabel.CENTER, "typeCredit.tcdCode", "String"));
			lesColumns.addObject(new ColumnData("Ub", 50,JLabel.CENTER, "organ.orgUb", "String"));
			lesColumns.addObject(new ColumnData("Cr", 80,JLabel.CENTER, "organ.orgCr", "String"));
			lesColumns.addObject(new ColumnData("Sous-Cr", 80,JLabel.CENTER, "organ.orrSouscr", "String"));

			setEngagementsDG();
			setMesEngagementsTBV(new JTableViewCocktail(lesColumns, getMesEngagementsDG(),
					new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonLivraisonDetailInspecteurNib().getMesEngagementsTBV().initTableViewCocktail(getMesEngagementsTBV());

			// creation de la table view des articles
			NSMutableArray lesColumnsArticles = new NSMutableArray();
			lesColumnsArticles.addObject(new ColumnData("Libelle", 100,JLabel.CENTER, "artLibelle", "String"));
			lesColumnsArticles.addObject(new ColumnData("Ttc", 100,JLabel.CENTER, "artPrixTotalTtc", "String"));
			lesColumnsArticles.addObject(new ColumnData("Qte Cde", 100,JLabel.CENTER, "artQuantite", "String"));

			setArticlesDG();
			setMesArticlesTBV(new JTableViewCocktail(lesColumnsArticles, getMesArticlesDG(),
					new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonLivraisonDetailInspecteurNib().getMesArticlesTBV().initTableViewCocktail(getMesArticlesTBV());


			// multi ou mono selection dans les tbv
			getMesEngagementsTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			getMesArticlesTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			// le cablage des  objets
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailValider().addDelegateActionListener(this, METHODE_ACTION_VALIDER);
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailCopierColler().addDelegateActionListener(this, METHODE_ACTION_COPIECOLLE);

			// les listeners
			getMesArticlesTBV().addDelegateSelectionListener(this,METHODE_CHANGEMENT_SELECTION_ENGAGEMENT);
			getMesArticlesTBV().addDelegateKeyListener(this,METHODE_CHANGEMENT_SELECTION_ENGAGEMENT);


			// les images des objets
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailValider().setIcone(IconeCocktail.VALIDER);//"valider16");
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailAnnuler().setIcone(IconeCocktail.FERMER);//"close_view");
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailCopierColler().setIcone(IconeCocktail.COPIECOLLE);//"copiecolle");

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonLivraisonDetailInspecteurNib());

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol) app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}
		getMesEngagementsTBV().refresh();
	}

	// methodes cablage
	public void actionAnnuler() {
		app.addLesPanelsModal(getMonLivraisonDetailInspecteurNib());
		((ApplicationCorossol) app).retirerLesGlassPane();
		app.getAppEditingContext().revert();
		getParentCtrl().inspecteurAnnuler(this);
		masquerFenetre();
	}
    
	public void actionValider (){	
       
		BigDecimal qte=null;
		BigDecimal montant=null;
		
		/*
         * Faire des contrôles ici
         * */
		
		if (getCurrentAction().equals(CREATION)) {
		   if (getMesEngagementsTBV().getSelectedObjects().count() == 0) {
				fenetreDeDialogueInformation("PROBLEME ! \n Vous devez choisir un engagement !");
				return;
		   }
		   if 	(getMesArticlesTBV().getSelectedObjects().count()==0) {
			   fenetreDeDialogueInformation("PROBLEME ! \n Vous devez choisir un article !");
			   return;
		   }
		}
		
		if (getCurrentAction().equals(CREATION) || getCurrentAction().equals(MODIFICATION)) {
		
		   try {
			qte = UtilCtrl.stringToBigDecimal(getMonLivraisonDetailInspecteurNib().getJFormattedTextFieldQte().getText());
			if (qte==null || qte.compareTo(BigDecimal.ZERO)<=0)throw new Exception (); 
		   } catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n Vous devez entrez une quantité valide !");
			return;
		   }
		   
		   String s = getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle().getText();
		   if(s==null || s.length()==0) {
			   fenetreDeDialogueInformation("PROBLEME ! \n Vous devez entrer un libellé");
			   return ; 
		   }
		   try {
				montant = UtilCtrl.stringToBigDecimal(getMonLivraisonDetailInspecteurNib().getJTextFieldMontant().getText());
				 if (montant==null || montant.compareTo(BigDecimal.ZERO)<0) throw new Exception (); 
				 montant = montant.setScale(2, BigDecimal.ROUND_HALF_UP);
			  } catch (Exception e) {
				fenetreDeDialogueInformation("PROBLEME ! \n Vous devez entrez un montant valide !");
				return;
			}  
		}
		
		/* On process ... */
		app.addLesPanelsModal(getMonLivraisonDetailInspecteurNib());
		((ApplicationCorossol) app).retirerLesGlassPane();		
		try {
			if (getCurrentAction().equals(CREATION))
				getMonProcessLivraisonDetail().ajouterUnLivraisonDetail( app.getAppEditingContext(),
						(EOLivraison)getParentCtrl().getMesLivraisonsTBV().getSelectedObjects().objectAtIndex(0),
						((EOVCommandeEngageCtrlPlanco)getMesEngagementsTBV().getSelectedObjects().objectAtIndex(0)).organ(),
						((EOVCommandeEngageCtrlPlanco)getMesEngagementsTBV().getSelectedObjects().objectAtIndex(0)).typeCredit(),
						((EOVCommandeEngageCtrlPlanco)getMesEngagementsTBV().getSelectedObjects().objectAtIndex(0)).planComptable(),
						getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle().getText(), montant, qte);

			if (getCurrentAction().equals(MODIFICATION))
				getMonProcessLivraisonDetail().modifierUnLivraisonDetail( app.getAppEditingContext(),
						(EOLivraisonDetail)getParentCtrl().getMesDetailsLivraisonTBV().getSelectedObjects().objectAtIndex(0),
						getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle().getText(), montant, qte);

			if (getCurrentAction().equals(SUPPRESSION))
				getMonProcessLivraisonDetail().supprimerUnLivraisonDetail( app.getAppEditingContext(),
						(EOLivraisonDetail)getParentCtrl().getMesDetailsLivraisonTBV().getSelectedObjects().objectAtIndex(0));

			app.getAppEditingContext().saveChanges();
			getParentCtrl().inspecteurFermer(this);
		} catch (Throwable e) {
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
			e.printStackTrace();	
			getParentCtrl().inspecteurFermer(this);	
		}
		masquerFenetre();
	}

	public void actionCopieColle(){
		EOVCommandeEngageCtrlPlanco tmpEngagement = null;
		EOVArticles tmpArticle = null;
	    BigDecimal tapTaux=null;

		// on recopie les informations des engagements
		if(getMesEngagementsTBV().getSelectedObjects().count() == 1){
			tmpEngagement = (EOVCommandeEngageCtrlPlanco)getMesEngagementsTBV().getSelectedObjects().objectAtIndex(0);
			getMonLivraisonDetailInspecteurNib().getJTextFieldCocktailImputation().setText(tmpEngagement.planComptable().pcoNum());
			getMonLivraisonDetailInspecteurNib().getJTextFieldCocktailCredit().setText(tmpEngagement.typeCredit().tcdCode());
			getMonLivraisonDetailInspecteurNib().getJTextFieldCocktailLigneBudgetaire().setText(
					tmpEngagement.organ().orgUb()+" - "+tmpEngagement.organ().orgCr()+" - "+tmpEngagement.organ().orgSouscr());
		}

		// on recopie les informations de  l article
		if(getMesArticlesTBV().getSelectedObjects().count() == 1)		{
			tmpArticle = (EOVArticles)getMesArticlesTBV().getSelectedObjects().objectAtIndex(0);
			getMonLivraisonDetailInspecteurNib().getJFormattedTextFieldQte().setValue(tmpArticle.artQuantite());
			getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle().setText(tmpArticle.artLibelle());

			// Remplacement du montant TTC de l'article par son montant budgetaire
			if (tmpEngagement!=null)
				tapTaux=tmpEngagement.tapTaux();
			BigDecimal montant = montantBudgetaire(tapTaux, tmpArticle.artPrixHt(), tmpArticle.artPrixTtc());
			getMonLivraisonDetailInspecteurNib().getJTextFieldMontant().setText((montant==null ? "": montant.toString()));
		}
	}

	private BigDecimal montantBudgetaire(BigDecimal tapTaux, BigDecimal ht, BigDecimal ttc) {
		if (tapTaux==null || ht==null || ttc==null)
			return ttc;

		if (ttc.equals(ht))
			return ttc;
		if (tapTaux.floatValue()==100.0)
			return ht;
		if (tapTaux.floatValue()==0.0)
			return ttc;

		BigDecimal tva=ttc.subtract(ht);
		return ht.add(tva.multiply(new BigDecimal(1.0).subtract(tapTaux.divide(new BigDecimal(100.0), 4, BigDecimal.ROUND_HALF_UP)).
				setScale(4, BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public void changementExercice() {
	}

	private void setArticlesDG() {
		try {
			getMesArticlesDG().removeAllObjects();
			if (((ApplicationCorossol)app).getCurrentCommande() != null)
				getMesArticlesDG().addObjectsFromArray(FinderLivraisons.findLesArticles(app));
			if (getMesArticlesTBV() != null)
				getMesArticlesTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
	}

	private void setEngagementsDG() {
		try {
			getMesEngagementsDG().removeAllObjects();
			if (((ApplicationCorossol)app).getCurrentCommande() != null)
				getMesEngagementsDG().addObjectsFromArray(FinderLivraisons.findLesEngagements(app));
			if (getMesEngagementsTBV() != null)
				getMesEngagementsTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
	}

	// changements de selection
	public void changementSelectionEngagement(){
	}

	// affichage 
	private void preparerInterface(){
		getFrameMain().setTitle(getCurrentAction()+" : "+getParentCtrl().FENETRE_DE_CREATION_LIVRAISON_DETAIL);

		if (getCurrentAction().equals(CREATION)){
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJButtonCocktailCopierColler(),true);
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJTextFieldMontant(),true);
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJFormattedTextFieldQte(),true);
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle(),true);
			setEnabledComposant(getMesArticlesTBV().getTable(),true);
			setEnabledComposant(getMesEngagementsTBV().getTable(),true);
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailValider().setText("Valider");
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailValider().setIcone("add_obj");
		}

		if (getCurrentAction().equals(MODIFICATION)){
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJButtonCocktailCopierColler(),false);
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJTextFieldMontant(),true);
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJFormattedTextFieldQte(),true);
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle(),true);
			setEnabledComposant(getMesArticlesTBV().getTable(),false);
			setEnabledComposant(getMesEngagementsTBV().getTable(),false);
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailValider().setText("Modifier");
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailValider().setIcone("refresh");
		}

		if (getCurrentAction().equals(SUPPRESSION)){
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJButtonCocktailCopierColler(),false);
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJTextFieldMontant(),false);
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJFormattedTextFieldQte(),false);
			setEnabledComposant(getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle(),false);
			setEnabledComposant(getMesArticlesTBV().getTable(),false);
			setEnabledComposant(getMesEngagementsTBV().getTable(),false);
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailValider().setText("Supprimer");
			getMonLivraisonDetailInspecteurNib().getJButtonCocktailValider().setIcone("delete_obj");
		}
	}

	private void afficherCurrentLivraisonDetail() {
		viderAffichageArticles();
		viderAffichageEngagements();

		EOLivraisonDetail tmpLivraisonDetail = (EOLivraisonDetail)getParentCtrl().getMesDetailsLivraisonTBV().getSelectedObjects().objectAtIndex(0);

		getMonLivraisonDetailInspecteurNib().getJTextFieldCocktailImputation().setText(tmpLivraisonDetail.planComptable().pcoNum());
		getMonLivraisonDetailInspecteurNib().getJTextFieldCocktailCredit().setText(tmpLivraisonDetail.typeCredit().tcdCode());
		getMonLivraisonDetailInspecteurNib().getJTextFieldCocktailLigneBudgetaire().setText(
				tmpLivraisonDetail.organ().orgUb()+" - "+tmpLivraisonDetail.organ().orgCr()+" - "+tmpLivraisonDetail.organ().orgSouscr());

		getMonLivraisonDetailInspecteurNib().getJFormattedTextFieldQte().setValue(new BigDecimal(tmpLivraisonDetail.lidQuantite()));
		getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle().setText(tmpLivraisonDetail.lidLibelle());
		getMonLivraisonDetailInspecteurNib().getJTextFieldMontant().setText((tmpLivraisonDetail.lidMontant()==null ? "" : tmpLivraisonDetail.lidMontant().toString()));
	}

	private void viderAffichageArticles() {
		getMonLivraisonDetailInspecteurNib().getJTextFieldMontant().setText("");
		getMonLivraisonDetailInspecteurNib().getJFormattedTextFieldQte().setText("");
		getMonLivraisonDetailInspecteurNib().getJTextAreaLibelle().setText("");
	}

	private void viderAffichageEngagements(){
		getMonLivraisonDetailInspecteurNib().getJTextFieldCocktailCredit().setText("");
		getMonLivraisonDetailInspecteurNib().getJTextFieldCocktailImputation().setText("");
		getMonLivraisonDetailInspecteurNib().getJTextFieldCocktailLigneBudgetaire().setText("");
	}

	// accesseur
	public LivraisonNibCtrl setParentCtrl(LivraisonNibCtrl parentCtrl) {
		return this.parentCtrl = parentCtrl;
	}

	private NSMutableArrayDisplayGroup getMesEngagementsDG() {
		return mesEngagementsDG;
	}

	private void setMesEngagementsDG(NSMutableArrayDisplayGroup mesEngagementsDG) {
		this.mesEngagementsDG = mesEngagementsDG;
	}

	private JTableViewCocktail getMesEngagementsTBV() {
		return mesEngagementsTBV;
	}

	private void setMesEngagementsTBV(JTableViewCocktail mesEngagementsTBV) {
		this.mesEngagementsTBV = mesEngagementsTBV;
	}

	private LivraisonDetailInspecteurNib getMonLivraisonDetailInspecteurNib() {
		return monLivraisonDetailInspecteurNib;
	}

	private void setMonLivraisonDetailInspecteurNib(
			LivraisonDetailInspecteurNib monLivraisonDetailInspecteurNib) {
		this.monLivraisonDetailInspecteurNib = monLivraisonDetailInspecteurNib;
	}

	private JTableViewCocktail getMesArticlesTBV() {
		return mesArticlesTBV;
	}

	private void setMesArticlesTBV(JTableViewCocktail mesArticlesTBV) {
		this.mesArticlesTBV = mesArticlesTBV;
	}

	private NSMutableArrayDisplayGroup getMesArticlesDG() {
		return mesArticlesDG;
	}

	private void setMesArticlesDG(NSMutableArrayDisplayGroup mesArticlesDG) {
		this.mesArticlesDG = mesArticlesDG;
	}

	private LivraisonNibCtrl getParentCtrl() {
		return parentCtrl;
	}

	private EOLivraison getCurrentLivraisonDetail() {
		return currentLivraisonDetail;
	}

	private void setCurrentLivraisonDetail(EOLivraison currentLivraisonDetail) {
		this.currentLivraisonDetail = currentLivraisonDetail;
	}

	private ProcessLivraisonDetail getMonProcessLivraisonDetail() {
		return monProcessLivraisonDetail;
	}

	private void setMonProcessLivraisonDetail(
			ProcessLivraisonDetail monProcessLivraisonDetail) {
		this.monProcessLivraisonDetail = monProcessLivraisonDetail;
	}

	private String getCurrentAction() {
		return currentAction;
	}

	private void setCurrentAction(String currentAction) {
		this.currentAction = currentAction;
		preparerInterface();
	}
}
