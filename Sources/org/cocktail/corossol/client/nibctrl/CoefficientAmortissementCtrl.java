package org.cocktail.corossol.client.nibctrl;

import java.awt.event.ActionEvent;
import java.math.BigDecimal;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EODegressif;
import org.cocktail.corossol.client.eof.metier.EODegressifCoef;
import org.cocktail.corossol.client.eof.procedure.ProcedureEnregistrerDegressif;
import org.cocktail.corossol.client.eof.procedure.ProcedureSupprimerDegressif;
import org.cocktail.corossol.client.finder.FinderDegressif;
import org.cocktail.corossol.client.nib.CoefficientAmortissement;
import org.cocktail.corossol.client.nib.DateCtrl;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CoefficientAmortissementCtrl {

	private ApplicationCorossol	myapp;

	private CoefficientAmortissement coefficientAmortissement;
	private EODegressif currentDegressif;
	private EODegressifCoef currentDegressifCoef;
	
	public CoefficientAmortissementCtrl(ApplicationCocktail ctrl) {
		super();
		myapp = (ApplicationCorossol)ctrl;

		coefficientAmortissement=new CoefficientAmortissement();
		initGui();

		setCurrentDegressif(null);
	}

	private void initGui() {
		coefficientAmortissement.btNouveau().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				setCurrentDegressif(EODegressif.createDegressif(myapp.getAppEditingContext(),null));
				if (currentDegressif!=null)
					coefficientAmortissement.panneauSuivant();
			} });
		coefficientAmortissement.btModifier().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				setCurrentDegressif(coefficientAmortissement.degressifSelectionne());
				if (currentDegressif!=null)
					coefficientAmortissement.panneauSuivant();
			} });
		coefficientAmortissement.btAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				setCurrentDegressif(null);
				myapp.getAppEditingContext().revert();
				coefficientAmortissement.panneauPrecedent();
			} });
		coefficientAmortissement.btEnregistrer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				enregistrer();
			} });
		coefficientAmortissement.btSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				supprimer();
			} });
		
		coefficientAmortissement.tableDegressif().addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if (coefficientAmortissement.degressifSelectionne()==null)
					coefficientAmortissement.setDegressifCoef(new NSArray());
				else
					coefficientAmortissement.setDegressifCoef(coefficientAmortissement.degressifSelectionne().degressifCoefs());
				
				if(e.getClickCount() == 2)
					coefficientAmortissement.btModifier().doClick();
			} });
		coefficientAmortissement.btAjouterCoef().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				ajouterCoef();
			} });
		coefficientAmortissement.btModifierCoef().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				modifierCoef();
			} });
		coefficientAmortissement.btSupprimerCoef().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				supprimerCoef();
			} });
		coefficientAmortissement.btAnnulerCoef().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				annulerCoef();
			} });
		coefficientAmortissement.btValiderCoef().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				validerCoef();
			} });
	}

	private void ajouterCoef() {
		setCurrentDegressifCoef(EODegressifCoef.createDegressifCoef(myapp.getAppEditingContext(),null,null,null));
		currentDegressifCoef.setDegressifRelationship(currentDegressif);
		coefficientAmortissement.panneauSaisieCoef();
	}
	
	private void modifierCoef() {
		setCurrentDegressifCoef(coefficientAmortissement.modifDegressifCoefSelectionne());
		coefficientAmortissement.panneauSaisieCoef();
	}
	
	private void supprimerCoef() {
		EODegressifCoef coef=coefficientAmortissement.modifDegressifCoefSelectionne();
		if (coef==null)
			return;
		if (!EODialogs.runConfirmOperationDialog("Suppression", "Voulez-vous vraiment supprimer ce coefficient ?", "OUI", "NON"))
			return;
		coef.setDegressifRelationship(null);
		coefficientAmortissement.setModifDegressifCoef(currentDegressif.degressifCoefs());
	}
	
	private void annulerCoef() {
		setCurrentDegressifCoef(null);
		coefficientAmortissement.panneauConsultationCoef();
	}
	
	private void validerCoef() {
		currentDegressifCoef.setDgcoDureeMin(new Integer(coefficientAmortissement.getTfCoefDureeMin().getText()));
		currentDegressifCoef.setDgcoDureeMax(new Integer(coefficientAmortissement.getTfCoefDureeMax().getText()));
		currentDegressifCoef.setDgcoCoef(new BigDecimal(coefficientAmortissement.getTfCoefficient().getText()));
		
		coefficientAmortissement.setModifDegressifCoef(currentDegressif.degressifCoefs());
		coefficientAmortissement.panneauConsultationCoef();
	}

	private void setCurrentDegressif(EODegressif degressif) {
		currentDegressif=degressif;
		//if (currentDegressif!=null && currentDegressif.degressifCoefs()==null)
		//	currentDegressif.degressifCoefs().setDegressifCoefs(new NSMutableArray());

		coefficientAmortissement.afficherDegressif(degressif);
	}

	private void setCurrentDegressifCoef(EODegressifCoef degressifCoef) {
		currentDegressifCoef=degressifCoef;
		coefficientAmortissement.afficherDegressifCoef(degressifCoef);
	}

	public void afficherFenetre() {
		coefficientAmortissement.setVisible(true);//show();
		coefficientAmortissement.setDegressif(FinderDegressif.findAll(myapp));
	}

	private void enregistrer() {
		if (currentDegressif==null)
			return;

		currentDegressif.setDgrfDateDebut(DateCtrl.stringToDate(coefficientAmortissement.getTfDegressifDateDebut().getText(),"%d/%m/%Y"));
		currentDegressif.setDgrfDateFin(DateCtrl.stringToDate(coefficientAmortissement.getTfDegressifDateFin().getText(),"%d/%m/%Y"));

		try {
			ProcedureEnregistrerDegressif.enregistrer(myapp, currentDegressif);
			coefficientAmortissement.setDegressif(FinderDegressif.findAll(myapp));
			myapp.getAppEditingContext().revert();
			coefficientAmortissement.panneauPrecedent();
		}
		catch (Exception ex) {
			myapp.getAppEditingContext().revert();
			System.out.println(ex);
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}
	}

	private void supprimer() {
		if (!EODialogs.runConfirmOperationDialog("Suppression", "Voulez-vous vraiment supprimer ces taux degressifs ?", "OUI", "NON"))
			return;
		try {
			ProcedureSupprimerDegressif.enregistrer(myapp, currentDegressif);
			coefficientAmortissement.setDegressif(FinderDegressif.findAll(myapp));
			coefficientAmortissement.panneauPrecedent();
		}
		catch (Exception ex) {
			System.out.println(ex);
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}
	}
}
