package org.cocktail.corossol.client.nibctrl;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;

import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.factory.FactoryAmortissement;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie;
import org.cocktail.corossol.client.eof.metier.EOTypeSortie;
import org.cocktail.corossol.client.eof.procedure.ProcedureEnregisrerSortieComptable;
import org.cocktail.corossol.client.eof.procedure.ProcedureSupprimerSortieComptable;
import org.cocktail.corossol.client.finder.FinderTypeSortie;
import org.cocktail.corossol.client.nib.DateCtrl;
import org.cocktail.corossol.client.nib.SortieComptableNib;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSArray;


public class SortieComptableNibCtrl {

	private ApplicationCorossol	myapp;

	private SortieComptableNib sortieComptableNib;
	private EOInventaireComptable currentInventaireComptable;
	private EOInventaireComptableSortie currentInventaireComptableSortie;
	private IntegrationNibCtrl currentIntegrationNibCtrl;

	public SortieComptableNibCtrl(ApplicationCorossol ctrl,IntegrationNibCtrl integrationNibCtrl) {
		super();
		myapp = ctrl;

		sortieComptableNib=new SortieComptableNib();
		setCurrentInventaireComptable(null, null);
		currentIntegrationNibCtrl=integrationNibCtrl;

		initGui();
	}

	private void initGui() {
		sortieComptableNib.remplirPopupTypeSortie(FinderTypeSortie.findAll(myapp));
		sortieComptableNib.btEnregistrer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				enregistrer();
			} });
		sortieComptableNib.tfDateSortie().addKeyListener( new KeyListener(){
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()== KeyEvent.VK_ENTER) {
					sortieComptableNib.btCalculer().doClick();
				}
			}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){} });
		sortieComptableNib.btCalculer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (currentInventaireComptable==null || currentInventaireComptableSortie==null)
					return;
				currentInventaireComptableSortie.setInvcsDate(DateCtrl.stringToDate(sortieComptableNib.tfDateSortie().getText(),"%d/%m/%Y"));
				currentInventaireComptableSortie.setInvcsMontantAcquisitionSortie(new BigDecimal(sortieComptableNib.tfMontantAcquisitionSortie().getText()));

				if (currentInventaireComptableSortie.invcsDate()==null || currentInventaireComptableSortie.invcsMontantAcquisitionSortie()==null)
					return;
				currentInventaireComptableSortie.setInvcsVnc(FactoryAmortissement.precalculVNC(currentInventaireComptable, 
						currentInventaireComptableSortie.invcsMontantAcquisitionSortie(),currentInventaireComptableSortie.invcsDate()));

				sortieComptableNib.tfValeurNetteComptable().setText(currentInventaireComptableSortie.invcsVnc().toString());
			} });
	}

	public void setCurrentInventaireComptable(EOInventaireComptable inventaire, EOInventaireComptableSortie inventaireSortie) {
		currentInventaireComptable=inventaire;
		if (currentInventaireComptable==null)
			currentInventaireComptableSortie=null;
		else
			if (inventaireSortie==null)
				currentInventaireComptableSortie=EOInventaireComptableSortie.createInventaireComptableSortie(myapp.getAppEditingContext(),null,null,null,null,null,null);
			else
				currentInventaireComptableSortie=inventaireSortie;

		sortieComptableNib.afficherInventaire(currentInventaireComptable, currentInventaireComptableSortie);
	}

	public void afficherFenetre() {
		sortieComptableNib.show();
	}

	private void enregistrer() {
		if (currentInventaireComptableSortie==null)
			return;

		try {
			
			if (sortieComptableNib.tfMontantAcquisitionSortie().getText().length() > 0)
				currentInventaireComptableSortie.setInvcsMontantAcquisitionSortie(new BigDecimal(sortieComptableNib.tfMontantAcquisitionSortie().getText()));
			if (sortieComptableNib.tfValeurNetteComptable().getText().length() > 0)
				currentInventaireComptableSortie.setInvcsVnc(new BigDecimal(sortieComptableNib.tfValeurNetteComptable().getText()));
			if (sortieComptableNib.tfValeurCession().getText().length() > 0)
				currentInventaireComptableSortie.setInvcsValeurCession(new BigDecimal(sortieComptableNib.tfValeurCession().getText()));

			currentInventaireComptableSortie.setInvcsMotif(sortieComptableNib.tfMotifSortie().getText());
			currentInventaireComptableSortie.setInvcsDate(DateCtrl.stringToDate(sortieComptableNib.tfDateSortie().getText(),"%d/%m/%Y"));
			currentInventaireComptableSortie.setTypeSortieRelationship((EOTypeSortie)sortieComptableNib.popupTypeSortie().getSelectedItem());
			currentInventaireComptableSortie.setInventaireComptableRelationship(currentInventaireComptable);

			ProcedureEnregisrerSortieComptable.enregistrer(myapp, currentInventaireComptableSortie);
			sortieComptableNib.hide();
		}
		catch (NumberFormatException nfe) {
			System.out.println(nfe);
			EODialogs.runErrorDialog("ERREUR", "Un ou plusieurs montants ne sont pas au format numérique.");
			return;
		}
		catch (Exception ex) {
			System.out.println(ex);
			currentInventaireComptableSortie.setInventaireComptableRelationship(null);
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}

		myapp.getAppEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(myapp.getAppEditingContext().globalIDForObject(currentInventaireComptable)));
		myapp.getAppEditingContext().revert();
		myapp.getAppEditingContext().saveChanges();
		
		if (currentIntegrationNibCtrl!=null)
			currentIntegrationNibCtrl.actionSelectionner();
	}

	public void supprimer() {
		if (!EODialogs.runConfirmOperationDialog("Suppression", "Voulez-vous vraiment supprimer cette sortie d'inventaire ?", "OUI", "NON"))
			return;

		if (currentInventaireComptableSortie==null || myapp.getAppEditingContext().globalIDForObject(currentInventaireComptableSortie)==null || 
				myapp.getAppEditingContext().globalIDForObject(currentInventaireComptableSortie).isTemporary())
			return;

		try {
			if (ProcedureSupprimerSortieComptable.enregistrer(myapp, currentInventaireComptableSortie)) {
				if (currentInventaireComptable!=null) {
					myapp.getAppEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(myapp.getAppEditingContext().globalIDForObject(currentInventaireComptable)));
					myapp.getAppEditingContext().revert();
					myapp.getAppEditingContext().saveChanges();
				}
				if (currentIntegrationNibCtrl!=null)
					currentIntegrationNibCtrl.actionSelectionner();
			}
		}
		catch (Exception ex) {
			System.out.println(ex);
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}
	}
}
