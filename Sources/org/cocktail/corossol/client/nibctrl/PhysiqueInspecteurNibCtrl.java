package org.cocktail.corossol.client.nibctrl;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.nib.PhysiqueInspecteurNib;

public class PhysiqueInspecteurNibCtrl extends NibCtrl {

	// methodes
	private static final String METHODE_ACTION_FERMER = "actionFermer";
	private static final String METHODE_ACTION_VALIDER = "actionValider";

	private PhysiqueNibCtrl parentCtrl =null;

	private EOInventaire currentInventaire =null;
	// le nib
	private PhysiqueInspecteurNib monPhysiqueInspecteurNib = null;

	public PhysiqueInspecteurNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(false);
	}

	public void creationFenetre(PhysiqueInspecteurNib lePhysiqueInspecteurNib, String title,PhysiqueNibCtrl parentCtrl) {
		super.creationFenetre(lePhysiqueInspecteurNib, title);
		setMonPhysiqueInspecteurNib(lePhysiqueInspecteurNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
		this.parentCtrl = parentCtrl;
	}

	public void afficherFenetre() {
		super.afficherFenetre();
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonPhysiqueInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();
		this.currentInventaire = (EOInventaire)getParentCtrl().getMesBiensTBV().getSelectedObjects().objectAtIndex(0);
		afficherInspecteurInventaire();
	}

	private void bindingAndCustomization() {
		try {
			// le cablage des  objets
			getMonPhysiqueInspecteurNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_FERMER);
			getMonPhysiqueInspecteurNib().getJButtonCocktailValider().addDelegateActionListener(this, METHODE_ACTION_VALIDER);

			// les images des objets
			getMonPhysiqueInspecteurNib().getJButtonCocktailValider().setIcone(IconeCocktail.VALIDER);//"valider16");
			getMonPhysiqueInspecteurNib().getJButtonCocktailAnnuler().setIcone(IconeCocktail.FERMER);//"close_view");

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonPhysiqueInspecteurNib());
		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}
	}

	// methodes cablage
	public void actionFermer() {
		trace(METHODE_ACTION_FERMER);
		app.getAppEditingContext().revert();
		app.addLesPanelsModal(getMonPhysiqueInspecteurNib());
		((ApplicationCorossol) app).retirerLesGlassPane();
		getParentCtrl().inspecteurAnnuler(this);
		viderInspecteurInventaire();
		masquerFenetre();
	}

	public void actionValider() {
		trace(METHODE_ACTION_VALIDER);
		app.addLesPanelsModal(getMonPhysiqueInspecteurNib());
		((ApplicationCorossol) app).retirerLesGlassPane();
		
		try {
			getCurrentInventaire().setInvDoc(getMonPhysiqueInspecteurNib().getJTextFieldCocktailDocumentation().getText());
			getCurrentInventaire().setInvSerie(getMonPhysiqueInspecteurNib().getJTextFieldCocktailSerie().getText());
			getCurrentInventaire().setInvModele(getMonPhysiqueInspecteurNib().getJTextFieldCocktailModele().getText());
			getCurrentInventaire().setInvMaint(getMonPhysiqueInspecteurNib().getJTextFieldCocktailMaintenance().getText());
			getCurrentInventaire().setInvGar(getMonPhysiqueInspecteurNib().getJTextFieldCocktailGarantie().getText());
			getCurrentInventaire().setInvCommentaire(getMonPhysiqueInspecteurNib().getJTextAreaCommentaire().getText());
			getCurrentInventaire().setCodeBarre(getMonPhysiqueInspecteurNib().getJTextFieldCocktailCodeBarre().getText());
			
			app.getAppEditingContext().saveChanges();
			getParentCtrl().inspecteurFermer(this);
			viderInspecteurInventaire();
			masquerFenetre();
		} catch (Exception e) {
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
			getParentCtrl().inspecteurAnnuler(this);
			masquerFenetre();
		}
	}

	//affichaege
	private void afficherInspecteurInventaire() {
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailDocumentation().setText(getCurrentInventaire().invDoc());
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailSerie().setText(getCurrentInventaire().invSerie());
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailModele().setText(getCurrentInventaire().invModele());
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailMaintenance().setText(getCurrentInventaire().invMaint());
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailGarantie().setText(getCurrentInventaire().invGar());
		getMonPhysiqueInspecteurNib().getJTextAreaCommentaire().setText(getCurrentInventaire().invCommentaire());
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailCodeBarre().setText(getCurrentInventaire().codeBarre());
	}

	private void viderInspecteurInventaire() {
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailDocumentation().setText("");
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailSerie().setText("");
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailModele().setText("");
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailMaintenance().setText("");
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailGarantie().setText("");
		getMonPhysiqueInspecteurNib().getJTextAreaCommentaire().setText("");
		getMonPhysiqueInspecteurNib().getJTextFieldCocktailCodeBarre().setText("");
	}

	public void changementExercice() {
		//		System.out.println("MODIFIER LES QUALIFIERS VIDER LES SELECTION ETC.......");
	}

	private EOInventaire getCurrentInventaire() {
		return currentInventaire;
	}

	private void setCurrentInventaire(EOInventaire currentInventaire) {
		this.currentInventaire = currentInventaire;
	}

	private PhysiqueInspecteurNib getMonPhysiqueInspecteurNib() {
		return monPhysiqueInspecteurNib;
	}

	private void setMonPhysiqueInspecteurNib(
			PhysiqueInspecteurNib monPhysiqueInspecteurNib) {
		this.monPhysiqueInspecteurNib = monPhysiqueInspecteurNib;
	}

	private PhysiqueNibCtrl getParentCtrl() {
		return parentCtrl;
	}

	private void setParentCtrl(PhysiqueNibCtrl parentCtrl) {
		this.parentCtrl = parentCtrl;
	}
}
