package org.cocktail.corossol.client.nibctrl;

/* Copyright Cocktail, 2001-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOPlanComptableAmo;
import org.cocktail.application.client.swingfinder.SwingFinderInterface;
import org.cocktail.application.client.swingfinder.SwingFinderPlanComptable;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.UtilCtrl;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;
import org.cocktail.corossol.client.eof.metier.EOOrigineFinancement;
import org.cocktail.corossol.client.eof.procedure.ProcedureEnregistrerHb;
import org.cocktail.corossol.client.eof.procedure.ProcedureSupprimerHb;
import org.cocktail.corossol.client.eof.process.ProcessInventaireNonBudgetaire;
import org.cocktail.corossol.client.finder.FinderComptable;
import org.cocktail.corossol.client.finder.FinderInventaireNonBudgetaire;
import org.cocktail.corossol.client.nib.DateCtrl;
import org.cocktail.corossol.client.nib.IntegrationNib;
import org.cocktail.corossol.client.zutil.DateFieldListener;
import org.cocktail.corossol.client.zutil.FormattedFieldProvider;
import org.cocktail.corossol.client.zutil.wo.table.ZEOTable;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class IntegrationNibCtrl extends NibCtrl  implements SwingFinderInterface {

	private SwingFinderExercice  monSwingFinderExercice =null;
	private SwingFinderPlanComptable monSwingFinderPlanComptable = null;
	private SwingFinderOrgan monSwingFinderOrgan = null;
	
	private SortieComptableNibCtrl sortieComptableNibCtrl=null;
	
	private EOExercice currentEOExercice = null;
	private EOOrgan currentEOOrgan = null;
	private EOPlanComptable currentEOPlanComptable = null;

	// le nib
	private IntegrationNib monIntegrationNib = null;

	public IntegrationNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setWithLogs(false);

		setMonSwingFinderExercice(new SwingFinderExercice(ctrl,this,alocation_x , alocation_y, 260,160,true));
		setMonSwingFinderPlanComptable( new SwingFinderPlanComptable(ctrl,this,alocation_x , alocation_y, 400,300,true));
		setMonSwingFinderOrgan(new SwingFinderOrgan(ctrl,this,alocation_x , alocation_y, 400,300,true,true));
		
		sortieComptableNibCtrl=new SortieComptableNibCtrl((ApplicationCorossol) ctrl, this);
	}

	public void setExercice(EOExercice exercice) {
		currentEOExercice = exercice;
		majOrigineFinancement();
	}

	public void creationFenetre(IntegrationNib leIntegrationNib, String title) {
		super.creationFenetre(leIntegrationNib, title);
		setMonIntegrationNib(leIntegrationNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void afficherFenetre() {
		super.afficherFenetre();
		setInventaireDG();
	}

	private void bindingAndCustomization() {
		try {
			// formateurs
			formaterUnTexfieldInteger(getMonIntegrationNib().jTextFieldDuree);
			formaterUnTexfieldNSTimestamp(getMonIntegrationNib().jTextFieldDateAcquisition, "dd/MM/yyyy");
			getMonIntegrationNib().jTextFieldDateAcquisition.addFocusListener(new DateFieldListener());
			getMonIntegrationNib().jTextFieldMontant.setText("0.00");
			getMonIntegrationNib().jTextFieldResiduel.setText("0.00");
			getMonIntegrationNib().jTextFieldMontant.setFormatterFactory(FormattedFieldProvider.getMontantFormatterFactory());
			getMonIntegrationNib().jTextFieldResiduel.setFormatterFactory(FormattedFieldProvider.getMontantFormatterFactory());

			// init des popups
			getMonIntegrationNib().jComboBoxCocktailAmort.removeAllItems();
			getMonIntegrationNib().jComboBoxCocktailAmort.addItem(EOCleInventaireComptable.LINEAIRE);
			getMonIntegrationNib().jComboBoxCocktailAmort.addItem(EOCleInventaireComptable.DEGRESSIF);
			getMonIntegrationNib().jComboBoxCocktailAmort.setSelectedItem(EOCleInventaireComptable.LINEAIRE);

			getMonIntegrationNib().initTableViewReprises();
			getMonIntegrationNib().getMaTableReprises().addListener(new ListenerTable());
			getMonIntegrationNib().getMaTableReprises().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			
			getMonIntegrationNib().initTableViewSorties();
			getMonIntegrationNib().getMaTableSorties().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			// le cablage des  objets
			getMonIntegrationNib().jButtonCocktailExercice.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionExercice(); } });
			getMonIntegrationNib().jButtonCocktailExercice.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.TABLEAU));

			getMonIntegrationNib().jButtonCocktailOrgan.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionOrgan(); } });
			getMonIntegrationNib().jButtonCocktailOrgan.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.TABLEAU));

			getMonIntegrationNib().jButtonCocktailImputation.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionImputation(); } });
			getMonIntegrationNib().jButtonCocktailImputation.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.TABLEAU));

			getMonIntegrationNib().jButtonCocktailFermer.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionFermer(); } });
			getMonIntegrationNib().jButtonCocktailFermer.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.FERMER));

			getMonIntegrationNib().jButtonCocktailAjouter.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionAjouter(); } });
			getMonIntegrationNib().jButtonCocktailAjouter.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.AJOUTER));

			getMonIntegrationNib().jButtonCocktailModifier.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionModifier(); } });
			getMonIntegrationNib().jButtonCocktailModifier.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.MODIFIER));

			getMonIntegrationNib().jButtonCocktailSupprimer.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionSupprimer(); } });
			
			getMonIntegrationNib().jButtonCocktailSupprimer.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.SUPPRIMER));

			getMonIntegrationNib().jButtonCocktailValider.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionValider(); } });
			getMonIntegrationNib().jButtonCocktailValider.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.VALIDER));

			getMonIntegrationNib().jButtonCocktailImprimer.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionImprimer(); } });
			getMonIntegrationNib().jButtonCocktailImprimer.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.IMPRIMER));

			getMonIntegrationNib().jButtonRechercher.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionFiltrer(); } });
			getMonIntegrationNib().jButtonReinitRecherche.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionReinitialiserFiltre(); } });
           
			getMonIntegrationNib().jTextFieldRecherche.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) { actionFiltrer(); } });
			
			getMonIntegrationNib().getBtAjouterSortie().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionAjouterSortie(); } });
			getMonIntegrationNib().getBtSupprimerSortie().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionSupprimerSortie(); } });

		
			setExercice(null);

		} catch (Throwable e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}

		getMonIntegrationNib().refreshInventaire();
		majInterface(false);
	}

	private class ListenerTable implements ZEOTable.ZEOTableListener {
		public void onDbClick() {
		}
		public void onSelectionChanged() {
			actionSelectionner();
		}
	}

	public void majOrigineFinancement() {
		getMonIntegrationNib().jComboBoxCocktailFinancement.removeAllItems();
		if (currentEOExercice==null)
			return;
		NSArray array=FinderComptable.findLesFinancements(app, currentEOExercice);
		for (int i=0;i<array.count();i++)
			getMonIntegrationNib().jComboBoxCocktailFinancement.addItem(array.objectAtIndex(i));
	}

	private void actionExercice() {
		getMonSwingFinderExercice().getMonSwingFinderEOExerciceNibCtrl().afficherFenetre();
	}
	private void actionOrgan() {
		getMonSwingFinderOrgan().getMonSwingFinderOrganTreeNibCtrl().afficherFenetre();
	} 
	private void actionImputation() {
		getMonSwingFinderPlanComptable().getMonSwingFinderEOPlanComptableNibCtrl().afficherFenetre();
	} 

	// action Fermer
	private void actionFermer() {
		app.retirerLesGlassPane();
		masquerFenetre();
	} 

	private void actionImprimer() {
		try {
			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(new Integer(getMonIntegrationNib().inventaireSelectionne().inventaireComptable().cleInventaireComptable().clicIdBis().intValue()),"CLIC_ID");
			parameters.takeValueForKey(app.getApplicationParametre("REP_BASE_JASPER_PATH"),"SUBREPORT_DIR");

			app.getToolsCocktailReports().imprimerReportParametres("fiche_inventaire.jasper", parameters, "fiche_inventaire"+UtilCtrl.lastModif());
		} catch (Exception ex) {
			System.out.println("actionImprimer OUPS"+ex);
		}
	}    
	
	private boolean checkDates() {
		// Date postérieure à la date du jour
		if (DateCtrl.isDateAfterNow(getMonIntegrationNib().jTextFieldDateAcquisition, 0))
			if (!EODialogs.runConfirmOperationDialog("Attention",
    				"La date d'acquisition n'est pas encore passee. Confirmez-vous ?",
    				"OUI", "NON"))
				return false;
		return true;
	}
	

	private void actionAjouter() {
		if (!getMonIntegrationNib().jButtonCocktailValider.isEnabled()) {
			currentEOPlanComptable = null;
			currentEOOrgan = null;
			setExercice(null);
			getMonIntegrationNib().jTextFieldDuree.setText("");
			getMonIntegrationNib().jTextFieldCocktailAmort.setText("");
			getMonIntegrationNib().jTextFieldBudgetaire.setText("");
			getMonIntegrationNib().jTextFieldExercice.setText("");
			getMonIntegrationNib().jTextFieldDateAcquisition.setText("");
			getMonIntegrationNib().jTextFieldNumero.setText("");
			getMonIntegrationNib().jComboBoxCocktailAmort.setSelectedItem(EOCleInventaireComptable.LINEAIRE);
			getMonIntegrationNib().jTextFieldMontant.setValue(new BigDecimal(0));
			getMonIntegrationNib().jTextFieldResiduel.setValue(new BigDecimal(0));
			getMonIntegrationNib().jTextAreaInformations.setText("");
			getMonIntegrationNib().jTextFieldFournisseur.setText("");
			getMonIntegrationNib().jTextAreaFactures.setText("");
			getMonIntegrationNib().jTextFieldDuree.setValue(new Integer(0));
			getMonIntegrationNib().jTextFieldImputation.setText("");

			majInterface(true);
		} else {
			majInterface(false);
			actionSelectionner();
		}
	}
	private void actionModifier() {
		if (currentEOOrgan == null || currentEOPlanComptable == null || currentEOExercice == null) {
			fenetreDeDialogueInformation("PROBLEME ! \n "+"Choisir un exercice, une imputation et un centre de responsabilite !");
			return;
		}

		System.out.println("EXERCICE : "+currentEOExercice.exeExercice());

		if (currentEOExercice.exeExercice().intValue()>=2007) {
			if (currentEOOrgan == null || currentEOOrgan.orgNiveau().intValue() != 3){
				fenetreDeDialogueInformation("PROBLEME ! \n "+"Il faut chosir un centre de responsabilite !");
				return;
			}
		}

		if (currentEOPlanComptable == null ||  getMonIntegrationNib().jTextFieldCocktailAmort.getText() == null){
			fenetreDeDialogueInformation("PROBLEME ! \n "+"Il faut chosir un compte et un compte d'amortissement !");
			return;
		}
		if (currentEOExercice == null){
			fenetreDeDialogueInformation("PROBLEME ! \n "+"Il faut chosir un exercice !");
			return;
		}
		if ( ((Number) getMonIntegrationNib().jTextFieldMontant.getValue()).intValue() <= 0) {
			fenetreDeDialogueInformation("PROBLEME ! \n "+"Le montant initial du bien ne peut pas etre inferieur ou egal a zero !");
			return;
		}
		
		if (!checkDates())
			return;

		try {
			ProcessInventaireNonBudgetaire monProcessInventaireNonBudgetaire = new ProcessInventaireNonBudgetaire();
			EOInventaireNonBudgetaire current = getMonIntegrationNib().inventaireSelectionne();

			BigDecimal montant=new BigDecimal(getMonIntegrationNib().jTextFieldMontant.getValue().toString());
			BigDecimal montantResiduel=new BigDecimal(getMonIntegrationNib().jTextFieldResiduel.getValue().toString());
			NSTimestamp date=DateCtrl.stringToDate(getMonIntegrationNib().jTextFieldDateAcquisition.getText(), "dd/MM/yyyy");

			monProcessInventaireNonBudgetaire.modifierUnInventaireNonBudgetaire(app.getAppEditingContext(), current,
					app.getCurrentUtilisateur(), currentEOPlanComptable,
					(EOOrigineFinancement)getMonIntegrationNib().jComboBoxCocktailFinancement.getSelectedItem(),
					currentEOOrgan, currentEOExercice,
					(String) getMonIntegrationNib().jComboBoxCocktailAmort.getSelectedItem(),
					(String) getMonIntegrationNib().jTextAreaInformations.getText(),//setInbNumeroSerie,
					montantResiduel,
					montant,
					(String) getMonIntegrationNib().jTextAreaInformations.getText(),
					(String) getMonIntegrationNib().jTextFieldFournisseur.getText(),
					(String) getMonIntegrationNib().jTextAreaFactures.getText(),
					new Integer(((Number) getMonIntegrationNib().jTextFieldDuree.getValue()).intValue()), date);

			ProcedureEnregistrerHb.enregistrer((ApplicationCorossol)app, current, app.getCurrentUtilisateur());

			if (app.getAppEditingContext().globalIDForObject(getMonIntegrationNib().inventaireSelectionne())!=null
					&& !app.getAppEditingContext().globalIDForObject(getMonIntegrationNib().inventaireSelectionne()).isTemporary()) {

				app.getAppEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(app.getAppEditingContext().globalIDForObject(current)));
				if (current.inventaireComptable()!=null)
					app.getAppEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(app.getAppEditingContext().globalIDForObject(current.inventaireComptable())));
			}
			app.getAppEditingContext().revert();

		} catch (Exception ex) {
			ex.printStackTrace();
			fenetreDeDialogueInformation("PROBLEMES ! \n"+ex.toString());
		}

		majInterface(false);
		actionSelectionner();
	}

	private void actionValider() {
		String messageProbleme ="";
		
		if (currentEOOrgan == null || currentEOPlanComptable == null || currentEOExercice == null) {
			fenetreDeDialogueInformation("PROBLEME ! \n "+"Choisir un exercice, une imputation et un centre de responsabilite !");
			return;
		}
		if (currentEOOrgan == null || currentEOOrgan.orgNiveau().intValue() != 3){
			fenetreDeDialogueInformation("PROBLEME ! \n "+"Il faut choisir un centre de responsabilite !");
			return;
		}
		if (currentEOPlanComptable == null ||  getMonIntegrationNib().jTextFieldCocktailAmort.getText() == null){
			fenetreDeDialogueInformation("PROBLEME ! \n "+"Il faut choisir un compte et un compte d'amortissement !");
			return;
		}
		
		if ( new BigDecimal(""+getMonIntegrationNib().jTextFieldMontant.getValue()).floatValue() <= 0.0) {
			fenetreDeDialogueInformation("PROBLEME ! \n "+"Le montant initial du bien ne peut pas etre inferieur ou egal a zero !");
			return;
		}
		if (!checkDates())
			return;
		try {
			ProcessInventaireNonBudgetaire monProcessInventaireNonBudgetaire = new ProcessInventaireNonBudgetaire();

			BigDecimal montant=new BigDecimal(""+getMonIntegrationNib().jTextFieldMontant.getValue());
			BigDecimal montantResiduel=new BigDecimal(""+getMonIntegrationNib().jTextFieldResiduel.getValue());
			NSTimestamp date=DateCtrl.stringToDate(getMonIntegrationNib().jTextFieldDateAcquisition.getText(), "dd/MM/yyyy");

			System.out.println("\n\n\n\n"+getMonIntegrationNib().jComboBoxCocktailFinancement.getSelectedItem());

			EOInventaireNonBudgetaire monInventaireNonBudgetaire = monProcessInventaireNonBudgetaire.ajouterUnInventaireNonBudgetaire(
					app.getAppEditingContext(), app.getCurrentUtilisateur(), currentEOPlanComptable,
					(EOOrigineFinancement) getMonIntegrationNib().jComboBoxCocktailFinancement.getSelectedItem(),
					currentEOOrgan, currentEOExercice, (String) getMonIntegrationNib().jComboBoxCocktailAmort.getSelectedItem(),
					(String) getMonIntegrationNib().jTextAreaInformations.getText(),
					montantResiduel,
					montant,
					(String) getMonIntegrationNib().jTextAreaInformations.getText(),
					(String) getMonIntegrationNib().jTextFieldFournisseur.getText(),
					(String) getMonIntegrationNib().jTextAreaFactures.getText(),
					(Integer) getMonIntegrationNib().jTextFieldDuree.getValue(), date);

			ProcedureEnregistrerHb.enregistrer((ApplicationCorossol)app, monInventaireNonBudgetaire, app.getCurrentUtilisateur());
			app.getAppEditingContext().saveChanges();
			
		} catch (Exception ex) {
			ex.printStackTrace();
			app.getAppEditingContext().revert();
			app.getAppEditingContext().saveChanges();
			fenetreDeDialogueInformation("PROBLEME ! \n"+ex.toString());	
		}
		
		// On sort du mode Ajouter
		setInventaireDG();
		majInterface(false);

		getMonIntegrationNib().refreshInventaire();
		actionSelectionner();
	}

	private void actionSupprimer() {
		if (EODialogs.runConfirmOperationDialog("Attention","Confirmez-vous la suppression ?","OUI", "NON"))
		try{
			ProcedureSupprimerHb.enregistrer((ApplicationCorossol)app, getMonIntegrationNib().inventaireSelectionne(), app.getCurrentUtilisateur());
			setInventaireDG();
			majInterface(false);
		} catch (Exception ex) {
			ex.printStackTrace();
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME ! \n"+ex.toString());
		}
		actionSelectionner();
	}

	public void actionSelectionner() {
		getMonIntegrationNib().jTextFieldDuree.setText("");
		getMonIntegrationNib().jTextFieldCocktailAmort.setText("");
		getMonIntegrationNib().jTextFieldBudgetaire.setText("");
		getMonIntegrationNib().jTextFieldExercice.setText("");

		getMonIntegrationNib().jTextFieldNumero.setText("");
		getMonIntegrationNib().jComboBoxCocktailAmort.setSelectedItem(EOCleInventaireComptable.LINEAIRE);
		getMonIntegrationNib().jTextFieldMontant.setValue(new BigDecimal(0));
		getMonIntegrationNib().jTextFieldResiduel.setValue(new BigDecimal(0));
		getMonIntegrationNib().jTextAreaInformations.setText("");
		getMonIntegrationNib().jTextFieldFournisseur.setText("");
		getMonIntegrationNib().jTextAreaFactures.setText("");
		getMonIntegrationNib().jTextFieldDuree.setValue(new Integer(0));
		getMonIntegrationNib().jTextFieldImputation.setText("");
		getMonIntegrationNib().jTextFieldDateAcquisition.setText("");

		getMonIntegrationNib().setSorties(new NSArray());

		if (getMonIntegrationNib().inventaireSelectionne()!=null) {
			EOInventaireNonBudgetaire current = getMonIntegrationNib().inventaireSelectionne();
			currentEOPlanComptable = current.planComptable();
			currentEOOrgan =  current.organ();
			setExercice(current.exercice());

			getMonIntegrationNib().jTextFieldDuree.setValue(new Integer(current.inbDuree().intValue()));
			getMonIntegrationNib().jTextFieldImputation.setText((currentEOPlanComptable.pcoNum()));
			EOPlanComptableAmo pcoa=FinderComptable.findLeCompteAmortissement(app, currentEOPlanComptable);

			if (pcoa!=null)
				getMonIntegrationNib().jTextFieldCocktailAmort.setText(pcoa.pcoaNum());

			if (current.inventaireComptable()!=null)
				getMonIntegrationNib().jComboBoxCocktailFinancement.setSelectedItem(current.inventaireComptable().origineFinancement());

			if(current.inbTypeAmort()!=null)
				if (current.inbTypeAmort().equals(EOCleInventaireComptable.LINEAIRE))
					getMonIntegrationNib().jComboBoxCocktailAmort.setSelectedItem(EOCleInventaireComptable.LINEAIRE);
				else
					getMonIntegrationNib().jComboBoxCocktailAmort.setSelectedItem(EOCleInventaireComptable.DEGRESSIF);

			getMonIntegrationNib().jTextFieldBudgetaire.setText((currentEOOrgan.orgUb())+"/"+(currentEOOrgan.orgCr()));
			getMonIntegrationNib().jTextFieldExercice.setText((currentEOExercice.exeExercice()).toString());
			getMonIntegrationNib().jTextFieldMontant.setValue(current.inbMontant());
			getMonIntegrationNib().jTextFieldResiduel.setValue(current.inbMontantResiduel());

			if (current.inventaireComptable() != null)
				getMonIntegrationNib().jTextFieldNumero.setText(current.inventaireComptable().cleInventaireComptable().clicNumComplet());

			getMonIntegrationNib().jTextAreaInformations.setText(current.inbInformations());
			getMonIntegrationNib().jTextFieldFournisseur.setText(current.inbFournisseur());
			getMonIntegrationNib().jTextAreaFactures.setText(current.inbFacture());

			if (current.inbDateAcquisition()!=null)
				getMonIntegrationNib().jTextFieldDateAcquisition.setText(DateCtrl.dateToString(current.inbDateAcquisition(), "dd/MM/yyyy"));
		
			if (current.inventaireComptable()!=null)
				getMonIntegrationNib().setSorties(current.inventaireComptable().inventaireComptableSorties());
		}
	}
	public void actionAjouterSortie() {
		EOInventaireNonBudgetaire inventaire=getMonIntegrationNib().inventaireSelectionne();
		if (inventaire==null || inventaire.inventaireComptable()==null)
			return;
		sortieComptableNibCtrl.setCurrentInventaireComptable(inventaire.inventaireComptable(), null);
		sortieComptableNibCtrl.afficherFenetre();
	}
	
	public void actionSupprimerSortie() {
		EOInventaireNonBudgetaire inventaire=getMonIntegrationNib().inventaireSelectionne();
		if (inventaire==null || inventaire.inventaireComptable()==null)
			return;
		if (getMonIntegrationNib().inventaireSortieSelectionne()==null)
			return;
		
		sortieComptableNibCtrl.setCurrentInventaireComptable(inventaire.inventaireComptable(), getMonIntegrationNib().inventaireSortieSelectionne());
		sortieComptableNibCtrl.supprimer();
	}

	public void actionFiltrer() {
		NSMutableArray qualifier=new NSMutableArray();

		String texte=getMonIntegrationNib().jTextFieldRecherche.getText();
		if (!texte.equals("")) {
			qualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireNonBudgetaire.EXERCICE_KEY+"."+
					EOExercice.EXE_EXERCICE_KEY+"=%@",  new NSArray(texte)));
			qualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireNonBudgetaire.PLAN_COMPTABLE_KEY+"."+
					EOPlanComptable.PCO_NUM_KEY+" caseInsensitiveLike %@",  new NSArray("*"+texte+"*")));
			qualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireNonBudgetaire.INVENTAIRE_COMPTABLE_KEY+"."+
					EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY+"."+EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY+
					" caseInsensitiveLike %@",  new NSArray("*"+texte+"*")));

			qualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireNonBudgetaire.INB_FOURNISSEUR_KEY+
					" caseInsensitiveLike %@",  new NSArray("*"+texte+"*")));
			qualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireNonBudgetaire.INB_FACTURE_KEY+
					" caseInsensitiveLike %@",  new NSArray("*"+texte+"*")));
			qualifier.addObject(EOQualifier.qualifierWithQualifierFormat(EOInventaireNonBudgetaire.INB_INFORMATIONS_KEY+
					" caseInsensitiveLike %@",  new NSArray("*"+texte+"*")));
		}

		if (qualifier.count()==0) {
			actionReinitialiserFiltre();
			return;
		}

		getMonIntegrationNib().setQualifier(new EOOrQualifier(qualifier));
	}

	public void actionReinitialiserFiltre() {
		getMonIntegrationNib().jTextFieldRecherche.setText("");
		getMonIntegrationNib().setQualifier(null);	
	}

	private void majInterface(boolean modeAjouter) {
		if (modeAjouter){
			getMonIntegrationNib().jButtonCocktailAjouter.setText("Annuler");
			getMonIntegrationNib().jButtonCocktailAjouter.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.MODIFIER));
			getMonIntegrationNib().getMaTableReprises().setEnabled(false);
		} else {
			getMonIntegrationNib().jButtonCocktailAjouter.setText("Ajouter");
			getMonIntegrationNib().jButtonCocktailAjouter.setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.AJOUTER));
			getMonIntegrationNib().getMaTableReprises().setEnabled(true);
		}

		getMonIntegrationNib().jButtonCocktailOrgan.setEnabled(modeAjouter);
		getMonIntegrationNib().jButtonCocktailExercice.setEnabled(modeAjouter);
		getMonIntegrationNib().jButtonCocktailFermer.setEnabled(!modeAjouter);
		getMonIntegrationNib().jButtonCocktailModifier.setEnabled(!modeAjouter);
		getMonIntegrationNib().jButtonCocktailSupprimer.setEnabled(!modeAjouter);
		getMonIntegrationNib().jButtonCocktailValider.setEnabled(modeAjouter);
	}

	public void changementExercice() {

	}

	private void setInventaireDG() {
		getMonIntegrationNib().setInventaire(new NSArray());
		getMonIntegrationNib().setInventaire(FinderInventaireNonBudgetaire.findLesInventaireNonBudgetaire(app, app.getCurrentExercice()));
	}

	private IntegrationNib getMonIntegrationNib() {
		return monIntegrationNib;
	}

	private void setMonIntegrationNib(IntegrationNib monIntegrationNib) {
		this.monIntegrationNib = monIntegrationNib;
	}

	private SwingFinderExercice getMonSwingFinderExercice() {
		return monSwingFinderExercice;
	}

	private void setMonSwingFinderExercice(SwingFinderExercice monSwingFinderExercice) {
		this.monSwingFinderExercice = monSwingFinderExercice;
	}

	private SwingFinderOrgan getMonSwingFinderOrgan() {
		return monSwingFinderOrgan;
	}

	private void setMonSwingFinderOrgan(SwingFinderOrgan monSwingFinderOrgan) {
		this.monSwingFinderOrgan = monSwingFinderOrgan;
	}

	private SwingFinderPlanComptable getMonSwingFinderPlanComptable() {
		return monSwingFinderPlanComptable;
	}

	private void setMonSwingFinderPlanComptable(SwingFinderPlanComptable monSwingFinderPlanComptable) {
		this.monSwingFinderPlanComptable = monSwingFinderPlanComptable;
	}

	public void swingFinderAnnuler(Object sender) {
	}

	public void swingFinderTerminer(Object sender) {
		if(sender == monSwingFinderExercice){
			setExercice((EOExercice)monSwingFinderExercice.getResultat().objectAtIndex(0));
			getMonIntegrationNib().jTextFieldExercice.setText((currentEOExercice.exeExercice()).toString());
			majOrigineFinancement();
		}

		if(sender == monSwingFinderOrgan){
			currentEOOrgan= (EOOrgan) monSwingFinderOrgan.getResultat().objectAtIndex(0);
			getMonIntegrationNib().jTextFieldBudgetaire.setText((currentEOOrgan.orgUb())+"/"+(currentEOOrgan.orgCr()));
		}

		if(sender == monSwingFinderPlanComptable){
			currentEOPlanComptable = (EOPlanComptable) monSwingFinderPlanComptable.getResultat().objectAtIndex(0);
			getMonIntegrationNib().jTextFieldImputation.setText((currentEOPlanComptable.pcoNum()));
			getMonIntegrationNib().jTextFieldDuree.setValue(new Integer(FinderComptable.findLeCompteAmortissementDuree(app, currentEOPlanComptable).intValue()));
			getMonIntegrationNib().jTextFieldCocktailAmort.setText(FinderComptable.findLeCompteAmortissement(app, currentEOPlanComptable).pcoaNum());
		}
	}
}
