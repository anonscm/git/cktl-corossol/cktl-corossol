package org.cocktail.corossol.client.nibctrl;


import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOUtilisateurOrgan;
import org.cocktail.application.client.swingfinder.SwingFinderInterface;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.corossol.client.eof.metier.EOBordereau;
import org.cocktail.corossol.client.eof.metier.EOVTitreRecette;
import org.cocktail.corossol.client.finder.FinderTitre;
import org.cocktail.corossol.client.nib.TitreNib;

import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class TitreNibCtrl extends NibCtrl implements SwingFinderInterface{

	// methodes
	//	private static final String METHODE_ACTION_VALIDER = "actionValider";
	//	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	//	private static final String METHODE_ACTION_RECHERCHER = "actionRechercherCle";

	private static final String TITRE = "Recherche des titres de recette";

	// le nib
	private TitreNib monTitreNib = null;

	private OriginesFinancementNibCtrl currentOriginesFinancementNibCtrl = null;
	private EditionsFinancementsNibCtrl currentEditionsFinancementsNibCtrl = null;
	
	private NibCtrl parentControleur = null;

	public TitreNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setMonTitreNib(new TitreNib());
//		monTitreNib.setPreferredSize(new java.awt.Dimension(ataille_x, ataille_y));
//		monTitreNib.setSize(ataille_x, ataille_y);

		setWithLogs(false);
	}

	public void creationFenetre(NibCtrl parentControleur) {
		super.creationFenetre(monTitreNib, TITRE);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
		this.parentControleur =  parentControleur;
	}

	public void afficherFenetre(OriginesFinancementNibCtrl tmpCurrentOriginesFinancementNibCtrl) {
		currentOriginesFinancementNibCtrl=tmpCurrentOriginesFinancementNibCtrl;
		afficherFenetre();
	}
	
	public void afficherFenetre(EditionsFinancementsNibCtrl tmpCurrentEditionsFinancementsNibCtrl) {
		currentEditionsFinancementsNibCtrl=tmpCurrentEditionsFinancementsNibCtrl;
		afficherFenetre();
	}

	public void afficherFenetre(){
		super.afficherFenetre();
//		try {
//			app.activerLesGlassPane(this.currentNib);
//		} catch (Throwable e) {
//			e.printStackTrace();
//		}
	}

	private void bindingAndCustomization() {
		try {
			// le cablage des  objets
			getMonTitreNib().getJButtonCocktailValider().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionValider();
				} });
			getMonTitreNib().getJButtonCocktailAnnuler().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionAnnuler();
				} });
			
			// Action RECHERCHER
			java.awt.event.ActionListener actionListenerRechercher = new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionRechercher();
				}
			};
			getMonTitreNib().getJButtonCocktailRechercher().addActionListener(actionListenerRechercher);
			getMonTitreNib().getjTextFieldExercice().addActionListener(actionListenerRechercher);
			getMonTitreNib().getjTextFieldUb().addActionListener(actionListenerRechercher);
			getMonTitreNib().getjTextFieldTitNumero().addActionListener(actionListenerRechercher);
			getMonTitreNib().getjTextFieldBorNumero().addActionListener(actionListenerRechercher);
			getMonTitreNib().getjTextFieldTitLibelle().addActionListener(actionListenerRechercher);
			getMonTitreNib().getjTextFieldFournisseur().addActionListener(actionListenerRechercher);
			getMonTitreNib().getjTextFieldHT().addActionListener(actionListenerRechercher);
			getMonTitreNib().getjTextFieldTTC().addActionListener(actionListenerRechercher);
			
			// les images des objets
			getMonTitreNib().getJButtonCocktailValider().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.VALIDER));
			getMonTitreNib().getJButtonCocktailAnnuler().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.FERMER));
			getMonTitreNib().getJButtonCocktailRechercher().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.RECHERCHER));

			//sera modal lors d une transaction 
			//app.addLesPanelsModal(getMonTitreNib());
		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}
	}

	// methodes cablage
	public void actionAnnuler() {
		//	trace(METHODE_ACTION_FERMER);
		masquerFenetre();
		getParentControleur().assistantsAnnuler(this);
	}

	public void actionValider() {
		if (currentOriginesFinancementNibCtrl == null && currentEditionsFinancementsNibCtrl == null)
			return;
		if (currentOriginesFinancementNibCtrl != null)
			currentOriginesFinancementNibCtrl.setTitre(getMonTitreNib().titreSelectionne());

		if (currentEditionsFinancementsNibCtrl != null)
			currentEditionsFinancementsNibCtrl.setCurrentTitre(getMonTitreNib().titreSelectionne());

		app.removeFromLesPanelsModal(getMonTitreNib());
		masquerFenetre();
		getParentControleur().swingFinderTerminer(this);
	}

	public void viderTitres() {
		getMonTitreNib().setVTitresRecette(new NSArray());
	}
	
	public void actionRechercher() {
		NSArray array = FinderTitre.find(app, getRechercheBindings());
		getMonTitreNib().setVTitresRecette(array);
	}

	/**
	 * @return
	 */
	private NSMutableDictionary getRechercheBindings() {
		NSMutableDictionary bindings = new NSMutableDictionary();

		if (app != null && app.getCurrentUtilisateur() != null) {
			updateBindings(bindings, app.getCurrentUtilisateur().utlOrdre().toString(), EOUtilisateurOrgan.UTL_ORDRE_KEY);
		}

		updateBindings(bindings, getFiltrePcoNum(), EOVTitreRecette.PCO_NUM_KEY);
		updateBindings(bindings, getMonTitreNib().getjTextFieldExercice().getText(), EOVTitreRecette.EXE_ORDRE_KEY);
		updateBindings(bindings, getMonTitreNib().getjTextFieldUb().getText(), EOVTitreRecette.GES_CODE_KEY);
		updateBindings(bindings, getMonTitreNib().getjTextFieldTitNumero().getText(), EOVTitreRecette.TIT_NUMERO_KEY);
		updateBindings(bindings, getMonTitreNib().getjTextFieldBorNumero().getText(), EOBordereau.BOR_NUM_KEY);
		updateBindings(bindings, getMonTitreNib().getjTextFieldTitLibelle().getText(), EOVTitreRecette.TIT_LIBELLE_KEY);
		updateBindings(bindings, getMonTitreNib().getjTextFieldFournisseur().getText(), "fournisseur");
		updateBindings(bindings, getMonTitreNib().getjTextFieldHT().getText(), EOVTitreRecette.TIT_HT_KEY);
		updateBindings(bindings, getMonTitreNib().getjTextFieldTTC().getText(), EOVTitreRecette.TIT_TTC_KEY);
		return bindings;
	}

	/**
	 * @return
	 */
	private String getFiltrePcoNum() {
		String filtrePcoNum;
		if (currentOriginesFinancementNibCtrl != null && currentOriginesFinancementNibCtrl.origineFinancement() != null
				&& currentOriginesFinancementNibCtrl.origineFinancement().pcoNum() != null) {
			filtrePcoNum = currentOriginesFinancementNibCtrl.origineFinancement().pcoNum();
		} else {
			// Titre de recette de classe 1
			filtrePcoNum = "1";
		}
		return filtrePcoNum;
	}

	/**
	 * @param bindings
	 * @param value
	 * @param key
	 */
	private void updateBindings(NSMutableDictionary bindings, String value, String key) {
		if (value != null && !value.equals("")) {
			bindings.setObjectForKey(value, key);
		}
	}

	//	accesseurs
	public TitreNib getMonTitreNib() {
		return monTitreNib;
	}

	public void setMonTitreNib(TitreNib monTitreNib) {
		this.monTitreNib = monTitreNib;
	}

	private NibCtrl getParentControleur() {
		return parentControleur;
	}

	private void setParentControleur(NibCtrl parentControleur) {
		this.parentControleur = parentControleur;
	}

	//	listener inspecteur
	public  void swingFinderAnnuler(Object obj) {
	}

	public  void swingFinderTerminer(Object obj) {
	}
}
