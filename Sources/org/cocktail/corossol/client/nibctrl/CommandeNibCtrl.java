/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)


package org.cocktail.corossol.client.nibctrl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.finder.FinderCommande;
import org.cocktail.corossol.client.nib.CommandeNib;

import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class CommandeNibCtrl extends NibCtrl {
	// le nib
	private CommandeNib mesCommandeNib =null;

	public CommandeNibCtrl(ApplicationCocktail arg0, int arg1, int arg2, int arg3, int arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
		
	}

	public void creationFenetre(CommandeNib commandeNib,String title) {
		super.creationFenetre( commandeNib,title);
		setMesCommandeNib(commandeNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void afficherFenetre(){
		super.afficherFenetre();
		getMesCommandeNib().getJFormattedTextFieldBorneInf().selectAll();
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMesCommandeNib());
		((ApplicationCorossol) app).activerLesGlassPane();
		refreshCommandes();
	}

	private void bindingAndCustomization() {
		try {
			getMesCommandeNib().initTableView();
			getMesCommandeNib().getMaTable().addMouseListener(new MouseAdapter() {
		        public void mouseClicked(MouseEvent e) {if (e.getClickCount() == 2) actionSelectionner();} });

			// default
			getMesCommandeNib().getJCheckBoxClasse().setSelected(true);

			//formateurs
			formaterUnTexfieldInteger(getMesCommandeNib().getJFormattedTextFieldBorneSup());
			formaterUnTexfieldInteger(getMesCommandeNib().getJFormattedTextFieldBorneInf());

			// le cablage des objets
			getMesCommandeNib().getJButtonCocktailAnnuler().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionAnnuler(); } });
			getMesCommandeNib().getJButtonCocktailSelectionner().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionSelectionner(); } });
			getMesCommandeNib().getJButtonCocktailRecherche().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionRechercher(); } });

			getMesCommandeNib().getJFormattedTextFieldBorneInf().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) { refreshCommandes(); } });
			getMesCommandeNib().getJFormattedTextFieldBorneSup().addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) { refreshCommandes(); } });

			// les images des objets
			getMesCommandeNib().getJButtonCocktailSelectionner().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.VALIDER));
			getMesCommandeNib().getJButtonCocktailAnnuler().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.REFUSER));
			getMesCommandeNib().getJButtonCocktailRecherche().setIcon((ImageIcon)new EOClientResourceBundle().getObject("linkto_help"));

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMesCommandeNib());
		}
		catch(Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
		}
	}

	// les actions
	public void actionAnnuler(){
		((ApplicationCorossol) app).retirerLesGlassPane();
		app.addLesPanelsModal(getMesCommandeNib());
		masquerFenetre();
	}

	public void actionSelectionner(){
		try {
			((ApplicationCorossol) app).retirerLesGlassPane();
			app.addLesPanelsModal(getMesCommandeNib());

			((ApplicationCorossol)app).setCurrentCommande(getMesCommandeNib().commandeSelectionnee());
			((ApplicationCorossol)app).getObserveurCommandeSelection().sendMessageToObserver(this,"changementCommande");

			masquerFenetre();
		}
		catch(Throwable e) {
			e.printStackTrace();
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
		}
	}

	public void actionRechercher(){
		refreshCommandes();
	}

	private void refreshCommandes(){	
		try {
			getMesCommandeNib().setCommandes(FinderCommande.findLesCommandes(app, 
					(Number)getMesCommandeNib().getJFormattedTextFieldBorneInf().getValue(),
					(Number)getMesCommandeNib().getJFormattedTextFieldBorneSup().getValue(), 
					getMesCommandeNib().getJCheckBoxClasse().isSelected()));
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
	}

	// accesseur
	private CommandeNib getMesCommandeNib() {
		return mesCommandeNib;
	}

	private void setMesCommandeNib(CommandeNib mesCommandeNib) {
		this.mesCommandeNib = mesCommandeNib;
	}
}
