/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.corossol.client.nibctrl;


import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOIndividuUlr;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.ToolsSwing;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOMagasin;
import org.cocktail.corossol.client.eof.metier.EOVCommande;
import org.cocktail.corossol.client.eof.process.ProcessInventaire;
import org.cocktail.corossol.client.eof.process.ProcessLivraison;
import org.cocktail.corossol.client.finder.FinderLivraisons;
import org.cocktail.corossol.client.nib.LivraisonDetailInspecteurNib;
import org.cocktail.corossol.client.nib.LivraisonInspecteurNib;
import org.cocktail.corossol.client.nib.LivraisonNib;
import org.cocktail.corossol.client.nibctrl.interfaces.InterfaceInspecteur;

import com.webobjects.foundation.NSMutableArray;

public class LivraisonNibCtrl extends NibCtrl implements InterfaceInspecteur {
	//	titres
	static final String FENETRE_DE_CREATION_LIVRAISON = "Fenetre de gestion d'une livraison ...";
	static final String FENETRE_DE_CREATION_LIVRAISON_DETAIL = "Fenetre de gestion des biens ...";

	//methodes 
	private static final String METHODE_AFFICHER_FENETRE_CREATION = "afficherFenetreCreation";
	private static final String METHODE_AFFICHER_FENETRE_MODIFICATION = "afficherFenetreModification";
	private static final String METHODE_AFFICHER_FENETRE_SUPPRESSION = "afficherFenetreSuppression";

	private static final String METHODE_AFFICHER_FENETRE_CREATION_DETAIL = "afficherFenetreCreationDetail";
	private static final String METHODE_AFFICHER_FENETRE_MODIFICATION_DETAIL = "afficherFenetreModificationDetail";
	private static final String METHODE_AFFICHER_SUPPRIMER_LIVRAISON_DETAIL = "afficherFenetreSuppressionDetail";


	private static final String METHODE_ACTION_FERMER = "actionFermer";
	private static final String METHODE_ACTION_AJOUTER_BIENS = "actionAjouterBiens";
	private static final String METHODE_ACTION_RETIRER_BIENS = "actionRetirerBiens";


	private static final String METHODE_CHANGEMENT_SELECTION_LIVRAISON = "changementSelectionLivraison";
	private static final String METHODE_CHANGEMENT_SELECTION_LIVRAISON_DETAIL = "changementSelectionLivraisonDetail";

	private static final boolean AVEC_LOGS = true;

	// les tables view
	private JTableViewCocktail mesLivraisonsTBV=null;
	private JTableViewCocktail mesDetailsLivraisonTBV=null;
	private JTableViewCocktail mesBiensTBV=null;

	// les displaygroup
	private NSMutableArrayDisplayGroup mesLivraisonsDG = new NSMutableArrayDisplayGroup();
	private NSMutableArrayDisplayGroup mesDetailsLivraisonDG = new NSMutableArrayDisplayGroup();
	private NSMutableArrayDisplayGroup mesBiensDG = new NSMutableArrayDisplayGroup();

	//	le nib
	private LivraisonNib monLivraisonNib =null;

	//	autres nib 
	private LivraisonInspecteurNib monLivraisonInspecteurNib = null;
	private LivraisonInspecteurNibCtrl monLivraisonInspecteurNibCtrl = null;
	private LivraisonDetailInspecteurNib monLivraisonDetailInspecteurNib = null;
	private LivraisonDetailInspecteurNibCtrl monLivraisonDetailInspecteurNibCtrl = null;

	//	Process 
	private ProcessLivraison monProcessLivraison =null;

	public LivraisonNibCtrl(ApplicationCocktail arg0, int arg1, int arg2,
			int arg3, int arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
		

		// creation du nib l'inspecteur livraison
		setMonLivraisonInspecteurNibCtrl(new LivraisonInspecteurNibCtrl(arg0, arg1 , arg2, arg3 , arg4));
		setMonLivraisonInspecteurNib( new LivraisonInspecteurNib());
		getMonLivraisonInspecteurNibCtrl().creationFenetre(getMonLivraisonInspecteurNib(),ToolsSwing.formaterStringU(FENETRE_DE_CREATION_LIVRAISON));
		getMonLivraisonInspecteurNib().setPreferredSize(new java.awt.Dimension(430, 350));
		getMonLivraisonInspecteurNib().setSize(430, 350);

		// creation du nib l'inspecteur livraisonDetail
		setMonLivraisonDetailInspecteurNibCtrl(new LivraisonDetailInspecteurNibCtrl(arg0, arg1 , arg2, arg3 , arg4));
		setMonLivraisonDetailInspecteurNib( new LivraisonDetailInspecteurNib());
		getMonLivraisonDetailInspecteurNibCtrl().creationFenetre(getMonLivraisonDetailInspecteurNib(),ToolsSwing.formaterStringU(FENETRE_DE_CREATION_LIVRAISON_DETAIL));
		getMonLivraisonDetailInspecteurNib().setPreferredSize(new java.awt.Dimension(500, 500));
		getMonLivraisonDetailInspecteurNib().setSize(500,500);

		// inspecteurs 
		monLivraisonInspecteurNibCtrl.setParentCtrl(this);
		monLivraisonDetailInspecteurNibCtrl.setParentCtrl(this);

		//process
		setMonProcessLivraison (new ProcessLivraison());
	}

	public void creationFenetre(LivraisonNib livraisonNib,String title)
	{
		super.creationFenetre( livraisonNib,title);
		setMonLivraisonNib(livraisonNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void afficherFenetre(){
		super.afficherFenetre();

		if (((ApplicationCorossol)app).getCurrentCommande() == null){
			masquerFenetre();
			fenetreDeDialogueInformation("Impossible \n Vous devez choisir une commande !");
		}
	}

	private void bindingAndCustomization() {
		try {
			// creation de la table view de livraison
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData( "Commande",100, JLabel.CENTER ,EOLivraison.COMMANDE_KEY+"."+EOVCommande.COMM_NUMERO_KEY,"String"));
			lesColumns.addObject(new ColumnData( "Exercice",200, JLabel.CENTER ,EOLivraison.EXERCICE_KEY+"."+EOExercice.EXE_EXERCICE_KEY,"String"));
			lesColumns.addObject(new ColumnData( "Magasin",200, JLabel.CENTER ,EOLivraison.MAGASIN_KEY+"."+EOMagasin.MAG_LIBELLE_KEY,"String"));
			lesColumns.addObject(new ColumnData( "Createur",200, JLabel.CENTER ,EOLivraison.UTILISATEUR_KEY+"."+EOUtilisateur.INDIVIDU_KEY+"."+EOIndividuUlr.NOM_USUEL_KEY,"String"));
			lesColumns.addObject(new ColumnData( "Date Liv.",200, JLabel.CENTER ,EOLivraison.LIV_DATE_KEY,"String"));
			lesColumns.addObject(new ColumnData( "Numero Liv.",200, JLabel.CENTER ,EOLivraison.LIV_NUMERO_KEY,"String"));
			lesColumns.addObject(new ColumnData( "Reception ",200, JLabel.CENTER ,EOLivraison.LIV_RECEPTION_KEY,"String"));

			setLivraisonsDG();
			setMesLivraisonsTBV(new JTableViewCocktail(lesColumns,getMesLivraisonsDG(),new Dimension(100,100),JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonLivraisonNib().getMesLivraisonsTBV().initTableViewCocktail(getMesLivraisonsTBV());

			// creation de la table view de detail livraison
			NSMutableArray lesColumnsDetail = new NSMutableArray();
			lesColumnsDetail.addObject(new ColumnData( "Libelle",200, JLabel.CENTER ,EOLivraisonDetail.LID_LIBELLE_KEY,"String"));
			lesColumnsDetail.addObject(new ColumnData( "Montant",100, JLabel.CENTER ,EOLivraisonDetail.LID_MONTANT_KEY,"String"));
			lesColumnsDetail.addObject(new ColumnData( "Qte",50, JLabel.CENTER ,EOLivraisonDetail.LID_QUANTITE_KEY,"String"));
			lesColumnsDetail.addObject(new ColumnData( "Credit",50, JLabel.CENTER ,EOLivraisonDetail.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_CODE_KEY,"String"));
			lesColumnsDetail.addObject(new ColumnData( "Imputation",50, JLabel.CENTER ,EOLivraisonDetail.PLAN_COMPTABLE_KEY+"."+EOPlanComptable.PCO_NUM_KEY,"String"));
			lesColumnsDetail.addObject(new ColumnData( "Ub",50, JLabel.CENTER ,EOLivraisonDetail.ORGAN_KEY+"."+EOOrgan.ORG_UB_KEY,"String"));
			lesColumnsDetail.addObject(new ColumnData( "Cr",80, JLabel.CENTER ,EOLivraisonDetail.ORGAN_KEY+"."+EOOrgan.ORG_CR_KEY,"String"));
			lesColumnsDetail.addObject(new ColumnData( "Sous-Cr",100, JLabel.CENTER ,EOLivraisonDetail.ORGAN_KEY+"."+EOOrgan.ORG_SOUSCR_KEY,"String"));

			setLivraisonsDetailDG();
			setMesDetailsLivraisonTBV(new JTableViewCocktail(lesColumnsDetail,getMesDetailsLivraisonDG(),new Dimension(100,100),JTable.AUTO_RESIZE_OFF));
			getMonLivraisonNib().getMesDetailsLivraisonTBV().initTableViewCocktail(getMesDetailsLivraisonTBV());

			// creation de la table view de detail livraison
			NSMutableArray lesColumnsBiens = new NSMutableArray();
			lesColumnsBiens.addObject(new ColumnData( "Libelle",100, JLabel.CENTER ,"livraisonDetail.lidLibelle","String"));

			setBiensDG();
			setMesBiensTBV(new JTableViewCocktail(lesColumnsBiens,getMesBiensDG(),new Dimension(100,100),JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonLivraisonNib().getMesBiensTBV().initTableViewCocktail(getMesBiensTBV());

			// les listeners
			getMesLivraisonsTBV().addDelegateSelectionListener(this,METHODE_CHANGEMENT_SELECTION_LIVRAISON);
			getMesLivraisonsTBV().addDelegateKeyListener(this,METHODE_CHANGEMENT_SELECTION_LIVRAISON);
			getMesDetailsLivraisonTBV().addDelegateSelectionListener(this,METHODE_CHANGEMENT_SELECTION_LIVRAISON_DETAIL);
			getMesDetailsLivraisonTBV().addDelegateKeyListener(this,METHODE_CHANGEMENT_SELECTION_LIVRAISON_DETAIL);

			// multi ou mono selection dans les tbv
			getMesLivraisonsTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			getMesBiensTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			getMesDetailsLivraisonTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);


			// le cablage des  objets
			getMonLivraisonNib().getJButtonCocktailFermer().addDelegateActionListener(this,METHODE_ACTION_FERMER);
			getMonLivraisonNib().getJButtonCocktailCreerLivraison().addDelegateActionListener(getMonLivraisonInspecteurNibCtrl(),METHODE_AFFICHER_FENETRE_CREATION);
			getMonLivraisonNib().getJButtonCocktailModifierLivraison().addDelegateActionListener(getMonLivraisonInspecteurNibCtrl(),METHODE_AFFICHER_FENETRE_MODIFICATION);
			getMonLivraisonNib().getJButtonCocktailAnnulerLivraison().addDelegateActionListener(getMonLivraisonInspecteurNibCtrl(),METHODE_AFFICHER_FENETRE_SUPPRESSION);

			getMonLivraisonNib().getJButtonCocktailAjouterDetailLivraison().addDelegateActionListener(getMonLivraisonDetailInspecteurNibCtrl(),METHODE_AFFICHER_FENETRE_CREATION_DETAIL);
			getMonLivraisonNib().getJButtonCocktailModifierArticle().addDelegateActionListener(getMonLivraisonDetailInspecteurNibCtrl(),METHODE_AFFICHER_FENETRE_MODIFICATION_DETAIL);
			getMonLivraisonNib().getJButtonCocktailSupprimerDetail().addDelegateActionListener(getMonLivraisonDetailInspecteurNibCtrl(),METHODE_AFFICHER_SUPPRIMER_LIVRAISON_DETAIL);

			getMonLivraisonNib().getJButtonCocktailAjouterBiens().addDelegateActionListener(this,METHODE_ACTION_AJOUTER_BIENS);
			getMonLivraisonNib().getJButtonCocktailRetirerBien().addDelegateActionListener(this,METHODE_ACTION_RETIRER_BIENS);

			// les images des objets
			getMonLivraisonNib().getJButtonCocktailAnnulerLivraison().setIcone(IconeCocktail.REFUSER);//"delete_obj");
			getMonLivraisonNib().getJButtonCocktailCreerLivraison().setIcone(IconeCocktail.AJOUTER);//"add_obj");
			getMonLivraisonNib().getJButtonCocktailModifierLivraison().setIcone(IconeCocktail.MODIFIER);//"refresh");

			getMonLivraisonNib().getJButtonCocktailSupprimerDetail().setIcone(IconeCocktail.SUPPRIMER);//"delete_obj");
			getMonLivraisonNib().getJButtonCocktailAjouterDetailLivraison().setIcone(IconeCocktail.AJOUTER);//"add_obj");
			getMonLivraisonNib().getJButtonCocktailModifierArticle().setIcone(IconeCocktail.MODIFIER);//"refresh");

			getMonLivraisonNib().getJButtonCocktailFermer().setIcone(IconeCocktail.FERMER);//"close_view");
			getMonLivraisonNib().getJButtonCocktailRetirerBien().setIcone(IconeCocktail.FLECHEGAUCHE);//"backward_nav");
			getMonLivraisonNib().getJButtonCocktailAjouterBiens().setIcone(IconeCocktail.FLECHEDROITE);//"forward_nav");

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol)app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");

			// obseration des changements de selection de commande :
			((ApplicationCorossol)app).getObserveurCommandeSelection().addObserverForMessage(this, "changementCommande");

			//sera modal lors d une transaction
			app.addLesPanelsModal(getMonLivraisonNib());

		}catch(Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
		}
	}

	// methodes cablage
	public void actionFermer(){
		trace(METHODE_ACTION_FERMER);
		masquerFenetre();	
	}

	public void actionAjouterBiens(){
		trace(METHODE_ACTION_AJOUTER_BIENS);
		EOLivraisonDetail tmpLivraisonDetail =(EOLivraisonDetail)getMesDetailsLivraisonTBV().getSelectedObjects().objectAtIndex(0);
		int qte = tmpLivraisonDetail.lidReste().intValue();
		ProcessInventaire monProcessInventaire = new ProcessInventaire(AVEC_LOGS);

		try {
			for (int i = 0; i < qte; i++)
				monProcessInventaire.ajouterUnInventaire(app.getAppEditingContext(), app.getCurrentUtilisateur(),
						(EOLivraisonDetail)getMesDetailsLivraisonTBV().getSelectedObjects().objectAtIndex(0),
						null, null, null, null, null, null, null,
						((EOLivraisonDetail)getMesDetailsLivraisonTBV().getSelectedObjects().objectAtIndex(0)).lidMontant(),
						null, null, null, null, null, null);
			app.getAppEditingContext().saveChanges();	
		} catch (Exception e) {
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
		setBiensDG();
		((ApplicationCorossol)app).getObserveurCommandeSelection().sendMessageToObserver(this,"changementCommandeDetail");
	}

	public void actionRetirerBiens(){
		trace(METHODE_ACTION_RETIRER_BIENS);
		ProcessInventaire monProcessInventaire = new ProcessInventaire(AVEC_LOGS);

		for (int i = 0; i < getMesBiensTBV().getSelectedObjects().count(); i++) {
			try {
				monProcessInventaire.supprimerUnInventaire(app.getAppEditingContext(), (EOInventaire) getMesBiensTBV().getSelectedObjects().objectAtIndex(i));
				app.getAppEditingContext().saveChanges();
			} catch (Exception e) {
				app.getAppEditingContext().revert();
				fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
			}
		}
		setBiensDG();
		((ApplicationCorossol)app).getObserveurCommandeSelection().sendMessageToObserver(this,"changementCommandeDetail");
	}

	// changements de selection
	public void changementSelectionLivraison(){
		setLivraisonsDetailDG();
		setBiensDG();
	}

	public void changementSelectionLivraisonDetail(){
		setBiensDG();
	}

	// methodes observeurs
	public void changementExercice(){
		setLivraisonsDG();
		setLivraisonsDetailDG();
		setBiensDG();
	}

	public void changementCommande(){
		setLivraisonsDG();
		setLivraisonsDetailDG();
		setBiensDG();
	}
	
	// datasource TBV
	private void setLivraisonsDG() {
		getMesLivraisonsDG().removeAllObjects();
		if (getMesLivraisonsTBV() != null)
			getMesLivraisonsTBV().refresh();	

		try {
			if (((ApplicationCorossol)app).getCurrentCommande() != null)
				getMesLivraisonsDG().addObjectsFromArray(FinderLivraisons.findLesLivraisons(app));
			if (getMesLivraisonsTBV() != null) {
				getMesLivraisonsTBV().refresh();
				
				if (getMesLivraisonsTBV().getData().count()>0)
					getMesLivraisonsTBV().getTable().setRowSelectionInterval(0,0);

				getMesLivraisonsTBV().repaint();
				getMesLivraisonsTBV().getTable().repaint();
			}
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
	}

	private void setLivraisonsDetailDG() {
		getMesDetailsLivraisonDG().removeAllObjects();
		if (getMesDetailsLivraisonTBV() != null)
			getMesDetailsLivraisonTBV().refresh();	

		if (getMesLivraisonsTBV().getSelectedObjects().count() == 1) {
			try {
				if (((ApplicationCorossol)app).getCurrentCommande() != null)
					getMesDetailsLivraisonDG().addObjectsFromArray(
							FinderLivraisons.findLesLivraisonDetails(app, (EOLivraison) getMesLivraisonsTBV().getSelectedObjects().objectAtIndex(0)));
				
				if (getMesDetailsLivraisonTBV() != null) {
					getMesDetailsLivraisonTBV().refresh();
					
					if (getMesDetailsLivraisonTBV().getData().count()>0)
						getMesDetailsLivraisonTBV().getTable().setRowSelectionInterval(0,0);

					getMesDetailsLivraisonTBV().repaint();
					getMesDetailsLivraisonTBV().getTable().repaint();
				}
			} catch (Exception e) {
				fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
			}
		}
	}

	private void setBiensDG() {
		getMesBiensDG().removeAllObjects();
		if (getMesBiensTBV() != null)
			getMesBiensTBV().refresh();	
		
		if (getMesDetailsLivraisonTBV().getSelectedObjects().count() == 1) {
			try {
				if (((ApplicationCorossol)app).getCurrentCommande() != null)
					getMesBiensDG().addObjectsFromArray(
							FinderLivraisons.findLesInventaires(app, (EOLivraisonDetail) getMesDetailsLivraisonTBV().getSelectedObjects().objectAtIndex(0)));	
				
				if (getMesBiensTBV() != null)
					  getMesBiensTBV().refresh();	
				
			} catch (Exception e) {
				fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
			}
		}
	}

	// accesseur
	private LivraisonNib getMonLivraisonNib() {
		return monLivraisonNib;
	}

	private void setMonLivraisonNib(LivraisonNib monLivraisonNib) {
		this.monLivraisonNib = monLivraisonNib;
	}

	private NSMutableArrayDisplayGroup getMesBiensDG() {
		return mesBiensDG;
	}

	private void setMesBiensDG(NSMutableArrayDisplayGroup mesBiensDG) {
		this.mesBiensDG = mesBiensDG;
	}

	private JTableViewCocktail getMesBiensTBV() {
		return mesBiensTBV;
	}

	private void setMesBiensTBV(JTableViewCocktail mesBiensTBV) {
		this.mesBiensTBV = mesBiensTBV;
	}

	private NSMutableArrayDisplayGroup getMesDetailsLivraisonDG() {
		return mesDetailsLivraisonDG;
	}

	private void setMesDetailsLivraisonDG(NSMutableArrayDisplayGroup mesDetailsLivraisonDG) {
		this.mesDetailsLivraisonDG = mesDetailsLivraisonDG;
	}

	JTableViewCocktail getMesDetailsLivraisonTBV() {
		return mesDetailsLivraisonTBV;
	}

	private void setMesDetailsLivraisonTBV(JTableViewCocktail mesDetailsLivraisonTBV) {
		this.mesDetailsLivraisonTBV = mesDetailsLivraisonTBV;
	}

	private NSMutableArrayDisplayGroup getMesLivraisonsDG() {
		return mesLivraisonsDG;
	}

	private void setMesLivraisonsDG(NSMutableArrayDisplayGroup mesLivraisonsDG) {
		this.mesLivraisonsDG = mesLivraisonsDG;
	}

	JTableViewCocktail getMesLivraisonsTBV() {
		return mesLivraisonsTBV;
	}

	private void setMesLivraisonsTBV(JTableViewCocktail mesLivraisonsTBV) {
		this.mesLivraisonsTBV = mesLivraisonsTBV;
	}

	private LivraisonInspecteurNib getMonLivraisonInspecteurNib() {
		return monLivraisonInspecteurNib;
	}

	private void setMonLivraisonInspecteurNib(LivraisonInspecteurNib monLivraisonInspecteurNib) {
		this.monLivraisonInspecteurNib = monLivraisonInspecteurNib;
	}

	private LivraisonInspecteurNibCtrl getMonLivraisonInspecteurNibCtrl() {
		return monLivraisonInspecteurNibCtrl;
	}

	private void setMonLivraisonInspecteurNibCtrl(LivraisonInspecteurNibCtrl monLivraisonInspecteurNibCtrl) {
		this.monLivraisonInspecteurNibCtrl = monLivraisonInspecteurNibCtrl;
	}

	// methodes d interfaces inspecteur
	public void inspecteurAnnuler(Object sender) {
		
	}

	public void inspecteurFermer(Object sender) {
		
		if (sender == getMonLivraisonInspecteurNibCtrl()) {
			((ApplicationCorossol)app).getObserveurCommandeSelection().sendMessageToObserver(this,"changementCommande");
			setLivraisonsDG();
			setBiensDG();
		}
		if (sender == getMonLivraisonDetailInspecteurNibCtrl()) {
			setLivraisonsDetailDG();
		    setBiensDG();
		}
	}

	private ProcessLivraison getMonProcessLivraison() {
		return monProcessLivraison;
	}

	private void setMonProcessLivraison(ProcessLivraison monProcessLivraison) {
		this.monProcessLivraison = monProcessLivraison;
	}

	private LivraisonDetailInspecteurNib getMonLivraisonDetailInspecteurNib() {
		return monLivraisonDetailInspecteurNib;
	}

	private void setMonLivraisonDetailInspecteurNib(LivraisonDetailInspecteurNib monLivraisonDetailInspecteurNib) {
		this.monLivraisonDetailInspecteurNib = monLivraisonDetailInspecteurNib;
	}

	private LivraisonDetailInspecteurNibCtrl getMonLivraisonDetailInspecteurNibCtrl() {
		return monLivraisonDetailInspecteurNibCtrl;
	}

	private void setMonLivraisonDetailInspecteurNibCtrl(LivraisonDetailInspecteurNibCtrl monLivraisonDetailInspecteurNibCtrl) {
		this.monLivraisonDetailInspecteurNibCtrl = monLivraisonDetailInspecteurNibCtrl;
	}
}
