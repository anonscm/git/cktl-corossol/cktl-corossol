package org.cocktail.corossol.client.nibctrl;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.nib.Cal;
import org.cocktail.corossol.client.nib.CalendarNib;

public class CalendarNibCtrl extends NibCtrl {

	// methodes
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_FERMER = "actionFermer";
	private static final String METHODE_ACTION_OK = "actionOk";

	// parent controleur
	private Cal monCal = null;

	// le nib
	private CalendarNib monCalendarNib = null;

	public CalendarNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		monCal = new Cal(); 
		setWithLogs(false);
	}

	public void creationFenetre(CalendarNib leCalendarNib, String title) {
		super.creationFenetre(leCalendarNib, title);
		setMonCalendarNib(leCalendarNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void afficherFenetre() {
		super.afficherFenetre();
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonCalendarNib());
		((ApplicationCorossol) app).activerLesGlassPane();
	}

	private void bindingAndCustomization() {
		try {
			getMonCalendarNib().getJPanelCal().add(monCal);

			// le cablage des  objets
			getMonCalendarNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonCalendarNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_OK);
			getMonCalendarNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_FERMER);

			// les images des objets
			getMonCalendarNib().getJButtonCocktailOk().setIcone(IconeCocktail.VALIDER);//"valider16");
			getMonCalendarNib().getJButtonCocktailAnnuler().setIcone(IconeCocktail.REFUSER);//"delete_obj");
			getMonCalendarNib().getJButtonCocktailFermer().setIcone(IconeCocktail.FERMER);//"close_view");
			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonCalendarNib());

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol) app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	// methodes cablage
	public void actionFermer() {
		masquerFenetre();
	}

	// methodes cablage
	public void actionAnnuler() {
		masquerFenetre();
	}

	// methodes cablage
	public void actionOk() {
		String dateString =	monCal.getActiveDate();
		masquerFenetre();
	}

	private CalendarNib getMonCalendarNib() {
		return monCalendarNib;
	}

	private void setMonCalendarNib(CalendarNib monCalendarNib) {
		this.monCalendarNib = monCalendarNib;
	}
}
