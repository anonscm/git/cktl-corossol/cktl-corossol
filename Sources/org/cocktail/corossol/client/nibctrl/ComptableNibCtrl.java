/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.corossol.client.nibctrl;

import java.awt.Dimension;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.ToolsSwing;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.UtilCtrl;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.process.ProcessCleInventaireComptable;
import org.cocktail.corossol.client.eof.process.ProcessInventaire;
import org.cocktail.corossol.client.eof.process.ProcessInventaireComptable;
import org.cocktail.corossol.client.finder.FinderComptable;
import org.cocktail.corossol.client.nib.ComptableInventaireInspecteurNib;
import org.cocktail.corossol.client.nib.ComptableNib;
import org.cocktail.corossol.client.nib.DepenseInspecteurNib;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ComptableNibCtrl extends NibCtrl {

	private static final String LA_SELECTION_N_EST_PAS_DE_MEME_IMPUTATION = "La selection n est pas de meme imputation";
	private static final String LA_SELECTION_N_EST_PAS_DE_MEME_EXERCICE = "La selection n est pas de meme exercice";
	private static final String LA_SELECTION_N_EST_PAS_DE_MEME_CR = "La selection n est pas de meme CR";
	// les tables view
	private JTableViewCocktail mesBiensAttenteTBV = null;
	private JTableViewCocktail mesNumeroTBV = null;
	private JTableViewCocktail mesNumeroDetailTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup mesBiensAttenteDG = new NSMutableArrayDisplayGroup();
	private NSMutableArrayDisplayGroup mesNumeroDG = new NSMutableArrayDisplayGroup();
	private NSMutableArrayDisplayGroup mesNumeroDetailDG = new NSMutableArrayDisplayGroup();

	// methodes
	private static final String METHODE_ACTION_FERMER = "actionFermer";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_AJOUTER = "actionAjouter";
	private static final String METHODE_ACTION_MODIFIER = "actionModifier";
	private static final String METHODE_ACTION_IMPRIMER = "actionImprimer";
	private static final String METHODE_ACTION_MODIFIER_DEPENSE = "actionModifierDepense";

	private static final String METHODE_CHANGEMENT_SELECTION_BIENS = "changementSelectionDetail";
	private static final String FENETRE_INSPECTEUR_INVENTAIRE = "Définition inventaire comptable";
	private static final String FENETRE_INSPECTEUR_DEPENSE = "Fenetre de gestion du lien inventaire - depense";

	// le nib
	private ComptableNib monComptableNib = null;

	// ComptableInventaireInspecteurNib
	ComptableInventaireInspecteurNib monComptableInventaireInspecteurNib = null;
	ComptableInventaireInspecteurNibCtrl monComptableInventaireInspecteurNibCtrl = null;

	DepenseInspecteurNib monDepenseInspecteurNib = null;
	DepenseInspecteurNibCtrl monDepenseInspecteurNibCtrl = null;

	ProcessCleInventaireComptable monProcessCleInventaireComptable = null;
	ProcessInventaireComptable monProcessInventaireComptable = null;
	ProcessInventaire monProcessInventaire = null;

	public ComptableNibCtrl(ApplicationCocktail arg0, int arg1, int arg2,
			int arg3, int arg4) {
		super(arg0, arg1, arg2, arg3, arg4);

		monProcessCleInventaireComptable = new ProcessCleInventaireComptable();
		monProcessInventaireComptable = new ProcessInventaireComptable();
		monProcessInventaire = new ProcessInventaire(true);

		// creation de l'assistant
		setMonComptableInventaireInspecteurNibCtrl(new ComptableInventaireInspecteurNibCtrl(
				arg0, arg1, arg2, 610, 620));
		setMonComptableInventaireInspecteurNib(new ComptableInventaireInspecteurNib());
		getMonComptableInventaireInspecteurNibCtrl()
				.creationFenetre(
						getMonComptableInventaireInspecteurNib(),
						ToolsSwing
								.formaterStringU(FENETRE_INSPECTEUR_INVENTAIRE),
						this);
		getMonComptableInventaireInspecteurNib().setPreferredSize(
				new java.awt.Dimension(630, 630));
		getMonComptableInventaireInspecteurNib().setSize(630, 630);
		getMonComptableInventaireInspecteurNib().setMinimumSize(
				new java.awt.Dimension(630, 630));

		// creation de l'assistant Depense
		setMonDepenseInspecteurNibCtrl(new DepenseInspecteurNibCtrl(arg0, arg1,
				arg2, arg3, arg4));
		setMonDepenseInspecteurNib(new DepenseInspecteurNib());
		getMonDepenseInspecteurNibCtrl().creationFenetre(
				getMonDepenseInspecteurNib(),
				ToolsSwing.formaterStringU(FENETRE_INSPECTEUR_DEPENSE), this);
		getMonDepenseInspecteurNib().setPreferredSize(
				new java.awt.Dimension(600, 500));
		getMonDepenseInspecteurNib().setSize(600, 500);
	}

	public void creationFenetre(ComptableNib comptableNib, String title) {
		super.creationFenetre(comptableNib, title);
		setMonComptableNib(comptableNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void afficherFenetre() {
		super.afficherFenetre();
		if (((ApplicationCorossol) app).getCurrentCommande() == null) {
			masquerFenetre();
			fenetreDeDialogueInformation("Impossible \n Vous devez choisir une commande !");
		}
		setMesBiensAttenteDG();
		setNumeroDG();
		setNumeroDetailDG();
	}

	private void bindingAndCustomization() {

		try {
			// creation de la table view des biens en attente
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Libelle", 200, JLabel.CENTER,
					"livraisonDetail.lidLibelle", "String"));
			lesColumns.addObject(new ColumnData("Montant", 80, JLabel.CENTER,
					"invMontantAcquisition", "String"));
			// lesColumns.addObject(new ColumnData( "Num. Serie",80,
			// JLabel.CENTER ,"invSerie","String"));
			lesColumns.addObject(new ColumnData("Imput.", 80, JLabel.CENTER,
					"livraisonDetail.planComptable.pcoNum", "String"));
			lesColumns.addObject(new ColumnData("Ub", 80, JLabel.CENTER,
					"livraisonDetail.organ.orgUb", "String"));
			lesColumns.addObject(new ColumnData("Cr", 80, JLabel.CENTER,
					"livraisonDetail.organ.orgCr", "String"));

			NSMutableArray lesColumnsInvComptable = new NSMutableArray();
			lesColumnsInvComptable.addObject(new ColumnData("Libelle", 200,
					JLabel.CENTER, "livraisonDetail.lidLibelle", "String"));
			lesColumnsInvComptable
					.addObject(new ColumnData(
							"Imput.",
							80,
							JLabel.CENTER,
							"inventaireComptable.cleInventaireComptable.planComptable.pcoNum",
							"String"));
			lesColumnsInvComptable.addObject(new ColumnData("Ub", 80,
					JLabel.CENTER, "livraisonDetail.organ.orgUb", "String"));
			lesColumnsInvComptable.addObject(new ColumnData("Cr", 80,
					JLabel.CENTER, "livraisonDetail.organ.orgCr", "String"));

			setMesBiensAttenteTBV(new JTableViewCocktail(lesColumns,
					getMesBiensAttenteDG(), new Dimension(100, 100),
					JTable.AUTO_RESIZE_LAST_COLUMN));
			// getMonComptableNib().getJPanelListeBiens().add(getMesBiensAttenteTBV());
			getMonComptableNib().getMesBiensAttenteTBV().initTableViewCocktail(
					getMesBiensAttenteTBV());

			// setMesBiensAttenteDG();

			// creation de la table view des inventaires comptables
			NSMutableArray lesColumnsDetail = new NSMutableArray();
			lesColumnsDetail.addObject(new ColumnData("Numero", 150,
					JLabel.CENTER,
					EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY + "."
							+ EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY,
					"String"));
			lesColumnsDetail.addObject(new ColumnData("Amort.", 80,
					JLabel.CENTER,
					EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY + "."
							+ EOCleInventaireComptable.CLIC_TYPE_AMORT_KEY,
					"String"));
			lesColumnsDetail
					.addObject(new ColumnData(
							"Montant",
							100,
							JLabel.CENTER,
							EOInventaireComptable.INVC_MONTANT_AMORTISSABLE_AVEC_ORVS_KEY,
							"String"));
			lesColumnsDetail.addObject(new ColumnData("Liquide ?", 80,
					JLabel.CENTER, EOInventaireComptable.DPCO_ID_BOOLEAN_KEY,
					"boolean"));

			setNumeroDG();
			setMesNumeroTBV(new JTableViewCocktail(lesColumnsDetail,
					getMesNumeroDG(), new Dimension(100, 100),
					JTable.AUTO_RESIZE_LAST_COLUMN));
			// getMonComptableNib().getJPanelComptable().add(getMesNumeroTBV());
			getMonComptableNib().getMesNumeroTBV().initTableViewCocktail(
					getMesNumeroTBV());

			setMesNumeroDetailTBV(new JTableViewCocktail(
					lesColumnsInvComptable, getMesNumeroDetailDG(),
					new Dimension(100, 100), JTable.AUTO_RESIZE_OFF));
			// getMonComptableNib().getJPanelBiensInventories().add(getMesNumeroDetailTBV());
			getMonComptableNib().getMesNumeroDetailTBV().initTableViewCocktail(
					getMesNumeroDetailTBV());

			setNumeroDetailDG();

			// les listeners
			getMesNumeroTBV().addDelegateSelectionListener(this,
					METHODE_CHANGEMENT_SELECTION_BIENS);
			getMesNumeroTBV().addDelegateKeyListener(this,
					METHODE_CHANGEMENT_SELECTION_BIENS);

			// multi ou mono selection dans les tbv
			getMesNumeroTBV().getTable().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			getMesNumeroDetailTBV().getTable().setSelectionMode(
					ListSelectionModel.SINGLE_SELECTION);
			getMesBiensAttenteTBV().getTable().setSelectionMode(
					ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// le cablage des objets
			getMonComptableNib().getJButtonCocktailFermer()
					.addDelegateActionListener(this, METHODE_ACTION_FERMER);
			getMonComptableNib().getJButtonCocktailAjouter()
					.addDelegateActionListener(this, METHODE_ACTION_AJOUTER);
			getMonComptableNib().getJButtonCocktailAnnuler()
					.addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonComptableNib().getJButtonCocktailModifier()
					.addDelegateActionListener(this, METHODE_ACTION_MODIFIER);
			getMonComptableNib().getJButtonCocktailImprimer()
					.addDelegateActionListener(this, METHODE_ACTION_IMPRIMER);
			getMonComptableNib().getJButtonCocktailDepense()
					.addDelegateActionListener(this,
							METHODE_ACTION_MODIFIER_DEPENSE);

			// les images des objets
			getMonComptableNib().getJButtonCocktailModifier().setIcone(
					IconeCocktail.REFRESH);// "refresh");
			getMonComptableNib().getJButtonCocktailDepense().setIcone(
					IconeCocktail.PAGE);// "page_obj");
			getMonComptableNib().getJButtonCocktailAnnuler().setIcone(
					IconeCocktail.REFUSER);// "backward_nav");
			getMonComptableNib().getJButtonCocktailAjouter().setIcone(
					IconeCocktail.AJOUTER);// "forward_nav");
			getMonComptableNib().getJButtonCocktailFermer().setIcone(
					IconeCocktail.FERMER);// "close_view");
			getMonComptableNib().getJButtonCocktailImprimer().setIcone(
					IconeCocktail.IMPRIMER);// "print_edit");

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol) app).getObserveurExerciceSelection()
					.addObserverForMessage(this, "changementExercice");

			// obseration des changements de selection de commande :
			((ApplicationCorossol) app).getObserveurCommandeSelection()
					.addObserverForMessage(this, "changementCommande");
			((ApplicationCorossol) app).getObserveurCommandeSelection()
					.addObserverForMessage(this, "changementCommandeDetail");

			// sera modal lors d une transaction
			app.addLesPanelsModal(getMonComptableNib());

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",
					e.getMessage(), true);
		}
	}

	public void changementSelectionDetail() {
		setNumeroDetailDG();
	}

	// les DG
	private void setMesBiensAttenteDG() {
		getMesBiensAttenteDG().removeAllObjects();
		if (getMesBiensAttenteTBV() != null)
			getMesBiensAttenteTBV().refresh();

		try {
			if (((ApplicationCorossol) app).getCurrentCommande() != null)
				getMesBiensAttenteDG().addObjectsFromArray(
						FinderComptable.findLesBiensAttente(app,
								((ApplicationCorossol) app)
										.getCurrentCommande()));
			if (getMesBiensAttenteTBV() != null)
				getMesBiensAttenteTBV().refresh();
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME setMesBiensAttenteDG ! \n"
					+ e.toString());
		}
	}

	private void setNumeroDG() {
		getMesNumeroDG().removeAllObjects();
		if (getMesNumeroTBV() != null)
			getMesNumeroTBV().refresh();
		try {
			if (((ApplicationCorossol) app).getCurrentCommande() != null)
				getMesNumeroDG().addObjectsFromArray(
						FinderComptable.findLesBiensInventorieDeLaCommande(app,
								((ApplicationCorossol) app)
										.getCurrentCommande()));
			if (getMesNumeroTBV() != null)
				getMesNumeroTBV().refresh();
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME setNumeroDG ! \n"
					+ e.toString());
		}
	}

	private void setNumeroDetailDG() {
		getMesNumeroDetailDG().removeAllObjects();
		if (getMesNumeroDetailTBV() != null)
			getMesNumeroDetailTBV().refresh();

		try {
			if (getMesNumeroTBV().getSelectedObjects().count() == 1)
				getMesNumeroDetailDG()
						.addObjectsFromArray(
								FinderComptable
										.findLesBiensInventories(
												app,
												(EOInventaireComptable) getMesNumeroTBV()
														.getSelectedObjects()
														.objectAtIndex(0)));
			if (getMesNumeroDetailTBV() != null)
				getMesNumeroDetailTBV().refresh();
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME setNumeroDetailDG ! \n"
					+ e.getMessage());
		}
	}

	// methode cablage
	public void actionModifier() {
		// trace(METHODE_ACTION_FERMER);
		if (getMesNumeroTBV().getSelectedObjects().count() == 1)
			getMonComptableInventaireInspecteurNibCtrl()
					.afficherFenetreModification(
							(EOInventaireComptable) getMesNumeroTBV()
									.getSelectedObjects().objectAtIndex(0));
	}

	public void actionModifierDepense() {
		// trace(METHODE_ACTION_FERMER);
		if (getMesNumeroTBV().getSelectedObjects().count() != 0)
			getMonDepenseInspecteurNibCtrl().afficherFenetre(
					getMesNumeroTBV().getSelectedObjects());
	}

	public void actionImprimer() {
		for (int i = 0; i < getMesNumeroTBV().getSelectedObjects().count(); i++) {
			try {
				NSMutableDictionary parameters = new NSMutableDictionary();
				parameters.takeValueForKey(new Integer(
						((EOInventaireComptable) getMesNumeroTBV()
								.getSelectedObjects().objectAtIndex(i))
								.cleInventaireComptable().clicIdBis()
								.intValue()), "CLIC_ID");
				parameters.takeValueForKey(
						app.getApplicationParametre("REP_BASE_JASPER_PATH"),
						"SUBREPORT_DIR");

				app.getToolsCocktailReports().imprimerReportParametres(
						"fiche_inventaire.jasper", parameters,
						"fiche_inventaire" + UtilCtrl.lastModif());
			} catch (Exception e) {
				System.out.println("actionImprimer OUPS" + e);
			}
			// fenetreDeDialogueInformation("Bientot ! \n Prochaine version");
		}
	}

	public void actionFermer() {
		// trace(METHODE_ACTION_FERMER);
		masquerFenetre();
	}

	public void actionAnnuler() {
		if (getMesNumeroTBV().getSelectedObjects().count() == 1)
			if (((EOInventaireComptable) getMesNumeroTBV().getSelectedObjects()
					.objectAtIndex(0)).dpcoIdBoolean())
				fenetreDeDialogueInformation("ATTENTION ! \n Vous devez annuler le lien avec la depense");
			else {
				try {
					EOInventaireComptable inventaireSelectionnee = (EOInventaireComptable) getMesNumeroTBV()
							.getSelectedObjects().objectAtIndex(0);
					getMonProcessInventaireComptable()
							.supprimerUnInventaireComptable(app,
									inventaireSelectionnee,
									app.getCurrentUtilisateur());

					app.getAppEditingContext().saveChanges();
				} catch (Exception e) {
					app.getAppEditingContext().revert();
					fenetreDeDialogueInformation("PROBLEME ! \n" + e.toString());
				}
				assistantsTerminer(this);
			}
	}

	public void actionAjouter() {
		// trace(METHODE_ACTION_FERMER);
		try {

			if (getMesBiensAttenteTBV().getSelectedObjects().count() == 0) {
				fenetreDeDialogueInformation("Vous devez sélectionner un ou plusieurs bien(s) en attente.");
				return;
			}

			EOOrgan tmpEOOrgan = ((EOInventaire) getMesBiensAttenteTBV()
					.getSelectedObjects().objectAtIndex(0)).livraisonDetail()
					.organ();
			;
			EOExercice tmpEOExercice = ((EOInventaire) getMesBiensAttenteTBV()
					.getSelectedObjects().objectAtIndex(0)).livraisonDetail()
					.livraison().exercice();
			;
			EOPlanComptable tmpEOPlanComptable = ((EOInventaire) getMesBiensAttenteTBV()
					.getSelectedObjects().objectAtIndex(0)).livraisonDetail()
					.planComptable();

			EOInventaireComptable tmpInventaireComptable = null;

			// TODO VERIFIER QE TOUTE LA SELECTION ES DE MEME : UFR PCO_NUM EXER
			// CR
			for (int i = 1; i < getMesBiensAttenteTBV().getSelectedObjects()
					.count(); i++) {
				if (!tmpEOOrgan.equals(((EOInventaire) getMesBiensAttenteTBV()
						.getSelectedObjects().objectAtIndex(i))
						.livraisonDetail().organ()))
					throw new Exception(LA_SELECTION_N_EST_PAS_DE_MEME_CR);
				if (!tmpEOExercice
						.equals(((EOInventaire) getMesBiensAttenteTBV()
								.getSelectedObjects().objectAtIndex(i))
								.livraisonDetail().livraison().exercice()))
					throw new Exception(LA_SELECTION_N_EST_PAS_DE_MEME_EXERCICE);
				if (!tmpEOPlanComptable
						.equals(((EOInventaire) getMesBiensAttenteTBV()
								.getSelectedObjects().objectAtIndex(i))
								.livraisonDetail().planComptable()))
					throw new Exception(
							LA_SELECTION_N_EST_PAS_DE_MEME_IMPUTATION);
			}

			tmpInventaireComptable = getMonProcessInventaireComptable()
					.ajouterUnInventaireComptable(
							app.getAppEditingContext(),
							app.getCurrentUtilisateur(),
							null,
							app.getCurrentExercice(),
							null,
							calcSommeOfBigDecimals(getMesBiensAttenteTBV()
									.getSelectedObjects(),
									EOInventaire.INV_MONTANT_ACQUISITION_KEY),
							calcSommeOfBigDecimals(getMesBiensAttenteTBV()
									.getSelectedObjects(),
									EOInventaire.INV_MONTANT_ACQUISITION_KEY),
							null, null,
							getMesBiensAttenteTBV().getSelectedObjects());

			for (int i = 0; i < getMesBiensAttenteTBV().getSelectedObjects()
					.count(); i++)
				getMonProcessInventaire().addInventaireComptable(
						(EOInventaire) getMesBiensAttenteTBV()
								.getSelectedObjects().objectAtIndex(i),
						tmpInventaireComptable);

			getMonComptableInventaireInspecteurNibCtrl()
					.afficherFenetreCreation(tmpInventaireComptable,
							tmpEOPlanComptable, tmpEOOrgan.orgCr(),
							tmpEOOrgan.orgUb());

		} catch (Exception e) {
			System.out.println(e);
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME ! \n" + e.toString());
		}
		assistantsTerminer(this);
	}

	public void actionDepense() {
	}

	// affichages
	public void changementCommande() {
		setMesBiensAttenteDG();
		setNumeroDG();
		setNumeroDetailDG();
	}

	public void changementCommandeDetail() {
		setMesBiensAttenteDG();
	}

	private ProcessInventaire getMonProcessInventaire() {
		return monProcessInventaire;
	}

	private ProcessInventaireComptable getMonProcessInventaireComptable() {
		return monProcessInventaireComptable;
	}

	private NSMutableArrayDisplayGroup getMesBiensAttenteDG() {
		return mesBiensAttenteDG;
	}

	private JTableViewCocktail getMesBiensAttenteTBV() {
		return mesBiensAttenteTBV;
	}

	private void setMesBiensAttenteTBV(JTableViewCocktail mesBiensAttenteTBV) {
		this.mesBiensAttenteTBV = mesBiensAttenteTBV;
	}

	private NSMutableArrayDisplayGroup getMesNumeroDetailDG() {
		return mesNumeroDetailDG;
	}

	private JTableViewCocktail getMesNumeroDetailTBV() {
		return mesNumeroDetailTBV;
	}

	private void setMesNumeroDetailTBV(JTableViewCocktail mesNumeroDetailTBV) {
		this.mesNumeroDetailTBV = mesNumeroDetailTBV;
	}

	private NSMutableArrayDisplayGroup getMesNumeroDG() {
		return mesNumeroDG;
	}

	private JTableViewCocktail getMesNumeroTBV() {
		return mesNumeroTBV;
	}

	private void setMesNumeroTBV(JTableViewCocktail mesNumeroTBV) {
		this.mesNumeroTBV = mesNumeroTBV;
	}

	private ComptableNib getMonComptableNib() {
		return monComptableNib;
	}

	private void setMonComptableNib(ComptableNib monComptableNib) {
		this.monComptableNib = monComptableNib;
	}

	private ComptableInventaireInspecteurNib getMonComptableInventaireInspecteurNib() {
		return monComptableInventaireInspecteurNib;
	}

	private void setMonComptableInventaireInspecteurNib(
			ComptableInventaireInspecteurNib monComptableInventaireInspecteurNib) {
		this.monComptableInventaireInspecteurNib = monComptableInventaireInspecteurNib;
	}

	private ComptableInventaireInspecteurNibCtrl getMonComptableInventaireInspecteurNibCtrl() {
		return monComptableInventaireInspecteurNibCtrl;
	}

	public void setMonComptableInventaireInspecteurNibCtrl(
			ComptableInventaireInspecteurNibCtrl monComptableInventaireInspecteurNibCtrl) {
		this.monComptableInventaireInspecteurNibCtrl = monComptableInventaireInspecteurNibCtrl;
	}

	public void assistantsAnnuler(Object sender) {
		setMesBiensAttenteDG();
		setNumeroDG();
		setNumeroDetailDG();
	}

	public void assistantsTerminer(Object sender) {
		setMesBiensAttenteDG();
		setNumeroDG();
		setNumeroDetailDG();
	}

	private ProcessCleInventaireComptable getMonProcessCleInventaireComptable() {
		return monProcessCleInventaireComptable;
	}

	private DepenseInspecteurNib getMonDepenseInspecteurNib() {
		return monDepenseInspecteurNib;
	}

	private void setMonDepenseInspecteurNib(
			DepenseInspecteurNib monDepenseInspecteurNib) {
		this.monDepenseInspecteurNib = monDepenseInspecteurNib;
	}

	private DepenseInspecteurNibCtrl getMonDepenseInspecteurNibCtrl() {
		return monDepenseInspecteurNibCtrl;
	}

	private void setMonDepenseInspecteurNibCtrl(
			DepenseInspecteurNibCtrl monDepenseInspecteurNibCtrl) {
		this.monDepenseInspecteurNibCtrl = monDepenseInspecteurNibCtrl;
	}

	private BigDecimal calcSommeOfBigDecimals(NSArray array, String keyName) {
		return calcSommeOfBigDecimals(array.vector(), keyName);
	}

	private BigDecimal calcSommeOfBigDecimals(Vector array, String keyName) {
		BigDecimal res = (new BigDecimal((double) 0)).setScale(2);
		for (Iterator iter = array.iterator(); iter.hasNext();) {
			EOEnterpriseObject element = (EOEnterpriseObject) iter.next();
			res = res.add((BigDecimal) element.valueForKey(keyName));
		}
		return res;
	}
}
