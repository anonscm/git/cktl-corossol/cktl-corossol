package org.cocktail.corossol.client.nibctrl;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.ImageIcon;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOPlanComptableAmo;
import org.cocktail.application.client.swingfinder.SwingFinderInterface;
import org.cocktail.application.client.swingfinder.SwingFinderPlanComptable;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.ToolsSwing;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.Privileges;
import org.cocktail.corossol.client.UtilCtrl;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;
import org.cocktail.corossol.client.eof.metier.EOParametre;
import org.cocktail.corossol.client.eof.procedure.ProcedureEnregistrerInventaireComptable;
import org.cocktail.corossol.client.eof.process.ProcessCleInventaireComptable;
import org.cocktail.corossol.client.eof.process.ProcessInventaireComptable;
import org.cocktail.corossol.client.finder.FinderComptable;
import org.cocktail.corossol.client.finder.FinderDegressifCoef;
import org.cocktail.corossol.client.finder.FinderParametre;
import org.cocktail.corossol.client.nib.ComptableInventaireInspecteurNib;
import org.cocktail.corossol.client.nib.ComptableInventaireRechercherNib;
import org.cocktail.corossol.client.nib.DateCtrl;
import org.cocktail.corossol.client.zutil.DateFieldListener;
import org.cocktail.corossol.client.zutil.FormattedFieldProvider;
import org.cocktail.corossol.common.eof.repartition.InventaireComptableHelper;
import org.cocktail.corossol.common.zutil.CalculMontantUtil;
import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class ComptableInventaireInspecteurNibCtrl extends NibCtrl implements SwingFinderInterface{

	private static final String CREATION = "CREATION";
	private static final String MODIFICATION = "MODIFICATION";

	private static final String FENETRE_DE_SELECTION_D_INVENTAIRE_COMPTABLE = "Fenetre de selection d'un inventaire existant";

	// le nib
	private ComptableInventaireInspecteurNib monComptableInventaireInspecteurNib = null;
	private String currentAction = null;

	private EOInventaireComptable currentInventaireComptable = null;
	private NibCtrl parentControleur = null;
	private EOCleInventaireComptable currentCleInventaireComptable = null;

	// inteface de recherche d'un numero d inventaire
	private ComptableInventaireRechercherNib monComptableInventaireRechercherNib = null;
	private ComptableInventaireRechercherNibCtrl monComptableInventaireRechercherNibCtrl = null;
	private SwingFinderPlanComptable monSwingFinderPlanComptable = null;
	private OriginesFinancementNibCtrl originesCtrl = null;

	public ComptableInventaireInspecteurNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		// creation du nib commande
		originesCtrl = new OriginesFinancementNibCtrl(ctrl, alocation_x, alocation_y, 450, 290);
		originesCtrl.creationFenetre(this);

		setMonComptableInventaireRechercherNibCtrl(new ComptableInventaireRechercherNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonComptableInventaireRechercherNib(new ComptableInventaireRechercherNib());
		getMonComptableInventaireRechercherNibCtrl().creationFenetre(getMonComptableInventaireRechercherNib(), ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_D_INVENTAIRE_COMPTABLE), this);
		getMonComptableInventaireRechercherNib().setPreferredSize(new java.awt.Dimension(525, 255));
		getMonComptableInventaireRechercherNib().setSize(525, 255);

		monSwingFinderPlanComptable = new SwingFinderPlanComptable(app, this,alocation_x , alocation_y, 525, 255,true);

		setWithLogs(false);
	}

	public void creationFenetre(ComptableInventaireInspecteurNib leComptableInventaireInspecteurNib, String title,NibCtrl parentControleur) {
		super.creationFenetre(leComptableInventaireInspecteurNib, title);
		setMonComptableInventaireInspecteurNib(leComptableInventaireInspecteurNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
		this.parentControleur =  parentControleur;
	}

	public void majOrigineFinancement(EOExercice exercice) {
		if (originesCtrl == null) {
			return;
		}
		originesCtrl.majOrigineFinancement(exercice);
	}
	
	public void afficherFenetreCreation(EOInventaireComptable tmpInventaireComptable,EOPlanComptable pco,String cr , String comp) 
	  throws NumberFormatException, Exception {
		super.afficherFenetre();
		
		EOPlanComptableAmo pcoAmo = FinderComptable.findLeCompteAmortissement(app, pco);
		if (pcoAmo == null) {
			masquerFenetre();
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME  !\n Impossible de determiner le compte d'amortissement !\n " +
					"Verifier votre parametrage dans Maracuja pour le compte "+pco.pcoNum()+" !");
			return;
		}

		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonComptableInventaireInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();
		setCurrentAction(CREATION);
		
		currentInventaireComptable = tmpInventaireComptable;
		ProcessInventaireComptable monProcessInventaireComptable = new ProcessInventaireComptable();

		//parametres
		String parametreTypeAmortissement = FinderParametre.parametreTypeAmortissement(app, app.getCurrentExercice());
		boolean parametreProrataTemporis = FinderParametre.isParametreProrataTemporis(app, app.getCurrentExercice());

		// creation du CleInventaireComptable
		Integer duree = FinderComptable.findLeCompteAmortissementDuree(app, pco).intValue();
		
		ProcessCleInventaireComptable  monProcessCleInventaireComptable = new ProcessCleInventaireComptable();
		currentCleInventaireComptable = monProcessCleInventaireComptable.ajouterUneCleInventaireComptable(app.getAppEditingContext(),
				pcoAmo, pco, app.getCurrentExercice(), typeAmortissementForParametre(parametreTypeAmortissement),
				parametreProrataTemporis ? new Integer(1):new Integer(0), "automatique", new Integer(1), "etat", duree, 
			    cr, comp);
		//FIXME : quelle est la bonne date ?

		//init du popup		
		if (parametreTypeAmortissement == null) {
			getMonComptableInventaireInspecteurNib().getJComboBoxTypeAmort().setEnabled(true);
		} else {
			getMonComptableInventaireInspecteurNib().getJComboBoxTypeAmort().setEnabled(false);
		}

		// init de la check box
		getMonComptableInventaireInspecteurNib().getJCheckBoxProrata().setSelected(parametreProrataTemporis);
		
		monProcessInventaireComptable.addCleInventaireComptable(currentInventaireComptable, currentCleInventaireComptable);
		afficher();
	}

	public void afficherFenetreModification(EOInventaireComptable tmpInventaireComptable) {
		super.afficherFenetre();
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonComptableInventaireInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();
		setCurrentAction(MODIFICATION);
		currentInventaireComptable = tmpInventaireComptable;
		currentCleInventaireComptable = currentInventaireComptable.cleInventaireComptable();

		majOrigineFinancement(tmpInventaireComptable.exercice());

		// init du popup
		String parametreTypeAmortissement = FinderParametre.parametreTypeAmortissement(app, currentInventaireComptable.exercice());
		if (parametreTypeAmortissement == null
				|| !currentCleInventaireComptable.clicTypeAmort().equals(typeAmortissementForParametre(parametreTypeAmortissement))) {
			getMonComptableInventaireInspecteurNib().getJComboBoxTypeAmort().setEnabled(true);
		} else {
			getMonComptableInventaireInspecteurNib().getJComboBoxTypeAmort().setEnabled(false);
		}

		afficher();
	}

	// Static pour pouvoir la tester.
	public static String typeAmortissementForParametre(String parametre) {
        if (parametre == null) {
            return null;
        }
        
        if (EOParametre.DEGRESSIF.equals(parametre)) {
            return EOCleInventaireComptable.DEGRESSIF;
        }
        
        return EOCleInventaireComptable.LINEAIRE;
    }

	
	private String DureeAmort = null;
	private String nbEtiq = null;
	private void sauverAmortissementEtiquette() {
		DureeAmort = getMonComptableInventaireInspecteurNib().getJTextFieldCocktailDuree().getText();
		nbEtiq = getMonComptableInventaireInspecteurNib().getJTextFieldCocktailNbEtiquettes().getText();
	}

	private void bindingAndCustomization() {
		getMonComptableInventaireInspecteurNib().initTableView();

		// listener sur date
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldDate().addFocusListener(new DateFieldListener());

		// le cablage des objets
		getMonComptableInventaireInspecteurNib().getJButtonCocktailValider().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				actionValider();
			}
		});
		getMonComptableInventaireInspecteurNib().getJButtonCocktailAnnuler().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				actionAnnuler();
			}
		});
		getMonComptableInventaireInspecteurNib().getJButtonCocktailNumRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				actionRechercherCle();
			}
		});
		getMonComptableInventaireInspecteurNib().getJButtonCocktailCompteAmort().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				monSwingFinderPlanComptable.afficherFenetreFinder(null);
			}
		});

		getMonComptableInventaireInspecteurNib().getjButtonAjouterOrigine().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				sauverAmortissementEtiquette();
				originesCtrl.afficherFenetre(currentInventaireComptable);
			}
		});
		getMonComptableInventaireInspecteurNib().getjButtonModifierOrigine().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				EOInventaireComptableOrig orig = getMonComptableInventaireInspecteurNib().inventaireComptableOrigSelectionne();
				if (orig == null) {
					return;
				}
				sauverAmortissementEtiquette();
				originesCtrl.afficherFenetrePourModification(currentInventaireComptable, orig);
			}
		});
		getMonComptableInventaireInspecteurNib().getjButtonSupprimerOrigine().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				sauverAmortissementEtiquette();
				actionSupprimerOrigine();
			}
		});

		// les images des objets
		getMonComptableInventaireInspecteurNib().getJButtonCocktailValider().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.VALIDER));
		getMonComptableInventaireInspecteurNib().getJButtonCocktailAnnuler().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.FERMER));
		getMonComptableInventaireInspecteurNib().getJButtonCocktailNumRechercher().setIcon(
				(ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.SHOW));
		getMonComptableInventaireInspecteurNib().getJButtonCocktailCompteAmort().setIcon(
				(ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.SHOW));
		getMonComptableInventaireInspecteurNib().getjButtonAjouterOrigine().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.AJOUTER));
		getMonComptableInventaireInspecteurNib().getjButtonModifierOrigine()
				.setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.MODIFIER));
		getMonComptableInventaireInspecteurNib().getjButtonSupprimerOrigine().setIcon(
				(ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.SUPPRIMER));

		// cablage
		monSwingFinderPlanComptable.getMonSwingFinderEOPlanComptableNib().getJCheckBoxBudgetaire().setEnabled(true);
		monSwingFinderPlanComptable.getMonSwingFinderEOPlanComptableNib().getJTextFieldCocktailCompte().setText("2*");

		setEnabledComposant(getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCompteAmort(), false);
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getJTextFieldCocktailNumero(), false);
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getJTextFieldCocktailUfr(), false);
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCompte(), false);
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCr(), false);
		
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable().setFormatterFactory(FormattedFieldProvider.getMontantAvecDecimalesFormatterFactory());
		
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if ((ae.getID() & ActionEvent.KEY_EVENT_MASK) != 0) {
					updateTotauxOrigines();
				}
			}
		});

		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable().getDocument().addDocumentListener(new DocumentListener() {
			public void insertUpdate(DocumentEvent de) {
				updateTotauxOrigines();
			}

			public void removeUpdate(DocumentEvent de) {
			}

			public void changedUpdate(DocumentEvent de) {
			}
		});
		
		// Montant d'acquisition non modifiable
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAcquisition(), false);
		// Montant d'amortisssement non modifiable
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable(), false);

		// Total montants
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getjFormattedTextFieldTotalMontants(), false);
//		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldTotalMontants().setFormatterFactory(FormattedFieldProvider.getMontantAvecDecimalesFormatterFactory());
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldTotalMontants().setDisabledTextColor(CocktailConstantes.BG_COLOR_BLACK);

		// Total pourcentages
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getjFormattedTextFieldTotalPourcentage(), false);
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldTotalPourcentage().setFormatterFactory(
				new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00%"))));
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldTotalPourcentage().setDisabledTextColor(CocktailConstantes.BG_COLOR_BLACK);

		// sera modal lors d une transaction
		app.addLesPanelsModal(getMonComptableInventaireInspecteurNib());

		// obseration des changements de selection de lexercice :
		((ApplicationCorossol) app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");

	}

	/**
	 * @return
	 */
	private boolean isMontantAmortissableModifiable() {
		/* ** On ne peut plus modifier le montant amortissable **
		Integer exercice = (new GregorianCalendar()).get(Calendar.YEAR);
		Integer invcExercice = currentInventaireComptable.exercice().exeExercice();
		boolean prorataTemporis = (new Integer(1)).equals(currentCleInventaireComptable.clicProRata());
		// Année en cours si prorata temporis, année suivante si non prorata temporis, ou création de l'invc
		if ((exercice.equals(invcExercice) && prorataTemporis) 
				|| (exercice.equals(invcExercice + 1) && !prorataTemporis)
				|| CREATION.equals(getCurrentAction())) {
			// Recherche des droits de l'utilisateur 
			return Privileges.isPrivileges(app.getMesPrivileges(), Privileges.INVAMORT);
		}
		*/
		return false;
	}
	
	private boolean isDateAmortissementModifiable() {
		return Privileges.isPrivileges(app.getMesPrivileges(), Privileges.INVAMORT);
	}

	public void masquerFenetre() {
		originesCtrl.masquerFenetre();
		super.masquerFenetre();
		DureeAmort = null;
		nbEtiq = null;
	}

	// methodes cablage
	public void actionSupprimerOrigine() {
		EOInventaireComptableOrig orig = getMonComptableInventaireInspecteurNib().inventaireComptableOrigSelectionne();
		if (orig == null) {
			return;
		}
		
		orig.setInventaireComptableRelationship(null);
		afficher();
	}
	
	public void actionAnnuler() {
		app.getAppEditingContext().revert();
		// juste pour mettre à zéro les changements dans corossol
		app.getAppEditingContext().saveChanges();
				
		app.addLesPanelsModal(getMonComptableInventaireInspecteurNib());
		((ApplicationCorossol) app).retirerLesGlassPane();
		masquerFenetre();
		getParentControleur().assistantsAnnuler(this);
	}
    
	private int compareDate(NSTimestamp d1, NSTimestamp d2, String sFormat) {
		// 0 = egal 1 plus grand et -1 plus petit
		int sd1 = Integer.valueOf(DateCtrl.dateToString(d1, sFormat));
		int sd2 = Integer.valueOf(DateCtrl.dateToString(d2, sFormat));
		if (sd1 > sd2) {
			return 1;
		}
		if (sd1 < sd2) {
			return -1;
		}
		return 0;
	}
	
	public void actionValider() {
		// recup des donnees
		try {

			if (currentInventaireComptable.inventaireComptableOrigs() == null || currentInventaireComptable.inventaireComptableOrigs().count() == 0) {
				EODialogs.runErrorDialog("Attention", "Veuillez sélectionner au moins une origine de financement !");
				return;
			}

			try {
				InventaireComptableHelper.checkSomme(currentInventaireComptable);
			} catch (ValidationException e) {
				EODialogs.runErrorDialog("Attention", e.getMessage());
				return;
			}

			if (isDateAmortissementModifiable()) {

				NSTimestamp dppDateServiceFait = null;
				if (currentInventaireComptable.vDepense() != null) {
					dppDateServiceFait = currentInventaireComptable.vDepense().dppDateServiceFait();
				}
				NSTimestamp dateServiceReel = DateCtrl.stringToDate(getMonComptableInventaireInspecteurNib().getjFormattedTextFieldDate().getText());

				// Cas des inventaires non budgétaires
				// La date de service fait se trouve ailleurs
				if (currentInventaireComptable.inventaireNonBudgetaires() != null && currentInventaireComptable.inventaireNonBudgetaires().count() > 0) {
					EOInventaireNonBudgetaire invNHB = (EOInventaireNonBudgetaire) currentInventaireComptable.inventaireNonBudgetaires().get(0);
					dppDateServiceFait = invNHB.inbDateAcquisition();
				}

				// Date antérieur à la date de service Fait
				if ((dppDateServiceFait == null && dateServiceReel != null)
						|| (dateServiceReel != null && dppDateServiceFait != null && compareDate(dateServiceReel, dppDateServiceFait, "%Y%m%d") < 0)) {
					String errorMessage = "La date de début d'amortissement doit être postérieur ou égale à la date de service fait (";
					if (currentInventaireComptable.inventaireNonBudgetaires() != null && currentInventaireComptable.inventaireNonBudgetaires().count() > 0) {
						errorMessage = "La date de début d'amortissement doit être postérieur ou égale à la date de début d'acquisition (";
					}
					EODialogs.runErrorDialog("Attention", errorMessage + DateCtrl.dateToString(dppDateServiceFait) + ") !");

					// Pour éviter le blocage
					if (dppDateServiceFait == null && dateServiceReel != null) {
						getMonComptableInventaireInspecteurNib().getjFormattedTextFieldDate().setText("");
						getMonComptableInventaireInspecteurNib().getjFormattedTextFieldDate().setValue(null);
					}
					return;
				}

				// Dates qui ne sont pas dans la même année
				if (dateServiceReel != null && dppDateServiceFait != null && compareDate(dateServiceReel, dppDateServiceFait, "%Y") != 0) {
					if (!EODialogs.runConfirmOperationDialog(
							"Attention",
							"La date de début d'amortissement n'est pas dans la même année que la date de service fait ("
									+ DateCtrl.dateToString(dppDateServiceFait) + "). Confirmez-vous ?", "OUI", "NON")) {
						return;
					}
				}

				if (dateServiceReel == null) {
					dateServiceReel = dppDateServiceFait;
				}
				currentInventaireComptable.setInvcDateAcquisition(dateServiceReel);
			}

			if (isMontantAmortissableModifiable()) {	
				// Contrôle du montant amortissable
				BigDecimal mntAmortissable = null;
				try {
					String mntText = getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable().getValue().toString();
					mntAmortissable = UtilCtrl.stringToBigDecimal(mntText);				
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				
				BigDecimal mntAcquisition = currentInventaireComptable.invcMontantAcquisition();
				if (mntAmortissable == null || mntAmortissable.compareTo(mntAcquisition) > 0) {
					EODialogs.runErrorDialog("Attention", "Le montant amortissable (" + mntAmortissable
							+ ") ne peux pas être supérieur au montant d'acquisition (" + mntAcquisition + ").");
					currentInventaireComptable.setInvcMontantAmortissable(mntAcquisition);
					getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable().setValue(mntAcquisition);
					return;
				} else {
					currentInventaireComptable.setInvcMontantAmortissable(mntAmortissable);
				}
				getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable().setValue(currentInventaireComptable.invcMontantAmortissableAvecOrvs());
				
			}

			currentInventaireComptable.cleInventaireComptable().setClicDureeAmort(
					new Integer(getMonComptableInventaireInspecteurNib().getJTextFieldCocktailDuree().getText()));
			currentInventaireComptable.cleInventaireComptable().setClicNbEtiquette(
					new Integer(getMonComptableInventaireInspecteurNib().getJTextFieldCocktailNbEtiquettes().getText()));
			currentInventaireComptable.cleInventaireComptable().setClicTypeAmort(
					(String) getMonComptableInventaireInspecteurNib().getJComboBoxTypeAmort().getSelectedItem());
			currentInventaireComptable.cleInventaireComptable().setClicProRata(
					getMonComptableInventaireInspecteurNib().getJCheckBoxProrata().isSelected() ? Integer.valueOf(1) : Integer.valueOf(0));

			currentInventaireComptable.cleInventaireComptable().setClicNumComplet((String) currentInventaireComptable.cleInventaireComptable().clicNumero());

			if (currentInventaireComptable.cleInventaireComptable().clicTypeAmort().equals(EOCleInventaireComptable.LINEAIRE)
					|| currentInventaireComptable.invcDateAcquisition() == null || currentInventaireComptable.cleInventaireComptable().clicDureeAmort() == null) {
				currentInventaireComptable.setDegressifCoefRelationship(null);
			} else {
				currentInventaireComptable.setDegressifCoefRelationship(FinderDegressifCoef.findLeCoefficient(app, currentInventaireComptable
						.cleInventaireComptable().clicDureeAmort(), currentInventaireComptable.invcDateAcquisition()));
			}

			try {
				ProcedureEnregistrerInventaireComptable.enregistrer((ApplicationCorossol) app, currentInventaireComptable);
				// Enlève les objets qui sont finalement créés en proc stock...
				EOGlobalID globalIDForClic = app.getAppEditingContext().globalIDForObject(currentInventaireComptable.cleInventaireComptable());
				if (globalIDForClic != null && !globalIDForClic.isTemporary()) {
					app.getAppEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(globalIDForClic));
				}

				EOGlobalID globalIDForInvc = app.getAppEditingContext().globalIDForObject(currentInventaireComptable);
				if (globalIDForInvc != null && !globalIDForInvc.isTemporary()) {
					app.getAppEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(globalIDForInvc));
				}

				app.getAppEditingContext().revert();
			} catch (Exception ex) {
				app.getAppEditingContext().revert();
				System.out.println(ex);
				EODialogs.runErrorDialog("ERREUR", ex.getMessage());
			}
		} catch (Throwable e) {
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME actionValider ! \n" + e.toString());
		}

		app.getAppEditingContext().revert();
		// juste pour mettre à zéro les changements dans Corossol
		app.getAppEditingContext().saveChanges();

		app.addLesPanelsModal(getMonComptableInventaireInspecteurNib());
		((ApplicationCorossol) app).retirerLesGlassPane();
		masquerFenetre();
		getParentControleur().assistantsTerminer(this);
	}

	public void actionNouvelleCle() {
	}

	public void actionRechercherCle() {
		getMonComptableInventaireRechercherNibCtrl().afficherFenetre();
	}

//	affichage
	private void afficher() {
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldDate().setText("");
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable().setText("");
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAcquisition().setText("");
		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCompteAmort().setText("");
		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCr().setText("");
		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailDuree().setText("");
		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailNbEtiquettes().setText("");
		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailNumero().setText("");
		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailUfr().setText("");
		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCompte().setText("");
		getMonComptableInventaireInspecteurNib().getJCheckBoxProrata().setEnabled(false);


		if (currentInventaireComptable == null) {
			return;
		}

		if (currentInventaireComptable.invcDateAcquisition() != null) {
			getMonComptableInventaireInspecteurNib().getjFormattedTextFieldDate().setText(
					DateCtrl.dateToString(currentInventaireComptable.invcDateAcquisition()));
		}

		if (currentInventaireComptable.invcMontantAcquisition() != null) {
			getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAcquisition().setValue(currentInventaireComptable.invcMontantAcquisitionAvecOrvs());
		}

		if (currentInventaireComptable.invcMontantAmortissable() == null && currentInventaireComptable.invcMontantAcquisition() != null) {
			currentInventaireComptable.setInvcMontantAmortissable(currentInventaireComptable.invcMontantAcquisition());
		}
		getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable().setValue(currentInventaireComptable.invcMontantAmortissableAvecOrvs());

		updateTotauxOrigines();
		
		if (currentInventaireComptable.cleInventaireComptable().planComptableAmort() != null) {
			getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCompteAmort().setText(
					currentInventaireComptable.cleInventaireComptable().planComptableAmort().pcoaNum());
		}

		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCr().setText(currentInventaireComptable.cleInventaireComptable().clicCr());

		if (DureeAmort != null) {
			getMonComptableInventaireInspecteurNib().getJTextFieldCocktailDuree().setText(DureeAmort);
		} else if (currentInventaireComptable.cleInventaireComptable().clicDureeAmort() != null) {
			getMonComptableInventaireInspecteurNib().getJTextFieldCocktailDuree().setText(
					currentInventaireComptable.cleInventaireComptable().clicDureeAmort().toString());
		}

		if (nbEtiq != null) {
			getMonComptableInventaireInspecteurNib().getJTextFieldCocktailNbEtiquettes().setText(nbEtiq);
		} else {
			getMonComptableInventaireInspecteurNib().getJTextFieldCocktailNbEtiquettes().setText(
					currentInventaireComptable.cleInventaireComptable().clicNbEtiquette().toString());
		}

		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailNumero().setText(currentInventaireComptable.cleInventaireComptable().clicNumero());

		getMonComptableInventaireInspecteurNib().getJTextFieldCocktailUfr().setText(currentInventaireComptable.cleInventaireComptable().clicComp());

		if (currentInventaireComptable.cleInventaireComptable().planComptable().pcoNum() != null) {
			getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCompte().setText(
					currentInventaireComptable.cleInventaireComptable().planComptable().pcoNum());
		}

		if (currentInventaireComptable.cleInventaireComptable().clicTypeAmort() != null) {
			if (currentInventaireComptable.cleInventaireComptable().clicTypeAmort().equals(EOCleInventaireComptable.DEGRESSIF)) {
				getMonComptableInventaireInspecteurNib().getJComboBoxTypeAmort().setSelectedItem(EOCleInventaireComptable.DEGRESSIF);
			} else {
				getMonComptableInventaireInspecteurNib().getJComboBoxTypeAmort().setSelectedItem(EOCleInventaireComptable.LINEAIRE);
			}
		}

		if (currentInventaireComptable.cleInventaireComptable().clicProRata() != null) {
			if (currentInventaireComptable.cleInventaireComptable().clicProRata().intValue() == 1) {
				getMonComptableInventaireInspecteurNib().getJCheckBoxProrata().setSelected(true);
			} else {
				getMonComptableInventaireInspecteurNib().getJCheckBoxProrata().setSelected(false);
			}
		}

		getMonComptableInventaireInspecteurNib().setOrigines(currentInventaireComptable.inventaireComptableOrigs());
		
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getjFormattedTextFieldDate(), isDateAmortissementModifiable());
		setEnabledComposant(getMonComptableInventaireInspecteurNib().getjFormattedTextFieldMontantAmortissable(), isMontantAmortissableModifiable());
	}
	
	/**
	 * Met à jour l'affichage des totaux des origines de financement
	 */
	private void updateTotauxOrigines() {
		// Total montants
		JFormattedTextField textFieldTotalMontants = getMonComptableInventaireInspecteurNib().getjFormattedTextFieldTotalMontants();
		JLabel labelTotalMontants = getMonComptableInventaireInspecteurNib().getjLabelTotalMontantsValue();

		JFormattedTextField textFieldTotalPourcentage = getMonComptableInventaireInspecteurNib().getjFormattedTextFieldTotalPourcentage();
		JLabel labelTotalPourcentage = getMonComptableInventaireInspecteurNib().getjLabelTotalPourcentagesValue();

		BigDecimal invcMontantAmortissableAvecOrvs = currentInventaireComptable.invcMontantAmortissableAvecOrvs();
		InventaireComptableHelper.corrigerOrigines(currentInventaireComptable);

		BigDecimal totalmontant = currentInventaireComptable.sommeMontantOriginesFinancement();
		textFieldTotalMontants.setText("");
		textFieldTotalMontants.setValue(currentInventaireComptable.sommeMontantOriginesFinancement());
		labelTotalMontants.setText(textFieldTotalMontants.getText());

		Color bgColor = null;
		if (invcMontantAmortissableAvecOrvs.compareTo(totalmontant) == 0) {
			bgColor = CocktailConstantes.BG_COLOR_GREEN;
		} else if (invcMontantAmortissableAvecOrvs.compareTo(totalmontant) > 0) {
			bgColor = CocktailConstantes.BG_COLOR_PINK;
		} else {
			bgColor = CocktailConstantes.BG_COLOR_RED;
		}
		textFieldTotalMontants.setBackground(bgColor);
		labelTotalMontants.setBackground(bgColor);

		// Total pourcentages
		BigDecimal totalPourcentage = currentInventaireComptable.sommePourcentageOriginesFinancement().divide(CalculMontantUtil.CENT);
		textFieldTotalPourcentage.setText("");
		textFieldTotalPourcentage.setValue(totalPourcentage);
		labelTotalPourcentage.setText(textFieldTotalPourcentage.getText());

		getMonComptableInventaireInspecteurNib().refresh();
	}

	void recupererCleInventaireComptable(EOCleInventaireComptable cle) {
		if (getCurrentAction().equals(CREATION)) {
			try {
				if (cle != null) {
					EOCleInventaireComptable clic = currentInventaireComptable.cleInventaireComptable();
						
					if (currentInventaireComptable.cleInventaireComptable() != null && currentCleInventaireComptable != null) {
						currentInventaireComptable.setCleInventaireComptableRelationship(null);
						if (currentCleInventaireComptable!=null && app.getAppEditingContext().globalIDForObject(currentCleInventaireComptable)!=null) {
							app.getAppEditingContext().deleteObject(currentCleInventaireComptable);
						}
					}

					currentInventaireComptable.setCleInventaireComptableRelationship(cle);
					afficherFenetreModification(currentInventaireComptable);
				}

			} catch (Throwable e) {
				e.printStackTrace();
				app.getAppEditingContext().revert();
				fenetreDeDialogueInformation("PROBLEME LORS DE LA SELECTION ! \n" + e.toString());
			}
		} else {
			fenetreDeDialogueInformation("ATTENTION ! \n Impossible de changer de numero d'inventaire ! \n "
					+ "Il faut le supprimer et le recontruire \n (Inventaire Comptable -> fleche gauche) ");
		}
	}

//	accesseurs
	private ComptableInventaireInspecteurNib getMonComptableInventaireInspecteurNib() {
		return monComptableInventaireInspecteurNib;
	}

	private void setMonComptableInventaireInspecteurNib(ComptableInventaireInspecteurNib monComptableInventaireInspecteurNib) {
		this.monComptableInventaireInspecteurNib = monComptableInventaireInspecteurNib;
	}

	private String getCurrentAction() {
		return currentAction;
	}

	private void setCurrentAction(String currentAction) {
		this.currentAction = currentAction;
	}

	EOInventaireComptable getCurrentInventaireComptable() {
		return currentInventaireComptable;
	}

	private NibCtrl getParentControleur() {
		return parentControleur;
	}

	public ComptableInventaireRechercherNib getMonComptableInventaireRechercherNib() {
		return monComptableInventaireRechercherNib;
	}

	private void setMonComptableInventaireRechercherNib(ComptableInventaireRechercherNib monComptableInventaireRechercherNib) {
		this.monComptableInventaireRechercherNib = monComptableInventaireRechercherNib;
	}

	private ComptableInventaireRechercherNibCtrl getMonComptableInventaireRechercherNibCtrl() {
		return monComptableInventaireRechercherNibCtrl;
	}

	private void setMonComptableInventaireRechercherNibCtrl(ComptableInventaireRechercherNibCtrl monComptableInventaireRechercherNibCtrl) {
		this.monComptableInventaireRechercherNibCtrl = monComptableInventaireRechercherNibCtrl;
	}

	//	listener inspecteur
	public  void swingFinderAnnuler(Object obj) {
	}

	public  void swingFinderTerminer(Object obj) {
		if (obj == originesCtrl) {
		    InventaireComptableHelper.corrigerOrigines(currentInventaireComptable);
			afficher();
		}

		if (obj == monSwingFinderPlanComptable) {
			if (FinderComptable.findLeCompteAmortissement(app, (EOPlanComptable) monSwingFinderPlanComptable.getResultat().objectAtIndex(0)) == null) {
				masquerFenetre();
				app.getAppEditingContext().revert();
				fenetreDeDialogueInformation("PROBLEME  ! \n Impossible de determiner le compte d'amortissement ! \n "
						+ "Verifier votre parametrage dans Maracuja pour le compte "
						+ ((EOPlanComptable) monSwingFinderPlanComptable.getResultat().objectAtIndex(0)).pcoNum() + " !");
				return;
			}

			currentCleInventaireComptable.setPlanComptableAmortRelationship(FinderComptable.findLeCompteAmortissement(app,
					(EOPlanComptable) monSwingFinderPlanComptable.getResultat().objectAtIndex(0)));
			currentCleInventaireComptable.setPlanComptableRelationship((EOPlanComptable) monSwingFinderPlanComptable.getResultat().objectAtIndex(0));
			getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCompte().setText(currentCleInventaireComptable.planComptable().pcoNum());
			getMonComptableInventaireInspecteurNib().getJTextFieldCocktailCompteAmort().setText(currentCleInventaireComptable.planComptableAmort().pcoaNum());
		}
	}
}
