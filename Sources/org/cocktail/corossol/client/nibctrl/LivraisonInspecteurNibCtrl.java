package org.cocktail.corossol.client.nibctrl;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.ToolsSwing;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOMagasin;
import org.cocktail.corossol.client.eof.process.ProcessLivraison;
import org.cocktail.corossol.client.nib.CalendarNib;
import org.cocktail.corossol.client.nib.DateCtrl;
import org.cocktail.corossol.client.nib.LivraisonInspecteurNib;
import org.cocktail.corossol.client.zutil.DateFieldListener;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSTimestamp;
public class LivraisonInspecteurNibCtrl extends NibCtrl {

	// methodes
	private static final String METHODE_ACTION_FERMER = "actionFermer";
	private static final String METHODE_ACTION_VALIDER = "actionValider";
	private static final String METHODE_ACTION_MAGASIN = "actionChoisirMagasin";
	private static final boolean AVEC_LOGS = true;
	private static final String FENETRE_DE_INSPECTEUR_DATE = "Choisir une date...";
	private static final String CREATION = "CREATION";
	private static final String MODIFICATION = "MODIFICATION";
	private static final String SUPPRESSION = "SUPPRESSION";

	private String currentAction = null;

	// le nib
	private LivraisonInspecteurNib monLivraisonInspecteurNib = null;

	// autre nib
	private CalendarNib monCalendarNib = null;
	private CalendarNibCtrl monCalendarNibCtrl = null;

	private ProcessLivraison monProcessLivraison =null;
	private EOLivraison currentLivraison = null;
	private EOMagasin selectedMagasin =null;
	private LivraisonNibCtrl parentCtrl =null;

	public LivraisonInspecteurNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(AVEC_LOGS);

		// init du ProcessLivraison 
		setMonProcessLivraison(new ProcessLivraison(AVEC_LOGS));

		// creation du nib l'inspecteur livraison
		setMonCalendarNibCtrl(new CalendarNibCtrl(ctrl, alocation_x, alocation_y, ataille_x, ataille_y));
		setMonCalendarNib( new CalendarNib());
		getMonCalendarNibCtrl().creationFenetre(getMonCalendarNib(),ToolsSwing.formaterStringU(FENETRE_DE_INSPECTEUR_DATE));
		getMonCalendarNib().setPreferredSize(new java.awt.Dimension(300, 200));
		getMonCalendarNib().setSize(300,200);
	}

	public void creationFenetre(LivraisonInspecteurNib leLivraisonInspecteurNib, String title) {
		super.creationFenetre(leLivraisonInspecteurNib, title);
		setMonLivraisonInspecteurNib(leLivraisonInspecteurNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void afficherFenetreCreation() {
		if (getParentCtrl() == null){
			fenetreDeDialogueInformation("PROBLEME ! \n PAS DE CONTROLEUR PARENTS");
			return;
		}
		super.afficherFenetre();

		setCurrentLivraison(null);
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonLivraisonInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();

		if (getCurrentLivraison() != null)
			afficherCurrentLivraison();
		setCurrentAction(CREATION);
	}

	public void afficherFenetreModification() {
		if (getParentCtrl() == null){
			fenetreDeDialogueInformation("PROBLEME ! \n PAS DE CONTROLEUR PARENTS");
			return;
		}

		if (getParentCtrl().getMesLivraisonsTBV().getSelectedObjects().count() == 0){
			fenetreDeDialogueInformation("Il faut choisir une livraison !");
			return;
		}

		super.afficherFenetre();
		setCurrentLivraison((EOLivraison)getParentCtrl().getMesLivraisonsTBV().getSelectedObjects().objectAtIndex(0));
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonLivraisonInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();

		if (getCurrentLivraison() != null)
			afficherCurrentLivraison();
		setCurrentAction(MODIFICATION);
	}

	public void afficherFenetreSuppression() {
		if (getParentCtrl() == null){
			fenetreDeDialogueInformation("PROBLEME ! \n PAS DE CONTROLEUR PARENTS");
			return;
		}
		if (getParentCtrl().getMesLivraisonsTBV().getSelectedObjects().count() == 0){
			fenetreDeDialogueInformation("Il faut choisir une livraison !");
			return;
		}
		super.afficherFenetre();

		setCurrentLivraison((EOLivraison)getParentCtrl().getMesLivraisonsTBV().getSelectedObjects().objectAtIndex(0));
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonLivraisonInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();

		if (getCurrentLivraison() != null)
			afficherCurrentLivraison();
		setCurrentAction(SUPPRESSION);
	}

	private void bindingAndCustomization() {
		try {
			formaterUnTexfieldNSTimestamp(getMonLivraisonInspecteurNib().getJFormattedTextFieldDate(),"dd/MM/yyyy");
			getMonLivraisonInspecteurNib().getJFormattedTextFieldDate().setValue(new NSTimestamp());
			getMonLivraisonInspecteurNib().getJFormattedTextFieldDate().addFocusListener(new DateFieldListener());
			// le cablage des  objets
			getMonLivraisonInspecteurNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_FERMER);
			getMonLivraisonInspecteurNib().getJButtonCocktailValider().addDelegateActionListener(this, METHODE_ACTION_VALIDER);
			getMonLivraisonInspecteurNib().getJButtonCocktailMagasin().addDelegateActionListener(this, METHODE_ACTION_MAGASIN);

			// les images des objets
			getMonLivraisonInspecteurNib().getJButtonCocktailAnnuler().setIcone(IconeCocktail.FERMER);//"close_view");
			getMonLivraisonInspecteurNib().getJButtonCocktailValider().setIcone(IconeCocktail.VALIDER);//"valider16");
			//getMonLivraisonInspecteurNib().getJButtonCocktailMagasin().setIcone("valider16");

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonLivraisonInspecteurNib());

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol) app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");
			((ApplicationCorossol) app).getObserveurCommandeSelection().addObserverForMessage(this, "changementCommande");

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}
	}

	// methodes cablage
	public void actionFermer() {
		trace(METHODE_ACTION_FERMER);
		app.addLesPanelsModal(getMonLivraisonInspecteurNib());
		((ApplicationCorossol) app).retirerLesGlassPane();
		getParentCtrl().inspecteurAnnuler(this);
		viderInspecteurLivraison();
		masquerFenetre();
	}

	private boolean checkDates() {
		// Date postérieure à la date du jour
		if (DateCtrl.isDateAfterNow(getMonLivraisonInspecteurNib().getJFormattedTextFieldDate(), 0))
			if (!EODialogs.runConfirmOperationDialog("Attention",
    				"La date de livraison n'est pas encore passée. Confirmez-vous ?",
    				"OUI", "NON"))
				return false;
			
		// Date antérieure à la date du jour
		if (DateCtrl.isDateBeforeNow(getMonLivraisonInspecteurNib().getJFormattedTextFieldDate(), -365))
			if (!EODialogs.runConfirmOperationDialog("Attention",
    				"La date de livraison date de plus d'un an. Confirmez-vous ?",
    				"OUI", "NON"))
				return false;
		return true;
	}
	
	public void actionValider (){
		if (getCurrentAction().equals(CREATION) || getCurrentAction().equals(MODIFICATION))
			if (!checkDates())
				return;
		
		app.addLesPanelsModal(getMonLivraisonInspecteurNib());
		((ApplicationCorossol) app).retirerLesGlassPane();

		try {
			if (getCurrentAction().equals(CREATION))
				getMonProcessLivraison().creerUneLivraison( app.getAppEditingContext(),
						app.getCurrentUtilisateur(),
						((ApplicationCorossol)app).getCurrentCommande().devise(),
						app.getCurrentExercice(),
						((ApplicationCorossol)app).getCurrentCommande(),
						selectedMagasin,
						getMonLivraisonInspecteurNib().getJTextFieldCocktailReception().getText(),
						getMonLivraisonInspecteurNib().getJTextFieldCocktailNumero().getText(),
						getMonLivraisonInspecteurNib().getJTextFieldCocktailRemarque().getText(),
						((ApplicationCorossol)app).stringToTimestamp(getMonLivraisonInspecteurNib().getJFormattedTextFieldDate().getText()));

			if (getCurrentAction().equals(MODIFICATION))
				getMonProcessLivraison().majUneLivraison( app.getAppEditingContext(),
						getCurrentLivraison(),
						app.getCurrentUtilisateur(),
						selectedMagasin,
						getMonLivraisonInspecteurNib().getJTextFieldCocktailReception().getText(),
						getMonLivraisonInspecteurNib().getJTextFieldCocktailNumero().getText(),
						getMonLivraisonInspecteurNib().getJTextFieldCocktailRemarque().getText(),
						((ApplicationCorossol)app).stringToTimestamp(getMonLivraisonInspecteurNib().getJFormattedTextFieldDate().getText()) );

			if (getCurrentAction().equals(SUPPRESSION))
				getMonProcessLivraison().supprimerUneLivraison(app.getAppEditingContext(), getCurrentLivraison() );

			app.getAppEditingContext().saveChanges();
			getParentCtrl().inspecteurFermer(this);
		} catch (Exception e) {
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
			getParentCtrl().inspecteurFermer(this);
		}
		viderInspecteurLivraison();
		masquerFenetre();
		getParentCtrl().changementCommande();
	}

	public void actionChoisirMagasin (){
		trace(METHODE_ACTION_MAGASIN);
	}

	//observeurs
	public void changementCommande() {
		System.out.println("changementCommande.......");
		getMonLivraisonInspecteurNib().getJTextFieldCocktailExercice().setText(((ApplicationCorossol)app).getCurrentCommande().exercice().exeExercice()+"");
		getMonLivraisonInspecteurNib().getJTextFieldCocktailCommande().setText(((ApplicationCorossol)app).getCurrentCommande().commandeLibelle());
	}

	public void changementExercice() {
	}

	// affichage

	private void preparerInterface(){
		getFrameMain().setTitle(getCurrentAction()+" : "+LivraisonNibCtrl.FENETRE_DE_CREATION_LIVRAISON);

		if (getCurrentAction().equals(CREATION)){
			setEnabledComposant(getMonLivraisonInspecteurNib().getJFormattedTextFieldDate(),true);
			setEnabledComposant(getMonLivraisonInspecteurNib().getJTextFieldCocktailNumero(),true);
			setEnabledComposant(getMonLivraisonInspecteurNib().getJTextFieldCocktailReception(),true);
			setEnabledComposant(getMonLivraisonInspecteurNib().getJTextFieldCocktailRemarque(),true);
			getMonLivraisonInspecteurNib().getJButtonCocktailValider().setText("Valider");
			getMonLivraisonInspecteurNib().getJButtonCocktailValider().setIcone("add_obj");
		}

		if (getCurrentAction().equals(MODIFICATION)){
			setEnabledComposant(getMonLivraisonInspecteurNib().getJFormattedTextFieldDate(),true);
			setEnabledComposant(getMonLivraisonInspecteurNib().getJTextFieldCocktailNumero(),true);
			setEnabledComposant(getMonLivraisonInspecteurNib().getJTextFieldCocktailReception(),true);
			setEnabledComposant(getMonLivraisonInspecteurNib().getJTextFieldCocktailRemarque(),true);
			getMonLivraisonInspecteurNib().getJButtonCocktailValider().setText("Modifier");
			getMonLivraisonInspecteurNib().getJButtonCocktailValider().setIcone("refresh");
		}

		if (getCurrentAction().equals(SUPPRESSION)){
			setEnabledComposant(getMonLivraisonInspecteurNib().getJFormattedTextFieldDate(),false);
			setEnabledComposant(getMonLivraisonInspecteurNib().getJTextFieldCocktailNumero(),false);
			setEnabledComposant(getMonLivraisonInspecteurNib().getJTextFieldCocktailReception(),false);
			setEnabledComposant(getMonLivraisonInspecteurNib().getJTextFieldCocktailRemarque(),false);
			getMonLivraisonInspecteurNib().getJButtonCocktailValider().setText("Supprimer");
			getMonLivraisonInspecteurNib().getJButtonCocktailValider().setIcone("delete_obj");
		}
	}

	private void afficherCurrentLivraison() {
		getMonLivraisonInspecteurNib().getJFormattedTextFieldDate().setValue(getCurrentLivraison().livDate());
		getMonLivraisonInspecteurNib().getJFormattedTextFieldDate().updateUI();
		getMonLivraisonInspecteurNib().getJTextFieldCocktailNumero().setText(getCurrentLivraison().livNumero()+"");
		getMonLivraisonInspecteurNib().getJTextFieldCocktailReception().setText(getCurrentLivraison().livReception());
		getMonLivraisonInspecteurNib().getJTextFieldCocktailRemarque().setText(getCurrentLivraison().livDivers());
	}

	private void viderInspecteurLivraison() {
		getMonLivraisonInspecteurNib().getJFormattedTextFieldDate().setValue(new NSTimestamp());
		getMonLivraisonInspecteurNib().getJTextFieldCocktailNumero().setText("");
		getMonLivraisonInspecteurNib().getJTextFieldCocktailReception().setText("");
		getMonLivraisonInspecteurNib().getJTextFieldCocktailRemarque().setText("");
	}

	// accesseurs

	private LivraisonInspecteurNib getMonLivraisonInspecteurNib() {
		return monLivraisonInspecteurNib;
	}

	private void setMonLivraisonInspecteurNib(LivraisonInspecteurNib monLivraisonInspecteurNib) {
		this.monLivraisonInspecteurNib = monLivraisonInspecteurNib;
	}

	private EOLivraison getCurrentLivraison() {
		return currentLivraison;
	}

	private void setCurrentLivraison(EOLivraison currentLivraison) {
		this.currentLivraison = currentLivraison;
	}

	private ProcessLivraison getMonProcessLivraison() {
		return monProcessLivraison;
	}

	private void setMonProcessLivraison(ProcessLivraison monProcessLivraison) {
		this.monProcessLivraison = monProcessLivraison;
	}

	private LivraisonNibCtrl getParentCtrl() {
		return parentCtrl;
	}

	public void setParentCtrl(LivraisonNibCtrl parentCtrl) {
		this.parentCtrl = parentCtrl;
	}

	private CalendarNib getMonCalendarNib() {
		return monCalendarNib;
	}

	private void setMonCalendarNib(CalendarNib monCalendarNib) {
		this.monCalendarNib = monCalendarNib;
	}

	private CalendarNibCtrl getMonCalendarNibCtrl() {
		return monCalendarNibCtrl;
	}

	private void setMonCalendarNibCtrl(CalendarNibCtrl monCalendarNibCtrl) {
		this.monCalendarNibCtrl = monCalendarNibCtrl;
	}

	private EOMagasin getSelectedMagasin() {
		return selectedMagasin;
	}

	private void setSelectedMagasin(EOMagasin selectedMagasin) {
		this.selectedMagasin = selectedMagasin;
	}

	private String getCurrentAction() {
		return currentAction;
	}

	private void setCurrentAction(String currentAction) {
		this.currentAction = currentAction;
		preparerInterface();
	}
}
