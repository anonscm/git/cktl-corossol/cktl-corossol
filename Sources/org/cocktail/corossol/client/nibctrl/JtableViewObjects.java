/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.corossol.client.nibctrl;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.cocktail.application.palette.JTableCocktailSorter;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.zutil.wo.table.MyJTable;

import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class JtableViewObjects extends JPanel {

	NSMutableArrayDisplayGroup data = null;
	NSMutableArray columns = null;
	MyJTable table = null;
	JTableCocktailModelData modelData = null;

	JTableCocktailSorters sorter = null;
	JScrollPane scrollPane = null;

	public JtableViewObjects(NSMutableArray columns, NSMutableArrayDisplayGroup lesDatas, Dimension scrollDimension, int jTableMode) {
		super(new GridLayout(1, 0));
		data = lesDatas;
		this.columns = columns;
		modelData = new JTableCocktailModelData();
		sorter = new JTableCocktailSorters(modelData, this);
		table = new MyJTable(sorter);
		initColumnSizes(table);
		table.setAutoResizeMode(jTableMode);
		sorter.setTableHeader(table.getTableHeader());
		table.setPreferredScrollableViewportSize(scrollDimension);
		// Set up tool tips for column headers.
		table.getTableHeader().setToolTipText("trier en cliquant");
		// Create the scroll pane and add the table to it.
		scrollPane = new JScrollPane(table);
		// Add the scroll pane to this panel.
		add(scrollPane);
	}

	public void initTableViewObjects(JtableViewObjects tvc) {
		removeAll();
		setLayout(new GridLayout(1, 0));
		data = tvc.data;
		columns = tvc.columns;
		table = tvc.table;
		modelData = tvc.modelData;
		scrollPane = tvc.scrollPane;
		sorter = tvc.sorter;
		add(tvc);
		validate();
		updateUI();
		refresh();
	}

	private void initColumnSizes(JTable table) {
		TableColumn column = null;
		for (int i = 0; i < table.getColumnCount(); i++) {
			column = table.getColumnModel().getColumn(i);
			column.setPreferredWidth(((ColumnData) columns.objectAtIndex(i)).getM_width());
		}
	}

	public JTable getTable() {
		return table;
	}

	public void refresh() {
		initalisationDesTris();
		this.updateUI();
		modelData.fireTableDataChanged();
	}

	public void initalisationDesTris() {
		for (int i = 0; i < getTable().getColumnCount(); i++) {
			sorter.setEmptyStatus(i);
		}
	}

	public NSMutableArrayDisplayGroup getSelectedObjects() {
		int lesSelected[] = null;
		NSMutableArrayDisplayGroup monNSMutableArrayDisplayGroup = new NSMutableArrayDisplayGroup();

		lesSelected = getTable().getSelectedRows();
		for (int i = 0; i < lesSelected.length; i++) {
			monNSMutableArrayDisplayGroup.addObject(data.objectAtIndex(lesSelected[i]));
		}
		return monNSMutableArrayDisplayGroup;
	}

	public void setSelectedObjects(NSMutableArrayDisplayGroup nouvelleSelection) {
		int tmpSelecection = 0;

		System.out.println("setSelectedObjects" + nouvelleSelection);
		for (int i = 0; i < nouvelleSelection.count(); i++) {
			tmpSelecection = data.indexOfObject(nouvelleSelection.objectAtIndex(i));
			getTable().setRowSelectionInterval(tmpSelecection, tmpSelecection);
			// System.out.println("tmpSelecection "+tmpSelecection);
		}
		this.refresh();
		super.repaint();
		getTable().repaint();
	}

	public NSMutableArrayDisplayGroup removeSelectedObjects() {
		int[] lesSelected = null;
		NSMutableArrayDisplayGroup monNSMutableArrayDisplayGroup = new NSMutableArrayDisplayGroup();
		lesSelected = getTable().getSelectedRows();
		for (int i = 0; i < lesSelected.length; i++) {
			monNSMutableArrayDisplayGroup.addObject(data.objectAtIndex(lesSelected[i]));
		}
		return monNSMutableArrayDisplayGroup;
	}

	// selection
	public void addDelegateSelectionListener(Object delegate, String methode) {
		getTable().getSelectionModel().addListSelectionListener(new TableViewCocktailSelectionListener(this, delegate, methode));
	}

	// clavier
	public void addDelegateKeyListener(Object delegate, String methode) {
		getTable().addKeyListener(new TableViewCocktailKeyAdapter(delegate, methode));
	}

	class TableViewCocktailKeyAdapter extends KeyAdapter {
		private Object delegate = null;
		private String methode = null;

		public TableViewCocktailKeyAdapter(Object delegate, String methode) {
			this.delegate = delegate;
			this.methode = methode;
		}

		public void keyPressed(KeyEvent evt) {
			super.keyPressed(evt);
		}

		public void keyReleased(KeyEvent arg0) {
			super.keyReleased(arg0);
			// System.out.println("keyReleased");
			callMethode();
		}

		public void keyTyped(KeyEvent arg0) {
			super.keyTyped(arg0);
		}

		private void callMethode() {
			// try du call
			try {
				Class c;
				c = delegate.getClass();
				// Recupereation de la methode getXxxxx :
				Method call = c.getMethod(methode, (Class[]) null);
				// appel de la methode
				call.invoke(delegate, (Object[]) null);
			} catch (Exception ex) {
				// System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
			}
		}

		private void callMethode(String param) {
			// try du call
			try {
				Class c;
				c = delegate.getClass();
				// Recupereation de la methode getXxxxx :
				Class[] parametres = {param.getClass()};
				Method call = c.getMethod(methode, parametres);
				call.invoke(delegate, new Object[] {param});
			} catch (Exception ex) {
				// System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
			}
		}
	}

	class TableViewCocktailSelectionListener implements ListSelectionListener {
		private JtableViewObjects maTableViewCocktail;
		private Object delegate = null;
		private String methode = null;

		public TableViewCocktailSelectionListener(JtableViewObjects maTableViewCocktail, Object delegate, String methode) {
			super();
			this.delegate = delegate;
			this.methode = methode;
			this.maTableViewCocktail = maTableViewCocktail;
		}

		public void valueChanged(ListSelectionEvent e) {
			// If cell selection is enabled, both row and column change events are fired
			/*
	 		if (e.getSource() == maTableViewCocktail.getSelectionModel()
					&& maTableViewCocktail.getRowSelectionAllowed() ) {
				// Column selection changed
				int first = e.getFirstIndex();
				int last = e.getLastIndex();
				System.out.println(" Column selection changed");
				callMethode();

			} 

			if (e.getSource() == maTableViewCocktail.getColumnModel().getSelectionModel()
					&& maTableViewCocktail.getColumnSelectionAllowed() ){
				// Row selection changed
				int first = e.getFirstIndex();
				int last = e.getLastIndex();
				System.out.println("Row selection changed");
				callMethode();
			}
			 */

			if (e.getValueIsAdjusting()) {
				// The mouse button has not yet been released
				//System.out.println("The mouse button has not yet been released");
				callMethode();
			}
		}

		private void callMethode() {
			// try du call
			try {
				Class c;
				c = delegate.getClass();
				// Recupereation de la methode getXxxxx :
				Method call = c.getMethod(methode, (Class[]) null);
				// appel de la methode
				call.invoke(delegate, (Object[]) null);
			} catch (Exception ex) {
				// System.out.println(ex+"objet "+delegate+" methode "+methode+" inexistante");
			}
		}
	}

	class JTableCocktailModelData extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		public int getColumnCount() {
			return columns.count();
		}

		public int getRowCount() {
			return data.count();
		}

		public String getColumnName(int column) {
			return ((ColumnData) columns.objectAtIndex(column)).getM_title();

		}

		public String getColumnKey(int column) {
			return ((ColumnData) columns.objectAtIndex(column)).getM_key();
		}

		public Object getValueAt(int nRow, int nCol) {
			ColumnData tmp_Column = null;
			final Object row = data.objectAtIndex(nRow);

			if (nRow < 0 || nRow >= getRowCount()) {
				return "";
			}

			tmp_Column = (ColumnData) columns.objectAtIndex(nCol);

			// try du keyName()
			try {
				Class c;
				c = row.getClass();
				// Recupereation de la methode getXxxxx :
				final Method get = c.getMethod(tmp_Column.getM_key(), (Class[]) null);
				// appel de l accesseur
				return get.invoke(row, (Object[]) null);
			} catch (final Exception e) {

			}
			// try du getKeyName()
			try {
				Class c;
				c = row.getClass();
				// Recupereation de la methode getXxxxx :
				final Method get = c.getMethod("get" + getCapitalizedString(tmp_Column.getM_key()), (Class[]) null);
				// appel de l accesseur
				return get.invoke(row, (Object[]) null);
			} catch (final Exception e) {

			}
			return "...";
		}

		private  String getCapitalizedString( final String str ) {
			// Print a copy of str to standard output, with the
			// first letter of each word in upper case.
			char ch;       // One of the characters in str.
			char prevCh;   // The character that comes before ch in the string.
			int i;         // A position in str, from 0 to str.length()-1.
			prevCh = '.';  // Prime the loop with any non-letter character.
			String result = "";
			for (i = 0; i < str.length(); i++) {
				ch = str.charAt(i);
				if (Character.isLetter(ch) && !Character.isLetter(prevCh)) {
					// System.out.print( Character.toUpperCase(ch) );
					result = "" + Character.toUpperCase(ch);
				} else {
					// System.out.print( ch );
					result = result + ch;
				}
				prevCh = ch;
			}
			return result;
		}

		public NSArray getLesDatas() {
			return data;
		}

		public void setLesDatas(NSMutableArrayDisplayGroup lesDatas) {
			data = lesDatas;
		}

		/*
		 * JTable uses this method to determine the default renderer/
		 * editor for each cell.  If we didn't implement this method,
		 * then the last column would contain text ("true"/"false"),
		 * rather than a check box.
		 */
		public Class getColumnClass(int c) {
			if (getValueAt(0, c) == null) {
				return " ".getClass();
			} else {
				return getValueAt(0, c).getClass();
			}
		}

		/*
		 * Don't need to implement this method unless your table's
		 * editable.
		 */
		public boolean isCellEditable(int row, int col) {
			//Note that the data/cell address is constant,
			//no matter where the cell appears onscreen.
			//            if (col < 2) {
			//                return false;
			//            } else {
			//                return true;
			//            }
			return false;
		}

		/*
		 * Don't need to implement this method unless your table's
		 * data can change.
		 */
		public void setValueAt(Object value, int row, int col) {
			//            if (DEBUG) {
			//                System.out.println("Setting value at " + row + "," + col
			//                                   + " to " + value
			//                                   + " (an instance of "
			//                                   + value.getClass() + ")");
			//            }
			//
			//            data[row][col] = value;
			//            fireTableCellUpdated(row, col);
			//
			//            if (DEBUG) {
			//                System.out.println("New value of data:");
			//                printDebugData();
			//            }
		}
	}

	public class JTableCocktailSorters extends AbstractTableModel {
		protected TableModel tableModel;
		final EOClientResourceBundle leBundle = new EOClientResourceBundle();
		public static final int DESCENDING = -1;
		public static final int NOT_SORTED = 0;
		public static final int ASCENDING = 1;

		private Directive EMPTY_DIRECTIVE = new Directive(-1, NOT_SORTED);

		public Comparator COMPARABLE_COMAPRATOR = new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) o1).compareTo(o2);
			}
		};
		
		public Comparator LEXICAL_COMPARATOR = new Comparator() {
			public int compare(Object o1, Object o2) {
				return o1.toString().compareTo(o2.toString());
			}
		};

		private Row[] viewToModel;
		private int[] modelToView;

		private JTableHeader tableHeader;
		private MouseListener mouseListener;
		private TableModelListener tableModelListener;
		private Map columnComparators = new HashMap();
		private List sortingColumns = new ArrayList();
		private JtableViewObjects tbv;

		public JTableCocktailSorters() {
			this.mouseListener = new MouseHandler();
			this.tableModelListener = new TableModelHandler();
		}

		public JTableCocktailSorters(TableModel tableModel, JtableViewObjects tbv) {
			this();
			setTableModel(tableModel);
			setTbv(tbv);
		}

		public JTableCocktailSorters(TableModel tableModel, JTableHeader tableHeader) {
			this();
			setTableHeader(tableHeader);
			setTableModel(tableModel);
		}

		public void setTbv(JtableViewObjects tbv) {
			this.tbv = tbv;
		}

		private void clearSortingState() {
			viewToModel = null;
			modelToView = null;
		}

		public TableModel getTableModel() {
			return tableModel;
		}

		public void setTableModel(TableModel tableModel) {
			if (this.tableModel != null) {
				this.tableModel.removeTableModelListener(tableModelListener);
			}

			this.tableModel = tableModel;
			if (this.tableModel != null) {
				this.tableModel.addTableModelListener(tableModelListener);
			}

			clearSortingState();
			fireTableStructureChanged();
		}

		public JTableHeader getTableHeader() {
			return tableHeader;
		}

		public void setTableHeader(JTableHeader tableHeader) {
			if (this.tableHeader != null) {
				this.tableHeader.removeMouseListener(mouseListener);
				TableCellRenderer defaultRenderer = this.tableHeader.getDefaultRenderer();
				if (defaultRenderer instanceof SortableHeaderRenderer) {
					this.tableHeader.setDefaultRenderer(((SortableHeaderRenderer) defaultRenderer).tableCellRenderer);
				}
			}
			this.tableHeader = tableHeader;
			if (this.tableHeader != null) {
				this.tableHeader.addMouseListener(mouseListener);
				this.tableHeader.setDefaultRenderer(new SortableHeaderRenderer(this.tableHeader.getDefaultRenderer()));
			}
		}

		public boolean isSorting() {
			return sortingColumns.size() != 0;
		}

		private Directive getDirective(int column) {
			for (int i = 0; i < sortingColumns.size(); i++) {
				Directive directive = (Directive) sortingColumns.get(i);
				if (directive.column == column) {
					return directive;
				}
			}
			return EMPTY_DIRECTIVE;
		}

		public int getSortingStatus(int column) {
			return getDirective(column).direction;
		}

		private void sortingStatusChanged() {
			clearSortingState();
			fireTableDataChanged();
			if (tableHeader != null) {
				tableHeader.repaint();
			}
		}

		public void setSortingStatus(int column, int status) {
			Directive directive = getDirective(column);
			if (directive != EMPTY_DIRECTIVE) {
				sortingColumns.remove(directive);
			}
			if (status != NOT_SORTED) {
				sortingColumns.add(new Directive(column, status));
			}
			sortingStatusChanged();
		}

		public void setEmptyStatus(int column) {// , int status) {
			Directive directive = getDirective(column);
			if (directive != EMPTY_DIRECTIVE) {
				sortingColumns.remove(directive);
			}
			// if (status != NOT_SORTED) {
			// sortingColumns.add(new Directive(column, status));
			// }
			sortingStatusChanged();
		}

		protected Icon getHeaderRendererIcon(int column, int size) {
			Directive directive = getDirective(column);
			if (directive == EMPTY_DIRECTIVE) {
				return (ImageIcon) leBundle.getObject("alpha_AA");
			}

			if (directive.direction == NOT_SORTED) {
				return (ImageIcon) leBundle.getObject("alpha_AA");
			}

			if (directive.direction == DESCENDING) {
				return (ImageIcon) leBundle.getObject("alphab_ZA");
			} else {
				return (ImageIcon) leBundle.getObject("alphab_AZ");
			}
		}

		private void cancelSorting() {
			sortingColumns.clear();
			sortingStatusChanged();
		}

		public void setColumnComparator(Class type, Comparator comparator) {
			if (comparator == null) {
				columnComparators.remove(type);
			} else {
				columnComparators.put(type, comparator);
			}
		}

		protected Comparator getComparator(int column) {
			Class columnType = tableModel.getColumnClass(column);
			Comparator comparator = (Comparator) columnComparators.get(columnType);
			if (comparator != null) {
				return comparator;
			}
			if (Comparable.class.isAssignableFrom(columnType)) {
				return COMPARABLE_COMAPRATOR;
			}
			return LEXICAL_COMPARATOR;
		}

		private Row[] getViewToModel() {
			if (viewToModel == null) {
				int tableModelRowCount = tableModel.getRowCount();
				viewToModel = new Row[tableModelRowCount];
				for (int row = 0; row < tableModelRowCount; row++) {
					viewToModel[row] = new Row(row);
				}

				if (isSorting()) {
					Arrays.sort(viewToModel);
				}
			}
			return viewToModel;
		}

		public int modelIndex(int viewIndex) {
			return getViewToModel()[viewIndex].modelIndex;
		}

		private int[] getModelToView() {
			if (modelToView == null) {
				int n = getViewToModel().length;
				modelToView = new int[n];
				for (int i = 0; i < n; i++) {
					modelToView[modelIndex(i)] = i;
				}
			}
			return modelToView;
		}

		// TableModel interface methods

		public int getRowCount() {
			if (tableModel != null) {
				return tableModel.getRowCount();
			}
			return 0;
		}

		public int getColumnCount() {
			if (tableModel != null) {
				return tableModel.getColumnCount();
			}
			return 0;
		}

		public String getColumnName(int column) {
			return tableModel.getColumnName(column);
		}

		public Class getColumnClass(int column) {
			return tableModel.getColumnClass(column);
		}

		public boolean isCellEditable(int row, int column) {
			return tableModel.isCellEditable(modelIndex(row), column);
		}

		public Object getValueAt(int row, int column) {
			return tableModel.getValueAt(modelIndex(row), column);
		}

		public void setValueAt(Object aValue, int row, int column) {
			tableModel.setValueAt(aValue, modelIndex(row), column);
		}

		// Helper classes

		private class Row implements Comparable {
			private int modelIndex;

			public Row(int index) {
				this.modelIndex = index;
			}

			public int compareTo(Object o) {
				int row1 = modelIndex;
				int row2 = ((Row) o).modelIndex;

				for (Iterator it = sortingColumns.iterator(); it.hasNext();) {
					Directive directive = (Directive) it.next();
					int column = directive.column;
					Object o1 = tableModel.getValueAt(row1, column);
					Object o2 = tableModel.getValueAt(row2, column);

					int comparison = 0;
					// Define null less than everything, except null.
					if (o1 == null && o2 == null) {
						comparison = 0;
					} else if (o1 == null) {
						comparison = -1;
					} else if (o2 == null) {
						comparison = 1;
					} else {
						comparison = getComparator(column).compare(o1, o2);
					}
					if (comparison != 0) {
						if (directive.direction == DESCENDING) {
							return -comparison;
						} else {
							return comparison;
						}
					}
				}
				return 0;
			}
		}

		private class TableModelHandler implements TableModelListener {
			public void tableChanged(TableModelEvent e) {
				// If we're not sorting by anything, just pass the event along.
				if (!isSorting()) {
					clearSortingState();
					fireTableChanged(e);
					return;
				}

				// If the table structure has changed, cancel the sorting; the
				// sorting columns may have been either moved or deleted from
				// the model.
				if (e.getFirstRow() == TableModelEvent.HEADER_ROW) {
					cancelSorting();
					fireTableChanged(e);
					return;
				}

				// We can map a cell event through to the view without widening             
				// when the following conditions apply: 
				// 
				// a) all the changes are on one row (e.getFirstRow() == e.getLastRow()) and, 
				// b) all the changes are in one column (column != TableModelEvent.ALL_COLUMNS) and,
				// c) we are not sorting on that column (getSortingStatus(column) == NOT_SORTED) and, 
				// d) a reverse lookup will not trigger a sort (modelToView != null)
				//
				// Note: INSERT and DELETE events fail this test as they have column == ALL_COLUMNS.
				// 
				// The last check, for (modelToView != null) is to see if modelToView 
				// is already allocated. If we don't do this check; sorting can become 
				// a performance bottleneck for applications where cells  
				// change rapidly in different parts of the table. If cells 
				// change alternately in the sorting column and then outside of             
				// it this class can end up re-sorting on alternate cell updates - 
				// which can be a performance problem for large tables. The last 
				// clause avoids this problem. 
				int column = e.getColumn();
				if (e.getFirstRow() == e.getLastRow() && column != TableModelEvent.ALL_COLUMNS && getSortingStatus(column) == NOT_SORTED && modelToView != null) {
					int viewIndex = getModelToView()[e.getFirstRow()];
					fireTableChanged(new TableModelEvent(JTableCocktailSorters.this, viewIndex, viewIndex, column, e.getType()));
					return;
				}

				// Something has happened to the data that may have invalidated the row order.
				clearSortingState();
				fireTableDataChanged();
				return;
			}
		}

		private class MouseHandler extends MouseAdapter {
			public void mouseClicked(MouseEvent e) {
				JTableHeader h = (JTableHeader) e.getSource();
				TableColumnModel columnModel = h.getColumnModel();
				int viewColumn = columnModel.getColumnIndexAtX(e.getX());
				int column = columnModel.getColumn(viewColumn).getModelIndex();
				if (column != -1) {
					int status = getSortingStatus(column);
					if (!e.isControlDown()) {
						cancelSorting();
					}
					// Cycle the sorting states through {NOT_SORTED, ASCENDING, DESCENDING} or
					// {NOT_SORTED, DESCENDING, ASCENDING} depending on whether shift is pressed.
					// status = status + (e.isShiftDown() ? -1 : 1);
					// status = (status + 4) % 3 - 1; // signed mod, returning {-1, 0, 1}
					// setSortingStatus(column, status);
					switch (status) {
					case JTableCocktailSorter.NOT_SORTED:
						setSortingStatus(column, JTableCocktailSorter.ASCENDING);
						tbv.data.sortAscending(tbv.modelData.getColumnKey(column));
						break;
					case JTableCocktailSorter.ASCENDING:
						setSortingStatus(column, JTableCocktailSorter.DESCENDING);
						tbv.data.sortDescending(tbv.modelData.getColumnKey(column));
						break;
					case JTableCocktailSorter.DESCENDING:
						setSortingStatus(column, JTableCocktailSorter.ASCENDING);
						tbv.data.sortAscending(tbv.modelData.getColumnKey(column));
						break;

					default:
						break;
					}

				}
			}
		}

		private class SortableHeaderRenderer implements TableCellRenderer {
			private TableCellRenderer tableCellRenderer;

			public SortableHeaderRenderer(TableCellRenderer tableCellRenderer) {
				this.tableCellRenderer = tableCellRenderer;
			}

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				Component c = tableCellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (c instanceof JLabel) {
					JLabel l = (JLabel) c;
					l.setHorizontalTextPosition(JLabel.LEFT);
					int modelColumn = table.convertColumnIndexToModel(column);
					l.setIcon(getHeaderRendererIcon(modelColumn, l.getFont().getSize()));
				}
				return c;
			}
		}

		class Directive {
			private int column;
			private int direction;

			public Directive(int column, int direction) {
				this.column = column;
				this.direction = direction;
			}
		}
	}
}