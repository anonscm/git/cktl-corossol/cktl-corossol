package org.cocktail.corossol.client.nibctrl;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.MyToolsCocktailEOF;
import org.cocktail.corossol.client.UtilCtrl;
import org.cocktail.corossol.client.nib.ActifNib;
import org.cocktail.corossol.client.nib.StringCtrl;
import org.cocktail.corossol.client.zutil.StringUtils;
import org.cocktail.corossol.common.EditionsSql;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableDictionary;


public class ActifNibCtrl extends NibCtrl {

	static final String METHODE_CHANGEMENT_EDITION="changementEdition";
	
	private static final String PROJECTION_SEPARATOR = ",";
	
	// le nib
	private ActifNib monActifNib = null;

	public ActifNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(false);
	}

	public void creationFenetre(ActifNib leActifNib, String title) {
		super.creationFenetre(leActifNib, title);
		setMonActifNib(leActifNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	private void bindingAndCustomization() {
		try {
			getMonActifNib().initTableView();
			getMonActifNib().getEditionsTBV().addDelegateSelectionListener(this, METHODE_CHANGEMENT_EDITION);
			getMonActifNib().setEditionsDG(setActifDG());

			// le cablage des  objets
			getMonActifNib().getJButtonCocktailFermer().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionFermer(); } });
			getMonActifNib().getJButtonCocktailPdf().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionImprimer(); } });
			getMonActifNib().getJButtonCocktailXls().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionExporter(); } });

			// les images des objets
			getMonActifNib().getJButtonCocktailFermer().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.FERMER));
			getMonActifNib().getJButtonCocktailPdf().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.PDF));
			getMonActifNib().getJButtonCocktailXls().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.XLS));

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonActifNib());

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol) app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");
		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}
	}

	private NSMutableArrayDisplayGroup setActifDG() {
		NSMutableArrayDisplayGroup editionsDG = new NSMutableArrayDisplayGroup();

		ApplicationCorossol app = (ApplicationCorossol) EOApplication.sharedApplication();
		MyToolsCocktailEOF tools = new MyToolsCocktailEOF(app);
		
		String xlsEtatActifDetailUbSql = tools.loadSql(EditionsSql.XLS_ETAT_ACTIF_DETAIL_UB.toString());
		String[] xlsEtatActifDetailUbProjections = new String[] {
				"EXE_ORDRE", "INVC_ID", "NUMERO_INVENTAIRE", "NUMERO_COMMANDE", "EXERCICE_DEBUT", "ORIGINE_FINANCEMENT", "FOURNISSEUR",
				"IMPUTATION_DEPENSE", "IMPUTATION_DEP_LIBELLE", "UB", "CR", "COMPTE_AMORTISSEMENT", "ACQUISITION", "AMORTISSABLE", 
				"ANNUITE", "CUMUL", "RESIDUEL", "EXERCICE_FIN"};
		
		String xlsListeBiensPourEtatActifDetailUbSql = tools.loadSql(EditionsSql.XLS_LISTE_BIENS_ETAT_ACTIF_DETAIL_UB.toString());
		String[] xlsListeBiensPourEtatActifDetailUbProjections = new String[] {
				"EXE_ORDRE", "INVC_ID,NUMERO_INVENTAIRE", "NUMERO_COMMANDE", "EXERCICE_DEBUT", "ORIGINE_FINANCEMENT", "FOURNISSEUR", "IMPUTATION_DEPENSE",
				"IMPUTATION_DEP_LIBELLE", "UB", "CR" , "COMPTE_AMORTISSEMENT", "ACQUISITION", "AMORTISSABLE", "ANNUITE", "CUMUL", "RESIDUEL", "EXERCICE_FIN", "BIEN",
				"SALLE_BATIMENT", "SALLE_ETAGE", "SALLE_PORTE",	"NUMERO_SERIE", "UTILISATEUR_DU_BIEN"};
		
		String xlsListeNumerosInventaireSql = tools.loadSql(EditionsSql.XLS_LISTE_NUMEROS_INVENTAIRE.toString());
		String[] xlsListeNumerosInventaireProjections = new String[] {
				"MONTANT_ACQUISITION", "MONTANT_AMORTISSABLE", "MONTANT_RESIDUEL", "UB", "CR", "SOUS_CR", "LBUD_LIBELLE", "EXERCICE", "CDE_NUMERO", "CDE_REFERENCE", "CDE_LIBELLE",
				"IMPUTATION", "IMP_LIBELLE", "TYPE_CREDIT", "TCD_LIBELLE", "MANDAT_NUMERO", "BORD_NUMERO", "BORD_GESTION", "CODE_FOUR",
				"FOUR_CIVILITE", "FOUR_NOM", "NUMERO_INVENTAIRE", "FINANCEMENT"};
		
		editionsDG.addObject(new DocumentEditions("Etat Actif Composante", "Etat de l'actif au niveau composante", false, true, "EtatActif.jasper", "", "", ""));

		editionsDG.addObject(new DocumentEditions("Etat Actif Etablissement", "Etat de l'actif au niveau etablissement",false,true,"EtatActifEtab.jasper","","",""));

		editionsDG.addObject(new DocumentEditions("Etat Actif détaillé Etablissement","Etat de l'actif détaillé au niveau etablissement",false,true,"Etat_actif_detail.jasper","","",""));
		editionsDG.addObject(new DocumentEditions("Etat Actif détaillé UB","Etat de l'actif détaillé au niveau UB",false,true,"Etat_actif_detail_UB.jasper","","",""));

		editionsDG.addObject(new DocumentEditions("Etat de l'actif détaillé au niveau UB", "Etat de l'actif détaillé au niveau UB",true,false, "", 
				xlsEtatActifDetailUbSql, 
				StringUtils.join(PROJECTION_SEPARATOR, xlsEtatActifDetailUbProjections), ""));

		editionsDG.addObject(new DocumentEditions("Liste des biens de l'etat de l'actif détaillé au niveau UB", "Liste des biens de l'etat de l'actif détaillé au niveau UB",true,false,"",
				xlsListeBiensPourEtatActifDetailUbSql,
				StringUtils.join(PROJECTION_SEPARATOR, xlsListeBiensPourEtatActifDetailUbProjections), ""));

		StringBuilder sb = new StringBuilder();
		editionsDG.addObject(new DocumentEditions("Les tableaux d'amortissement jusqu'a l'exercice en cours ", "Les tableaux d'amortissement jusqu'a l'exercice en cours", true, false, "",
				sb.append("select EXERCICE, ")
				  .append("       GES_CODE, ")
				  .append("       NUMERO, ")
				  .append("       EXER, ")
				  .append("       replace(AMO_ANNUITE,'.',',') AMO_ANNUITE, ")
				  .append("       replace(AMO_CUMUL,'.',',') AMO_CUMUL, ")
				  .append("       replace(AMO_RESIDUEL,'.',',') AMO_RESIDUEL, ")
				  .append("       replace(AMO_ACQUISITION,'.',',') AMO_AMORTISSABLE, ")
				  .append("       replace(INVC_ACQUISITION,'.',',') INVC_ACQUISITION, ")
				  .append("       PCO_NUM ")
				  .append("  from jefy_inventaire.v_actif_inventaire ")
				  .append(" where exer = $P{EXERCICE} and exercice <= $P{EXERCICE} ")
				  .toString(),
				"EXERCICE,COMP,NUMERO,EXER,ANNUITE,CUMUL,RESIDUEL,AMORTISSABLE,ACQUISITION,IMPUTATION",""));

		editionsDG.addObject(new DocumentEditions("Amortissements par origine de financement","Amortissements par origine de financement",true,false,"",
				"select orgf.orgf_libelle ORIGINE_FINANCEMENT, clic.pco_num IMPUTATION, replace(to_char(sum(amo.amo_annuite*icor_pourcentage/100)),'.',',') ANNUITE " +
				"from jefy_inventaire.amortissement amo, jefy_inventaire.inventaire_comptable invc, jefy_inventaire.inventaire_comptable_orig invco, "+
				"   jefy_inventaire.origine_financement orgf, jefy_inventaire.cle_inventaire_comptable clic "+
				"where amo.exe_ordre=$P{EXERCICE} "+
				"   and amo.invc_id=invc.invc_id and invco.orgf_id=orgf.orgf_id and invc.clic_id=clic.clic_id and invc.invc_id=invco.invc_id "+
				"group by orgf.orgf_libelle, clic.pco_num order by orgf.orgf_libelle, clic.pco_num ",
				"ORIGINE_FINANCEMENT,IMPUTATION,ANNUITE",""));

		editionsDG.addObject(new DocumentEditions("Amortissements par origine de financement et UB",
				"Amortissements par origine de financement et UB",true,false,"",
				"select orgf.orgf_libelle ORIGINE_FINANCEMENT, organ.org_ub UB, clic.pco_num IMPUTATION, replace(to_char(sum(amo.amo_annuite*icor_pourcentage/100)),'.',',') ANNUITE "+
				"from jefy_inventaire.amortissement amo, jefy_inventaire.inventaire_comptable invc, jefy_inventaire.inventaire_comptable_orig invco, "+ 
				"jefy_inventaire.origine_financement orgf, jefy_inventaire.cle_inventaire_comptable clic, "+
				"(select ic.invc_id, o.org_ub "+ 
				"   from jefy_admin.organ o, jefy_depense.engage_budget e, jefy_depense.depense_budget d, jefy_depense.depense_ctrl_planco p, "+
				"        jefy_inventaire.inventaire_comptable ic "+
				"   where o.org_id=e.org_id and e.eng_id=d.eng_id and d.dep_id=p.dep_id and p.dpco_id=ic.dpco_id and ic.dpco_id<>-1000 "+
				" union all "+
				" select inb.invc_id, o.org_ub "+
				"   from jefy_admin.organ o, jefy_inventaire.inventaire_non_budgetaire inb "+
				"   where inb.invc_id is not null and inb.org_id=o.org_id(+) ) organ "+
				"where amo.exe_ordre=$P{EXERCICE} and amo.invc_id=invc.invc_id and invco.orgf_id=orgf.orgf_id and invc.clic_id=clic.clic_id "+
				"  and organ.invc_id=invc.invc_id and invc.invc_id=invco.invc_id "+ 
				"group by orgf.orgf_libelle, org_ub, clic.pco_num "+
				"order by orgf.orgf_libelle, org_ub, clic.pco_num ",
				"ORIGINE_FINANCEMENT,UB,IMPUTATION,ANNUITE",""));

		editionsDG.addObject(new DocumentEditions("Amortissements par origine de financement et ligne budgetaire",
				"Amortissements par origine de financement et ligne budgetaire",true,false,"",
				"select orgf.orgf_libelle ORIGINE_FINANCEMENT, org_ub UB, org_cr CR, org_souscr SOUS_CR, clic.pco_num IMPUTATION, " +
				"  replace(to_char(sum(amo.amo_annuite*icor_pourcentage/100)),'.',',') ANNUITE "+
				"from jefy_inventaire.amortissement amo, jefy_inventaire.inventaire_comptable invc, jefy_inventaire.inventaire_comptable_orig invco, "+ 
				"jefy_inventaire.origine_financement orgf, jefy_inventaire.cle_inventaire_comptable clic, "+
				"(select ic.invc_id, o.org_ub, o.org_cr, decode(o.org_souscr,null,' ',o.org_souscr) org_souscr "+ 
				"   from jefy_admin.organ o, jefy_depense.engage_budget e, jefy_depense.depense_budget d, jefy_depense.depense_ctrl_planco p, "+
				"        jefy_inventaire.inventaire_comptable ic "+
				"   where o.org_id=e.org_id and e.eng_id=d.eng_id and d.dep_id=p.dep_id and p.dpco_id=ic.dpco_id and ic.dpco_id<>-1000 "+
				" union all "+
				" select inb.invc_id, o.org_ub, o.org_cr, decode(o.org_souscr,null,' ',o.org_souscr) "+
				"   from jefy_admin.organ o, jefy_inventaire.inventaire_non_budgetaire inb "+
				"   where inb.invc_id is not null and inb.org_id=o.org_id(+) ) organ "+
				"where amo.exe_ordre=$P{EXERCICE} and amo.invc_id=invc.invc_id and invco.orgf_id=orgf.orgf_id and invc.clic_id=clic.clic_id "+
				"  and organ.invc_id=invc.invc_id and invc.invc_id=invco.invc_id "+ 
				"group by orgf.orgf_libelle, org_ub, org_cr, org_souscr, clic.pco_num "+
				"order by orgf.orgf_libelle, org_ub, org_cr, org_souscr, clic.pco_num ",
				"ORIGINE_FINANCEMENT,UB,CR,SOUS_CR,IMPUTATION,ANNUITE",""));

		editionsDG.addObject(new DocumentEditions("Liste des numeros d'inventaire de l'exercice ","Liste des numeros d'inventaire de l'exercice",true,true,"ListeInventaire.jasper",
				xlsListeNumerosInventaireSql,
				StringUtils.join(PROJECTION_SEPARATOR, xlsListeNumerosInventaireProjections), ""));

		editionsDG.addObject(new DocumentEditions("Export detaille des liquidations de classe 2 non inventoriees ",
				"Export detaille des liquidations de classe 2 non inventoriees",true,true,"DepenseNonInventoriees.jasper",
				" select dpco.exe_ordre, dep.pco_num, dep.DPP_NUMERO_FACTURE, replace(dep.DPCO_MONTANT_BUDGETAIRE,'.',','),"+
				"   replace(dep.DPCO_TVA_SAISIE,'.',','), replace(dep.DPCO_HT_SAISIE,'.',','), replace(dep.DPCO_TTC_SAISIE,'.',','),"+
				"   nvl(dep.BOR_NUM,'0'), nvl(dep.MAN_NUMERO,'0'), nvl(dep.GES_CODE,'0'), com.comm_numero, com.comm_libelle,"+
				"   o.org_ub, o.org_cr, o.org_souscr, four.fou_code, four.adr_civilite, four.adr_nom||four.adr_prenom"+
				" from jefy_depense.depense_ctrl_planco dpco, jefy_inventaire.V_DEPENSE_export dep,jefy_depense.commande com,jefy_admin.organ o," +
				"   grhum.V_FOURNIS_GRHUM four"+
				" where dpco.pco_num like '2%'"+
				"   and dpco.dpco_id not in ( select dpco_id from jefy_inventaire.INVENTAIRE_COMPTABLE where exe_ordre = $P{EXERCICE} and dpco_id is not null)"+
				"   and dpco.dpco_id = dep.dpco_id and dpco.dpco_montant_budgetaire>0 and com.comm_id = dep.comm_id and o.org_id = dep.org_id"+
				"   and four.fou_ordre = com.fou_ordre and dpco.exe_ordre = $P{EXERCICE}",
				"EXERCICE,IMPUTATION,NUMERO_FACTURE,MONTANT_BUDGETAIRE,TVA_SAISIE,HT_SAISIE,TTC_SAISIE,BOR_NUMERO,MAN_NUMERO,"+
				"GES_CODE,COM_NUMERO,COM_LIBELLE,UB,CR,SOUS_CR,CODE_FOUR,CIVILITE,NOM",  ""));
		
		return editionsDG;
	}
	
	public void changementEdition() {
		if (getMonActifNib().selectedDocument()!=null) {
			getMonActifNib().getJButtonCocktailPdf().setEnabled(getMonActifNib().selectedDocument().isPdf());
			getMonActifNib().getJButtonCocktailXls().setEnabled(getMonActifNib().selectedDocument().isXls());
		}
	}

	public void actionFermer(){
		masquerFenetre();
	}

	public void actionImprimer (){
		try {
			DocumentEditions document=getMonActifNib().selectedDocument();
			if (document==null) return;
			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(app.getApplicationParametre("REP_BASE_JASPER_PATH"),"SUBREPORT_DIR");
			parameters.takeValueForKey(app.getCurrentExercice().exeOrdre(),"EXERCICE");

			System.out.println(parameters);

			app.getToolsCocktailReports().imprimerReportParametres(document.getJasper(), parameters, "actif"+UtilCtrl.lastModif());
		} catch (Exception e) {
			System.out.println("actionImprimer OUPS"+e);
		}
	}

	public void actionExporter (){
		try {
			DocumentEditions document=getMonActifNib().selectedDocument();
			if (document==null) return;
			
			app.ceerTransactionLog();
			app.afficherUnLogDansTransactionLog("DEBUT....",10);
			app.afficherUnLogDansTransactionLog("Creation du fichier XLS ...",20);
			app.afficherUnLogDansTransactionLog("Recuperation des donnees XLS ...",25);
			
			NSData mesDatas = new NSData(app.getSQlResult(
					StringCtrl.replace(document.getSql(), "$P{EXERCICE}", app.getCurrentExercice().exeOrdre().toString()),
					NSArray.componentsSeparatedByString(document.getProjection(), ",")));
			app.afficherUnLogDansTransactionLog("Recuperation des donnees XLS ...OK",50);
			app.afficherUnLogDansTransactionLog("Ecriture des donnees XLS ...",55);

			String targetDir = System.getProperty("java.io.tmpdir");
			if (!targetDir.endsWith(java.io.File.separator)) {
				targetDir = targetDir + java.io.File.separator;
			}        
			
			java.io.File f = new java.io.File(targetDir + "export"+UtilCtrl.lastModif()+ ".csv");
			//on verifie si le fichier cible existe deja :

			if (f.createNewFile()) {
				UtilCtrl.writeTextFileSystemEncoding (f, UtilCtrl.nsdataToCsvString(mesDatas));
			}
			
			app.afficherUnLogDansTransactionLog("Ecriture des donnees XLS ...OK",85);

			app.getToolsCocktailSystem().openFile(f.getAbsolutePath());

			app.afficherUnLogDansTransactionLog("Creation du fichier XLS...OK",100);
			app.finirTransactionLog();
			app.fermerTransactionLog();
		} catch (Exception e) {
			app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
			app.afficherUnLogDansTransactionLog("probleme !!"+e.getMessage(),0);
			app.finirTransactionLog();
		}
	}
	
	private ActifNib getMonActifNib() {
		return monActifNib;
	}

	private void setMonActifNib(ActifNib monActifNib) {
		this.monActifNib = monActifNib;
	}
}
