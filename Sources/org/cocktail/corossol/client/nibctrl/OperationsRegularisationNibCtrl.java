package org.cocktail.corossol.client.nibctrl;

import java.awt.event.ActionEvent;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.palette.ToolsSwing;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.procedure.ProcedureEclater;
import org.cocktail.corossol.client.eof.procedure.ProcedureRegrouper;
import org.cocktail.corossol.client.finder.FinderCleInventaireComptable;
import org.cocktail.corossol.client.finder.FinderInventaireComptable;
import org.cocktail.corossol.client.nib.ComptableInventaireInspecteurNib;
import org.cocktail.corossol.client.nib.OperationsRegularisationNib;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

public class OperationsRegularisationNibCtrl {

	private ApplicationCorossol	myapp;

	private OperationsRegularisationNib operationsRegularisationNib;
	private EOCleInventaireComptable currentCleInventaireComptable;

	ComptableInventaireInspecteurNibCtrl monComptableInventaireInspecteurNibCtrl=null;
	ComptableInventaireInspecteurNib monComptableInventaireInspecteurNib=null;
	private static final String FENETRE_INSPECTEUR_INVENTAIRE = "Fenetre de gestion du numero d'inventaire comptable";

	public OperationsRegularisationNibCtrl(ApplicationCocktail ctrl) {
		super();
		myapp = (ApplicationCorossol) ctrl;

		operationsRegularisationNib=new OperationsRegularisationNib();
		setCurrentCleInventaireComptable(null);

		initGui();
	}

	private void initGui() {
		monComptableInventaireInspecteurNibCtrl=new ComptableInventaireInspecteurNibCtrl(myapp, 100,100,610,620);
		monComptableInventaireInspecteurNib=new ComptableInventaireInspecteurNib();
		monComptableInventaireInspecteurNibCtrl.creationFenetre(monComptableInventaireInspecteurNib,ToolsSwing.formaterStringU(FENETRE_INSPECTEUR_INVENTAIRE),null);
		monComptableInventaireInspecteurNib.setPreferredSize(new java.awt.Dimension(610, 620));
		monComptableInventaireInspecteurNib.setSize(610, 620);



		operationsRegularisationNib.btRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				operationsRegularisationNib.setInventaire(rechercher(operationsRegularisationNib.tfRechercheCommande().getText(), operationsRegularisationNib.tfRechercheNumeroInventaire().getText()));
			} });

		operationsRegularisationNib.btRegroupement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				setCurrentCleInventaireComptable(operationsRegularisationNib.inventaireSelectionne());
				if (currentCleInventaireComptable!=null) {
					operationsRegularisationNib.panneauRegroupement();

					operationsRegularisationNib.setInventaireRegroupement(
							FinderInventaireComptable.findLesClesInventaireComptables(myapp, currentCleInventaireComptable));
				}
			} });
		operationsRegularisationNib.btEclatement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				setCurrentCleInventaireComptable(operationsRegularisationNib.inventaireSelectionne());
				if (currentCleInventaireComptable!=null) {
					operationsRegularisationNib.panneauEclatement();

					NSMutableArray arrayInventaires=new NSMutableArray();
					for (int i=0; i<currentCleInventaireComptable.inventaireComptables().count(); i++) {
						arrayInventaires.addObjectsFromArray(((EOInventaireComptable)currentCleInventaireComptable.inventaireComptables().objectAtIndex(i)).inventaires());
					}
					operationsRegularisationNib.setInventaireEclatement(arrayInventaires);
				}
			} });
		operationsRegularisationNib.btPrecedentRegroupement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				setCurrentCleInventaireComptable(null);
				operationsRegularisationNib.panneauPrecedent();
			} });
		operationsRegularisationNib.btPrecedentEclatement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				setCurrentCleInventaireComptable(null);
				operationsRegularisationNib.panneauPrecedent();
			} });
		operationsRegularisationNib.btEnregistrerRegroupement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (!EODialogs.runConfirmOperationDialog("Regroupement", "Voulez-vous vraiment regrouper ces inventaires ?", "OUI", "NON"))
					return;

				enregistrerRegroupement();
				operationsRegularisationNib.panneauPrecedent();
			} });

		operationsRegularisationNib.btEnregistrerEclatement().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (!EODialogs.runConfirmOperationDialog("Eclatement", "Voulez-vous vraiment eclater ces inventaires ?", "OUI", "NON"))
					return;

				enregistrerEclatement();
				operationsRegularisationNib.panneauPrecedent();
			} });
	}

	private void setCurrentCleInventaireComptable(EOCleInventaireComptable inventaire) {
		currentCleInventaireComptable=inventaire;
		operationsRegularisationNib.afficherInventaire(currentCleInventaireComptable);
	}

	public void afficherFenetre() {
		operationsRegularisationNib.show();
	}

	private NSArray rechercher(String critereCommande, String critereNumeroInventaire) {
		return FinderCleInventaireComptable.findLesInventaireComptables(myapp, critereNumeroInventaire, critereCommande);
	}

	private void enregistrerRegroupement() {
		if (currentCleInventaireComptable==null || operationsRegularisationNib.inventaireRegroupementSelectionne()==null)
			return;

		try {
			ProcedureRegrouper.enregistrer(myapp, currentCleInventaireComptable, operationsRegularisationNib.inventaireRegroupementSelectionne());
		}
		catch (Exception ex) {
			System.out.println(ex);
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}
	}

	private void enregistrerEclatement() {  // 962/CRI/2009/35
		System.out.println("enregistrerEclatement ...");
		if (operationsRegularisationNib.inventairesEclatementSelectionnes()==null || operationsRegularisationNib.inventairesEclatementSelectionnes().count()==0)
			return;
		try {
			ProcedureEclater.enregistrer(myapp, currentCleInventaireComptable, operationsRegularisationNib.inventairesEclatementSelectionnes());
			NSMutableArray array=new NSMutableArray();
			NSArray lesObjets=currentCleInventaireComptable.inventaireComptables();
			for (int i=0; i<lesObjets.count(); i++)
				array.addObject(myapp.getAppEditingContext().globalIDForObject((EOEnterpriseObject)lesObjets.objectAtIndex(i)));
		    myapp.getAppEditingContext().invalidateObjectsWithGlobalIDs(array);
		}
		catch (Exception ex) {
			System.out.println(ex);
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}

		NSDictionary dico=myapp.returnValuesForLastStoredProcedureInvocation();
		if (dico!=null && dico.objectForKey("20_a_clic_id_destination")!=null) {
			Integer cle=(Integer)dico.objectForKey("20_a_clic_id_destination");
			EOCleInventaireComptable inventaire=FinderCleInventaireComptable.findInventaireComptable(myapp, cle);
			if (inventaire!=null)
				EODialogs.runErrorDialog("INFO", "Le nouvel inventaire comptable est : "+inventaire.clicNumComplet());			
		}
	}
}
