package org.cocktail.corossol.client.nibctrl;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.UtilCtrl;
import org.cocktail.corossol.client.eof.factory.FactoryAmortissement;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireSortie;
import org.cocktail.corossol.client.eof.metier.EOTypeSortie;
import org.cocktail.corossol.client.eof.procedure.ProcedureEnregisrerSortie;
import org.cocktail.corossol.client.eof.procedure.ProcedureSupprimerSortie;
import org.cocktail.corossol.client.finder.FinderInventaire;
import org.cocktail.corossol.client.finder.FinderTypeSortie;
import org.cocktail.corossol.client.nib.DateCtrl;
import org.cocktail.corossol.client.nib.SortieNib;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class SortieNibCtrl {

	private ApplicationCorossol	myapp;

	private SortieNib sortieNib;
	private EOInventaire currentInventaire;
	private EOInventaireSortie currentInventaireSortie;

	public SortieNibCtrl(ApplicationCocktail ctrl) {
		super();
		myapp = (ApplicationCorossol)ctrl;

		sortieNib=new SortieNib();
		setCurrentInventaire(null);

		initGui();
	}

	private void initGui() {
		sortieNib.remplirPopupTypeSortie(FinderTypeSortie.findAll(myapp));

		sortieNib.btRechercher().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				sortieNib.setInventaire(rechercher(sortieNib.tfRechercheCommande().getText(), sortieNib.tfRechercheNumeroInventaire().getText(),
						sortieNib.cbRechercheSortie().isSelected()));
			} });
		sortieNib.table().addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent e) {
				if(e.getClickCount() == 2)
					sortieNib.btSuivant().doClick();
			} });

		sortieNib.btSuivant().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				setCurrentInventaire(sortieNib.inventaireSelectionne());
				if (currentInventaire!=null)
					sortieNib.panneauSuivant();
			} });
		sortieNib.btPrecedent().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				setCurrentInventaire(null);
				sortieNib.panneauPrecedent();
			} });
		sortieNib.btEnregistrer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				enregistrer();
			} });
		sortieNib.btImprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				imprimerSortieInventaire();
			} });
		sortieNib.btSupprimer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				supprimer();
			} });
		sortieNib.tfDateSortie().addKeyListener( new KeyListener(){
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()== KeyEvent.VK_ENTER) {
					sortieNib.btCalculer().doClick();
				}
			}
			public void keyReleased(KeyEvent e){}
			public void keyTyped(KeyEvent e){} });
		sortieNib.btCalculer().addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (currentInventaire==null || currentInventaireSortie==null)
					return;
				currentInventaireSortie.setInvsDate(DateCtrl.stringToDate(sortieNib.tfDateSortie().getText(),"%d/%m/%Y"));
				
				if (currentInventaireSortie.invsDate()==null)
					return;
				currentInventaireSortie.setInvsVnc(FactoryAmortissement.precalculVNC(currentInventaire,currentInventaireSortie.invsDate()));
				sortieNib.tfValeurNetteComptable().setText(currentInventaireSortie.invsVnc().toString());
			} });
	}

	private void setCurrentInventaire(EOInventaire inventaire) {
		currentInventaire=inventaire;
		if (currentInventaire==null)
			currentInventaireSortie=null;
		else
			if (currentInventaire.inventaireSorties()==null || currentInventaire.inventaireSorties().count()==0)
				currentInventaireSortie=EOInventaireSortie.createInventaireSortie(myapp.getAppEditingContext(),null,null,null,null);
			else
				currentInventaireSortie=(EOInventaireSortie)currentInventaire.inventaireSorties().objectAtIndex(0);

		sortieNib.afficherInventaire(currentInventaire, currentInventaireSortie);
	}

	public void afficherFenetre() {
		sortieNib.btPrecedent().doClick();
		sortieNib.show();
	}

	private NSArray rechercher(String critereCommande, String critereNumeroInventaire, boolean isSortie) {
		return FinderInventaire.findLesInventaires(myapp, critereCommande, critereNumeroInventaire, isSortie);
	}

	private void enregistrer() {
		if (currentInventaireSortie==null)
			return;

		try {
			
			if (sortieNib.tfValeurNetteComptable().getText().length() > 0)
				currentInventaireSortie.setInvsVnc(new BigDecimal(sortieNib.tfValeurNetteComptable().getText()));
			if (sortieNib.tfValeurCession().getText().length() > 0)
				currentInventaireSortie.setInvsValeurCession(new BigDecimal(sortieNib.tfValeurCession().getText()));	
			
			currentInventaireSortie.setInvsMotif(sortieNib.tfMotifSortie().getText());
			currentInventaireSortie.setInvsDate(DateCtrl.stringToDate(sortieNib.tfDateSortie().getText(),"%d/%m/%Y"));
			currentInventaireSortie.setTypeSortieRelationship((EOTypeSortie)sortieNib.popupTypeSortie().getSelectedItem());
			currentInventaireSortie.setInventaireRelationship(currentInventaire);
		
			ProcedureEnregisrerSortie.enregistrer(myapp, currentInventaireSortie);
		}
		catch (NumberFormatException nfe) {
			System.out.println(nfe);
			EODialogs.runErrorDialog("ERREUR", "Un ou plusieurs montants ne sont pas au format numérique.");
			return;
		}
		catch (Exception ex) {
			
			System.out.println(ex);
			currentInventaireSortie.setInventaireRelationship(null);
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}

		myapp.getAppEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(myapp.getAppEditingContext().globalIDForObject(currentInventaire)));
		myapp.getAppEditingContext().revert();
		myapp.getAppEditingContext().saveChanges();
	}

	private void supprimer() {
		if (!EODialogs.runConfirmOperationDialog("Suppression", "Voulez-vous vraiment supprimer cette sortie d'inventaire ?", "OUI", "NON"))
			return;

		if (currentInventaireSortie==null || myapp.getAppEditingContext().globalIDForObject(currentInventaireSortie)==null || 
				myapp.getAppEditingContext().globalIDForObject(currentInventaireSortie).isTemporary())
			return;

		try {
			if (ProcedureSupprimerSortie.enregistrer(myapp, currentInventaireSortie)) {
				if (currentInventaire!=null) {
					myapp.getAppEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(myapp.getAppEditingContext().globalIDForObject(currentInventaire)));
					myapp.getAppEditingContext().revert();
					myapp.getAppEditingContext().saveChanges();
				}
				sortieNib.btPrecedent().doClick();
			}
		}
		catch (Exception ex) {
			System.out.println(ex);
			EODialogs.runErrorDialog("ERREUR", ex.getMessage());
		}
	}
	private void imprimerSortieInventaire() {
		if (currentInventaire==null 
			|| currentInventaire.inventaireComptable()==null 
			|| currentInventaire.inventaireComptable().cleInventaireComptable()==null
			|| currentInventaireSortie==null
			|| currentInventaireSortie.inventaire()==null
			|| currentInventaireSortie.inventaire().inventaireComptable()==null
			|| currentInventaireSortie.inventaire().inventaireComptable().cleInventaireComptable()==null
			|| currentInventaireSortie.inventaire().inventaireComptable().cleInventaireComptable().clicIdBis()==null)
			return;

		NSMutableDictionary parameters = new NSMutableDictionary();
		parameters.takeValueForKey(myapp.getApplicationParametre("REP_BASE_JASPER_PATH"),"SUBREPORT_DIR");
		
		parameters.takeValueForKey(new Integer(currentInventaireSortie.inventaire().inventaireComptable().cleInventaireComptable().clicIdBis().intValue()),"CLIC_ID");
		
		try {
			myapp.getToolsCocktailReports().imprimerReportParametres("fiche_sortie.jasper", parameters, "fiche_sortie"+UtilCtrl.lastModif());
		} catch (Exception e) {
			System.out.println("Imprimer sortie inventaire :\n"+e);
		}
	}
}
