package org.cocktail.corossol.client.nibctrl;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.finder.FinderGrhum;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.Privileges;
import org.cocktail.corossol.client.UtilCtrl;
import org.cocktail.corossol.client.eof.metier.EOTitre;
import org.cocktail.corossol.client.eof.metier.MyExercice;
import org.cocktail.corossol.client.finder.FinderTitre;
import org.cocktail.corossol.client.nib.EditionsFinancementsNib;
import org.cocktail.corossol.client.nib.StringCtrl;

import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSKeyValueCoding.Null;
import com.webobjects.foundation.NSMutableDictionary;


public class EditionsFinancementsNibCtrl extends NibCtrl {

	static final String METHODE_CHANGEMENT_EDITION="changementEdition";
	
	// le nib
	private EditionsFinancementsNib monEditionsFinancementsNib = null;
	
	// Pour la sélection de titre
	private EOTitre currentTitre=null;
	private TitreNibCtrl titreNibCtrl = null;

	public EditionsFinancementsNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		titreNibCtrl=new TitreNibCtrl(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		titreNibCtrl.creationFenetre(this);
		titreNibCtrl.setModal(true);
//		titreNibCtrl.getMonTitreNib().setPreferredSize(new java.awt.Dimension(600, 500));;
//		titreNibCtrl.getMonTitreNib().setSize(600, 500);
		setWithLogs(false);
	}

	public void creationFenetre(EditionsFinancementsNib leEditionsFinancementsNib, String title) {
		super.creationFenetre(leEditionsFinancementsNib, title);
		setMonEditionsFinancementsNib(leEditionsFinancementsNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	private void bindingAndCustomization() {
		try {
			// popUpExercice ...
			getMonEditionsFinancementsNib().getJComboBoxExercice().removeAllItems();
			NSArray array=FinderGrhum.findLesExercices(app);
			for (int i=0;i<array.count();i++) {
				 MyExercice myExercice=new MyExercice((EOExercice)array.objectAtIndex(i));
				 getMonEditionsFinancementsNib().getJComboBoxExercice().addItem(myExercice);
			}

			getMonEditionsFinancementsNib().getjButtonRechercherTitre().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (!Privileges.isPrivileges(app.getMesPrivileges(), Privileges.INVTITRE)) {
						fenetreDeDialogueInformation("Privilèges insuffisants!");
						return;
					}
					actionRechercherTitre();
				} });
			
			
			getMonEditionsFinancementsNib().getjButtonImprimerFinancement().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if (!Privileges.isPrivileges(app.getMesPrivileges(), Privileges.INVTITRE)) {
						fenetreDeDialogueInformation("Privilèges insuffisants!");
						return;
					}
					actionImprimerFicheSuiviFinancement();
				} });
			
			getMonEditionsFinancementsNib().initTableView();
			getMonEditionsFinancementsNib().getEditionsTBV().addDelegateSelectionListener(this, METHODE_CHANGEMENT_EDITION);
			getMonEditionsFinancementsNib().setEditionsDG(setActifDG());
			getMonEditionsFinancementsNib().getjTextFieldNumTitre().setEnabled(false);

			// le cablage des  objets
			getMonEditionsFinancementsNib().getJButtonCocktailFermer().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionFermer(); } });
			getMonEditionsFinancementsNib().getJButtonCocktailPdf().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionImprimer(); } });
			getMonEditionsFinancementsNib().getJButtonCocktailXls().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) { actionExporter(); } });

			// les images des objets
			getMonEditionsFinancementsNib().getJButtonCocktailFermer().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.FERMER));
			getMonEditionsFinancementsNib().getJButtonCocktailPdf().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.PDF));
			getMonEditionsFinancementsNib().getJButtonCocktailXls().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.XLS));
			getMonEditionsFinancementsNib().getjButtonRechercherTitre().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.RECHERCHER));
			getMonEditionsFinancementsNib().getjButtonImprimerFinancement().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.IMPRIMER));

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonEditionsFinancementsNib());

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol)app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");
		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}
	}

	private NSMutableArrayDisplayGroup setActifDG() {
		NSMutableArrayDisplayGroup editionsDG = new NSMutableArrayDisplayGroup();
		
		editionsDG.addObject(new DocumentEditions("Etat annuel des reprises sur subvention ","Export de l'état annuel des reprises sur subvention",true,true,"Etat_annuel_subventions.jasper",
				" select distinct "+
				" tit.exe_ordre,"+
				" org.org_ub, org.org_cr, nvl(org.org_souscr,' ') org_souscr,"+
				" tit.pco_num,"+
				" nvl(pcoa.pcoa_libelle,' ') pcoa_libelle,"+
				" bor.bor_num,"+
				" rec.rec_numero,"+
				" tit.tit_numero, " +
				" tit.tit_libelle, " +
				" replace(to_char(tit.tit_ht),'.',',') tit_ht,"+
				" replace(to_char(nvl(tit_mntu.mntu,0)),'.',',') mnt_financement,"+
				" replace(to_char(tit.tit_ht-nvl(tit_mntu.mntu,0)),'.',',') mnt_disponible,"+
				" tab_amo.exe_ordre amo_exe_ordre, replace(to_char(tab_amo.sub_montant),'.',',') amo_sub_montant, " +
				" replace(to_char(tab_amo.sub_annuite),'.',',')  amo_sub_annuite, replace(to_char(tab_amo.sub_cumul),'.',',') amo_sub_cumul, " +
				" replace(to_char(tab_amo.sub_residuel),'.',',') amo_sub_residuel"+
				" from maracuja.titre tit, maracuja.plan_comptable pco, maracuja.plan_comptable_amo pcoa, maracuja.planco_amortissement pam,  maracuja.bordereau bor,"+
				" jefy_recette.recette rec, jefy_recette.recette_ctrl_planco reccp,"+
				" jefy_admin.organ org, jefy_admin.utilisateur_organ utlorg,"+
				" (select icor.tit_id, amo.EXE_ORDRE exe_ordre,"+
				" round(SUM(amo.AMO_ACQUISITION*icor.icor_pourcentage/100),2) as SUB_MONTANT,"+
				" round(SUM(amo.AMO_ANNUITE*icor.icor_pourcentage/100),2) as SUB_ANNUITE, "+
				" round(SUM(amo.AMO_CUMUL*icor.icor_pourcentage/100),2) as SUB_CUMUL,"+
				" round(SUM(amo.AMO_RESIDUEL*icor.icor_pourcentage/100),2) as SUB_RESIDUEL "+
				" from jefy_inventaire.amortissement amo,"+
				" jefy_inventaire.inventaire_comptable_orig icor "+
				" where icor.invc_id=amo.invc_id "+
				" and icor.tit_id is not null "+
				" group by icor.tit_id, exe_ordre "+
				" ) tab_amo,"+
				" (select icor.tit_id, sum(icor.icor_montant) mntu"+
				" from jefy_inventaire.inventaire_comptable_orig icor"+
				"  group by icor.tit_id"+
				" ) tit_mntu "+
				" where tit.exe_ordre=$P{EXERCICE}"+
				" and tit.pco_num like $P{PCONUM}||'%'"+
				" and ( org.org_ub=$P{UB} or $P{UB} is null )"+
				" and tit.tit_etat='VISE'"+
				" and tit.org_ordre=org.org_id(+)"+
				" and pco.pco_num=tit.pco_num"+
				" and pco.pco_num=pam.pco_num(+)"+
				" and pam.pcoa_id=pcoa.pcoa_id(+)"+
				" and reccp.tit_id=tit.tit_id"+
				" and tab_amo.tit_id = tit.tit_id"+
				" and tit_mntu.tit_id = tit.tit_id"+
				" and reccp.rec_id=rec.rec_id"+
				" and bor.bor_id=tit.bor_id"+
				" and utlorg.org_id=org.org_id"+
				" and utlorg.utl_ordre=$P{UTLORDRE}"+
				" order by org.org_ub, org.org_cr, org_souscr, tit.pco_num, tit.tit_numero, tab_amo.exe_ordre",
				" EXERCICE, UB, CR, SOUS_CR, COMPTE_IMPUTATION, COMPTE_AMORTISSEMENT, BORDEREAU,"+
				" RECETTE, NUMERO_TITRE, LIBELLE_TITRE , MONTANT_TITRE, MONTANT_FINANCE, MONTANT_DISPONIBLE,"+
				" ANNEE_AMORTISSEMENT, MONTANT_AMORTISSEMENT, ANNUITE_AMORTISSEMENT, CUMUL_AMORTISSEMENT, RESIDUEL_AMORTISSEMENT",
				""));

		editionsDG.addObject(new DocumentEditions("Subventions intégralement reprises ",
				"Export des subventions intégralement reprises ",true,true,"subventions_integlmt_reprises.jasper",
				        " select distinct "+
						" tit.exe_ordre,"+
						" org.org_ub, org.org_cr, nvl(org.org_souscr,' ') org_souscr,"+
						" tit.pco_num,"+
						" nvl(pcoa.pcoa_libelle,' ') pcoa_libelle,"+
						" bor.bor_num,"+
						" rec.rec_numero,"+
						" tit.tit_numero, " +
						"tit.tit_libelle, " +
						" replace(to_char(tit.tit_ht),'.',',') tit_ht,"+
						" replace(to_char(nvl(tit_mntu.mntu,0)),'.',',') mnt_financement,"+
						" replace(to_char(tit.tit_ht-nvl(tit_mntu.mntu,0)),'.',',') mnt_disponible,"+
						" tab_amo.exe_ordre amo_exe_ordre, replace(to_char(tab_amo.sub_montant),'.',',') amo_sub_montant, " +
						" replace(to_char(tab_amo.sub_annuite),'.',',')  amo_sub_annuite, replace(to_char(tab_amo.sub_cumul),'.',',') amo_sub_cumul, " +
						" replace(to_char(tab_amo.sub_residuel),'.',',') amo_sub_residuel"+
						" from maracuja.titre tit, maracuja.plan_comptable pco, maracuja.plan_comptable_amo pcoa, maracuja.planco_amortissement pam,  maracuja.bordereau bor,"+
						" jefy_recette.recette rec, jefy_recette.recette_ctrl_planco reccp,"+
						" jefy_admin.organ org, jefy_admin.utilisateur_organ utlorg,"+
						" (select icor.tit_id, amo.EXE_ORDRE exe_ordre,"+
						" round(SUM(amo.AMO_ACQUISITION*icor.icor_pourcentage/100),2) as SUB_MONTANT,"+
						" round(SUM(amo.AMO_ANNUITE*icor.icor_pourcentage/100),2) as SUB_ANNUITE, "+
						" round(SUM(amo.AMO_CUMUL*icor.icor_pourcentage/100),2) as SUB_CUMUL,"+
						" round(SUM(amo.AMO_RESIDUEL*icor.icor_pourcentage/100),2) as SUB_RESIDUEL "+
						" from jefy_inventaire.amortissement amo,"+
						" jefy_inventaire.inventaire_comptable_orig icor "+
						" where icor.invc_id=amo.invc_id "+
						" and icor.tit_id is not null "+
						" group by icor.tit_id, exe_ordre "+
						" ) tab_amo,"+
						" (select icor.tit_id, sum(icor.icor_montant) mntu"+
						" from jefy_inventaire.inventaire_comptable_orig icor"+
						"  group by icor.tit_id"+
						" ) tit_mntu "+
						" where tit.pco_num like $P{PCONUM}||'%'"+
						" and ( org.org_ub=$P{UB} or $P{UB} is null )"+
						" and tit.tit_etat='VISE'"+
						" and tit.org_ordre=org.org_id(+)"+
						" and pco.pco_num=tit.pco_num"+
						" and pco.pco_num=pam.pco_num(+)"+
						" and pam.pcoa_id=pcoa.pcoa_id(+)"+
						" and reccp.tit_id=tit.tit_id"+
						" and tab_amo.tit_id = tit.tit_id"+
						" and tit_mntu.tit_id = tit.tit_id"+
						" and reccp.rec_id=rec.rec_id"+
						" and bor.bor_id=tit.bor_id"+
						" and utlorg.org_id=org.org_id"+
						" and utlorg.utl_ordre=$P{UTLORDRE}"+
						" and (select SUM(amo.AMO_RESIDUEL) as SUB_RESIDUEL"+
						" from jefy_inventaire.amortissement amo, jefy_inventaire.inventaire_comptable_orig icor"+
						" where icor.invc_id=amo.invc_id and icor.tit_id = tit.tit_id and amo.exe_ordre=$P{EXERCICE}) = 0"+
						" order by org.org_ub, org.org_cr, org_souscr, tit.pco_num, tit.tit_numero, tab_amo.exe_ordre",
				" EXERCICE, UB, CR, SOUS_CR, COMPTE_IMPUTATION, COMPTE_AMORTISSEMENT, BORDEREAU,"+
				" RECETTE, NUMERO_TITRE, LIBELLE_TITRE , MONTANT_TITRE, MONTANT_FINANCE, MONTANT_DISPONIBLE,"+
				" ANNEE_AMORTISSEMENT, MONTANT_AMORTISSEMENT, ANNUITE_AMORTISSEMENT, CUMUL_AMORTISSEMENT, RESIDUEL_AMORTISSEMENT",
				""));
		
		editionsDG.addObject(new DocumentEditions("Subventions non utilisées ",
				"Export des subventions non utilisées",true,true,"subventions_non_utilisees.jasper",
				        " select distinct "+
						" tit.exe_ordre,"+
						" org.org_ub, " +
						" org.org_cr, " +
						" nvl(org.org_souscr,' ') org_souscr,"+
						" tit.pco_num,"+
						" nvl(pcoa.pcoa_libelle,' ') pcoa_libelle,"+
						" bor.bor_num,"+
						" rec.rec_numero,"+
						" tit.tit_numero," +
						" tit.tit_libelle, " +
						" replace(to_char(tit.tit_ht),'.',',') tit_ht,"+
						" replace(to_char(nvl(tit_mntu.mntu,0)),'.',',') mnt_financement,"+
						" replace(to_char(tit.tit_ht-nvl(tit_mntu.mntu,0)),'.',',') mnt_disponible,"+
						" nvl (tab_amo.exe_ordre,' ') amo_exe_ordre, " +
						" nvl (replace(to_char(tab_amo.sub_montant),'.',','),' ') amo_sub_montant, " +
						" nvl (replace(to_char(tab_amo.sub_annuite),'.',','),' ')  amo_sub_annuite, " +
						" nvl (replace(to_char(tab_amo.sub_cumul),'.',','),' ') amo_sub_cumul, " +
						" nvl (replace(to_char(tab_amo.sub_residuel),'.',','),' ') amo_sub_residuel"+
						" from maracuja.titre tit, maracuja.plan_comptable pco, maracuja.plan_comptable_amo pcoa, maracuja.planco_amortissement pam,  maracuja.bordereau bor,"+
						" jefy_recette.recette rec, jefy_recette.recette_ctrl_planco reccp,"+
						" jefy_admin.organ org, jefy_admin.utilisateur_organ utlorg,"+
						" (select icor.tit_id, amo.EXE_ORDRE exe_ordre,"+
						" round(SUM(amo.AMO_ACQUISITION*icor.icor_pourcentage/100),2) as SUB_MONTANT,"+
						" round(SUM(amo.AMO_ANNUITE*icor.icor_pourcentage/100),2) as SUB_ANNUITE, "+
						" round(SUM(amo.AMO_CUMUL*icor.icor_pourcentage/100),2) as SUB_CUMUL,"+
						" round(SUM(amo.AMO_RESIDUEL*icor.icor_pourcentage/100),2) as SUB_RESIDUEL "+
						" from jefy_inventaire.amortissement amo,"+
						" jefy_inventaire.inventaire_comptable_orig icor "+
						" where icor.invc_id=amo.invc_id "+
						" and icor.tit_id is not null "+
						" group by icor.tit_id, exe_ordre "+
						" ) tab_amo,"+
						" (select icor.tit_id, sum(icor.icor_montant) mntu"+
						" from jefy_inventaire.inventaire_comptable_orig icor"+
						"  group by icor.tit_id"+
						" ) tit_mntu "+
						" where tit.exe_ordre=$P{EXERCICE}"+
						" and tit.pco_num like $P{PCONUM}||'%'"+
						" and ( org.org_ub=$P{UB} or $P{UB} is null )"+
						" and tit.tit_etat='VISE'"+
						" and tit.org_ordre=org.org_id(+)"+
						" and pco.pco_num=tit.pco_num"+
						" and pco.pco_num=pam.pco_num(+)"+
						" and pam.pcoa_id=pcoa.pcoa_id(+)"+
						" and reccp.tit_id=tit.tit_id"+
						" and tab_amo.tit_id(+) = tit.tit_id"+
						" and tit_mntu.tit_id(+) = tit.tit_id"+
						" and reccp.rec_id=rec.rec_id"+
						" and bor.bor_id=tit.bor_id"+
						" and utlorg.org_id=org.org_id"+
						" and utlorg.utl_ordre=$P{UTLORDRE}"+
						" and (tit.tit_ht-nvl(tit_mntu.mntu,0))>0"+
						" order by org.org_ub, org.org_cr, org_souscr, tit.pco_num, tit.tit_numero, amo_exe_ordre",			
				" EXERCICE, UB, CR, SOUS_CR, COMPTE_IMPUTATION, COMPTE_AMORTISSEMENT, BORDEREAU,"+
				" RECETTE, NUMERO_TITRE, LIBELLE_TITRE , MONTANT_TITRE, MONTANT_FINANCE, MONTANT_DISPONIBLE,"+
				" ANNEE_AMORTISSEMENT, MONTANT_AMORTISSEMENT, ANNUITE_AMORTISSEMENT, CUMUL_AMORTISSEMENT, RESIDUEL_AMORTISSEMENT",
				""));
		
		return editionsDG;
	}

	public void changementEdition() {
		if (getMonEditionsFinancementsNib().selectedDocument()!=null) {
			getMonEditionsFinancementsNib().getJButtonCocktailPdf().setEnabled(getMonEditionsFinancementsNib().selectedDocument().isPdf());
			getMonEditionsFinancementsNib().getJButtonCocktailXls().setEnabled(getMonEditionsFinancementsNib().selectedDocument().isXls());
		}
	}

	public void actionFermer(){
		masquerFenetre();
	}
	
	public void actionRechercherTitre() {
		setCurrentTitre(null);
		titreNibCtrl.afficherFenetre(this);		
	}

	public void actionImprimerFicheSuiviFinancement() {
		
        if (currentTitre==null){
			fenetreDeDialogueInformation("Veuillez sélectionner un titre afin de lancer l'impression");
			return;
		}
		   
		try {
			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(new Integer(currentTitre.titIdBis()),"TIT_ID");
			parameters.takeValueForKey(app.getApplicationParametre("REP_BASE_JASPER_PATH"),"SUBREPORT_DIR");

			app.getToolsCocktailReports().imprimerReportParametres("fiche_suivi_financement.jasper", parameters, "fiche_suivi_financement"+UtilCtrl.lastModif());

		} catch (Exception e) {
			System.out.println("actionImprimer OUPS"+e);
		}
		
	}
	
	@Override
	public void swingFinderTerminer(Object sender){
     // L'action est de faire l'impression
		if (sender!=titreNibCtrl) return;
		if (currentTitre!=null) {
			String labeltitre = currentTitre.titLibelle()+"/"+currentTitre.exercice().exeExercice()+"/N°"+currentTitre.titNumero();
			getMonEditionsFinancementsNib().getjTextFieldNumTitre().setText(labeltitre);
		}
	}
	
	public void actionImprimer() {
		try {
			DocumentEditions document = getMonEditionsFinancementsNib().selectedDocument();
			if (document == null) {
				return;
			}
			NSMutableDictionary parameters = new NSMutableDictionary();

			parameters.takeValueForKey(app.getApplicationParametre("REP_BASE_JASPER_PATH"), "SUBREPORT_DIR");

			parameters.takeValueForKey(new Integer(getMonEditionsFinancementsNib().getJComboBoxExercice().getSelectedItem().toString()), "EXERCICE");

			if (getMonEditionsFinancementsNib().getJTextFieldUB().getText() != null
					&& getMonEditionsFinancementsNib().getJTextFieldUB().getText().trim().length() > 0) {
				parameters.takeValueForKey(getMonEditionsFinancementsNib().getJTextFieldUB().getText().toString(), "UB");
			}

			if (getMonEditionsFinancementsNib().getJTextFieldImputation().getText() != null 
					&& getMonEditionsFinancementsNib().getJTextFieldImputation().getText().trim().length() > 0) {
				parameters.takeValueForKey(getMonEditionsFinancementsNib().getJTextFieldImputation().getText().toString(), "PCONUM");
			} else {
				parameters.takeValueForKey("1", "PCONUM");
			}
			if (app != null && app.getCurrentUtilisateur() != null && app.getCurrentUtilisateur().utlOrdre() != null) {
				parameters.takeValueForKey(new Integer(app.getCurrentUtilisateur().utlOrdre()), "UTLORDRE");
			}

			System.out.println(parameters);
			System.out.println(document.getJasper());

			String fileName = document.getJasper().replaceFirst("\\.jasper", "");
			app.getToolsCocktailReports().imprimerReportParametres(document.getJasper(), parameters, fileName + UtilCtrl.lastModif());
		} catch (Exception e) {
			System.out.println("actionImprimer OUPS" + e);
		}
	}

	public void actionExporter() {
		try {
			DocumentEditions document = getMonEditionsFinancementsNib().selectedDocument();
			if (document == null) {
				return;
			}

			app.ceerTransactionLog();
			app.afficherUnLogDansTransactionLog("DEBUT....", 10);
			app.afficherUnLogDansTransactionLog("Creation du fichier XLS ...", 20);
			app.afficherUnLogDansTransactionLog("Recuperation des donnees XLS ...", 25);

			// Mise à jour des paramètres
			// EXERCICE
			String sqlString = StringCtrl.replace(document.getSql(), "$P{EXERCICE}", getMonEditionsFinancementsNib().getJComboBoxExercice().getSelectedItem()
					.toString());

			// UB
			if (getMonEditionsFinancementsNib().getJTextFieldUB().getText() != null
					&& getMonEditionsFinancementsNib().getJTextFieldUB().getText().trim().length() > 0) {
				sqlString = StringCtrl.replace(sqlString, "$P{UB}", getMonEditionsFinancementsNib().getJTextFieldUB().getText());
			} else {
				sqlString = StringCtrl.replace(sqlString, "$P{UB}", "null");
			}

			// PCONUM
			if (getMonEditionsFinancementsNib().getJTextFieldImputation().getText() != null
					&& getMonEditionsFinancementsNib().getJTextFieldImputation().getText().trim().length() > 0) {
				sqlString = StringCtrl.replace(sqlString, "$P{PCONUM}", getMonEditionsFinancementsNib().getJTextFieldImputation().getText().toString());
			} else {
				sqlString = StringCtrl.replace(sqlString, "$P{PCONUM}", "1");
			}

			// UTLORDRE
			if (app != null && app.getCurrentUtilisateur() != null && app.getCurrentUtilisateur().utlOrdre() != null) {
				sqlString = StringCtrl.replace(sqlString, "$P{UTLORDRE}", app.getCurrentUtilisateur().utlOrdre().toString());
			} else {
				sqlString = StringCtrl.replace(sqlString, "$P{UTLORDRE}", "null");
			}

			NSData mesDatas = new NSData(app.getSQlResult(sqlString, NSArray.componentsSeparatedByString(document.getProjection(), ",")));

			app.afficherUnLogDansTransactionLog("Recuperation des donnees XLS ...OK", 50);
			app.afficherUnLogDansTransactionLog("Ecriture des donnees XLS ...", 55);

			String targetDir = System.getProperty("java.io.tmpdir");
			if (!targetDir.endsWith(java.io.File.separator)) {
				targetDir = targetDir + java.io.File.separator;
			}
			String fileName = document.getJasper().replaceFirst("\\.jasper", "");
			java.io.File f = new java.io.File(targetDir + "export_" + fileName + UtilCtrl.lastModif() + ".csv");
			// on verifie si le fichier cible existe deja :

			if (f.createNewFile()) {
				UtilCtrl.writeTextFileSystemEncoding(f, UtilCtrl.nsdataToCsvString(mesDatas));
			}

			app.afficherUnLogDansTransactionLog("Ecriture des donnees XLS ...OK", 85);
			app.getToolsCocktailSystem().openFile(f.getAbsolutePath());
			app.afficherUnLogDansTransactionLog("Creation du fichier XLS...OK", 100);
			app.finirTransactionLog();
			app.fermerTransactionLog();
		} catch (Exception e) {
			app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
			app.afficherUnLogDansTransactionLog("probleme !!" + e.getMessage(), 0);
			app.finirTransactionLog();
		}
	}
	
	private EditionsFinancementsNib getMonEditionsFinancementsNib() {
		return monEditionsFinancementsNib;
	}

	private void setMonEditionsFinancementsNib(
			EditionsFinancementsNib monEditionsFinancementsNib) {
		this.monEditionsFinancementsNib = monEditionsFinancementsNib;
	}

	public EOTitre getCurrentTitre() {
		return currentTitre;
	}

	public void setCurrentTitre(EOTitre currentTitre) {
		this.currentTitre = currentTitre;
	}
}
