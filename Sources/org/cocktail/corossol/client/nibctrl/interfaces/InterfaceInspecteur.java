package org.cocktail.corossol.client.nibctrl.interfaces;

public interface InterfaceInspecteur {

	public void inspecteurFermer(Object sender);
	public void inspecteurAnnuler(Object sender);
	
}
