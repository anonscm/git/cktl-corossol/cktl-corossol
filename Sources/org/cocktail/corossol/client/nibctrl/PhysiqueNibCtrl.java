/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.corossol.client.nibctrl;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOIndividuUlr;
import org.cocktail.application.client.eof.EOSalles;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.ToolsSwing;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.process.ProcessInventaire;
import org.cocktail.corossol.client.finder.FinderPhysique;
import org.cocktail.corossol.client.nib.PhysiqueInspecteurNib;
import org.cocktail.corossol.client.nib.PhysiqueNib;

import com.webobjects.foundation.NSMutableArray;

public class PhysiqueNibCtrl extends NibCtrl {

	// les tables view
	private JTableViewCocktail mesLivraisonsTBV=null;
	private JTableViewCocktail mesBiensTBV=null;

	// les displaygroup
	private NSMutableArrayDisplayGroup mesLivraisonsDG = new NSMutableArrayDisplayGroup();
	private NSMutableArrayDisplayGroup mesBiensDG = new NSMutableArrayDisplayGroup();

	//methodes 
	private static final String METHODE_ACTION_FERMER = "actionFermer";
	private static final String FENETRE_DE_CREATION_INSPECTEUR_INVENTAIRE ="Fenetre inspecteur inventaire";
	private static final String METHODE_AFFICHER_FENETRE = "afficherFenetre";
	private static final String METHODE_CHANGEMENT_SELECTION_LIVRAISON = "changementSelectionLivraison";
	private static final String METHODE_CHANGEMENT_SELECTION_BIENS = "changementSelectionBiens";

	// le nib
	private PhysiqueNib monPhysiqueNib =null;

	// inspecteur
	PhysiqueInspecteurNibCtrl monPhysiqueInspecteurNibCtrl=null;
	PhysiqueInspecteurNib monPhysiqueInspecteurNib=null;


	private SwingFinderIndividuUlr monSwingFinderIndividuUlrResp=null;
	private SwingFinderIndividuUlr monSwingFinderIndividuUlrTitulaire=null;
	private SwingFinderLocal monSwingFinderLocal=null;

	public PhysiqueNibCtrl(ApplicationCocktail arg0, int arg1, int arg2,
			int arg3, int arg4) {
		super(arg0, arg1, arg2, arg3, arg4);

		// creation de lassistant du repsonsable
		monSwingFinderIndividuUlrResp = new SwingFinderIndividuUlr(arg0, this,arg1 , arg2, 400, 380,true);

		// reation de lassistant du titulire
		monSwingFinderIndividuUlrTitulaire = new SwingFinderIndividuUlr(arg0, this,arg1 , arg2, 400, 380,true);
		
		// creation de lassistant de localistion
		monSwingFinderLocal = new SwingFinderLocal(arg0, this,arg1 , arg2, 400, 380,true);

		// creation de l inspecteur physique
		setMonPhysiqueInspecteurNibCtrl(new PhysiqueInspecteurNibCtrl(arg0, arg1 , arg2, arg3 , arg4));
		setMonPhysiqueInspecteurNib( new PhysiqueInspecteurNib());
		getMonPhysiqueInspecteurNibCtrl().creationFenetre(getMonPhysiqueInspecteurNib(),ToolsSwing.formaterStringU(FENETRE_DE_CREATION_INSPECTEUR_INVENTAIRE),this);
		getMonPhysiqueInspecteurNib().setPreferredSize(new java.awt.Dimension(500, 500));
		getMonPhysiqueInspecteurNib().setSize(500,500);
	}

	public void creationFenetre(PhysiqueNib physiqueNib,String title) {
		super.creationFenetre( physiqueNib,title);
		setMonPhysiqueNib(physiqueNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void afficherFenetre(){
		super.afficherFenetre();
		if (((ApplicationCorossol)app).getCurrentCommande() == null){
			masquerFenetre();
			fenetreDeDialogueInformation("Impossible \n Vous devez choisir une commande !");
		}
	}

	private void bindingAndCustomization() {
		try {
			// creation de la table view de livraison
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData( "Commande",100, JLabel.CENTER ,"commande.commNumero","String"));
			lesColumns.addObject(new ColumnData( "Exercice",200, JLabel.CENTER ,"exercice.exeExercice","String"));
			lesColumns.addObject(new ColumnData( "Magasin",200, JLabel.CENTER ,"magasin.magLibelle","String"));
			lesColumns.addObject(new ColumnData( "Createur",200, JLabel.CENTER ,"utilisateur.individu.nomUsuel","String"));
			lesColumns.addObject(new ColumnData( "Date Liv.",200, JLabel.CENTER ,"livDate","String"));
			lesColumns.addObject(new ColumnData( "Numero Liv.",200, JLabel.CENTER ,"livNumero","String"));
			lesColumns.addObject(new ColumnData( "Reception ",200, JLabel.CENTER ,"livReception","String"));

			setLivraisonsDG();
			setMesLivraisonsTBV(new JTableViewCocktail(lesColumns,getMesLivraisonsDG(),new Dimension(100,100),JTable.AUTO_RESIZE_LAST_COLUMN));
		//	getMonPhysiqueNib().getJPanelListeLivraisons().add(getMesLivraisonsTBV());
			getMonPhysiqueNib().getMesLivraisonsTBV().initTableViewCocktail(getMesLivraisonsTBV());

			// creation de la table view de detail livraison
			NSMutableArray lesColumnsDetail = new NSMutableArray();
			lesColumnsDetail.addObject(new ColumnData( "Libelle",200, JLabel.CENTER ,"livraisonDetail.lidLibelle","String"));
			lesColumnsDetail.addObject(new ColumnData( "Montant",80, JLabel.CENTER ,"invMontantAcquisition","String"));
			lesColumnsDetail.addObject(new ColumnData( "Num. Serie",80, JLabel.CENTER ,"invSerie","String"));

			setBiensDG();
			setMesBiensTBV(new JTableViewCocktail(lesColumnsDetail,getMesBiensDG(),new Dimension(100,100),JTable.AUTO_RESIZE_LAST_COLUMN));
			//getMonPhysiqueNib().getJPanelListeBiens().add(getMesBiensTBV());
			getMonPhysiqueNib().getMesBiensTBV().initTableViewCocktail(getMesBiensTBV());

			// les listeners
			getMesLivraisonsTBV().addDelegateSelectionListener(this,METHODE_CHANGEMENT_SELECTION_LIVRAISON);
			getMesLivraisonsTBV().addDelegateKeyListener(this,METHODE_CHANGEMENT_SELECTION_LIVRAISON);
			getMesBiensTBV().addDelegateSelectionListener(this,METHODE_CHANGEMENT_SELECTION_BIENS);
			getMesBiensTBV().addDelegateKeyListener(this,METHODE_CHANGEMENT_SELECTION_BIENS);

			// multi ou mono selection dans les tbv
			getMesLivraisonsTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			getMesBiensTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

//			 cablage des Inspecteurs
			monSwingFinderIndividuUlrResp.relierBouton((JButtonCocktail) getMonPhysiqueNib().getJButtonCocktailInspecResponsable());
			monSwingFinderIndividuUlrTitulaire.relierBouton((JButtonCocktail) getMonPhysiqueNib().getJButtonCocktailInspecProprietaire());
			monSwingFinderLocal.relierBouton((JButtonCocktail) getMonPhysiqueNib().getJButtonCocktailInspecLocalisation());
			
			// le cablage des  objets
			getMonPhysiqueNib().getJButtonCocktailFermer().addDelegateActionListener(this,METHODE_ACTION_FERMER);
			getMonPhysiqueNib().getJButtonCocktailInspecInformations().addDelegateActionListener(getMonPhysiqueInspecteurNibCtrl(),METHODE_AFFICHER_FENETRE);
			getMonPhysiqueNib().getJButtonCocktailFermer().addDelegateActionListener(this,METHODE_ACTION_FERMER);

			// les images des objets
//			getMonPhysiqueNib().getJButtonCocktailTraiterLaSelection().setIcone("page_obj");
			getMonPhysiqueNib().getJButtonCocktailFermer().setIcone(IconeCocktail.FERMER);//"close_view");
			getMonPhysiqueNib().getJButtonCocktailInspecDateAcquisition().setIcone(IconeCocktail.INSPECTER);//"int_obj");
			getMonPhysiqueNib().getJButtonCocktailInspecInformations().setIcone(IconeCocktail.INSPECTER);//"int_obj");
			getMonPhysiqueNib().getJButtonCocktailInspecLocalisation().setIcone(IconeCocktail.INSPECTER);//"int_obj");
			getMonPhysiqueNib().getJButtonCocktailInspecNumero().setIcone(IconeCocktail.INSPECTER);//"int_obj");
			getMonPhysiqueNib().getJButtonCocktailInspecProprietaire().setIcone(IconeCocktail.INSPECTER);//"int_obj");
			getMonPhysiqueNib().getJButtonCocktailInspecResponsable().setIcone(IconeCocktail.INSPECTER);//"int_obj");

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol)app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");

			// obseration des changements de selection de commande :
			((ApplicationCorossol)app).getObserveurCommandeSelection().addObserverForMessage(this, "changementCommande");
			((ApplicationCorossol)app).getObserveurCommandeSelection().addObserverForMessage(this, "changementCommandeDetail");

			// disable
			setEnabledComposant(getMonPhysiqueNib().getJTextFieldCocktailDateAcquisition(),false);
			setEnabledComposant(getMonPhysiqueNib().getJTextFieldCocktailInformations(),false);
			setEnabledComposant(getMonPhysiqueNib().getJTextFieldCocktailLocalisation(),false);
			setEnabledComposant(getMonPhysiqueNib().getJTextFieldCocktailNumero(),false);
			setEnabledComposant(getMonPhysiqueNib().getJTextFieldCocktailProritaire(),false);
			setEnabledComposant(getMonPhysiqueNib().getJTextFieldCocktailResponsable(),false);

			//sera modal lors d une transaction
			app.addLesPanelsModal(getMonPhysiqueNib());
		}catch(Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
		}
	}

	// methode cablage 
	public void actionFermer(){
		//	trace(METHODE_ACTION_FERMER);
		masquerFenetre();	
	}

	// methodes observeurs
	public void changementExercice(){
		setLivraisonsDG();
	}

	public void changementCommande(){
		setLivraisonsDG();
		setBiensDG();
	}
	public void changementCommandeDetail(){
		setBiensDG();
	}
	
	public void changementSelectionLivraison(){
		setBiensDG();
	}

	public void changementSelectionBiens(){
		afficherInventaireCourant((EOInventaire)getMesBiensTBV().getSelectedObjects().objectAtIndex(0));
	}

	private void afficherInventaireCourant(EOInventaire inventaire) {
		viderInventaireCourant();
		getMonPhysiqueNib().getJTextFieldCocktailDateAcquisition().setText(inventaire.livraisonDetail().livraison().livDate().toLocaleString());

		if(inventaire.invCommentaire() != null)
			getMonPhysiqueNib().getJTextFieldCocktailInformations().setText(inventaire.invCommentaire());

		if(inventaire.salles() != null)
			getMonPhysiqueNib().getJTextFieldCocktailLocalisation().setText(inventaire.salles().local().appellation()+"-"+inventaire.salles().salEtage()+"-"+inventaire.salles().salPorte());

		if(inventaire.individuTit() != null)
			getMonPhysiqueNib().getJTextFieldCocktailProritaire().setText(
					inventaire.individuTit().cCivilite()+" "+inventaire.individuTit().nomUsuel()+" "+inventaire.individuTit().prenom());

		if(inventaire.individuResp() != null)
			getMonPhysiqueNib().getJTextFieldCocktailResponsable().setText(
					inventaire.individuResp().cCivilite()+" "+inventaire.individuResp().nomUsuel()+" "+inventaire.individuResp().prenom());

		if(inventaire.cleInventairePhysique()!= null)
			getMonPhysiqueNib().getJTextFieldCocktailNumero().setText(inventaire.cleInventairePhysique().clipNumero());

		// informations :
		EOInventaire tmpinv = (EOInventaire)getMesBiensTBV().getSelectedObjects().objectAtIndex(0);
		String info="\n Numero de serie : "+tmpinv.invSerie()+"\n Doc : "+tmpinv.invDoc()+"\n Garantie : "+tmpinv.invGar()+
		"\n Maintenance : "+tmpinv.invMaint()+"\n Modele : "+tmpinv.invModele()+"\n Code barre : "+tmpinv.codeBarre()+"\n Commentaire : "+tmpinv.invCommentaire();
		getMonPhysiqueNib().getJTextFieldCocktailInformations().setText(info);
	}

	private void viderInventaireCourant() {
		getMonPhysiqueNib().getJTextFieldCocktailDateAcquisition().setText("");
		getMonPhysiqueNib().getJTextFieldCocktailInformations().setText("");
		getMonPhysiqueNib().getJTextFieldCocktailLocalisation().setText("");
		getMonPhysiqueNib().getJTextFieldCocktailNumero().setText("");
		getMonPhysiqueNib().getJTextFieldCocktailProritaire().setText("");
		getMonPhysiqueNib().getJTextFieldCocktailResponsable().setText("");
		getMonPhysiqueNib().getJTextFieldCocktailInformations().setText("");
	}
	
	// datasource TBV
	private void setLivraisonsDG() {
		getMesLivraisonsDG().removeAllObjects();
		if (getMesLivraisonsTBV() != null)
			getMesLivraisonsTBV().refresh();	

		try {
			if (((ApplicationCorossol)app).getCurrentCommande() != null)
				getMesLivraisonsDG().addObjectsFromArray(FinderPhysique.findLesLivraisons(app));
			if (getMesLivraisonsTBV() != null)
				getMesLivraisonsTBV().refresh();	
		} catch (Exception e) {
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
	}

	private void setBiensDG() {
		getMesBiensDG().removeAllObjects();
		if (getMesBiensTBV() != null)
			getMesBiensTBV().refresh();	

		if (getMesLivraisonsTBV().getSelectedObjects().count() == 1) {
			try {
				if (((ApplicationCorossol)app).getCurrentCommande() != null)
					getMesBiensDG().addObjectsFromArray(FinderPhysique.findLesInventaires(app, (EOLivraison) getMesLivraisonsTBV().getSelectedObjects().objectAtIndex(0)));
				if (getMesBiensTBV() != null)
					getMesBiensTBV().refresh();	
			} catch (Exception e) {
				fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
			}
		}
		viderInventaireCourant();
	}

	private void majLesInventairesTitulaire(EOIndividuUlr object) {
		ProcessInventaire monProcessInventaire=new ProcessInventaire(true);
		try {
			for (int i = 0; i < getMesBiensTBV().getSelectedObjects().count(); i++)
				monProcessInventaire.addIndividuTit((EOInventaire)getMesBiensTBV().getSelectedObjects().objectAtIndex(i), object);
			app.getAppEditingContext().saveChanges();
		} catch (Exception e) {
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
	}

	private void majLesInventairesResponsable(EOIndividuUlr object) {
		ProcessInventaire monProcessInventaire=new ProcessInventaire(true);
		try {
			for (int i = 0; i < getMesBiensTBV().getSelectedObjects().count(); i++)
				monProcessInventaire.addIndividuResp((EOInventaire)getMesBiensTBV().getSelectedObjects().objectAtIndex(i), object);
			app.getAppEditingContext().saveChanges();
		} catch (Exception e) {
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
	}

	private void majLesInventairesLocalisation(EOSalles object) {
		ProcessInventaire monProcessInventaire=new ProcessInventaire(true);
		try {
			for (int i = 0; i < getMesBiensTBV().getSelectedObjects().count(); i++)
				monProcessInventaire.addSalles((EOInventaire)getMesBiensTBV().getSelectedObjects().objectAtIndex(i), object);
			app.getAppEditingContext().saveChanges();
		} catch (Exception e) {
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME ! \n"+e.toString());
		}
	}

	// methodes d interfaces inspecteur
	public void inspecteurAnnuler(Object sender) {
		
	}

	public void inspecteurFermer(Object sender) {
		afficherInventaireCourant((EOInventaire)getMesBiensTBV().getSelectedObjects().objectAtIndex(0));
	}

	// accesseurs
	private PhysiqueNib getMonPhysiqueNib() {
		return monPhysiqueNib;
	}

	private void setMonPhysiqueNib(PhysiqueNib monPhysiqueNib) {
		this.monPhysiqueNib = monPhysiqueNib;
	}

	private NSMutableArrayDisplayGroup getMesBiensDG() {
		return mesBiensDG;
	}

	private void setMesBiensDG(NSMutableArrayDisplayGroup mesBiensDG) {
		this.mesBiensDG = mesBiensDG;
	}

	JTableViewCocktail getMesBiensTBV() {
		return mesBiensTBV;
	}

	private void setMesBiensTBV(JTableViewCocktail mesBiensTBV) {
		this.mesBiensTBV = mesBiensTBV;
	}

	private NSMutableArrayDisplayGroup getMesLivraisonsDG() {
		return mesLivraisonsDG;
	}

	private void setMesLivraisonsDG(NSMutableArrayDisplayGroup mesLivraisonsDG) {
		this.mesLivraisonsDG = mesLivraisonsDG;
	}

	private JTableViewCocktail getMesLivraisonsTBV() {
		return mesLivraisonsTBV;
	}

	private void setMesLivraisonsTBV(JTableViewCocktail mesLivraisonsTBV) {
		this.mesLivraisonsTBV = mesLivraisonsTBV;
	}

	private PhysiqueInspecteurNib getMonPhysiqueInspecteurNib() {
		return monPhysiqueInspecteurNib;
	}

	private void setMonPhysiqueInspecteurNib(PhysiqueInspecteurNib monPhysiqueInspecteurNib) {
		this.monPhysiqueInspecteurNib = monPhysiqueInspecteurNib;
	}

	private PhysiqueInspecteurNibCtrl getMonPhysiqueInspecteurNibCtrl() {
		return monPhysiqueInspecteurNibCtrl;
	}

	private void setMonPhysiqueInspecteurNibCtrl(PhysiqueInspecteurNibCtrl monPhysiqueInspecteurNibCtrl) {
		this.monPhysiqueInspecteurNibCtrl = monPhysiqueInspecteurNibCtrl;
	}
	
	public void swingFinderAnnuler(Object sender) {
	}

	public void swingFinderTerminer(Object sender) {
		if (sender == monSwingFinderIndividuUlrTitulaire)
			majLesInventairesTitulaire((EOIndividuUlr)monSwingFinderIndividuUlrTitulaire.getResultat().objectAtIndex(0));
		if (sender == monSwingFinderIndividuUlrResp)
			majLesInventairesResponsable((EOIndividuUlr)monSwingFinderIndividuUlrResp.getResultat().objectAtIndex(0));
		if (sender == monSwingFinderLocal)
			majLesInventairesLocalisation((EOSalles)monSwingFinderLocal.getResultat().objectAtIndex(0));

		changementSelectionBiens();
	}
}
