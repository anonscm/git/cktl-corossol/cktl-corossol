package org.cocktail.corossol.client.nibctrl;



import java.awt.Dimension;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.tools.Factory;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv;
import org.cocktail.corossol.client.eof.metier.EOVDepense;
import org.cocktail.corossol.client.eof.procedure.ProcedureAmortissementCle;
import org.cocktail.corossol.client.finder.FinderComptable;
import org.cocktail.corossol.client.finder.FinderDepenses;
import org.cocktail.corossol.client.nib.DepenseInspecteurNib;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class DepenseInspecteurNibCtrl extends NibCtrl {

	// methodes
	private static final String METHODE_ACTION_ANNULER_ASSOCIATION = "actionAnnulerAssociation";
	private static final String METHODE_ACTION_MODIFIER = "actionModifier";
	private static final String METHODE_ACTION_ASSOCIER = "actionAssocier";

	private static final String METHODE_CHANGEMENT_SELECTION_INVC ="changementSelectionInventaireComptable";
	private static final String METHODE_CHANGEMENT_SELECTION_DEP ="changementSelectionDepenses";

	private static final String METHODE_ACTION_FERMER = "actionFermer";

	private boolean rechercherCommande = false;
	// les tables view
	private JTableViewCocktail inventaireTBV = null;
	private JTableViewCocktail depensesTBV = null;
	private JTableViewCocktail inventaireComptableTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup inventaireDG = new NSMutableArrayDisplayGroup();
	private NSMutableArrayDisplayGroup depenseDG = new NSMutableArrayDisplayGroup();
	private NSMutableArrayDisplayGroup inventaireComptableDG = new NSMutableArrayDisplayGroup();

	private Factory maFactory = null;

	// le nib
	private DepenseInspecteurNib monDepenseInspecteurNib = null;

	// current 
	private NSArray currentInventaireComptables = null;

	public DepenseInspecteurNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(false);
		setMaFactory(new Factory());
	}

	public void creationFenetre(DepenseInspecteurNib leDepenseInspecteurNib, String title) {
		super.creationFenetre(leDepenseInspecteurNib, title);
		setMonDepenseInspecteurNib(leDepenseInspecteurNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void activerRechercheCommande(boolean b){
		setRechercherCommande(b);
	}

	private void setMonDepenseInspecteurNib(DepenseInspecteurNib leDepenseInspecteurNib) {
		monDepenseInspecteurNib = leDepenseInspecteurNib;
	}

	public void afficherFenetre() {
		super.afficherFenetre();
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonDepenseInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();
	}

	public void afficherFenetre(NSArray invc) {
		super.afficherFenetre();
		setCurrentInventaireComptables(invc);
		setInventaireComptableDG();
		setDepenseDG();
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonDepenseInspecteurNib());
		((ApplicationCorossol) app).activerLesGlassPane();
	}

	private DepenseInspecteurNib getMonDepenseInspecteurNib() {
		return monDepenseInspecteurNib;
	}

	public void creationFenetre(DepenseInspecteurNib leDepenseInspecteurNib, String title,NibCtrl  parentControleur) {
		super.creationFenetre(leDepenseInspecteurNib, title);
		setMonDepenseInspecteurNib(leDepenseInspecteurNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
		this.parentControleur =  parentControleur;
	}

	private void bindingAndCustomization() {
		try {
			this.setEnabledComposant(getMonDepenseInspecteurNib().getJTextFieldCocktailDiff(), false);
			this.setEnabledComposant(getMonDepenseInspecteurNib().getJTextFieldCocktailSomme(), false);
			this.setEnabledComposant(getMonDepenseInspecteurNib().getJTextFieldCocktailSommeDepense(), false);

			formaterUnTexfieldBigDecimal(getMonDepenseInspecteurNib().getJTextFieldCocktailDiff(),"#0.00#");
			formaterUnTexfieldBigDecimal(getMonDepenseInspecteurNib().getJTextFieldCocktailSomme(),"#0.00#");
			formaterUnTexfieldBigDecimal(getMonDepenseInspecteurNib().getJTextFieldCocktailSommeDepense(),"#0.00#");
			formaterUnTexfieldBigDecimal(getMonDepenseInspecteurNib().getJTextFieldCocktailMontant(),"#0.00#");
			getMonDepenseInspecteurNib().getJTextFieldCocktailDiff().setForeground(new java.awt.Color(255,0,0));

			// creation de la table view des inventaires comptables
			NSMutableArray lesColumnsDetail = new NSMutableArray();
			lesColumnsDetail.addObject(new ColumnData("Numero", 150, JLabel.CENTER, EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY + "." + EOCleInventaireComptable.CLIC_NUM_COMPLET_KEY, "String"));
			lesColumnsDetail.addObject(new ColumnData("Amort.", 80, JLabel.CENTER, EOInventaireComptable.CLE_INVENTAIRE_COMPTABLE_KEY + "." + EOCleInventaireComptable.CLIC_TYPE_AMORT_KEY, "String"));
			lesColumnsDetail.addObject(new ColumnData("Montant", 110, JLabel.CENTER, EOInventaireComptable.INVC_MONTANT_AMORTISSABLE_AVEC_ORVS_KEY, "String"));
			lesColumnsDetail.addObject(new ColumnData("Lien Dep. ?", 80, JLabel.CENTER, EOInventaireComptable.DPCO_ID_BOOLEAN_KEY, "boolean"));
			lesColumnsDetail.addObject(new ColumnData("Facture", 150, JLabel.CENTER, EOInventaireComptable.V_DEPENSE_KEY + "." + EOVDepense.DPP_NUMERO_FACTURE_KEY, "String"));
			lesColumnsDetail.addObject(new ColumnData("Fact. Mont. bud.", 110, JLabel.CENTER, EOInventaireComptable.V_DEPENSE_KEY + "." + EOVDepense.DPCO_MONTANT_BUDGETAIRE_KEY, "String"));
			lesColumnsDetail.addObject(new ColumnData("Fact. Mont. TTC", 110, JLabel.CENTER, EOInventaireComptable.V_DEPENSE_KEY + "." + EOVDepense.DPCO_TTC_SAISIE_KEY, "String"));
			lesColumnsDetail.addObject(new ColumnData("Mandat", 60, JLabel.CENTER, EOInventaireComptable.V_DEPENSE_KEY + "." + EOVDepense.MAN_NUMERO_KEY, "String"));
			lesColumnsDetail.addObject(new ColumnData("Bord.", 60, JLabel.CENTER, EOInventaireComptable.V_DEPENSE_KEY + "." + EOVDepense.BOR_NUM_KEY, "String"));

			setInventaireComptableTBV(new JTableViewCocktail(lesColumnsDetail, getInventaireComptableDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_OFF));
			//getMonDepenseInspecteurNib().getJPanelInventaireComptable().add(getInventaireComptableTBV());
			getMonDepenseInspecteurNib().getInventaireComptableTBV().initTableViewCocktail(getInventaireComptableTBV());

			setInventaireComptableDG();

			//			creation de la table view des inventaires
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData( "Imput.",80, JLabel.CENTER ,"livraisonDetail.planComptable.pcoNum","String"));
			lesColumns.addObject(new ColumnData( "Ub",60, JLabel.CENTER ,"livraisonDetail.organ.orgUb","String"));
			lesColumns.addObject(new ColumnData( "Cr",60, JLabel.CENTER ,"livraisonDetail.organ.orgCr","String"));
			lesColumns.addObject(new ColumnData( "Libelle",150, JLabel.CENTER ,"livraisonDetail.lidLibelle","String"));
			lesColumns.addObject(new ColumnData( "Montant",80, JLabel.CENTER ,"invMontantAcquisition","String"));


			setInventaireTBV(new JTableViewCocktail(lesColumns, getInventaireDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			//			getMonDepenseInspecteurNib().getJTbvListeInventaire().add(getInventaireTBV());
			getMonDepenseInspecteurNib().getInventaireTBV().initTableViewCocktail(getInventaireTBV());

			setInventaireDG();

			//			creation de la table view des depenses
			NSMutableArray lesColumnsInv = new NSMutableArray();
			lesColumnsInv.addObject(new ColumnData("Imput.", 80,JLabel.CENTER, "ca_PlanComptable.pcoNum", "String"));
			lesColumnsInv.addObject(new ColumnData("Ub", 60,JLabel.CENTER, "ca_Organ.orgUb", "String"));
			lesColumnsInv.addObject(new ColumnData("Cr", 60,JLabel.CENTER, "ca_Organ.orgCr", "String"));
			lesColumnsInv.addObject(new ColumnData("Numero", 150,JLabel.CENTER, "dppNumeroFacture", "String"));
			lesColumnsInv.addObject(new ColumnData("Montant budgetaire", 90,JLabel.CENTER, "dpcoMontantBudgetaire", "String"));
			lesColumnsInv.addObject(new ColumnData("Montant TTC", 90,JLabel.CENTER, "dpcoTtcSaisie", "String"));
			lesColumnsInv.addObject(new ColumnData("Mandat", 60,JLabel.CENTER, "manNumero", "String"));
			lesColumnsInv.addObject(new ColumnData("Bord.", 60,JLabel.CENTER, "borNum", "String"));

			setDepensesTBV(new JTableViewCocktail(lesColumnsInv, getDepenseDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_OFF));
			//			getMonDepenseInspecteurNib().getJTbvListeDepenses().add(getDepensesTBV());
			getMonDepenseInspecteurNib().getDepensesTBV().initTableViewCocktail(getDepensesTBV());

			setDepenseDG();

			// multi ou mono selection dans les tbv
			getInventaireTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			getDepensesTBV().getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			// le cablage des  objets
			getMonDepenseInspecteurNib().getJButtonCocktailFermer().addDelegateActionListener(this, METHODE_ACTION_FERMER);
			getMonDepenseInspecteurNib().getJButtonCocktailAnnulerLien().addDelegateActionListener(this, METHODE_ACTION_ANNULER_ASSOCIATION);
			getMonDepenseInspecteurNib().getJButtonCocktailAssocier().addDelegateActionListener(this, METHODE_ACTION_ASSOCIER);
			getMonDepenseInspecteurNib().getJButtonCocktailModifier().addDelegateActionListener(this, METHODE_ACTION_MODIFIER);

			// selection
			getInventaireComptableTBV().addDelegateSelectionListener(this,METHODE_CHANGEMENT_SELECTION_INVC);
			getDepensesTBV().addDelegateSelectionListener(this,METHODE_CHANGEMENT_SELECTION_DEP);

			// les images des objets
			getMonDepenseInspecteurNib().getJButtonCocktailFermer().setIcone(IconeCocktail.FERMER);//"close_view");
			getMonDepenseInspecteurNib().getJButtonCocktailAssocier().setIcone(IconeCocktail.VALIDER);//"valider16");
			getMonDepenseInspecteurNib().getJButtonCocktailAnnulerLien().setIcone(IconeCocktail.SUPPRIMER);//"delete_obj");
			getMonDepenseInspecteurNib().getJButtonCocktailModifier().setIcone(IconeCocktail.REFRESH);//"refresh");

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonDepenseInspecteurNib());

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol) app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",e.getMessage(), true);
		}

		getInventaireTBV().refresh();
		getDepensesTBV().refresh();
	}

	// actions 
	public void actionAnnulerAssociation(){
		try {
			//suppression des amortissements
			for (int i = 0; i < getInventaireComptableTBV().getSelectedObjects().count(); i++)
				ProcedureAmortissementCle.enregistrer((ApplicationCorossol)app, 
						((EOInventaireComptable)getInventaireComptableTBV().getSelectedObjects().objectAtIndex(i)).cleInventaireComptable(), 1);

			// on casse le lien
			for (int i = 0; i < getInventaireComptableTBV().getSelectedObjects().count(); i++)
				((EOInventaireComptable)getInventaireComptableTBV().getSelectedObjects().objectAtIndex(i)).setVDepenseRelationship(null);

			app.getAppEditingContext().saveChanges();
			getInventaireComptableTBV().refresh();
		} catch (Exception e) {	
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME de suppression de l'association ! \n"+e.toString());
			getInventaireComptableTBV().refresh();
		}
	}

	public void actionModifier() {
		try {
			if (getInventaireComptableTBV().getSelectedObjects().count() == 1) {
				EOInventaireComptable invc = (EOInventaireComptable) getInventaireComptableTBV().getSelectedObjects()
						.objectAtIndex(0);
				BigDecimal mntAmo = (BigDecimal) getMonDepenseInspecteurNib().getJTextFieldCocktailMontant().getValue();
				BigDecimal mntAmoOld = invc.invcMontantAmortissable();
				mntAmo = mntAmo.subtract((BigDecimal) invc.inventaireComptableOrvs().valueForKey(
						"@sum." + EOInventaireComptableOrv.INVO_MONTANT_ORV_KEY));
				invc.setInvcMontantAmortissable(mntAmo);
				app.getAppEditingContext().saveChanges();
				getInventaireComptableTBV().refresh();
				if (!mntAmo.equals(mntAmoOld)) {
					fenetreDeDialogueInformation("Le montant amortissable ayant été modifié, vous devez modifier les financements \net cette modification regénèrera les amortissements et reprises.");
				}
			} else
				fenetreDeDialogueInformation("Attention ! \n Vous devez choisir un seul numero d'inventaire a modifier !");
		} catch (Exception e) {
			app.getAppEditingContext().revert();
			fenetreDeDialogueInformation("PROBLEME de modification ! \n" + e.toString());
			getInventaireComptableTBV().refresh();
		}
		getMonDepenseInspecteurNib().getJTextFieldCocktailMontant().setValue(new BigDecimal(0));
	}

	public void actionAssocier(){
		if (getInventaireComptableTBV().getSelectedObjects().count() == 0) {
			fenetreDeDialogueInformation("PROBLEME ! \n Il faut choisir des inventaires !");
			return;
		} 
		if (Factory.different((BigDecimal)getMonDepenseInspecteurNib().getJTextFieldCocktailDiff().getValue(),new BigDecimal(0))) {
			fenetreDeDialogueInformation("PROBLEME ! \n Il y a une difference de montant (depenses<>inventaires) !");
			return;
		}

		if(verificationCoherenceComptable(getInventaireDG(), getDepensesTBV().getSelectedObjects())) {
			try {
				for (int i = 0; i < getInventaireComptableTBV().getSelectedObjects().count(); i++)
					ProcedureAmortissementCle.enregistrer((ApplicationCorossol)app, 
							((EOInventaireComptable)getInventaireComptableTBV().getSelectedObjects().objectAtIndex(i)).cleInventaireComptable(), 0);

				for (int i = 0; i < getInventaireComptableTBV().getSelectedObjects().count(); i++)
					((EOInventaireComptable)getInventaireComptableTBV().getSelectedObjects().objectAtIndex(i))
					.setVDepenseRelationship((EOVDepense)getDepensesTBV().getSelectedObjects().objectAtIndex(0));

				app.getAppEditingContext().saveChanges();
				getInventaireComptableTBV().refresh();
			} catch (Exception e) {
				app.getAppEditingContext().revert();
				fenetreDeDialogueInformation("PROBLEME d'association ! \n"+e.toString());
				getInventaireComptableTBV().refresh();
			}
		}	
		else
			fenetreDeDialogueInformation("PROBLEME d'association ! \n" +
			" Impossible d'associer des inventaires et des depenses \nqui ne concernent pas la meme imputation et la meme ligne budgetaire !");
	}

	private boolean verificationCoherenceComptable(NSMutableArrayDisplayGroup a , NSMutableArrayDisplayGroup b){
		// si les depense et les inventaires ne sont pas de meme orgUb orgCr et pcoNum ..... probleme
		for (int i = 0; i < a.count(); i++) {
			for (int j = 0; j < b.count(); j++) {
				if (! (((EOInventaire)a.objectAtIndex(i)).livraisonDetail().organ().orgUb()).equals(((EOVDepense)b.objectAtIndex(j)).ca_Organ().orgUb()))
					return false; 

				if (! (((EOInventaire)a.objectAtIndex(i)).livraisonDetail().organ().orgUb()).equals(((EOVDepense)b.objectAtIndex(j)).ca_Organ().orgUb()))
					return false; 

				if (! (((EOInventaire)a.objectAtIndex(i)).livraisonDetail().organ().orgCr()).equals(((EOVDepense)b.objectAtIndex(j)).ca_Organ().orgCr()))
					return false; 

				if (! (((EOInventaire)a.objectAtIndex(i)).livraisonDetail().planComptable().pcoNum()).equals(
						((EOVDepense)b.objectAtIndex(j)).ca_PlanComptable().pcoNum()))
					return false; 
			}	
		}
		return true;
	}

	//	affichage
	private void setInventaireComptableDG() {
		getInventaireComptableDG().removeAllObjects();
		if (getInventaireComptableTBV() != null)
			getInventaireComptableTBV().refresh();

		try {
			// if (((ApplicationCorossol)app).getCurrentCommande() != null)
			getInventaireComptableDG().addObjectsFromArray(getCurrentInventaireComptables());

			if (getInventaireComptableTBV() != null)
				getInventaireComptableTBV().refresh();	
		} catch (Exception e) {
			System.out.println(e);
		} 
	}

	private void setDepenseDG() {
		if (!isRechercherCommande()) {
			
			getDepenseDG().removeAllObjects();
			if (getDepensesTBV() != null)
				getDepensesTBV().refresh();
		   
			if (((ApplicationCorossol)app).getCurrentCommande()==null) return;

			try {
				
				getDepenseDG().addObjectsFromArray(FinderDepenses.findLesDepensesDeLaCommande(app,((ApplicationCorossol)app).getCurrentCommande()));
				
				if (getDepensesTBV() != null)
					getDepensesTBV().refresh();	
			} catch (Exception e) {
				System.out.println(e);
			} 
			return;
		}

		// recup du premier inventaire componsant cet inventaire comptable 
		getDepenseDG().removeAllObjects();
		EOInventaire tmpoInventaire = (EOInventaire)((NSMutableArrayDisplayGroup) FinderComptable.findLesBiensInventories(app,
						(EOInventaireComptable)getCurrentInventaireComptables().objectAtIndex(0)) ).objectAtIndex(0);
		getDepenseDG().addObjectsFromArray(FinderDepenses.findLesDepensesDeLaCommande(app,tmpoInventaire.livraisonDetail().livraison().commande()));

		if (getDepensesTBV() != null)
			getDepensesTBV().refresh();	
	}

	private void setInventaireDG() {
		getInventaireDG().removeAllObjects();
		if (getInventaireTBV() != null)
			getInventaireTBV().refresh();	

		try {
			for (int i = 0; i < getInventaireComptableTBV().getSelectedObjects().count(); i++)
				getInventaireDG().addObjectsFromArray(FinderComptable.findLesBiensInventories(app,(EOInventaireComptable)getInventaireComptableTBV().getSelectedObjects().objectAtIndex(i)));

			if (getInventaireTBV() != null)
				getInventaireTBV().refresh();	
		} catch (Exception e) {
			System.out.println(e);
		}	 
	}

	private void affichageControle(){
		getMonDepenseInspecteurNib().getJTextFieldCocktailSomme().setValue(new BigDecimal(0.00));
		getMonDepenseInspecteurNib().getJTextFieldCocktailSommeDepense().setValue(new BigDecimal(0.00));
		getMonDepenseInspecteurNib().getJTextFieldCocktailDiff().setValue(new BigDecimal(0.00));
		getMonDepenseInspecteurNib().getJTextFieldCocktailMontant().setValue(new BigDecimal(0.00));

		getMonDepenseInspecteurNib().getJTextFieldCocktailSomme().setValue(
				getMaFactory().computeSumForKeyBigDecimal(getInventaireComptableTBV().getSelectedObjects(), EOInventaireComptable.INVC_MONTANT_ACQUISITION_KEY));
		getMonDepenseInspecteurNib().getJTextFieldCocktailSommeDepense().setValue(
				getMaFactory().computeSumForKeyBigDecimal(getDepensesTBV().getSelectedObjects(), EOVDepense.DPCO_MONTANT_BUDGETAIRE_KEY));
		getMonDepenseInspecteurNib().getJTextFieldCocktailDiff().setValue(
				getMaFactory().computeSumForKeyBigDecimal(getInventaireComptableTBV().getSelectedObjects(), EOInventaireComptable.INVC_MONTANT_ACQUISITION_KEY)
				.subtract(getMaFactory().computeSumForKeyBigDecimal(getDepensesTBV().getSelectedObjects(), EOVDepense.DPCO_MONTANT_BUDGETAIRE_KEY)));

		if (getInventaireComptableTBV().getSelectedObjects().count() == 1 )
			getMonDepenseInspecteurNib().getJTextFieldCocktailMontant().setValue(
					((EOInventaireComptable)getInventaireComptableTBV().getSelectedObjects().objectAtIndex(0)).invcMontantAmortissableAvecOrvs());
	}

	//	changement de selection
	public void changementSelectionInventaireComptable(){
		setInventaireDG();
		affichageControle();
	}

	public void changementSelectionDepenses(){
		affichageControle();
	}

	//	action Fermer
	public void actionFermer() {
		app.addLesPanelsModal(getMonDepenseInspecteurNib());
		app.retirerLesGlassPane();
		masquerFenetre();
		parentControleur.assistantsTerminer(this);
	}

	public void changementExercice() {
	}

	//	accesseurs
	private NSMutableArrayDisplayGroup getInventaireDG() {
		return inventaireDG;
	}

	private JTableViewCocktail getInventaireTBV() {
		return inventaireTBV;
	}

	private void setInventaireTBV(JTableViewCocktail uninventaireTBV) {
		inventaireTBV = uninventaireTBV;
	}

	private NSMutableArrayDisplayGroup getDepenseDG() {
		return depenseDG;
	}

	private JTableViewCocktail getDepensesTBV() {
		return depensesTBV;
	}

	private void setDepensesTBV(JTableViewCocktail undepensesTBV) {
		depensesTBV = undepensesTBV;
	}

	private NSMutableArrayDisplayGroup getInventaireComptableDG() {
		return inventaireComptableDG;
	}

	private JTableViewCocktail getInventaireComptableTBV() {
		return inventaireComptableTBV;
	}

	private void setInventaireComptableTBV(JTableViewCocktail inventaireComptableTBV) {
		this.inventaireComptableTBV = inventaireComptableTBV;
	}

	private NSArray getCurrentInventaireComptables() {
		return currentInventaireComptables;
	}

	private void setCurrentInventaireComptables(NSArray currentInventaireComptables) {
		this.currentInventaireComptables = currentInventaireComptables;
	}

	private Factory getMaFactory() {
		return maFactory;
	}

	private void setMaFactory(Factory maFactory) {
		this.maFactory = maFactory;
	}

	private boolean isRechercherCommande() {
		return rechercherCommande;
	}

	private void setRechercherCommande(boolean rechercherCommande) {
		this.rechercherCommande = rechercherCommande;
	}
}
