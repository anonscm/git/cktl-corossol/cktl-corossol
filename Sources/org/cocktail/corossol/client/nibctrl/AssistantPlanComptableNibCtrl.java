package org.cocktail.corossol.client.nibctrl;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JTableViewCocktail;
import org.cocktail.application.palette.models.ColumnData;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.nib.AssistantPlanComptableNib;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class AssistantPlanComptableNibCtrl extends NibCtrl {

	// methodes
	private static final String METHODE_ACTION_SELECTIONNER = "actionSelectionner";
	private static final String METHODE_ACTION_ANNULER = "actionAnnuler";
	private static final String METHODE_ACTION_FILTRER = "actionFiltrer";

	// les tables view
	private JTableViewCocktail resultatTBV = null;

	// les displaygroup
	private NSMutableArrayDisplayGroup resultatDG = new NSMutableArrayDisplayGroup();

	// le nib
	private AssistantPlanComptableNib monAssistantPlanComptableNib = null;

	// parentControleur
	NibCtrl parentControleur = null;
	
	public AssistantPlanComptableNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
		setWithLogs(false);
	}

	public void creationFenetre(AssistantPlanComptableNib leAssistantPlanComptableNib, String title,NibCtrl parentControleur) {
		super.creationFenetre(leAssistantPlanComptableNib, title);
		setMonAssistantPlanComptableNib(leAssistantPlanComptableNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
		this.parentControleur =  parentControleur;
	}

	public void afficherFenetre() {
		super.afficherFenetre();
		((ApplicationCorossol) app).removeFromLesPanelsModal(getMonAssistantPlanComptableNib());
		((ApplicationCorossol) app).activerLesGlassPane();
	}

	private void bindingAndCustomization() {
		try {
			// creation de la table view des types de bordereaux
			NSMutableArray lesColumns = new NSMutableArray();
			lesColumns.addObject(new ColumnData("Numero", 100,JLabel.CENTER, "pcoNum", "String"));
			lesColumns.addObject(new ColumnData("Libelle", 100,JLabel.CENTER, "pcoLibelle", "String"));


			setAssistantPlanComptableDG();
			setResultatTBV(new JTableViewCocktail(lesColumns, getResultatDG(),new Dimension(100, 100), JTable.AUTO_RESIZE_LAST_COLUMN));
			getMonAssistantPlanComptableNib().getJPanelResultat().add(getResultatTBV());

			// multi ou mono selection dans les tbv
			getResultatTBV().getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// le cablage des  objets
			getMonAssistantPlanComptableNib().getJButtonCocktailAnnuler().addDelegateActionListener(this, METHODE_ACTION_ANNULER);
			getMonAssistantPlanComptableNib().getJButtonCocktailSelectionner().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
			getMonAssistantPlanComptableNib().getJButtonCocktailFiltrer().addDelegateActionListener(this, METHODE_ACTION_FILTRER);

			// les images des objets
			getMonAssistantPlanComptableNib().getJButtonCocktailSelectionner().setIcone(IconeCocktail.VALIDER);//"valider16");
			getMonAssistantPlanComptableNib().getJButtonCocktailAnnuler().setIcone(IconeCocktail.REFUSER);//"close_view");
			getMonAssistantPlanComptableNib().getJButtonCocktailFiltrer().setIcone(IconeCocktail.FILTRER);//"linkto_help");

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonAssistantPlanComptableNib());
		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}

		getResultatTBV().refresh();
	}

	private void setAssistantPlanComptableDG() {
		
	}

	// methodes cablage
	public void actionAnnuler() {
		//	trace(METHODE_ACTION_FERMER);
		rechercheAnnuler();
		getMonAssistantPlanComptableNib().getJTextFieldCocktailCompte().setText("");
		getResultatDG().removeAllObjects();
		getResultatTBV().refresh();	
		masquerFenetre();
	}

	// methodes cablage
	public void actionSelectionner() {
		//	trace(METHODE_ACTION_FERMER);
		rechercheTerminer();
		masquerFenetre();
	}

	// methodes cablage
	public void actionFiltrer() {
		//	trace(METHODE_ACTION_FERMER);
		setAssistantPlanComptableDG();
	}

	private NSArray getResultat(){
		NSArray resltat = new NSArray(getResultatTBV().getSelectedObjects());
		getResultatDG().removeAllObjects();
		getResultatTBV().refresh();	
		return resltat;
	}
	
	// methode pour le parentControleur
	private void rechercheAnnuler(){
		parentControleur.assistantsAnnuler(this);
	}

	private void rechercheTerminer(){
		parentControleur.assistantsTerminer(this);
	}
	
	private AssistantPlanComptableNib getMonAssistantPlanComptableNib() {
		return monAssistantPlanComptableNib;
	}

	private void setMonAssistantPlanComptableNib(
			AssistantPlanComptableNib monAssistantPlanComptableNib) {
		this.monAssistantPlanComptableNib = monAssistantPlanComptableNib;
	}

	private NSMutableArrayDisplayGroup getResultatDG() {
		return resultatDG;
	}

	private void setResultatDG(NSMutableArrayDisplayGroup resultatDG) {
		this.resultatDG = resultatDG;
	}

	private JTableViewCocktail getResultatTBV() {
		return resultatTBV;
	}

	private void setResultatTBV(JTableViewCocktail resultatTBV) {
		this.resultatTBV = resultatTBV;
	}
}
