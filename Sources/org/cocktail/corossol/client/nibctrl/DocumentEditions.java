package org.cocktail.corossol.client.nibctrl;


public class DocumentEditions	
{
	 String edition="";
	 String description="";
	 boolean xls = false;
	 boolean pdf = false;
	 String jasper = "";
	 String sql = "";
	 String projection="";
	 String projection_type="";
	 
	public DocumentEditions(String edition, String description, boolean xls, boolean pdf,String jasper,String sql,String projection,String projection_type) {
		super();
		this.edition = edition;
		this.description = description;
		this.xls = xls;
		this.pdf = pdf;
		this.jasper = jasper;
		this.sql = sql;
		this.projection = projection;
		this.projection_type = projection_type;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getEdition() {
		return edition;
	}
	
	public void setEdition(String edition) {
		this.edition = edition;
	}
	
	public boolean isPdf() {
		return pdf;
	}
	
	public void setPdf(boolean pdf) {
		this.pdf = pdf;
	}
	
	public boolean isXls() {
		return xls;
	}
	
	public void setXls(boolean xls) {
		this.xls = xls;
	}
	
	public String getJasper() {
		return jasper;
	}
	
	public void setJasper(String jasper) {
		this.jasper = jasper;
	}

	public String getProjection() {
		return projection;
	}

	public void setProjection(String projection) {
		this.projection = projection;
	}

	public String getProjection_type() {
		return projection_type;
	}

	public void setProjection_type(String projection_type) {
		this.projection_type = projection_type;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}




}
