package org.cocktail.corossol.client.nibctrl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.finder.FinderGrhum;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.ToolsSwing;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.UtilCtrl;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.MyExercice;
import org.cocktail.corossol.client.eof.procedure.ProcedureTicketImpression;
import org.cocktail.corossol.client.finder.FinderComptable;
import org.cocktail.corossol.client.nib.ComptableInventaireInspecteurNib;
import org.cocktail.corossol.client.nib.DepenseInspecteurNib;
import org.cocktail.corossol.client.nib.ImpressionsNib;

import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ImpressionsNibCtrl extends NibCtrl {

	// inspecteurs 
	private static final String FENETRE_INSPECTEUR_INVENTAIRE = "Fenetre de gestion du numero d'inventaire comptable";
	private static final String FENETRE_INSPECTEUR_DEPENSE = "Fenetre de gestion du lien inventaire - depense";

	ComptableInventaireInspecteurNib monComptableInventaireInspecteurNib=null;
	ComptableInventaireInspecteurNibCtrl monComptableInventaireInspecteurNibCtrl=null;

	DepenseInspecteurNib monDepenseInspecteurNib=null;
	DepenseInspecteurNibCtrl monDepenseInspecteurNibCtrl=null;

	// le nib
	private ImpressionsNib monImpressionsNib = null;

	public ImpressionsNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setWithLogs(false);

		// creation de l'assistant
		setMonComptableInventaireInspecteurNibCtrl(new ComptableInventaireInspecteurNibCtrl(ctrl, alocation_x , alocation_y, 610 , 620));
		setMonComptableInventaireInspecteurNib( new ComptableInventaireInspecteurNib());
		getMonComptableInventaireInspecteurNibCtrl().creationFenetre(getMonComptableInventaireInspecteurNib(),ToolsSwing.formaterStringU(FENETRE_INSPECTEUR_INVENTAIRE),this);
		getMonComptableInventaireInspecteurNib().setPreferredSize(new java.awt.Dimension(610, 620));
		getMonComptableInventaireInspecteurNib().setSize(610, 620);

		// creation de l'assistant Depense
		setMonDepenseInspecteurNibCtrl(new DepenseInspecteurNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonDepenseInspecteurNib( new DepenseInspecteurNib());
		getMonDepenseInspecteurNibCtrl().creationFenetre(getMonDepenseInspecteurNib(),ToolsSwing.formaterStringU(FENETRE_INSPECTEUR_DEPENSE),this);
		getMonDepenseInspecteurNib().setPreferredSize(new java.awt.Dimension(600,500));
		getMonDepenseInspecteurNib().setSize(600,500);
		getMonDepenseInspecteurNibCtrl().activerRechercheCommande(true);
	}

	public void creationFenetre(ImpressionsNib leImpressionsNib, String title) {
		super.creationFenetre(leImpressionsNib, title);
		setMonImpressionsNib(leImpressionsNib);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
	}

	public void afficherFenetre() {
		super.afficherFenetre();
	}

	private void bindingAndCustomization() {

		getMonImpressionsNib().initTableView();

		try {
			// popUpExercice ...
			getMonImpressionsNib().getJComboBoxCocktailExercice().removeAllItems();
			NSArray array=FinderGrhum.findLesExercices(app);
			for (int i=0;i<array.count();i++) {
				MyExercice myExercice=new MyExercice((EOExercice)array.objectAtIndex(i));
				getMonImpressionsNib().getJComboBoxCocktailExercice().addItem(myExercice);
			}

			setImpressionsDG();

			// multi ou mono selection dans les tbv
			getMonImpressionsNib().getMaTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

			// le cablage des  objets
			getMonImpressionsNib().getJButtonCocktailFermer().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionFermer();
				} });

			getMonImpressionsNib().getJButtonCocktailImprimer().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionImprimer();
				} });

			getMonImpressionsNib().getJButtonCocktailEtiquette().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionEtiquette();
				} });

			getMonImpressionsNib().getJButtonCocktailCbPhysique().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionCbPhysique();
				} });

			getMonImpressionsNib().getJButtonCocktailModifier().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionModifier();
				} });

			getMonImpressionsNib().getJButtonCocktailRecherche().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionRechercher();
				} });

			getMonImpressionsNib().jTextFieldCocktailInventaire.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) { actionRechercher(); } });
			getMonImpressionsNib().jTextFieldCocktailCommande.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) { actionRechercher(); } });
			getMonImpressionsNib().jTextFieldCocktailImputation.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) { actionRechercher(); } });

			getMonImpressionsNib().getJButtonCocktailDepense().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionDepense();
				} });

			// les images des objets
			getMonImpressionsNib().getJButtonCocktailFermer().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.FERMER));//"close_view");
			getMonImpressionsNib().getJButtonCocktailImprimer().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.IMPRIMER));//"print_edit");
			getMonImpressionsNib().getJButtonCocktailEtiquette().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.IMPRIMER));//"print_edit");
			getMonImpressionsNib().getJButtonCocktailModifier().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.REFRESH));//"refresh");
			getMonImpressionsNib().getJButtonCocktailRecherche().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.INSPECTER));//"linkto_help");
			getMonImpressionsNib().getJButtonCocktailDepense().setIcon((ImageIcon)new EOClientResourceBundle().getObject(IconeCocktail.PAGE));//"page_obj");

			//sera modal lors d une transaction 
			app.addLesPanelsModal(getMonImpressionsNib());

			// obseration des changements de selection de lexercice :
			((ApplicationCorossol) app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}

		if (getMonImpressionsNib().getMaTable() != null)
			getMonImpressionsNib().refresh();	
	}

	private void setImpressionsDG() {
		getMonImpressionsNib().setInventaires(FinderComptable.findLesInventairesComptable(app,
				((MyExercice)getMonImpressionsNib().getJComboBoxCocktailExercice().getSelectedItem()).exercice(),
				getMonImpressionsNib().jTextFieldCocktailInventaire.getText().toString(),
				getMonImpressionsNib().jTextFieldCocktailCommande.getText().toString(),
				getMonImpressionsNib().jTextFieldCocktailImputation.getText().toString()));

		if (getMonImpressionsNib().getMaTable() != null)
			getMonImpressionsNib().refresh();	
	}

	// actions
	public void actionRechercher(){
		setImpressionsDG();
	}

	public void actionEtiquette(){
		//System.out.println("actionEtiquette init ");
		try {
			Integer ticket=ProcedureTicketImpression.enregistrer((ApplicationCorossol)app, getMonImpressionsNib().inventairesComptableSelectionnes());
			
			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(ticket,"TICKET");
			parameters.takeValueForKey(app.getApplicationParametre("LOGO"),"LOGO");
			parameters.takeValueForKey(app.getApplicationParametre("REP_BASE_JASPER_PATH"),"SUBREPORT_DIR");

			if(getMonImpressionsNib().getJCheckBoxA4().isSelected())
				app.getToolsCocktailReports().imprimerReportParametres("etiquettes.jasper", parameters, "etiquettes"+UtilCtrl.lastModif());
			else
				app.getToolsCocktailReports().imprimerReportParametres("etiquettesBande.jasper", parameters, "etiquettesBande"+UtilCtrl.lastModif());

			app.getToolsCocktailReports().imprimerReportParametres("EtiquettesInformations.jasper", parameters, "EtiquettesInformations"+UtilCtrl.lastModif());

		} catch (Exception e) {
			System.out.println("actionEtiquette : "+e);
		}
	}

	public void actionCbPhysique(){
		try {
			Integer ticket=ProcedureTicketImpression.enregistrer((ApplicationCorossol)app, getMonImpressionsNib().inventairesComptableSelectionnes());
			
			NSMutableDictionary parameters = new NSMutableDictionary();
			parameters.takeValueForKey(ticket,"TICKET");
			parameters.takeValueForKey(app.getApplicationParametre("LOGO"),"LOGO");
			parameters.takeValueForKey(app.getApplicationParametre("REP_BASE_JASPER_PATH"),"SUBREPORT_DIR");

			if(getMonImpressionsNib().getJCheckBoxA4().isSelected())
				app.getToolsCocktailReports().imprimerReportParametres("etiquettes.pau.jasper", parameters, "etiquettes"+UtilCtrl.lastModif());
			else
				app.getToolsCocktailReports().imprimerReportParametres("etiquettesBande.pau.jasper", parameters, "etiquettesBande"+UtilCtrl.lastModif());
		} catch (Exception e) {
			System.out.println("actionCbPhysique : "+e);
		}
	}

	public void actionImprimer(){
		for (int i = 0; i < getMonImpressionsNib().inventairesComptableSelectionnes().count(); i++) {
			try {
				NSMutableDictionary parameters = new NSMutableDictionary();
				parameters.takeValueForKey(new Integer(((EOInventaireComptable)getMonImpressionsNib().inventairesComptableSelectionnes().objectAtIndex(i)).cleInventaireComptable().clicIdBis().intValue()),"CLIC_ID");
				parameters.takeValueForKey(app.getApplicationParametre("REP_BASE_JASPER_PATH"),"SUBREPORT_DIR");

				app.getToolsCocktailReports().imprimerReportParametres("fiche_inventaire.jasper", parameters, "fiche_inventaire"+UtilCtrl.lastModif());

			} catch (Exception e) {
				System.out.println("actionImprimer OUPS"+e);
			}
		}
	}

	public void actionModifier(){
		if (getMonImpressionsNib().inventairesComptableSelectionnes().count() == 1)
			getMonComptableInventaireInspecteurNibCtrl().afficherFenetreModification(
					(EOInventaireComptable)getMonImpressionsNib().inventairesComptableSelectionnes().objectAtIndex(0));
	}

	public void actionDepense(){
		if (getMonImpressionsNib().inventairesComptableSelectionnes().count() != 0)
			getMonDepenseInspecteurNibCtrl().afficherFenetre(getMonImpressionsNib().inventairesComptableSelectionnes());
	}

	public void actionFermer() {
		masquerFenetre();
	}

	public void changementExercice() {
		actionRechercher();
	}

	private ImpressionsNib getMonImpressionsNib() {
		return monImpressionsNib;
	}

	private void setMonImpressionsNib(ImpressionsNib monImpressionsNib) {
		this.monImpressionsNib = monImpressionsNib;
	}

	private ComptableInventaireInspecteurNib getMonComptableInventaireInspecteurNib() {
		return monComptableInventaireInspecteurNib;
	}

	private void setMonComptableInventaireInspecteurNib(
			ComptableInventaireInspecteurNib monComptableInventaireInspecteurNib) {
		this.monComptableInventaireInspecteurNib = monComptableInventaireInspecteurNib;
	}

	private ComptableInventaireInspecteurNibCtrl getMonComptableInventaireInspecteurNibCtrl() {
		return monComptableInventaireInspecteurNibCtrl;
	}

	private void setMonComptableInventaireInspecteurNibCtrl(
			ComptableInventaireInspecteurNibCtrl monComptableInventaireInspecteurNibCtrl) {
		this.monComptableInventaireInspecteurNibCtrl = monComptableInventaireInspecteurNibCtrl;
	}

	private DepenseInspecteurNib getMonDepenseInspecteurNib() {
		return monDepenseInspecteurNib;
	}

	private void setMonDepenseInspecteurNib(
			DepenseInspecteurNib monDepenseInspecteurNib) {
		this.monDepenseInspecteurNib = monDepenseInspecteurNib;
	}

	private DepenseInspecteurNibCtrl getMonDepenseInspecteurNibCtrl() {
		return monDepenseInspecteurNibCtrl;
	}

	private void setMonDepenseInspecteurNibCtrl(
			DepenseInspecteurNibCtrl monDepenseInspecteurNibCtrl) {
		this.monDepenseInspecteurNibCtrl = monDepenseInspecteurNibCtrl;
	}
}
