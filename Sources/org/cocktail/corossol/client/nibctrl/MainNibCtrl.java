/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.corossol.client.nibctrl;

import javax.swing.ImageIcon;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swingfinder.SwingFinderInterface;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.ToolsSwing;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.Privileges;
import org.cocktail.corossol.client.nib.ActifNib;
import org.cocktail.corossol.client.nib.CommandeNib;
import org.cocktail.corossol.client.nib.ComptableNib;
import org.cocktail.corossol.client.nib.EditionsFinancementsNib;
import org.cocktail.corossol.client.nib.ImpressionsNib;
import org.cocktail.corossol.client.nib.IntegrationNib;
import org.cocktail.corossol.client.nib.LivraisonNib;
import org.cocktail.corossol.client.nib.MainNib;
import org.cocktail.corossol.client.nib.PhysiqueNib;

import com.webobjects.eoapplication.client.EOClientResourceBundle;

public class MainNibCtrl  extends NibCtrl implements SwingFinderInterface {

	//methodes
	private static final String METHODE_QUIT = "quit";
	private static final String METHODE_AFFICHER_FENETRE = "afficherFenetre";

	//libelles
	private static final String FENETRE_DE_SELECTION_DE_COMMANDE = "Fenetre de selection d'une commande ...";
	private static final String FENETRE_DE_LIVRAISON = "Fenetre de gestion des livraisons ...";
	private static final String	FENETRE_INVENTAIRE_PHYSIQUE = "Fenetre de gestion de l'inventaire physique ...";
	private static final String	FENETRE_INVENTAIRE_COMPTABLE = "Fenetre de gestion de l'inventaire comptable ...";
	private static final String	FENETRE_DE_SORTIE_INVENTAIRE = "Fenetre de gestion de sortie d'inventaire ...";
	private static final String	FENETRE_D_IMPRESSION = "Fenetre d'impressions ...";
	private static final String	FENETRE_ACTIF = "Fenetre de suivi de l'actif ...";
	private static final String FENETRE_INTEGRATION = "Fenetre d'integration d'inventaires hors module carambole...";
	private static final String EDITIONS_FINANCEMENTS = "Fenetre d'editions des financements";

	private MainNib monMainNib = null;

	private CommandeNib monCommandeNib = null;
	private CommandeNibCtrl monCommandeNibCtrl = null;

	private LivraisonNib monLivraisonNib = null;
	private LivraisonNibCtrl monLivraisonNibCtrl = null;

	private PhysiqueNib monPhysiqueNib = null;
	private PhysiqueNibCtrl monPhysiqueNibCtrl = null;

	private IntegrationNib monIntegrationNib = null;
	private IntegrationNibCtrl monIntegrationNibCtrl = null;
	
	private ImpressionsNib monImpressionsNib = null;
	private ImpressionsNibCtrl monImpressionsNibCtrl = null;
	
	private EditionsFinancementsNib monEditionsFinancementsNib = null;
	private EditionsFinancementsNibCtrl monEditionsFinancementsNibCtrl = null;
	
	private ActifNib monActifNib = null;
	private ActifNibCtrl monActifNibCtrl = null;
	 
	private SortieNibCtrl monSortieNibCtrl = null;
	private OperationsRegularisationNibCtrl monOperationsRegularisationNibCtrl = null;

	private ComptableNib monComptableNib = null;
	private ComptableNibCtrl monComptableNibCtrl = null;

	private SwingFinderExercice monSwingFinderExercice = null;


	public MainNibCtrl (ApplicationCocktail ctrl,int alocation_x , int alocation_y,int ataille_x , int ataille_y) {
		super( ctrl, alocation_x , alocation_y, ataille_x , ataille_y);

		monSwingFinderExercice = new SwingFinderExercice(ctrl,this,alocation_x , alocation_y, 260,160,true);

		// creation du nib commande
		setMonCommandeNibCtrl(new CommandeNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonCommandeNib( new CommandeNib());
		getMonCommandeNibCtrl().creationFenetre(getMonCommandeNib(),ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_DE_COMMANDE));
		getMonCommandeNib().setPreferredSize(new java.awt.Dimension(600,400));
		getMonCommandeNib().setSize(600,400);

		// creation du nib livraison
		setMonLivraisonNibCtrl(new LivraisonNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonLivraisonNib ( new LivraisonNib ());
		getMonLivraisonNibCtrl().creationFenetre(getMonLivraisonNib(),ToolsSwing.formaterStringU(FENETRE_DE_LIVRAISON));
		getMonLivraisonNib().setPreferredSize(new java.awt.Dimension(800, 600));
		getMonLivraisonNib().setSize(800, 600);

		// creation du nib d inventaire physique
		setMonPhysiqueNibCtrl(new PhysiqueNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonPhysiqueNib ( new PhysiqueNib());
		getMonPhysiqueNibCtrl().creationFenetre(getMonPhysiqueNib(),ToolsSwing.formaterStringU(FENETRE_INVENTAIRE_PHYSIQUE));
		getMonPhysiqueNib().setPreferredSize(new java.awt.Dimension(800, 600));
		getMonPhysiqueNib().setSize(800, 600);

		// creation du nib d inventaire comptable
		setMonComptableNibCtrl(new ComptableNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonComptableNib ( new ComptableNib ());
		getMonComptableNibCtrl().creationFenetre(getMonComptableNib(),ToolsSwing.formaterStringU(FENETRE_INVENTAIRE_COMPTABLE));
		getMonComptableNib().setPreferredSize(new java.awt.Dimension(800, 600));
		getMonComptableNib().setSize(800, 600);


		setMonOperationsRegularisationNibCtrl(new OperationsRegularisationNibCtrl(ctrl));

		// creation du nib des sorties
		setMonSortieNibCtrl(new SortieNibCtrl(ctrl));

		// creation du nib des reprises
		setMonIntegrationNibCtrl(new IntegrationNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonIntegrationNib ( new IntegrationNib ());
		getMonIntegrationNibCtrl().creationFenetre(getMonIntegrationNib(),ToolsSwing.formaterStringU(FENETRE_INTEGRATION));
		getMonIntegrationNib().setPreferredSize(new java.awt.Dimension(800, 750));
		getMonIntegrationNib().setSize(800, 750);


		// creation du nib des impressions
		setMonImpressionsNibCtrl(new ImpressionsNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonImpressionsNib ( new ImpressionsNib ());
		getMonImpressionsNibCtrl().creationFenetre(getMonImpressionsNib(),ToolsSwing.formaterStringU(FENETRE_D_IMPRESSION));
		getMonImpressionsNib().setPreferredSize(new java.awt.Dimension(800, 600));
		getMonImpressionsNib().setSize(800, 600);
		
		// creation du nib Editions de financements
		setMonEditionsFinancementsNibCtrl (new EditionsFinancementsNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonEditionsFinancementsNib ( new EditionsFinancementsNib());
		getMonEditionsFinancementsNibCtrl().creationFenetre(getMonEditionsFinancementsNib(),ToolsSwing.formaterStringU(EDITIONS_FINANCEMENTS));
//		getMonEditionsFinancementsNib().setPreferredSize(new java.awt.Dimension(810, 355));
//		getMonEditionsFinancementsNib().setSize(810, 355);
		
		// creation du nib ACTIF
		setMonActifNibCtrl(new ActifNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonActifNib ( new ActifNib ());
		getMonActifNibCtrl().creationFenetre(getMonActifNib(),ToolsSwing.formaterStringU(FENETRE_ACTIF));
		getMonActifNib().setPreferredSize(new java.awt.Dimension(800, 350));
		getMonActifNib().setSize(800, 350);
		
	}

	public void creationFenetre(MainNib leMainNib,String title){
		super.creationFenetre( leMainNib,title);
		setMonMainNib(leMainNib);
		bindingAndCustomization();
		bindingAndPrivileges();
		getFrameMain().pack();
		getFrameMain().setVisible(false);
	}

//	Initialisation des boutons en swing
	private void bindingAndCustomization() {
		// Chargement des icones pour les boutons
		try {
			EOClientResourceBundle leBundle = new EOClientResourceBundle();

//			 cablage des Inspecteurs
			monSwingFinderExercice.relierBouton((JButtonCocktail) getMonMainNib().getJButtonCocktailExercice());
			
			// cablage des buttons
			((JButtonCocktail) getMonMainNib().getJButtonCocktailCommande()).addDelegateActionListener(getMonCommandeNibCtrl(),METHODE_AFFICHER_FENETRE);	
			((JButtonCocktail) getMonMainNib().getJButtonCocktailLivraison()).addDelegateActionListener(getMonLivraisonNibCtrl(),METHODE_AFFICHER_FENETRE);
			((JButtonCocktail) getMonMainNib().getJButtonCocktailInvPhysique()).addDelegateActionListener(getMonPhysiqueNibCtrl(),METHODE_AFFICHER_FENETRE);			
			((JButtonCocktail) getMonMainNib().getJButtonCocktailInvComptable()).addDelegateActionListener(getMonComptableNibCtrl(),METHODE_AFFICHER_FENETRE);
			((JButtonCocktail) getMonMainNib().getJButtonCocktailImpressions()).addDelegateActionListener(getMonImpressionsNibCtrl(),METHODE_AFFICHER_FENETRE);
			((JButtonCocktail) getMonMainNib().getJButtonCocktailReprise()).addDelegateActionListener(getMonIntegrationNibCtrl(),METHODE_AFFICHER_FENETRE);
			((JButtonCocktail) getMonMainNib().getJButtonCocktailActif()).addDelegateActionListener(getMonActifNibCtrl(),METHODE_AFFICHER_FENETRE);
			((JButtonCocktail) getMonMainNib().getjButtonCocktailEditionsFinancements()).addDelegateActionListener(getMonEditionsFinancementsNibCtrl(),METHODE_AFFICHER_FENETRE);
			((JButtonCocktail) getMonMainNib().getJButtonCocktailSortie()).addDelegateActionListener(getMonSortieNibCtrl(),METHODE_AFFICHER_FENETRE);
			((JButtonCocktail) getMonMainNib().getjButtonCocktailSortie1()).addDelegateActionListener(getMonOperationsRegularisationNibCtrl(),METHODE_AFFICHER_FENETRE);

			((JButtonCocktail) getMonMainNib().getJButtonCocktailQuitter()).addDelegateActionListener( app,METHODE_QUIT);
			getMonMainNib().getJTextPaneBdJvm().setText("   "+app.getToolsCocktailEOF().getConnectionBaseDonnees()+"\n   java : "+app.getToolsCocktailSystem().systemClientJVM());

			// les images des buttons
			getMonMainNib().getJButtonCocktailExercice().setIcone(IconeCocktail.TABLEAU);
			getMonMainNib().getJButtonCocktailCommande().setIcone(IconeCocktail.RECHERCHER);
			getMonMainNib().getJButtonCocktailLivraison().setIcone("livraison");
			getMonMainNib().getJButtonCocktailInvPhysique().setIcone("invphysique");
			getMonMainNib().getJButtonCocktailInvComptable().setIcone("invcomptable");
			getMonMainNib().getJButtonCocktailImpressions().setIcone(IconeCocktail.IMPRIMER);
			getMonMainNib().getjButtonCocktailEditionsFinancements().setIcone(IconeCocktail.IMPRIMER);
			getMonMainNib().getJButtonCocktailReprise().setIcone("reprise");
			getMonMainNib().getJButtonCocktailSortie().setIcone("sortie");
			getMonMainNib().getJButtonCocktailActif().setIcone(IconeCocktail.TRAITEMENT_BD);//"newconnect_wiz");
			getMonMainNib().getJButtonCocktailQuitter().setIcone(IconeCocktail.QUITTER);//"quitter");
			getMonMainNib().getjButtonCocktailSortie1().setIcone("regularisation");

			// les images des labels
			getMonMainNib().getJLabelIcone().setIcon((ImageIcon)leBundle.getObject("corosol01"));
			getMonMainNib().getJLabelIconeBd().setIcon((ImageIcon)leBundle.getObject("database"));

			// obseration des changements de selection de commande :
			((ApplicationCorossol)app).getObserveurCommandeSelection().addObserverForMessage(this, "changementCommande");

			
			//sera modal lors d une transaction 
			app.addLesPanelsModal(monMainNib);
		}
		catch(Exception e) {
			app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(),true);
		}
	}

	private void bindingAndPrivileges() {

		//System.out.println("bindingAndPrivileges..");
		((ApplicationCorossol)app).getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(getMonMainNib().getJButtonCocktailLivraison(),Privileges.INV1);
		((ApplicationCorossol)app).getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(getMonMainNib().getJButtonCocktailInvPhysique(),Privileges.INV2);
		((ApplicationCorossol)app).getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(getMonMainNib().getJButtonCocktailInvComptable(),Privileges.INV3);
		((ApplicationCorossol)app).getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(getMonMainNib().getJButtonCocktailSortie(),Privileges.INV4);
		((ApplicationCorossol)app).getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(getMonMainNib().getJButtonCocktailReprise(),Privileges.INV5);
		((ApplicationCorossol)app).getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(getMonMainNib().getJButtonCocktailImpressions(),Privileges.INV6);
		((ApplicationCorossol)app).getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(getMonMainNib().getJButtonCocktailActif(),Privileges.INV7);
		((ApplicationCorossol)app).getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(getMonMainNib().getjButtonCocktailSortie1(),Privileges.INVREGUL);
		((ApplicationCorossol)app).getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(getMonMainNib().getjButtonCocktailEditionsFinancements(),Privileges.INVTITRE);
		//System.out.println("bindingAndPrivileges..");
		changementCommande();
	}

	public void changementCommande(){
//		if (((ApplicationCorosol)app).getCurrentCommande() == null)
//		{
//			getMonMainNib().getJButtonCocktailLivraison().setEnabled(false);
//			getMonMainNib().getJButtonCocktailInvPhysique().setEnabled(false);
//			getMonMainNib().getJButtonCocktailInvComptable().setEnabled(false);
//		}
//		else
//		{			
//			getMonMainNib().getJButtonCocktailLivraison().setEnabled(true);
//			getMonMainNib().getJButtonCocktailInvPhysique().setEnabled(true);
//			getMonMainNib().getJButtonCocktailInvComptable().setEnabled(true);
//		}
	}

	public void setInfos (String s){
		getMonMainNib().getJLabelInfo().setText(s);
	}


	void setMonMainNib(MainNib monMainNib) {
		this.monMainNib = monMainNib;
	}


	MainNib getMonMainNib() {
		return monMainNib;
	}


	private LivraisonNib getMonLivraisonNib() {
		return monLivraisonNib;
	}


	private void setMonLivraisonNib(LivraisonNib monLivraisonNib) {
		this.monLivraisonNib = monLivraisonNib;
	}


	private LivraisonNibCtrl getMonLivraisonNibCtrl() {
		return monLivraisonNibCtrl;
	}


	private void setMonLivraisonNibCtrl(LivraisonNibCtrl monLivraisonNibCtrl) {
		this.monLivraisonNibCtrl = monLivraisonNibCtrl;
	}


	private PhysiqueNib getMonPhysiqueNib() {
		return monPhysiqueNib;
	}


	private void setMonPhysiqueNib(PhysiqueNib monPhysiqueNib) {
		this.monPhysiqueNib = monPhysiqueNib;
	}


	private PhysiqueNibCtrl getMonPhysiqueNibCtrl() {
		return monPhysiqueNibCtrl;
	}


	private void setMonPhysiqueNibCtrl(PhysiqueNibCtrl monPhysiqueNibCtrl) {
		this.monPhysiqueNibCtrl = monPhysiqueNibCtrl;
	}


	private ComptableNib getMonComptableNib() {
		return monComptableNib;
	}


	private void setMonComptableNib(ComptableNib monComptableNib) {
		this.monComptableNib = monComptableNib;
	}


	private ComptableNibCtrl getMonComptableNibCtrl() {
		return monComptableNibCtrl;
	}


	private void setMonComptableNibCtrl(ComptableNibCtrl monComptableNibCtrl) {
		this.monComptableNibCtrl = monComptableNibCtrl;
	}


	private CommandeNib getMonCommandeNib() {
		return monCommandeNib;
	}


	private void setMonCommandeNib(CommandeNib monCommandeNib) {
		this.monCommandeNib = monCommandeNib;
	}


	private CommandeNibCtrl getMonCommandeNibCtrl() {
		return monCommandeNibCtrl;
	}


	private void setMonCommandeNibCtrl(CommandeNibCtrl monCommandeNibCtrl) {
		this.monCommandeNibCtrl = monCommandeNibCtrl;
	}
	
	
	
	public void swingFinderAnnuler(Object sender) {
	}


	public void swingFinderTerminer(Object sender) {
		
		if(sender == monSwingFinderExercice){
			app.setCurrentExercice((EOExercice) monSwingFinderExercice.getResultat().objectAtIndex(0));
			getMonMainNib().getJLabelExercice().setText(app.getCurrentExercice().exeExercice().toString());
			((ApplicationCorossol)app).getObserveurExerciceSelection().sendMessageToObserver(this,"changementExercice");
		}
	}


	private ImpressionsNib getMonImpressionsNib() {
		return monImpressionsNib;
	}


	private void setMonImpressionsNib(ImpressionsNib monImpressionsNib) {
		this.monImpressionsNib = monImpressionsNib;
	}


	private ImpressionsNibCtrl getMonImpressionsNibCtrl() {
		return monImpressionsNibCtrl;
	}


	private void setMonImpressionsNibCtrl(ImpressionsNibCtrl monImpressionsNibCtrl) {
		this.monImpressionsNibCtrl = monImpressionsNibCtrl;
	}


	private ActifNibCtrl getMonActifNibCtrl() {
		return monActifNibCtrl;
	}


	private void setMonActifNibCtrl(ActifNibCtrl monActifNibCtrl) {
		this.monActifNibCtrl = monActifNibCtrl;
	}


	private ActifNib getMonActifNib() {
		return monActifNib;
	}


	private void setMonActifNib(ActifNib monActifNib) {
		this.monActifNib = monActifNib;
	}


	private IntegrationNib getMonIntegrationNib() {
		return monIntegrationNib;
	}


	private void setMonIntegrationNib(IntegrationNib monIntegrationNib) {
		this.monIntegrationNib = monIntegrationNib;
	}


	private IntegrationNibCtrl getMonIntegrationNibCtrl() {
		return monIntegrationNibCtrl;
	}


	private void setMonIntegrationNibCtrl(IntegrationNibCtrl monIntegrationNibCtrl) {
		this.monIntegrationNibCtrl = monIntegrationNibCtrl;
	}
	
	private SortieNibCtrl getMonSortieNibCtrl() {
		return monSortieNibCtrl;
	}

	private void setMonSortieNibCtrl(SortieNibCtrl monSortieNibCtrl) {
		this.monSortieNibCtrl = monSortieNibCtrl;
	}

	private OperationsRegularisationNibCtrl getMonOperationsRegularisationNibCtrl() {
		return monOperationsRegularisationNibCtrl;
	}

	private void setMonOperationsRegularisationNibCtrl(OperationsRegularisationNibCtrl monOperationsRegularisationNibCtrl) {
		this.monOperationsRegularisationNibCtrl = monOperationsRegularisationNibCtrl;
	}

	private EditionsFinancementsNib getMonEditionsFinancementsNib() {
		return monEditionsFinancementsNib;
	}

	private void setMonEditionsFinancementsNib(
			EditionsFinancementsNib monEditionsFinancementsNib) {
		this.monEditionsFinancementsNib = monEditionsFinancementsNib;
	}

	private EditionsFinancementsNibCtrl getMonEditionsFinancementsNibCtrl() {
		return monEditionsFinancementsNibCtrl;
	}

	private void setMonEditionsFinancementsNibCtrl(
			EditionsFinancementsNibCtrl monEditionsFinancementsNibCtrl) {
		this.monEditionsFinancementsNibCtrl = monEditionsFinancementsNibCtrl;
	}

}
