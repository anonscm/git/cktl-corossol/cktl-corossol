package org.cocktail.corossol.client.nibctrl;


import java.awt.event.ActionEvent;
import java.math.BigDecimal;

import javax.swing.ImageIcon;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.IconeCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.swingfinder.SwingFinderInterface;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.Privileges;
import org.cocktail.corossol.client.UtilCtrl;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv;
import org.cocktail.corossol.client.eof.metier.EOOrigineFinancement;
import org.cocktail.corossol.client.eof.metier.EOTitre;
import org.cocktail.corossol.client.finder.FinderComptable;
import org.cocktail.corossol.client.nib.OriginesFinancementNib;
import org.cocktail.corossol.client.zutil.FormattedFieldProvider;
import org.cocktail.corossol.common.eof.repartition.IInventaireComptableOrig;
import org.cocktail.corossol.common.eof.repartition.InventaireComptableOrigHelper;
import org.cocktail.corossol.common.zutil.CalculMontantUtil;

import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.foundation.NSArray;

public class OriginesFinancementNibCtrl extends NibCtrl implements SwingFinderInterface{
	
	private static final String TITRE = "Définition financement";

	// Etat de la fenêtre
	private static final String CREATION = "CREATION";
	private static final String MODIFICATION = "MODIFICATION";

	// le nib
	private OriginesFinancementNib monOriginesFinancementNib = null;

	private EOInventaireComptable currentInventaireComptable = null;
	private EOInventaireComptableOrig currentInventaireComptableOrig=null;
	private EOTitre currentTitre = null;
	private NibCtrl parentControleur = null;
	private TitreNibCtrl titreNibCtrl = null;
	private String currentAction = null;
	
	public OriginesFinancementNibCtrl(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
		super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);

		setMonOriginesFinancementNib(new OriginesFinancementNib());
		monOriginesFinancementNib.setPreferredSize(new java.awt.Dimension(ataille_x, ataille_y));
		monOriginesFinancementNib.setSize(ataille_x, ataille_y);

		titreNibCtrl = new TitreNibCtrl(ctrl, alocation_x, alocation_y, 610, 620);
		titreNibCtrl.creationFenetre(this);
		
		setWithLogs(false);
	}

	public void creationFenetre(NibCtrl parentControleur) {
		super.creationFenetre(monOriginesFinancementNib, TITRE);
		setNibCtrlLocation(LOCATION_MIDDLE);
		bindingAndCustomization();
		this.parentControleur =  parentControleur;
	}

	public void majOrigineFinancement(EOExercice exercice) {
		getMonOriginesFinancementNib().getJComboBoxOrigineFinancement().removeAllItems();
		if (exercice == null) {
			return;
		}
		NSArray array = FinderComptable.findLesFinancements(app, exercice);
		for (int i = 0; i < array.count(); i++) {
			getMonOriginesFinancementNib().getJComboBoxOrigineFinancement().addItem(array.objectAtIndex(i));
		}
	}
    
	public void afficherFenetrePourModification(EOInventaireComptable tmpInventaireComptable, EOInventaireComptableOrig icorgf) {
		if (tmpInventaireComptable == null || icorgf == null) {
			return;
		}

		majOrigineFinancement(tmpInventaireComptable.exercice());

		BigDecimal pourcentage = BigDecimal.ZERO.add(icorgf.icorPourcentage());
		BigDecimal montant = icorgf.icorMontant();

		if (montant == null) {
			montant = CalculMontantUtil.montantDuPourcentage(tmpInventaireComptable.invcMontantAmortissableAvecOrvs(), pourcentage);
		}

		currentInventaireComptable = tmpInventaireComptable;
		currentInventaireComptableOrig = icorgf;

		getMonOriginesFinancementNib().getJTextFieldPourcentage().setValue(pourcentage);
		getMonOriginesFinancementNib().getjTextFieldMontant().setValue(montant);
		getMonOriginesFinancementNib().getJComboBoxOrigineFinancement().setSelectedItem(icorgf.origineFinancement());
		setTitre(icorgf.titre());
		currentAction = MODIFICATION;

		afficherFenetre();
	}

	public void afficherFenetre(EOInventaireComptable tmpInventaireComptable) {
		if (tmpInventaireComptable == null) {
			return;
		}
		
		currentInventaireComptable = tmpInventaireComptable;
		currentInventaireComptableOrig = null;

		majOrigineFinancement(currentInventaireComptable.exercice());

		// Montant par défaut = mntAmortissable - sommeMntOrigines
		BigDecimal invcMontantAmortissableAvecOrvs = currentInventaireComptable.invcMontantAmortissableAvecOrvs();
		BigDecimal montant = invcMontantAmortissableAvecOrvs.subtract(currentInventaireComptable.sommeMontantOriginesFinancement());
		// Pourcentage par défaut = mntParDefaut / mntAmortissable 
		BigDecimal pourcentage = CalculMontantUtil.pourcentageDuMontant(invcMontantAmortissableAvecOrvs, montant);

		getMonOriginesFinancementNib().getJTextFieldPourcentage().setValue(pourcentage);
		getMonOriginesFinancementNib().getjTextFieldMontant().setValue(montant);
		setTitre(null);
		currentAction = CREATION;
		afficherFenetre();
	}

	public void afficherFenetre() {
		super.afficherFenetre();
		try {
			app.activerLesGlassPane(this.currentNib);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private void bindingAndCustomization() {
		try {
			// le cablage des objets
			getMonOriginesFinancementNib().getJButtonCocktailValider().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionValider();
				}
			});
			getMonOriginesFinancementNib().getJButtonCocktailAnnuler().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionAnnuler();
				}
			});
			getMonOriginesFinancementNib().getJButtonCocktailRechercher().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					actionRechercher();
				}
			});
			getMonOriginesFinancementNib().getJComboBoxOrigineFinancement().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					setTitre(null);
					titreNibCtrl.viderTitres();
				}
			});

			getMonOriginesFinancementNib().getjButtonCalculMontant().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					updateMontantViaPourcentage();
				}
			});

			// Gestion des montants et des pourcentages
			getMonOriginesFinancementNib().getJTextFieldPourcentage().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if ((ae.getID() & ActionEvent.KEY_EVENT_MASK) != 0) {
						updateMontantViaPourcentage();
					}
				}
			});

			getMonOriginesFinancementNib().getJTextFieldPourcentage().setFormatterFactory(FormattedFieldProvider.getMontantAvecDecimalesFormatterFactory());

			getMonOriginesFinancementNib().getjTextFieldMontant().setFormatterFactory(FormattedFieldProvider.getMontantAvecDecimalesFormatterFactory());
			getMonOriginesFinancementNib().getjTextFieldMontant().addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(ActionEvent ae) {
					if ((ae.getID() & ActionEvent.KEY_EVENT_MASK) != 0) {
						updatePourcentageViaMontant();
					}
				}
			});

			getMonOriginesFinancementNib().getjTextFieldMontant().getDocument().addDocumentListener(new DocumentListener() {
				public void insertUpdate(DocumentEvent de) {
					updatePourcentageViaMontant();
				}

				public void removeUpdate(DocumentEvent de) {
					updatePourcentageViaMontant();
				}

				public void changedUpdate(DocumentEvent de) {
				}
			});

			// les images des objets
			getMonOriginesFinancementNib().getJButtonCocktailValider().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.VALIDER));
			getMonOriginesFinancementNib().getJButtonCocktailAnnuler().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.FERMER));
			getMonOriginesFinancementNib().getJButtonCocktailRechercher().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.SHOW));
			getMonOriginesFinancementNib().getjButtonCalculMontant().setIcon((ImageIcon) new EOClientResourceBundle().getObject(IconeCocktail.FLECHEDROITE));

			getMonOriginesFinancementNib().getJTextFieldTitre().setEditable(false);

			// sera modal lors d une transaction
			app.addLesPanelsModal(getMonOriginesFinancementNib());

			// observation des changements de selection de lexercice :
			((ApplicationCorossol) app).getObserveurExerciceSelection().addObserverForMessage(this, "changementExercice");

		} catch (Exception e) {
			this.app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
		}
	}

	/**
	 * 
	 */
	private void updateMontantViaPourcentage() {
		// FIXME
		String sPourcentage = getMonOriginesFinancementNib().getJTextFieldPourcentage().getText();
		try {
			BigDecimal pourcentage = UtilCtrl.stringToBigDecimal(sPourcentage);
			BigDecimal montantDuPourcentage = CalculMontantUtil.montantDuPourcentage(currentInventaireComptable.invcMontantAmortissableAvecOrvs(), pourcentage);
			System.out.println("updateMontantViaPourcentage : " + pourcentage + "%");
			getMonOriginesFinancementNib().getjTextFieldMontant().setValue(montantDuPourcentage);
			getMonOriginesFinancementNib().getjTextFieldMontant().repaint();
		} catch (NumberFormatException e) {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void updatePourcentageViaMontant() {
		// FIXME
		String sMontant = getMonOriginesFinancementNib().getjTextFieldMontant().getText();
		try {
			BigDecimal montant = UtilCtrl.stringToBigDecimal(sMontant);
			BigDecimal pourcentageDuMontant = CalculMontantUtil.pourcentageDuMontant(currentInventaireComptable.invcMontantAmortissableAvecOrvs(), montant);
			System.out.println("updatePourcentageViaMontant : " + montant + "");
			getMonOriginesFinancementNib().getJTextFieldPourcentage().setValue(pourcentageDuMontant);
			getMonOriginesFinancementNib().getJTextFieldPourcentage().repaint();
		} catch (NumberFormatException e) {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setTitre(EOTitre titre) {
		currentTitre = titre;
		afficherTitre(currentTitre);
	}

	public void afficherTitre(EOTitre titre) {
		if (titre == null) {
			getMonOriginesFinancementNib().jTextFieldTitre.setText("");
			return;
		}
		getMonOriginesFinancementNib().jTextFieldTitre.setText("num " + titre.titNumero() + ", du " + titre.gesCode() + ", exer : "
				+ titre.exercice().exeExercice() + ", mnt dispo. : " + titre.montantDisponible());
	}
	
	// methodes cablage
	public void actionAnnuler() {
		//	trace(METHODE_ACTION_FERMER);
		masquerFenetre();
		getParentControleur().assistantsAnnuler(this);
	}

	public void masquerFenetre() {
		titreNibCtrl.masquerFenetre();
		super.masquerFenetre();
	}
 	
	public void actionValider() {

		String chaineMontant = getMonOriginesFinancementNib().getjTextFieldMontant().getValue().toString();
		String chainePourcentage = getMonOriginesFinancementNib().getJTextFieldPourcentage().getValue().toString();

		try {			
			// Calcul des montants/pourcentages
			IInventaireComptableOrig origine = CalculMontantUtil.calculRepartition(chaineMontant, chainePourcentage, currentInventaireComptable);
			// Check du montant disponible sur le titre
			InventaireComptableOrigHelper.checkTitreMontantDisponible(currentInventaireComptableOrig, origine, currentTitre);
			
			updateCurrentInventaireComptableOrig(origine);
			
		} catch (Exception e) {
			if (e.getMessage() == null) {
			    e.printStackTrace();
				fenetreDeDialogueInformation("Erreur de conversion du montant, vérifier que le nombre est valide !");
			} else {
				fenetreDeDialogueInformation(e.getMessage());
			}
			return;
		}
		masquerFenetre();
		getParentControleur().swingFinderTerminer(this);
	}

	private void updateCurrentInventaireComptableOrig(IInventaireComptableOrig origine) {
        if (currentInventaireComptableOrig == null) {
            currentInventaireComptableOrig = EOInventaireComptableOrig.creerInstance(app.getAppEditingContext());
        }
        currentInventaireComptableOrig.setIcorMontant(origine.icorMontant());
        currentInventaireComptableOrig.setIcorPourcentage(origine.icorPourcentage());
        currentInventaireComptableOrig.setInventaireComptableRelationship(currentInventaireComptable);
        currentInventaireComptableOrig.setTitreRelationship(currentTitre);
        currentInventaireComptableOrig.setOrigineFinancementRelationship(origineFinancement());
    }

    public EOOrigineFinancement origineFinancement() {
		if (getMonOriginesFinancementNib() == null || getMonOriginesFinancementNib().getJComboBoxOrigineFinancement() == null) {
			return null;
		}
		return (EOOrigineFinancement) getMonOriginesFinancementNib().getJComboBoxOrigineFinancement().getSelectedItem();
	}
		
	public void actionRechercher() {
		if (!Privileges.isPrivileges(app.getMesPrivileges(), Privileges.INVTITRE)) {
			fenetreDeDialogueInformation("Privilèges insuffisants!");
			return;
		}
		if (origineFinancement() == null) {
			fenetreDeDialogueInformation("Aucun origine de financement sélectionné");
			return;
		}
		if (origineFinancement() != null && origineFinancement().pcoNum()==null){
			fenetreDeDialogueInformation("Aucun compte n'est associé à l'origine de financement \"" +
		    (origineFinancement().orgfLibelle() == null ? "?" : origineFinancement().orgfLibelle()) +
			"\".\n"+
		    "La recherche de titre sur ce compte ne peut donc être réalisée.\n"+
		    "Veuillez contacter votre Administrateur.");
			return;
		}
		if (!origineFinancement().pcoNum().startsWith("1")) {
			fenetreDeDialogueInformation("Seuls les titres sur compte de classe 1 peuvent être sélectionnés \n"+
		    "Le compte de recherche de titre est " + origineFinancement().pcoNum());
			return;
		}
		
		titreNibCtrl.afficherFenetre(this);
	}

	//	accesseurs
	private OriginesFinancementNib getMonOriginesFinancementNib() {
		return monOriginesFinancementNib;
	}

	private void setMonOriginesFinancementNib(OriginesFinancementNib monOriginesFinancementNib) {
		this.monOriginesFinancementNib = monOriginesFinancementNib;
	}

	EOInventaireComptable getCurrentInventaireComptable() {
		return currentInventaireComptable;
	}

	private void setCurrentInventaireComptable(EOInventaireComptable currentInventaireComptable) {
		this.currentInventaireComptable = currentInventaireComptable;
	}

	EOInventaireComptableOrig getCurrentInventaireComptableOrig() {
		return currentInventaireComptableOrig;
	}

	private void setCurrentInventaireComptableOrig(EOInventaireComptableOrig currentInventaireComptableOrig) {
		this.currentInventaireComptableOrig = currentInventaireComptableOrig;
	}

	private NibCtrl getParentControleur() {
		return parentControleur;
	}

	private void setParentControleur(NibCtrl parentControleur) {
		this.parentControleur = parentControleur;
	}

	//	listener inspecteur
	public  void swingFinderAnnuler(Object obj) {
	}

	public  void swingFinderTerminer(Object obj) {
		
	}
}
