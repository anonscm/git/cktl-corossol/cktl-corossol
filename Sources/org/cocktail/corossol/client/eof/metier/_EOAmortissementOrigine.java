// _EOAmortissementOrigine.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAmortissementOrigine.java instead.
package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOAmortissementOrigine extends  EOGenericRecord {
	public static final String ENTITY_NAME = "AmortissementOrigine";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.AMORTISSEMENT_ORIGINE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idAmortissementOrigine";

	public static final String AMOO_ANNUITE_KEY = "amooAnnuite";
	public static final String AMOO_CUMUL_KEY = "amooCumul";
	public static final String AMOO_MONTANT_KEY = "amooMontant";
	public static final String AMOO_RESIDUEL_KEY = "amooResiduel";
	public static final String D_CALCUL_KEY = "dCalcul";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ICOR_ID_KEY = "icorId";
	public static final String ID_AMORTISSEMENT_ORIGINE_KEY = "idAmortissementOrigine";

//Colonnes dans la base de donnees
	public static final String AMOO_ANNUITE_COLKEY = "AMOO_ANNUITE";
	public static final String AMOO_CUMUL_COLKEY = "AMOO_CUMUL";
	public static final String AMOO_MONTANT_COLKEY = "AMOO_MONTANT";
	public static final String AMOO_RESIDUEL_COLKEY = "AMOO_RESIDUEL";
	public static final String D_CALCUL_COLKEY = "D_CALCUL";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ICOR_ID_COLKEY = "ICOR_ID";
	public static final String ID_AMORTISSEMENT_ORIGINE_COLKEY = "ID_AMORTISSEMENT_ORIGINE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String TO_INVENTAIRE_COMPTABLE_ORIG_KEY = "toInventaireComptableOrig";



	// Accessors methods
  public java.math.BigDecimal amooAnnuite() {
    return (java.math.BigDecimal) storedValueForKey(AMOO_ANNUITE_KEY);
  }

  public void setAmooAnnuite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AMOO_ANNUITE_KEY);
  }

  public java.math.BigDecimal amooCumul() {
    return (java.math.BigDecimal) storedValueForKey(AMOO_CUMUL_KEY);
  }

  public void setAmooCumul(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AMOO_CUMUL_KEY);
  }

  public java.math.BigDecimal amooMontant() {
    return (java.math.BigDecimal) storedValueForKey(AMOO_MONTANT_KEY);
  }

  public void setAmooMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AMOO_MONTANT_KEY);
  }

  public java.math.BigDecimal amooResiduel() {
    return (java.math.BigDecimal) storedValueForKey(AMOO_RESIDUEL_KEY);
  }

  public void setAmooResiduel(java.math.BigDecimal value) {
    takeStoredValueForKey(value, AMOO_RESIDUEL_KEY);
  }

  public NSTimestamp dCalcul() {
    return (NSTimestamp) storedValueForKey(D_CALCUL_KEY);
  }

  public void setDCalcul(NSTimestamp value) {
    takeStoredValueForKey(value, D_CALCUL_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig toInventaireComptableOrig() {
    return (org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig)storedValueForKey(TO_INVENTAIRE_COMPTABLE_ORIG_KEY);
  }

  public void setToInventaireComptableOrigRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig oldValue = toInventaireComptableOrig();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INVENTAIRE_COMPTABLE_ORIG_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INVENTAIRE_COMPTABLE_ORIG_KEY);
    }
  }
  

  public static EOAmortissementOrigine createAmortissementOrigine(EOEditingContext editingContext, java.math.BigDecimal amooAnnuite
, java.math.BigDecimal amooCumul
, java.math.BigDecimal amooMontant
, java.math.BigDecimal amooResiduel
, NSTimestamp dCalcul
, org.cocktail.application.client.eof.EOExercice exercice, org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig toInventaireComptableOrig) {
    EOAmortissementOrigine eo = (EOAmortissementOrigine) createAndInsertInstance(editingContext, _EOAmortissementOrigine.ENTITY_NAME);    
		eo.setAmooAnnuite(amooAnnuite);
		eo.setAmooCumul(amooCumul);
		eo.setAmooMontant(amooMontant);
		eo.setAmooResiduel(amooResiduel);
		eo.setDCalcul(dCalcul);
    eo.setExerciceRelationship(exercice);
    eo.setToInventaireComptableOrigRelationship(toInventaireComptableOrig);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOAmortissementOrigine.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOAmortissementOrigine.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOAmortissementOrigine creerInstance(EOEditingContext editingContext) {
		  		EOAmortissementOrigine object = (EOAmortissementOrigine)createAndInsertInstance(editingContext, _EOAmortissementOrigine.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOAmortissementOrigine localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAmortissementOrigine)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOAmortissementOrigine localInstanceIn(EOEditingContext editingContext, EOAmortissementOrigine eo) {
    EOAmortissementOrigine localInstance = (eo == null) ? null : (EOAmortissementOrigine)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOAmortissementOrigine#localInstanceIn a la place.
   */
	public static EOAmortissementOrigine localInstanceOf(EOEditingContext editingContext, EOAmortissementOrigine eo) {
		return EOAmortissementOrigine.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOAmortissementOrigine fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOAmortissementOrigine fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAmortissementOrigine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAmortissementOrigine)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAmortissementOrigine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAmortissementOrigine fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAmortissementOrigine eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAmortissementOrigine)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOAmortissementOrigine fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAmortissementOrigine eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAmortissementOrigine ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAmortissementOrigine fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
