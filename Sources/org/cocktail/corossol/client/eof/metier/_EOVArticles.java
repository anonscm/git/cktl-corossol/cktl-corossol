// _EOVArticles.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVArticles.java instead.
package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVArticles extends  EOGenericRecord {
	public static final String ENTITY_NAME = "VArticles";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.V_ARTICLES";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "artId";

	public static final String ARTC_ID_KEY = "artcId";
	public static final String ART_LIBELLE_KEY = "artLibelle";
	public static final String ART_PRIX_HT_KEY = "artPrixHt";
	public static final String ART_PRIX_TOTAL_HT_KEY = "artPrixTotalHt";
	public static final String ART_PRIX_TOTAL_TTC_KEY = "artPrixTotalTtc";
	public static final String ART_PRIX_TTC_KEY = "artPrixTtc";
	public static final String ART_QUANTITE_KEY = "artQuantite";
	public static final String ART_REFERENCE_KEY = "artReference";
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String TVA_ID_KEY = "tvaId";
	public static final String TYPA_ID_KEY = "typaId";

// Attributs non visibles
	public static final String ART_ID_KEY = "artId";
	public static final String COMM_ID_KEY = "commId";

//Colonnes dans la base de donnees
	public static final String ARTC_ID_COLKEY = "ARTC_ID";
	public static final String ART_LIBELLE_COLKEY = "ART_LIBELLE";
	public static final String ART_PRIX_HT_COLKEY = "ART_PRIX_HT";
	public static final String ART_PRIX_TOTAL_HT_COLKEY = "ART_PRIX_TOTAL_HT";
	public static final String ART_PRIX_TOTAL_TTC_COLKEY = "ART_PRIX_TOTAL_TTC";
	public static final String ART_PRIX_TTC_COLKEY = "ART_PRIX_TTC";
	public static final String ART_QUANTITE_COLKEY = "ART_QUANTITE";
	public static final String ART_REFERENCE_COLKEY = "ART_REFERENCE";
	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String TVA_ID_COLKEY = "TVA_ID";
	public static final String TYPA_ID_COLKEY = "TYPA_ID";

	public static final String ART_ID_COLKEY = "ART_ID";
	public static final String COMM_ID_COLKEY = "COMM_ID";


	// Relationships
	public static final String V_COMMANDE_KEY = "vCommande";



	// Accessors methods
  public Integer artcId() {
    return (Integer) storedValueForKey(ARTC_ID_KEY);
  }

  public void setArtcId(Integer value) {
    takeStoredValueForKey(value, ARTC_ID_KEY);
  }

  public String artLibelle() {
    return (String) storedValueForKey(ART_LIBELLE_KEY);
  }

  public void setArtLibelle(String value) {
    takeStoredValueForKey(value, ART_LIBELLE_KEY);
  }

  public java.math.BigDecimal artPrixHt() {
    return (java.math.BigDecimal) storedValueForKey(ART_PRIX_HT_KEY);
  }

  public void setArtPrixHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_PRIX_HT_KEY);
  }

  public java.math.BigDecimal artPrixTotalHt() {
    return (java.math.BigDecimal) storedValueForKey(ART_PRIX_TOTAL_HT_KEY);
  }

  public void setArtPrixTotalHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_PRIX_TOTAL_HT_KEY);
  }

  public java.math.BigDecimal artPrixTotalTtc() {
    return (java.math.BigDecimal) storedValueForKey(ART_PRIX_TOTAL_TTC_KEY);
  }

  public void setArtPrixTotalTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_PRIX_TOTAL_TTC_KEY);
  }

  public java.math.BigDecimal artPrixTtc() {
    return (java.math.BigDecimal) storedValueForKey(ART_PRIX_TTC_KEY);
  }

  public void setArtPrixTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_PRIX_TTC_KEY);
  }

  public java.math.BigDecimal artQuantite() {
    return (java.math.BigDecimal) storedValueForKey(ART_QUANTITE_KEY);
  }

  public void setArtQuantite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ART_QUANTITE_KEY);
  }

  public String artReference() {
    return (String) storedValueForKey(ART_REFERENCE_KEY);
  }

  public void setArtReference(String value) {
    takeStoredValueForKey(value, ART_REFERENCE_KEY);
  }

  public Integer attOrdre() {
    return (Integer) storedValueForKey(ATT_ORDRE_KEY);
  }

  public void setAttOrdre(Integer value) {
    takeStoredValueForKey(value, ATT_ORDRE_KEY);
  }

  public Integer ceOrdre() {
    return (Integer) storedValueForKey(CE_ORDRE_KEY);
  }

  public void setCeOrdre(Integer value) {
    takeStoredValueForKey(value, CE_ORDRE_KEY);
  }

  public Integer tvaId() {
    return (Integer) storedValueForKey(TVA_ID_KEY);
  }

  public void setTvaId(Integer value) {
    takeStoredValueForKey(value, TVA_ID_KEY);
  }

  public Integer typaId() {
    return (Integer) storedValueForKey(TYPA_ID_KEY);
  }

  public void setTypaId(Integer value) {
    takeStoredValueForKey(value, TYPA_ID_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOVCommande vCommande() {
    return (org.cocktail.corossol.client.eof.metier.EOVCommande)storedValueForKey(V_COMMANDE_KEY);
  }

  public void setVCommandeRelationship(org.cocktail.corossol.client.eof.metier.EOVCommande value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOVCommande oldValue = vCommande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_COMMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_COMMANDE_KEY);
    }
  }
  

  public static EOVArticles createVArticles(EOEditingContext editingContext, String artLibelle
, java.math.BigDecimal artPrixHt
, java.math.BigDecimal artPrixTotalHt
, java.math.BigDecimal artPrixTotalTtc
, java.math.BigDecimal artPrixTtc
, java.math.BigDecimal artQuantite
, Integer ceOrdre
, Integer tvaId
) {
    EOVArticles eo = (EOVArticles) createAndInsertInstance(editingContext, _EOVArticles.ENTITY_NAME);    
		eo.setArtLibelle(artLibelle);
		eo.setArtPrixHt(artPrixHt);
		eo.setArtPrixTotalHt(artPrixTotalHt);
		eo.setArtPrixTotalTtc(artPrixTotalTtc);
		eo.setArtPrixTtc(artPrixTtc);
		eo.setArtQuantite(artQuantite);
		eo.setCeOrdre(ceOrdre);
		eo.setTvaId(tvaId);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVArticles.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVArticles.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOVArticles creerInstance(EOEditingContext editingContext) {
		  		EOVArticles object = (EOVArticles)createAndInsertInstance(editingContext, _EOVArticles.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOVArticles localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVArticles)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVArticles localInstanceIn(EOEditingContext editingContext, EOVArticles eo) {
    EOVArticles localInstance = (eo == null) ? null : (EOVArticles)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVArticles#localInstanceIn a la place.
   */
	public static EOVArticles localInstanceOf(EOEditingContext editingContext, EOVArticles eo) {
		return EOVArticles.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVArticles fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVArticles fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVArticles eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVArticles)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVArticles fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVArticles fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVArticles eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVArticles)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVArticles fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVArticles eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVArticles ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVArticles fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
