// _EODegressifCoef.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODegressifCoef.java instead.
package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODegressifCoef extends  EOGenericRecord {
	public static final String ENTITY_NAME = "DegressifCoef";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.DEGRESSIF_COEF";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dgcoId";

	public static final String DGCO_COEF_KEY = "dgcoCoef";
	public static final String DGCO_DUREE_MAX_KEY = "dgcoDureeMax";
	public static final String DGCO_DUREE_MIN_KEY = "dgcoDureeMin";

// Attributs non visibles
	public static final String DGCO_ID_KEY = "dgcoId";
	public static final String DGRF_ID_KEY = "dgrfId";

//Colonnes dans la base de donnees
	public static final String DGCO_COEF_COLKEY = "DGCO_COEF";
	public static final String DGCO_DUREE_MAX_COLKEY = "DGCO_DUREE_MAX";
	public static final String DGCO_DUREE_MIN_COLKEY = "DGCO_DUREE_MIN";

	public static final String DGCO_ID_COLKEY = "DGCO_ID";
	public static final String DGRF_ID_COLKEY = "DGRF_ID";


	// Relationships
	public static final String DEGRESSIF_KEY = "degressif";



	// Accessors methods
  public java.math.BigDecimal dgcoCoef() {
    return (java.math.BigDecimal) storedValueForKey(DGCO_COEF_KEY);
  }

  public void setDgcoCoef(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DGCO_COEF_KEY);
  }

  public Integer dgcoDureeMax() {
    return (Integer) storedValueForKey(DGCO_DUREE_MAX_KEY);
  }

  public void setDgcoDureeMax(Integer value) {
    takeStoredValueForKey(value, DGCO_DUREE_MAX_KEY);
  }

  public Integer dgcoDureeMin() {
    return (Integer) storedValueForKey(DGCO_DUREE_MIN_KEY);
  }

  public void setDgcoDureeMin(Integer value) {
    takeStoredValueForKey(value, DGCO_DUREE_MIN_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EODegressif degressif() {
    return (org.cocktail.corossol.client.eof.metier.EODegressif)storedValueForKey(DEGRESSIF_KEY);
  }

  public void setDegressifRelationship(org.cocktail.corossol.client.eof.metier.EODegressif value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EODegressif oldValue = degressif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEGRESSIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEGRESSIF_KEY);
    }
  }
  

  public static EODegressifCoef createDegressifCoef(EOEditingContext editingContext, java.math.BigDecimal dgcoCoef
, Integer dgcoDureeMax
, Integer dgcoDureeMin
) {
    EODegressifCoef eo = (EODegressifCoef) createAndInsertInstance(editingContext, _EODegressifCoef.ENTITY_NAME);    
		eo.setDgcoCoef(dgcoCoef);
		eo.setDgcoDureeMax(dgcoDureeMax);
		eo.setDgcoDureeMin(dgcoDureeMin);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EODegressifCoef.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EODegressifCoef.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EODegressifCoef creerInstance(EOEditingContext editingContext) {
		  		EODegressifCoef object = (EODegressifCoef)createAndInsertInstance(editingContext, _EODegressifCoef.ENTITY_NAME);
		  		return object;
			}


		
  	  public EODegressifCoef localInstanceIn(EOEditingContext editingContext) {
	  		return (EODegressifCoef)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EODegressifCoef localInstanceIn(EOEditingContext editingContext, EODegressifCoef eo) {
    EODegressifCoef localInstance = (eo == null) ? null : (EODegressifCoef)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EODegressifCoef#localInstanceIn a la place.
   */
	public static EODegressifCoef localInstanceOf(EOEditingContext editingContext, EODegressifCoef eo) {
		return EODegressifCoef.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EODegressifCoef fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EODegressifCoef fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODegressifCoef eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODegressifCoef)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODegressifCoef fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODegressifCoef fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODegressifCoef eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODegressifCoef)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EODegressifCoef fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODegressifCoef eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODegressifCoef ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODegressifCoef fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
