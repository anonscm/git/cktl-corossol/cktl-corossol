/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.NSValidation;
import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.*;

public class EODegressif extends _EODegressif {

    public EODegressif() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      if (dgrfDateDebut()==null)
    		throw new ValidationException("il faut une date de début de validite pour ces taux");
    	if (degressifCoefs()==null || degressifCoefs().count()==0)
    		throw new ValidationException("il faut au moins un coefficient");
    	
    	for (int i=0; i<degressifCoefs().count(); i++) {
    		EODegressifCoef coef=(EODegressifCoef)degressifCoefs().objectAtIndex(i);
    		if (coef.dgcoDureeMin()==null)
        		throw new ValidationException("il faut une duree minimale pour chaque coefficient");
    		if (coef.dgcoDureeMax()==null)
        		throw new ValidationException("il faut une duree maximale pour chaque coefficient");
    		if (coef.dgcoCoef()==null || coef.dgcoCoef().floatValue()<0.0)
        		throw new ValidationException("il faut un coefficient d'amortissement positif");
    		if (coef.dgcoDureeMin().intValue()<0 || coef.dgcoDureeMax().intValue()<0)
        		throw new ValidationException("Les durees d'amortissement doivent etre un entier positif");
    		if (coef.dgcoDureeMin().intValue()>coef.dgcoDureeMax().intValue())
        		throw new ValidationException("La borne minimale de la duree d'amortissement doit etre inferieure a la borne maximale");
    	}

    	if (degressifCoefs().count()==1)
    		return;
    	
    	NSArray arrayTrie=EOSortOrdering.sortedArrayUsingKeyOrderArray(degressifCoefs(), 
    			new NSArray(EOSortOrdering.sortOrderingWithKey(EODegressifCoef.DGCO_DUREE_MIN_KEY, EOSortOrdering.CompareAscending)));
    	
    	int max=((EODegressifCoef)arrayTrie.objectAtIndex(0)).dgcoDureeMax().intValue();
    	for (int i=1; i<degressifCoefs().count(); i++) {
    		EODegressifCoef coef=(EODegressifCoef)degressifCoefs().objectAtIndex(i);
    		if (coef.dgcoDureeMin().intValue()<=max)
        		throw new ValidationException("Les periodes de 2 coefficients se chevauchent");
    	}

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
