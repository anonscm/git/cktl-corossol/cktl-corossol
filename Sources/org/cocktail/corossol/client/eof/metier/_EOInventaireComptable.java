// _EOInventaireComptable.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOInventaireComptable.java instead.
package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOInventaireComptable extends  EOGenericRecord {
	public static final String ENTITY_NAME = "InventaireComptable";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "invcId";

	public static final String INVC_DATE_ACQUISITION_KEY = "invcDateAcquisition";
	public static final String INVC_DATE_SORTIE_KEY = "invcDateSortie";
	public static final String INVC_ETAT_KEY = "invcEtat";
	public static final String INVC_MONTANT_ACQUISITION_KEY = "invcMontantAcquisition";
	public static final String INVC_MONTANT_AMORTISSABLE_KEY = "invcMontantAmortissable";
	public static final String INVC_MONTANT_RESIDUEL_KEY = "invcMontantResiduel";

// Attributs non visibles
	public static final String CLIC_ID_KEY = "clicId";
	public static final String DGCO_ID_KEY = "dgcoId";
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String INVC_ID_KEY = "invcId";
	public static final String ORGF_ID_KEY = "orgfId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String INVC_DATE_ACQUISITION_COLKEY = "INVC_DATE_ACQUISITION";
	public static final String INVC_DATE_SORTIE_COLKEY = "INVC_DATE_SORTIE";
	public static final String INVC_ETAT_COLKEY = "INVC_ETAT";
	public static final String INVC_MONTANT_ACQUISITION_COLKEY = "INVC_MONTANT_ACQUISITION";
	public static final String INVC_MONTANT_AMORTISSABLE_COLKEY = "INVC_MONTANT_AMORTISSABLE";
	public static final String INVC_MONTANT_RESIDUEL_COLKEY = "INVC_MONTANT_RESIDUEL";

	public static final String CLIC_ID_COLKEY = "CLIC_ID";
	public static final String DGCO_ID_COLKEY = "DGCO_ID";
	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String INVC_ID_COLKEY = "INVC_ID";
	public static final String ORGF_ID_COLKEY = "ORGF_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String CLE_INVENTAIRE_COMPTABLE_KEY = "cleInventaireComptable";
	public static final String DEGRESSIF_COEF_KEY = "degressifCoef";
	public static final String EXERCICE_KEY = "exercice";
	public static final String INVENTAIRE_COMPTABLE_ORIGS_KEY = "inventaireComptableOrigs";
	public static final String INVENTAIRE_COMPTABLE_ORVS_KEY = "inventaireComptableOrvs";
	public static final String INVENTAIRE_COMPTABLE_SORTIES_KEY = "inventaireComptableSorties";
	public static final String INVENTAIRE_NON_BUDGETAIRES_KEY = "inventaireNonBudgetaires";
	public static final String INVENTAIRES_KEY = "inventaires";
	public static final String ORIGINE_FINANCEMENT_KEY = "origineFinancement";
	public static final String UTILISATEUR_KEY = "utilisateur";
	public static final String V_DEPENSE_KEY = "vDepense";



	// Accessors methods
  public NSTimestamp invcDateAcquisition() {
    return (NSTimestamp) storedValueForKey(INVC_DATE_ACQUISITION_KEY);
  }

  public void setInvcDateAcquisition(NSTimestamp value) {
    takeStoredValueForKey(value, INVC_DATE_ACQUISITION_KEY);
  }

  public NSTimestamp invcDateSortie() {
    return (NSTimestamp) storedValueForKey(INVC_DATE_SORTIE_KEY);
  }

  public void setInvcDateSortie(NSTimestamp value) {
    takeStoredValueForKey(value, INVC_DATE_SORTIE_KEY);
  }

  public String invcEtat() {
    return (String) storedValueForKey(INVC_ETAT_KEY);
  }

  public void setInvcEtat(String value) {
    takeStoredValueForKey(value, INVC_ETAT_KEY);
  }

  public java.math.BigDecimal invcMontantAcquisition() {
    return (java.math.BigDecimal) storedValueForKey(INVC_MONTANT_ACQUISITION_KEY);
  }

  public void setInvcMontantAcquisition(java.math.BigDecimal value) {
    takeStoredValueForKey(value, INVC_MONTANT_ACQUISITION_KEY);
  }

  public java.math.BigDecimal invcMontantAmortissable() {
    return (java.math.BigDecimal) storedValueForKey(INVC_MONTANT_AMORTISSABLE_KEY);
  }

  public void setInvcMontantAmortissable(java.math.BigDecimal value) {
    takeStoredValueForKey(value, INVC_MONTANT_AMORTISSABLE_KEY);
  }

  public java.math.BigDecimal invcMontantResiduel() {
    return (java.math.BigDecimal) storedValueForKey(INVC_MONTANT_RESIDUEL_KEY);
  }

  public void setInvcMontantResiduel(java.math.BigDecimal value) {
    takeStoredValueForKey(value, INVC_MONTANT_RESIDUEL_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable cleInventaireComptable() {
    return (org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable)storedValueForKey(CLE_INVENTAIRE_COMPTABLE_KEY);
  }

  public void setCleInventaireComptableRelationship(org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable oldValue = cleInventaireComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CLE_INVENTAIRE_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CLE_INVENTAIRE_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EODegressifCoef degressifCoef() {
    return (org.cocktail.corossol.client.eof.metier.EODegressifCoef)storedValueForKey(DEGRESSIF_COEF_KEY);
  }

  public void setDegressifCoefRelationship(org.cocktail.corossol.client.eof.metier.EODegressifCoef value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EODegressifCoef oldValue = degressifCoef();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEGRESSIF_COEF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEGRESSIF_COEF_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOOrigineFinancement origineFinancement() {
    return (org.cocktail.corossol.client.eof.metier.EOOrigineFinancement)storedValueForKey(ORIGINE_FINANCEMENT_KEY);
  }

  public void setOrigineFinancementRelationship(org.cocktail.corossol.client.eof.metier.EOOrigineFinancement value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOOrigineFinancement oldValue = origineFinancement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORIGINE_FINANCEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORIGINE_FINANCEMENT_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.client.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.client.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOVDepense vDepense() {
    return (org.cocktail.corossol.client.eof.metier.EOVDepense)storedValueForKey(V_DEPENSE_KEY);
  }

  public void setVDepenseRelationship(org.cocktail.corossol.client.eof.metier.EOVDepense value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOVDepense oldValue = vDepense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_DEPENSE_KEY);
    }
  }
  
  public NSArray inventaireComptableOrigs() {
    return (NSArray)storedValueForKey(INVENTAIRE_COMPTABLE_ORIGS_KEY);
  }

  public NSArray inventaireComptableOrigs(EOQualifier qualifier) {
    return inventaireComptableOrigs(qualifier, null, false);
  }

  public NSArray inventaireComptableOrigs(EOQualifier qualifier, boolean fetch) {
    return inventaireComptableOrigs(qualifier, null, fetch);
  }

  public NSArray inventaireComptableOrigs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig.INVENTAIRE_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inventaireComptableOrigs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInventaireComptableOrigsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_ORIGS_KEY);
  }

  public void removeFromInventaireComptableOrigsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_ORIGS_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig createInventaireComptableOrigsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("InventaireComptableOrig");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRE_COMPTABLE_ORIGS_KEY);
    return (org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig) eo;
  }

  public void deleteInventaireComptableOrigsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_ORIGS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventaireComptableOrigsRelationships() {
    Enumeration objects = inventaireComptableOrigs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventaireComptableOrigsRelationship((org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig)objects.nextElement());
    }
  }

  public NSArray inventaireComptableOrvs() {
    return (NSArray)storedValueForKey(INVENTAIRE_COMPTABLE_ORVS_KEY);
  }

  public NSArray inventaireComptableOrvs(EOQualifier qualifier) {
    return inventaireComptableOrvs(qualifier, null, false);
  }

  public NSArray inventaireComptableOrvs(EOQualifier qualifier, boolean fetch) {
    return inventaireComptableOrvs(qualifier, null, fetch);
  }

  public NSArray inventaireComptableOrvs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results = null;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv.INVENTAIRE_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    
    // FLA : tres tres moche. Il arrive que le fetch true echoue : 
    // uniquement lorsque notre premiere action consiste a valider un ajout d'origine de financement.
    // Dans ce cas, on verifie que la collection d'orv n'est pas déjà en mémoire.
    if (results == null || results.count() == 0) {
      results = inventaireComptableOrvs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    
    return results;
  }
  
  public void addToInventaireComptableOrvsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_ORVS_KEY);
  }

  public void removeFromInventaireComptableOrvsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_ORVS_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv createInventaireComptableOrvsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("InventaireComptableOrv");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRE_COMPTABLE_ORVS_KEY);
    return (org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv) eo;
  }

  public void deleteInventaireComptableOrvsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_ORVS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventaireComptableOrvsRelationships() {
    Enumeration objects = inventaireComptableOrvs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventaireComptableOrvsRelationship((org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv)objects.nextElement());
    }
  }

  public NSArray inventaireComptableSorties() {
    return (NSArray)storedValueForKey(INVENTAIRE_COMPTABLE_SORTIES_KEY);
  }

  public NSArray inventaireComptableSorties(EOQualifier qualifier) {
    return inventaireComptableSorties(qualifier, null, false);
  }

  public NSArray inventaireComptableSorties(EOQualifier qualifier, boolean fetch) {
    return inventaireComptableSorties(qualifier, null, fetch);
  }

  public NSArray inventaireComptableSorties(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie.INVENTAIRE_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inventaireComptableSorties();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInventaireComptableSortiesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_SORTIES_KEY);
  }

  public void removeFromInventaireComptableSortiesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_SORTIES_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie createInventaireComptableSortiesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("InventaireComptableSortie");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRE_COMPTABLE_SORTIES_KEY);
    return (org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie) eo;
  }

  public void deleteInventaireComptableSortiesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_SORTIES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventaireComptableSortiesRelationships() {
    Enumeration objects = inventaireComptableSorties().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventaireComptableSortiesRelationship((org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie)objects.nextElement());
    }
  }

  public NSArray inventaireNonBudgetaires() {
    return (NSArray)storedValueForKey(INVENTAIRE_NON_BUDGETAIRES_KEY);
  }

  public NSArray inventaireNonBudgetaires(EOQualifier qualifier) {
    return inventaireNonBudgetaires(qualifier, null, false);
  }

  public NSArray inventaireNonBudgetaires(EOQualifier qualifier, boolean fetch) {
    return inventaireNonBudgetaires(qualifier, null, fetch);
  }

  public NSArray inventaireNonBudgetaires(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire.INVENTAIRE_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inventaireNonBudgetaires();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInventaireNonBudgetairesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRE_NON_BUDGETAIRES_KEY);
  }

  public void removeFromInventaireNonBudgetairesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_NON_BUDGETAIRES_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire createInventaireNonBudgetairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("InventaireNonBudgetaire");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRE_NON_BUDGETAIRES_KEY);
    return (org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire) eo;
  }

  public void deleteInventaireNonBudgetairesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_NON_BUDGETAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventaireNonBudgetairesRelationships() {
    Enumeration objects = inventaireNonBudgetaires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventaireNonBudgetairesRelationship((org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire)objects.nextElement());
    }
  }

  public NSArray inventaires() {
    return (NSArray)storedValueForKey(INVENTAIRES_KEY);
  }

  public NSArray inventaires(EOQualifier qualifier) {
    return inventaires(qualifier, null, false);
  }

  public NSArray inventaires(EOQualifier qualifier, boolean fetch) {
    return inventaires(qualifier, null, fetch);
  }

  public NSArray inventaires(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.client.eof.metier.EOInventaire.INVENTAIRE_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.client.eof.metier.EOInventaire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inventaires();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInventairesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaire object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRES_KEY);
  }

  public void removeFromInventairesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRES_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOInventaire createInventairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Inventaire");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRES_KEY);
    return (org.cocktail.corossol.client.eof.metier.EOInventaire) eo;
  }

  public void deleteInventairesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventairesRelationships() {
    Enumeration objects = inventaires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventairesRelationship((org.cocktail.corossol.client.eof.metier.EOInventaire)objects.nextElement());
    }
  }


  public static EOInventaireComptable createInventaireComptable(EOEditingContext editingContext) {
    EOInventaireComptable eo = (EOInventaireComptable) createAndInsertInstance(editingContext, _EOInventaireComptable.ENTITY_NAME);    
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOInventaireComptable.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOInventaireComptable.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOInventaireComptable creerInstance(EOEditingContext editingContext) {
		  		EOInventaireComptable object = (EOInventaireComptable)createAndInsertInstance(editingContext, _EOInventaireComptable.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOInventaireComptable localInstanceIn(EOEditingContext editingContext) {
	  		return (EOInventaireComptable)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOInventaireComptable localInstanceIn(EOEditingContext editingContext, EOInventaireComptable eo) {
    EOInventaireComptable localInstance = (eo == null) ? null : (EOInventaireComptable)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOInventaireComptable#localInstanceIn a la place.
   */
	public static EOInventaireComptable localInstanceOf(EOEditingContext editingContext, EOInventaireComptable eo) {
		return EOInventaireComptable.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOInventaireComptable fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOInventaireComptable fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOInventaireComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOInventaireComptable)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOInventaireComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOInventaireComptable fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOInventaireComptable eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOInventaireComptable)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOInventaireComptable fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOInventaireComptable eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOInventaireComptable ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOInventaireComptable fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
