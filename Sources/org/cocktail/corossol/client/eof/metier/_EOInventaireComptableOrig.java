// _EOInventaireComptableOrig.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOInventaireComptableOrig.java instead.
package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOInventaireComptableOrig extends  EOGenericRecord {
	public static final String ENTITY_NAME = "InventaireComptableOrig";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.INVENTAIRE_COMPTABLE_ORIG";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "icorId";

	public static final String ICOR_MONTANT_KEY = "icorMontant";
	public static final String ICOR_POURCENTAGE_KEY = "icorPourcentage";

// Attributs non visibles
	public static final String ICOR_ID_KEY = "icorId";
	public static final String INVC_ID_KEY = "invcId";
	public static final String ORGF_ID_KEY = "orgfId";
	public static final String TIT_ID_KEY = "titId";

//Colonnes dans la base de donnees
	public static final String ICOR_MONTANT_COLKEY = "ICOR_MONTANT";
	public static final String ICOR_POURCENTAGE_COLKEY = "ICOR_POURCENTAGE";

	public static final String ICOR_ID_COLKEY = "ICOR_ID";
	public static final String INVC_ID_COLKEY = "INVC_ID";
	public static final String ORGF_ID_COLKEY = "ORGF_ID";
	public static final String TIT_ID_COLKEY = "TIT_ID";


	// Relationships
	public static final String AMORTISSEMENT_ORIGINES_KEY = "amortissementOrigines";
	public static final String INVENTAIRE_COMPTABLE_KEY = "inventaireComptable";
	public static final String ORIGINE_FINANCEMENT_KEY = "origineFinancement";
	public static final String TITRE_KEY = "titre";



	// Accessors methods
  public java.math.BigDecimal icorMontant() {
    return (java.math.BigDecimal) storedValueForKey(ICOR_MONTANT_KEY);
  }

  public void setIcorMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ICOR_MONTANT_KEY);
  }

  public java.math.BigDecimal icorPourcentage() {
    return (java.math.BigDecimal) storedValueForKey(ICOR_POURCENTAGE_KEY);
  }

  public void setIcorPourcentage(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ICOR_POURCENTAGE_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOInventaireComptable inventaireComptable() {
    return (org.cocktail.corossol.client.eof.metier.EOInventaireComptable)storedValueForKey(INVENTAIRE_COMPTABLE_KEY);
  }

  public void setInventaireComptableRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptable value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOInventaireComptable oldValue = inventaireComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INVENTAIRE_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INVENTAIRE_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOOrigineFinancement origineFinancement() {
    return (org.cocktail.corossol.client.eof.metier.EOOrigineFinancement)storedValueForKey(ORIGINE_FINANCEMENT_KEY);
  }

  public void setOrigineFinancementRelationship(org.cocktail.corossol.client.eof.metier.EOOrigineFinancement value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOOrigineFinancement oldValue = origineFinancement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORIGINE_FINANCEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORIGINE_FINANCEMENT_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOTitre titre() {
    return (org.cocktail.corossol.client.eof.metier.EOTitre)storedValueForKey(TITRE_KEY);
  }

  public void setTitreRelationship(org.cocktail.corossol.client.eof.metier.EOTitre value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOTitre oldValue = titre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITRE_KEY);
    }
  }
  
  public NSArray amortissementOrigines() {
    return (NSArray)storedValueForKey(AMORTISSEMENT_ORIGINES_KEY);
  }

  public NSArray amortissementOrigines(EOQualifier qualifier) {
    return amortissementOrigines(qualifier, null, false);
  }

  public NSArray amortissementOrigines(EOQualifier qualifier, boolean fetch) {
    return amortissementOrigines(qualifier, null, fetch);
  }

  public NSArray amortissementOrigines(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.client.eof.metier.EOAmortissementOrigine.TO_INVENTAIRE_COMPTABLE_ORIG_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.client.eof.metier.EOAmortissementOrigine.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = amortissementOrigines();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAmortissementOriginesRelationship(org.cocktail.corossol.client.eof.metier.EOAmortissementOrigine object) {
    addObjectToBothSidesOfRelationshipWithKey(object, AMORTISSEMENT_ORIGINES_KEY);
  }

  public void removeFromAmortissementOriginesRelationship(org.cocktail.corossol.client.eof.metier.EOAmortissementOrigine object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, AMORTISSEMENT_ORIGINES_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOAmortissementOrigine createAmortissementOriginesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("AmortissementOrigine");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, AMORTISSEMENT_ORIGINES_KEY);
    return (org.cocktail.corossol.client.eof.metier.EOAmortissementOrigine) eo;
  }

  public void deleteAmortissementOriginesRelationship(org.cocktail.corossol.client.eof.metier.EOAmortissementOrigine object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, AMORTISSEMENT_ORIGINES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAmortissementOriginesRelationships() {
    Enumeration objects = amortissementOrigines().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAmortissementOriginesRelationship((org.cocktail.corossol.client.eof.metier.EOAmortissementOrigine)objects.nextElement());
    }
  }


  public static EOInventaireComptableOrig createInventaireComptableOrig(EOEditingContext editingContext, java.math.BigDecimal icorPourcentage
, org.cocktail.corossol.client.eof.metier.EOInventaireComptable inventaireComptable, org.cocktail.corossol.client.eof.metier.EOOrigineFinancement origineFinancement) {
    EOInventaireComptableOrig eo = (EOInventaireComptableOrig) createAndInsertInstance(editingContext, _EOInventaireComptableOrig.ENTITY_NAME);    
		eo.setIcorPourcentage(icorPourcentage);
    eo.setInventaireComptableRelationship(inventaireComptable);
    eo.setOrigineFinancementRelationship(origineFinancement);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOInventaireComptableOrig.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOInventaireComptableOrig.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOInventaireComptableOrig creerInstance(EOEditingContext editingContext) {
		  		EOInventaireComptableOrig object = (EOInventaireComptableOrig)createAndInsertInstance(editingContext, _EOInventaireComptableOrig.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOInventaireComptableOrig localInstanceIn(EOEditingContext editingContext) {
	  		return (EOInventaireComptableOrig)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOInventaireComptableOrig localInstanceIn(EOEditingContext editingContext, EOInventaireComptableOrig eo) {
    EOInventaireComptableOrig localInstance = (eo == null) ? null : (EOInventaireComptableOrig)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOInventaireComptableOrig#localInstanceIn a la place.
   */
	public static EOInventaireComptableOrig localInstanceOf(EOEditingContext editingContext, EOInventaireComptableOrig eo) {
		return EOInventaireComptableOrig.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOInventaireComptableOrig fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOInventaireComptableOrig fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOInventaireComptableOrig eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOInventaireComptableOrig)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOInventaireComptableOrig fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOInventaireComptableOrig fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOInventaireComptableOrig eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOInventaireComptableOrig)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOInventaireComptableOrig fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOInventaireComptableOrig eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOInventaireComptableOrig ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOInventaireComptableOrig fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
