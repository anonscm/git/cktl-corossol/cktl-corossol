/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.corossol.client.eof.metier;

import java.math.BigDecimal;

import org.cocktail.corossol.common.eof.repartition.IInventaireComptable;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOInventaireComptable extends _EOInventaireComptable implements IInventaireComptable {
	public final static String COMMANDE_KEY = "commande";
	public final static String DPCO_ID_BOOLEAN_KEY = "dpcoIdBoolean";	
	public final static String INVC_MONTANT_AMORTISSABLE_AVEC_ORVS_KEY = "invcMontantAmortissableAvecOrvs";	
	public final static String INVC_MONTANT_ACQUISITION_AVEC_ORVS_KEY = "invcMontantAcquisitionAvecOrvs";	

	NSArray inventairesAssocies;
	
    public EOInventaireComptable() {
        super();
        inventairesAssocies=new NSArray();
    }
   
	public String resume() {
		return cleInventaireComptable().clicNumComplet()+
		"\n Montant acquisition : "+invcMontantAcquisition();
	}

	public boolean dpcoIdBoolean() {
		if (vDepense() != null)
			return true;
		return false;
	}

	public EOVCommande commande() {
		if (inventaires()==null || inventaires().count()==0)
			return null;
		
		EOInventaire inventaire=(EOInventaire)inventaires().objectAtIndex(0);
		if (inventaire==null || inventaire.livraisonDetail()==null || inventaire.livraisonDetail().livraison()==null || inventaire.livraisonDetail().livraison().commande()==null)
			return null;
		
		return inventaire.livraisonDetail().livraison().commande();
	}
	
    
    public BigDecimal sommePourcentageOriginesFinancement() {
        BigDecimal somme = BigDecimal.ZERO;

        if (inventaireComptableOrigs() == null || inventaireComptableOrigs().count() == 0) {
            return somme;
        }

        for (int i = 0; i < inventaireComptableOrigs().count(); i++) {
            EOInventaireComptableOrig orig = (EOInventaireComptableOrig) inventaireComptableOrigs().objectAtIndex(i);
            if (orig != null && orig.icorPourcentage() != null) {
                somme = somme.add(orig.icorPourcentage());
            }
        }
        return somme;
    }

    /**
     * Retourne la somme des montants des origines de financements
     * 
     * @return
     */
    public BigDecimal sommeMontantOriginesFinancement() {
        BigDecimal somme = BigDecimal.ZERO;
        if (inventaireComptableOrigs() == null || inventaireComptableOrigs().count() == 0) {
            return somme;
        }

        for (int i = 0; i < inventaireComptableOrigs().count(); i++) {
            EOInventaireComptableOrig orig = (EOInventaireComptableOrig) inventaireComptableOrigs().objectAtIndex(i);
            if (orig != null && orig.icorMontant() != null) {
                somme = somme.add(orig.icorMontant());
            }
        }
        return somme;
    }
	
	public void setInventairesAssocies(NSArray array) {
		inventairesAssocies=array;
	}
	
	public NSArray getInventairesAssocies() {
		if (inventairesAssocies==null)
			return new NSArray();
		return inventairesAssocies;
	}
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    public String methodeDeCalculPourLesRepartitions() {
        // TODO a finir
        return MONTANT;
    }

	public BigDecimal invcMontantAmortissableAvecOrvs() {
		if (invcMontantAmortissable() == null) {
			return null;
		} else {
			return invcMontantAmortissable().add(montantOrvs());
		}
	}

	public BigDecimal invcMontantAcquisitionAvecOrvs() {
		return invcMontantAcquisition().add(montantOrvs());
	}

	public BigDecimal montantOrvs() {
		NSArray inventaireComptableOrvs;
		boolean temporary = this.editingContext().globalIDForObject(this).isTemporary();
		
		if (temporary) {
			inventaireComptableOrvs = inventaireComptableOrvs(); 
		} else {
			inventaireComptableOrvs = inventaireComptableOrvs(null, true);
		}
		
		return (BigDecimal) inventaireComptableOrvs.valueForKey(
				"@sum." + EOInventaireComptableOrv.INVO_MONTANT_ORV_KEY);
	}
}
