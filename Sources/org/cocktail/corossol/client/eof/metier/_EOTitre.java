// _EOTitre.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTitre.java instead.
package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTitre extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Titre";
	public static final String ENTITY_TABLE_NAME = "MARACUJA.TITRE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "titId";

	public static final String BOR_ID_KEY = "borId";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PREST_ID_KEY = "prestId";
	public static final String TIT_ETAT_KEY = "titEtat";
	public static final String TIT_HT_KEY = "titHt";
	public static final String TIT_ID_BIS_KEY = "titIdBis";
	public static final String TIT_LIBELLE_KEY = "titLibelle";
	public static final String TIT_NUMERO_KEY = "titNumero";
	public static final String TIT_TTC_KEY = "titTtc";
	public static final String TIT_TVA_KEY = "titTva";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TIT_ID_KEY = "titId";

//Colonnes dans la base de donnees
	public static final String BOR_ID_COLKEY = "BOR_ID";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String ORG_ORDRE_COLKEY = "ORG_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PREST_ID_COLKEY = "PREST_ID";
	public static final String TIT_ETAT_COLKEY = "TIT_ETAT";
	public static final String TIT_HT_COLKEY = "TIT_HT";
	public static final String TIT_ID_BIS_COLKEY = "TIT_ID";
	public static final String TIT_LIBELLE_COLKEY = "TIT_LIBELLE";
	public static final String TIT_NUMERO_COLKEY = "TIT_NUMERO";
	public static final String TIT_TTC_COLKEY = "TIT_TTC";
	public static final String TIT_TVA_COLKEY = "TIT_TVA";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TIT_ID_COLKEY = "TIT_ID";


	// Relationships
	public static final String BORDEREAU_KEY = "bordereau";
	public static final String EXERCICE_KEY = "exercice";
	public static final String INVENTAIRE_COMPTABLE_ORIGS_KEY = "inventaireComptableOrigs";
	public static final String ORGAN_KEY = "organ";
	public static final String V_FOURNISSEUR_KEY = "vFournisseur";



	// Accessors methods
  public Integer borId() {
    return (Integer) storedValueForKey(BOR_ID_KEY);
  }

  public void setBorId(Integer value) {
    takeStoredValueForKey(value, BOR_ID_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public Integer orgOrdre() {
    return (Integer) storedValueForKey(ORG_ORDRE_KEY);
  }

  public void setOrgOrdre(Integer value) {
    takeStoredValueForKey(value, ORG_ORDRE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public Integer prestId() {
    return (Integer) storedValueForKey(PREST_ID_KEY);
  }

  public void setPrestId(Integer value) {
    takeStoredValueForKey(value, PREST_ID_KEY);
  }

  public String titEtat() {
    return (String) storedValueForKey(TIT_ETAT_KEY);
  }

  public void setTitEtat(String value) {
    takeStoredValueForKey(value, TIT_ETAT_KEY);
  }

  public java.math.BigDecimal titHt() {
    return (java.math.BigDecimal) storedValueForKey(TIT_HT_KEY);
  }

  public void setTitHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_HT_KEY);
  }

  public Integer titIdBis() {
    return (Integer) storedValueForKey(TIT_ID_BIS_KEY);
  }

  public void setTitIdBis(Integer value) {
    takeStoredValueForKey(value, TIT_ID_BIS_KEY);
  }

  public String titLibelle() {
    return (String) storedValueForKey(TIT_LIBELLE_KEY);
  }

  public void setTitLibelle(String value) {
    takeStoredValueForKey(value, TIT_LIBELLE_KEY);
  }

  public Integer titNumero() {
    return (Integer) storedValueForKey(TIT_NUMERO_KEY);
  }

  public void setTitNumero(Integer value) {
    takeStoredValueForKey(value, TIT_NUMERO_KEY);
  }

  public java.math.BigDecimal titTtc() {
    return (java.math.BigDecimal) storedValueForKey(TIT_TTC_KEY);
  }

  public void setTitTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_TTC_KEY);
  }

  public java.math.BigDecimal titTva() {
    return (java.math.BigDecimal) storedValueForKey(TIT_TVA_KEY);
  }

  public void setTitTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_TVA_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOBordereau bordereau() {
    return (org.cocktail.corossol.client.eof.metier.EOBordereau)storedValueForKey(BORDEREAU_KEY);
  }

  public void setBordereauRelationship(org.cocktail.corossol.client.eof.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOBordereau oldValue = bordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOOrgan organ() {
    return (org.cocktail.application.client.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.client.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.VFournisseur vFournisseur() {
    return (org.cocktail.application.client.eof.VFournisseur)storedValueForKey(V_FOURNISSEUR_KEY);
  }

  public void setVFournisseurRelationship(org.cocktail.application.client.eof.VFournisseur value) {
    if (value == null) {
    	org.cocktail.application.client.eof.VFournisseur oldValue = vFournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_FOURNISSEUR_KEY);
    }
  }
  
  public NSArray inventaireComptableOrigs() {
    return (NSArray)storedValueForKey(INVENTAIRE_COMPTABLE_ORIGS_KEY);
  }

  public NSArray inventaireComptableOrigs(EOQualifier qualifier) {
    return inventaireComptableOrigs(qualifier, null, false);
  }

  public NSArray inventaireComptableOrigs(EOQualifier qualifier, boolean fetch) {
    return inventaireComptableOrigs(qualifier, null, fetch);
  }

  public NSArray inventaireComptableOrigs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig.TITRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inventaireComptableOrigs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInventaireComptableOrigsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_ORIGS_KEY);
  }

  public void removeFromInventaireComptableOrigsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_ORIGS_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig createInventaireComptableOrigsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("InventaireComptableOrig");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRE_COMPTABLE_ORIGS_KEY);
    return (org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig) eo;
  }

  public void deleteInventaireComptableOrigsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_COMPTABLE_ORIGS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventaireComptableOrigsRelationships() {
    Enumeration objects = inventaireComptableOrigs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventaireComptableOrigsRelationship((org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig)objects.nextElement());
    }
  }


  public static EOTitre createTitre(EOEditingContext editingContext, String gesCode
, String pcoNum
, String titEtat
, java.math.BigDecimal titHt
, Integer titNumero
, java.math.BigDecimal titTtc
, java.math.BigDecimal titTva
) {
    EOTitre eo = (EOTitre) createAndInsertInstance(editingContext, _EOTitre.ENTITY_NAME);    
		eo.setGesCode(gesCode);
		eo.setPcoNum(pcoNum);
		eo.setTitEtat(titEtat);
		eo.setTitHt(titHt);
		eo.setTitNumero(titNumero);
		eo.setTitTtc(titTtc);
		eo.setTitTva(titTva);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTitre.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTitre.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOTitre creerInstance(EOEditingContext editingContext) {
		  		EOTitre object = (EOTitre)createAndInsertInstance(editingContext, _EOTitre.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOTitre localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTitre)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOTitre localInstanceIn(EOEditingContext editingContext, EOTitre eo) {
    EOTitre localInstance = (eo == null) ? null : (EOTitre)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTitre#localInstanceIn a la place.
   */
	public static EOTitre localInstanceOf(EOEditingContext editingContext, EOTitre eo) {
		return EOTitre.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOTitre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTitre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTitre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTitre)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTitre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTitre)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOTitre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTitre eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTitre ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTitre fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
