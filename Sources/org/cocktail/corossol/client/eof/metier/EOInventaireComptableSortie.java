/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.corossol.client.eof.metier;

import java.math.BigDecimal;

import com.webobjects.foundation.NSValidation;

public class EOInventaireComptableSortie extends _EOInventaireComptableSortie {

    public EOInventaireComptableSortie() {
        super();
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	if (invcsDate()==null)
			throw new ValidationException("il faut une date de sortie d'inventaire comptable au format jj/mm/aaaa");  	
		if (invcsMotif()==null || invcsMotif().length()==0)
			throw new ValidationException("il faut un motif pour la sortie d'inventaire comptable");
		if (invcsMontantAcquisitionSortie()==null)
			throw new ValidationException("il faut un montant d'acquisition pour la sortie d'inventaire comptable");
		if (invcsVnc()==null)
			throw new ValidationException("il faut une valeur nette comptable pour la sortie d'inventaire comptable");
		if (invcsVnc().floatValue()<0.0)
			throw new ValidationException("la valeur nette comptable de la sortie d'inventaire comptable doit etre > 0");
		if (typeSortie()==null)
			throw new ValidationException("il faut un type de sortie pour la sortie d'inventaire comptable");

		if (invcsVnc().floatValue()>invcsMontantAcquisitionSortie().floatValue())
			throw new ValidationException("la valeur nette comptable de la sortie d'inventaire comptable est supérieure au montant d'acquisition");

		if (invcsValeurCession()==null)
			setInvcsValeurCession(new BigDecimal(0.0));
		if (invcsValeurCession().floatValue()<0.0)
			throw new ValidationException("la valeur de cession du bien doit etre > 0");  
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
