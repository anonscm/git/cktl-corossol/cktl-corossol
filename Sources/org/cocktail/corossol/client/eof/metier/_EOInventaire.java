// _EOInventaire.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOInventaire.java instead.
package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOInventaire extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Inventaire";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.INVENTAIRE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "invId";

	public static final String INV_COMMENTAIRE_KEY = "invCommentaire";
	public static final String INV_DATE_MAINT_KEY = "invDateMaint";
	public static final String INV_DOC_KEY = "invDoc";
	public static final String INV_GAR_KEY = "invGar";
	public static final String INV_MAINT_KEY = "invMaint";
	public static final String INV_MODELE_KEY = "invModele";
	public static final String INV_MONTANT_ACQUISITION_KEY = "invMontantAcquisition";
	public static final String INV_SERIE_KEY = "invSerie";

// Attributs non visibles
	public static final String CLIP_ID_KEY = "clipId";
	public static final String INV_ID_KEY = "invId";
	public static final String INVC_ID_KEY = "invcId";
	public static final String LID_ORDRE_KEY = "lidOrdre";
	public static final String MAG_ID_KEY = "magId";
	public static final String NO_INDIVIDU_RESP_KEY = "noIndividuResp";
	public static final String NO_INDIVIDU_TITULAIRE_KEY = "noIndividuTitulaire";
	public static final String SAL_NUMERO_KEY = "salNumero";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String INV_COMMENTAIRE_COLKEY = "INV_COMMENTAIRE";
	public static final String INV_DATE_MAINT_COLKEY = "INV_DATE_MAINT";
	public static final String INV_DOC_COLKEY = "INV_DOC";
	public static final String INV_GAR_COLKEY = "INV_GAR";
	public static final String INV_MAINT_COLKEY = "INV_MAINT";
	public static final String INV_MODELE_COLKEY = "INV_MODELE";
	public static final String INV_MONTANT_ACQUISITION_COLKEY = "INV_MONTANT_ACQUISITION";
	public static final String INV_SERIE_COLKEY = "INV_SERIE";

	public static final String CLIP_ID_COLKEY = "CLIP_ID";
	public static final String INV_ID_COLKEY = "INV_ID";
	public static final String INVC_ID_COLKEY = "INVC_ID";
	public static final String LID_ORDRE_COLKEY = "LID_ORDRE";
	public static final String MAG_ID_COLKEY = "mag_id";
	public static final String NO_INDIVIDU_RESP_COLKEY = "NO_INDIVIDU_RESP";
	public static final String NO_INDIVIDU_TITULAIRE_COLKEY = "NO_INDIVIDU_TITULAIRE";
	public static final String SAL_NUMERO_COLKEY = "SAL_Numero";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String CLE_INVENTAIRE_PHYSIQUE_KEY = "cleInventairePhysique";
	public static final String INDIVIDU_RESP_KEY = "individuResp";
	public static final String INDIVIDU_TIT_KEY = "individuTit";
	public static final String INVENTAIRE_CBS_KEY = "inventaireCbs";
	public static final String INVENTAIRE_COMPTABLE_KEY = "inventaireComptable";
	public static final String INVENTAIRE_SORTIES_KEY = "inventaireSorties";
	public static final String LIVRAISON_DETAIL_KEY = "livraisonDetail";
	public static final String MAGASIN_KEY = "magasin";
	public static final String SALLES_KEY = "salles";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String invCommentaire() {
    return (String) storedValueForKey(INV_COMMENTAIRE_KEY);
  }

  public void setInvCommentaire(String value) {
    takeStoredValueForKey(value, INV_COMMENTAIRE_KEY);
  }

  public NSTimestamp invDateMaint() {
    return (NSTimestamp) storedValueForKey(INV_DATE_MAINT_KEY);
  }

  public void setInvDateMaint(NSTimestamp value) {
    takeStoredValueForKey(value, INV_DATE_MAINT_KEY);
  }

  public String invDoc() {
    return (String) storedValueForKey(INV_DOC_KEY);
  }

  public void setInvDoc(String value) {
    takeStoredValueForKey(value, INV_DOC_KEY);
  }

  public String invGar() {
    return (String) storedValueForKey(INV_GAR_KEY);
  }

  public void setInvGar(String value) {
    takeStoredValueForKey(value, INV_GAR_KEY);
  }

  public String invMaint() {
    return (String) storedValueForKey(INV_MAINT_KEY);
  }

  public void setInvMaint(String value) {
    takeStoredValueForKey(value, INV_MAINT_KEY);
  }

  public String invModele() {
    return (String) storedValueForKey(INV_MODELE_KEY);
  }

  public void setInvModele(String value) {
    takeStoredValueForKey(value, INV_MODELE_KEY);
  }

  public java.math.BigDecimal invMontantAcquisition() {
    return (java.math.BigDecimal) storedValueForKey(INV_MONTANT_ACQUISITION_KEY);
  }

  public void setInvMontantAcquisition(java.math.BigDecimal value) {
    takeStoredValueForKey(value, INV_MONTANT_ACQUISITION_KEY);
  }

  public String invSerie() {
    return (String) storedValueForKey(INV_SERIE_KEY);
  }

  public void setInvSerie(String value) {
    takeStoredValueForKey(value, INV_SERIE_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOCleInventairePhysique cleInventairePhysique() {
    return (org.cocktail.corossol.client.eof.metier.EOCleInventairePhysique)storedValueForKey(CLE_INVENTAIRE_PHYSIQUE_KEY);
  }

  public void setCleInventairePhysiqueRelationship(org.cocktail.corossol.client.eof.metier.EOCleInventairePhysique value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOCleInventairePhysique oldValue = cleInventairePhysique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CLE_INVENTAIRE_PHYSIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CLE_INVENTAIRE_PHYSIQUE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOIndividuUlr individuResp() {
    return (org.cocktail.application.client.eof.EOIndividuUlr)storedValueForKey(INDIVIDU_RESP_KEY);
  }

  public void setIndividuRespRelationship(org.cocktail.application.client.eof.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOIndividuUlr oldValue = individuResp();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_RESP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_RESP_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOIndividuUlr individuTit() {
    return (org.cocktail.application.client.eof.EOIndividuUlr)storedValueForKey(INDIVIDU_TIT_KEY);
  }

  public void setIndividuTitRelationship(org.cocktail.application.client.eof.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOIndividuUlr oldValue = individuTit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_TIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_TIT_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOInventaireComptable inventaireComptable() {
    return (org.cocktail.corossol.client.eof.metier.EOInventaireComptable)storedValueForKey(INVENTAIRE_COMPTABLE_KEY);
  }

  public void setInventaireComptableRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireComptable value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOInventaireComptable oldValue = inventaireComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INVENTAIRE_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INVENTAIRE_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOLivraisonDetail livraisonDetail() {
    return (org.cocktail.corossol.client.eof.metier.EOLivraisonDetail)storedValueForKey(LIVRAISON_DETAIL_KEY);
  }

  public void setLivraisonDetailRelationship(org.cocktail.corossol.client.eof.metier.EOLivraisonDetail value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOLivraisonDetail oldValue = livraisonDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LIVRAISON_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LIVRAISON_DETAIL_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOMagasin magasin() {
    return (org.cocktail.corossol.client.eof.metier.EOMagasin)storedValueForKey(MAGASIN_KEY);
  }

  public void setMagasinRelationship(org.cocktail.corossol.client.eof.metier.EOMagasin value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOMagasin oldValue = magasin();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MAGASIN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MAGASIN_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOSalles salles() {
    return (org.cocktail.application.client.eof.EOSalles)storedValueForKey(SALLES_KEY);
  }

  public void setSallesRelationship(org.cocktail.application.client.eof.EOSalles value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOSalles oldValue = salles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SALLES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SALLES_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.client.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.application.client.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray inventaireCbs() {
    return (NSArray)storedValueForKey(INVENTAIRE_CBS_KEY);
  }

  public NSArray inventaireCbs(EOQualifier qualifier) {
    return inventaireCbs(qualifier, null, false);
  }

  public NSArray inventaireCbs(EOQualifier qualifier, boolean fetch) {
    return inventaireCbs(qualifier, null, fetch);
  }

  public NSArray inventaireCbs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.client.eof.metier.EOInventaireCb.INVENTAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.client.eof.metier.EOInventaireCb.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inventaireCbs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInventaireCbsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireCb object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRE_CBS_KEY);
  }

  public void removeFromInventaireCbsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireCb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_CBS_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOInventaireCb createInventaireCbsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("InventaireCb");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRE_CBS_KEY);
    return (org.cocktail.corossol.client.eof.metier.EOInventaireCb) eo;
  }

  public void deleteInventaireCbsRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireCb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_CBS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventaireCbsRelationships() {
    Enumeration objects = inventaireCbs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventaireCbsRelationship((org.cocktail.corossol.client.eof.metier.EOInventaireCb)objects.nextElement());
    }
  }

  public NSArray inventaireSorties() {
    return (NSArray)storedValueForKey(INVENTAIRE_SORTIES_KEY);
  }

  public NSArray inventaireSorties(EOQualifier qualifier) {
    return inventaireSorties(qualifier, null, false);
  }

  public NSArray inventaireSorties(EOQualifier qualifier, boolean fetch) {
    return inventaireSorties(qualifier, null, fetch);
  }

  public NSArray inventaireSorties(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.corossol.client.eof.metier.EOInventaireSortie.INVENTAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.corossol.client.eof.metier.EOInventaireSortie.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = inventaireSorties();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToInventaireSortiesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireSortie object) {
    addObjectToBothSidesOfRelationshipWithKey(object, INVENTAIRE_SORTIES_KEY);
  }

  public void removeFromInventaireSortiesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireSortie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_SORTIES_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOInventaireSortie createInventaireSortiesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("InventaireSortie");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, INVENTAIRE_SORTIES_KEY);
    return (org.cocktail.corossol.client.eof.metier.EOInventaireSortie) eo;
  }

  public void deleteInventaireSortiesRelationship(org.cocktail.corossol.client.eof.metier.EOInventaireSortie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, INVENTAIRE_SORTIES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllInventaireSortiesRelationships() {
    Enumeration objects = inventaireSorties().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteInventaireSortiesRelationship((org.cocktail.corossol.client.eof.metier.EOInventaireSortie)objects.nextElement());
    }
  }


  public static EOInventaire createInventaire(EOEditingContext editingContext) {
    EOInventaire eo = (EOInventaire) createAndInsertInstance(editingContext, _EOInventaire.ENTITY_NAME);    
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOInventaire.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOInventaire.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOInventaire creerInstance(EOEditingContext editingContext) {
		  		EOInventaire object = (EOInventaire)createAndInsertInstance(editingContext, _EOInventaire.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOInventaire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOInventaire)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOInventaire localInstanceIn(EOEditingContext editingContext, EOInventaire eo) {
    EOInventaire localInstance = (eo == null) ? null : (EOInventaire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOInventaire#localInstanceIn a la place.
   */
	public static EOInventaire localInstanceOf(EOEditingContext editingContext, EOInventaire eo) {
		return EOInventaire.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOInventaire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOInventaire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOInventaire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOInventaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOInventaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOInventaire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOInventaire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOInventaire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOInventaire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOInventaire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
