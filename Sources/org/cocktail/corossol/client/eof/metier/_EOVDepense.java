// _EOVDepense.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVDepense.java instead.
package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVDepense extends  EOGenericRecord {
	public static final String ENTITY_NAME = "VDepense";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.V_DEPENSE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dpcoId";

	public static final String BOR_NUM_KEY = "borNum";
	public static final String DPCO_HT_SAISIE_KEY = "dpcoHtSaisie";
	public static final String DPCO_ID_BIS_KEY = "dpcoIdBis";
	public static final String DPCO_MONTANT_BUDGETAIRE_KEY = "dpcoMontantBudgetaire";
	public static final String DPCO_TTC_SAISIE_KEY = "dpcoTtcSaisie";
	public static final String DPCO_TVA_SAISIE_KEY = "dpcoTvaSaisie";
	public static final String DPP_DATE_FACTURE_KEY = "dppDateFacture";
	public static final String DPP_DATE_RECEPTION_KEY = "dppDateReception";
	public static final String DPP_DATE_SAISIE_KEY = "dppDateSaisie";
	public static final String DPP_DATE_SERVICE_FAIT_KEY = "dppDateServiceFait";
	public static final String DPP_HT_INITIAL_KEY = "dppHtInitial";
	public static final String DPP_HT_SAISIE_KEY = "dppHtSaisie";
	public static final String DPP_ID_KEY = "dppId";
	public static final String DPP_ID_REVERSEMENT_KEY = "dppIdReversement";
	public static final String DPP_NB_PIECE_KEY = "dppNbPiece";
	public static final String DPP_NUMERO_FACTURE_KEY = "dppNumeroFacture";
	public static final String DPP_TTC_INITIAL_KEY = "dppTtcInitial";
	public static final String DPP_TTC_SAISIE_KEY = "dppTtcSaisie";
	public static final String DPP_TVA_INITIAL_KEY = "dppTvaInitial";
	public static final String DPP_TVA_SAISIE_KEY = "dppTvaSaisie";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MAN_NUMERO_KEY = "manNumero";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

// Attributs non visibles
	public static final String COMM_ID_KEY = "commId";
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String BOR_NUM_COLKEY = "BOR_NUM";
	public static final String DPCO_HT_SAISIE_COLKEY = "DPCO_HT_SAISIE";
	public static final String DPCO_ID_BIS_COLKEY = "DPCO_ID";
	public static final String DPCO_MONTANT_BUDGETAIRE_COLKEY = "DPCO_MONTANT_BUDGETAIRE";
	public static final String DPCO_TTC_SAISIE_COLKEY = "DPCO_TTC_SAISIE";
	public static final String DPCO_TVA_SAISIE_COLKEY = "DPCO_TVA_SAISIE";
	public static final String DPP_DATE_FACTURE_COLKEY = "DPP_DATE_FACTURE";
	public static final String DPP_DATE_RECEPTION_COLKEY = "DPP_DATE_RECEPTION";
	public static final String DPP_DATE_SAISIE_COLKEY = "DPP_DATE_SAISIE";
	public static final String DPP_DATE_SERVICE_FAIT_COLKEY = "DPP_DATE_SERVICE_FAIT";
	public static final String DPP_HT_INITIAL_COLKEY = "DPP_HT_INITIAL";
	public static final String DPP_HT_SAISIE_COLKEY = "DPP_HT_SAISIE";
	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String DPP_ID_REVERSEMENT_COLKEY = "DPP_ID_REVERSEMENT";
	public static final String DPP_NB_PIECE_COLKEY = "DPP_NB_PIECE";
	public static final String DPP_NUMERO_FACTURE_COLKEY = "DPP_NUMERO_FACTURE";
	public static final String DPP_TTC_INITIAL_COLKEY = "DPP_TTC_INITIAL";
	public static final String DPP_TTC_SAISIE_COLKEY = "DPP_TTC_SAISIE";
	public static final String DPP_TVA_INITIAL_COLKEY = "DPP_TVA_INITIAL";
	public static final String DPP_TVA_SAISIE_COLKEY = "DPP_TVA_SAISIE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String MAN_NUMERO_COLKEY = "MAN_NUMERO";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";

	public static final String COMM_ID_COLKEY = "COMM_ID";
	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String CA__ORGAN_KEY = "ca_Organ";
	public static final String CA__PLAN_COMPTABLE_KEY = "ca_PlanComptable";
	public static final String V_COMMANDE_KEY = "vCommande";



	// Accessors methods
  public Integer borNum() {
    return (Integer) storedValueForKey(BOR_NUM_KEY);
  }

  public void setBorNum(Integer value) {
    takeStoredValueForKey(value, BOR_NUM_KEY);
  }

  public java.math.BigDecimal dpcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_HT_SAISIE_KEY);
  }

  public void setDpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_HT_SAISIE_KEY);
  }

  public Integer dpcoIdBis() {
    return (Integer) storedValueForKey(DPCO_ID_BIS_KEY);
  }

  public void setDpcoIdBis(Integer value) {
    takeStoredValueForKey(value, DPCO_ID_BIS_KEY);
  }

  public java.math.BigDecimal dpcoMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDpcoMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal dpcoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_TTC_SAISIE_KEY);
  }

  public void setDpcoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal dpcoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_TVA_SAISIE_KEY);
  }

  public void setDpcoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_TVA_SAISIE_KEY);
  }

  public NSTimestamp dppDateFacture() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_FACTURE_KEY);
  }

  public void setDppDateFacture(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_FACTURE_KEY);
  }

  public NSTimestamp dppDateReception() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_RECEPTION_KEY);
  }

  public void setDppDateReception(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_RECEPTION_KEY);
  }

  public NSTimestamp dppDateSaisie() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_SAISIE_KEY);
  }

  public void setDppDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_SAISIE_KEY);
  }

  public NSTimestamp dppDateServiceFait() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_SERVICE_FAIT_KEY);
  }

  public void setDppDateServiceFait(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_SERVICE_FAIT_KEY);
  }

  public java.math.BigDecimal dppHtInitial() {
    return (java.math.BigDecimal) storedValueForKey(DPP_HT_INITIAL_KEY);
  }

  public void setDppHtInitial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_HT_INITIAL_KEY);
  }

  public java.math.BigDecimal dppHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPP_HT_SAISIE_KEY);
  }

  public void setDppHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_HT_SAISIE_KEY);
  }

  public Integer dppId() {
    return (Integer) storedValueForKey(DPP_ID_KEY);
  }

  public void setDppId(Integer value) {
    takeStoredValueForKey(value, DPP_ID_KEY);
  }

  public Integer dppIdReversement() {
    return (Integer) storedValueForKey(DPP_ID_REVERSEMENT_KEY);
  }

  public void setDppIdReversement(Integer value) {
    takeStoredValueForKey(value, DPP_ID_REVERSEMENT_KEY);
  }

  public Integer dppNbPiece() {
    return (Integer) storedValueForKey(DPP_NB_PIECE_KEY);
  }

  public void setDppNbPiece(Integer value) {
    takeStoredValueForKey(value, DPP_NB_PIECE_KEY);
  }

  public String dppNumeroFacture() {
    return (String) storedValueForKey(DPP_NUMERO_FACTURE_KEY);
  }

  public void setDppNumeroFacture(String value) {
    takeStoredValueForKey(value, DPP_NUMERO_FACTURE_KEY);
  }

  public java.math.BigDecimal dppTtcInitial() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TTC_INITIAL_KEY);
  }

  public void setDppTtcInitial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TTC_INITIAL_KEY);
  }

  public java.math.BigDecimal dppTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TTC_SAISIE_KEY);
  }

  public void setDppTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal dppTvaInitial() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TVA_INITIAL_KEY);
  }

  public void setDppTvaInitial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TVA_INITIAL_KEY);
  }

  public java.math.BigDecimal dppTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPP_TVA_SAISIE_KEY);
  }

  public void setDppTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPP_TVA_SAISIE_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public Integer manNumero() {
    return (Integer) storedValueForKey(MAN_NUMERO_KEY);
  }

  public void setManNumero(Integer value) {
    takeStoredValueForKey(value, MAN_NUMERO_KEY);
  }

  public Integer modOrdre() {
    return (Integer) storedValueForKey(MOD_ORDRE_KEY);
  }

  public void setModOrdre(Integer value) {
    takeStoredValueForKey(value, MOD_ORDRE_KEY);
  }

  public Integer ribOrdre() {
    return (Integer) storedValueForKey(RIB_ORDRE_KEY);
  }

  public void setRibOrdre(Integer value) {
    takeStoredValueForKey(value, RIB_ORDRE_KEY);
  }

  public Integer tcdOrdre() {
    return (Integer) storedValueForKey(TCD_ORDRE_KEY);
  }

  public void setTcdOrdre(Integer value) {
    takeStoredValueForKey(value, TCD_ORDRE_KEY);
  }

  public Integer utlOrdre() {
    return (Integer) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Integer value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public org.cocktail.application.client.eof.EOOrgan ca_Organ() {
    return (org.cocktail.application.client.eof.EOOrgan)storedValueForKey(CA__ORGAN_KEY);
  }

  public void setCa_OrganRelationship(org.cocktail.application.client.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOOrgan oldValue = ca_Organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CA__ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CA__ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOPlanComptable ca_PlanComptable() {
    return (org.cocktail.application.client.eof.EOPlanComptable)storedValueForKey(CA__PLAN_COMPTABLE_KEY);
  }

  public void setCa_PlanComptableRelationship(org.cocktail.application.client.eof.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOPlanComptable oldValue = ca_PlanComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CA__PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CA__PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOVCommande vCommande() {
    return (org.cocktail.corossol.client.eof.metier.EOVCommande)storedValueForKey(V_COMMANDE_KEY);
  }

  public void setVCommandeRelationship(org.cocktail.corossol.client.eof.metier.EOVCommande value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOVCommande oldValue = vCommande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_COMMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_COMMANDE_KEY);
    }
  }
  

  public static EOVDepense createVDepense(EOEditingContext editingContext, java.math.BigDecimal dpcoHtSaisie
, java.math.BigDecimal dpcoMontantBudgetaire
, java.math.BigDecimal dpcoTtcSaisie
, java.math.BigDecimal dpcoTvaSaisie
, NSTimestamp dppDateFacture
, NSTimestamp dppDateReception
, NSTimestamp dppDateSaisie
, NSTimestamp dppDateServiceFait
, java.math.BigDecimal dppHtInitial
, java.math.BigDecimal dppHtSaisie
, Integer dppId
, Integer dppNbPiece
, String dppNumeroFacture
, java.math.BigDecimal dppTtcInitial
, java.math.BigDecimal dppTtcSaisie
, java.math.BigDecimal dppTvaInitial
, java.math.BigDecimal dppTvaSaisie
, Integer exeOrdre
, Integer fouOrdre
, Integer tcdOrdre
, Integer utlOrdre
) {
    EOVDepense eo = (EOVDepense) createAndInsertInstance(editingContext, _EOVDepense.ENTITY_NAME);    
		eo.setDpcoHtSaisie(dpcoHtSaisie);
		eo.setDpcoMontantBudgetaire(dpcoMontantBudgetaire);
		eo.setDpcoTtcSaisie(dpcoTtcSaisie);
		eo.setDpcoTvaSaisie(dpcoTvaSaisie);
		eo.setDppDateFacture(dppDateFacture);
		eo.setDppDateReception(dppDateReception);
		eo.setDppDateSaisie(dppDateSaisie);
		eo.setDppDateServiceFait(dppDateServiceFait);
		eo.setDppHtInitial(dppHtInitial);
		eo.setDppHtSaisie(dppHtSaisie);
		eo.setDppId(dppId);
		eo.setDppNbPiece(dppNbPiece);
		eo.setDppNumeroFacture(dppNumeroFacture);
		eo.setDppTtcInitial(dppTtcInitial);
		eo.setDppTtcSaisie(dppTtcSaisie);
		eo.setDppTvaInitial(dppTvaInitial);
		eo.setDppTvaSaisie(dppTvaSaisie);
		eo.setExeOrdre(exeOrdre);
		eo.setFouOrdre(fouOrdre);
		eo.setTcdOrdre(tcdOrdre);
		eo.setUtlOrdre(utlOrdre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVDepense.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVDepense.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOVDepense creerInstance(EOEditingContext editingContext) {
		  		EOVDepense object = (EOVDepense)createAndInsertInstance(editingContext, _EOVDepense.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOVDepense localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVDepense)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVDepense localInstanceIn(EOEditingContext editingContext, EOVDepense eo) {
    EOVDepense localInstance = (eo == null) ? null : (EOVDepense)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVDepense#localInstanceIn a la place.
   */
	public static EOVDepense localInstanceOf(EOEditingContext editingContext, EOVDepense eo) {
		return EOVDepense.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVDepense fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVDepense fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVDepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVDepense)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVDepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVDepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVDepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVDepense)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVDepense fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVDepense eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVDepense ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVDepense fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
