// _EOVTitreRecette.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVTitreRecette.java instead.
package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVTitreRecette extends  EOGenericRecord {
	public static final String ENTITY_NAME = "VTitreRecette";
	public static final String ENTITY_TABLE_NAME = "JEFY_INVENTAIRE.V_TITRE_RECETTE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "titId";

	public static final String GES_CODE_KEY = "gesCode";
	public static final String MONTANT_DISPONIBLE_KEY = "montantDisponible";
	public static final String MONTANT_REDUCTION_KEY = "montantReduction";
	public static final String MONTANT_UTILISE_KEY = "montantUtilise";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TIT_HT_KEY = "titHt";
	public static final String TIT_LIBELLE_KEY = "titLibelle";
	public static final String TIT_NUMERO_KEY = "titNumero";
	public static final String TIT_TTC_KEY = "titTtc";

// Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String TIT_ID_KEY = "titId";

//Colonnes dans la base de donnees
	public static final String GES_CODE_COLKEY = "GES_CODE";
	public static final String MONTANT_DISPONIBLE_COLKEY = "MONTANT_DISPONIBLE";
	public static final String MONTANT_REDUCTION_COLKEY = "MONTANT_REDUCTION";
	public static final String MONTANT_UTILISE_COLKEY = "MONTANT_UTILISE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TIT_HT_COLKEY = "TIT_HT";
	public static final String TIT_LIBELLE_COLKEY = "TIT_LIBELLE";
	public static final String TIT_NUMERO_COLKEY = "TIT_NUMERO";
	public static final String TIT_TTC_COLKEY = "TIT_TTC";

	public static final String BOR_ID_COLKEY = "BOR_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "ORG_ORDRE";
	public static final String TIT_ID_COLKEY = "TIT_ID";


	// Relationships
	public static final String BORDEREAU_KEY = "bordereau";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TITRE_KEY = "titre";
	public static final String V_FOURNISSEUR_KEY = "vFournisseur";



	// Accessors methods
  public String gesCode() {
    return (String) storedValueForKey(GES_CODE_KEY);
  }

  public void setGesCode(String value) {
    takeStoredValueForKey(value, GES_CODE_KEY);
  }

  public java.math.BigDecimal montantDisponible() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_DISPONIBLE_KEY);
  }

  public void setMontantDisponible(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_DISPONIBLE_KEY);
  }

  public java.math.BigDecimal montantReduction() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_REDUCTION_KEY);
  }

  public void setMontantReduction(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_REDUCTION_KEY);
  }

  public java.math.BigDecimal montantUtilise() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_UTILISE_KEY);
  }

  public void setMontantUtilise(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_UTILISE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public java.math.BigDecimal titHt() {
    return (java.math.BigDecimal) storedValueForKey(TIT_HT_KEY);
  }

  public void setTitHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_HT_KEY);
  }

  public String titLibelle() {
    return (String) storedValueForKey(TIT_LIBELLE_KEY);
  }

  public void setTitLibelle(String value) {
    takeStoredValueForKey(value, TIT_LIBELLE_KEY);
  }

  public Double titNumero() {
    return (Double) storedValueForKey(TIT_NUMERO_KEY);
  }

  public void setTitNumero(Double value) {
    takeStoredValueForKey(value, TIT_NUMERO_KEY);
  }

  public java.math.BigDecimal titTtc() {
    return (java.math.BigDecimal) storedValueForKey(TIT_TTC_KEY);
  }

  public void setTitTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_TTC_KEY);
  }

  public org.cocktail.corossol.client.eof.metier.EOBordereau bordereau() {
    return (org.cocktail.corossol.client.eof.metier.EOBordereau)storedValueForKey(BORDEREAU_KEY);
  }

  public void setBordereauRelationship(org.cocktail.corossol.client.eof.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOBordereau oldValue = bordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOOrgan organ() {
    return (org.cocktail.application.client.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.client.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.corossol.client.eof.metier.EOTitre titre() {
    return (org.cocktail.corossol.client.eof.metier.EOTitre)storedValueForKey(TITRE_KEY);
  }

  public void setTitreRelationship(org.cocktail.corossol.client.eof.metier.EOTitre value) {
    if (value == null) {
    	org.cocktail.corossol.client.eof.metier.EOTitre oldValue = titre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITRE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.VFournisseur vFournisseur() {
    return (org.cocktail.application.client.eof.VFournisseur)storedValueForKey(V_FOURNISSEUR_KEY);
  }

  public void setVFournisseurRelationship(org.cocktail.application.client.eof.VFournisseur value) {
    if (value == null) {
    	org.cocktail.application.client.eof.VFournisseur oldValue = vFournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, V_FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, V_FOURNISSEUR_KEY);
    }
  }
  

  public static EOVTitreRecette createVTitreRecette(EOEditingContext editingContext, String gesCode
, String pcoNum
, java.math.BigDecimal titHt
, Double titNumero
, java.math.BigDecimal titTtc
, org.cocktail.corossol.client.eof.metier.EOBordereau bordereau, org.cocktail.corossol.client.eof.metier.EOTitre titre) {
    EOVTitreRecette eo = (EOVTitreRecette) createAndInsertInstance(editingContext, _EOVTitreRecette.ENTITY_NAME);    
		eo.setGesCode(gesCode);
		eo.setPcoNum(pcoNum);
		eo.setTitHt(titHt);
		eo.setTitNumero(titNumero);
		eo.setTitTtc(titTtc);
    eo.setBordereauRelationship(bordereau);
    eo.setTitreRelationship(titre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVTitreRecette.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVTitreRecette.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOVTitreRecette creerInstance(EOEditingContext editingContext) {
		  		EOVTitreRecette object = (EOVTitreRecette)createAndInsertInstance(editingContext, _EOVTitreRecette.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOVTitreRecette localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVTitreRecette)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVTitreRecette localInstanceIn(EOEditingContext editingContext, EOVTitreRecette eo) {
    EOVTitreRecette localInstance = (eo == null) ? null : (EOVTitreRecette)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVTitreRecette#localInstanceIn a la place.
   */
	public static EOVTitreRecette localInstanceOf(EOEditingContext editingContext, EOVTitreRecette eo) {
		return EOVTitreRecette.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVTitreRecette fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVTitreRecette fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVTitreRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVTitreRecette)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVTitreRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVTitreRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVTitreRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVTitreRecette)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVTitreRecette fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVTitreRecette eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVTitreRecette ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVTitreRecette fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
