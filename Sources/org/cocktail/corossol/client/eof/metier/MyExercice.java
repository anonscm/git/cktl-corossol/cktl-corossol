package org.cocktail.corossol.client.eof.metier;

import org.cocktail.application.client.eof.EOExercice;

public class MyExercice {
    private EOExercice exercice;
    
    public MyExercice(EOExercice lexercice) {
    	super();
    	exercice=lexercice;
    }
    
    public String toString() {
    	return exercice.exeExercice().toString();
    }
    
    public EOExercice exercice() {
    	return exercice;
    }
}
