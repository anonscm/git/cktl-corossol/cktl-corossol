/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.corossol.client.eof.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.*;

import org.cocktail.corossol.client.eof.factory.FactoryInventaireCb;
import org.cocktail.corossol.client.eof.metier.EOInventaireCb;

public class EOInventaire extends _EOInventaire {
	
	static public String STRING_IS_SORTIE="stringIsSortie";
	public static final Integer DPCO_ID_HORS_BUDGET = -1000;
    
	public EOInventaire() {
        super();
    }
    

	public String resume() {
		return inventaireComptable().cleInventaireComptable().clicNumComplet()+", commande :"+livraisonDetail().livraison().commande().commNumero()+"\n"+
		livraisonDetail().lidLibelle()+"\n Num série :"+invSerie()+"\n Montant acquisition : "+invMontantAcquisition();
	}

	public String codeBarre() {
		if (inventaireCbs()==null || inventaireCbs().count()==0)
			return "";
		return ((EOInventaireCb)inventaireCbs().objectAtIndex(0)).incbCode();
	}

	public void setCodeBarre(String code) {
		if (inventaireCbs()!=null && inventaireCbs().count()>0) {
			if (code==null || code.equals("")) {
				editingContext().deleteObject((EOInventaireCb)inventaireCbs().objectAtIndex(0));
			} else 
				((EOInventaireCb)inventaireCbs().objectAtIndex(0)).setIncbCode(code);
			return;
		}
		try {
			if (code!=null && !code.equals(""))
				new FactoryInventaireCb(false).insertInventaire(editingContext(), this, code, "");
		} catch (Exception e) {
			System.out.println(e);
			return;
		}
	}

	public String stringIsSortie() {
		if (inventaireSorties()!=null && inventaireSorties().count()>0)
			return "O";
		return "";
	}
	
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
