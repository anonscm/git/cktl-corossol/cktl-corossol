package org.cocktail.corossol.client.eof.factory;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.tools.Factory;
import org.cocktail.corossol.client.eof.metier.EOCleInventairePhysique;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryCleInventairePhysique extends Factory {

	public FactoryCleInventairePhysique() {
		
	}

	public FactoryCleInventairePhysique(boolean arg0) {
		super(arg0);
	}

	public EOCleInventairePhysique insertCleInventairePhysique(EOEditingContext monEOEditingContext, EOExercice setExercice, String setClipNumero,
			Integer setClipNbEtiquette, String setClipEtat) throws Exception {

		try {
			
			EOCleInventairePhysique newCleInventairePhysique = EOCleInventairePhysique.createCleInventairePhysique(monEOEditingContext);
			
			System.out.println("FactoryCleInventairePhysique.insertCleInventairePhysique() ==> " + newCleInventairePhysique);

			newCleInventairePhysique.setExerciceRelationship(setExercice);
			newCleInventairePhysique.setClipNumero(setClipNumero);
			newCleInventairePhysique.setClipNbEtiquette(setClipNbEtiquette);
			newCleInventairePhysique.setClipEtat(setClipEtat);

			return newCleInventairePhysique;
			
		} catch (Exception e) {
			throw e;
		}
	}

	public void updateCleInventairePhysique(EOEditingContext monEOEditingContext, EOCleInventairePhysique setCleInventairePhysique,
			Integer setClipNbEtiquette) throws Exception {
		try {
			setCleInventairePhysique.setClipNbEtiquette(setClipNbEtiquette);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void deleteCleInventairePhysique(EOEditingContext monEOEditingContext, EOCleInventairePhysique setCleInventairePhysique) throws Exception {
		try {
			monEOEditingContext.deleteObject(setCleInventairePhysique);
		} catch (Exception e) {
			throw e;
		}
	}
}
