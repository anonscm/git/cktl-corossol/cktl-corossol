package org.cocktail.corossol.client.eof.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.tools.Factory;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.nib.DateCtrl;

import com.webobjects.foundation.NSTimestamp;


public class FactoryAmortissement extends Factory {

	public FactoryAmortissement() {

	}

	public FactoryAmortissement(boolean arg0) {
		super(arg0);
	}

	public static BigDecimal precalculVNC(EOInventaire inventaire, NSTimestamp dateSortie) {
		if (inventaire==null)
			return new BigDecimal(0.0);
		return precalculVNC(inventaire.inventaireComptable(), inventaire.invMontantAcquisition(), dateSortie);
	}

	public static BigDecimal precalculVNC(EOInventaireComptable inventaireComptable, BigDecimal montant, NSTimestamp dateSortie) {
		if (inventaireComptable.cleInventaireComptable().clicTypeAmort().equals("Lineaire"))
			return precalculVNCLineaire(inventaireComptable, montant, dateSortie);
		return precalculVNCDegressif(inventaireComptable, montant, dateSortie);
	}

	public static BigDecimal precalculVNCLineaire(EOInventaireComptable inventaireComptable, BigDecimal montant, NSTimestamp dateSortie) {
		if (inventaireComptable==null)
			return new BigDecimal(0.0);

		int annee=new Integer(DateCtrl.dateToString(dateSortie, "%Y")).intValue();
			
		Number duree=inventaireComptable.cleInventaireComptable().clicDureeAmort();
		BigDecimal annuite=montant.divide(new BigDecimal(duree.toString()), BigDecimal.ROUND_HALF_UP);

		int nbAnnees=annee-inventaireComptable.exercice().exeExercice().intValue();

		if (nbAnnees<=0)
			return montant;
		if (nbAnnees>duree.intValue())
			return new BigDecimal(0.0);

		BigDecimal reste;
		if (inventaireComptable.cleInventaireComptable().clicProRata().intValue()==0) {
			if (nbAnnees>=duree.intValue())
				return new BigDecimal(0.0);

			reste=montant.subtract(annuite.multiply(new BigDecimal(nbAnnees)));
		}
		else {
			BigDecimal montantAmorti=new BigDecimal(0.0);
			montantAmorti=montantAmorti.add(annuite.multiply(new BigDecimal(nbAnnees-1)));
			montantAmorti=montantAmorti.add(annuite.multiply(tauxPremiereAnnee(inventaireComptable.exercice(), inventaireComptable.invcDateAcquisition()))
					.setScale(2,BigDecimal.ROUND_UP));
			montantAmorti=montantAmorti.add(annuite.multiply(tauxAmorti(annuite, dateSortie)).setScale(2,BigDecimal.ROUND_UP));
			
			reste=montant.subtract(montantAmorti);
		}

		if (reste.floatValue()<0)
			return new BigDecimal(0.0);

		return reste;
	}

	public static BigDecimal tauxPremiereAnnee(EOExercice exercice, NSTimestamp date) {
		if (date==null)
			return new BigDecimal(1.0);

		int annee=new Integer(DateCtrl.dateToString(date, "%Y")).intValue();
		if (exercice.exeExercice().intValue()>annee) 
			new BigDecimal(1.0);

		double nbJours=360.0 - (new Integer(DateCtrl.dateToString(date, "%m")).floatValue()-1)*30 - 
		     (new Integer(DateCtrl.dateToString(date, "%d")).floatValue()-1);
		if (nbJours>360)
			nbJours=360.0;
		if (nbJours<0)
			nbJours=0.0;

		return new BigDecimal(nbJours/360.0).setScale(5,BigDecimal.ROUND_UP);
	}

	public static BigDecimal tauxAmorti(BigDecimal annuite, NSTimestamp date) {
		if (date==null)
			return new BigDecimal(1.0);

		double nbJours=(new Integer(DateCtrl.dateToString(date, "%m")).floatValue()-1)*30 + 
		     (new Integer(DateCtrl.dateToString(date, "%d")).floatValue()-1);
		if (nbJours>360)
			nbJours=360.0;
		if (nbJours<0)
			nbJours=0.0;

		return new BigDecimal(nbJours/360.0).setScale(5,BigDecimal.ROUND_UP);
	}

	public static BigDecimal precalculVNCDegressif(EOInventaireComptable inventaireComptable, BigDecimal montant, NSTimestamp dateSortie) {
		return precalculVNCLineaire(inventaireComptable, montant, dateSortie);
	}	
}
