package org.cocktail.corossol.client.eof.factory;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOPlanComptableAmo;
import org.cocktail.application.client.tools.Factory;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

public class FactoryCleInventaireComptable extends Factory {

	public FactoryCleInventaireComptable() {
		
	}

	public FactoryCleInventaireComptable(boolean arg0) {
		super(arg0);
	}

	public EOCleInventaireComptable insertCleInventaireComptable(EOEditingContext monEOEditingContext, EOPlanComptableAmo setPlanComptableAmort,
			EOPlanComptable setPlanComptable, EOExercice setExercice, String setClicTypeAmort, Integer setClicProRata, String setClicNumero,
			Integer setClicNbEtiquette, String setClicEtat, Integer setClicDureeAmort, String setClicCr, String setClicComp) throws Exception {

		try {
			EOCleInventaireComptable newCleInventaireComptable = EOCleInventaireComptable.createCleInventaireComptable(monEOEditingContext);
			
			System.out.println(setPlanComptableAmort.pcoaNum()); 
			
			newCleInventaireComptable.setPlanComptableAmortRelationship(setPlanComptableAmort);
			newCleInventaireComptable.setPlanComptableRelationship(setPlanComptable);
			newCleInventaireComptable.setExerciceRelationship(setExercice);

			newCleInventaireComptable.setClicTypeAmort(setClicTypeAmort);
			newCleInventaireComptable.setClicProRata(setClicProRata);
			newCleInventaireComptable.setClicNumero(setClicNumero);
			newCleInventaireComptable.setClicNbEtiquette(setClicNbEtiquette);
			newCleInventaireComptable.setClicEtat(setClicEtat);
			newCleInventaireComptable.setClicDureeAmort(setClicDureeAmort);
			newCleInventaireComptable.setClicCr(setClicCr);
			newCleInventaireComptable.setClicComp(setClicComp);

			newCleInventaireComptable.setClicNumComplet(setClicNumero);

			return newCleInventaireComptable;
			
		} catch (Exception e) {
			throw e;
		}
	}

	public void updateCleInventaireComptable(EOEditingContext monEOEditingContext, EOCleInventaireComptable setCleInventaireComptable, String setClicTypeAmort,
			Integer setClicProRata, Integer setClicNbEtiquette, String setClicEtat, Integer setClicDureeAmort) throws Exception {

		try {
			System.out.println("FactoryCleInventaireComptable.updateCleInventaireComptable() ==> " + setCleInventaireComptable);
			setCleInventaireComptable.setClicTypeAmort(setClicTypeAmort);
			setCleInventaireComptable.setClicProRata(setClicProRata);
			setCleInventaireComptable.setClicNbEtiquette(setClicNbEtiquette);
			setCleInventaireComptable.setClicEtat(setClicEtat);
			setCleInventaireComptable.setClicDureeAmort(setClicDureeAmort);
		} catch (Exception e) {
			throw e;
		}
	}

	public void deleteCleInventaireComptable(EOEditingContext monEOEditingContext, EOCleInventaireComptable setCleInventaireComptable) throws Exception {
		try {
			System.out.println("FactoryCleInventaireComptable.deleteCleInventaireComptable() ==> " + setCleInventaireComptable);
			
			NSArray array=setCleInventaireComptable.cleInventaireComptModifs();
			if (array!=null && array.count()>0) {
				for (int i=array.count()-1; i>=0; i--)
					monEOEditingContext.deleteObject((EOEnterpriseObject)array.objectAtIndex(i));
			}
			
			monEOEditingContext.deleteObject(setCleInventaireComptable);
		} catch (Exception e) {
			throw e;
		}
	}
}
