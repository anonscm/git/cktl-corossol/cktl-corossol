package org.cocktail.corossol.client.eof.factory;

import org.cocktail.application.client.eof.EODevise;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.Factory;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOMagasin;
import org.cocktail.corossol.client.eof.metier.EOVCommande;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryLivraison extends Factory {

	private static final String ERROR_DEL_LIVRAISON_DETAILS = "IMPOSSIBLE , IL EXISTE DES LIGNES DE LIVRAISON";

	public FactoryLivraison() {
	}

	public FactoryLivraison(boolean arg0) {
		super(arg0);
	}

	public EOLivraison insertLivraison(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur, EODevise setDevise, EOExercice setExercice,
			EOVCommande setCommande, EOMagasin setMagasin, String setLivReception, String setLivNumero, String setLivDivers, NSTimestamp setLivDate) throws Exception {
		try {
			EOLivraison newLivraison=EOLivraison.createLivraison(monEOEditingContext, setLivDate);
			System.out.println("FactoryLivraison.insertLivraison() ==> ");

			newLivraison.setUtilisateurRelationship(setUtilisateur);
			newLivraison.setExerciceRelationship(setExercice);
			newLivraison.setDeviseRelationship(setDevise);
			newLivraison.setMagasinRelationship(setMagasin);
			newLivraison.setCommandeRelationship(setCommande);

			newLivraison.setLivReception(setLivReception);
			newLivraison.setLivNumero(setLivNumero);
			newLivraison.setLivDivers(setLivDivers);
			newLivraison.setLivDate(setLivDate);
			
			return newLivraison;
		} catch (Exception e) {
			throw e;
		}
	}

	public void updateLivraison(EOEditingContext monEOEditingContext, EOLivraison maLivraison, EOUtilisateur setUtilisateur, EOMagasin setMagasin,
			String setLivReception, String setLivNumero, String setLivDivers, NSTimestamp setLivDate) throws Exception {
		try {
			System.out.println("FactoryLivraison.updateLivraison() ==> " + maLivraison);

			maLivraison.setUtilisateurRelationship(setUtilisateur);
			maLivraison.setMagasinRelationship(setMagasin);

			maLivraison.setLivReception(setLivReception);
			maLivraison.setLivNumero(setLivNumero);
			maLivraison.setLivDivers(setLivDivers);
			maLivraison.setLivDate(setLivDate);
		} catch (Exception e) {
			throw e;
		}
	}

	public void deleteLivraison(EOEditingContext monEOEditingContext, EOLivraison maLivraison) throws Exception {
		try {
			System.out.println("FactoryLivraison.updateLivraison() ==> " + maLivraison);

			if(maLivraison.livraisonDetails().count() > 0)
				throw new Exception(ERROR_DEL_LIVRAISON_DETAILS);
			monEOEditingContext.deleteObject(maLivraison);
		} catch (Exception e) {
			throw e;
		}
	}
}
