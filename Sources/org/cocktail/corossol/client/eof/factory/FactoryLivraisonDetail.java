package org.cocktail.corossol.client.eof.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.application.client.tools.Factory;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryLivraisonDetail extends Factory {
	
	private static final String  ERROR_DEL_INVENTAIRE = "IMPOSSBLE, IL EXISTE DES INVENTAIRES ";

	public FactoryLivraisonDetail() {
		
	}

	public FactoryLivraisonDetail(boolean arg0) {
		super(arg0);
	}
	
	public EOLivraisonDetail insertLivraisonDetail(EOEditingContext monEOEditingContext, EOLivraison setLivraison, EOOrgan setOrgan, EOTypeCredit setTypeCredit,
			EOPlanComptable setPlanComptable, String setLidLibelle, BigDecimal setLidMontant, Integer setLidQuantite) throws Exception {

		
		try {
			EOLivraisonDetail newLivraisonDetail = EOLivraisonDetail.createLivraisonDetail(monEOEditingContext, setLidQuantite,setLidQuantite);
			
			System.out.println("FactoryLivraisonDetail.insertLivraisonDetail() ==> " );

			newLivraisonDetail.setLivraisonRelationship(setLivraison);
			newLivraisonDetail.setOrganRelationship(setOrgan);
			newLivraisonDetail.setTypeCreditRelationship(setTypeCredit);
			newLivraisonDetail.setPlanComptableRelationship(setPlanComptable);

			newLivraisonDetail.setLidLibelle(setLidLibelle);
			newLivraisonDetail.setLidMontant(setLidMontant);
			newLivraisonDetail.setLidQuantite(setLidQuantite);
			newLivraisonDetail.setLidReste(setLidQuantite);
			
			return newLivraisonDetail;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void updateLivraisonDetail(EOEditingContext monEOEditingContext, EOLivraisonDetail setLivraisonDetail, String setLidLibelle,
			BigDecimal setLidMontant, Integer setLidQuantite) throws Exception {
		try {
			System.out.println("FactoryLivraisonDetail.updateLivraisonDetail() ==>" );

			setLivraisonDetail.setLidLibelle(setLidLibelle);
			setLivraisonDetail.setLidMontant(setLidMontant);

			int variation = setLidQuantite.intValue() - setLivraisonDetail.lidQuantite().intValue();
			int newReste = setLivraisonDetail.lidReste().intValue() + variation;
			if (newReste < 0)
				throw new Exception("IMPOSSIBLE ! IL FAUT ANNULER DES BIENS");
			
			setLivraisonDetail.setLidReste(new Integer(newReste));
			setLivraisonDetail.setLidQuantite(setLidQuantite);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void deleteLivraisonDetail(EOEditingContext monEOEditingContext, EOLivraisonDetail setLivraisonDetail) throws Exception {
		try {
			System.out.println("FactoryLivraisonDetail.deleteLivraisonDetail() ==> " );

			if(setLivraisonDetail.inventaires().count() > 0)
				throw new Exception(ERROR_DEL_INVENTAIRE);
			monEOEditingContext.deleteObject(setLivraisonDetail);
		} catch (Exception e) {
			throw e;
		}
	}
}
