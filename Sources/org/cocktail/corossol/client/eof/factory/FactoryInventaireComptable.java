package org.cocktail.corossol.client.eof.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.Factory;
import org.cocktail.application.palette.models.NSMutableArrayDisplayGroup;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOAmortissement;
import org.cocktail.corossol.client.eof.metier.EOAmortissementOrigine;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EODegressifCoef;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrv;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie;
import org.cocktail.corossol.client.eof.metier.EOInventaireSortie;
import org.cocktail.corossol.client.eof.metier.EOOrigineFinancement;
import org.cocktail.corossol.client.eof.procedure.ProcedureSupprimerSortie;
import org.cocktail.corossol.client.eof.process.ProcessCleInventaireComptable;
import org.cocktail.corossol.client.finder.FinderAmortissement;
import org.cocktail.corossol.client.finder.FinderComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FactoryInventaireComptable extends Factory {

	private ProcessCleInventaireComptable processCleInventaireComptable;
	
	public FactoryInventaireComptable() {
		this(false);
	}

	public FactoryInventaireComptable(boolean arg0) {
		super(arg0);
		this.processCleInventaireComptable = new ProcessCleInventaireComptable();
	}

	private static final String ERROR_DEL_INVENTAIRE_COMTPABLE_CLE =  "IMPOSSIBLE , il existe un numero d'inventaire";
	private static final String ERROR_DEL_DEPENSE =  "IMPOSSIBLE , cet inventaire comptable est lie a une depense";

	public EOInventaireComptable insertInventaireComptable(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur, 
			EOOrigineFinancement setOrigineFinancement, EOExercice setExercice, EOCleInventaireComptable setCleInventaireComptable, 
			BigDecimal setInvcMontantResiduel, BigDecimal setInvcMontantAcquisition, String setInvcEtat, EODegressifCoef coef, NSArray inventairesAssocies)throws Exception {

		try {
			EOInventaireComptable newInventaireComptable = EOInventaireComptable.createInventaireComptable(monEOEditingContext);

			newInventaireComptable.setUtilisateurRelationship(setUtilisateur);
			newInventaireComptable.setOrigineFinancementRelationship(setOrigineFinancement);
			newInventaireComptable.setExerciceRelationship(setExercice);
			newInventaireComptable.setCleInventaireComptableRelationship(setCleInventaireComptable);
			newInventaireComptable.setVDepenseRelationship(null);
			newInventaireComptable.setDegressifCoefRelationship(coef);

			newInventaireComptable.setInvcMontantResiduel(setInvcMontantResiduel);
			newInventaireComptable.setInvcMontantAcquisition(setInvcMontantAcquisition);
			newInventaireComptable.setInvcMontantAmortissable(setInvcMontantAcquisition);
			newInventaireComptable.setInvcEtat(setInvcEtat);

			newInventaireComptable.setInventairesAssocies(inventairesAssocies);

			System.out.println("FactoryInventaireComptable.insertInventaireComptable() ==> "+newInventaireComptable);

			return newInventaireComptable;
		} catch (Exception e) {
			throw e;
		}
	}

	public void updateInventaireComptable(EOEditingContext monEOEditingContext, EOInventaireComptable setInventaireComptable,
			EOUtilisateur setUtilisateur, EOOrigineFinancement setOrigineFinancement, BigDecimal setInvcMontantResiduel,
			BigDecimal setInvcMontantAcquisition, String setInvcEtat, EODegressifCoef coef) throws Exception {
		try {
			System.out.println("FactoryInventaireComptable.updateInventaireComptable() ==> "+setInventaireComptable);
			setInventaireComptable.setDegressifCoefRelationship(coef);

			setInventaireComptable.setUtilisateurRelationship(setUtilisateur);
			setInventaireComptable.setOrigineFinancementRelationship(setOrigineFinancement);
			setInventaireComptable.setInvcMontantResiduel(setInvcMontantResiduel);
			setInventaireComptable.setInvcMontantAcquisition(setInvcMontantAcquisition);
			setInventaireComptable.setInvcEtat(setInvcEtat);
		} catch (Exception e) {
			throw e;
		}
	}

	public void deleteInventaireComptable(ApplicationCocktail app, EOInventaireComptable inventaireComptable, EOUtilisateur utilisateur) throws Exception {

			System.out.println("FactoryInventaireComptable.deleteInventaireComptable() ==> " + inventaireComptable);

			if(inventaireComptable.dpcoIdBoolean()) {
				throw new Exception(ERROR_DEL_DEPENSE);
			}
			EOEditingContext editingContext = app.getAppEditingContext();

			EOCleInventaireComptable cle  = inventaireComptable.cleInventaireComptable();
//			inventaireComptable.setCleInventaireComptableRelationship(null);

			deleteAmortissement(app, inventaireComptable);
			deleteBiensInventories(app, inventaireComptable);
			deleteOrigines(editingContext, inventaireComptable);
			deleteOrvs(editingContext, inventaireComptable);
			deleteSorties((ApplicationCorossol) app, inventaireComptable);
			deleteCleInventaireComptable(app, cle);

			editingContext.deleteObject(inventaireComptable);
	}
	
	private void deleteAmortissement(ApplicationCocktail app, EOInventaireComptable inventaireComptable) {
		NSMutableArrayDisplayGroup amortissements = FinderAmortissement.findLesAmortissements(app, inventaireComptable);
		for (int i = 0; i < amortissements.count(); i++) {
			EOAmortissement amortissement = (EOAmortissement) amortissements.objectAtIndex(i);
			app.getAppEditingContext().deleteObject(amortissement);			
		}
	}

	private void deleteCleInventaireComptable(ApplicationCocktail app, EOCleInventaireComptable cle) throws Exception {
		NSMutableArrayDisplayGroup inventairesPourCle = FinderComptable.findLesInventairesComptableDuneCleInventaire(app, cle);
		if (inventairesPourCle.count() <= 1) {
			processCleInventaireComptable.supprimerUneCleInventaireComptable(app.getAppEditingContext(),cle);
		}
	}
	
	private void deleteBiensInventories(ApplicationCocktail app, EOInventaireComptable inventaireComptable) {
		NSMutableArrayDisplayGroup biens = FinderComptable.findLesBiensInventories(app, inventaireComptable);
		for (int i = 0; i < biens.count(); i++) {
			((EOInventaire) biens.objectAtIndex(i)).removeObjectFromBothSidesOfRelationshipWithKey(inventaireComptable, EOInventaire.INVENTAIRE_COMPTABLE_KEY);
		}
	}
	
	private void deleteOrigines(EOEditingContext editingContext, EOInventaireComptable inventaireComptable) {
		NSArray origines = inventaireComptable.inventaireComptableOrigs();
		if (origines != null && origines.count() > 0) {
			for (int idxOri = 0; idxOri < origines.count(); idxOri++) {
				EOInventaireComptableOrig origineCourante = (EOInventaireComptableOrig) origines.objectAtIndex(idxOri);
				NSArray amortissementOrigines = origineCourante.amortissementOrigines();
				for (int idxAmoOri = 0; idxAmoOri < amortissementOrigines.count(); idxAmoOri++) { 
					EOAmortissementOrigine amortissementOrigineCourant =  (EOAmortissementOrigine) amortissementOrigines.objectAtIndex(idxAmoOri);
					editingContext.deleteObject(amortissementOrigineCourant);
				}
				editingContext.deleteObject(origineCourante); 
			}
		}
	}
	
	private void deleteOrvs(EOEditingContext editingContext, EOInventaireComptable inventaireComptable) {
		NSArray invcOrvs = inventaireComptable.inventaireComptableOrvs();
		if (invcOrvs != null && invcOrvs.count() > 0) {
			for (int idx = 0; idx < invcOrvs.count(); idx++) {
				EOInventaireComptableOrv invcOrv = (EOInventaireComptableOrv) invcOrvs.objectAtIndex(idx); 
				editingContext.deleteObject(invcOrv);
			}
		}
	}
	
    private void deleteSorties(ApplicationCorossol app, EOInventaireComptable inventaireComptable) throws Exception {
        NSArray inventaires = inventaireComptable.inventaires(null, null, true);
        for (Object obj : inventaires) {
            EOInventaire inventaire = (EOInventaire) obj;
            NSArray sorties = inventaire.inventaireSorties();
            for (Object obj2 : sorties) {
                EOInventaireSortie sortie = (EOInventaireSortie) obj2;
                ProcedureSupprimerSortie.enregistrer(app, sortie);
            }
        }
    }
}
