package org.cocktail.corossol.client.eof.factory;

import org.cocktail.application.client.tools.Factory;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireCb;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryInventaireCb extends Factory {

	public FactoryInventaireCb(boolean arg0) {
		super(arg0);
	}

	public EOInventaireCb insertInventaire(EOEditingContext monEOEditingContext, EOInventaire inventaire, String code, String commentaire) throws Exception {
	
		try {
			
			EOInventaireCb newInventaire=EOInventaireCb.createInventaireCb(monEOEditingContext, code);
			
			newInventaire.setInventaireRelationship(inventaire);
			newInventaire.setIncbCode(code);
			newInventaire.setIncbCommentaire(commentaire);
			
			return newInventaire;
			
		} catch (Exception e) {
			throw e;
		}
	}

	public void updateInventaireCb(EOEditingContext monEOEditingContext, EOInventaireCb inventaireCb, String code, String commentaire) throws Exception {
		try {
			inventaireCb.setIncbCode(code);
			inventaireCb.setIncbCommentaire(commentaire);
		} catch (Exception e) {
			throw e;
		}
	}

	public void deleteInventaireCb(EOEditingContext monEOEditingContext, EOInventaireCb inventaireCb) throws Exception {
		try {
			monEOEditingContext.deleteObject(inventaireCb);
		} catch (Exception e) {
			throw e;
		}
	}
}
