package org.cocktail.corossol.client.eof.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOIndividuUlr;
import org.cocktail.application.client.eof.EOSalles;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.Factory;
import org.cocktail.corossol.client.eof.metier.EOCleInventairePhysique;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireCb;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOMagasin;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryInventaire extends Factory {

	private static final String ERROR_DEL_INVENTAIRE_COMTPABLE = "IMPOSSIBLE, IL EXISTE DES INVENTAIRES COMPTABLES";
	
	public FactoryInventaire(boolean arg0) {
		super(arg0);
	}

	public EOInventaire insertInventaire(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur, EOLivraisonDetail setLivraisonDetail,
			EOInventaireComptable setInventaireComptable, EOIndividuUlr setIndividuResp, EOIndividuUlr setIndividuTit, EOMagasin setMagasin,
			EOCleInventairePhysique setCleInventairePhysique, EOSalles setSalle, String setInvSerie, BigDecimal setInvMontantAcquisition,
			String setInvModele, String setInvMaint, String setInvGar, String setInvDoc, NSTimestamp setInvDateMaint, String setInvCommentaire) throws Exception {

		try {
			
			EOInventaire newInventaire = EOInventaire.createInventaire(monEOEditingContext);
			
			System.out.println("FactoryInventaire.creerInventaire() ==> " + newInventaire);

			newInventaire.setUtilisateurRelationship(setUtilisateur);
			newInventaire.setLivraisonDetailRelationship(setLivraisonDetail);
			newInventaire.setInventaireComptableRelationship(setInventaireComptable);
			newInventaire.setIndividuRespRelationship(setIndividuResp);

			newInventaire.setIndividuTitRelationship(setIndividuTit);
			newInventaire.setMagasinRelationship(setMagasin);
			newInventaire.setCleInventairePhysiqueRelationship(setCleInventairePhysique);
			newInventaire.setSallesRelationship(setSalle);

			newInventaire.setInvSerie(setInvSerie);
			newInventaire.setInvMontantAcquisition(setInvMontantAcquisition);
			newInventaire.setInvModele(setInvModele);
			newInventaire.setInvMaint(setInvMaint);
			newInventaire.setInvGar(setInvGar);
			newInventaire.setInvDoc(setInvDoc);
			newInventaire.setInvDateMaint(setInvDateMaint);
			newInventaire.setInvCommentaire(setInvCommentaire);

			// modifier le lidReste
			newInventaire.livraisonDetail().setLidReste(new Integer(newInventaire.livraisonDetail().lidReste().intValue() - 1));
			return newInventaire;
		} catch (Exception e) {
			throw e;
		}
	}

	public void updateInventaire(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur, EOInventaire setInventaire, EOIndividuUlr setIndividuResp,
			EOIndividuUlr setIndividuTit, EOMagasin setMagasin, EOSalles setSalle, String setInvSerie, BigDecimal setInvMontantAcquisition,
			String setInvModele, String setInvMaint, String setInvGar, String setInvDoc, NSTimestamp setInvDateMaint, 
			String setInvCommentaire) throws Exception {

		try {
			System.out.println("FactoryInventaire.updateInventaire() ==> " + setInventaire);

			setInventaire.setUtilisateurRelationship(setUtilisateur);
			setInventaire.setIndividuRespRelationship(setIndividuResp);
			setInventaire.setIndividuTitRelationship(setIndividuTit);
			setInventaire.setMagasinRelationship(setMagasin);
			setInventaire.setSallesRelationship(setSalle);

			setInventaire.setInvSerie(setInvSerie);
			setInventaire.setInvMontantAcquisition(setInvMontantAcquisition);
			setInventaire.setInvModele(setInvModele);
			setInventaire.setInvMaint(setInvMaint);
			setInventaire.setInvGar(setInvGar);
			setInventaire.setInvDoc(setInvDoc);
			setInventaire.setInvDateMaint(setInvDateMaint);
			setInventaire.setInvCommentaire(setInvCommentaire);
			
		} catch (Exception e) {
			throw e;
		}
	}

	public void deleteInventaire(EOEditingContext monEOEditingContext, EOInventaire setInventaire) throws Exception {
		try {
			System.out.println("FactoryInventaire.deleteInventaire() ==> " + setInventaire);

			if(setInventaire.inventaireComptable() != null)
				throw new Exception(ERROR_DEL_INVENTAIRE_COMTPABLE);
	
			// modifier le lidReste
			setInventaire.livraisonDetail().setLidReste(new Integer(setInventaire.livraisonDetail().lidReste().intValue() + 1));

			if (setInventaire.inventaireCbs().count()>0) {
				for (int i=setInventaire.inventaireCbs().count()-1; i>=0; i--)
					monEOEditingContext.deleteObject((EOInventaireCb)setInventaire.inventaireCbs().objectAtIndex(i));
			}
			monEOEditingContext.deleteObject(setInventaire);
		} catch (Exception e) {
			throw e;
		}
	}
}
