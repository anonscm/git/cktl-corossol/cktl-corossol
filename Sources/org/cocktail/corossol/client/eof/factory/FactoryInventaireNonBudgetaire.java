package org.cocktail.corossol.client.eof.factory;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.Factory;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;
import org.cocktail.corossol.client.eof.metier.EOOrigineFinancement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryInventaireNonBudgetaire extends Factory {
	
	public FactoryInventaireNonBudgetaire() {
		super();
	}

	public FactoryInventaireNonBudgetaire(boolean arg0) {
		super(arg0);
	}

	public EOInventaireNonBudgetaire insertInventaireNonBudgetaire(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur,
			EOPlanComptable setPlanComptable, EOOrigineFinancement setOrigineFinancement, EOOrgan setOrgan, 
			EOExercice setExercice, String setInbTypeAmort, String setInbNumeroSerie, BigDecimal setInbMontantResiduel, BigDecimal setInbMontant,
			String setInbInformations, String setInbFournisseur, String setInbFacture, Integer setInbDuree, NSTimestamp dateAcquisition) throws Exception {

		

		try {
			
			EOInventaireNonBudgetaire newInventaireNonBudgetaire = 
			                EOInventaireNonBudgetaire.createInventaireNonBudgetaire(
					                                              monEOEditingContext,
					                                              setInbDuree,
					                                              setInbMontant,
					                                              setInbMontantResiduel,
					                                              setInbTypeAmort,
					                                              setExercice,
					                                              setOrgan,
					                                              setOrigineFinancement,
					                                              setPlanComptable,
					                                              setUtilisateur);
			
			System.out.println("FactoryInventaireNonBudgetaire.insertInventaireNonBudgetaire() ==> " + newInventaireNonBudgetaire);

			newInventaireNonBudgetaire.setExerciceRelationship(setExercice);
			newInventaireNonBudgetaire.setUtilisateurRelationship(setUtilisateur);
			newInventaireNonBudgetaire.setPlanComptableRelationship(setPlanComptable);
			newInventaireNonBudgetaire.setOrigineFinancementRelationship(setOrigineFinancement);
			newInventaireNonBudgetaire.setOrganRelationship(setOrgan);
			newInventaireNonBudgetaire.setInventaireComptableRelationship(null);
			
			newInventaireNonBudgetaire.setInbTypeAmort(setInbTypeAmort);
			newInventaireNonBudgetaire.setInbNumeroSerie(setInbNumeroSerie);
			newInventaireNonBudgetaire.setInbMontantResiduel(setInbMontantResiduel);
			newInventaireNonBudgetaire.setInbMontant(setInbMontant);
			newInventaireNonBudgetaire.setInbInformations(setInbInformations);
			newInventaireNonBudgetaire.setInbFournisseur(setInbFournisseur);
			newInventaireNonBudgetaire.setInbFacture(setInbFacture);
			newInventaireNonBudgetaire.setInbDuree(setInbDuree);
			newInventaireNonBudgetaire.setInbDateAcquisition(dateAcquisition);

			return newInventaireNonBudgetaire;
		} catch (Exception e) {
			throw e;
		}
	}

	public void updateInventaireNonBudgetaire(EOEditingContext monEOEditingContext, EOInventaireNonBudgetaire monInventaireNonBudgetaire,
			EOUtilisateur setUtilisateur, EOPlanComptable setPlanComptable, EOOrigineFinancement setOrigineFinancement, EOOrgan setOrgan,
		    EOExercice setExercice,  String setInbTypeAmort, String setInbNumeroSerie,
			BigDecimal setInbMontantResiduel, BigDecimal setInbMontant, String setInbInformations, String setInbFournisseur, String setInbFacture,
			Integer setInbDuree, NSTimestamp dateAcquisition) throws Exception {
		try {
			monInventaireNonBudgetaire.setUtilisateurRelationship(setUtilisateur);
			monInventaireNonBudgetaire.setPlanComptableRelationship(setPlanComptable);
			monInventaireNonBudgetaire.setOrigineFinancementRelationship(setOrigineFinancement);
			monInventaireNonBudgetaire.setOrganRelationship(setOrgan);
			monInventaireNonBudgetaire.setExerciceRelationship(setExercice);
			
			monInventaireNonBudgetaire.setInbTypeAmort(setInbTypeAmort);
			monInventaireNonBudgetaire.setInbNumeroSerie(setInbNumeroSerie);
			monInventaireNonBudgetaire.setInbMontantResiduel(setInbMontantResiduel);
			monInventaireNonBudgetaire.setInbMontant(setInbMontant);
			monInventaireNonBudgetaire.setInbInformations(setInbInformations);
			monInventaireNonBudgetaire.setInbFournisseur(setInbFournisseur);
			monInventaireNonBudgetaire.setInbFacture(setInbFacture);
			monInventaireNonBudgetaire.setInbDuree(setInbDuree);
			monInventaireNonBudgetaire.setInbDateAcquisition(dateAcquisition);
			
			System.out.println("FactoryInventaireNonBudgetaire.updateInventaireNonBudgetaire() ==> " + monInventaireNonBudgetaire);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void deleteInventaireNonBudgetaire(EOEditingContext monEOEditingContext, EOInventaireNonBudgetaire monInventaireNonBudgetaire) throws Exception {
		try {
			System.out.println("FactoryInventaireNonBudgetaire.deleteInventaireNonBudgetaire() ==> " + monInventaireNonBudgetaire);
			monEOEditingContext.deleteObject(monInventaireNonBudgetaire);
		} catch (Exception e) {
			throw e;
		}
	}
}
