package org.cocktail.corossol.client.eof.process;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOPlanComptableAmo;
import org.cocktail.application.client.tools.Process;
import org.cocktail.corossol.client.eof.factory.FactoryCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;

import com.webobjects.eocontrol.EOEditingContext;

public class ProcessCleInventaireComptable extends Process {

	private static final boolean AVEC_LOGS = true;

	public EOCleInventaireComptable ajouterUneCleInventaireComptable(EOEditingContext monEOEditingContext, EOPlanComptableAmo setPlanComptableAmort,
			EOPlanComptable setPlanComptable, EOExercice setExercice, String setClicTypeAmort, Integer setClicProRata, String setClicNumero,
			Integer setClicNbEtiquette, String setClicEtat, Integer setClicDureeAmort, String setClicCr, String setClicComp) throws Exception {
		try {
			return new FactoryCleInventaireComptable(AVEC_LOGS).insertCleInventaireComptable(monEOEditingContext, setPlanComptableAmort, setPlanComptable,
					setExercice, setClicTypeAmort, setClicProRata, setClicNumero, setClicNbEtiquette, setClicEtat, setClicDureeAmort, setClicCr, setClicComp);
		} catch (Exception e) {
			throw e;
		}
	}

	public void modifierUneCleInventaireComptable(EOEditingContext monEOEditingContext, EOCleInventaireComptable setCleInventaireComptable,
			String setClicTypeAmort, Integer setClicProRata, Integer setClicNbEtiquette, String setClicEtat, Integer setClicDureeAmort) throws Exception {
		try {
			new FactoryCleInventaireComptable(AVEC_LOGS).updateCleInventaireComptable(monEOEditingContext, setCleInventaireComptable, setClicTypeAmort,
					setClicProRata, setClicNbEtiquette, setClicEtat, setClicDureeAmort);
		} catch (Exception e) {
			throw e;
		}
	}

	public void supprimerUneCleInventaireComptable(EOEditingContext monEOEditingContext, EOCleInventaireComptable setCleInventaireComptable) throws Exception {
		try {
			new FactoryCleInventaireComptable(AVEC_LOGS).deleteCleInventaireComptable(monEOEditingContext, setCleInventaireComptable);
		} catch (Exception e) {
			throw e;
		}
	}
}
