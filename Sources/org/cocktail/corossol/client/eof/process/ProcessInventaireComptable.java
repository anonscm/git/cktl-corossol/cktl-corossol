package org.cocktail.corossol.client.eof.process;

import java.math.BigDecimal;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.Process;
import org.cocktail.corossol.client.eof.factory.FactoryInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EODegressifCoef;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOOrigineFinancement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class ProcessInventaireComptable extends Process {

	private static final boolean AVEC_LOGS = true;

	public EOInventaireComptable ajouterUnInventaireComptable(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur, 
			EOOrigineFinancement setOrigineFinancement, EOExercice setExercice, EOCleInventaireComptable setCleInventaireComptable,
			BigDecimal setInvcMontantResiduel, BigDecimal setInvcMontantAcquisition, String setInvcEtat, EODegressifCoef coef, NSArray inventairesAssocies) throws Exception {
		try {
			return new FactoryInventaireComptable(AVEC_LOGS).insertInventaireComptable(monEOEditingContext, setUtilisateur, setOrigineFinancement, setExercice,
					setCleInventaireComptable, setInvcMontantResiduel, setInvcMontantAcquisition, setInvcEtat, coef, inventairesAssocies);
		} catch (Exception e) {
			throw e;
		}
	}

	public void modifierUnInventaireComptable(EOEditingContext monEOEditingContext, EOInventaireComptable setInventaireComptable, EOUtilisateur setUtilisateur,	
			EOOrigineFinancement setOrigineFinancement, BigDecimal setInvcMontantResiduel, BigDecimal setInvcMontantAcquisition, String setInvcEtat, 
			EODegressifCoef coef) throws Exception {
		try {
			new FactoryInventaireComptable(AVEC_LOGS).updateInventaireComptable(monEOEditingContext, setInventaireComptable, setUtilisateur, setOrigineFinancement,
					setInvcMontantResiduel, setInvcMontantAcquisition, setInvcEtat, coef);
		} catch (Exception e) {
			throw e;
		}
	}

	public void supprimerUnInventaireComptable(ApplicationCocktail app, EOInventaireComptable setInventaireComptable, 
			EOUtilisateur setUtilisateur) throws Exception {
		try {
			new FactoryInventaireComptable(AVEC_LOGS).deleteInventaireComptable(app, setInventaireComptable, setUtilisateur);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void addCleInventaireComptable(EOInventaireComptable eog,EOCleInventaireComptable eo){
		eog.setCleInventaireComptableRelationship(eo);
	}
}
