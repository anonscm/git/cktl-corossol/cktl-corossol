package org.cocktail.corossol.client.eof.process;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.Process;
import org.cocktail.corossol.client.eof.factory.FactoryInventaireNonBudgetaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;
import org.cocktail.corossol.client.eof.metier.EOOrigineFinancement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class ProcessInventaireNonBudgetaire extends Process {

	private static final boolean AVEC_LOGS = true;

	public EOInventaireNonBudgetaire ajouterUnInventaireNonBudgetaire(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur,
			EOPlanComptable setPlanComptable, EOOrigineFinancement financement, EOOrgan setOrgan,
			EOExercice setExercice, String setInbTypeAmort, String setInbNumeroSerie, BigDecimal setInbMontantResiduel, BigDecimal setInbMontant,
			String setInbInformations, String setInbFournisseur, String setInbFacture, Integer setInbDuree, NSTimestamp dateAcquisition) throws Exception {
		try {
			return new FactoryInventaireNonBudgetaire(AVEC_LOGS).insertInventaireNonBudgetaire(monEOEditingContext, setUtilisateur, setPlanComptable, financement,
					setOrgan, setExercice, setInbTypeAmort, setInbNumeroSerie, setInbMontantResiduel, setInbMontant,
					setInbInformations, setInbFournisseur, setInbFacture, setInbDuree, dateAcquisition);
		} catch (Exception e) {
			throw e;
		}
	}

	public void modifierUnInventaireNonBudgetaire(EOEditingContext monEOEditingContext, EOInventaireNonBudgetaire monInventaireNonBudgetaire,
			EOUtilisateur setUtilisateur, EOPlanComptable setPlanComptable, EOOrigineFinancement setOrigineFinancement, EOOrgan setOrgan,
			EOExercice setExercice, String setInbTypeAmort, String setInbNumeroSerie, BigDecimal setInbMontantResiduel, BigDecimal setInbMontant,
			String setInbInformations, String setInbFournisseur, String setInbFacture, Integer setInbDuree, NSTimestamp dateAcquisition) throws Exception {
		try {
			new FactoryInventaireNonBudgetaire(AVEC_LOGS).updateInventaireNonBudgetaire(monEOEditingContext, monInventaireNonBudgetaire, setUtilisateur, setPlanComptable,
					setOrigineFinancement, setOrgan, setExercice, setInbTypeAmort, setInbNumeroSerie, setInbMontantResiduel, setInbMontant,
					setInbInformations, setInbFournisseur, setInbFacture, setInbDuree, dateAcquisition);
		} catch (Exception e) {
			throw e;
		}
	}

	public void supprimerUnInventaireNonBudgetaire(EOEditingContext monEOEditingContext, EOInventaireNonBudgetaire monInventaireNonBudgetaire) throws Exception {
		try {
			new FactoryInventaireNonBudgetaire(AVEC_LOGS).deleteInventaireNonBudgetaire(monEOEditingContext, monInventaireNonBudgetaire);
		} catch (Exception e) {
			throw e;
		}
	}
}
