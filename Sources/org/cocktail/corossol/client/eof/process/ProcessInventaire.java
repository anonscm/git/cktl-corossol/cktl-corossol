package org.cocktail.corossol.client.eof.process;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOIndividuUlr;
import org.cocktail.application.client.eof.EOSalles;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.Process;
import org.cocktail.corossol.client.eof.factory.FactoryInventaire;
import org.cocktail.corossol.client.eof.metier.EOCleInventairePhysique;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOMagasin;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class ProcessInventaire extends Process {

	private static final boolean AVEC_LOGS = true;

	public ProcessInventaire(boolean arg0) {
		super(arg0);
	}

	public EOInventaire ajouterUnInventaire(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur, EOLivraisonDetail setLivraisonDetail,
			EOInventaireComptable setInventaireComptable, EOIndividuUlr setIndividuResp, EOIndividuUlr setIndividuTit, EOMagasin setMagasin,
			EOCleInventairePhysique setCleInventairePhysique, EOSalles setSalle, String setInvSerie, BigDecimal setInvMontantAcquisition,
			String setInvModele, String setInvMaint, String setInvGar, String setInvDoc, NSTimestamp setInvDateMaint, String setInvCommentaire) throws Exception {
		try {
			return new FactoryInventaire(AVEC_LOGS).insertInventaire(monEOEditingContext, setUtilisateur, setLivraisonDetail, setInventaireComptable, setIndividuResp,
					setIndividuTit, setMagasin, setCleInventairePhysique, setSalle, setInvSerie, setInvMontantAcquisition, setInvModele, setInvMaint,
					setInvGar, setInvDoc, setInvDateMaint, setInvCommentaire);
		} catch (Exception e) {
			throw e;
		}
	}

	public void modifierUnInventaire(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur, EOInventaire setInventaire, EOIndividuUlr setIndividuResp,
			EOIndividuUlr setIndividuTit, EOMagasin setMagasin, EOSalles setSalle, String setInvSerie, BigDecimal setInvMontantAcquisition, String setInvModele,
			String setInvMaint, String setInvGar, String setInvDoc,	NSTimestamp setInvDateMaint, String setInvCommentaire) throws Exception {
		try {
			new FactoryInventaire(AVEC_LOGS).updateInventaire(monEOEditingContext, setUtilisateur, setInventaire, setIndividuResp, setIndividuTit, setMagasin, setSalle,
					setInvSerie, setInvMontantAcquisition, setInvModele, setInvMaint, setInvGar, setInvDoc, setInvDateMaint, setInvCommentaire);
		} catch (Exception e) {
			throw e;
		}
	}

	public void supprimerUnInventaire(EOEditingContext monEOEditingContext, EOInventaire setInventaire) throws Exception {
		try {
			new FactoryInventaire(AVEC_LOGS).deleteInventaire(monEOEditingContext, setInventaire);
		} catch (Exception e) {
			throw e;
		}
	}

    public void addIndividuResp(EOInventaire eog, EOIndividuUlr eo) {
    	eog.setIndividuRespRelationship(eo);
	}
    
    public void addInventaireComptable(EOInventaire eog, EOInventaireComptable eo) {
    	eog.setInventaireComptableRelationship(eo);
	}
    
    public void addIndividuTit(EOInventaire eog, EOIndividuUlr eo) {
    	eog.setIndividuTitRelationship(eo);
	}
    
    public void addLivraisonDetail(EOInventaire eog, EOLivraisonDetail eo) {
    	eog.setLivraisonDetailRelationship(eo);
	}
    
    public void addUtilisateur(EOInventaire eog, EOUtilisateur eo) {
    	eog.setUtilisateurRelationship(eo);
	}
    
    public void addCleInventairePhysique(EOInventaire eog, EOCleInventairePhysique eo) {
    	eog.setCleInventairePhysiqueRelationship(eo);
	}
    
	public void addMagasin(EOInventaire eog, EOMagasin eo) {
	  	eog.setMagasinRelationship(eo);	
	}
	
	public void addSalles(EOInventaire eog, EOSalles eo) {
	  	eog.setSallesRelationship(eo);	
	}
}
