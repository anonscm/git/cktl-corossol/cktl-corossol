package org.cocktail.corossol.client.eof.process;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOOrgan;
import org.cocktail.application.client.eof.EOPlanComptable;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.corossol.client.eof.factory.FactoryLivraisonDetail;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraisonDetail;

import com.webobjects.eocontrol.EOEditingContext;



public class ProcessLivraisonDetail extends ProcessLivraison {

	public ProcessLivraisonDetail() {
	}

	public ProcessLivraisonDetail(boolean arg0) {
		super(arg0);
	}

	public EOLivraisonDetail ajouterUnLivraisonDetail(EOEditingContext monEOEditingContext, EOLivraison setLivraison, EOOrgan setOrgan, EOTypeCredit setTypeCredit,
			EOPlanComptable setPlanComptable, String setLidLibelle, BigDecimal setLidMontant, BigDecimal setLidQuantite) throws Exception {
		try {
			return new FactoryLivraisonDetail().insertLivraisonDetail(monEOEditingContext, setLivraison, setOrgan, setTypeCredit, setPlanComptable,
					setLidLibelle, setLidMontant, setLidQuantite.intValue());
		} catch (Exception e) {
			throw e;
		}
	}

	public void modifierUnLivraisonDetail(EOEditingContext monEOEditingContext, EOLivraisonDetail setLivraisonDetail, String setLidLibelle,
			BigDecimal setLidMontant, BigDecimal setLidQuantite) throws Exception {
		try {
			new FactoryLivraisonDetail().updateLivraisonDetail(monEOEditingContext, setLivraisonDetail, setLidLibelle, setLidMontant, setLidQuantite.intValue());
			for (int i = 0; i < setLivraisonDetail.inventaires().count(); i++)
				((EOInventaire)setLivraisonDetail.inventaires().objectAtIndex(i)).setInvMontantAcquisition(setLidMontant);
		} catch (Exception e) {
			throw e;
		}
	}

	public void supprimerUnLivraisonDetail(EOEditingContext monEOEditingContext, EOLivraisonDetail setLivraisonDetail) throws Exception {
		try {
			new FactoryLivraisonDetail().deleteLivraisonDetail(monEOEditingContext, setLivraisonDetail);
		} catch (Exception e) {
			throw e;
		}
	}
}
