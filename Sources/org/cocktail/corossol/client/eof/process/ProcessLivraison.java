package org.cocktail.corossol.client.eof.process;

import org.cocktail.application.client.eof.EODevise;
import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.application.client.tools.Process;
import org.cocktail.corossol.client.eof.factory.FactoryLivraison;
import org.cocktail.corossol.client.eof.metier.EOLivraison;
import org.cocktail.corossol.client.eof.metier.EOMagasin;
import org.cocktail.corossol.client.eof.metier.EOVCommande;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class ProcessLivraison extends Process {

	public ProcessLivraison() {
		
	}

	public ProcessLivraison(boolean arg0) {
		super(true);
		
	}

	public EOLivraison creerUneLivraison(EOEditingContext monEOEditingContext, EOUtilisateur setUtilisateur, EODevise setDevise, EOExercice setExercice,
			EOVCommande setCommande, EOMagasin setMagasin, String setLivReception, String setLivNumero, String setLivDivers, NSTimestamp setLivDate) throws Exception {
		try {
			return  new FactoryLivraison().insertLivraison(monEOEditingContext, setUtilisateur, setDevise, setExercice, setCommande, setMagasin, setLivReception,
					setLivNumero, setLivDivers, setLivDate);
		} catch (Exception e) {
			throw e;
		}
	}

	public void majUneLivraison(EOEditingContext monEOEditingContext, EOLivraison maLivraison, EOUtilisateur setUtilisateur, EOMagasin setMagasin, String setLivReception,
			String setLivNumero, String setLivDivers, NSTimestamp setLivDate) throws Exception {
		try {
			new FactoryLivraison().updateLivraison(monEOEditingContext, maLivraison, setUtilisateur, setMagasin, setLivReception, setLivNumero, setLivDivers, setLivDate);
		} catch (Exception e) {
			throw e;
		}
	}

	public void supprimerUneLivraison(EOEditingContext monEOEditingContext, EOLivraison maLivraison) throws Exception {
		try {
			new FactoryLivraison().deleteLivraison(monEOEditingContext, maLivraison);
		} catch (Exception e) {
			throw e;
		}
	}
}
