package org.cocktail.corossol.client.eof.process;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.tools.Process;
import org.cocktail.corossol.client.eof.factory.FactoryCleInventairePhysique;
import org.cocktail.corossol.client.eof.metier.EOCleInventairePhysique;

import com.webobjects.eocontrol.EOEditingContext;

public class ProcessCleInventairePhysique extends Process {

	private static final boolean AVEC_LOGS = false;

	public ProcessCleInventairePhysique() {
	}

	public ProcessCleInventairePhysique(boolean arg0) {
		super(arg0);
	}
	
	public EOCleInventairePhysique ajouterUneCleInventairePhysique(EOEditingContext monEOEditingContext, EOExercice setExercice, String setClipNumero,
			Integer setClipNbEtiquette, String setClipEtat) throws Exception {
		try {
			return new FactoryCleInventairePhysique(AVEC_LOGS).insertCleInventairePhysique(monEOEditingContext, setExercice, setClipNumero, setClipNbEtiquette, setClipEtat);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void modifierUneCleInventairePhysique(EOEditingContext monEOEditingContext, EOCleInventairePhysique setCleInventairePhysique, 
			Integer setClipNbEtiquette) throws Exception {
		try {
			new FactoryCleInventairePhysique(AVEC_LOGS).updateCleInventairePhysique(monEOEditingContext,setCleInventairePhysique, setClipNbEtiquette);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void supprimerUneCleInventairePhysique(EOEditingContext monEOEditingContext, EOCleInventairePhysique setCleInventairePhysique) throws Exception {
		try {
			new FactoryCleInventairePhysique(AVEC_LOGS).deleteCleInventairePhysique(monEOEditingContext, setCleInventairePhysique);
		} catch (Exception e) {
			throw e;
		}
	}
}
