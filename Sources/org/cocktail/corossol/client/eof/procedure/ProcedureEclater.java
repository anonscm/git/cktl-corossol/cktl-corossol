package org.cocktail.corossol.client.eof.procedure;



import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureEclater {

	private static final String PROCEDURE_NAME = "eclater";

	public static boolean enregistrer(ApplicationCorossol app, EOCleInventaireComptable origine, NSArray inventaires) throws Exception {
		if (origine==null || inventaires==null || inventaires.count()==0)
			return false;
		app.executeStoredProcedure(ProcedureEclater.PROCEDURE_NAME, 
				ProcedureEclater.construireDictionnaire(app.getAppEditingContext(), origine, inventaires));
		return true;
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOCleInventaireComptable origine, NSArray inventaires) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
				"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(origine)}, false);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("clicId"), "01_a_clic_id_origine");

		String chaine="";
		for (int i=0; i<inventaires.count(); i++)
			chaine=chaine+((NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
					new Object[]{ec.globalIDForObject((EOInventaire)inventaires.objectAtIndex(i))}, false)).objectForKey("invId")+"$";
		chaine=chaine+"$";
		dico.takeValueForKey(chaine, "10_a_chaine_inventaires");

		return dico;
	}
}