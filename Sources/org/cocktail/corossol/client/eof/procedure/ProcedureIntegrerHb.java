package org.cocktail.corossol.client.eof.procedure;

import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureIntegrerHb {

	private static final String PROCEDURE_NAME = "integrerHb";

	public static void enregistrer(ApplicationCorossol app, EOInventaireNonBudgetaire inventaireNonBudgetaire) throws Exception{
		if (inventaireNonBudgetaire==null)
			return;
		
		app.executeStoredProcedure(ProcedureIntegrerHb.PROCEDURE_NAME, 
				ProcedureIntegrerHb.construireDictionnaire(app.getAppEditingContext(), inventaireNonBudgetaire));
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOInventaireNonBudgetaire inventaireNonBudgetaire) {
		NSMutableDictionary dico=new NSMutableDictionary();
		dico.takeValueForKey(inventaireNonBudgetaire.inbIdBis(), "inbid");
		return dico;
	}
}