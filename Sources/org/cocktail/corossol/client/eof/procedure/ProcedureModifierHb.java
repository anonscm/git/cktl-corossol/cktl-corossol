package org.cocktail.corossol.client.eof.procedure;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureModifierHb {

	private static final String PROCEDURE_NAME = "modifierHb";

	public static void enregistrer(ApplicationCorossol app, EOInventaireNonBudgetaire inventaireNonBudgetaire, EOUtilisateur utilisateur) throws Exception{
		if (inventaireNonBudgetaire==null)
			return;
		
		inventaireNonBudgetaire.setUtilisateurRelationship(utilisateur);
		
		app.executeStoredProcedure(ProcedureModifierHb.PROCEDURE_NAME, 
				ProcedureModifierHb.construireDictionnaire(app.getAppEditingContext(), inventaireNonBudgetaire));
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOInventaireNonBudgetaire inventaireNonBudgetaire) {
		NSMutableDictionary dico=new NSMutableDictionary();
		dico.takeValueForKey(inventaireNonBudgetaire.inbIdBis(), "inbid");
		return dico;
	}
}