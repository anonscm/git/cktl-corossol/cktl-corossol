package org.cocktail.corossol.client.eof.procedure;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureEnregistrerHb {

	private static final String PROCEDURE_NAME = "enregistrerHb";

	public static boolean enregistrer(ApplicationCorossol app, EOInventaireNonBudgetaire inventaireNonBudgetaire, EOUtilisateur utilisateur) throws Exception{
		if (inventaireNonBudgetaire==null)
			return false;
		
		inventaireNonBudgetaire.setUtilisateurRelationship(utilisateur);
		
		inventaireNonBudgetaire.validateForSave();
		
		app.executeStoredProcedure(ProcedureEnregistrerHb.PROCEDURE_NAME, 
				ProcedureEnregistrerHb.construireDictionnaire(app.getAppEditingContext(), inventaireNonBudgetaire));
		
		return true;
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOInventaireNonBudgetaire inventaireNonBudgetaire) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		if (inventaireNonBudgetaire.inbIdBis()!=null)
			dico.takeValueForKey(inventaireNonBudgetaire.inbIdBis(), "10_a_inb_id");
		
		if (inventaireNonBudgetaire.utilisateur()!=null) {
			EOGlobalID globalId=ec.globalIDForObject(inventaireNonBudgetaire.utilisateur());
			if (globalId!=null && !globalId.isTemporary()) {
				dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
						"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
						new Object[]{ec.globalIDForObject(inventaireNonBudgetaire.utilisateur())}, false);
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("utlOrdre"), "15_a_utl_ordre");
			}
		}

		if (inventaireNonBudgetaire.exercice()!=null) {
			EOGlobalID globalId=ec.globalIDForObject(inventaireNonBudgetaire.exercice());
			if (globalId!=null && !globalId.isTemporary()) {
				dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
						"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
						new Object[]{ec.globalIDForObject(inventaireNonBudgetaire.exercice())}, false);
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("exeOrdre"), "20_a_exe_ordre");
			}
		}

		if (inventaireNonBudgetaire.organ()!=null) {
			EOGlobalID globalId=ec.globalIDForObject(inventaireNonBudgetaire.organ());
			if (globalId!=null && !globalId.isTemporary()) {
				dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
						"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
						new Object[]{ec.globalIDForObject(inventaireNonBudgetaire.organ())}, false);
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("orgId"), "25_a_org_id");
			}
		}

		if (inventaireNonBudgetaire.planComptable()!=null)
			dico.takeValueForKey(inventaireNonBudgetaire.planComptable().pcoNum(), "30_a_pco_num");

		dico.takeValueForKey(inventaireNonBudgetaire.inbDuree(), "35_a_inb_duree");
		dico.takeValueForKey(inventaireNonBudgetaire.inbTypeAmort(), "40_a_inb_type_amort");
		dico.takeValueForKey(inventaireNonBudgetaire.inbMontant(), "45_a_inb_montant");
		dico.takeValueForKey(inventaireNonBudgetaire.inbMontantResiduel(), "50_a_inb_montant_residuel");

		if (inventaireNonBudgetaire.origineFinancement()!=null) {
			EOGlobalID globalId=ec.globalIDForObject(inventaireNonBudgetaire.origineFinancement());
			if (globalId!=null && !globalId.isTemporary()) {
				dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
						"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
						new Object[]{ec.globalIDForObject(inventaireNonBudgetaire.origineFinancement())}, false);
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("orgfId"), "55_a_orig_id");
			}
		}

		dico.takeValueForKey(inventaireNonBudgetaire.inbNumeroSerie(), "60_a_inb_numero_serie");
		dico.takeValueForKey(inventaireNonBudgetaire.inbInformations(), "65_a_inb_informations");
		dico.takeValueForKey(inventaireNonBudgetaire.inbFournisseur(), "70_a_inb_fournisseur");
		dico.takeValueForKey(inventaireNonBudgetaire.inbFacture(), "75_a_inb_facture");

		dico.takeValueForKey(null, "80_a_invc_id");
		
		dico.takeValueForKey(inventaireNonBudgetaire.inbDateAcquisition(), "85_a_inb_date_acquisition");

		return dico;
	}
}