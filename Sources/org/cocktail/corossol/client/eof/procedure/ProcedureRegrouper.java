package org.cocktail.corossol.client.eof.procedure;



import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureRegrouper {

	private static final String PROCEDURE_NAME = "regrouper";

	public static boolean enregistrer(ApplicationCorossol app, EOCleInventaireComptable origine, EOCleInventaireComptable destination) throws Exception {
		if (origine==null || destination==null)
			return false;
		
		app.executeStoredProcedure(ProcedureRegrouper.PROCEDURE_NAME, 
				ProcedureRegrouper.construireDictionnaire(app.getAppEditingContext(), origine, destination));
		
		return true;
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOCleInventaireComptable origine, EOCleInventaireComptable destination) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
				"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(origine)}, false);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("clicId"), "01_a_clic_id_origine");

		dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
				"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(destination)}, false);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("clicId"), "02_a_clic_id_destination");

		return dico;
	}
}