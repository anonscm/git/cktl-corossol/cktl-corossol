package org.cocktail.corossol.client.eof.procedure;

import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaireSortie;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureEnregisrerSortie {

	private static final String PROCEDURE_NAME = "enregistrerSortie";

	public static void enregistrer(ApplicationCorossol app, EOInventaireSortie inventaireSortie) throws Exception{
		if (inventaireSortie==null)
			return;
		
		inventaireSortie.validateForSave();
		
		app.executeStoredProcedure(ProcedureEnregisrerSortie.PROCEDURE_NAME, 
				ProcedureEnregisrerSortie.construireDictionnaire(app.getAppEditingContext(), inventaireSortie));
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOInventaireSortie inventaireSortie) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		if (ec.globalIDForObject(inventaireSortie)!=null && !ec.globalIDForObject(inventaireSortie).isTemporary()) {
			dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(inventaireSortie)}, false);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("invsId"), "01_invs_id");
		}

		dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
				"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(inventaireSortie.inventaire())}, false);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("invId"), "02_inv_id");

		dico.takeValueForKey(inventaireSortie.invsMotif(), "03_invs_motif");
		dico.takeValueForKey(inventaireSortie.invsDate(), "04_invs_date");
		dico.takeValueForKey(inventaireSortie.invsVnc(), "05_invs_vnc");
		dico.takeValueForKey(inventaireSortie.invsValeurCession(), "06_invs_valeur_cession");

		dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
				"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(inventaireSortie.typeSortie())}, false);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("tysoId"), "07_tyso_id");

		return dico;
	}
}