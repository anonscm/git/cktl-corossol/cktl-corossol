package org.cocktail.corossol.client.eof.procedure;



import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureTicketImpression {

	private static final String PROCEDURE_NAME = "getTicketImpressions";

	public static Integer enregistrer(ApplicationCorossol app, NSArray inventairesComptables) throws Exception {
		if (inventairesComptables==null || inventairesComptables.count()==0)
			return null;

		app.executeStoredProcedure(ProcedureTicketImpression.PROCEDURE_NAME, 
				ProcedureTicketImpression.construireDictionnaire(app.getAppEditingContext(), inventairesComptables));
		return (Integer)app.returnValuesForLastStoredProcedureInvocation().valueForKey("returnValue");
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, NSArray inventairesComptables) {
		NSMutableDictionary dico=new NSMutableDictionary();
		
		String chaine = "";
		for (int i=0; i<inventairesComptables.count(); i++)
			chaine=chaine+((EOInventaireComptable)inventairesComptables.objectAtIndex(i)).cleInventaireComptable().clicIdBis()+"$";
		chaine= chaine+"$";

		dico.takeValueForKey(chaine,  "a01clicids");

		return dico;
	}
}