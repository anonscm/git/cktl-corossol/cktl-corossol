package org.cocktail.corossol.client.eof.procedure;

import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EODegressif;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureSupprimerDegressif {

	private static final String PROCEDURE_NAME = "supprimerDegressif";

	public static void enregistrer(ApplicationCorossol app, EODegressif degressif) throws Exception{
		if (degressif==null || app.getAppEditingContext().globalIDForObject(degressif)==null || app.getAppEditingContext().globalIDForObject(degressif).isTemporary())
			return;
		
		app.executeStoredProcedure(ProcedureSupprimerDegressif.PROCEDURE_NAME, 
				ProcedureSupprimerDegressif.construireDictionnaire(app.getAppEditingContext(), degressif));
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EODegressif degressif) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		if (ec.globalIDForObject(degressif)!=null && !ec.globalIDForObject(degressif).isTemporary()) {
			dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(degressif)}, false);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("dgrfId"), "01_a_dgrf_id");
		}

		return dico;
	}
}