package org.cocktail.corossol.client.eof.procedure;

import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EODegressif;
import org.cocktail.corossol.client.eof.metier.EODegressifCoef;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureEnregistrerDegressif {

	private static final String PROCEDURE_NAME = "enregistrerDegressif";

	public static void enregistrer(ApplicationCorossol app, EODegressif degressif) throws Exception{
		if (degressif==null)
			return;
		
		degressif.validateForSave();
		
		app.executeStoredProcedure(ProcedureEnregistrerDegressif.PROCEDURE_NAME, 
				ProcedureEnregistrerDegressif.construireDictionnaire(app.getAppEditingContext(), degressif));
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EODegressif degressif) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		if (ec.globalIDForObject(degressif)!=null && !ec.globalIDForObject(degressif).isTemporary()) {
			dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(degressif)}, false);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("dgrfId"), "01_a_dgrf_id");
		}

		dico.takeValueForKey(degressif.dgrfDateDebut(), "02_a_dgrf_date_debut");
		dico.takeValueForKey(degressif.dgrfDateFin(), "03_a_dgrf_date_fin");
		dico.takeValueForKey(chaineCoefficients(ec, degressif), "04_a_chaine_degressif_coef");

		return dico;
	}
	
	private static String chaineCoefficients(EOEditingContext ec, EODegressif degressif) {
		String chaine="";
		
		for (int i=0; i<degressif.degressifCoefs().count(); i++) {
			EODegressifCoef coef=(EODegressifCoef)degressif.degressifCoefs().objectAtIndex(i);
			chaine=chaine+coef.dgcoDureeMin()+"$"+coef.dgcoDureeMax()+"$"+coef.dgcoCoef()+"$";
		}
		return chaine+"$";
	}
}