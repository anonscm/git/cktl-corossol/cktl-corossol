package org.cocktail.corossol.client.eof.procedure;



import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaireSortie;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureSupprimerSortie {

	private static final String PROCEDURE_NAME = "supprimerSortie";

	public static boolean enregistrer(ApplicationCorossol app, EOInventaireSortie inventaireSortie) throws Exception {
		if (inventaireSortie==null)
			return false;
		
		EOGlobalID globalId=app.getAppEditingContext().globalIDForObject(inventaireSortie);
		if (globalId==null || globalId.isTemporary())
			return false;
		
		inventaireSortie.validateForDelete();

		app.executeStoredProcedure(ProcedureSupprimerSortie.PROCEDURE_NAME, 
				ProcedureSupprimerSortie.construireDictionnaire(app.getAppEditingContext(), inventaireSortie));
		
		return true;
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOInventaireSortie inventaireSortie) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
				"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(inventaireSortie)}, false);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("invsId"), "01_invs_id");

		return dico;
	}
}