package org.cocktail.corossol.client.eof.procedure;



import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaire;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptable;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableOrig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureEnregistrerInventaireComptable {

	private static final String PROCEDURE_NAME = "enregistrerComptable";

	public static boolean enregistrer(ApplicationCorossol app, EOInventaireComptable inventaireComptable) throws Exception {
		if (inventaireComptable==null)
			return false;

		inventaireComptable.validateForSave();

		app.executeStoredProcedure(ProcedureEnregistrerInventaireComptable.PROCEDURE_NAME, 
				ProcedureEnregistrerInventaireComptable.construireDictionnaire(app.getAppEditingContext(), inventaireComptable));

		return true;
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOInventaireComptable inventaireComptable) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		// CLIC_ID
		if (inventaireComptable.cleInventaireComptable()!=null) {
			EOGlobalID globalId=ec.globalIDForObject(inventaireComptable.cleInventaireComptable());
			if (globalId!=null && !globalId.isTemporary()) {
				dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
						"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
						new Object[]{ec.globalIDForObject(inventaireComptable.cleInventaireComptable())}, false);
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("clicId"), "05_a_clic_id");
			}
		}

		// DPCO_ID
		if (inventaireComptable.vDepense() != null) {
		    Integer dpcoIdBis = EOInventaire.DPCO_ID_HORS_BUDGET;
		    if (inventaireComptable.vDepense().dpcoIdBis() != null) {
		        dpcoIdBis = inventaireComptable.vDepense().dpcoIdBis();
		    }
		    dico.takeValueForKey(dpcoIdBis, "10_a_dpco_id");
		}
			
		// EXE_ORDRE
		if (inventaireComptable.exercice()!=null) {
			dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate",
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
					new Object[]{ec.globalIDForObject(inventaireComptable.exercice())}, false);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("exeOrdre"), "15_a_exe_ordre");
		}

		// INVC_DATE_ACQUISITION
		dico.takeValueForKey(inventaireComptable.invcDateAcquisition(), "20_a_invc_date_acquisition");
		
		// INVC_ETAT
		dico.takeValueForKey(inventaireComptable.invcEtat(), "25_a_invc_etat");

		// INVC_ID
		EOGlobalID globalId=ec.globalIDForObject(inventaireComptable);
		if (globalId!=null && !globalId.isTemporary()) {
			dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
					new Object[]{ec.globalIDForObject(inventaireComptable)}, false);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("invcId"), "30_a_invc_id");
		}

		// INVC_MONTANT_ACQUISITION
		dico.takeValueForKey(inventaireComptable.invcMontantAcquisition(), "35_a_invc_montant_acquisition");
		
		// INVC_MONTANT_RESIDUEL
		dico.takeValueForKey(inventaireComptable.invcMontantResiduel(), "40_a_invc_montant_residuel");
		
		// INVC_MONTANT_AMORTISSABLE
		dico.takeValueForKey(inventaireComptable.invcMontantAmortissable(), "43_a_invc_montant_amortissable");

		// CHAINE_ORIGINE
		String chaineOrig="";
		if (inventaireComptable.inventaireComptableOrigs()!=null && inventaireComptable.inventaireComptableOrigs().count()>0) {
			for (int i=0; i<inventaireComptable.inventaireComptableOrigs().count(); i++) {
				EOInventaireComptableOrig orig=(EOInventaireComptableOrig)inventaireComptable.inventaireComptableOrigs().objectAtIndex(i);

				dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
						"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(orig.origineFinancement())}, false);
				chaineOrig=chaineOrig+dicoForPrimaryKeys.objectForKey("orgfId")+"$";

				chaineOrig=chaineOrig+orig.icorPourcentage()+"$";
				
				if (orig.titre()!=null) {
					dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
							"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(orig.titre())}, false);
					chaineOrig=chaineOrig+dicoForPrimaryKeys.objectForKey("titId");
				} 
				chaineOrig=chaineOrig+"$";
				
				if (orig.icorMontant()!=null) {
					chaineOrig=chaineOrig+orig.icorMontant();
				} 
				chaineOrig=chaineOrig+"$";
			}
		}
		chaineOrig=chaineOrig+"$";
		dico.takeValueForKey(chaineOrig, "45_a_chaine_origine");

		// UTL_ORDRE
		if (inventaireComptable.utilisateur()!=null) {
			dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
					new Object[]{ec.globalIDForObject(inventaireComptable.utilisateur())}, false);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("utlOrdre"), "50_a_utl_ordre");
		}

		// INVC_DATE_SORTIE
		dico.takeValueForKey(inventaireComptable.invcDateSortie(), "55_a_invc_date_sortie");
		
		// DGCO_ID
		if (inventaireComptable.degressifCoef()!=null) {
			dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
					new Object[]{ec.globalIDForObject(inventaireComptable.degressifCoef())}, false);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("dgcoId"), "57_a_dgco_id");
		}

		// CHAINE_INVENTAIRE
		if (inventaireComptable.getInventairesAssocies().count()>0) {
			String chaine="";
			for (int i=0; i<inventaireComptable.getInventairesAssocies().count(); i++) {
				EOInventaire inventaire=(EOInventaire)inventaireComptable.getInventairesAssocies().objectAtIndex(i);
				dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
						"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(inventaire)}, false);
				chaine=chaine+dicoForPrimaryKeys.objectForKey("invId")+"$";
			}
			chaine=chaine+"$";
			dico.takeValueForKey(chaine, "58_a_chaine_inventaire");
		}
		
		// CLIC_COMP 
		dico.takeValueForKey(inventaireComptable.cleInventaireComptable().clicComp(), "60_a_clic_comp");
		// CLIC_CR
		dico.takeValueForKey(inventaireComptable.cleInventaireComptable().clicCr(), "65_a_clic_cr");
		// CLIC_DUREE_AMORT
		dico.takeValueForKey(inventaireComptable.cleInventaireComptable().clicDureeAmort(), "70_a_clic_duree_amort");
		// CLIC_ETAT
		dico.takeValueForKey(inventaireComptable.cleInventaireComptable().clicEtat(), "75_a_clic_etat");
		// CLIC_NB_ETIQUETTE
		dico.takeValueForKey(inventaireComptable.cleInventaireComptable().clicNbEtiquette(), "80_a_clic_nb_etiquette");
		// CLIC_PRO_RATA
		dico.takeValueForKey(inventaireComptable.cleInventaireComptable().clicProRata(), "85_a_clic_pro_rata");
		// CLIC_TYPE_AMORT
		dico.takeValueForKey(inventaireComptable.cleInventaireComptable().clicTypeAmort(), "90_a_clic_type_amort");
		// PCO_NUM
		dico.takeValueForKey(inventaireComptable.cleInventaireComptable().planComptable().pcoNum(), "95_a_pco_num");

		// PCOA_ID
		if (inventaireComptable.cleInventaireComptable().planComptableAmort()!=null) {
			dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, 
					new Object[]{ec.globalIDForObject(inventaireComptable.cleInventaireComptable().planComptableAmort())}, false);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("pcoaId"), "97_a_pcoa_id");
		}

		System.out.println(dico);
		return dico;
	}
}