package org.cocktail.corossol.client.eof.procedure;



import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureSupprimerSortieComptable {

	private static final String PROCEDURE_NAME = "supprimerSortieComptable";

	public static boolean enregistrer(ApplicationCorossol app, EOInventaireComptableSortie inventaireComptableSortie) throws Exception {
		if (inventaireComptableSortie==null)
			return false;
		
		EOGlobalID globalId=app.getAppEditingContext().globalIDForObject(inventaireComptableSortie);
		if (globalId==null || globalId.isTemporary())
			return false;
		
		inventaireComptableSortie.validateForDelete();

		app.executeStoredProcedure(ProcedureSupprimerSortieComptable.PROCEDURE_NAME, 
				ProcedureSupprimerSortieComptable.construireDictionnaire(app.getAppEditingContext(), inventaireComptableSortie));
		
		return true;
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOInventaireComptableSortie inventaireComptableSortie) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
				"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(inventaireComptableSortie)}, false);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("invcsId"), "01_invcs_id");

		return dico;
	}
}