package org.cocktail.corossol.client.eof.procedure;



import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOCleInventaireComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureAmortissementCle {

	private static final String PROCEDURE_NAME = "amortissementCle";

	public static boolean enregistrer(ApplicationCorossol app, EOCleInventaireComptable inventaire, int annule) throws Exception {
		if (inventaire==null)
			return false;
		app.executeStoredProcedure(ProcedureAmortissementCle.PROCEDURE_NAME, 
				ProcedureAmortissementCle.construireDictionnaire(app.getAppEditingContext(), inventaire, annule));
		return true;
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOCleInventaireComptable inventaire, int annule) {
		NSMutableDictionary dico=new NSMutableDictionary();

		dico.takeValueForKey(inventaire.clicIdBis(), "a01clicid");
		dico.takeValueForKey(new Integer(annule), "a02annule");
		return dico;
	}
}