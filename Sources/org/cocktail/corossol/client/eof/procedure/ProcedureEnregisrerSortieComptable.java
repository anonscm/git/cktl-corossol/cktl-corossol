package org.cocktail.corossol.client.eof.procedure;

import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaireComptableSortie;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureEnregisrerSortieComptable {

	private static final String PROCEDURE_NAME = "enregistrerSortieComptable";

	public static void enregistrer(ApplicationCorossol app, EOInventaireComptableSortie inventaireComptableSortie) throws Exception{
		if (inventaireComptableSortie==null)
			return;
		
		inventaireComptableSortie.validateForSave();
		
		app.executeStoredProcedure(ProcedureEnregisrerSortieComptable.PROCEDURE_NAME, 
				ProcedureEnregisrerSortieComptable.construireDictionnaire(app.getAppEditingContext(), inventaireComptableSortie));
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOInventaireComptableSortie inventaireComptableSortie) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary 		dicoForPrimaryKeys=null;

		if (ec.globalIDForObject(inventaireComptableSortie)!=null && !ec.globalIDForObject(inventaireComptableSortie).isTemporary()) {
			dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
					"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(inventaireComptableSortie)}, false);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("invcsId"), "01_invcs_id");
		}

		dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
				"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(inventaireComptableSortie.inventaireComptable())}, false);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("invcId"), "02_invc_id");

		dico.takeValueForKey(inventaireComptableSortie.invcsMotif(), "03_invcs_motif");
		dico.takeValueForKey(inventaireComptableSortie.invcsDate(), "04_invcs_date");
		dico.takeValueForKey(inventaireComptableSortie.invcsMontantAcquisitionSortie(), "05_invcs_montant_acq_sortie");
		dico.takeValueForKey(inventaireComptableSortie.invcsVnc(), "06_invcs_vnc");
		dico.takeValueForKey(inventaireComptableSortie.invcsValeurCession(), "07_invcs_valeur_cession");

		dicoForPrimaryKeys=(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.remoteCallDelagate", 
				"clientSideRequestPrimaryKeyForGlobalId", new Class[]{EOGlobalID.class}, new Object[]{ec.globalIDForObject(inventaireComptableSortie.typeSortie())}, false);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey("tysoId"), "08_tyso_id");

		return dico;
	}
}