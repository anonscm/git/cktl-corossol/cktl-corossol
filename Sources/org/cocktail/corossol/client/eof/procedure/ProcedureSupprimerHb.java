package org.cocktail.corossol.client.eof.procedure;

import org.cocktail.application.client.eof.EOUtilisateur;
import org.cocktail.corossol.client.ApplicationCorossol;
import org.cocktail.corossol.client.eof.metier.EOInventaireNonBudgetaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class ProcedureSupprimerHb {

	private static final String PROCEDURE_NAME = "supprimerHb";

	public static void enregistrer(ApplicationCorossol app, EOInventaireNonBudgetaire inventaireNonBudgetaire, EOUtilisateur utilisateur) throws Exception{
		if (inventaireNonBudgetaire==null)
			return;
		
		app.executeStoredProcedure(ProcedureSupprimerHb.PROCEDURE_NAME, 
				ProcedureSupprimerHb.construireDictionnaire(app.getAppEditingContext(), inventaireNonBudgetaire));
	}

	public static NSDictionary construireDictionnaire(EOEditingContext ec, EOInventaireNonBudgetaire inventaireNonBudgetaire) {
		NSMutableDictionary dico=new NSMutableDictionary();
		dico.takeValueForKey(inventaireNonBudgetaire.inbIdBis(), "inbid");
		return dico;
	}
}